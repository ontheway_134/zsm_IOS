
/*~!
 | @FUNC  框架通用
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRCommon_h
#define HRCommon_h

#pragma mark - ---------- HRFramework ----------

#import <AVFoundation/AVFoundation.h>
#import "02 HRMacro.h"
#import "03 HRConstant.h"
#import "04 HROnLineFunction.h"

#pragma mark - ---------- Vender ----------

#import "Masonry.h"
#import "SDAutoLayout.h"

#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"

#import "MJRefresh.h"
#import "MJExtension.h"

#pragma mark - ---------- Tool ----------

#import "HRNetworking.h"
#import "HSGifHeader.h"
#import "HTTPManager.h"
#import "ICN_StartViewController.h"
#import "ICN_SignViewController.h"
#import "NSString+Emoji.h"
#endif /* HRCommon_h */
