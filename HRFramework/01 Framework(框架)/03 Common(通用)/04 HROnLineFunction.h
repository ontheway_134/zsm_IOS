
/*~!
 | @FUNC  内敛函数
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  替代宏，更高效
 */

#ifndef _4_HROnLineFunction_h
#define _4_HROnLineFunction_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//访问URL
static inline BOOL OPEN_URL(NSString *urlStr) {
    if (SYSTEM_VERSION >= 10) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr] options:@{} completionHandler:nil];
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlStr]]) {
            return YES;
        } else {
            NSLog(@"无法访问-%@", urlStr);
            return NO;
        }
    } else {
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlStr]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
            return YES;
        } else {
            NSLog(@"无法访问-%@", urlStr);
            return NO;
        }
    }
}



#endif /* _4_HROnLineFunction_h */
