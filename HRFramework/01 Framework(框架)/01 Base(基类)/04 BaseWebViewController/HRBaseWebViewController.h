
/*~!
 | @FUNC  Web页面基类视图控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

@interface HRBaseWebViewController : HRBaseViewController <UIWebViewDelegate>
//webView
@property (strong, nonatomic) UIWebView *webView;
//url
@property (copy, nonatomic) NSString *url;
//context
@property (strong, nonatomic) JSContext *context;
//是否显示js标题
@property (assign, nonatomic) BOOL showJSTitle;

@end
