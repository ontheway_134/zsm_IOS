
/*~!
 | @FUNC  数据模型基类
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  解归档时model直接继承本类就不需要对属性解码与编码
 */

#import <Foundation/Foundation.h>

@interface HRBaseModel : NSObject

@end
