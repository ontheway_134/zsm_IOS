
/*~!
 | @FUNC  基类导航控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <UIKit/UIKit.h>

@interface HRBaseNavigationController : UINavigationController

@end
