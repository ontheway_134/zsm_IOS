
#import "HRBaseNavigationController.h"

@interface HRBaseNavigationController ()

@end

@implementation HRBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
}

/// adjust statusBar
- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

#pragma mark - ---------- Protocol Methods -----------


@end
