
/*~!
 | @FUNC  ViewModel基类
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <Foundation/Foundation.h>

@interface HRBaseViewModel : NSObject

//参数
@property (strong, nonatomic) NSMutableDictionary *paras;

@end
