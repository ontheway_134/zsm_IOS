//
//  ViewController.h
//  DLCY
//
//  Created by 辛忠志 on 2017/1/5.
//  Copyright © 2017年 辛忠志. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>


/**
 接口数据请求成功的block
 
 @param responseObject 返回数据
 */
typedef void (^HTTPSuccess) (id responseObject);

/**
 接口数据请求失败的block
 
 @param error 错误信息
 */
typedef void (^HTTPFailure) (NSError *error);

/**
 上传图片进度的block
 */
typedef void (^HTTPProgress) (int64_t bytesProgress, int64_t totalBytesProgress);


@interface HTTPManager : AFHTTPSessionManager
//3.1 网络状态
@property (assign, nonatomic) HRNetStatus netStatus;

+ (HTTPManager *)sharedManager;


/**
 post请求
 
 @param path       路径
 @param params     参数
 @param success    请求成功
 @param failure    请求失败
 */
+ (void)POST_PATH:(NSString *)path
           params:(NSDictionary *)params
          success:(HTTPSuccess)success
          failure:(HTTPFailure)failure;


@end
