//
//  ViewController.h
//  DLCY
//
//  Created by 辛忠志 on 2017/1/5.
//  Copyright © 2017年 辛忠志. All rights reserved.
//
#ifdef DEBUG
#   define WKLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define WKLog(...)
#endif

#import "HTTPManager.h"
@implementation HTTPManager
+ (HTTPManager *)sharedManager{
    static HTTPManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[HTTPManager alloc]init];
        manager.requestSerializer.timeoutInterval = 60;
        manager.responseSerializer = [AFJSONResponseSerializer serializer];//设置返回数据为json
        manager.requestSerializer.stringEncoding = NSUTF8StringEncoding;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                                  @"text/html",
                                                                                  @"text/json",
                                                                                  @"text/plain",
                                                                                  @"text/javascript",
                                                                                  @"text/xml",
                                                                                  @"image/*"]];
    });
    return manager;
}

//post请求
+ (void)POST_PATH:(NSString *)path
           params:(NSDictionary *)params
          success:(HTTPSuccess)success
          failure:(HTTPFailure)failure
{
    NSString *PATH= SF(@"%@%@%@%@%@", URL_PROTOCOL,URL_HOST, URL_PORT, URL_PATH_PREFIX,path);
    NSString *utf = [PATH stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    WKLog(@"请求地址----%@\n    请求参数----%@",PATH,params);
    if (path==nil) {
        return;
    }
    
    [[self sharedManager] POST:utf parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        WKLog(@"请求结果=%@",responseObject);
        // NSLog(@"\n[Request Success]:\n%@", responseObject);
        //NSString *res = responseObject[@"code"];
        //if ([res isKindOfClass:[NSNumber class]]) {
            NSInteger code  = [responseObject[@"code"]integerValue];
            if (code == 0) {
                success(responseObject[@"result"]);
            }else{
                failure(nil);
            }
        //}
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        WKLog(@"error=%@",error);
        NSLog(@"\n[Request Failure]:\n[Failure Code]:%ld\n[Failure Message]:%@", (long)error.code, error.userInfo);
        
        failure(error);
    }];
}




@end
