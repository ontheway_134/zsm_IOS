//
//  OrderThreeTableViewCell.h
//  TXAD
//
//  Created by shilei on 16/10/10.
//  Copyright © 2016年 risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface OrderThreeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *photoCollection;
@property (nonatomic, strong)NSMutableArray *selectedPhotos;   //照片


@end
