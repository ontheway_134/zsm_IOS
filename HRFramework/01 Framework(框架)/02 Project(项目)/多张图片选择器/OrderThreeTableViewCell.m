//
//  OrderThreeTableViewCell.m
//  TXAD
//
//  Created by shilei on 16/10/10.
//  Copyright © 2016年 risenb. All rights reserved.
//

#import "OrderThreeTableViewCell.h"
#import "TXAllHeader.h"
#import "PhotosCollectionViewCell.h"
#import "TXPhotoPopupWindowView.h"

#import <TZImagePickerController/TZImagePickerController.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TZImageManager.h"
#import "TZVideoPlayerController.h"




@interface OrderThreeTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>


@property (nonatomic, strong)NSMutableArray *selectedAssets;
@property (nonatomic, assign)CGFloat itemWH;
@property (nonatomic, assign)CGFloat margin;
@property(nonatomic, strong)UIImagePickerController *imagePickerController;
@property(nonatomic,strong)TXPhotoPopupWindowView *photoPopView;

@end


@implementation OrderThreeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.photoCollection registerNib:[UINib nibWithNibName:@"PhotosCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PhotosCollectionViewCell"];
    self.photoCollection.delegate=self;
    self.photoCollection.dataSource=self;
    
}

#pragma mark - ----- Protocol ----- -

#pragma mark - --- UICollectionViewDataSource ---

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;{
     return self.selectedPhotos.count + 1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    PhotosCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PhotosCollectionViewCell" forIndexPath:indexPath];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"PhotosCollectionViewCell" owner:self options:nil] lastObject];
    }
    if (indexPath.row == self.selectedPhotos.count) {
        cell.imageView.image = [UIImage imageNamed:@"+"];
        cell.deleteBtn.hidden = YES;
    } else {
        cell.imageView.image = self.selectedPhotos[indexPath.row];
        cell.deleteBtn.hidden = NO;
    }
    NSLog(@"%ld",indexPath.row);
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - --- UICollectionViewDelegate ---

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.selectedPhotos.count) {
        self.photoPopView=[[TXPhotoPopupWindowView alloc]init];
        self.photoPopView=[[[NSBundle mainBundle] loadNibNamed:@"TXPhotoPopupWindowView" owner:self options:nil] firstObject];
        [kApplicationShared.keyWindow addSubview:self.photoPopView];
        [self.photoPopView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.top.mas_equalTo(0);
        }];

    }
    else{
        //预览照片
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:self.selectedAssets selectedPhotos:self.selectedPhotos index:indexPath.row];
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            self.selectedPhotos = [NSMutableArray arrayWithArray:photos];
            self.selectedAssets = [NSMutableArray arrayWithArray:assets];
            [self.photoCollection reloadData];
            self.photoCollection.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
        }];
        [self.viewController presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

//调用相机
- (void)choosePhoto:(NSIndexPath *)indexPath{
    [self pushImagePickerController];
}

#pragma mark --- TZImagePickerController ---

- (void)pushImagePickerController {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc]initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.navigationBar.barTintColor = HRRGB0X(0X098d7);
    imagePickerVc.selectedAssets = self.selectedAssets;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowTakePicture = NO;
    [self.viewController presentViewController:imagePickerVc animated:YES completion:nil];
}

#pragma mark --- UIImagePickerController ---

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:@"public.image"]) {
        TZImagePickerController *tzImagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
        [tzImagePickerVc showProgressHUD];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [[TZImageManager manager] savePhotoWithImage:image completion:^(NSError *error){
            if (error) {
                [tzImagePickerVc hideProgressHUD];
            } else {
                [[TZImageManager manager] getCameraRollAlbum:NO allowPickingImage:YES completion:^(TZAlbumModel *model) {
                    [[TZImageManager manager] getAssetsFromFetchResult:model.result allowPickingVideo:NO allowPickingImage:YES completion:^(NSArray<TZAssetModel *> *models) {
                        [tzImagePickerVc hideProgressHUD];
                        TZAssetModel *assetModel = [models firstObject];
                        if (tzImagePickerVc.sortAscendingByModificationDate) {
                            assetModel = [models lastObject];
                        }
                        [_selectedAssets addObject:assetModel.asset];
                        [_selectedPhotos addObject:image];
                        [self.photoCollection reloadData];
                    }];
                }];
            }
        }];
    }
}

#pragma mark --- TZImagePickerControllerDelegate ---

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.selectedPhotos = [NSMutableArray arrayWithArray:photos];
    self.selectedAssets = [NSMutableArray arrayWithArray:assets];
    [self.photoCollection reloadData];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingVideo:(UIImage *)coverImage sourceAssets:(id)asset {
    _selectedPhotos = [NSMutableArray arrayWithArray:@[coverImage]];
    _selectedAssets = [NSMutableArray arrayWithArray:@[asset]];
    [self.photoCollection reloadData];
    self.photoCollection.contentSize = CGSizeMake(0, ((_selectedPhotos.count + 2) / 3 ) * (_margin + _itemWH));
}

- (void)deleteBtnClik:(UIButton *)sender {
    [_selectedPhotos removeObjectAtIndex:sender.tag];
    [_selectedAssets removeObjectAtIndex:sender.tag];
    
    [self.photoCollection performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        /**/
        [self.photoCollection deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self.photoCollection reloadData];
    }];
}

#pragma mark - ----- 懒加载 ----- -

- (NSMutableArray *)selectedPhotos{
    if (!_selectedPhotos) {
        _selectedPhotos = [NSMutableArray array];
    }
    return _selectedPhotos;
}

- (NSMutableArray *)selectedAssets{
    if (!_selectedAssets) {
        _selectedAssets = [NSMutableArray array];
    }
    return _selectedAssets;
}
@end
