//
//  ICN_BusinessNameTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TextComplitedBlock)(NSString *textKey , NSString *contentStr);

@interface ICN_BusinessNameTableViewCell : UITableViewCell

@property (nonatomic , copy)NSString *TitleStr; // 标题字段
@property (nonatomic , copy)NSString *contentStr; // 内容字段

@property (nonatomic , assign)NSInteger keyType; // 字段对应的枚举
@property (nonatomic , copy)TextComplitedBlock block; // 用于返回文本以及文本对应的字段的key的block
@property (weak, nonatomic) IBOutlet UITextField *contentTextField; // 内容文本输入框
@property (weak, nonatomic) IBOutlet UILabel *kLabel;/*金钱k*/


- (void)callBackWithTextComplitedBlock:(TextComplitedBlock)block;

@property (assign, nonatomic) BOOL isNumber;
@property (assign, nonatomic) NSInteger highestNumber;

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end
