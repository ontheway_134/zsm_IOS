//
//  ICN_PhotoTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TZImagePickerController,TZImageManager;


@interface ICN_PhotoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIButton *addButton;




@property (nonatomic , strong)TZImagePickerController *p_ImagePickerVC; // 跳转的图片选择器页面vc


@property (nonatomic , strong)NSMutableArray * imageListArr; // 当前图片列表数组
@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewListArr; // 当前相框列表


- (void)configImagesListWithUrlArrs:(NSArray *)usrls;

@property (nonatomic , assign , getter=isFristLoad)BOOL firstLoad;

@property (nonatomic , strong) NSMutableArray *urlArray;

@end
