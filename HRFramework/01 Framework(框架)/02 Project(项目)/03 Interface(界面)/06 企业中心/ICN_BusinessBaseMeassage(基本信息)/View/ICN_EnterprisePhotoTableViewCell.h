//
//  ICN_EnterprisePhotoTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/4/11.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_EnterprisePhotoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end
