//
//  ICN_BriefTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_BriefTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextView *BriefTextView;
@property (weak, nonatomic) IBOutlet UILabel *CountLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
