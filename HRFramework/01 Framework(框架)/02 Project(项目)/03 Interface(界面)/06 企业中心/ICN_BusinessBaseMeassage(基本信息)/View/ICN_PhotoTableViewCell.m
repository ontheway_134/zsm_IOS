//
//  ICN_PhotoTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PhotoTableViewCell.h"
#import "ICN_BusinessPhotoCollectionViewCell.h"
#import "TZImagePickerController.h"
#import "TZImageManager.h"


static const NSInteger ICN_TopicImageAddMaxCount = 3; // 默认添加图片的最大数量

@interface ICN_PhotoTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate>



@end

@implementation ICN_PhotoTableViewCell

- (void)setUrlArray:(NSMutableArray *)urlArray{
    _urlArray = urlArray;
    [self setNeedsLayout];
}

- (void)configImagesListWithUrlArrs:(NSArray *)usrls{
    
    self.firstLoad = YES;
    for (NSInteger i = 0; i < usrls.count; i++) {
        if (i < self.imageViewListArr.count) {
            [self.imageViewListArr[i] sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(usrls[i])] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }else{
            UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
            [imageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(usrls[i])] placeholderImage:[UIImage imageNamed:@"占位图"]];
            imageview.userInteractionEnabled = YES;
            [self.scrollView addSubview:imageview];
            [self.imageViewListArr addObject:imageview];
        }
        
    }
   
    
    BOOL isallowContentSize = self.imageViewListArr.count * 70.0 > self.scrollView.size.width - 80.0;
    if (isallowContentSize) {
        CGFloat width =  self.imageViewListArr.count * 70.0 + 80.0;
        CGFloat height = self.scrollView.height;
        self.scrollView.contentSize = CGSizeMake(width, height);
    }else{
        self.scrollView.contentSize = CGSizeZero;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.addButton = [[UIButton alloc] init];
    [self.addButton setBackgroundImage:[UIImage imageNamed:@"加"] forState:UIControlStateNormal];
    [self.addButton addTarget:self action:@selector(btnclick) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.addButton];
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.scrollView).offset(10.0);
        make.top.equalTo(self.scrollView).offset(10.0);
        make.size.mas_equalTo(CGSizeMake(self.scrollView.frame.size.height-30, self.scrollView.frame.size.height-30));
    }];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    if (self.isFristLoad) {
        if (self.urlArray) {
            [self configImagesListWithUrlArrs:[NSArray arrayWithArray:self.urlArray]];
            for (NSInteger i = 0; i < self.imageViewListArr.count; i++) {
                if (i == 0) {
                    [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.scrollView).offset(10.0);
                        make.top.equalTo(self.scrollView).offset(10.0);
                        make.size.mas_equalTo(CGSizeMake(self.scrollView.frame.size.height-20, self.scrollView.frame.size.height-20));
                    }];
                }else{
                    UIImageView *lastItem = self.imageViewListArr[i - 1];
                    [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(lastItem.mas_right).offset(10.0);
                        make.top.equalTo(lastItem);
                        make.size.equalTo(lastItem);
                    }];
                    
                }
            }
        }

        
        CGRect frame = CGRectMake(self.imageViewListArr.count * (self.scrollView.frame.size.height-10) + 10.0, 15.0, self.scrollView.frame.size.height-30, self.scrollView.frame.size.height-30);
        
        self.addButton = [[UIButton alloc] initWithFrame:frame];
        [self.addButton setBackgroundImage:[UIImage imageNamed:@"加"] forState:UIControlStateNormal];
        [self.addButton addTarget:self action:@selector(btnclick) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.addButton];

        
        self.addButton = nil;
        self.urlArray = nil;
        [self.imageViewListArr removeAllObjects];
        self.firstLoad = NO;
    }
}

- (void)btnclick {
    [[self viewController] presentViewController:self.p_ImagePickerVC animated:YES completion:nil];
}


#pragma mark --- TZImagePickerControllerDelegate ---
// The picker should dismiss itself; when it dismissed these handle will be called.
// You can also set autoDismiss to NO, then the picker don't dismiss itself.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的handle
// 你也可以设置autoDismiss属性为NO，选择器就不会自己dismis了
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{

    [_imageListArr removeAllObjects];
    [_imageViewListArr removeAllObjects];
    for (UIView *v in _scrollView.subviews) {
        [v removeFromSuperview];
    }
    
    if (isSelectOriginalPhoto) {
        // 当选中原图的时候获取到原图并添加到图片展示列表中
        [[TZImageManager manager] getOriginalPhotoWithAsset:assets.firstObject completion:^(UIImage *photo, NSDictionary *info) {
            [self.imageListArr addObject:photo];
        }];
        
    }else{
    
        [self.imageListArr addObjectsFromArray:photos];
        
        [self configImageViewlistAfterImageListUpdate];
    }
    
}

// 对于图片列表的重新布局 - 在图片数组更新之后
- (void)configImageViewlistAfterImageListUpdate{
    
    for (NSInteger i = 0; i < self.imageListArr.count; i++) {
        if (i < self.imageViewListArr.count) {
            self.imageViewListArr[i].image = self.imageListArr[i];
        }else{
            UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageview.image = self.imageListArr[i];
            imageview.userInteractionEnabled = YES;
            [self.scrollView addSubview:imageview];
            [self.imageViewListArr addObject:imageview];
        }
        
    }
    
    [self updateImageViewFromIndex:0 EndIndex:self.imageListArr.count - 1];
    if (self.imageViewListArr.count > self.imageListArr.count) {
        NSMutableArray *removeItemsArr = [NSMutableArray array];
        for (NSInteger i = self.imageListArr.count; i < self.imageViewListArr.count; i++) {
            [self.imageViewListArr[i] removeFromSuperview];
            [removeItemsArr addObject:self.imageViewListArr[i]];
        }
        [self.imageViewListArr removeObjectsInArray:removeItemsArr];
    }
    
    BOOL isallowContentSize = self.imageViewListArr.count * 70.0 > self.scrollView.size.width - 80.0;
    if (isallowContentSize) {
        CGFloat width =  self.imageViewListArr.count * 70.0 + 80.0;
        CGFloat height = self.scrollView.height;
        self.scrollView.contentSize = CGSizeMake(width, height);
    }else{
        self.scrollView.contentSize = CGSizeZero;
    }
    
}


- (void)updateImageViewFromIndex:(NSInteger)startIndex
                        EndIndex:(NSInteger)endIndex{
    for (NSInteger i = startIndex; i <= endIndex; i++) {
        if (i == 0) {
//            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(self.scrollView).offset(10.0);
//                make.top.equalTo(self.scrollView).offset(10.0);
//                make.size.mas_equalTo(CGSizeMake(self.scrollView.frame.size.height-20, self.scrollView.frame.size.height-20));
//            }];
            
            self.imageViewListArr[i].frame = CGRectMake(10, 10, self.scrollView.frame.size.height-20, self.scrollView.frame.size.height-20);
        }else{
//            UIImageView *lastItem = self.imageViewListArr[i - 1];
//            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
//                make.left.equalTo(lastItem.mas_right).offset(10.0);
//                make.top.equalTo(lastItem);
//                make.size.equalTo(lastItem);
//            }];
            self.imageViewListArr[i].frame = CGRectMake(10 + i*(self.scrollView.frame.size.height-20 + 10), 10, self.scrollView.frame.size.height-20, self.scrollView.frame.size.height-20);
        }
    }
    

    self.addButton = nil;
    
    CGRect frame = CGRectMake(self.imageViewListArr.count * (self.scrollView.frame.size.height-10) + 10.0, 15.0, self.scrollView.frame.size.height-30, self.scrollView.frame.size.height-30);
    
    self.addButton = [[UIButton alloc] initWithFrame:frame];
    [self.addButton setBackgroundImage:[UIImage imageNamed:@"加"] forState:UIControlStateNormal];
    [self.addButton addTarget:self action:@selector(btnclick) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.addButton];

    
}

- (NSMutableArray *)imageListArr{
    if (_imageListArr == nil) {
        _imageListArr = [NSMutableArray array];
    }
    return _imageListArr;
}

- (NSMutableArray<UIImageView *> *)imageViewListArr{
    if (_imageViewListArr == nil) {
        _imageViewListArr = [NSMutableArray array];
    }
    return _imageViewListArr;
}

- (TZImagePickerController *)p_ImagePickerVC{
    if (_p_ImagePickerVC == nil) {
        
        NSInteger maxCountInRow = [UIScreen mainScreen].bounds.size.width / 100.0;
        _p_ImagePickerVC = [[TZImagePickerController alloc] initWithMaxImagesCount:ICN_TopicImageAddMaxCount columnNumber:maxCountInRow delegate:self pushPhotoPickerVc:NO];
        _p_ImagePickerVC.alwaysEnableDoneBtn = NO;
        _p_ImagePickerVC.sortAscendingByModificationDate = NO;
        _p_ImagePickerVC.allowPickingVideo = NO;
        _p_ImagePickerVC.hideWhenCanNotSelect = YES;
    }
    return _p_ImagePickerVC;
}


- (UIViewController *)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

@end
