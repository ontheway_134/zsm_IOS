//
//  optionModel.h
//  ICan
//
//  Created by shilei on 17/3/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface optiondetialmodel : JSONModel

@property(nonatomic ,strong)NSString *industryName;
@property(nonatomic ,strong)NSString *qualificationName;
@property(nonatomic ,strong)NSString *workTypeName;
@property(nonatomic ,strong)NSString *workExperienceName;

@end


@interface optionModel : JSONModel

@property (nonatomic,assign)NSInteger code;
@property (nonatomic ,strong)NSString *info;
@property (nonatomic, strong)optiondetialmodel *result;

@end
