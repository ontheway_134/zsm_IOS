//
//  ICN_BusinessIndustryPickerView.m
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BusinessIndustryPickerView.h"
#import "ICN_BaseClassKindModel.h"

@interface ICN_BusinessIndustryPickerView ()<UIPickerViewDataSource , UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *businessIndustryPicker; // 行业选择器
@property (nonatomic , strong) ICN_BaseClassKindModel *selectedModel;

@end

@implementation ICN_BusinessIndustryPickerView

#pragma mark - ---------- 初始化方法 ----------

+ (instancetype)loadXibWithCurrentBound:(CGRect)bound{
    ICN_BusinessIndustryPickerView *view = XIB(ICN_BusinessIndustryPickerView);
    view.businessIndustryPicker.delegate = view;
    view.businessIndustryPicker.dataSource = view;
    view.bounds = bound;
    return view;
}

- (NSArray<ICN_BaseClassKindModel *> *)dataModelsArr{
    if (_dataModelsArr == nil) {
        _dataModelsArr = [NSArray array];
    }
    return _dataModelsArr;
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnLocationFouncBtnAction:(UIButton *)sender {
    
    if (sender.tag == ICN_PickerViewCancerBtn) {
        self.selectedModel = nil;
        self.hidden = YES;
        [self.delegate responseWithIndustryConfirm:NO SelectedModel:nil];
    }else{
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(responseWithIndustryConfirm:SelectedModel:)]) {
                if (self.selectedModel) {
                    [self.delegate responseWithIndustryConfirm:YES SelectedModel:self.selectedModel];
                    self.selectedModel = nil;
                    self.hidden = YES;
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"请选择行业"];
                }
            }
        }
    }
}

#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0) {
        if (self.dataModelsArr.count > 0) {
            // 设置默认选中第一行
            if (self.selectedModel == nil) {
                self.selectedModel = self.dataModelsArr.firstObject;
            }
            return self.dataModelsArr.count;
        }
    }
    
    return 0;
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH - 50.0;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        ICN_BaseClassKindModel *model = self.dataModelsArr[row];
        return model.className;
    }
    
    return nil;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // 添加选中行的字段内容
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        // 点击选中省一栏的内容
        self.selectedModel = self.dataModelsArr[row];
    }
    
}



@end
