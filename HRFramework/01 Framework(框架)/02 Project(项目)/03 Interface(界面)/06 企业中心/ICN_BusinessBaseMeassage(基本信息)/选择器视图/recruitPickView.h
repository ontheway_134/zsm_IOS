//
//  recruitPickView.h
//  ICan
//
//  Created by shilei on 17/3/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_BaseClassKindModel;

@protocol BusinessrecruitPickerViewDelegate <NSObject>

- (void)responseRecruitWithConfirm:(BOOL)isConfirm
              SelectedModel:(ICN_BaseClassKindModel *)model;

@end

@interface recruitPickView : UIView

@property (nonatomic , strong)NSArray <ICN_BaseClassKindModel *>*dataModelsArr; // 数据源

@property (nonatomic , weak)id<BusinessrecruitPickerViewDelegate> delegate;

+ (instancetype)loadXibWithCurrentBound:(CGRect)bound;



@end
