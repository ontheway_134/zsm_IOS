//
//  ICN_BaseClassKindModel.h
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_BaseClassKindModel : BaseOptionalModel

//id": "59",
//"className": "互联网/软件"
@property (nonatomic , copy)NSString *baseId; // id
@property (nonatomic , copy)NSString *className; // 类名

- (instancetype)initWithId:(NSString *)baseId
                 ClassName:(NSString *)name;

@end
