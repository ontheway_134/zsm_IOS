//
//  ICN_basicInformationModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/23.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_basicInformationModel : BaseOptionalModel
@property (strong,nonatomic)NSString * companyScale;
@property (strong,nonatomic)NSString * cityName;
@property (strong,nonatomic)NSString * companyNature;
@property (strong,nonatomic)NSString * companyDetail;
@property (strong,nonatomic)NSString * province;
@property (strong,nonatomic)NSString * companyIndustryName;
@property (strong,nonatomic)NSString * companyNatureName;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * city;
@property (strong,nonatomic)NSString * companyName;
@property (strong,nonatomic)NSString * provinceName;
@property (strong,nonatomic)NSString * companyLogo;
@property (strong,nonatomic)NSString * companyIndustry;
@property (strong,nonatomic)NSString * authStatus;
@end
