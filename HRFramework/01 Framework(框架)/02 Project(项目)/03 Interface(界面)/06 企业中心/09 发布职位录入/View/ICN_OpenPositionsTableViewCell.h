//
//  ICN_OpenPositionsTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ReturnAdressTapClick)();
typedef void (^ReturnIndustryTapClick)();
typedef void (^ReturnSchoolTapClick)();
typedef void (^ReturnWorkTapClick)();
typedef void (^ReturnNatureTapClick)();
@interface ICN_OpenPositionsTableViewCell : UITableViewCell

/*公司所在地*/
-(void)ReturnAdressTapClick:(ReturnAdressTapClick)block;
@property (strong,nonatomic)ReturnAdressTapClick returnAdressTapClick;
/*所属行业*/
-(void)returnIndustryTapClick:(ReturnIndustryTapClick)block;
@property (strong,nonatomic)ReturnIndustryTapClick returnIndustryTapClick;
/*学历要求*/
-(void)returnSchoolTapClick:(ReturnSchoolTapClick)block;
@property (strong,nonatomic)ReturnSchoolTapClick returnSchoolTapClick;
/*工作经验*/
-(void)returnWorkTapClick:(ReturnWorkTapClick)block;
@property (strong,nonatomic)ReturnWorkTapClick returnWorkTapClick;
/*工作性质*/
-(void)returnNatureTapClick:(ReturnNatureTapClick)block;
@property (strong,nonatomic)ReturnNatureTapClick returnNatureTapClick;
@end
