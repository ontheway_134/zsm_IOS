//
//  ICN_OpenPositionsTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_OpenPositionsTableViewCell.h"

@implementation ICN_OpenPositionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark 地址
- (IBAction)adressTap:(UITapGestureRecognizer *)sender {
    if (self.returnAdressTapClick) {
        self.returnAdressTapClick(sender);
    }
}
#pragma mark 所属行业
- (IBAction)IndustryTap:(UITapGestureRecognizer *)sender {
    if (self.returnIndustryTapClick) {
        self.returnIndustryTapClick(sender);
    }
}
#pragma mark 学历要求
- (IBAction)schoolTap:(UITapGestureRecognizer *)sender {
    if (self.returnSchoolTapClick) {
        self.returnSchoolTapClick(sender);
    }
}
#pragma mark 工作经验
- (IBAction)workTap:(UITapGestureRecognizer *)sender {
    if (self.returnWorkTapClick) {
        self.returnWorkTapClick(sender);
    }
}
#pragma mark 工作性质
- (IBAction)workXTap:(UITapGestureRecognizer *)sender {
    if (self.returnNatureTapClick) {
        self.returnNatureTapClick(sender);
    }
}
-(void)returnIndustryTapClick:(ReturnIndustryTapClick)block{
    self.returnIndustryTapClick = block;
}
-(void)returnWorkTapClick:(ReturnWorkTapClick)block{
    self.returnWorkTapClick = block;
}
-(void)returnNatureTapClick:(ReturnNatureTapClick)block{
    self.returnNatureTapClick = block;
}
-(void)returnSchoolTapClick:(ReturnSchoolTapClick)block{
    self.returnSchoolTapClick = block;
}
-(void)ReturnAdressTapClick:(ReturnAdressTapClick)block{
    self.returnAdressTapClick = block;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
