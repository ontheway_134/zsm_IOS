//
//  ICN_OpenPositionsViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_GetEnterPriseModel;

@interface ICN_OpenPositionsViewController : BaseViewController
@property (nonatomic , strong)ICN_GetEnterPriseModel *model;

@property (nonatomic , assign) BOOL notSetUserInfo;

@property (assign, nonatomic) BOOL isFirst;
@property (weak, nonatomic) IBOutlet UILabel *issueLabel;

@property (nonatomic ,strong)NSString *positionid;
@property (weak, nonatomic) IBOutlet UILabel *titlelabel;

@end
