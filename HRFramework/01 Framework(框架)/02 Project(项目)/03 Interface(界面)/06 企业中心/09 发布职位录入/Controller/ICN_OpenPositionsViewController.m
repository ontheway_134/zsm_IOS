//
//  ICN_OpenPositionsViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_OpenPositionsViewController.h"
#import "ICN_OpenPositionsTableViewCell.h"/*发布职位*/
#import "ICN_DynLocationPickerView.h"   // 城市选择器
#import "ICN_LocationModel.h"
#import "ICN_BaseClassKindModel.h"
#import "ICN_GetEnterPriseModel.h" // Model头文件
#import "HRNetworkingManager.h" // 网络请求头文件
#import "ICN_HeadImageSelView.h" // 编辑logo视图头文件
#import "STPhotoKitController.h" // 单张视图编辑所调用三方头文件
#import "UIImagePickerController+ST.h"
#import "HRNetworkingManager+ICN_Publication.h" // 发布页面的网络请求
#import "ICN_BusinessBaseMessageViewModel.h" // ViewModel
#pragma mark - ---------- 视图属性 ----------
#import "ICN_BusinessNaturePickerView.h"
#import "ICN_BusinessIndustryPickerView.h"
#import "ICN_DynLocationPickerView.h"
#import "ICN_BusinessNameTableViewCell.h"
#import "ICN_BaseBusinessMessagessTableViewCell.h"
#import "ICN_busiAddressTableViewCell.h"
#import "ICN_BriefTableViewCell.h"
#import "recruitPickView.h"   //招聘人数
#import "ICN_EducationView.h"   //工作经验
#import "ICN_NoImageTableCell.h"   //学历的cell
#import "ICN_JobExperienceView.h"   //工作经验的cell
#import "NSString+BlankString.h"
#pragma mark --- model属性头文件 ---
#import "ICN_SetUserInfoModel.h"   //model
#import "ICN_PositionDetialModel.h" // 用于获取企业发布职位详情的Model

#import "optionModel.h"   //企业信息的详情




//JobExperienceDelegate, PositionDesiredDelegate,  UITextFieldDelegate,DynLocationPickerViewDelegate,HangYeViewDelegate   birthdayDelegate,


#import "HRNetworkingManager+ICN_Publication.h"
@interface ICN_OpenPositionsViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,STPhotoKitDelegate,BusinessNaturePickerViewDelegate,BusinessIndustryPickerViewDelegate,DynLocationPickerViewDelegate,BusinessrecruitPickerViewDelegate,EducationDelegate,JobExperienceDelegate,UITextFieldDelegate>{
    
    
    UIView *grayview;
    
    NSString *tempBirthdayStr;
    NSString *educationHighest;
    
    NSString *hopehangye;
    NSString *hopehangyeID;
    
    NSString *jobCondition;
    NSString *experience;
    NSString *PositionDesired;
    NSString *LocalityStr;
    
    UIImage *tempImage;
    
    
}

@property (nonatomic , assign ,getter = isFirstLoad)BOOL firstLoad;


@property (weak, nonatomic) IBOutlet UITableView *OpenpositionTableView;

@property(nonatomic,strong)ICN_BriefTableViewCell *Briefcell;

@property (nonatomic , strong)ICN_BusinessNaturePickerView *naturePickerView;
@property (nonatomic , strong)ICN_BusinessIndustryPickerView *industryPickerView;
@property (nonatomic , strong)ICN_DynLocationPickerView *locationPickerView;
@property (nonatomic , strong)recruitPickView *recruiPickView;   //招聘人数
@property (nonatomic , strong)UIView *pickerBacground; // 用于显示pickerView的背景板
@property (nonatomic , strong)NSMutableDictionary *updateContentDic; // 更新修改的内容字典
@property (nonatomic , strong)ICN_BusinessBaseMessageViewModel *viewModel;
@property (nonatomic , strong)NSMutableArray *locationIdArr; // 选中的城市的Id数组（省id，市id）

@property (nonatomic , strong)ICN_EducationView *education;  //工作经验

@property (nonatomic, strong) NSMutableDictionary *dict;

@property (nonatomic, strong)ICN_JobExperienceView *jobexperiencr;

@property (nonatomic, strong) NSMutableArray *dataArr;

@property(nonatomic ,strong)ICN_BusinessNameTableViewCell *nickcell;  //项目名称

@property(nonatomic,strong)ICN_BusinessNameTableViewCell *peoplecell;   //人数

@property(nonatomic ,strong)ICN_BusinessNameTableViewCell *sarlycell;   //薪资

@property(nonatomic ,strong)NSString *educationId;    //学历的id
@property(nonatomic ,strong)NSString *experienceId;    //经验的id

@property (nonatomic , strong)ICN_PositionDetialModel *positionDetialModel; // 发布职位的详情Model



@end

@implementation ICN_OpenPositionsViewController
#pragma mark - ---------- 懒加载 ----------
- (NSMutableArray *)locationIdArr{
    if (_locationIdArr == nil) {
        _locationIdArr = [NSMutableArray array];
    }
    return _locationIdArr;
}
- (ICN_BusinessBaseMessageViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_BusinessBaseMessageViewModel alloc] init];
    }
    return _viewModel;
}
- (NSMutableDictionary *)updateContentDic{
    if (_updateContentDic == nil) {
        _updateContentDic = [NSMutableDictionary dictionary];
    }
    return _updateContentDic;
}
#pragma mark 地址
- (ICN_DynLocationPickerView *)locationPickerView{
    if (_locationPickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _locationPickerView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _locationPickerView.locationsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)];
        _locationPickerView.frame = frame;
        _locationPickerView.delegate = self;
        [self.pickerBacground addSubview:_locationPickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _locationPickerView;
    
}
#pragma mark 企业
- (ICN_BusinessNaturePickerView *)naturePickerView{
    if (_naturePickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyNATURE)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _naturePickerView = [ICN_BusinessNaturePickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _naturePickerView.dataModelsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyNATURE)];
        _naturePickerView.frame = frame;
        _naturePickerView.delegate = self;
        [self.pickerBacground addSubview:_naturePickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyNATURE)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _naturePickerView;
}

- (ICN_BusinessIndustryPickerView *)industryPickerView{
    if (_industryPickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _industryPickerView = [ICN_BusinessIndustryPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _industryPickerView.dataModelsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)];
        _industryPickerView.frame = frame;
        _industryPickerView.delegate = self;
        [self.pickerBacground addSubview:_industryPickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _industryPickerView;
}


- (UIView *)pickerBacground{
    if (_pickerBacground == nil) {
        _pickerBacground = [[UIView alloc] initWithFrame:SCREEN_BOUNDS];
        _pickerBacground.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
        UITapGestureRecognizer *backGroundHideTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBackGroundViewAction)];
        [_pickerBacground addGestureRecognizer:backGroundHideTap];
        [[UIApplication sharedApplication].keyWindow addSubview:_pickerBacground];
    }
    return _pickerBacground;
}

#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (self.positionid) {
        [self configData];
        self.titlelabel.text = @"修改职位";
    }else{
        self.titlelabel.text = @"发布职位";
    }
    
    
}
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_pickerBacground) {
        [_pickerBacground removeFromSuperview];
    }
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.firstLoad = YES;
    
    [self.viewModel requestWithBusinessLinkNetWork];
    [self.navigationController setNavigationBarHidden:YES];
    
    // 注册基本用户信息的Cell
    [self.OpenpositionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_BusinessNameTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_BusinessNameTableViewCell class])];
    // 注册用户所在地址的Cell
    [self.OpenpositionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_busiAddressTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_busiAddressTableViewCell class])];
    
    
    // 注册公司简介的Cell
    [self.OpenpositionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_BriefTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_BriefTableViewCell class])];
    //工作经验的cell
    [self.OpenpositionTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NoImageTableCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NoImageTableCell class])];
    self.OpenpositionTableView.delegate=self;
    self.OpenpositionTableView.dataSource=self;
    self.nickcell.contentTextField.delegate = self;
    //    [self.OpenpositionTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}
#pragma mark - ---------- IBActions ----------
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
- (void)hideBackGroundViewAction{
    self.locationPickerView.hidden = YES;
    self.naturePickerView.hidden = YES;
    self.industryPickerView.hidden = YES;
    self.pickerBacground.hidden = YES;
    self.recruiPickView.hidden = YES;
}
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            //职位名称
            self.nickcell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessNameTableViewCell"];
            self.nickcell.contentTextField.delegate = self;
            self.nickcell.TitleStr = @"职位名称";
            self.nickcell.keyType = ICN_BusinessCompanyNAME;
            // 两种情况 - 存在DetialModel为 修改简历 - 一应操作在DetialModel中进行
            if (self.positionDetialModel == nil) {
                NSString *contentStr = [self.updateContentDic valueForKey:SF(@"%ld",(long)ICN_BusinessCompanyNAME)];
                if (contentStr == nil) {
                    contentStr = self.model.result.companyName;
                }
                
                self.nickcell.contentStr = contentStr;
            }else{
                self.nickcell.TitleStr = self.positionDetialModel.positionTitle;
            }
            [self.nickcell callBackWithTextComplitedBlock:^(NSString *textKey, NSString *contentStr) {
                // 使用positionid 有则是修改简历
                [self.updateContentDic setValue:contentStr forKey:textKey];
                if (self.positionid) {
                    self.positionDetialModel.positionTitle = SF(@"%@",contentStr);
                }
            }];
            return self.nickcell;
            
        }
            
            break;
        case 1:{
            //公司所在地
            //warn === 未找到修改城市的编码位置需要后期找到将修改后的值传给详情Model
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyAREA;
            if (self.positionDetialModel == nil) {
                // 发布职位
                if ([self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)]) {
                    cell.content = [[self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)] componentsJoinedByString:@" "];
                } else if (cell.content.length == 0) {
                    if (self.model.result.provinceName == nil || self.model.result.cityName == nil) {
                        cell.content = @"";
                    } else {
                        cell.content = SF(@"%@ %@",self.model.result.provinceName, self.model.result.cityName);
                    }
                }
            }else{
                // 修改职位
                if (self.positionDetialModel.cityName) {
                    cell.content = SF(@"%@ %@",self.positionDetialModel.provinceName , self.positionDetialModel.cityName);
                }
            }
            return cell;
            
        }
            break;
        case 2:{
            
            //所属行业
            //warn=== 需要在点击行业标签的回调中 为detialModel赋值
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyINDUSTRY;
            if (self.positionDetialModel == nil) {
                // 是发布职位
                if ([self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)]) {
                    cell.content = [self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)];
                }else{
                    cell.content = @"";
                }
                if (cell.content.length == 0) {
                    cell.content = self.model.result.companyIndustryName;
                }
            }else{
                // 是编辑职位
                if (self.positionDetialModel.industryName) {
                    cell.content = self.positionDetialModel.industryName;
                }
            }
            return cell;

        }
            break;
        case 3:{
            
            //学历要求
            ICN_NoImageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell"
                                                                         forIndexPath:indexPath];
            cell.leftLabel.font = FONT(13);
            cell.leftLabel.textColor = [UIColor blackColor];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.leftLabel.text = @"学历要求";
            ICN_SetUserInfoModel *model = self.dataArr.firstObject;
            if (self.positionDetialModel == nil) {
                // 发布职位
                cell.model = model;
                cell.rightLabel.text = educationHighest.length == 0 ? model.memberQualificationName : educationHighest;
            }else{
                // 编辑职位
                if (self.positionDetialModel.qualification) {
                    cell.rightLabel.text = self.positionDetialModel.qualificationName;
                }
            }
            return cell;
            
            
        }
            break;
        case 4:{
            
            //工作经验
            ICN_NoImageTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell"
                                                                         forIndexPath:indexPath];
            cell.leftLabel.text = @"工作经验";
            cell.leftLabel.font = FONT(13);
            cell.leftLabel.textColor = [UIColor blackColor];
            ICN_SetUserInfoModel *model = self.dataArr.firstObject;
            cell.model = model;
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (self.positionDetialModel == nil) {
                // 发布职位
                cell.rightLabel.text = experience == 0 ? model.workExperienceName : experience;
            }else{
                // 编辑职位
                if (self.positionDetialModel.workExperienceName) {
                    cell.rightLabel.text = self.positionDetialModel.workExperienceName;
                }
            }
            return cell;
            
        }
            break;
        case 5:{
            
            //招聘人数
            self.peoplecell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessNameTableViewCell"];
            self.peoplecell.TitleStr = @"招聘人数";
            if (self.positionDetialModel) {
                // 编辑模式 需要设置之前的招聘人数
                if (self.positionDetialModel.personCount) {
                    self.peoplecell.contentStr = self.positionDetialModel.personCount;
                }
            }
            return self.peoplecell;
        }
            break;
        case 6:{
            //工作性质    正确无需修改
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyNATURE;
            if ([self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyNATURE)]) {
                cell.content = [self.updateContentDic valueForKey:SF(@"%d",ICN_BusinessCompanyNATURE)];
            }else{
                cell.content = @"";
            }
            if (cell.content.length == 0) {
                cell.content = self.model.result.companyNatureName;
            }
            return cell;
        }
            break;
        case 7:{
            //薪质   公司名称
            self.sarlycell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessNameTableViewCell"];
            self.sarlycell.TitleStr = @"薪资";
            self.sarlycell.kLabel.hidden = NO;
            
            return self.sarlycell;
            
        }
            break;
        case 8:{
            //职位描述    正确无需修改
            self.Briefcell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BriefTableViewCell"];
            if (self.Briefcell.BriefTextView.text.length == 0) {
                self.Briefcell.BriefTextView.text = self.model.result.companyDetail;
                self.Briefcell.CountLabel.text = SF(@"%d/500",self.Briefcell.BriefTextView.text.length);
            }
            
            self.Briefcell.titleLabel.text = @"职位描述";
            self.Briefcell.BriefTextView.delegate=self;
            
            return self.Briefcell;
        }
            break;
        default:
            break;
    }
    
    return nil;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            return 45;
            break;
            
            
        case 8:
            return 110;
            break;
            
    }
    return 0;
}
#pragma mark - ---------- UITableViewDataSource ---------


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            NSLog(@"公司的名称");
            break;
        }
            
        case 1:{
            
            // 弹出所属地区(正确)
            self.pickerBacground.hidden = NO;
            self.locationPickerView.hidden = NO;
            [self.view endEditing:YES];
            break;
        }
        case 2:{
            
            // 弹出所属行业（正确）
            self.pickerBacground.hidden = NO;
            self.industryPickerView.hidden = NO;
            [self.view endEditing:YES];
            
            break;
        }
        case 3:{
            // 学历要求（改）
            [self setUserEducationsss];
            [self.view endEditing:YES];
            
            break;
        }
        case 4:{
            // 工作经验（改）
            [self setUserJobExperiencesss];
            [self.view endEditing:YES];
            break;
        }
        case 5:{
            // 招聘人数（正确）
            
            break;
        }
            
        case 6:{
            // 企业性质（正确）
            self.pickerBacground.hidden = NO;
            self.naturePickerView.hidden = NO;
            break;
        }
            
            
        default:
            break;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 9;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=500)
    {
        return  NO;
    }
    else
    {
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    //该判断用于联想输入
    if (textView.text.length >= 500)
    {
        textView.text = [textView.text substringToIndex:500];
        
    }
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.Briefcell.CountLabel.text=[NSString stringWithFormat:@"%ld/500",existTextNum];
}


#pragma mark - ---------- pickerView代理 ----------

#pragma mark --- BusinessNaturePickerViewDelegate,BusinessIndustryPickerViewDelegate,DynLocationPickerViewDelegate ---

- (void)responseWithConfirm:(BOOL)isConfirm SelectedModel:(ICN_BaseClassKindModel *)model{
    if (isConfirm) {
        [self.updateContentDic setValue:model.className forKey:SF(@"%d",ICN_BusinessCompanyNATURE)];
        [self.updateContentDic setValue:model.baseId forKey:SF(@"%d", ICN_BusinessCompanyNATUREID)];
        [self.OpenpositionTableView reloadData];
    }
    self.pickerBacground.hidden = YES;
}

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (code > 0) {
        // 获取到数据了
        NSMutableArray *array = [NSMutableArray array];
        for (ICN_LocationModel *model in [self.viewModel.pickerModelsDic valueForKey:SF(@"%d",ICN_BusinessCompanyAREA)]) {
            for (ICN_CityDetialModel *detialModel in model.list) {
                if (detialModel.cityId.integerValue == code) {
                    [array addObject:model.className];
                    [array addObject:detialModel.className];
                    [self.locationIdArr removeAllObjects];
                    [self.locationIdArr addObject:model.provinceId];
                    [self.locationIdArr addObject:detialModel.cityId];
                }
            }
        }
        if (array.count == 2) {
            [self.updateContentDic setObject:array forKey:SF(@"%d",ICN_BusinessCompanyAREA)];
            [self.OpenpositionTableView reloadData];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未选中城市"];
        }
    }
    self.pickerBacground.hidden = YES;
}

- (void)responseWithIndustryConfirm:(BOOL)isConfirm SelectedModel:(ICN_BaseClassKindModel *)model{
    if (isConfirm) {
        [self.updateContentDic setValue:model.className forKey:SF(@"%d",ICN_BusinessCompanyINDUSTRY)];
        [self.updateContentDic setValue:model.baseId forKey:SF(@"%d",ICN_BusinessCompanyINDUSTRYID)];
        [self.OpenpositionTableView reloadData];
    }
    self.pickerBacground.hidden = YES;
}



#pragma mark - ---------- 学历选择 ----------
- (void)setUserEducationsss{
    
    [self setGrayViewsss];
    self.education = XIB(ICN_EducationView);
    self.education.delegate = self;
    self.education.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.education.cancelButton addAction:^(NSInteger tag) {
        [grayview removeFromSuperview];
        
    }];
    
    [self.education.confirmButton addTarget:self action:@selector(educationChoosess) forControlEvents:UIControlEventTouchUpInside];
    [grayview addSubview:self.education];
    
}
- (void)setGrayViewsss {
    self.pickerBacground.hidden = YES;
    grayview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayview.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [self.view addSubview:grayview];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayview addGestureRecognizer:tapGesture];
}


- (void)dismissContactView {
    
    
    [self.education removeFromSuperview];
    [grayview removeFromSuperview];
    self.pickerBacground.hidden = YES;
}
- (void)educationChoosess {
    
    if (educationHighest == NULL) {
        educationHighest = @"小学";
        [self.dict setObject:@"1" forKey:@"memberQualification"];
    }
    [self.OpenpositionTableView reloadData];
    
    [self.education removeFromSuperview];
    [grayview removeFromSuperview];
    
    
}


- (void)getEducation:(NSString *)education andID:(NSString *)ID temp:(NSString *)strID{
    
    self.educationId = strID;
    
    if (self.pickerBacground) {
        self.pickerBacground.hidden = YES;
    }
    
    educationHighest = [NSString stringWithFormat:@"%@",education];
    
    if (educationHighest == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择学历"];
    }else {
        [self.dict setObject:ID forKey:@"memberQualification"];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---------- 工作经验 ----------
- (void)setUserJobExperiencesss{
    [self setGrayViewsss];
    self.jobexperiencr = XIB(ICN_JobExperienceView);
    self.jobexperiencr.delegate = self;
    self.jobexperiencr.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.jobexperiencr.cancelButton addAction:^(NSInteger tag) {
        [grayview removeFromSuperview];
    }];
    [grayview addSubview:self.jobexperiencr];
    [self.jobexperiencr.confirmButton addTarget:self action:@selector(JobExperienceChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)JobExperienceChoose {
    
    if (experience == NULL) {
        experience = @"在读";
    }
    
    [self.OpenpositionTableView reloadData];
    [grayview removeFromSuperview];
}

- (void)getJobExperiencen:(NSString *)JobExperience andID:(NSString *)ID{
    
    self.experienceId = ID;
    
    if (experience == NULL) {
        experience = @"在读";
    }
    
    experience = [NSString stringWithFormat:@"%@",JobExperience];
    
    [self.dict setObject:ID forKey:@"workExperience"];
    
}



//发布职位的点击事件
- (IBAction)issueBtnActons:(UIButton *)sender {
    
    if ([self.nickcell.contentTextField.text isBlankString]||[self.nickcell.contentTextField.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入职位名称"];
        return;
    }
    //项目简介为空
    if ([self.Briefcell.BriefTextView.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入项目描述"];
        return;
    }
    
    if ([self.nickcell.contentStr isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入项目名称"];
        return;
    }
    if ([self.peoplecell.contentStr isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入招聘人数"];
        return;
    }
    if ([self.sarlycell.contentStr isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入薪资"];
        return;
    }
    
    
    
    
    //设置当前的token值
    NSMutableDictionary *params ;
    NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    
    if (token) {
        params = [@{@"token" : token} mutableCopy];
    } else {
        return ;
    }
    
    //职位详情
    if (![self.Briefcell.BriefTextView.text isEqualToString:@""])
    {
        [params setValue:self.Briefcell.BriefTextView.text forKey:@"summary"];
        
    }
    
    
    // 根据更新字典中的字段名称添加内容
    for (NSString *typeStr in self.updateContentDic) {
        
        NSInteger keyType = typeStr.integerValue;
        switch (keyType) {
                
            case ICN_BusinessCompanyNAME:{
                // 更新的是企业名称
                [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"positionTitle"];
                
                break;
            }
                
            case ICN_BusinessCompanyINDUSTRY:{
                
                //所属行业
                [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"IndustryId"];
                
            }
                
                
            case ICN_BusinessCompanyAREA:{
                // 更新的是企业地址 -- 需要添加所在省份和所在城市两个内容'province'：所在省份 'city'：所在城市
                [params setValue:_locationIdArr[0] forKey:@"province"];
                [params setValue:_locationIdArr[1] forKey:@"city"];
                break;
            }
                
            case ICN_BusinessCompanyNATURE:{
            
                //企业的性质
                [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"workType"];
                break;
            }
            

            default:
                break;
        }
    }
//    [params setValue:self.sarlycell.contentTextField.text forKey:@"minSalary"];
//    
//    [params setValue:self.sarlycell.contentTextField.text forKey:@"minSalary"];
    /*将字符串乘以 1000*/
    NSInteger k = [self.sarlycell.contentTextField.text integerValue]*1000;
    NSString * STR = [NSString stringWithFormat:@"%ld",k];
    [params setValue:STR  forKey:@"minSalary"];
    
    [params setValue:self.educationId forKey:@"qualification"];
    [params setValue:self.experienceId forKey:@"workExperience"];
    [params setValue:self.peoplecell.contentTextField.text forKey:@"personCount"];
    
    [params setValue:self.nickcell.contentTextField.text forKey:@"positionTitle"];
    [params setValue:@"" forKey:@"middleId"];
    [params setValue:@"" forKey:@"positionId"];
    [params setValue:@"" forKey:@"area"];
    
    [params setValue:@"" forKey:@"workType"];
    
    NSLog(@"%@",params);
    
    
    //发布职位的接口，
    [[[HRRequestManager alloc]init] POST_URL:PATH_ISSUEDUTY params:params success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [self.navigationController popViewControllerAnimated:YES];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"发布成功"];
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
}

#pragma mark - --- 网络请求 ---

// 用户获取本页的职位信息的网络请求
- (void)configData{
    
    
    if (self.positionid) {
        NSDictionary *dic = @{@"id":self.positionid,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
        [[HRRequest alloc] POST_PATH:PATH_PosicationDetial params:dic success:^(id result) {
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                // 获取到正确的数据并进行下一步操作
                NSDictionary *resultDic = [result valueForKey:@"result"];
                self.positionDetialModel = [[ICN_PositionDetialModel alloc]  initWithDictionary:resultDic error:nil];
            }else{
                // 数据请求异常直接返回上级页面
                [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:baseModel.info];
                [self.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(NSDictionary *errorInfo) {
            // 数据获取失败之后直接返回上级界面
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"网络请求失败"];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
}


@end
