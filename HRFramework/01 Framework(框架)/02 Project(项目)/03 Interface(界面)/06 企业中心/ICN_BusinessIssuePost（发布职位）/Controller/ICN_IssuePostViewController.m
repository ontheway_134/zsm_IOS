//
//  ICN_IssuePostViewController.m
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IssuePostViewController.h"
#import "ICN_IsuePostTableViewCell.h"
#import "ICN_PositionListModel.h"
#import "ICN_PositionNextOneViewController.h"
#import "ICN_OpenPositionsViewController.h"

@interface ICN_IssuePostViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *IssuePostTableView;
@property (nonatomic, copy) ICN_PositionListModel *model;
@property (nonatomic, strong) NSMutableArray *modelArr;
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/

@end

@implementation ICN_IssuePostViewController


#pragma mark - ---------- 懒加载 ----------
- (NSMutableArray *)modelArr{
    if (!_modelArr) {
        _modelArr = [NSMutableArray array];
    }
    return _modelArr;
}
#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.IssuePostTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self CollectnetWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self CollectnetWorkRequest:TYPE_RELOADDATA_UP];
    }];
    self.IssuePostTableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.IssuePostTableView.mj_footer.hidden = YES;
    
    [self.IssuePostTableView.mj_header beginRefreshing];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.IssuePostTableView.mj_header beginRefreshing];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark - --- IBActions ---
- (IBAction)backActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 发布按钮
- (IBAction)ReleaseBtnClick:(UIButton *)sender {
    
    //发布职位
    ICN_OpenPositionsViewController * vc = [[ICN_OpenPositionsViewController alloc]init];
    
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark 我职位删除
- (void)applyDeleteActive:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    [[HRRequest manager]POST:PATH_doDeletePosition para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.IssuePostTableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
    }];
}

#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark 我的收藏职位网络请求
- (void)CollectnetWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        [[HRRequest manager]POST:PATH_POSITIONLIST para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_PositionListModel * model=[[ICN_PositionListModel alloc]initWithDictionary:obj error:nil];
                [self.modelArr addObject:model];
            }];
            if (self.modelArr.count == 0) {
                [self.IssuePostTableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.modelArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [[HRRequest manager]POST:PATH_POSITIONLIST para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_PositionListModel * model=[[ICN_PositionListModel alloc]initWithDictionary:obj error:nil];
                [self.modelArr addObject:model];
            }];
            [self endRefresh];
            [self.IssuePostTableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            [self endRefresh];
            [self.IssuePostTableView reloadData];
        }];
    }
}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.IssuePostTableView.mj_footer endRefreshing];
    [self.IssuePostTableView.mj_header endRefreshing];
}
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.IssuePostTableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.IssuePostTableView.mj_footer.hidden) {
                self.IssuePostTableView.mj_footer.hidden = NO;
            }
        }
    }
}

#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.IssuePostTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_IsuePostTableViewCell * cell = XIB(ICN_IsuePostTableViewCell);
    cell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /*赋值*/
    cell.model = self.modelArr[indexPath.row] ;
    
    return  cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    ICN_PositionListModel * model = self.modelArr[indexPath.row];
    [self.modelArr removeObjectAtIndex:indexPath.row];
    [self applyDeleteActive:model.PositionId];
}
-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath {
    return @"删除";
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //修改职位
    //warn=== test 修改职位功能暂时屏蔽
    ICN_PositionNextOneViewController * vc = [[ICN_PositionNextOneViewController alloc]init];
    
    vc.url = [self.modelArr[indexPath.row] PositionId];
    [self.navigationController pushViewController:vc animated:YES];

}


@end
