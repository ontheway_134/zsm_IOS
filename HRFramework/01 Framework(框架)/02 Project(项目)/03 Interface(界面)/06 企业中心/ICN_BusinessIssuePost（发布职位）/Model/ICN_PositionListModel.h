//
//  ICN_PositionListModel.h
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface ICN_PositionListModel : BaseOptionalModel

@property (copy, nonatomic) NSString *PositionId;

@property (copy, nonatomic) NSString *companyLogo;
@property (copy, nonatomic) NSString *companyName;

@property (copy, nonatomic) NSString *positionTitle;
@property (copy, nonatomic) NSString *cityName;
@property (copy, nonatomic) NSString *qualification;
@property (copy, nonatomic) NSString *qualificationName;

@property (copy, nonatomic) NSString *minSalary;
@property (copy, nonatomic) NSString *maxSalary;

@end
