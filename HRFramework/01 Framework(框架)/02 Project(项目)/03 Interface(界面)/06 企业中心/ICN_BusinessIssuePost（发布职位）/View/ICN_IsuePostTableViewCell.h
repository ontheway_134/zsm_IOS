//
//  ICN_IsuePostTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_PositionListModel;

@interface ICN_IsuePostTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@property (weak, nonatomic) IBOutlet UILabel *zhiwei;

@property (weak, nonatomic) IBOutlet UILabel *gongsi;
@property (weak, nonatomic) IBOutlet UILabel *didian;
@property (weak, nonatomic) IBOutlet UILabel *xueli;
@property (weak, nonatomic) IBOutlet UILabel *gongzi;

@property (strong,nonatomic)ICN_PositionListModel * model;
@end
