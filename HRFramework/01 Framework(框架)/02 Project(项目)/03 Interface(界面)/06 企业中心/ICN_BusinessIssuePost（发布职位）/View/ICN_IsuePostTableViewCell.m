//
//  ICN_IsuePostTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IsuePostTableViewCell.h"
#import "ICN_PositionListModel.h"

@implementation ICN_IsuePostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setModel:(ICN_PositionListModel *)model {

    NSURL *url = [NSURL URLWithString:SF(@"http://1ican.com%@",model.companyLogo)];
    
    [_headImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width*0.5;
    self.headImageView.layer.masksToBounds = YES;
    
    _zhiwei.text = model.positionTitle;
    _gongsi.text = model.companyName;
    _didian.text = model.cityName;
    _xueli.text = model.qualificationName;
    _gongzi.text = SF(@"%@",model.minSalary);
}
@end
