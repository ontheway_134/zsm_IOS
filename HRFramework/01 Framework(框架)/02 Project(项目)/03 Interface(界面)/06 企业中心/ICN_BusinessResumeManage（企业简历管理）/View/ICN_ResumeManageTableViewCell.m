//
//  ICN_ResumeManageTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ResumeManageTableViewCell.h"
#import "ICN_BiographicalNotesManageModel.h"

@implementation ICN_ResumeManageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_BiographicalNotesManageModel *)model {
    
    NSURL *url = [NSURL URLWithString:SF(@"http://1ican.com%@",model.memberLogo)];
    
    [_headImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"头像"]];
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width*0.5;
    self.headImageView.layer.masksToBounds = YES;
    _nameAndPosition.text = SF(@"%@    %@",model.memberNick ,model.positionTitle );
    _cityAndExperienceAndQualification.text = SF(@"%@ ｜ %@ ｜ %@",model.cityName ,model.workExperience, model.memberQualification);
    _mobile.text = SF(@"手机号 ：%@",model.memberMobile );
}

@end
