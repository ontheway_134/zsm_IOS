//
//  ICN_IndusTryView.m
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndusTryView.h"
#import "ICN_PosicationListModel.h"
#import "ICN_SLIndusTryTableViewCell.h"

@interface ICN_IndusTryView()<UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *firstStyleIndustryTableview;

@property (weak, nonatomic) IBOutlet UITableView *secondStyleIndustryTableView;

@property (nonatomic , strong)ICN_PosicationListModel *selectedModel; // 选中的一级列表Model



@end


@implementation ICN_IndusTryView

@synthesize modelsArr = _modelsArr; // 用于同时重写set get方法时候的属性名标识

- (void)setModelsArr:(NSMutableArray<ICN_PosicationListModel *> *)modelsArr{
    _modelsArr = modelsArr;
    if (_modelsArr.firstObject.code == 0) {
        // 数据返回成功
        [self.firstStyleIndustryTableview reloadData];
    }else{
        self.secondStyleIndustryTableView.hidden = YES;
        [MBProgressHUD ShowProgressWithBaseView:self Message:@"尚未获取到数据请稍后重试"];
    }
}

- (NSMutableArray<ICN_PosicationListModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (void)configTableViewWihleViewWillLoad{
    self.firstStyleIndustryTableview.delegate = self;
    self.firstStyleIndustryTableview.dataSource = self;
    self.secondStyleIndustryTableView.delegate = self;
    self.secondStyleIndustryTableView.dataSource = self;
    self.firstStyleIndustryTableview.rowHeight = 40.0;
    self.secondStyleIndustryTableView.rowHeight = 40.0;
    self.firstStyleIndustryTableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.secondStyleIndustryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // tableView注册Cell
    UINib *cellNib = [UINib nibWithNibName:NSStringFromClass([ICN_SLIndusTryTableViewCell class]) bundle:nil];
    
    [self.firstStyleIndustryTableview registerNib:cellNib forCellReuseIdentifier:NSStringFromClass([ICN_SLIndusTryTableViewCell class])];
    [self.secondStyleIndustryTableView registerNib:cellNib forCellReuseIdentifier:NSStringFromClass([ICN_SLIndusTryTableViewCell class])];
    
    self.secondStyleIndustryTableView.hidden = YES;

}

- (void)callBackWithCellSelectedBlock:(CellSelectedBlock)block{
    self.block = block;
}


#pragma mark - --- IBActions ---

#pragma mark - ---------- 代理 ----------

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    if ([tableView isEqual:self.secondStyleIndustryTableView]) {
        NSString *content = [self.selectedModel.list[indexPath.row] posicationDetialListId];
        if (self.block) {
            self.block(content);
        }
        self.hidden = YES;
    }else{
        if (self.selectedModel) {
            self.selectedModel.selectedCell = @"0";
        }
        self.selectedModel = self.modelsArr[indexPath.row];
        self.selectedModel.selectedCell = @"1";
        self.secondStyleIndustryTableView.hidden = NO;
        [self.firstStyleIndustryTableview reloadData];
        [self.secondStyleIndustryTableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([tableView isEqual:self.firstStyleIndustryTableview]) {
        return self.modelsArr.count;
    }
    if ([tableView isEqual:self.secondStyleIndustryTableView]) {
        if (self.selectedModel) {
            if (self.selectedModel.list != nil) {
                return self.selectedModel.list.count;
            }
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_SLIndusTryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_SLIndusTryTableViewCell class])];
    if ([tableView isEqual:self.firstStyleIndustryTableview]) {
        
        ICN_PosicationListModel *model = self.modelsArr[indexPath.row];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        if (model.selectedCell != nil && model.selectedCell.integerValue == 1) {
            // 需要改变字体的Cell
            cell.selectedColor = YES;
        }else{
            cell.selectedColor = NO;
        }
        if (self.modelsArr != nil) {
            cell.titleStr = [model className];
        }else{
            cell.titleStr = @"测试数据";
        }
        return cell;
    }else{
        // 是二级分栏 -- 添加尖角
        cell.backgroundColor = RGB0X(0xf0f0f0);
        NSInteger selectedIndex = [self.modelsArr indexOfObject:self.selectedModel];
        if (selectedIndex == indexPath.row) {
            // 添加尖角
            cell.showTriangle = YES;
        }else{
            cell.showTriangle = NO;
        }
        cell.selectedColor = NO;
        cell.titleStr = [self.selectedModel.list[indexPath.row] className];
        return cell;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}




@end
