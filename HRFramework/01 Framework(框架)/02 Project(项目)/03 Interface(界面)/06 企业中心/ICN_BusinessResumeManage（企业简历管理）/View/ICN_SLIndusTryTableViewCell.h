//
//  ICN_IndusTryTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_SLIndusTryTableViewCell : UITableViewCell

@property (nonatomic , copy)NSString *titleStr;
@property (nonatomic , assign , getter=isSelectedColor)BOOL selectedColor; // 是否是选中的颜色

@property (nonatomic , assign , getter=isShowCell)BOOL showTriangle; // 是否是展示三角的Cell

@end
