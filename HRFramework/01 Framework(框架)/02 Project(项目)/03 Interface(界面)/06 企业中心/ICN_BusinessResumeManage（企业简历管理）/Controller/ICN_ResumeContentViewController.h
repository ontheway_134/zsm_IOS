//
//  ICN_ResumeContentViewController.h
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_ResumeContentViewController : BaseViewController

@property (copy, nonatomic) NSString *memberId;
@property (copy, nonatomic) NSString *resumeId;
@property (copy, nonatomic) NSString *BiographicalNotesManageId;
@property (weak, nonatomic) IBOutlet UIButton *collectBtn;

@end
