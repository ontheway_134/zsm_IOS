//
//  ICN_ResumeContentViewController.m
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//
#define kApplicationShared  [UIApplication sharedApplication]
#import "ICN_ResumeContentViewController.h"
#import "ICN_Basic informationTableViewCell.h"
#import "ICN_LSkillTagTableViewCell.h"
#import "ICN_ResumeWorkExperienceTableViewCell.h"
#import "ICN_ResumeEducationExperienceTableViewCell.h"
#import "ICN_ResumeTrainTableViewCell.h"
#import "ICN_MyResumeSection.h"

#import "ICN_MyResumeWorkExModel.h"
#import "ICN_MyResumeEducationExperienceModel.h"
#import "ICN_MyResumeTrainExperienceModel.h"
#import "ICN_MyResumeBasicInformModel.h"
#import "interPopView.h"/*发送面试邀请*/
#import "NSString+BlankString.h"
#import "SQJudgeInfomation.h"
@interface ICN_ResumeContentViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrOfWork;
@property (nonatomic, strong) NSMutableArray *arrOfEducation;
@property (nonatomic, strong) NSMutableArray *arrOfTrain;

@property (weak, nonatomic) IBOutlet UIButton *xingxingButton;
@property (copy, nonatomic) ICN_MyResumeBasicInformModel *baseInfoModel;
@property (strong,nonatomic)interPopView * popView;
@property (copy, nonatomic) NSString *isCollectStr;
@property (strong,nonatomic)NSString * phoneStr;
@property (strong,nonatomic)NSString * positionTitle;
@end

@implementation ICN_ResumeContentViewController
- (interPopView *)popView{
    if (!_popView) {
        _popView = XIB(interPopView);
        _popView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    return _popView;
}
- (IBAction)shoucangClick:(id)sender {
    if (_xingxingButton.selected == NO) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        dic[@"resumeId"] = self.resumeId;
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DOADDCOLLECTION params:dic success:^(id result) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            [self.collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"收藏失败");
        }];
    } else {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        dic[@"resumeId"] = self.resumeId;
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DODELETECOLLECTION params:dic success:^(id result) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            [self.collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"取消收藏失败");
        }];
    }
    _xingxingButton.selected = !_xingxingButton.selected;
}

- (IBAction)mianshiyaoqingClick:(id)sender {
    /*2017 4 11 新加 这里要弹窗提示*/
     [kApplicationShared.keyWindow addSubview:self.popView];

    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"id"] = self.BiographicalNotesManageId;
    dic[@"fitted"] = @"1";
    
    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DoFittedResume params:dic success:^(id result) {
        NSString *tempStr = SF(@"%@",result[@"info"]);
        if (tempStr.length != 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
        } else {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"标记成功"];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"标记失败");
    }];

}


#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self popView];
    
    
    [self userBaseInfo];
    [self configDataWork];
    [self configDataEducation];
    [self configDataTrain];
    
    [self creationView];
    [self regeditCell];
    [self isCollect];
    [self netRequest];
    [self.popView returnSureBtn:^{
       
        /*这里需要判断填写的信息是否正常填写*/
        if ([self.popView.timeTF.text isBlankString]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写面试时间"];
            return;
        }
        if ([self.popView.adressTF.text isBlankString]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写面试地点"];
            return;
        }
        if ([self.popView.personTF.text isBlankString]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写联系人信息"];
            return;
        }
        if ([self.popView.phoneTF.text isBlankString]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写联系方式"];
            return;
        }
        if (![SQJudgeInfomation isMobileNumber:self.popView.phoneTF.text])
        {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写正确的手机号"];
            return;
        }
        
        /*初始化token*/
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        /*发送短信参数*/
        NSDictionary * dic = @{@"token":token,
                               @"mobile":self.phoneStr,
                               @"time":self.popView.timeTF.text,
                               @"addr":self.popView.adressTF.text,
                               @"hr":self.popView.personTF.text,
                               @"phoneNumber":self.popView.phoneTF.text,
                               @"deliveryId":self.BiographicalNotesManageId,
                               @"positionTitle":self.positionTitle
                               };
        NSLog(@"%@",dic);
        [[HRRequest manager]POST:PATH_resume_send para:dic success:^(id data) {
            NSLog(@"%@",data);
            
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"发送短信陈功"];
            [self.popView removeFromSuperview];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"发送短信失败"];
        }];
        
    }];
}



#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

#pragma mark - ---------- Section的内容 ----------
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"基本信息";
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
        
    }else if (section == 1) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"技能标签";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
        
    }else if (section == 2) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"工作经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
    } else if (section == 3) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"教育经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return sectionWork;
        
        
    } else {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.backgroundColor = [UIColor whiteColor];
        
        sectionWork.titleLabel.text = @"培训经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return sectionWork;
        
    }
    
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 45;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        
        return 1;
    } else if (section == 2) {
        
        return  self.arrOfWork.count;
    } else if (section == 3) {
        
        return  self.arrOfEducation.count;
        
    }else {
        
        return self.arrOfTrain.count;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 75;
    }else if (indexPath.section == 1){
        
        return 57;
    }else if (indexPath.section == 2) {
        
        return 100;
    }else if (indexPath.section == 3){
        return 60;
    }
    return 80;
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    
    if (indexPath.section == 0) {
        
        ICN_Basic_informationTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_Basic informationTableViewCell" forIndexPath:indexPath];
        [cell setModel:_baseInfoModel];
        self.phoneStr = _baseInfoModel.memberMobile;
        self.positionTitle= _baseInfoModel.hopePositionName;
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        return cell;
        
    }else if (indexPath.section == 1) {
        
        ICN_LSkillTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_LSkillTagTableViewCell" forIndexPath:indexPath];

        cell.memberId = self.memberId;
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        return cell;
        
        
    }else if (indexPath.section == 2) {
        //warn===  在这里可能需要根据Cell的内容的高度来确认显示的样式
        ICN_ResumeWorkExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeWorkExperienceTableViewCell" forIndexPath:indexPath];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeWorkExModel *model = self.arrOfWork[indexPath.row];
        cell.modelWork = model;
        return cell;
        
    }else if (indexPath.section == 3){
        
        ICN_ResumeEducationExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeEducationExperienceTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeEducationExperienceModel *model = self.arrOfEducation[indexPath.row];
        cell.modelEducation = model;
        return cell;
        
        
    }else {
        
        
        ICN_ResumeTrainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeTrainTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeTrainExperienceModel *model = self.arrOfTrain[indexPath.row];
        cell.modeloOfTrain = model;
        return cell;
        
    }
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.tableView)
        
    {
        
        CGFloat sectionHeaderHeight = 64; //你的header高度
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }
        
    }
    
    
    
}


#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)creationView{
    
    self.navigationItem.title = @"简历预览";
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- 114);
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = RGB(235, 236, 237);
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    
    
}
- (void)regeditCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_PerfectTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_PerfectTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_Basic informationTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_Basic informationTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_LSkillTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_LSkillTagTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeWorkExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeWorkExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeEducationExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeEducationExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeTrainTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeTrainTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeHideTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeHideTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyResumeSection" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ICN_MyResumeSection"];
    
}

//处理工作经历接口
- (void)configDataWork{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_OtherWorkExperienceList params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        
        self.arrOfWork = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeWorkExModel *model = [[ICN_MyResumeWorkExModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfWork addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
}

//处理教育经历接口
- (void)configDataEducation{

    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_OtherEducationExperienceList params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfEducation = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeEducationExperienceModel *model = [[ICN_MyResumeEducationExperienceModel alloc]init];
            [dic setValue:model.expID forKey:@"id"];
            
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfEducation addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
}
//培训经历
- (void)configDataTrain{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_OtherTrainExperienceList params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfTrain = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeTrainExperienceModel *model = [[ICN_MyResumeTrainExperienceModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfTrain addObject:model];
        }
        
        [_tableView reloadData];
        
        

    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
}

//基础信息
- (void)userBaseInfo {
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_OtherMemberInfo params:dic success:^(id result) {
        _baseInfoModel = [[ICN_MyResumeBasicInformModel alloc] initWithDictionary:result[@"result"] error:nil];
        [self.tableView reloadData];
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];

}

//是否被收藏了
- (void)isCollect
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_IsCollectResume params:dic success:^(id result) {
        _isCollectStr = SF(@"%@",result[@"result"][@"collection"] );
        if ([_isCollectStr isEqualToString:@"1"]) {
            _xingxingButton.selected = YES;
            [self.collectBtn setTitle:@"已收藏" forState:UIControlStateNormal];
        } else {
            _xingxingButton.selected = NO;
            [self.collectBtn setTitle:@"收藏" forState:UIControlStateNormal];
        }
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
}

//处理工作经历接口
- (void)netRequest{
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"id"] = self.BiographicalNotesManageId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_doBrowseResume params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showMessageLym:(NSString *)sign
{
    UIAlertController *Sign=[UIAlertController alertControllerWithTitle:sign message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *Yes=[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [Sign addAction:Yes];
    [self presentViewController:Sign animated:YES completion:nil];
}
@end
