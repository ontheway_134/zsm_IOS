//
//  ICN_ResumeManageViewController.m
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ResumeManageViewController.h"
#import "ICN_ResumeManageTableViewCell.h"
#import "ICN_IndusTryView.h"
#import "ICN_SLIndusTryTableViewCell.h"
#import "ICN_BusinessResumeManageViewModel.h"
#import "ICN_PosicationListModel.h"
#import "ICN_BiographicalNotesManageModel.h"
#import "ICN_EmptyDataView.h" // 显示数据为空的View

#import "ICN_ResumeContentViewController.h"

@interface ICN_ResumeManageViewController ()<UITableViewDataSource,UITableViewDelegate , ICN_BusinessResumeManageViewModelDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITableView *ResumeManageTabelView;
@property (weak, nonatomic) IBOutlet UIView *allView;
@property (weak, nonatomic) IBOutlet UIButton *receiveResumeBtn; // 收到的简历
@property (weak, nonatomic) IBOutlet UIButton *collectedResumeBtn; // 收藏的简历
@property (weak, nonatomic) IBOutlet UIButton *invilideResumeBtn; // 合适的简历

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;

#pragma mark - ---------- NormalViewProperty ----------

@property(nonatomic,strong)ICN_IndusTryView *indusTryView;

@property(nonatomic,strong)UIButton * defaultBtn;  //弹出视图的交换btn
@property(nonatomic,strong)UIButton *indusTryBtn;
@property (weak, nonatomic) IBOutlet UIView *LineView;

@property (nonatomic , strong)NSArray <ICN_PosicationListModel *>* resumeFilterModelsArr; // 简历筛选数组
@property (nonatomic , strong)ICN_BusinessResumeManageViewModel *viewModel;

@property (nonatomic, copy) ICN_BiographicalNotesManageModel *model;
@property (nonatomic, strong) NSMutableArray *modelArr;

@property (assign, nonatomic) NSInteger indexNumber;

@property (nonatomic , strong)ICN_EmptyDataView *emptyDataView;

@end

@implementation ICN_ResumeManageViewController

#pragma mark - ---------- 懒加载 ----------

- (ICN_EmptyDataView *)emptyDataView{
    if (_emptyDataView == nil) {
        _emptyDataView = [[ICN_EmptyDataView alloc] loadXibWithMessage:@"未检索到合适的简历" DefaultHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 100;
        frame.origin.y = 100;
        _emptyDataView.frame = frame;
        [self.view addSubview:_emptyDataView];
    }
    return _emptyDataView;
}
- (IBAction)searchContent:(id)sender {
    
    if (_searchTextField.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入内容"];
    } else {
        
//        self.indusTryBtn.selected=NO;
//        _receiveResumeBtn.selected = YES;
//        _collectedResumeBtn.selected = NO;
//        _invilideResumeBtn.selected = NO;
//        self.indusTryBtn = _receiveResumeBtn;
        
        //        CGPoint center = self.allView.center;
        //                center.x = SCREEN_WIDTH / 6.0;
        //        _indexNumber = 1;
        NSLog(@"%ld",_indexNumber);
        //        [UIView animateWithDuration:0.3 animations:^{
        //            self.allView.center = center;
        //        } completion:^(BOOL finished) {
        //            self.allView.center = center;
        //        }];
        
        [self.ResumeManageTabelView reloadData];
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        dic[@"keyword"] = _searchTextField.text;
        dic[@"position"] = @"0";
        dic[@"type"] = [NSString stringWithFormat:@"%ld",_indexNumber];
        NSLog(@"%@",dic);
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DOSEARCHMEMBERRESUME params:dic success:^(id result) {
            _modelArr = [NSMutableArray array];
            NSString *codeStr = [NSString stringWithFormat:@"%@",result[@"code"]];
            if ([codeStr isEqualToString:@"0"]) {
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:result[@"result"]];
                for (int i=0; i<tempArr.count; i++) {
                    _model = [[ICN_BiographicalNotesManageModel alloc]initWithDictionary:result[@"result"][i] error:nil];
                    [_modelArr addObject:_model];
                }
            }
            [self.ResumeManageTabelView reloadData];
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
    }
}

#pragma mark - --- 网络请求 ---

-(void)httpEnterPriseMessage:(NSInteger)index{
    /**
     * 123分别为 收到的简历、收藏的简历、合适的简历
     */
    
    NSString *url;
    if (index == 1) {
        url = PATH_RECEIVEDRESUME;
    } else if (index == 2) {
        url = PATH_COLLECTEDRESUME;
    } else {
        url = PATH_SUITABLERESUME;
    }
    NSMutableDictionary *parmas = [NSMutableDictionary dictionary];
    parmas[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    
    [[[HRNetworkingManager alloc]init ] POST_PATH:url params:parmas success:^(id result) {
        self.emptyDataView.viewHidden = YES;
        _modelArr = [NSMutableArray array];
        NSString *codeStr = [NSString stringWithFormat:@"%@",result[@"code"]];
        if ([codeStr isEqualToString:@"0"]) {
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:result[@"result"]];
            for (int i=0; i<tempArr.count; i++) {
                _model = [[ICN_BiographicalNotesManageModel alloc]initWithDictionary:result[@"result"][i] error:nil];
                [_modelArr addObject:_model];
            }
        }
        [self.ResumeManageTabelView reloadData];
        [self.ResumeManageTabelView.mj_header endRefreshing];
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
        [self.ResumeManageTabelView.mj_header endRefreshing];
    }];
}

- (ICN_BusinessResumeManageViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_BusinessResumeManageViewModel alloc] init];
        _viewModel.delegate = self;
    }
    return _viewModel;
}
- (ICN_IndusTryView *)indusTryView{
    if (_indusTryView == nil) {
        _indusTryView = XIB(ICN_IndusTryView);
        _indusTryView.frame = [UIScreen mainScreen].bounds;
        NSMutableArray *array = [NSMutableArray arrayWithArray:self.resumeFilterModelsArr];
        _indusTryView.modelsArr = array;
        //        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHiddenSelection)];
        //        [_indusTryView.leftHiddenBackground addGestureRecognizer:tap];
        [_indusTryView configTableViewWihleViewWillLoad];
        [_indusTryView callBackWithCellSelectedBlock:^(NSString *selectedContent) {
            
            //            self.indusTryBtn.selected=NO;
            //           _receiveResumeBtn.selected = YES;
            //            _collectedResumeBtn.selected = NO;
            //            _invilideResumeBtn.selected = NO;
            //            self.indusTryBtn = _receiveResumeBtn;
            
            // 获取到选中内容 -- 调用搜索接口
            //            CGPoint center = self.allView.center;
            //            center.x = SCREEN_WIDTH / 6.0;
            //            _indexNumber = 1;
            NSLog(@"%ld",_indexNumber);
            //            [UIView animateWithDuration:0.3 animations:^{
            //                self.allView.center = center;
            //            } completion:^(BOOL finished) {
            //                self.allView.center = center;
            //            }];
            
            //warn=== 职场筛选 position需要传id
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            dic[@"position"] = selectedContent;
            /*当前选择列表 type 收到 1 收藏 2  合适3 */
            dic[@"type"] = [NSString stringWithFormat:@"%ld",_indexNumber];
            dic[@"keyword"] = @"";
            NSLog(@"%@",dic);
            [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DOSEARCHMEMBERRESUME params:dic success:^(id result) {
                self.emptyDataView.viewHidden = YES;
                _modelArr = [NSMutableArray array];
                NSString *codeStr = [NSString stringWithFormat:@"%@",result[@"code"]];
                if ([codeStr isEqualToString:@"0"]) {
                    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:result[@"result"]];
                    for (int i=0; i<tempArr.count; i++) {
                        _model = [[ICN_BiographicalNotesManageModel alloc]initWithDictionary:result[@"result"][i] error:nil];
                        [_modelArr addObject:_model];
                    }
                }
                [self.ResumeManageTabelView reloadData];
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"请求数据失败");
                // 数据请求失败之后清空数组并刷新数据
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求错误"];
                [self.modelArr removeAllObjects];
                [self.ResumeManageTabelView reloadData];
                self.emptyDataView.viewHidden = NO;
            }];
        }];
        [[[UIApplication sharedApplication] keyWindow] addSubview:_indusTryView];
    }
    return _indusTryView;
}

#pragma mark - --- 生命周期 ---

- (void)tapHiddenSelection{
    _indusTryView.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewModel];
    [self.viewModel requestForIndustryModelListArrData];
    [self.navigationController setNavigationBarHidden:YES];
    [self.ResumeManageTabelView registerNib:[UINib nibWithNibName:@"ICN_ResumeManageTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeManageTableViewCell"];
    self.ResumeManageTabelView.delegate=self;
    self.ResumeManageTabelView.dataSource=self;
    [self.ResumeManageTabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self setMJRefresh];
    [self httpEnterPriseMessage:1];
    _indexNumber = 1;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if (_indexNumber != 0) {
        [self.ResumeManageTabelView.mj_header beginRefreshing];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_indusTryView) {
        [_indusTryView removeFromSuperview];
        _indusTryView = nil;
    }
}
#pragma mark - ---------- 私有方法 ----------
- (void)changeSelectedBtnDelearltViewWithIndex:(NSInteger)index{
    CGPoint center = self.allView.center;
    switch (index) {
        case 1:
            center.x = SCREEN_WIDTH / 6.0;
            break;
        case 2:
            center.x = SCREEN_WIDTH / 2.0;
            break;
        case 3:
            center.x = SCREEN_WIDTH / 6.0 * 5.0;
            break;
        default:
            break;
    }
    _indexNumber = index;
    [self.ResumeManageTabelView.mj_header beginRefreshing];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.allView.center = center;
    } completion:^(BOOL finished) {
        self.allView.center = center;
    }];
}

#pragma mark - --- Protocol ---

// ViewModel的代理
#pragma mark --- ICN_BusinessResumeManageViewModelDelegate ---
- (void)responseIndustryModelsRequestWithSuccess:(BOOL)success Error:(NSString *)error{
    if (success) {
        self.resumeFilterModelsArr = [NSArray arrayWithArray:self.viewModel.industryModelsArr];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"简历筛选数据获取失败"];
    }
}
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _modelArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_ResumeManageTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeManageTableViewCell"];
    if (cell==nil) {
        cell=[[NSBundle mainBundle] loadNibNamed:@"ICN_ResumeManageTableViewCell" owner:self options:nil].lastObject;
    }
    [cell setModel:_modelArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 85;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_ResumeContentViewController *vc = [[ICN_ResumeContentViewController alloc] init];
    ICN_BiographicalNotesManageModel *amodel = [[ICN_BiographicalNotesManageModel alloc] init];
    amodel = _modelArr[indexPath.row];
    
    vc.memberId = amodel.memberId;
    vc.resumeId = amodel.resumeId;
    vc.BiographicalNotesManageId = amodel.BiographicalNotesManageId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - --- IBActions ---

- (IBAction)ClickActions:(UIButton *)sender {
    
    
    switch ([sender tag]) {
        case 0:{
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        case 1:{
            
        }
            break;
        case 2:{
            // 点击简历筛选按钮
            if (self.resumeFilterModelsArr) {
                self.indusTryView.hidden = NO;
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请等待数据获取"];
            }
            
            break;
        }
    }
}

// 点击切换简历状态的按钮
- (IBAction)rollClickAction:(UIButton *)sender {
    
    self.indusTryBtn.selected=NO;
    sender.selected=YES;
    self.indusTryBtn = sender;
    
    switch ([sender tag]) {
        case 1001:{
            
            [self changeSelectedBtnDelearltViewWithIndex:1];
            [self.ResumeManageTabelView reloadData];
            
            
        }
            break;
            
        case 1002:{
            
            [self changeSelectedBtnDelearltViewWithIndex:2];
            [self.ResumeManageTabelView reloadData];
            
            
        }
            break;
            
        case 1003:{
            [self changeSelectedBtnDelearltViewWithIndex:3];
            [self.ResumeManageTabelView reloadData];
        }
            break;
    }
}


- (void)indusTryBtn:(UIButton *)sender{
    self.defaultBtn.selected=NO;
    sender.selected=YES;
    self.defaultBtn = sender;
    
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_indexNumber == 3) {
        return NO;
    } else {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // if (editingStyle == UITableViewCellEditingStyleDelete) {
    // [dataArray removeObjectAtIndex:indexPath.row];
    
    [self.ResumeManageTabelView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}



-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *rowAction;
    if (_indexNumber == 1) {
        rowAction = [UITableViewRowAction
                     rowActionWithStyle:UITableViewRowActionStyleNormal
                     title:@"收藏"
                     handler:^(UITableViewRowAction * _Nonnull action,
                               NSIndexPath * _Nonnull indexPath)
                     {
                         
                         ICN_BiographicalNotesManageModel *modell = [[ICN_BiographicalNotesManageModel alloc] init];
                         modell = _modelArr[indexPath.row];
                         NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                         dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                         dic[@"resumeId"] = modell.resumeId;
                         [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DOADDCOLLECTION params:dic success:^(id result) {
                             
                             [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
                             [self.ResumeManageTabelView.mj_header beginRefreshing];
                         } failure:^(NSDictionary *errorInfo) {
                             NSLog(@"请求数据失败");
                         }];
                     }];
        rowAction.backgroundColor = RGB0X(0x444444);
        NSArray *arr = @[rowAction];
        return arr;
        
    }   else if (_indexNumber == 2) {
        
        rowAction = [UITableViewRowAction
                     rowActionWithStyle:UITableViewRowActionStyleNormal
                     title:@"删除"
                     handler:^(UITableViewRowAction * _Nonnull action,
                               NSIndexPath * _Nonnull indexPath)
                     {
                         
                         ICN_BiographicalNotesManageModel *modell = [[ICN_BiographicalNotesManageModel alloc] init];
                         modell = _modelArr[indexPath.row];
                         NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                         dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                         dic[@"resumeId"] = modell.resumeId;
                         [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_DODELETECOLLECTION params:dic success:^(id result) {
                             
                             [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
                             [self.ResumeManageTabelView.mj_header beginRefreshing];
                         } failure:^(NSDictionary *errorInfo) {
                             NSLog(@"请求数据失败");
                         }];
                     }];
        rowAction.backgroundColor = [UIColor redColor];
        NSArray *arr = @[rowAction];
        return arr;
    } else {
        return nil;
    }
    
    
}
- (void)setMJRefresh {
    //下拉刷新
    self.ResumeManageTabelView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(mjNetRequest)];
    //上拉加载更多
    //    self.ResumeManageTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(mjMoreNetRequest)];
    [self.ResumeManageTabelView.mj_header beginRefreshing];
}

- (void)mjNetRequest {
    [self httpEnterPriseMessage:_indexNumber];
}

@end
