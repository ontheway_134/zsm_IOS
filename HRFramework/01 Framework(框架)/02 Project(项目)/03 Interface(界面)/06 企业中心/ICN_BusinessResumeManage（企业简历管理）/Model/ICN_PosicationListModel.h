//
//  ICN_PosicationListModel.h
//  ICan
//
//  Created by albert on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@protocol ICN_PosicationDetialListModel <NSObject>
@end

@protocol ICN_PosicationDetialModel <NSObject>
@end


@interface ICN_PosicationListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *selectedCell; // 逻辑属性 - 选中的Cell 0/nil未选中 1选中

@property (nonatomic , copy)NSString *posicationListId; // 一级列表Id
@property (nonatomic , copy)NSString *className; // 名称
@property (nonatomic , strong)NSMutableArray <ICN_PosicationDetialListModel> *list; // 子级列表

@end

@interface ICN_PosicationDetialListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *posicationDetialListId; // 二级列表Id
@property (nonatomic , copy)NSString *className; // 名称
@property (nonatomic , strong)NSMutableArray <ICN_PosicationDetialModel> *list; // 子级列表

@end

@interface ICN_PosicationDetialModel : BaseOptionalModel

@property (nonatomic , copy)NSString *posicationDetialId; // 三级列表Id
@property (nonatomic , copy)NSString *className; // 名称


@end
