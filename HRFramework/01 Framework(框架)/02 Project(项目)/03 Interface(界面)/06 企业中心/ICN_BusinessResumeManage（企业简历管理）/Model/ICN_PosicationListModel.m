//
//  ICN_PosicationListModel.m
//  ICan
//
//  Created by albert on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PosicationListModel.h"

@implementation ICN_PosicationListModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"posicationListId" : @"id"} ];
}

@end

@implementation ICN_PosicationDetialListModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"posicationDetialListId" : @"id"} ];
}

@end

@implementation ICN_PosicationDetialModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"posicationDetialId" : @"id"} ];
}

@end
