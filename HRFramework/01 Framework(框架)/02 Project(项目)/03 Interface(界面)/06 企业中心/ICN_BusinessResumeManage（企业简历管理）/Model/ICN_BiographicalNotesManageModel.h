//
//  ICN_BiographicalNotesManageModel.h
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ICN_BiographicalNotesManageModel : JSONModel

@property (copy, nonatomic) NSString *BiographicalNotesManageId;
@property (copy, nonatomic) NSString *resumeId;

@property (copy, nonatomic) NSString *memberLogo;
@property (copy, nonatomic) NSString *memberNick;
@property (copy, nonatomic) NSString *memberId;

@property (copy, nonatomic) NSString *isCollection;

@property (copy, nonatomic) NSString *positionTitle;
@property (copy, nonatomic) NSString *cityName;
@property (copy, nonatomic) NSString *memberMobile;
@property (copy, nonatomic) NSString *workExperience;
@property (copy, nonatomic) NSString *memberQualification;
@property (nonatomic,strong) NSString * hopePositionName;
@end
