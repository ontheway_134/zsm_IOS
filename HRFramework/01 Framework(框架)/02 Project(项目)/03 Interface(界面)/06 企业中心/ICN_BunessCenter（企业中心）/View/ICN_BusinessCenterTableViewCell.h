//
//  ICN_BusinessCenterTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_BusinessCenterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *BusinessImageView;
@property (weak, nonatomic) IBOutlet UILabel *BusinessLabel;

@end
