//
//  ICN_ActivityFristTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActivityFristTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_ActivityFristTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titmeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contentIcon;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;

@end
@implementation ICN_ActivityFristTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}
- (void)setModel:(ICN_activityListModel *)model
{
    _model = model;
    self.titleLabel.text = model.infoList.title;
    [self.contentIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
    if ([model.infoList.price doubleValue] == 0) {
        self.priceLabel.text = @"免费";
    }else{
        self.priceLabel.text = SF(@"￥%@",model.infoList.price);
    }
  
    self.titmeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.infoList.beginDate],[NSString conversionTimeStamp:model.infoList.endDate]) ;
    self.contentLabel.text = model.infoList.content;
    self.numLabel.text = SF(@"%@/%@人",model.infoList.subNum ,model.infoList.canNum);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
