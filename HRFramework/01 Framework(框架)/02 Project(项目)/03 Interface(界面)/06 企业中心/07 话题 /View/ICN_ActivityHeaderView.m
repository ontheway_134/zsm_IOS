//
//  ICN_ActivityHeaderView.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActivityHeaderView.h"
#import "ICN_activityNavModel.h"
#import "ICN_AdvertimistModel.h"
#import "UIView+Responder.h"

#import "ICN_GroupDetialViewController.h"    //职场规划
#import "ICN_PositionNextOneViewController.h"    //职场规划详情
#import "ICN_ActivityDetialViewController.h"    //活动详情

@interface ICN_ActivityHeaderView()<SDCycleScrollViewDelegate>
@end
@implementation ICN_ActivityHeaderView
- (void)setIndex:(NSString *)index
{
    
    
    _index = index;
   
        dispatch_queue_t queue =  dispatch_get_global_queue(0, 0);
        dispatch_async(queue, ^{
            

        });

    
    
    
    
}
-(void)setDataSources:(NSArray *)dataSources {
    
    _dataSources = dataSources;
    NSMutableArray *bannerArr = [NSMutableArray array];
    [_dataSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        ICN_AdvertimistModel *model = obj;
        [bannerArr addObject:model.pic];
    }];
    [self layoutSubviewss:bannerArr];
    
    
}
-(void)layoutSubviewss:(NSArray *)arr {
    
    self.cycleScrollView.backgroundColor = [UIColor redColor];
    
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200*AUTO_320) imageNamesGroup:arr];
    
    self.cycleScrollView.showPageControl = YES;
    
    self.cycleScrollView.placeholderImage = [UIImage imageNamed:@"default_Image"];
    
    self.cycleScrollView.delegate = self;
    
    
    
    self.cycleScrollView.autoScrollTimeInterval = 2.5;
    
    //设置轮播视图分也控件的位置
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    self.cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    
    // 当前分页控件小圆标颜色
    UIImage *img = [UIImage imageNamed:@"banner选中"];
    self.cycleScrollView.currentPageDotImage = img;
    
    //其他分页控件小圆标颜色
    self.cycleScrollView.pageDotImage = [UIImage imageNamed:@"banner未选中"];
    
    [self addSubview:self.cycleScrollView];
    
}
#pragma mark - 轮播图代理方法
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    NSLog(@"%ld",index);
    ICN_AdvertimistModel *model = self.dataSources[index];
    BOOL istoken = YES;
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == nil) {
        [USERDEFAULT setValue:SF(@"%ld",(long)HR_ICNVisitor) forKey:HR_UserTypeKEY];
        istoken = NO;
    }
    
    if ([model.adType integerValue] == 1) {
        // 跳转到内部链接
        // 1. 区分对应的跳转操作
        switch ([model.adClass integerValue]) {
            case 1:{
                // 跳转到首页列表
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    
                    [self.viewController presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 2:{
                //跳转到职场
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 2;
                    [self.viewController presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 3:{
                // 跳转到职场详情
                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                barController.selectedIndex = 2;
                ICN_PositionNextOneViewController *position = [[ICN_PositionNextOneViewController alloc]init];
                position.url = model.pageId;
                [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                [self.viewController presentViewController:barController animated:YES completion:^{
                    [APPLICATION keyWindow].rootViewController = barController;
                    
                }];
                
                
                break ;
            }
            case 4:{
                // 跳转到活动列表
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 3;
                    [self.viewController presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 5:{
                // 跳转到活动详情
                if (istoken) {
                    // 用户登录
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 3;
                    ICN_ActivityDetialViewController *position = [[ICN_ActivityDetialViewController alloc]init];
                    position.packID = model.pageId;
                    [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                    
                    [self.viewController presentViewController:barController animated:YES completion:^{
                        [APPLICATION keyWindow].rootViewController = barController;
                    }];
                }else{
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                        //延时后想要执行的代码
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        barController.selectedIndex = 3;
                        [self.viewController presentViewController:barController animated:YES completion:nil];
                    });
                }
                
                break ;
            }
            default:
                break;
        }
    }else{
        // 跳转到广告
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.adUrl]];
    }

}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
    
    
}


@end
