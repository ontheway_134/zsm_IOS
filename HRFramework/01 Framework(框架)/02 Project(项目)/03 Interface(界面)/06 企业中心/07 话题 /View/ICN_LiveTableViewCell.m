//
//  ICN_LiveTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_LiveTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UIImageView *timeImg;

@end
@implementation ICN_LiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.headImg.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:self.headImg.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.headImg.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.headImg.layer.mask = maskLayer;
    
}
- (void)setModel:(ICN_activityListModel *)model
{
    _model = model;
    self.titleLabel.text = model.infoList.title;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
    self.numLabel.text = SF(@"%@/%@",model.infoList.subNum,model.infoList.canNum);
    if ([model.infoList.price doubleValue] == 0) {
        self.priceLabel.text = @"免费";
    }else{
        self.priceLabel.text = SF(@"￥%@",model.infoList.price);
    }
    
    

    
    //时间区间
    //如果没有结束时间
    if ([model.infoList.endDate integerValue] == 0) {
        self.timeLabel.text = SF(@"%@-",[NSString conversionTimeStamp:model.infoList.beginDate]) ;
    }else{
    self.timeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.infoList.beginDate],[NSString conversionTimeStamp:model.infoList.endDate]) ;
    }
    self.nameLabel.text = model.infoList.memberNick;
    if ([model.infoList.beginDate integerValue] == 0) {
        self.timeLabel.text = @"";
        self.timeImg.hidden = YES;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
