//
//  ICN_ActivityFristTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_activityListModel.h"
@interface ICN_ActivityFristTableViewCell : UITableViewCell
@property (nonatomic,strong) ICN_activityListModel * model;
@end
