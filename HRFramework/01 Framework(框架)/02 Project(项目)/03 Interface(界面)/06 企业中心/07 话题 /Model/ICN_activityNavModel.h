//
//  ICN_activityNavModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_activityNavModel : BaseOptionalModel

@property (nonatomic,strong) NSString * actNavID;
@property (nonatomic,strong) NSString * label;
@property (nonatomic,strong) NSString * pic;
@end
