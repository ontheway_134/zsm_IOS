//
//  ICN_PicModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"
@interface ICN_PicModel : JSONModel
@property (nonatomic,strong) NSString * picID;
@property (nonatomic,strong) NSString * pic;
@end
