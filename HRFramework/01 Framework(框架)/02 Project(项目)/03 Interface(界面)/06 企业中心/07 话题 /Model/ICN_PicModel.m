//
//  ICN_PicModel.m
//  ICan
//
//  Created by 何壮壮 on 17/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PicModel.h"

@implementation ICN_PicModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"picID" : @"id"}];
}
@end
