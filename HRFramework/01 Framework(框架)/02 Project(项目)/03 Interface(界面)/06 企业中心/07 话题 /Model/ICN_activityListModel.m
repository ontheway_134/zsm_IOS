//
//  ICN_activityListModel.m
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_activityListModel.h"
@implementation activityListModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"packID" : @"id"}];
}

@end
@implementation ICN_activityListModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"infoList" : @"info"}];
}

@end
