//
//  ICN_ThemeVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ThemeVC.h"
#pragma mark ----------------MODEL---------------------
#import "ICN_ActivityMainModel.h"
#import "ICN_activityNavModel.h"
#import "ICN_PicModel.h"
#import "ICN_activityListModel.h"
#import "ICN_AdvertimistModel.h"
#pragma mark ----------------CELL/VIEW---------------------
#import "ICN_ActivityFristTableViewCell.h"
#import "ICN_LiveTableViewCell.h"
#import "ICN_ActivityHeaderView.h"
#pragma mark ----------------CONTROLLER---------------------
#import "ICN_ActivityParticularsViewController.h"
#import "ICN_ActivityDetialViewController.h"
#import "ICN_LiveDetialViewController.h"
//直播页面
#import "AliVcMoiveViewController.h"
#import "MJDIYAutoFooter.h"
#import "ICN_StartViewController.h"
#import "ICN_SignViewController.h"
#import "ICN_GroupDetialViewController.h"    //职场规划
#import "ICN_PositionNextOneViewController.h"    //职场规划详情
#import "ICN_ActivityDetialViewController.h"    //活动详情
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h> //空白页
#import "ICN_GuiderPager.h"
static NSString *const kICN_ActivityFristTableViewCell = @"ICN_ActivityFristTableViewCell";    //活动cell
static NSString *const kICN_LiveTableViewCell = @"ICN_LiveTableViewCell";    //直播cell
@interface ICN_ThemeVC ()<DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
#pragma mark ----------------UIBUTON---------------------
#pragma mark ----------------UIVIEW---------------------

@property (nonatomic,strong) ICN_ActivityHeaderView * headerView;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
#pragma mark ----------------NSARRY---------------------
@property (nonatomic,strong) NSMutableArray * dataSoureArr; /*数据源 **/
@property (nonatomic,strong) NSMutableArray * picArr; /*图片数据源 **/
#pragma mark ----------------属性--------------------
@property (nonatomic,strong) NSString * selectIndex; /*选择的标签 **/
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (nonatomic,strong) NSString * token;
@property (nonatomic,strong) NSURL * playUrl;
@property (nonatomic,strong) UIView * headView;
@property (nonatomic,strong) NSString * currentNavStr;
@end

@implementation ICN_ThemeVC
#pragma mark - ---------- 懒加载 ----------
- (NSString *)token
{
    if (!_token) {
        _token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    return _token;
}
- (NSMutableArray *)picArr
{
    if (!_picArr) {
        _picArr = [NSMutableArray array];
    }
    return _picArr;
}
- (ICN_ActivityHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_ActivityHeaderView);
       
    }
    return _headerView;
}
- (NSMutableArray *)navArr
{
    if (!_navArr) {
        _navArr = [NSMutableArray array];
    }
    return _navArr;
}
- (NSMutableArray *)dataSoureArr
{
    if (!_dataSoureArr) {
        _dataSoureArr = [NSMutableArray array];
    }
    return _dataSoureArr;
}
#pragma mark - ---------- DZNEmptyDataSetSource, DZNEmptyDataSetDelegate ----------
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    
    return [UIImage imageNamed:@"无结果"];
}
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"没有相关数据";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:12.0f],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
#pragma mark - ---------- 生命周期 ----------
/**
 *
 * 注意ZJScrollPageView不会保证viewWillAppear等生命周期方法一定会调用
 * 所以建议使用ZJScrollPageViewChildVcDelegate中的方法来替代对应的生命周期方法完成数据的加载
 */
#pragma mark ----------------ZJScrollPageViewChildVcDelegate---------------------
- (void)zj_viewWillAppearForIndex:(NSInteger)index
{
    
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    //如果选择的标签为热门
    if (index == 0) {
        
        //热门轮播
        
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:YES];
        });
        
        //热门下拉刷新
        self.headerView.index = 0;
        
        self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:YES];
        }];
        //热门上拉刷新
        self.mainTableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
            [self netWorkRequest:TYPE_RELOADDATA_UP isHot:YES];
        }];
        
        
    }
    //如果选择的标签为热门
    else{
        [[HRRequest manager]POST:PATH_activityNav para:@{} success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_activityNavModel *model = [[ICN_activityNavModel alloc]initWithDictionary:obj error:nil];
                [self.navArr addObject:model];
                
                ;
            }];
            ICN_activityNavModel *selectModel = self.navArr[index-1];
            
            self.currentNavStr = selectModel.label;
            //标签选择赋值
            self.selectIndex =  selectModel.actNavID;
            //非热门轮播
            
            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:NO];
            
            //非热门下拉刷新
            self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:NO];
            }];
            //非热门上拉刷新
            self.mainTableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
                [self netWorkRequest:TYPE_RELOADDATA_UP isHot:NO];
            }];
            
            
            
            ;
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
        }];
        
    }

}
- (void)zj_viewDidAppearForIndex:(NSInteger)index
{
    
}
- (void)zj_viewWillDisappearForIndex:(NSInteger)index
{
    
}
- (void)zj_viewDidDisappearForIndex:(NSInteger)index
{
    
}

- (void)zj_viewDidLoadForIndex:(NSInteger)index
{
//    //如果选择的标签为热门
//    if (index == 0) {
//        
//        //热门轮播
//        
//        
//        
//        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:YES];
//        });
//        
//        //热门下拉刷新
//        self.headerView.index = 0;
//        
//        self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
//            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:YES];
//        }];
//        //热门上拉刷新
//        self.mainTableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
//            [self netWorkRequest:TYPE_RELOADDATA_UP isHot:YES];
//        }];
//        
//        
//    }
//    //如果选择的标签为热门
//    else{
//        [[HRRequest manager]POST:PATH_activityNav para:@{} success:^(id data) {
//            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//                ICN_activityNavModel *model = [[ICN_activityNavModel alloc]initWithDictionary:obj error:nil];
//                [self.navArr addObject:model];
//                
//                ;
//            }];
//            ICN_activityNavModel *selectModel = self.navArr[index-1];
//            //标签选择赋值
//            self.selectIndex =  selectModel.actNavID;
//            //非热门轮播
//            
//            [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:NO];
//            
//            //非热门下拉刷新
//            self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
//                [self netWorkRequest:TYPE_RELOADDATA_DOWM isHot:NO];
//            }];
//            //非热门上拉刷新
//            self.mainTableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
//                [self netWorkRequest:TYPE_RELOADDATA_UP isHot:NO];
//            }];
//            
//            
//            
//            ;
//        } faiulre:^(NSString *errMsg) {
//            NSLog(@"%@",errMsg);
//        }];
//        
//    }
    
    
}

/**
 列表页数据请求
 
 @param isMore 是否加载更多
 */
- (void)netWorkRequest:(BOOL)isMore isHot:(BOOL)isHot
{
    //如果是热门
    if (isHot) {
        //并且是上拉
        if (isMore) {
            self.currentPage = SF(@"%ld",[self.currentPage integerValue] + 1);
            NSDictionary *dic = @{@"isHot":@"1",
                                  @"page":self.currentPage,
                                  //@"token":self.token
                                  };
            [[HRRequest manager]POST:PATH_activityPackageList para:dic success:^(id data) {
                //如果成功结束刷新
                [self endRefesh];
                
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    ICN_activityListModel *model = [[ICN_activityListModel alloc]initWithDictionary:obj error:nil];
                    [self.dataSoureArr addObject:model];
                }];
                
                //刷新数据源
                [self.mainTableView reloadData];
                
            } faiulre:^(NSString *errMsg) {
                //如果失败结束刷新
                [self endRefesh];
                //如果失败page-1
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
            }];
            
        }else{
            
            
            
            
            
            
            NSDictionary *dic1 = @{@"type":@"0",
                                   
                                   };
            
            [HTTPManager POST_PATH:PATH_carouselPicList123 params:dic1 success:^(id responseObject) {
                //如果请求成功 发送信号量
                
                NSArray *data = responseObject;
                NSLog(@"%@",data);
                self.picArr = nil;
                if (data.count > 0) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc]initWithDictionary:obj error:nil];
                        [self.picArr addObject:model];
                    }];
                }
               
                self.headerView.dataSources = self.picArr;
            } failure:^(NSError *error) {
                
            }];
            
            
            
            self.currentPage = @"1";
            NSDictionary *dic = @{@"type":@"0",
                                  @"isHot":@"1",
                                 // @"token":self.token
                                  };
            
            [HTTPManager POST_PATH:PATH_activityPackageList params:dic success:^(id responseObject) {
                //如果成功结束刷新
                [self endRefesh];
                NSArray *data = responseObject;
                self.dataSoureArr = nil;
                if (data.count > 0) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_activityListModel *model = [[ICN_activityListModel alloc]initWithDictionary:obj error:nil];
                        [self.dataSoureArr addObject:model];
                    }];
                }else{
                    [self.headerView setFrame:CGRectZero];
                }
               
                
                
                //刷新数据源
                [self.mainTableView reloadData];
                
            } failure:^(NSError *error) {
                //如果失败结束刷新
                [self endRefesh];
                [self.headView setFrame:CGRectZero];
                self.currentPage = @"1";
            }];
            
            
            
            
        }
        
        //如果是其他标签
    }else{
        if (isMore) {
            self.currentPage = SF(@"%ld",[self.currentPage integerValue] + 1);
            NSDictionary *dic = @{@"type":self.selectIndex,
                                  @"page":self.currentPage,
                                  //@"token":self.token
                                  };
            [[HRRequest manager]POST:PATH_activityPackageList para:dic success:^(id data) {
                //如果成功结束刷新
                [self endRefesh];
                
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    ICN_activityListModel *model = [[ICN_activityListModel alloc]initWithDictionary:obj error:nil];
                    [self.dataSoureArr addObject:model];
                }];
                
                //刷新数据源
                [self.mainTableView reloadData];
                
            } faiulre:^(NSString *errMsg) {
                //如果失败结束刷新
                [self endRefesh];
                //如果失败page-1
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
            }];
            
        }else{
            
            
            NSDictionary *dic1 = @{@"type":self.selectIndex,
                                  
                                   };
            
            [HTTPManager POST_PATH:PATH_carouselPicList123 params:dic1 success:^(id responseObject) {
                //如果请求成功 发送信号量
                
                NSArray *data = responseObject;
                NSLog(@"%@",data);
                self.picArr = nil;
                if (data.count > 0) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc]initWithDictionary:obj error:nil];
                        [self.picArr addObject:model];
                    }];
                }
                
                self.headerView.dataSources = self.picArr;
            } failure:^(NSError *error) {
                
            }];
            
            
            
            self.currentPage = @"1";
            NSDictionary *dic = @{@"type":self.selectIndex,
                                  @"page":@"1",
                                  //@"token":self.token
                                  };
            
            [HTTPManager POST_PATH:PATH_activityPackageList params:dic success:^(id responseObject) {
                //如果成功结束刷新
                [self endRefesh];
                NSArray *data = responseObject;
                self.dataSoureArr = nil;
                if (data.count > 0 ) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_activityListModel *model = [[ICN_activityListModel alloc]initWithDictionary:obj error:nil];
                        [self.dataSoureArr addObject:model];
                    }];
                }else{
                    [self.headView setFrame:CGRectZero];
                }
                
                
                
                //刷新数据源
                [self.mainTableView reloadData];
                
            } failure:^(NSError *error) {
                //如果失败结束刷新
                [self endRefesh];
                [self.headView setFrame:CGRectZero];
                self.currentPage = @"1";
            }];
            
        }
    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化设置界面
    [self configUI];
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    self.selectIndex = 0;
    //[self netPic:YES];
    
    self.mainTableView.emptyDataSetSource = self;
     self.mainTableView.emptyDataSetDelegate = self;
    
    
    
    
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark - ---------- 重写父类方法 ----------
- (void)configUI{
    //注册cell
    
    [self.mainTableView registerNib:[UINib nibWithNibName:kICN_ActivityFristTableViewCell bundle:nil] forCellReuseIdentifier:kICN_ActivityFristTableViewCell];
    [self.mainTableView registerNib:[UINib nibWithNibName:kICN_LiveTableViewCell bundle:nil] forCellReuseIdentifier:kICN_LiveTableViewCell];
    //设置头视图
   self.headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, 200.0*AUTO_320)];
    [self.headView addSubview:self.headerView];
    [self.headerView setFrame:CGRectMake(0, 0,SCREEN_WIDTH, 200.0*AUTO_320)];
   // [self.headerView setFrame:CGRectMake(0, 0,SCREEN_WIDTH, 180.0*AUTO_320)];
    [self.mainTableView setTableHeaderView:self.headView];
    
    
    
   
}
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
- (void)endRefesh
{
    [self.mainTableView.mj_header endRefreshing];
    [self.mainTableView.mj_footer endRefreshing];
}
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---

/**
 轮播图请求接口
 
 @param isHot 是否未热门
 */
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark --- UITextFieldDelegate ---
#pragma mark --- UITableViewDataSource ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSoureArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICN_activityListModel *model = self.dataSoureArr[indexPath.row];
    //0，多直播；1，单直播
    if (model.type == 0) {
        ICN_ActivityFristTableViewCell *cell = (ICN_ActivityFristTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kICN_ActivityFristTableViewCell forIndexPath:indexPath];
        if (cell == nil) {
            cell = XIB(ICN_ActivityFristTableViewCell);
        }
        
        cell.model = model;
        return cell;
    }else
    {
        ICN_LiveTableViewCell *cell = (ICN_LiveTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kICN_LiveTableViewCell forIndexPath:indexPath];
        if (cell == nil) {
            cell = XIB(ICN_LiveTableViewCell);
        }
        cell.model = model;
        return cell;
    }
    return nil;
}

#pragma mark --- UITableViewDelegate ---
/**
 
 
 @param tableView <#tableView description#>
 @param indexPath <#indexPath description#>
 @return <#return value description#>
 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.rowHeight = self.mainTableView.rowHeight;
    return self.mainTableView.rowHeight;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
        return;
    }
    // 先判断信息补全
    if ([self jumpToUserMessageSetUpPager]) {
        // 判断若需要跳转到补全页面则结束当前所有操作
        return ;
    }
    ICN_activityListModel *model = self.dataSoureArr[indexPath.row];
    //0，多直播；1，单直播
    if (model.type == 0) {
        BOOL isLogin =  [self getCurrentUserLoginStatus];
        //已登录
        if (isLogin) {
            ICN_ActivityDetialViewController *MVC = [[ICN_ActivityDetialViewController alloc]init];
            MVC.packID = model.infoList.packID;
            MVC.navTitle = self.currentNavStr;
            [self.navigationController pushViewController:MVC animated:YES];
            //未登录
       // }else{
            
        }
    }else
    {
        
        
//        NSURL* url = [NSURL URLWithString:@"http://omxwtmi0z.bkt.clouddn.com/%5B%E6%96%B0%E7%8C%9B%E9%BE%99%E8%BF%87%E6%B1%9F%5D%E7%AC%AC36%E9%9B%86_hd.mp4"];
//        AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//        MVC.liveID = model.infoList.packID;
//        MVC.actID = model.infoList.activityId;
//        [MVC SetMoiveSource:url];
//        [MVC setHidesBottomBarWhenPushed:YES];
//        [self.navigationController pushViewController:MVC animated:YES];
//        return;

         //未开始
        if ([model.infoList.hasBegin integerValue] == 0) {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.infoList.packID;
            MVC.status = model.infoList.memberStatus;
            MVC.actID = model.infoList.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
            //已开始
        }else{
            
            BOOL isLogin = [self getCurrentUserLoginStatus];
            if (!isLogin) {
                ICN_StartViewController *starPager = [[ICN_StartViewController alloc] init]; // 欢迎页面
                ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
                // 先静默从欢迎页跳转到登录页
                [nav pushViewController:pager animated:NO];
                [self presentViewController:nav animated:YES completion:nil];
            }
            
            //如果是录的直播
            if ((model.infoList.liveUrl == nil)||[model.infoList.liveUrl isEqualToString:@""]) {
                NSInteger memberStatus = [model.infoList.memberStatus integerValue];
                switch (memberStatus) {
                    case 1:
                    {
                        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                        MVC.liveID = model.infoList.packID;
                        MVC.status = model.infoList.memberStatus;
                        MVC.actID = model.infoList.activityId;
                        [self.navigationController pushViewController:MVC animated:YES];
                    }
                        break;
                    case 2:
                    {
                        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                        MVC.liveID = model.infoList.packID;
                        MVC.status = model.infoList.memberStatus;
                        MVC.actID = model.infoList.activityId;
                        [self.navigationController pushViewController:MVC animated:YES];
                        
                        
                    }
                        break;
                    case 3:
                    {
                        
                        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                        MVC.liveID = model.infoList.packID;
                        MVC.status = model.infoList.memberStatus;
                        MVC.actID = model.infoList.activityId;
                        [self.navigationController pushViewController:MVC animated:YES];
                    }
                        break;
                    case 4:
                    {
                        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                        MVC.liveID = model.infoList.packID;
                        MVC.status = model.infoList.memberStatus;
                        MVC.actID = model.infoList.activityId;
                        [self.navigationController pushViewController:MVC animated:YES];
                    }
                        break;
                    case 5:
                    {
                        if (model.infoList.liveUrl == nil||[model.infoList.liveUrl isEqualToString:@""]) {
                            //测试
                            //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                            NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.infoList.streamName)];
                            AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                            MVC.liveID = model.infoList.packID;
                            MVC.actID = model.infoList.activityId;
                            MVC.model = model.infoList;
                            [MVC SetMoiveSource:url];
                            [MVC setHidesBottomBarWhenPushed:YES];
                            [self.navigationController pushViewController:MVC animated:YES];
                        }else{
                            //测试
                            //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                            NSLog(@"%@",model.infoList.liveUrl);
                            NSString* encodedString = [model.infoList.liveUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                            NSString *str = model.infoList.liveUrl;
                            self.playUrl = [NSURL URLWithString:encodedString];
                            AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                            MVC.liveID = model.infoList.packID;
                            MVC.actID = model.infoList.activityId;
                            if (self.playUrl == nil) {
                                MVC.payUrl = str;
                            }else{
                                [MVC SetMoiveSource:self.playUrl];
                            }
                            [MVC setHidesBottomBarWhenPushed:YES];
                            [self.navigationController pushViewController:MVC animated:YES];
                        }
                        
                        
                    }
                        break;
                    case 6:
                    {
                        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                        MVC.liveID = model.infoList.packID;
                        MVC.status = model.infoList.memberStatus;
                        MVC.actID = model.infoList.activityId;
                        [self.navigationController pushViewController:MVC animated:YES];
                        
                    }
                        break;
                        
                        
                    default:
                        
                        break;
                }
            }
            //是真的直播
            else{
                if ([model.infoList.price doubleValue] == 0) {
                    //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                    NSURL* url = [NSURL URLWithString:model.infoList.liveUrl];
                    AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                    MVC.liveID = model.infoList.packID;
                    MVC.model = model.infoList;
                    [MVC SetMoiveSource:url];
                    [MVC setHidesBottomBarWhenPushed:YES];
                    [self.navigationController pushViewController:MVC animated:YES];
                }else{
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.packID;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                }

               
            }

            
            
            
            //二期优化免费直接进
//            if([model.infoList.price doubleValue] == 0){
//                //如果是真直播
//                if (model.infoList.liveUrl == nil||[model.infoList.liveUrl isEqualToString:@""]) {
//                    //测试
//                    //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
//                    NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.infoList.streamName)];
//                    AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.actID = model.infoList.activityId;
//                    MVC.model = model.infoList;
//                    [MVC SetMoiveSource:url];
//                    [MVC setHidesBottomBarWhenPushed:YES];
//                    [self.navigationController pushViewController:MVC animated:YES];
//                }
//                //else{
////                    //测试
////                    //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
////                    NSLog(@"%@",model.infoList.liveUrl);
////                    NSString* encodedString = [model.infoList.liveUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
////                    NSString *str = model.infoList.liveUrl;
////                    self.playUrl = [NSURL URLWithString:encodedString];
////                    AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
////                    MVC.liveID = model.infoList.packID;
////                    MVC.actID = model.infoList.activityId;
////                    if (self.playUrl == nil) {
////                        MVC.payUrl = str;
////                    }else{
////                        [MVC SetMoiveSource:self.playUrl];
////                    }
////                    [MVC setHidesBottomBarWhenPushed:YES];
////                    [self.navigationController pushViewController:MVC animated:YES];
////                }
//
//            }else{
//                ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                MVC.liveID = model.infoList.packID;
//                MVC.status = model.infoList.memberStatus;
//                MVC.actID = model.infoList.activityId;
//                [self.navigationController pushViewController:MVC animated:YES];
//            }
            //NSInteger memberStatus = [model.infoList.memberStatus integerValue];
//            switch (memberStatus) {
//                case 1:
//                {
//                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.status = model.infoList.memberStatus;
//                    MVC.actID = model.infoList.activityId;
//                    [self.navigationController pushViewController:MVC animated:YES];
//                }
//                    break;
//                case 2:
//                {
//                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.status = model.infoList.memberStatus;
//                    MVC.actID = model.infoList.activityId;
//                    [self.navigationController pushViewController:MVC animated:YES];
//                    
////                    NSURL* url = [NSURL URLWithString:model.infoList.liveUrl];
////                    AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
////                    MVC.liveID = model.infoList.packID;
////                    MVC.model = model.infoList;
////                    [MVC SetMoiveSource:url];
////                    [MVC setHidesBottomBarWhenPushed:YES];
////                    [self.navigationController pushViewController:MVC animated:YES];
//                }
//                    break;
//                case 3:
//                {
//                    
//                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.status = model.infoList.memberStatus;
//                    MVC.actID = model.infoList.activityId;
//                    [self.navigationController pushViewController:MVC animated:YES];
//                }
//                    break;
//                case 4:
//                {
//                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.status = model.infoList.memberStatus;
//                    MVC.actID = model.infoList.activityId;
//                    [self.navigationController pushViewController:MVC animated:YES];
//                }
//                    break;
//                case 5:
//                {
//                    if (model.infoList.liveUrl == nil||[model.infoList.liveUrl isEqualToString:@""]) {
//                        //测试
//                        //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
//                        NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.infoList.streamName)];
//                        AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//                        MVC.liveID = model.infoList.packID;
//                        MVC.actID = model.infoList.activityId;
//                        MVC.model = model.infoList;
//                        [MVC SetMoiveSource:url];
//                        [MVC setHidesBottomBarWhenPushed:YES];
//                        [self.navigationController pushViewController:MVC animated:YES];
//                    }else{
//                        //测试
//                        //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
//                        NSLog(@"%@",model.infoList.liveUrl);
//                        NSString* encodedString = [model.infoList.liveUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//                        NSString *str = model.infoList.liveUrl;
//                        self.playUrl = [NSURL URLWithString:encodedString];
//                        AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//                        MVC.liveID = model.infoList.packID;
//                        MVC.actID = model.infoList.activityId;
//                        if (self.playUrl == nil) {
//                            MVC.payUrl = str;
//                        }else{
//                        [MVC SetMoiveSource:self.playUrl];
//                        }
//                        [MVC setHidesBottomBarWhenPushed:YES];
//                        [self.navigationController pushViewController:MVC animated:YES];
//                    }
//                   
//                    
//                }
//                    break;
//                case 6:
//                {
//                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//                    MVC.liveID = model.infoList.packID;
//                    MVC.status = model.infoList.memberStatus;
//                    MVC.actID = model.infoList.activityId;
//                    [self.navigationController pushViewController:MVC animated:YES];
//                    
//                }
//                    break;
//
//                    
//                default:
//                    
//                    break;
//            }

            
            
        }
        
        
        
        
    }
    
    
}


#pragma mark --- NSCopying ---






@end
