//
//  ICN_ThemeVC.h
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageView.h"
#import "ZJContentView.h"
#pragma mark - ---------- 动态枚举 ----------

@interface ICN_ThemeVC : BaseViewController<ZJScrollPageViewChildVcDelegate>

@property (nonatomic,strong) NSMutableArray * navArr;
@property(nonatomic)NSString *memberId;
@end
