//
//  ICN_ActivityParticularsTopModel.h
//  ICan
//
//  Created by 那风__ on 16/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ActivityParticularsTopModel : NSObject
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *price;
@property(nonatomic)NSString *subnum;
@property(nonatomic)NSString *cannum;
@property(nonatomic)NSString *createdate;
@property(nonatomic)NSString *activityid;

@property(nonatomic)NSString *aid;

@end
