//
//  ICN_HotWorldsCell.m
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_HotWorldsCell.h"
#import "ICN_SearchPagerHeader.h"
#import "ICN_HotWorldsModel.h"

@interface ICN_HotWorldsCell ()

@property (nonatomic , strong)NSMutableArray<UIButton *> *btnArr; // 按钮数组


@end

@implementation ICN_HotWorldsCell

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<UIButton *> *)btnArr{
    if (_btnArr == nil) {
        _btnArr = [NSMutableArray array];
    }
    return _btnArr;
}


#pragma mark - ---------- 私有方法 ----------


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ICN_HotWorldsModel *)model{
    _model = model;
    // 执行对应的操作
    
    // 如果存在历史数据需要先清空
    if (self.btnArr.count > 0) {
        for (UIButton *button in self.btnArr) {
            [button removeFromSuperview];
        }
        [self.btnArr removeAllObjects];
    }
    
    if (_model.modelsArr) {
        if (_model.modelsArr.count > 0) {
            // 当热词数组存在的时候
            for (ICN_HotDetialModel *model in _model.modelsArr) {
                UIButton *button = [self createHotCellBtnWithTitle:model.hotWord index:[_model.modelsArr indexOfObject:model]];
                [self.btnArr addObject:button];
            }
        }
        [self setNeedsLayout];
    }
}

// 初始化并配置一个热词Cell
- (UIButton *)createHotCellBtnWithTitle:(NSString *)title index:(NSInteger)index{
    NSString *word;
    if (title) {
        word = title;
    }else{
        word = @"热词为空";
    }
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:button];
    [button setTitle:word forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"按钮-点击"] forState:UIControlStateNormal];
    [button setTitleColor:RGB0X(0xa2b4bb) forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:12.0];
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.5;
    button.tag = index;
    [button addAction:^(NSInteger tag) {
        if (self.block) {
            // 执行点击按钮所需要的对应操作
            NSString *HotContent = [self.model.modelsArr objectAtIndex:tag].hotWord;
            self.model.selectContent = HotContent;
            if (self.block) {
                self.block(self.model);
            }
        }
    }];
    return button;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // 配置按钮的位置
    CGSize itemSize = ICN_SearchPagerHotWorldItemSize;
    for (NSInteger i = 0; i < self.model.modelsArr.count; i = i + self.model.rowCount) {
        NSInteger Ycount = self.model.rowCount + i < self.model.modelsArr.count ? self.model.rowCount + i : self.model.modelsArr.count;
        for (NSInteger j = i; j < Ycount; j++) {
            
            if (j == i) {
                if (i == 0) {
                    [self.btnArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.size.mas_equalTo(itemSize);
                        make.left.equalTo(self.contentView).offset(10);
                        make.top.equalTo(self.contentView).offset(15.0);
                    }];
                }else{
                    [self.btnArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.size.mas_equalTo(itemSize);
                        make.left.equalTo(self.contentView).offset(10.0);
                        make.top.equalTo(self.btnArr[j - 1].mas_bottom).offset(10.0);
                    }];
                }
            }else{
                [self.btnArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.size.mas_equalTo(itemSize);
                    make.left.equalTo(self.btnArr[j - 1].mas_right).offset(15.0);
                    make.top.equalTo(self.btnArr[j - 1]);
                }];
            }
        }
    }
    
}

- (void)callBackWithBtnClickAction:(CellResponseBlock)block{
    self.block = block;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}



#pragma mark - ---------- ButtonAction ----------

- (void)clickOnHotWorldsBtnActionSender:(UIButton *)sender{
    if (self.block) {
        self.block(self.model);
    }
}

@end
