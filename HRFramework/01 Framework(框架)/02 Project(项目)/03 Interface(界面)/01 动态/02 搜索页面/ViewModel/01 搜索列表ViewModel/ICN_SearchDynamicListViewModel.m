//
//  ICN_SearchDynamicListViewModel.m
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SearchDynamicListViewModel.h"
#import "ICN_DynStateContentModel.h"
#import "HRNetworkingManager+DynFirstPager.h"
#import "HRNetworkingManager+SearchFirstPager.h"
#import "UILabel+ALB_SizeFit.h"
#import "ToolAboutTime.h"

@interface ICN_SearchDynamicListViewModel ()

@property (nonatomic , assign)NSInteger currentPage; // 当前页面
@property (nonatomic , strong)ICN_DynStateContentModel *likeUpModel; // 点赞用Model

@end

@implementation ICN_SearchDynamicListViewModel

- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
        // 默认设置智讯状态（也就是wisdom = NO）
        self.wisdomType = NO;
    }
    return self;
}

- (NSMutableArray<ICN_DynStateContentModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (void)requestWithSearchContent:(NSString *)content Load:(BOOL)isLoad Refresh:(BOOL)isRefresh{
    
    if (isRefresh) {
        self.currentPage = 1;
    }else{
        self.currentPage ++;
    }
    
    NSString *netPath;
    if (self.isWisdom) {
        netPath = PATH_WisdomSearch;
    }else{
        netPath = PATH_SearchDynList;
    }
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"search" : SF(@"%@",content) ,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    if (self.currentPage != 0) {
        [params setValue:SF(@"%ld",self.currentPage) forKey:@"page"];
    }
    [params setValue:SF(@"%d",9) forKey:@"size"];
    
    [[HRRequest alloc] POST_PATH:netPath params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:item error:nil];
                // 根据Model的相关属性做智讯还是动态的处理
                if (model.type.integerValue == 0) {
                    // 动态
                    model.widsom = NO;
                }else{
                    // 智讯
                    model.widsom = YES;
                    model.matterId = model.DynID;
                }
                model.contentSpread = NO;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [array addObject:model];
            }
            
            if (self.currentPage == 1) {
                if (_modelsArr) {
                    [_modelsArr removeAllObjects];
                }
            }

            for (ICN_DynStateContentModel *model in array) {
                [self configStateModel:model];
            }
            [self.modelsArr addObjectsFromArray:array];
            [self chooseDynamicStateTypeAfterDataRequest];
            [self callBackWithDynamicStateListError:nil Request:YES];
        }else{
            ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:result error:nil];
            [array addObject:model];
            [self callBackWithDynamicStateListError:model.info Request:NO];
        }
    } failure:^(NSDictionary *errorInfo) {
        // 回调数据获取失败
        [self callBackWithDynamicStateListError:@"网络请求失败" Request:NO];
    }];
    
}


- (void)likeUpWithType:(NSInteger)type Model:(ICN_DynStateContentModel *)model{
    NSString *typeStr = SF(@"%ld",type);
    if (model) {
        self.likeUpModel = model;
    }
    if (model.matterId == nil) {
        model.matterId = model.DynID;
    }
    [HRNetworkingManager updateDynamicLikeUpStateWithMatterId:model.matterId Type:typeStr Success:^(BaseOptionalModel *model) {
        if (model.code == 0) {
            if (self.likeUpModel.isPraise.integerValue == 0) {
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue + 1);
                self.likeUpModel.isPraise = @"1";
            }else{
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue - 1);
                self.likeUpModel.isPraise = @"0";
            }
            [self callBackWithLikeUpResuest:YES Error:nil];
        }
        if (model.code == 1) {
            [self callBackWithLikeUpResuest:NO Error:@"数据获取异常"];
        }
        if (model.code == 2) {
            // 用户无权限点赞
            [self callBackWithLikeUpResuest:NO Error:model.info];
        }
        if (model.code == 3) {
            // 用户已经点赞现在无法点赞只能取消
            self.likeUpModel.isPraise = @"1";
            [self callBackWithLikeUpResuest:NO Error:model.info];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithLikeUpResuest:NO Error:@"数据获取失败"];
    }];
}

#pragma mark - ---------- 私有方法 ----------

/** 在获取到数据之后进行筛选 */
- (void)chooseDynamicStateTypeAfterDataRequest{
    // 最后根据需要的是动态还是智讯进行筛选
    NSMutableArray *resultArray = [NSMutableArray array];
    for (ICN_DynStateContentModel *model in self.modelsArr) {
        if (self.isWisdom && model.title != nil && ![model.title isEqualToString:@""]) {
            // 获取到的是智讯
            [resultArray addObject:model];
        }
        
        if (self.isWisdom == NO) {
            if (model.title == nil || [model.title isEqualToString:@""]) {
                // 获取的是动态
                [resultArray addObject:model];
            }
        }
    }
    [self.modelsArr removeAllObjects];
    [self.modelsArr addObjectsFromArray:resultArray];
}

- (void)configStateModel:(ICN_DynStateContentModel *)model{
    
    if (model.pic != nil && [model.pic containsString:@"."]) {
        NSArray *array = [model.pic componentsSeparatedByString:@","];
        model.imageFooterHeight = [self countImageFooterHeightWithPicCount:array.count];
    }else
        model.imageFooterHeight = 0.0;
    
    
    model.contentSpreadHeight = [UILabel getTextHeight:model.content width:[UIScreen mainScreen].bounds.size.width - 20.0 fontSize:13.0];
}

- (CGFloat)countImageFooterHeightWithPicCount:(NSInteger)count{
    NSInteger groups = count / 3;
    if (count < 3) {
        groups = 0;
    }
    if (count % 3 > 0) {
        groups ++;
    }
    // 获取5s尺寸下的imageview占用高度
    CGFloat countHeight = groups * (97.0 + 5.0) - 5.0;
    return ICN_KScreen(countHeight);
}


- (void)callBackWithDynamicStateListError:(NSString *)error Request:(BOOL)success{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseSearchListRequestWithSuccess:Error:)]) {
            // 在获取到动态列表的数据请求之后将请求结果返回给vc
            [self.delegate responseSearchListRequestWithSuccess:success Error:error];
        }
    }
}

- (void)callBackWithLikeUpResuest:(BOOL)success Error:(NSString *)error{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithLikeUpRequestSuccess:Error:)]) {
            // 在获取到动态列表的数据请求之后将请求结果返回给vc
            [self.delegate responseWithLikeUpRequestSuccess:success Error:error];
        }
    }
}


@end
