//
//  ICN_SearchDynamicListViewModel.h
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_DynStateContentModel;

@protocol SearchDynamicStatesListDelegate <NSObject>

/** 根据字段或者是热词内容获取到网络请求并回调 */
- (void)responseSearchListRequestWithSuccess:(BOOL)success Error:(NSString *)error;

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success Error:(NSString *)error;

@end

@interface ICN_SearchDynamicListViewModel : NSObject

@property (nonatomic , weak)id<SearchDynamicStatesListDelegate> delegate;

@property (nonatomic , strong)NSMutableArray<ICN_DynStateContentModel *> *modelsArr;

@property (nonatomic , assign , getter=isWisdom)BOOL wisdomType; // 是否是智讯

/** 根据搜索内容获取结果 */
- (void)requestWithSearchContent:(NSString *)content Load:(BOOL)isLoad Refresh:(BOOL)isRefresh;

// 点赞功能
- (void)likeUpWithType:(NSInteger)type
                 Model:(ICN_DynStateContentModel *)model;



@end
