//
//  ICN_SearchPagerViewModel.h
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_HotWorldsModel;

@protocol SearchPagerDataSource <NSObject>

- (void)responseWithHotCellsRequest:(BOOL)success Error:(NSString *)error;

@end


@interface ICN_SearchPagerViewModel : NSObject

@property (nonatomic , weak)id<SearchPagerDataSource> delegate;
@property (nonatomic , strong)ICN_HotWorldsModel *model;


- (void)refreshCurrentPageContentCellsWithType:(NSInteger)type;




@end
