//
//  ICN_SearchPagerViewModel.m
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SearchPagerViewModel.h"
#import "ICN_SearchPagerHeader.h"
#import "HRNetworkingManager+SearchFirstPager.h"
#import "ICN_HotWorldsModel.h"

@interface ICN_SearchPagerViewModel ()


@end

@implementation ICN_SearchPagerViewModel


- (void)refreshCurrentPageContentCellsWithType:(NSInteger)type{
    [HRNetworkingManager requestHotWorldsWithHotType:type Success:^(ICN_HotWorldsModel *model) {
        
        [self callBackWithHotCellRequestWithModel:model Code:model.code Info:model.info];
        
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithHotCellRequestWithModel:nil Code:1 Info:@"网络获取失败"];
    }];
}
#pragma mark - ---------- 私有方法 ----------

// 用于计算Cell高度和行数的方法
- (void)configHotCellDetialHeightAndRowCountWithModel:(ICN_HotWorldsModel *)model{
    CGSize size = ICN_SearchPagerHotWorldItemSize;
    NSInteger longth = ((NSInteger)SCREEN_WIDTH - 10) % ((NSInteger)size.width + 15);
    model.rowCount = (SCREEN_WIDTH - 10) / (size.width + 15.0);
    if (longth > (NSInteger)size.height) {
        model.rowCount ++;
    }
    NSInteger count = model.modelsArr.count / model.rowCount;
    if (model.modelsArr.count % model.rowCount != 0) {
        count ++;
    }
    model.cellHeight = (15 * 2) + (size.height + 10.0) * count - 10.0;
}

- (void)callBackWithHotCellRequestWithModel:(ICN_HotWorldsModel *)model Code:(NSInteger)code Info:(NSString *)info{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithHotCellsRequest:Error:)]) {
            if (code == 0) {
                self.model = model;
                [self configHotCellDetialHeightAndRowCountWithModel:self.model];
                [self.delegate responseWithHotCellsRequest:YES Error:nil];
            }else{
                [self.delegate responseWithHotCellsRequest:NO Error:info];
            }
        }
    }
}





@end
