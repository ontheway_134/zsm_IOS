//
//  ICN_DynSearchPagerVCViewController.m
//  ICan
//
//  Created by albert on 2016/12/4.
//  Copyright © 2016年 albert. All rights reserved.
//  常量按钮的宽度是53.0 高度40.0

#import "ICN_DynSearchPagerVC.h"
#import "ICN_SearchPagerViewModel.h"
#import "ICN_HotWorldsCell.h"
#import "ICN_HotWorldsModel.h"
#import "YKJLocalSave.h" // 本地存储类
#import "ICN_SearchPagerHeader.h" // 搜索的总体头文件

#pragma mark - ---------- 搜索内容页面头文件 ----------
#import "ICN_PositionCell.h" // 职位Cell
#import "ICN_FirendListCell.h" // 好友Cell
#import "ICN_GroupListCell.h" // 通用小组Cell
#import "ICN_CommonActivityItemCell.h" // 通用活动Cell
#import "ICN_CommonPsersonDynamicCell.h" // 个人动态Cell
#import "ICN_ComSingleTitleSectionView.h" // 标题头文件

#pragma mark - ---------- 跳转页面头文件 ----------
#import "ICN_DynSearchDetialVC.h" // 动态搜索详情页面
//#import "ICN_MyfridentViewController.h" // 联系人搜索详情页
#import "ICN_SeekPeopleViewController.h" // 搜索好友页面
#import "ICN_ProfessionSearchViewController.h" // 职场搜索详情页面
#import "ICN_ActivityMultSearchViewController.h" // 活动、职业规划、行业资讯的搜索页面
#import "ICN_GroupSearchDetialVC.h" // 小组搜索页面
#import "ICN_SearchComplainListPager.h" // 吐槽的搜索页面
#import "ICN_SearchQuestionListPager.h" // 提问的搜索页面


static NSString * SearchMaduleName = @"FirstPageSearch"; // 搜索模块名
static NSString * SearchPathName = @"search"; // 搜索历史路径名

@interface ICN_DynSearchPagerVC ()<SearchPagerDataSource , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 当前页面展示的tableview

@property (weak, nonatomic) IBOutlet UITextField *searchTextF; // 搜索文本框属性

@property (weak, nonatomic) IBOutlet UIButton *clearHisBtn; // 清除历史按钮

@property (weak, nonatomic) IBOutlet UIView *selectedChangeBtnBackGround; // 切换选项按钮背景板

@property (weak, nonatomic) IBOutlet UIScrollView *buttonScroller; // 按钮轮播视图

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewHeightConstrains; // tableView的高度约束




#pragma mark - ---------- 私有属性 ----------
@property (nonatomic , strong)ICN_SearchPagerViewModel *viewModel; // 用于计算和网络请求的ViewModel
@property (nonatomic , assign)NSInteger p_HotWorldsRows; // 热词行数
@property (nonatomic , assign)NSInteger p_HotWorldRowCount; // 单行热词数量
@property (nonatomic , assign)CGFloat p_HotWorldsCellHeight; // 热词Cell高度
@property (nonatomic , assign)NSInteger currentBtnType; // 当前选中按钮的宏类型
@property (nonatomic , strong)UIButton *hisSelectBtn; // 上一次选中的热词按钮
@property (nonatomic , strong)UIView *stateView; // 选中按钮上的分隔栏
@property (nonatomic , strong)NSMutableArray <NSMutableArray *>* hisWordsArr; // 历史记录字段
@property (nonatomic , strong)NSMutableSet <NSString *>*hisWorldSet; // 历史数据集合字段(不会重复)


@end

@implementation ICN_DynSearchPagerVC


#pragma mark - ---------- 懒加载 ----------

- (NSMutableSet<NSString *> *)hisWorldSet{
    if (_hisWorldSet == nil) {
        _hisWorldSet = [NSMutableSet set];
    }
    return _hisWorldSet;
}

- (NSMutableArray<NSMutableArray *> *)hisWordsArr{
    if (_hisWordsArr == nil) {
        _hisWordsArr = [NSMutableArray array];
    }
    return _hisWordsArr;
}

- (ICN_SearchPagerViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_SearchPagerViewModel alloc] init];
    }
    return _viewModel;
}

- (UIView *)stateView{
    if (_stateView == nil) {
        _stateView = [[UIView alloc] initWithFrame:CGRectZero];
        _stateView.backgroundColor = RGB0X(0x009dff);
    }
    return _stateView;
}

#pragma mark --- 初始化方法 ---
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // 默认设置的索引是找人
        _indexTag = Search_People;
    }
    return self;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentBtnType = Search_People; // 设置默认的选中按钮是找人
    self.viewModel.delegate = self;
    // 获取历史数据的相关方法
    [self configHisSearchSetFromDataBase];
    [self configPropertyWhileViewDidLoad];
    [self configTableViewWhileViewDidLoad];
    // 在scroller上配置按钮的方法
    [self configButtonsListOnScroller];
    [self setHiddenDefaultNavBar:YES];
    // 调节搜索按钮位置
    
    if (self.indexTag == Search_WisdomState) {
        // 点击到智讯则滑到最右端
        [self.buttonScroller setContentOffset:CGPointMake(self.buttonScroller.contentSize.width - self.buttonScroller.width, self.buttonScroller.contentOffset.y) animated:YES];
    }
    if (self.indexTag == Search_Activity) {
        // 点击活动则跳转到最左端
        [self.buttonScroller setContentOffset:CGPointMake(0, self.buttonScroller.contentOffset.y) animated:YES];
    }
    // 加载热词数据
    [self.viewModel refreshCurrentPageContentCellsWithType:self.currentBtnType - Search_People + 1];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.hiddenDefaultNavBar = YES;
    // 每次刷新页面之后重新再数据库中获取历史搜索数据
    [self configHisSearchSetFromDataBase];
    if (self.viewModel.model) {
        [self changeTableViewHeightWithModel:self.viewModel.model];
    }
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 设置初始出现的状态分隔栏
    if (_stateView == nil) {
        [self.buttonScroller addSubview:self.stateView];
    }
    CGRect frame = CGRectMake(self.hisSelectBtn.centerX - 14.5, self.buttonScroller.height - 1.0, 29.0, 1.0);
    self.stateView.frame = frame;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // 在视图将要消失的时候将历史内容存储到本地 -- 并清空历史数据数组的内容
    [self saveCurrentPagerSearchHisSetToDataBase];
    if (_hisWordsArr) {
        [_hisWordsArr removeAllObjects];
    }
    if (_hisWorldSet) {
        [_hisWorldSet removeAllObjects];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 在scroller上面配置按钮的方法
- (void)configButtonsListOnScroller{
    NSMutableArray *buttonList = [NSMutableArray array];
    
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_People WithTitle:@"找人"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_WorkJob WithTitle:@"职位"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_DynamicState WithTitle:@"动态"]];
    // 如果是企业用户则屏蔽掉 小组和活动按钮
    if (![self isCurrentEnterpriseUser]) {
        [buttonList addObject:[self configCommonSearchBtnWithTag:Search_Group WithTitle:@"小组"]];
        [buttonList addObject:[self configCommonSearchBtnWithTag:Search_Activity WithTitle:@"活动"]];
    }
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_WisdomState WithTitle:@"智讯"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_ComplainState WithTitle:@"吐槽"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_QuestionState WithTitle:@"提问"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_CareerPlan WithTitle:@"职场规划"]];
    [buttonList addObject:[self configCommonSearchBtnWithTag:Search_InsdurtyInformation WithTitle:@"行业资讯"]];
    
    // 设置默认的选中按钮和选中tag
    self.currentBtnType = self.indexTag;
    self.hisSelectBtn = buttonList[self.indexTag - Search_People];
    
    // 将数组中的按钮动态添加在scroll上
    CGFloat maxWidth = 53.0 * (buttonList.count + 1);
    if (self.buttonScroller.width < maxWidth) {
        self.buttonScroller.contentSize = CGSizeMake(maxWidth, self.buttonScroller.height);
    }
    
    // 将数组中的按钮添加在scroll上
    for (NSInteger i = 0 ; i < buttonList.count ; i++) {
        UIButton *button = buttonList[i];
        [self.buttonScroller addSubview:button];
        // 对每个按钮进行约束布局
        [button mas_remakeConstraints:^(MASConstraintMaker *make) {
            // 针对button是第一的做特殊处理
            if (i == 0) {
                make.left.equalTo(self.buttonScroller);
            }else{
                make.left.equalTo([buttonList[i - 1] mas_right]);
            }
            make.top.equalTo(self.buttonScroller);
            if (i >= 8) {
                make.width.mas_equalTo(73.0);
            }else{
                make.width.mas_equalTo(53.0);
            }
            make.height.mas_equalTo(41.0);
        }];
    }
}

- (UIButton *)configCommonSearchBtnWithTag:(NSInteger)tag WithTitle:(NSString *)title{
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 53.0, 40.0)];
    // 设置按钮的自适应
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.minimumScaleFactor = 0.5;
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:RGB0X(0x009dff) forState:UIControlStateSelected];
    [button addTarget:self action:@selector(clickOnChangeHotWorldsAction:) forControlEvents:UIControlEventTouchUpInside];
    // 设置按钮的tag代表按钮的选种类型
    button.tag = tag;
    
    return button;
}

// 从数据库中获取历史并分类的方法
- (void)configHisSearchSetFromDataBase{
    // 在数据库中获取到代理索引标识的全部搜索历史词字段 并存储在set中
    NSMutableArray *array = [NSMutableArray arrayWithArray:[YKJLocalSave getLocalDataWithModuleName:SearchMaduleName pahtName:SearchPathName]];
    [self.hisWorldSet addObjectsFromArray:array];
    if (_hisWordsArr) {
        [_hisWordsArr removeAllObjects];
    }
    // 将搜索的类型tag添加到数据库的搜索索引中
    for (NSInteger i = Search_People; i <= Search_InsdurtyInformation; i++) {
        NSMutableArray *itemArr = [NSMutableArray array];
        [self.hisWordsArr addObject:itemArr];
        for (NSString *content in self.hisWorldSet) {
            if (![content isKindOfClass:[NSString class]]) {
                continue;
            }
            NSString *text = [self getSearchContentByHisContent:content];
            NSInteger index = [self getContentButtonTypeWithContent:content];
            if (index == i) {
                [itemArr addObject:text];
            }
        }
    }
}

// 将当前页面的全部搜索历史保存在数据库中
- (void)saveCurrentPagerSearchHisSetToDataBase{
    NSArray *result = self.hisWorldSet.allObjects;
    [YKJLocalSave saveDataLocalWithModuleName:SearchMaduleName pahtName:SearchPathName ContentData:result];
}

// 清空指定模块的搜索历史
- (void)clearCurrentPageSearchDataBaseWithIndex:(NSInteger)index{
    // 获取到需要移除的数组
    NSMutableArray *removeArr = self.hisWordsArr[index - Search_People] ? self.hisWordsArr[index - Search_People] : [NSMutableArray array];
    // 在 hisWordsArr 中重置对应索引的数组
    self.hisWordsArr[index - Search_People] = [NSMutableArray array];
    // 在 set 中清空需要移除的数据
    for (NSString *content in removeArr) {
        if ([content isKindOfClass:[NSString class]]) {
            // 判断content是否包含在 set 中，包含则移除
            NSString *compentContent = SF(@"%ld,%@",index , content);
            if ([self.hisWorldSet containsObject:compentContent]) {
                [self.hisWorldSet removeObject:compentContent];
            }
        }
    }
    // 重新保存需要保存的内容
    NSArray *saveArr = self.hisWorldSet.allObjects ? self.hisWorldSet.allObjects : [NSArray array];
    [YKJLocalSave saveDataLocalWithModuleName:SearchMaduleName pahtName:SearchPathName ContentData:saveArr];
    // 最后进行数据刷新
    [self.tableView reloadData];
    [self changeTableViewHeightWithModel:self.viewModel.model];
}

// 通过数据库字段获取历史搜索记录的方法
- (NSString *)getSearchContentByHisContent:(NSString *)content{
    NSString *text;
    if ([content componentsSeparatedByString:@","].count > 2) {
        // 如果之前的历史搜索词中有逗号，将第一部分（搜索索引截掉 -- 获取到真实搜索数据）
        NSArray *wordArr = [content componentsSeparatedByString:@","];
        NSMutableArray *array = [NSMutableArray arrayWithArray:wordArr];
        [array removeObjectAtIndex:0];
        text = [array componentsJoinedByString:@","];
    }else{
        text = [content componentsSeparatedByString:@","].lastObject;
    }
    return text;
}

// 获取数据库字段对应的按钮编号
- (NSInteger)getContentButtonTypeWithContent:(NSString *)content{
    if (content) {
        if ([[[content componentsSeparatedByString:@","] firstObject] integerValue]) {
            return [[[content componentsSeparatedByString:@","] firstObject] integerValue];
        }else{
            return -1;
        }
    }
    return -1;
}

// 配置默认属性
- (void)configPropertyWhileViewDidLoad {
    // 设置搜索框相关
    self.searchTextF.delegate = self;
}

// 配置tableView - 刷新只用于获取热词
- (void)configTableViewWhileViewDidLoad {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // 注册热词Cell
    [self.tableView registerClass:[ICN_HotWorldsCell class] forCellReuseIdentifier:SF(@"%@" , ICN_HotWorldCellStr)];
    //注册搜索历史Cell
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:NSStringFromClass([UITableViewCell class])];
    // 注册标题section
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = RGB0X(0xe5e5e5);
    
    // 禁止tableview滑动
    self.tableView.scrollEnabled = NO;
}

// 修改tableview的高度
- (void)changeTableViewHeightWithModel:(ICN_HotWorldsModel *)model{
    CGFloat realHeight = (36.0 * 2) + model.cellHeight + self.hisWordsArr[self.currentBtnType - Search_People].count * 40 ;
    if (realHeight < 444) {
        self.tableviewHeightConstrains.constant = realHeight;
    }else{
        self.tableviewHeightConstrains.constant = 444.0;
    }
}

// 根据当前选中的类型编码确认跳转的详情页面
- (void)jumpToSearchDetialPagerWith:(NSString *)hotWordsId SelectContent:(NSString *)selectContent{
    
    switch (self.currentBtnType) {
        case Search_People:{
            // 跳转到好友搜索
            ICN_SeekPeopleViewController *pager = [[ICN_SeekPeopleViewController alloc] init];
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_WorkJob:{
            // 跳转到职位搜索
            ICN_ProfessionSearchViewController *pager = [[ICN_ProfessionSearchViewController alloc] init];
            pager.searchContent = selectContent;
            pager.typeStr = @"0";
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_DynamicState:
        case Search_WisdomState:{
            // 跳转到动态搜索页面
            ICN_DynSearchDetialVC *pager = [[ICN_DynSearchDetialVC alloc] init];
            if (self.currentBtnType == Search_WisdomState) {
                // 智讯模式
                pager.wisdomType = YES;
            }else{
                pager.wisdomType = NO;
            }
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case Search_Group:{
            // 跳转到小组搜索页面
            ICN_GroupSearchDetialVC *pager = [[ICN_GroupSearchDetialVC alloc] init];
            pager.content = selectContent;
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case Search_ComplainState:{
            // 跳转到吐槽的搜索页面
            ICN_SearchComplainListPager *pager = [[ICN_SearchComplainListPager alloc] init];
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_QuestionState:{
            // 跳转到提问的搜索页面
            ICN_SearchQuestionListPager *pager = [[ICN_SearchQuestionListPager alloc] init];
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_Activity:{
            // 搜索活动的详情
            ICN_ActivityMultSearchViewController *pager = [[ICN_ActivityMultSearchViewController alloc] init];
            pager.searchContent = selectContent;
            pager.type = 0;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_CareerPlan:{
            // 搜索职业规划的详情
            ICN_ActivityMultSearchViewController *pager = [[ICN_ActivityMultSearchViewController alloc] init];
            pager.type = 1;
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case Search_InsdurtyInformation:{
            // 搜索行业资讯的详情
            ICN_ActivityMultSearchViewController *pager = [[ICN_ActivityMultSearchViewController alloc] init];
            pager.type = 2;
            pager.searchContent = selectContent;
            [self currentPagerJumpToPager:pager];
            break;
        }
        default:
            break;
    }

}

#pragma mark --- 搜索历史逻辑方法 ---

- (void)saveNewSearchWorldWithSearch:(NSString *)search{
    // 获取到set的全部数据
    NSInteger setTotal = self.hisWorldSet.count;
    // 在set中添加新的搜索词
    [self.hisWorldSet addObject:SF(@"%ld,%@",(long)self.currentBtnType , search)];
    // 如果set的总数增加说明之前没有这个搜索词则在历史搜索列表数组中添加这个词
    if (self.hisWorldSet.count != setTotal) {
        [self.hisWordsArr[self.currentBtnType - Search_People] addObject:SF(@"%ld,%@",(long)self.currentBtnType , self.searchTextF.text)];
    }
}


#pragma mark - ---------- IBAction ----------

- (IBAction)clearHistorySearchWordAction:(UIButton *)sender {
    
    [self clearCurrentPageSearchDataBaseWithIndex:self.currentBtnType];
    [self.tableView reloadData];
    [self changeTableViewHeightWithModel:self.viewModel.model];
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"清除完毕"];
}

- (IBAction)clickOnSearchBtnAction:(UIButton *)sender {
    // 调用显示加的搜索内容数据
    [self.searchTextF resignFirstResponder];
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""] && ![self.searchTextF.text isEqualToString:@"搜索关键字"]) {
        // -- 将新的搜索词保存（如果存在历史搜索词则不刷新，如果不存在则刷新）
        [self saveNewSearchWorldWithSearch:self.searchTextF.text];
        // 根据搜索内容跳转到对应页面
        [self jumpToSearchDetialPagerWith:nil SelectContent:self.searchTextF.text];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
    }

}



- (IBAction)navgationBackAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)clickOnChangeHotWorldsAction:(UIButton *)sender {
    
    [self.hisSelectBtn setTitleColor:RGB0X(0x000000) forState:UIControlStateNormal];
    [sender setTitleColor:RGB0X(0x009dff) forState:UIControlStateNormal];
    [self.stateView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.hisSelectBtn.mas_bottom);
        make.centerX.equalTo(sender);
        make.size.mas_equalTo(CGSizeMake(29.0, 1));
    }];

    self.hisSelectBtn = sender;
    // 获取到tag并根据tag刷新
    self.currentBtnType = sender.tag;
    // 根据tag刷新tableview
    [self changeTableViewHeightWithModel:self.viewModel.model];
    [self.viewModel refreshCurrentPageContentCellsWithType:self.currentBtnType - Search_People + 1];
//    [self.tableView reloadData];
//    [self.tableView.mj_header beginRefreshing];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // 调节位置
        if (sender.tag == Search_WisdomState) {
            // 点击到智讯则滑到最右端
            [self.buttonScroller setContentOffset:CGPointMake(self.buttonScroller.contentSize.width - self.buttonScroller.width, self.buttonScroller.contentOffset.y) animated:YES];
        }
        if (sender.tag == Search_Activity) {
            // 点击活动则跳转到最左端
            [self.buttonScroller setContentOffset:CGPointMake(0, self.buttonScroller.contentOffset.y) animated:YES];
        }

    });
}





#pragma mark - ---------- 代理 ----------

#pragma mark --- SearchPagerDataSource ---

- (void)responseWithHotCellsRequest:(BOOL)success Error:(NSString *)error{
    if (success) {
        [self changeTableViewHeightWithModel:self.viewModel.model];
        [self.tableView reloadData];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    // 调用显示加的搜索内容数据
    [self.searchTextF resignFirstResponder];
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""] && ![self.searchTextF.text isEqualToString:@"搜索关键字"]) {
        // -- 将新的搜索词保存（如果存在历史搜索词则不刷新，如果不存在则刷新）
        [self saveNewSearchWorldWithSearch:self.searchTextF.text];
        // 根据搜索内容跳转到对应的详细搜索页面
        [self jumpToSearchDetialPagerWith:nil SelectContent:self.searchTextF.text];
    }

    return YES;
}


#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    if (indexPath.section == 1) {
        // 点击搜索历史按钮
        NSString *content = self.hisWordsArr[self.currentBtnType - Search_People][indexPath.row];
        if (content != nil && ![content isEqualToString:@""]) {
            [self jumpToSearchDetialPagerWith:nil SelectContent:content];
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        // 返回搜索到的历史数据的Cell数量
        return self.hisWordsArr[self.currentBtnType - Search_People].count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        ICN_HotWorldsCell *cell = [tableView dequeueReusableCellWithIdentifier:SF(@"%@" , ICN_HotWorldCellStr)];
        if (self.viewModel.model) {
            cell.model = self.viewModel.model;
        }
        [cell callBackWithBtnClickAction:^(ICN_HotWorldsModel *model) {
            // -- 将新的搜索词保存（如果存在历史搜索词则不刷新，如果不存在则刷新）
            [self saveNewSearchWorldWithSearch:cell.model.selectContent];
            // 根据热词内容跳转到详细的搜索页面
            [self jumpToSearchDetialPagerWith:nil SelectContent:model.selectContent];
        }];
        return cell;
    }else if(indexPath.section == 1){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UITableViewCell class])];
        // 设置Cell的点击样式
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.hisWordsArr[indexPath.row]) {
            cell.textLabel.text = self.hisWordsArr[self.currentBtnType - Search_People][indexPath.row];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:12.0];
        cell.textLabel.textColor = RGB0X(0x666666);
        [cell.textLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(cell.contentView).offset(10.0);
            make.top.equalTo(cell.contentView.mas_top).offset(15.0);
        }];
        return cell;
    }
    return nil;//王俊凯
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ICN_ComSingleTitleSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    if (section == 0) {
        view.headerTitle = @"热门搜索";
    }else{
        view.headerTitle = @"搜索历史";
    }
    return view;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (self.viewModel.model) {
            return self.viewModel.model.cellHeight;
        }else{
            return 90.0;
        }
    }else{
        return 40.0;
    }
    return 0;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 36.0;
}


@end
