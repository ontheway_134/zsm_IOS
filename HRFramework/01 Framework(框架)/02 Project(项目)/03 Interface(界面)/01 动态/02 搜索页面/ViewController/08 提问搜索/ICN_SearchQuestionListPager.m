//
//  ICN_SearchQuestionListPager.m
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SearchQuestionListPager.h"
#import "ICN_QuestionContentCell.h" // 提问的列表Cell
#import "ICN_ComSingleTitleSectionView.h" // 小组的分组sectionView
#import "ICN_QuestionViewModel.h"// 提问需要的ViewModel
#import "ICN_QuestionListModel.h"
#import "ICN_AnswerListModel.h"
#import "ICN_MyQuestionDetialPager.h"
#import "ICN_OthersQuestionDetialPager.h"
#import "ICN_TransmitToDynamicPager.h"          // 转发到动态页面
#import "ICN_MYAnswerQuestionPager.h" // 我来回答页面
#import "ICN_UserHomePagerVC.h" // 用户主页

#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model


static NSString *const Private_WarnText = @"请添加搜索内容";

@interface ICN_SearchQuestionListPager ()<UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate , QuestionConfigurationDelegate , ICN_DynWarnViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextF; // 搜索文本框

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 数据表格
@property (nonatomic , strong)ICN_QuestionViewModel *viewModel;
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作

@end

@implementation ICN_SearchQuestionListPager

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.origin.y -= 64.0;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏默认导航栏
    [self setHiddenDefaultNavBar:YES];
    if (self.searchContent) {
        self.searchTextF.text = self.searchContent;
    }
    // 配置tableview
    [self configTableview];
    // 设置textF的代理
    self.searchTextF.delegate = self;
    
    // 设置ViewModel
    self.viewModel = [[ICN_QuestionViewModel alloc] init];
    self.viewModel.delegate = self;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 刷新提问列表操作
            [self.viewModel requestQuestionListWithType:Question_ResponseSearchList isLoad:NO MemberId:nil SearchContent:SF(@"%@",self.searchTextF.text)];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        
        // 加载提问列表操作
        [self.viewModel requestQuestionListWithType:Question_ResponseSearchList isLoad:YES MemberId:nil SearchContent:SF(@"%@",self.searchTextF.text)];
        
        
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;

    
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


- (void)configTableview{
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    // 注册Cell
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_QuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = RGB0X(0xf0f0f0);
    // 在使用group的时候设置的确保section间隔取消的方法
    _tableView.sectionFooterHeight = 1;
    // 配置tableview不显示分割框
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableView.sectionHeaderHeight = 0.01;
    self.tableView.sectionFooterHeight = 0.01;
    [self.view addSubview:_tableView];
    [self configTableViewRefreshHeaderFooterView];
}

// 根据搜索内容进行校验的方法
- (BOOL)judgeOnSearchText{
    // 判断在不符合搜索要求的时候不搜索
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:Private_WarnText]) {
        // 有搜索内容可以搜索
        [self.viewModel requestQuestionListWithType:Question_ResponseSearchList isLoad:NO MemberId:nil SearchContent:self.searchTextF.text];
        return YES;
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:Private_WarnText];
        self.searchTextF.text = Private_WarnText;
        self.searchTextF.textColor = [UIColor redColor];
    }
    return NO;
    
}


#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnNavBackBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)clickOnNavSearchBtnAction:(UIButton *)sender {
    
    if ([self judgeOnSearchText]) {
        // 添加搜索相关的操作
        self.tableView.hidden = NO;
        [self.tableView.mj_header beginRefreshing];
    }
}

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}

#pragma mark --- 提问代理 ---
- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Question_ResponseSearchList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 判断如果当前页面还有数据则不隐藏tableview
                if (self.viewModel.modelsArr.count == 0) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                    self.tableView.hidden = YES;
                }
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}



#pragma mark --- TextField代理 ---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.text != nil && [textField.text isEqualToString:Private_WarnText]) {
        textField.text = @"";
    }
    textField.textColor = [UIColor blackColor];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self judgeOnSearchText]) {
        // 添加搜索相关的操作
    }
    return YES;
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    // 获取到需要的Model
    ICN_QuestionListModel *model = self.viewModel.modelsArr[indexPath.row];
    if ([model.isMine integerValue] == 0) {
        // 是我发布的
        ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
        pager.constentId = model.questionId;
        [self currentPagerJumpToPager:pager];
    }else{
        // 不是我发布的
        ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
        pager.contentId = model.questionId;
        [self currentPagerJumpToPager:pager];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_QuestionContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
    // 添加根据当前的用户状态判断是否显示功能按钮（针对企业用户）
    cell.EnterpriseUser = [self isCurrentEnterpriseUser];
    cell.firstPager = YES;
    cell.model = self.viewModel.modelsArr[indexPath.row];
    
    // 点击头像跳转到用户主页的逻辑
    [cell callBackQuestionIconTapBlock01:^(ICN_QuestionListModel *model) {
            //相关逻辑 - 根据用户角色判断跳转的是用户主页还是公司详情
            ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
            pager.memberId = model.memberId;
            [self currentPagerJumpToPager:pager];
    }];
    
    [cell callBackCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isSelected) {
        switch (buttonType) {
                // 收藏 / 取消收藏操作
            case ICN_CellCollectType:{
                if (isSelected) {
                    [self.viewModel requestQuestionFunctionWithType:Question_ResponseLikeUp QuestionId:model.questionId Content:nil AnswerId:nil];
                }else{
                    [self.viewModel requestQuestionFunctionWithType:Question_ResponseDisLikeUp QuestionId:model.questionId Content:nil AnswerId:nil];
                }
                break ;
            }
                // 转发操作
            case ICN_CellReplayType:{
                //warn=== 提问的转发接口不存在需要修改
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_QuestionReplay ParamsKey:@"questionId" ModelId:model.questionId Title:@"i行提问" IconUrl:model.memberLogo Content:model.contant];
                // 2. 弹出转发窗口
                [self.view addSubview:self.warnView];
                break ;
            }
                // 回答操作 == 跳转到详情页面
            case ICN_CellAnswerType:{
                // 需要判断是跳转到别人的提问还是自己的提问
                if ([model.isMine integerValue] == 0) {
                    // 是我发布的
                    ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                    pager.constentId = model.questionId;
                }else{
                    // 别人发布的
                    ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                    pager.contentId = model.questionId;
                    [self currentPagerJumpToPager:pager];
                }
            }
            default:{
                break;
            }
        }
    }];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ICN_ComSingleTitleSectionView *view = (ICN_ComSingleTitleSectionView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    view.headerTitle = @"提问";
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0;
}



@end
