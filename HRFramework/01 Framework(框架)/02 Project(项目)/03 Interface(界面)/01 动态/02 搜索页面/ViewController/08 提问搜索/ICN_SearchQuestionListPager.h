//
//  ICN_SearchQuestionListPager.h
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_SearchQuestionListPager : BaseViewController

@property (nonatomic , copy)NSString *searchContent; // 搜索内容 -- 为空的时候显示holder

@end
