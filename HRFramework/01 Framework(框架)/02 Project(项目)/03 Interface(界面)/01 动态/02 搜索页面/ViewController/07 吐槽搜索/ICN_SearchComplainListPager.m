//
//  ICN_SearchComplainListPager.m
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SearchComplainListPager.h"
#import "ICN_ComplainContentCell.h"             // 吐槽列表Cell
#import "ICN_ComSingleTitleSectionView.h" // 小组的分组sectionView
#import "ICN_ComplainListViewModel.h"           // 吐槽的ViewModel
#import "ICN_ComplainListModel.h"               // 吐槽列表Model
#import "ICN_ComplainDetialPager.h"             // 跳转到吐槽详情页面
#import "ICN_TransmitToDynamicPager.h"          // 转发到动态页面

#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model

static NSString *const Private_WarnText = @"请添加搜索内容";

@interface ICN_SearchComplainListPager ()<UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate , ComplainListDelegate , ICN_DynWarnViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextF; // 搜索文本框

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 数据表格

@property (nonatomic , strong)ICN_ComplainListViewModel *viewModel; // 数据处理Model

@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作


@end

@implementation ICN_SearchComplainListPager

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏默认导航栏
    [self setHiddenDefaultNavBar:YES];
    if (self.searchContent) {
        self.searchTextF.text = self.searchContent;
    }
    // 配置tableview
    [self configTableview];
    // 设置ViewModel
    self.viewModel = [[ICN_ComplainListViewModel alloc] init];
    self.viewModel.delegate = self;
    // 设置textF的代理
    self.searchTextF.delegate = self;
    [self configTableViewRefreshHeaderFooterView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}


- (void)configTableview{
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    // 注册Cell
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
    _tableView.backgroundView = nil;
    _tableView.backgroundColor = RGB0X(0xf0f0f0);
    // 在使用group的时候设置的确保section间隔取消的方法
    _tableView.sectionFooterHeight = 1;
    // 配置tableview不显示分割框
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableView.sectionHeaderHeight = 0.01;
    self.tableView.sectionFooterHeight = 0.01;
    [self.view addSubview:_tableView];
}

// 根据搜索内容进行校验的方法
- (BOOL)judgeOnSearchText{
    // 判断在不符合搜索要求的时候不搜索
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:Private_WarnText]) {
        // 有搜索内容可以搜索
        return YES;
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:Private_WarnText];
        self.searchTextF.text = Private_WarnText;
        self.searchTextF.textColor = [UIColor redColor];
    }
    return NO;

}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestComplainListWithType:Complain_ResponseSearchList isLoad:NO MemberId:nil SearchContent:self.searchTextF.text];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestComplainListWithType:Complain_ResponseSearchList isLoad:YES MemberId:nil SearchContent:self.searchTextF.text];
        
        
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnNavBackBtnAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)clickOnNavSearchBtnAction:(UIButton *)sender {
    
    if ([self judgeOnSearchText]) {
        // 添加搜索相关的操作
        self.tableView.hidden = NO;
        [self.tableView.mj_header beginRefreshing];
    }
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}



#pragma mark - ---------- 吐槽代理 ----------

- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    
    [self endRefreshWithTableView:self.tableView];
    [self.searchTextF resignFirstResponder];
    
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseDisLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseSearchList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 判断在没有历史数据的时候在显示数据异常
                if (self.viewModel.modelsArr.count == 0) {
                    self.tableView.hidden = YES;
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                }
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}


#pragma mark --- TextField代理 ---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.text != nil && [textField.text isEqualToString:Private_WarnText]) {
        textField.text = @"";
    }
    textField.textColor = [UIColor blackColor];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self judgeOnSearchText]) {
        // 添加搜索相关的操作
        self.tableView.hidden = NO;
        [self.viewModel requestComplainListWithType:Complain_ResponseSearchList isLoad:NO MemberId:nil SearchContent:self.searchTextF.text];
    }
    return YES;
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    // 跳转到详情页面
    ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
    pager.complainId = [self.viewModel.modelsArr[indexPath.row] complainId] ;
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 测试用添加10个
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_ComplainContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
    cell.model = self.viewModel.modelsArr[indexPath.row];
    // 添加回调block的交互
    [cell callBackWithCellBlock:^(ICN_ComplainListModel *model, NSInteger selectedType, BOOL isSelected) {
        switch (selectedType) {
                // 点赞 / 取消点赞操作
            case ICN_CellLikeActionBtnType:{if (isSelected) {
                [self.viewModel requestComplainFunctionWithType:Complain_ResponseLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
            }else{
                [self.viewModel requestComplainFunctionWithType:Complain_ResponseDisLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
            }
                break ;
            }
                // 评论操作
            case ICN_CellCommentBtnType:{
                // 跳转到详情页面
                ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
                pager.complainId = model.complainId;
                [self currentPagerJumpToPager:pager];
                break ;
            }
                // 转发操作
            case ICN_CellReplayType:{
                // 跳转到转发页面
                // 1. 生成转发Model
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:model.complainId Title:model.RandomName IconUrl:model.RandomPicture Content:model.contant];
                // 2. 弹出转发窗口
                [self.view addSubview:self.warnView];
                break ;
            }
                
            default:
                break;
        }
    }];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ICN_ComSingleTitleSectionView *view = (ICN_ComSingleTitleSectionView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    view.headerTitle = @"吐槽";
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0;
}






@end
