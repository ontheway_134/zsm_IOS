//
//  ICN_DynSearchDetialVC.h
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_DynSearchDetialVC : BaseViewController

@property (nonatomic , copy)NSString *searchContent; // 搜索内容

@property (nonatomic , assign , getter=isWisdom)BOOL wisdomType; // 是否是智讯


@end
