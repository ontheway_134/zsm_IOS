//
//  ICN_DynSearchDetialVC.m
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynSearchDetialVC.h"
#import "ICN_SearchDynamicListViewModel.h"
#import "ICN_UserDynamicStateDetialVC.h" // 用户动态详情Cell
#import "ICN_CommonPsersonDynamicCell.h" // 动态Cell
#import "ICN_ComSingleTitleSectionView.h" // 小组的分组sectionView
#import "ICN_DynStateContentModel.h" // 动态Model
#import "ICN_ReviewPublication.h" // 转发页面
#import "ICN_ApplyModel.h"
#import "ICN_ShareManager.h"
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model

@interface ICN_DynSearchDetialVC ()<UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate , SearchDynamicStatesListDelegate , ICN_DynWarnViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextF;

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic , strong)ICN_SearchDynamicListViewModel *viewModel;

@property (nonatomic , strong)ICN_DynStateContentModel *replyModel; // 用于记录转发的Model
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model

@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作

@property (nonatomic , assign , getter=isFooterLoading)BOOL mj_footerLoading; // 用于判断mj footer是否处于刷新状态的方法


@end

@implementation ICN_DynSearchDetialVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}


- (ICN_SearchDynamicListViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_SearchDynamicListViewModel alloc] init];
        _viewModel.delegate = self;
        // 设置ViewModel是动态模式还是智讯模式
        _viewModel.wisdomType = _wisdomType;
    }
    return _viewModel;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置默认状态下mj_footer没有刷新
    self.mj_footerLoading = NO;
    [self setHiddenDefaultNavBar:YES];
    [self viewModel];
    [self warnView];
    [self configSearchBarWhileViewWillAppear];
    [self configTableViewWhileViewWillAppear];
    [self configTableViewRefreshHeaderFooterView];
    if (self.searchContent) {
        self.searchTextF.text = self.searchContent;
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                // 动态和智讯类型还是之前的跳转操作
                ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
                pager.model = self.replyModel;
                [self currentPagerJumpToPager:pager];
                break;
            }
            default:
                break;
        }
    }
}

- (void)configTableViewWhileViewWillAppear{
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
    // 配置tableview不显示分割框
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    self.tableview.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableview.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1)];
    self.tableview.sectionHeaderHeight = 0.01;
    self.tableview.sectionFooterHeight = 0.01;
}

- (void)configSearchBarWhileViewWillAppear{
    self.searchTextF.delegate = self;
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self.viewModel requestWithSearchContent:self.searchContent Load:NO Refresh:YES];
        }
        // 在调用block之后的3秒钟调用函数回到主线程结束刷新
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 回到主线程结束刷新
            [self.tableview.mj_header endRefreshing];
        });
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableview.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 设定线程令牌 -- 只允许单线程访问
        @synchronized (self) {
            if (!self.isFooterLoading) {
                // 如果mj_footer没有刷新的话调用网络请求 -- 并在3秒钟之后结束刷新动画
                [self.viewModel requestWithSearchContent:self.searchContent Load:YES Refresh:NO];
                // 在调用block之后的1秒钟调用函数回到主线程结束刷新
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    // 回到主线程结束刷新
                    [self.tableview.mj_footer endRefreshing];
                    // 设置现在的底部刷新状态时 未处于刷新状态
                    self.mj_footerLoading = NO;
                });
            }
        }
    }];
    // 设置footer的标识 MJRefreshAutoNormalFooter
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableview.mj_footer = footer;
    if (self.searchContent) {
        [self.tableview.mj_header beginRefreshing];
    }
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}



#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnBackAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clockOnSearchBtnAction:(UIButton *)sender {
    // 调用显示加的搜索内容数据
    self.tableview.hidden = NO;
    [self.view endEditing:YES];
    [self.searchTextF resignFirstResponder];
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""] && ![self.searchTextF.text isEqualToString:@"搜索关键字"]) {
        [self.tableview.mj_header beginRefreshing];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
    }

}


#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text != nil && ![textField.text isEqualToString:@""] && ![textField.text isEqualToString:@"搜索关键字"]) {
        self.tableview.hidden = NO;
        self.searchContent = textField.text;
        [textField resignFirstResponder];
        [self.tableview.mj_header beginRefreshing];
    }
    return YES;
}


#pragma mark --- ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            // 判断是自己的动态不转发
            if (self.replyModel != nil && self.replyModel.isMe.integerValue == 1) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无法转发自己的动态"];
                return ;
            }
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}

#pragma mark --- SearchDynamicStatesListDelegate ---

- (void)responseWithLikeUpRequestSuccess:(BOOL)success Error:(NSString *)error{
    if (success) {
        [self.tableview reloadData];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseSearchListRequestWithSuccess:(BOOL)success Error:(NSString *)error{
    // 暂时设定列表页面的mj_Footer的结束刷新在GCD的延时方法中完成
//    [self endRefreshWithTableView:self.tableview];
    if (success) {
        if (self.viewModel.modelsArr.count > 0) {
            self.tableview.hidden = NO;
            [self.tableview reloadData];
        }else{
            self.tableview.hidden = YES;
        }
    }else{
        self.tableview.hidden = YES;
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
    
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 - 跳转到动态详情页面
    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
    pager.model = self.viewModel.modelsArr[indexPath.row];
    pager.model.matterId = pager.model.DynID;
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_CommonPsersonDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
    cell.model = self.viewModel.modelsArr[indexPath.row];
    // 点击Cell的下拉按钮
    // 添加对于Cell上点击事件的处理
    [cell callWhileCellBtnClick:^(NSInteger SenderTag , ICN_DynStateContentModel *model) {
        switch (SenderTag) {
            case ICN_CellLikeActionBtnType:{
                NSInteger likeType = 0;
                if (model.isPraise.integerValue == 0) {
                    likeType = 1;
                }else{
                    likeType = 2;
                }
                [self.viewModel likeUpWithType:likeType Model:model];
                break;
            }
            case ICN_CellCommentBtnType:{
                //相关逻辑 - 跳转到动态详情页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = self.viewModel.modelsArr[indexPath.row];
                pager.model.matterId = pager.model.DynID;
                [self currentPagerJumpToPager:pager];
                break;
            }
            case ICN_CellReviewBtnType:{
                // 转发
                // 1. 获取转发需要的两种Model
                self.replyModel = model;
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                // 2. 对于智讯的title进行处理
                if (model.title == nil) {
                    self.transmitModel.title = @"i行动态";
                }
                // 3. 展开转发页面
                [self.view addSubview:self.warnView];
                break;
            }
            case ICN_CellPullBtnType:{
                [self.tableview reloadData];
                break;
            }
            case ICN_CellReportBtnType:{
                // 执行举报按钮相关操作
                ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                pager.matterId = model.matterId;
                [self currentPagerJumpToPager:pager];
                break;
            }
            default:
                break;
        }
    }];
    [cell setListStyle:YES];
    return cell;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DynStateContentModel *model = self.viewModel.modelsArr[indexPath.row];
    
    if (model.contentSpread == NO) {
        if (model.imageFooterHeight > 0) {
            return 230 + model.imageFooterHeight;
        }
        if (model.contentSpreadHeight > 57.5) {
            return 230;
        }
    }
    
    if (model.imageFooterHeight > 0) {
        return 230 + model.imageFooterHeight + model.contentSpreadHeight - 57.5;
    }else{
        return 230 + model.contentSpreadHeight - 57.5;
    }
    
    return 230.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ICN_ComSingleTitleSectionView *view = (ICN_ComSingleTitleSectionView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    if (self.isWisdom) {
        view.headerTitle = @"智讯";
    }else{
        view.headerTitle = @"动态";
    }
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0;
}

@end
