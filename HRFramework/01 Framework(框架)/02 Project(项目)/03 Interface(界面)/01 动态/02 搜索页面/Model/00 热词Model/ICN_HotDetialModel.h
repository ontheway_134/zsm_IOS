//
//  ICN_HotDetialModel.h
//  ICan
//
//  Created by albert on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_HotDetialModel : BaseOptionalModel

@property (nonatomic , copy)NSString *hotId; // 热词ID
@property (nonatomic , copy)NSString *hotWord; // 热词内容


- initWithHotId:(NSString *)hotId
       HotWorld:(NSString *)hotWorld;


@end
