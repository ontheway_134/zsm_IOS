//
//  ICN_HotDetialModel.m
//  ICan
//
//  Created by albert on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_HotDetialModel.h"

@implementation ICN_HotDetialModel

- (id)initWithHotId:(NSString *)hotId HotWorld:(NSString *)hotWorld{
    self = [super init];
    if (self) {
        _hotId = hotId;
        _hotWord = hotWorld;
    }
    return self;
}

@end
