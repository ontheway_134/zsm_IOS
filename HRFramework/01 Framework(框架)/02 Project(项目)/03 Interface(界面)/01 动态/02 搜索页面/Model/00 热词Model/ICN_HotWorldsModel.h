//
//  ICN_HotWorldsModel.h
//  ICan
//
//  Created by albert on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
#import "ICN_HotDetialModel.h"

@interface ICN_HotWorldsModel : BaseOptionalModel

#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)NSString *selectContent; // 选中的热词内容
@property (nonatomic , assign)NSInteger rowCount; // 一行能够容纳的热词数量

@property (nonatomic , assign)CGFloat cellHeight; // 实际Cell的高度


@property (nonatomic , strong)NSMutableArray <ICN_HotDetialModel *> *modelsArr;


@end
