//
//  ICN_DynGrooupListModel.m
//  ICan
//
//  Created by albert on 2017/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DynGrooupListModel.h"

@implementation ICN_DynGrooupListModel

//groupId
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"groupId" : @"id"}];
}

@end
