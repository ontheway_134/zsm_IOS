//
//  ICN_DynGrooupListModel.h
//  ICan
//
//  Created by albert on 2017/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynGrooupListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *createTime;
@property (nonatomic , copy)NSString *creatorId;
@property (nonatomic , copy)NSString *dismissTime;
@property (nonatomic , copy)NSString *groupLogo;
@property (nonatomic , copy)NSString *groupName;
@property (nonatomic , copy)NSString *groupId;
@property (nonatomic , copy)NSString *isopen;
@property (nonatomic , copy)NSString *number;
@property (nonatomic , copy)NSString *status;
@property (nonatomic , copy)NSString *status_g;
@property (nonatomic , copy)NSString *summary;


@end
