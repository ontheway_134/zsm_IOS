//
//  HRNetworkingManager+SearchFirstPager.m
//  ICan
//
//  Created by albert on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager+SearchFirstPager.h"
#import "ICN_HotWorldsModel.h"
#import "ICN_DynStateContentModel.h"
#import "ToolAboutTime.h"

@implementation HRNetworkingManager (SearchFirstPager)

+ (void)requestHotWorldsWithHotType:(NSInteger)hotType Success:(hotWordsRequest)success Failure:(ErrorBlock)failure{
    NSDictionary *params = [NSDictionary dictionaryWithObject:SF(@"%ld",(long)hotType) forKey:@"hotType"];
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HotWorlds params:params success:^(id result) {
        ICN_HotWorldsModel *model = [[ICN_HotWorldsModel alloc] initWithDictionary:result error:nil];
        if (model.code == 0) {
            model.modelsArr = [NSMutableArray array];
            NSArray *array = [result valueForKey:@"result"];
            for (NSDictionary *item in array) {
                ICN_HotDetialModel *temp = [[ICN_HotDetialModel alloc] initWithDictionary:item error:nil];
                [model.modelsArr addObject:temp];
            }
        }
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestDynamicStateListWithSearchContent:(NSString *)search Page:(NSInteger)page Size:(NSInteger)size Success:(dynamicStateRequest)success Failure:(ErrorBlock)failure{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"search" : search ,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    if (size != 0) {
        [params setValue:SF(@"%ld",size) forKey:@"size"];
    }
    if (page != 0) {
        [params setValue:SF(@"%ld",page) forKey:@"page"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_SearchDynList params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:item error:nil];
                // 根据Model的相关属性做智讯还是动态的处理
                if (model.type.integerValue == 0) {
                    // 动态
                    model.widsom = NO;
                }else{
                    // 智讯
                    model.widsom = YES;
                    model.matterId = model.DynID;
                }
                model.contentSpread = NO;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [array addObject:model];
            }
        }else{
            ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:result error:nil];
            [array addObject:model];
        }
        success(array);
        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

    
}

@end
