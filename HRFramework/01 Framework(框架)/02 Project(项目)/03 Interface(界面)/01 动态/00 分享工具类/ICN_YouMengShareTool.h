//
//  ICN_YouMengShareTool.h
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMSocialCore/UMSocialPlatformConfig.h> // 友盟官方枚举头文件
#import "ICN_YouMengShareModel.h"

@interface ICN_YouMengShareTool : NSObject

+ (void)shareWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel*)model;
//活动分享
+ (void)shareActivityWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model;
//直播分享
+ (void)shareLiveWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model;
//职位分享
+ (void)shareNewsWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model;
//资讯分享
+ (void)sharePositionWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model;
@end
