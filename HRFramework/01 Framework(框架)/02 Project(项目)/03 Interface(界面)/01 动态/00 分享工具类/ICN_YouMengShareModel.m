//
//  ICN_YouMengShareModel.m
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_YouMengShareModel.h"

@implementation ICN_YouMengShareModel


- (instancetype)initWithParamsType:(ICN_ReplayType)paramsType ParamsKey:(NSString *)paramsKey ModelId:(NSString *)modelId Title:(NSString *)title IconUrl:(NSString *)iconUrl Content:(NSString *)content{
    self = [super init];
    if (self) {
        _paramsType = paramsType;
        _paramsKey = paramsKey;
        _modelId = SF(@"%@",modelId);
        _title = SF(@"%@",title);
        _iconUrl = SF(@"%@",iconUrl);
        _content = SF(@"%@",content);
    }
    return self;
}

+ (instancetype)modelWithParamsType:(ICN_ReplayType)paramsType ParamsKey:(NSString *)paramsKey ModelId:(NSString *)modelId Title:(NSString *)title IconUrl:(NSString *)iconUrl Content:(NSString *)content{
    return [[super alloc] initWithParamsType:paramsType ParamsKey:paramsKey ModelId:modelId Title:title IconUrl:iconUrl Content:content];
}


@end
