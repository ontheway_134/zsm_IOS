//
//  ICN_YouMengShareTool.m
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_YouMengShareTool.h"
#import "ICN_ApplyModel.h"
#import "ICN_ShareManager.h"

@implementation ICN_YouMengShareTool

+ (void)shareWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model{
    NSDictionary *params ;
    NSString *netpath ;
    // 根据Model的类型判断调用的接口 以及参数
    switch (model.paramsType) {
        case REPLAY_ComplainReplay:{
            // 吐槽的参数
            params = @{@"id":model.modelId};
            netpath = PATH_ComplainSHARE;
            break;
        }
        case REPLAY_QuestionReplay:{
            // 提问的参数
            params = @{@"id":model.modelId};
            netpath = PATH_QuestionSHARE;
            break;
        }
        default:{
            // 默认调用之前的路径
            params = @{model.paramsKey:model.modelId};
            netpath = PATH_DynamicSHARE;
            break;
        }
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:netpath params:params success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0 && [result valueForKey:@"result"] != nil) {
            NSString *url ;
            // 存在合理内容 == 两张不同的获取内容方法
            
            if ([[result valueForKey:@"result"] valueForKey:@"src"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"src"];
            }
            
            if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"url"];
            }
            
            if (url) {
                [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:type andVC:[self getCurrentVC] andUrl:url andTitle:model.title andImage:model.iconUrl Detial:model.content];
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow]Message:basemodel.info];
            
        }
        
    } failure:nil];

}

/**
 活动分享

 @param type 分享类型
 @param model 分享model
 */
+ (void)shareActivityWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model{
    NSDictionary *params = @{model.paramsKey:model.modelId};
    
    
    NSString *netpath = PATH_shareActivityPackage;
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:netpath params:params success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"src"] != nil) {
            NSString *url ;
            if ([[result valueForKey:@"result"] valueForKey:@"src"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"src"];
                
                [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:type andVC:[self getCurrentVC] andUrl:url andTitle:model.title andImage:model.iconUrl Detial:model.content];
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow]Message:basemodel.info];
            
        }
        
    } failure:nil];
    
}
+ (void)shareLiveWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model
{
    NSDictionary *params = @{model.paramsKey:model.modelId};
    NSString *netpath = PATH_shareActivityPackage;
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:netpath params:params success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"src"] != nil) {
            NSString *url ;
            if ([[result valueForKey:@"result"] valueForKey:@"src"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"src"];
                
                [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:type andVC:[self getCurrentVC] andUrl:url andTitle:model.title andImage:model.iconUrl Detial:model.content];
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow]Message:basemodel.info];
            
        }
        
    } failure:nil];

}

/**
 职位分享

 @param type 分享类型
 @param model 分享model
 */
+ (void)sharePositionWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model
{
    NSDictionary *params = @{model.paramsKey:model.modelId};
    NSString *netpath = PATH_ShareLfx_position;
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:netpath params:params success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"src"] != nil) {
            NSString *url ;
            if ([[result valueForKey:@"result"] valueForKey:@"src"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"src"];
                
                [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:type andVC:[self getCurrentVC] andUrl:url andTitle:model.title andImage:model.iconUrl Detial:model.content];
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow]Message:basemodel.info];
            
        }
        
    } failure:nil];
    
}
/**
 资讯分享
 
 @param type 分享类型
 @param model 分享model
 */
+ (void)shareNewsWithYouMengPlatFormType:(UMSocialPlatformType)type Model:(ICN_YouMengShareModel *)model
{
    NSDictionary *params = @{model.paramsKey:model.modelId};
    NSString *netpath = PATH_ShareLfx_news;
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:netpath params:params success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"src"] != nil) {
            NSString *url ;
            if ([[result valueForKey:@"result"] valueForKey:@"src"]) {
                url = [[result valueForKey:@"result"] valueForKey:@"src"];
                
                [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:type andVC:[self getCurrentVC] andUrl:url andTitle:model.title andImage:model.iconUrl Detial:model.content];
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow]Message:basemodel.info];
            
        }
        
    } failure:nil];
    
}

//获取当前屏幕显示的viewcontroller
+ (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}


@end
