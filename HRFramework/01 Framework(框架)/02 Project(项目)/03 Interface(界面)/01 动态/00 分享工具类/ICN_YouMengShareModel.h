//
//  ICN_YouMengShareModel.h
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ICN_YouMengShareModel : NSObject

@property (nonatomic , assign)ICN_ReplayType paramsType; // 内容源的具体类型对应的枚举
@property (nonatomic , copy)NSString *paramsKey; // 获取网址的网络请求传入id参数的key
@property (nonatomic , copy)NSString *modelId;   // Model对应的信息的ID
@property (nonatomic , copy)NSString *title;     // 分享的标题
@property (nonatomic , copy)NSString *iconUrl;   // 分享的图片链接
@property (nonatomic , copy)NSString *content;   // 分享的内容


+ (instancetype)modelWithParamsType:(ICN_ReplayType)paramsType
                          ParamsKey:(NSString *)paramsKey
                          ModelId:(NSString *)modelId
                            Title:(NSString *)title
                          IconUrl:(NSString *)iconUrl
                          Content:(NSString *)content;


@end
