//
//  ICN_QuestionViewModel.m
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_QuestionViewModel.h"
#import "ICN_QuestionListModel.h"
#import "ICN_AnswerListModel.h"

@interface ICN_QuestionViewModel ()

@property (nonatomic , assign)NSInteger currentPage; // 当前页数

@end

@implementation ICN_QuestionViewModel

- (NSMutableArray<ICN_QuestionListModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (NSMutableArray *)answersModelArr{
    if (_answersModelArr == nil) {
        _answersModelArr = [NSMutableArray array];
    }
    return _answersModelArr;
}

- (BOOL)insertUserTokenWithDictionary:(NSMutableDictionary *)params{
    
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        HRLog(@"缺少必要的用户token");
        return NO;
    }
    [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    return YES;
}

// 1. 处理提问列表
- (void)requestQuestionListWithType:(QuestionResponseType)enumType isLoad:(BOOL)isload MemberId:(NSString *)memberId SearchContent:(NSString *)searchContent{
    
    // 根据是否加载的属性判断页面传入的currentPage
    if (isload) {
        self.currentPage ++;
    }else{
        self.currentPage = 1;
    }
    
    // 判断不能响应代理则直接返回
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseQuestionWithEnumType:Success:Info:)]) {
        // 满足条件
    }else{
        HRLog(@"warn=== 提问列表没有调用代理的功能");
        return ;
    }
    
    // 创建参数字典和路径变量
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:SF(@"%ld",(long)self.currentPage) forKey:@"page"];
    NSString *netPath;
    
    switch (enumType) {
        case Question_ResponseMainList:{
            netPath = PATH_MyQuestionList;
            // 在没有token的时候不加载这个页面的数据
            if (![USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return ;
            }
            [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
            break ;
        }
        case Question_ResponseOtherList:{
            netPath = PATH_FirstQuestionList;
            [params setValue:@"" forKey:@"token"];
            break ;
        }
        case Question_ResponseUserIdList:{
            [params setValue:memberId forKey:@"memberId"];
            // 在没有token的时候不加载这个页面的数据
            if (![USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return ;
            }
            [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
            netPath = PATH_UserQuestionList;
            break ;
        }
        case Question_ResponseSearchList:{
            netPath = PATH_SearchQuestionList;
            [params setObject:SF(@"%@",searchContent) forKey:@"screach"];
            break ;
        }
        default:
            break;
    }
    
    // 发起网络请求
    [[HRRequest alloc] POST_PATH:netPath params:params success:^(id result) {
        // 1. 判断是否成功获取到数据
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            
            // 网络请求的结果成功
            
            // 获取到有效数据的时候
            NSArray *resultArr = [result valueForKey:@"result"];
            // 判断是刷新还是加载
            if (isload == NO) {
                // 刷新
                [self.modelsArr removeAllObjects];
            }
            // 循环遍历获取Model
            for (NSDictionary *dic in resultArr) {
                ICN_QuestionListModel *model = [[ICN_QuestionListModel alloc] initWithDictionary:dic error:nil];
                // 判断在Model不为空的时候加载
                if (model) {
                    [self.modelsArr addObject:model];
                }
            }
            
            // 数据获取完毕后回调
            [self.delegate responseQuestionWithEnumType:enumType Success:YES Info:baseModel.info];
            
        }else{
            // 当刷新数据并且数据获取异常的时候当做错误处理
            if (baseModel.code == 1 && self.currentPage == 1) {
                // 刷新数据失败的时候清空历史数据
                [self.modelsArr removeAllObjects];
            }
            [self.delegate responseQuestionWithEnumType:enumType Success:NO Info:baseModel.info];
        }

    } failure:^(NSDictionary *errorInfo) {
        [self.delegate responseQuestionWithEnumType:enumType Success:NO Info:@"数据请求失败"];
    }];
    
}

// 2. 处理提问列表具体功能
- (void)requestQuestionFunctionWithType:(QuestionResponseType)enumType QuestionId:(NSString *)questionId Content:(NSString *)content AnswerId:(NSString *)answerId{
    
    // 1. 获取用户的token 不过不存在直接返回失败
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([self insertUserTokenWithDictionary:params] == NO) {
        return ;
    }
    // 生成路径 并对于字典中的特殊字段处理
    NSString *netPath ;
    
    // 不支持代理的直接返回
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseQuestionWithEnumType:Success:Info:)]) {
        // 满足条件
    }else{
        HRLog(@"warn=== 提问列表没有调用代理的功能");
        return ;
    }
    
    switch (enumType) {
        case Question_ResponseLikeUp:{
            // 提问列表收藏功能
            [params setValue:SF(@"%@",questionId) forKey:@"id"];
            netPath = PATH_QuestionListLikeUp;
            break;
        }
        case Question_ResponseDisLikeUp:{
            // 提问列表取消收藏功能
            [params setValue:SF(@"%@",questionId) forKey:@"id"];
            netPath = PATH_QuestionListDisLikeUp;
            break;
        }
        case Question_DetialLikeUp:{
            // 提问详情 回答的点赞
            [params setValue:SF(@"%@",questionId) forKey:@"id"];
            [params setValue:SF(@"%@",answerId) forKey:@"commentId"];
            netPath = PATH_AnswerLikeUp;
            break ;
        }
        case Question_DetialDisLikeUp:{
            // 提问详情 回答的取消点赞
            [params setValue:SF(@"%@",questionId) forKey:@"id"];
            [params setValue:SF(@"%@",answerId) forKey:@"commentId"];
            netPath = PATH_AnswerDisLikeUp;
            break ;
        }
        case Question_ResponseAnswer:{
            // 提问列表 回答提问的功能
            [params setValue:SF(@"%@",questionId) forKey:@"questionId"];
            netPath = PATH_AnswerQuestion;
            break;
        }
        case Question_DetialFocusPerson:{
            // 提问详情 关注提问的人 == 在questionid中加入对方的用户id
            [params setValue:SF(@"%@",questionId) forKey:@"sideId"];
            netPath = PATH_FocusQuestionPerson;
            break;
        }
        case Question_DetialDisFocusPerson:{
            // 提问详情 取消关注提问的人
            [params setValue:SF(@"%@",questionId) forKey:@"sideId"];
            netPath = PATH_DisFocusQuestionPerson;
            break;
        }
        case Question_AnswerPayRewards:{
            // 提问详情 打赏问题
            [params setValue:SF(@"%@",questionId) forKey:@"id"];
            [params setValue:SF(@"%@",answerId) forKey:@"commentId"];
            netPath = PATH_PayForAnswer;
            break;
        }
        default:
            break;
    }
    
    [[HRRequest alloc] POST_PATH:netPath params:params success:^(id result) {
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            // 数据处理成功
            [self.delegate responseQuestionWithEnumType:enumType Success:YES Info:baseModel.info];
        }else{
            // 数据处理失败
            [self.delegate responseQuestionWithEnumType:enumType Success:NO Info:baseModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        [self.delegate responseQuestionWithEnumType:enumType Success:NO Info:@"网络请求失败"];
    }];
    
    
}

// 3. 处理详情相关的接口
- (void)requestQuestionDetialWithType:(QuestionResponseType)enumType MemberId:(NSString *)memberId isLoad:(BOOL)isLoad{
    // 1. 获取用户的token 不过不存在直接返回失败
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([self insertUserTokenWithDictionary:params] == NO) {
        return ;
    }
    // 判断页数
    if (isLoad) {
        self.currentPage ++;
    }else{
        self.currentPage = 1;
    }
    [params setValue:SF(@"%@",memberId) forKey:@"id"];
    [params setValue:SF(@"%ld",self.currentPage) forKey:@"page"];
    // 生成路径 并对于字典中的特殊字段处理
    NSString *netPath ;
    
    // 不支持代理的直接返回
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseQuestionWithEnumType:Success:Info:)]) {
        // 满足条件
    }else{
        HRLog(@"warn=== 提问列表没有调用代理的功能");
        return ;
    }
    
    // 添加路径
    if (enumType == Question_Detial) {
        netPath = PATH_QuestionDetial;
    }
    
    // 开启网络请求并针对数据进行处理
    [[HRRequest manager] POST_PATH:netPath params:params success:^(id result) {
        // 1. 判断是否成功获取到数据
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        
        // 网络请求的结果成功
        // 进行数据处理成功的回调
        if (baseModel.code == 0) {
            // 获取到有效数据的时候
            NSDictionary *resultDic = [result valueForKey:@"result"];
            // 1. 获取提问详情的内容
            self.detialModel = [[ICN_QuestionListModel alloc] initWithDictionary:[resultDic valueForKey:@"top"] error:nil];
            // 2. 获取提问回答数组
            NSArray *answerDicArr = [resultDic valueForKey:@"bottom"];
            // 判断是刷新还是加载
            if (isLoad == NO) {
                // 刷新
                [self.answersModelArr removeAllObjects];
            }
            // 循环遍历获取Model
            for (NSDictionary *dic in answerDicArr) {
                ICN_AnswerListModel *model = [[ICN_AnswerListModel alloc] initWithDictionary:dic error:nil];
                if (model) {
                    [self.answersModelArr addObject:model];
                }
            }
            
            // 数据获取完毕后回调
            [self.delegate responseQuestionWithEnumType:Question_Detial Success:YES Info:baseModel.info];
        }
        else{
            [self.delegate responseQuestionWithEnumType:Question_Detial Success:NO Info:baseModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        [self.delegate responseQuestionWithEnumType:Question_Detial Success:NO Info:@"网络请求失败"];
    }];
}

@end
