//
//  ICN_QuestionViewModel.h
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    Question_ResponseMainList = 133300011, // 类型是获取到用户自己提问列表,
    Question_ResponseOtherList,     // 类型是获取到首页提问列表 133300012
    Question_ResponseSearchList,    // 类型是获取到搜索提问列表 133300013
    Question_ResponseUserIdList,    // 类型是获取到用户主页提问列表 133300014
    Question_ResponseLikeUp,        // 类型是提问列表收藏 133300015
    Question_ResponseDisLikeUp,     // 类型是提问列表取消收藏 133300016
    Question_ResponseAnswer,        // 类型是提问列表 回答提问 133300017
    Question_DetialLikeUp,          // 类型是提问的 回答的点赞 133300018
    Question_DetialDisLikeUp,       // 类型是提问的 回答的取消点赞 133300019
    Question_Detial,                // 类型是提问的 回答的取消点赞 133300020
    Question_DetialFocusPerson,     // 类型是提问的 关注提问的人 133300021
    Question_DetialDisFocusPerson,  // 类型是提问的 取消关注提问的人 133300021
    Question_AnswerPayRewards,      // 类型是打赏 提问的回答 积分 133300022
} QuestionResponseType;

@class ICN_QuestionListModel;

@protocol QuestionConfigurationDelegate <NSObject>

// 获取到数据处理之后的回调
- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info;

@end

@interface ICN_QuestionViewModel : NSObject

@property (nonatomic , weak)id<QuestionConfigurationDelegate> delegate;
@property (nonatomic , strong)NSMutableArray <ICN_QuestionListModel *> *modelsArr;
@property (nonatomic , strong)ICN_QuestionListModel *detialModel; // 提问详情页面的
@property (nonatomic , strong)NSMutableArray *answersModelArr; // 回答Model数组






/** 获取提问列表数据的方法 */
- (void)requestQuestionListWithType:(QuestionResponseType)enumType isLoad:(BOOL)isload MemberId:(NSString *)memberId SearchContent:(NSString *)searchContent;

/** 进行其他不获取到用户数据列表的操作 */
- (void)requestQuestionFunctionWithType:(QuestionResponseType)enumType QuestionId:(NSString *)questionId Content:(NSString *)content AnswerId:(NSString *)answerId;

/** 进行对于提问详情的相关操作 */
- (void)requestQuestionDetialWithType:(QuestionResponseType)enumType MemberId:(NSString *)memberId isLoad:(BOOL)isLoad;


@end
