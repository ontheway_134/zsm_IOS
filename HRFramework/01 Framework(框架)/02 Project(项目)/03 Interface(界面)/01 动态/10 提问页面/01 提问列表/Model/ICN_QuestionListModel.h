//
//  ICN_QuestionListModel.h
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_QuestionListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *isMine; // 是不是我自己 0 是我发布的 1 不是我发布的


@property (nonatomic , copy)NSString *memberId; // 发布提问者ID
@property (nonatomic , copy)NSString *questionId; // 提问id

@property (nonatomic , copy)NSString *title; // 提问标题
@property (nonatomic , copy)NSString *contant; // 提问内容
@property (nonatomic , copy)NSString *createTime; // 提问发布时间
@property (nonatomic , copy)NSString *collectNum; // 提问收藏数
@property (nonatomic , copy)NSString *score; // 提问奖励积分
@property (nonatomic , copy)NSString *memberLogo; // 提问发布者头像
@property (nonatomic , copy)NSString *memberNick; // 提问发布者昵称
@property (nonatomic , copy)NSString *IndustryLabel; // 提问发布者所属行业
@property (nonatomic , copy)NSString *plusv; // 加V验证 0未验证   1已验证   2验证中（只要不是1就都是没验证不加V）
@property (nonatomic , copy)NSString *position; // 提问发布者职业,
@property (nonatomic , copy)NSString *company; // 提问发布者公司
@property (nonatomic , copy)NSString *iscollect; // 是否收藏 0为已经收藏 1为未收藏
@property (nonatomic , copy)NSString *isconcern; // 是否关注 0为已经关注 1为未关注
@property (nonatomic , copy)NSString *isadopt; // 是否打赏郭 0为已打赏 1为打赏



@end
