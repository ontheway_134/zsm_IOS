//
//  ICN_AnswerListModel.h
//  ICan
//
//  Created by albert on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_AnswerListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *memberLogo; // 回答提问者头像,
@property (nonatomic , copy)NSString *memberNick; // 回答提问者昵称,
@property (nonatomic , copy)NSString *questionId; // 提问ID,
@property (nonatomic , copy)NSString *memberId; // 回答提问者memberId,
@property (nonatomic , copy)NSString *createTime; // 回答提问时间,
@property (nonatomic , copy)NSString *content; // 回答提问内容,
@property (nonatomic , copy)NSString *thumbsupNum; // 点赞数,
@property (nonatomic , copy)NSString *status; // 无意义,
@property (nonatomic , copy)NSString *questionStatus; // 打赏验证 0为未打赏 1未被打赏,
@property (nonatomic , copy)NSString *plusv; // 加V验证 0未验证   1已验证  2验证中（只要不是1就都是没验证不加V）,
@property (nonatomic , copy)NSString *isthumbsup; // 是否点赞,0为已经点赞 1为未点赞,
@property (nonatomic , copy)NSString *commentId; // 提问回答ID,



@end
