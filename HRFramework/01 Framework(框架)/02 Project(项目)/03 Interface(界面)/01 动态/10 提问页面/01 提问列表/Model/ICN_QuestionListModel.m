//
//  ICN_QuestionListModel.m
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_QuestionListModel.h"

@implementation ICN_QuestionListModel

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"questionId" : @"id"}];
}

@end
