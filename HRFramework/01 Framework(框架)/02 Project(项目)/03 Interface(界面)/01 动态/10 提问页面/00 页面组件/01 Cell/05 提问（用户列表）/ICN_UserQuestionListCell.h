//
//  ICN_UserQuestionListCell.h
//  ICan
//
//  Created by albert on 2017/3/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_QuestionListModel;

@interface ICN_UserQuestionListCell : UITableViewCell

@property (nonatomic , strong)ICN_QuestionListModel *model;
@property (nonatomic , assign , getter = isEnterprise)BOOL EnterpriseUser; // 判断当前页面是不是企业用户登录的属性

@end
