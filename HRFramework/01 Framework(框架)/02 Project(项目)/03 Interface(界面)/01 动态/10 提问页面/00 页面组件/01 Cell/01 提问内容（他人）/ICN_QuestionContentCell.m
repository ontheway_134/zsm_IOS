

#import "ICN_QuestionContentCell.h"
#import "ICN_QuestionListModel.h"

@interface ICN_QuestionContentCell ()

#pragma mark --- 用户部分 ---

@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *userNickLabel; // 用户昵称
@property (weak, nonatomic) IBOutlet UILabel *professionLabel; // 行业标签
@property (weak, nonatomic) IBOutlet UILabel *MajorOrWorkLabel; // 专业/职位标签
@property (weak, nonatomic) IBOutlet UILabel *schoolOrCompanyLabel; // 学校/公司标签

#pragma mark --- 正文部分 ---

@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel; // 问题标题标签
@property (weak, nonatomic) IBOutlet UILabel *questionContentLabel; // 问题正文标签
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏标签
@property (weak, nonatomic) IBOutlet UILabel *freePointLabel; // 悬赏积分标签


#pragma mark --- 功能按钮 ---

@property (weak, nonatomic) IBOutlet UIButton *forceOnBtn; // 关注用户按钮
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn; // 收藏问题按钮
@property (weak, nonatomic) IBOutlet UIButton *transmitBtn; // 转发问题按钮
@property (weak, nonatomic) IBOutlet UIButton *answerBtn; // 回答问题的按钮


@property (weak, nonatomic) IBOutlet UIView *bottomDelertView; // 底部分割栏
@property (nonatomic , strong)UIImageView *vplusIcon; // 加v的时候使用的图片

#pragma mark --- 约束 ---
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *answerBtnWidthConstraint; // 回答按钮的宽度约束



@end

@implementation ICN_QuestionContentCell

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = YES; // 默认隐藏图标
        [self addSubview:_vplusIcon];
    }
    return _vplusIcon;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 展开文本的属性
- (void)setSpreadText:(BOOL)spreadText{
    _spreadText = spreadText;
    if (spreadText) {
        self.questionContentLabel.numberOfLines = 0;
        self.questionContentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
}

- (void)setModel:(ICN_QuestionListModel *)model{
    _model = model;
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_model.memberLogo] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    self.userNickLabel.text = _model.memberNick;
    // 设置行业标签
    if (_model.IndustryLabel) {
        self.professionLabel.hidden = NO;
        self.professionLabel.text = SF(@"%@",_model.IndustryLabel);
    }else{
        self.professionLabel.hidden = YES;
    }
    self.MajorOrWorkLabel.text = SF(@"%@",_model.position);
    self.schoolOrCompanyLabel.text = SF(@"%@",_model.company);
    self.questionTitleLabel.text = SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode]);
    self.questionContentLabel.text = SF(@"%@",[_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    // 设置积分标签
    self.freePointLabel.text = SF(@"%@积分",_model.score);
    // 处理收藏按钮
    if ([_model.iscollect integerValue] == 0) {
        // 已收藏
        [self changeButton:self.collectionBtn WithSelected:YES Number:[_model.collectNum integerValue]];
    }else{
        [self changeButton:self.collectionBtn WithSelected:NO Number:[_model.collectNum integerValue]];
    }
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    // 处理提问回答数量按钮
    
    // 处理关注与取消关注按钮
    if ([_model.isconcern integerValue] == 0) {
        // 已关注
        self.forceOnBtn.selected = YES;
    }else{
        // 未关注
        self.forceOnBtn.selected = NO;
    }
    // 设置刷新数据后关注按钮可点击
    self.forceOnBtn.userInteractionEnabled = YES;
    
    // 添加是否有回答按钮的相关判断
    if (self.isEnterprise) {
        self.answerBtnWidthConstraint.constant = 0.0;
        self.answerBtn.hidden = YES;
    }else{
        self.answerBtnWidthConstraint.constant = 45.0;
        self.answerBtn.hidden = NO;
    }
    
}


// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%d",number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%d",number) forState:UIControlStateSelected];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    // 在每次布局的时候添加tap手势
    [self insertTapGestureWithIcon:self.userIcon];

    
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.userIcon];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.userIcon);
            make.bottom.equalTo(self.userIcon);
        }];
    }
    // 配置头像 行业标签等内容
    self.userIcon.layer.cornerRadius = self.userIcon.width / 2.0;
    self.userIcon.layer.masksToBounds = YES;
    self.professionLabel.layer.masksToBounds = YES;
    self.professionLabel.layer.cornerRadius = 2.0;
    if (self.isFirstPager) {
        // 是显示在首页的Cell
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.userNickLabel);
            make.right.equalTo(self.contentView).offset(-10);
            make.left.greaterThanOrEqualTo(self.professionLabel).offset(10);
        }];
        self.bottomDelertView.hidden = NO;
        self.forceOnBtn.hidden = YES;
    }else{
        self.forceOnBtn.hidden = NO;
        self.bottomDelertView.hidden = YES;
        [self.timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.questionContentLabel.mas_bottom).offset(10);
            make.left.equalTo(self.questionContentLabel);
            make.right.greaterThanOrEqualTo(self.contentView).offset(-10);
        }];
    }
}

- (void)callBackCellBlock:(QuestionCellBlock)block{
    self.block = block;
}

- (void)callBackQuestionCollectedCellBlock:(QuestionCollectedCellBlock)block{
    self.collectedBlock = block;
}

- (void)callBackQuestionIconTapBlock01:(QuestionIconTapBlock01)block{
    self.imageTapBlock = block;
}

- (void)insertTapGestureWithIcon:(UIImageView *)icon{
    icon.userInteractionEnabled = YES;
    if (icon.gestureRecognizers == nil || icon.gestureRecognizers.count == 0) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnUserIconWithGesture:)];
        [icon addGestureRecognizer:tap];
    }
    
}

- (void)clickOnUserIconWithGesture:(UITapGestureRecognizer *)tap{
    self.imageTapBlock(self.model);
}



- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    if (self.block) {
        // 有block之后对其进行处理 -- 对于点赞和评论做预处理(成功之后会刷新数据失败不会)
        switch (sender.tag) {
            case ICN_CellCollectType:{
                sender.selected = !sender.selected;
                if (sender.selected) {
                    _model.iscollect = @"0";
                    _model.collectNum = SF(@"%d",[_model.collectNum integerValue] + 1);
                }else{
                    _model.iscollect = @"1";
                    _model.collectNum = SF(@"%d",[_model.collectNum integerValue] - 1);
                }
                self.block(self.model , sender.tag , sender.selected);
                break;
            }
            case ICN_CellFocusType:{
                // 点击了关注按钮
                // 调用关注专用的block
                self.collectedBlock(self.model , sender.tag , !sender.selected);
                // 在没刷新数据的时候暂时不能点击按钮
                sender.userInteractionEnabled = NO;
                // 预处理
                if (sender.selected) {
                    // 按钮选中状态显示加关注 == （为未关注）==点击为已关注
                    _model.isconcern = @"1";
                    _model.collectNum = SF(@"%d",_model.collectNum.integerValue + 1);
                }else{
                    _model.isconcern = @"0";
                    _model.collectNum = SF(@"%d",_model.collectNum.integerValue - 1);
                }
                break;
            }
            default:
                self.block(self.model , sender.tag , sender.selected);
                break;
        }
    }
}



@end
