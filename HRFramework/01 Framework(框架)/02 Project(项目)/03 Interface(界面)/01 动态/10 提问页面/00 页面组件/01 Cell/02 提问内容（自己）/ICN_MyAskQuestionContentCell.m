//
//  ICN_MyAskQuestionContentCell.m
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyAskQuestionContentCell.h"
#import "ICN_QuestionListModel.h"

@interface ICN_MyAskQuestionContentCell ()

@property (weak, nonatomic) IBOutlet UILabel *contentTitleLabel; // 内容标题
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 内容正文
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏
@property (weak, nonatomic) IBOutlet UILabel *freePointsLabel; // 悬赏积分标签
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn; // 收藏按钮
@property (weak, nonatomic) IBOutlet UIButton *reviewBtn; // 转发按钮



@end

@implementation ICN_MyAskQuestionContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

// 展开文本的属性
- (void)setSpreadText:(BOOL)spreadText{
    _spreadText = spreadText;
    if (spreadText) {
        self.contentLabel.numberOfLines = 0;
    }
}

- (void)setModel:(ICN_QuestionListModel *)model{
    _model = model;
    self.contentTitleLabel.text = SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode]);
    self.contentLabel.text = SF(@"%@",[_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    self.freePointsLabel.text = SF(@"%@",_model.score);
    // 处理收藏按钮
    if ([_model.iscollect integerValue] == 0) {
        // 已收藏
        [self changeButton:self.collectionBtn WithSelected:YES Number:[_model.collectNum integerValue]];
    }else{
        [self changeButton:self.collectionBtn WithSelected:NO Number:[_model.collectNum integerValue]];
    }
}

// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%ld",(long)number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%ld",number) forState:UIControlStateSelected];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)callBackCellBlock:(QuestionCellBlock)block{
    self.block = block;
}


- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.collectionBtn]) {
        // 点击的是收藏按钮
        sender.selected = !sender.selected;
        if (sender.selected) {
            _model.iscollect = @"0";
            _model.collectNum = SF(@"%ld",[_model.collectNum integerValue] + 1);
        }else{
            _model.iscollect = @"1";
            _model.collectNum = SF(@"%ld",[_model.collectNum integerValue] - 1);
        }
    }
    if ([sender isEqual:self.reviewBtn]) {
        // 点击的是转发按钮
        
    }
    self.block(self.model , sender.tag , sender.selected);
    
}


@end
