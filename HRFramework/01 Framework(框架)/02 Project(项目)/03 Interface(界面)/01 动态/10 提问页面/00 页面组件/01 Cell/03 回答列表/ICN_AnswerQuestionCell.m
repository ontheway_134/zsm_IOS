//
//  ICN_AnswerQuestionCell.m
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_AnswerQuestionCell.h"
#import "ICN_AnswerListModel.h"

@interface ICN_AnswerQuestionCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *CollectBtnToPayBtnLeadingConstraint; // 收藏按钮距离打赏按钮左侧边界的约束


@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 评论内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *payForButton; // 打赏按钮
@property (weak, nonatomic) IBOutlet UIView *segmentGrayView; // 分割Cell用灰条 -- 只有最后一个不显示
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

#pragma mark --- 约束设置 ---
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *payforBtnWidthContrainst; // 打赏按钮宽度约束


@end

@implementation ICN_AnswerQuestionCell

- (void)setHiddenPay:(BOOL)hiddenPay{
    _hiddenPay = hiddenPay;
    if (_hiddenPay) {
        self.payForButton.hidden = YES;
        self.CollectBtnToPayBtnLeadingConstraint.constant = -60;
    }else{
        self.payForButton.hidden = NO;
        self.CollectBtnToPayBtnLeadingConstraint.constant = 5;
    }
}

- (void)setModel:(ICN_AnswerListModel *)model{
    _model = model;
    self.nickNameLabel.text = SF(@"%@",_model.memberNick);
    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:_model.memberLogo] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    self.contentLabel.text = SF(@"%@",[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    // 处理点赞按钮
    if ([_model.isthumbsup integerValue] == 0) {
        // 已点赞
        [self changeButton:self.likeUpBtn WithSelected:YES Number:[_model.thumbsupNum integerValue]];
    }else{
        [self changeButton:self.likeUpBtn WithSelected:NO Number:[_model.thumbsupNum integerValue]];
    }
    
    // 根据是否是企业用户判断是都显示打赏按钮
    if (self.isEnterprise) {
        self.hiddenPay = YES;
    }
//    if (self.isEnterprise) {
//        self.payForButton.hidden = YES;
//        self.payforBtnWidthContrainst.constant = 0.0;
//    }else{
//        self.payForButton.hidden = NO;
//        self.payforBtnWidthContrainst.constant = 50.0;
//    }
}

// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%d",number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%d",number) forState:UIControlStateSelected];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 在每次布局的时候添加tap手势
    [self insertTapGestureWithIcon:self.userLogoIcon];
    self.userLogoIcon.layer.masksToBounds = YES;
    self.userLogoIcon.layer.cornerRadius = self.userLogoIcon.width / 2.0;
}

- (void)callBackCellBlock:(AnswerCellBlock)block{
    self.block = block;
}

- (void)callBackAnswerIconTapBlock:(AnswerIconTapBlock)block{
    self.iconTapBlock = block;
}

- (void)insertTapGestureWithIcon:(UIImageView *)icon{
    icon.userInteractionEnabled = YES;
    if (icon.gestureRecognizers == nil || icon.gestureRecognizers.count == 0) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnUserIconWithGesture:)];
        [icon addGestureRecognizer:tap];
    }
}

- (void)clickOnUserIconWithGesture:(UITapGestureRecognizer *)tap{
    self.iconTapBlock(self.model);
}

// 点击了Cell上的按钮
- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.likeUpBtn]) {
        // 点击了点赞按钮
        // 先对于点赞数据预处理
        if (!sender.selected) {
            // 点击后需要进行点赞操作 -- 之前是取消点赞那
            self.model.isthumbsup = @"0";
            self.model.thumbsupNum = SF(@"%d",[self.model.thumbsupNum integerValue] + 1);
        }else{
            // 取消点赞操作
            self.model.isthumbsup = @"1";
            self.model.thumbsupNum = SF(@"%d",[self.model.thumbsupNum integerValue] - 1);
        }
        
        if (self.block) {
            self.block(self.model , sender.tag , !sender.selected);
        }
    }
    if ([sender isEqual:self.payForButton]) {
        // 点击了打赏按钮
        if (self.block) {
            self.block(self.model , sender.tag , sender.selected);
        }
    }
    
}



@end
