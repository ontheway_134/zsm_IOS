//
//  ICN_QuestionContentCell.h
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_QuestionListModel;

typedef void(^QuestionCellBlock)(ICN_QuestionListModel *model , NSInteger buttonType , BOOL isSelected);
typedef void(^QuestionCollectedCellBlock)(ICN_QuestionListModel *model , NSInteger buttonType , BOOL isCollected);
typedef void(^QuestionIconTapBlock01)(ICN_QuestionListModel *model);

@interface ICN_QuestionContentCell : UITableViewCell

@property (nonatomic , assign , getter=isFirstPager) BOOL firstPager; // 判断是否是动态首页的属性（动态首页对于时间栏有新的布局并且隐藏了关注按钮）

@property (nonatomic , strong)ICN_QuestionListModel *model;
@property (nonatomic , copy)QuestionCellBlock block;
@property (nonatomic , copy)QuestionCollectedCellBlock collectedBlock;
@property (nonatomic , copy)QuestionIconTapBlock01 imageTapBlock;
@property (nonatomic , assign , getter = isEnterprise)BOOL EnterpriseUser; // 判断当前页面是不是企业用户登录的属性
@property (nonatomic , assign)BOOL spreadText; // 展开文本




- (void)callBackCellBlock:(QuestionCellBlock)block;
- (void)callBackQuestionCollectedCellBlock:(QuestionCollectedCellBlock)block;
- (void)callBackQuestionIconTapBlock01:(QuestionIconTapBlock01)block;


@end
