//
//  ICN_AnswerQuestionCell.h
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_AnswerListModel;

typedef void(^AnswerCellBlock)(ICN_AnswerListModel *model , NSInteger buttonType , BOOL isSelected);
typedef void(^AnswerIconTapBlock)(ICN_AnswerListModel *model);

@interface ICN_AnswerQuestionCell : UITableViewCell

@property (nonatomic , assign)BOOL hiddenPay; // 隐藏打赏
@property (nonatomic , strong)ICN_AnswerListModel *model;
@property (nonatomic , copy)AnswerCellBlock block;
@property (nonatomic , copy)AnswerIconTapBlock iconTapBlock;
@property (nonatomic , assign , getter = isEnterprise)BOOL EnterpriseUser; // 判断当前页面是不是企业用户登录的属性


- (void)callBackCellBlock:(AnswerCellBlock)block;

- (void)callBackAnswerIconTapBlock:(AnswerIconTapBlock)block;


@end
