//
//  ICN_UserListQuestionContentCell.m
//  ICan
//
//  Created by albert on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserListQuestionContentCell.h"
#import "ICN_QuestionListModel.h"

@interface ICN_UserListQuestionContentCell ()

#pragma mark --- 用户部分 ---

@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *userNickLabel; // 用户昵称
@property (weak, nonatomic) IBOutlet UILabel *professionLabel; // 行业标签
@property (weak, nonatomic) IBOutlet UILabel *MajorOrWorkLabel; // 专业/职位标签
@property (weak, nonatomic) IBOutlet UILabel *schoolOrCompanyLabel; // 学校/公司标签

#pragma mark --- 正文部分 ---

@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel; // 问题标题标签
@property (weak, nonatomic) IBOutlet UILabel *questionContentLabel; // 问题正文标签
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏标签
@property (weak, nonatomic) IBOutlet UIButton *payCountBtn; // 打赏金额按钮（并不添加点击事件）

#pragma mark --- 功能按钮 ---

@property (weak, nonatomic) IBOutlet UIButton *transmitBtn; // 转发问题按钮
@property (nonatomic , strong)UIImageView *vplusIcon; // 加v的时候使用的图片



@end


@implementation ICN_UserListQuestionContentCell

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = YES; // 默认隐藏图标
        [self addSubview:_vplusIcon];
    }
    return _vplusIcon;
}

- (void)callBackQuestionCellBlock:(QuestionCellBlock)block{
    self.block = block;
}

- (void)callBackQuestionIconTapBlock:(QuestionIconTapBlock)block{
    self.iconTapBlcok = block;
}

- (void)setModel:(ICN_QuestionListModel *)model{
    _model = model;
    
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:_model.memberLogo] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    self.userNickLabel.text = _model.memberNick;
    // 设置行业标签
    self.professionLabel.text = SF(@"%@",_model.IndustryLabel);
    self.MajorOrWorkLabel.text = SF(@"%@",_model.position);
    self.schoolOrCompanyLabel.text = SF(@"%@",_model.company);
    self.questionTitleLabel.text = SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode]);
    self.questionContentLabel.text = SF(@"%@",[_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    // 设置积分标签
    [self changeButton:self.payCountBtn WithSelected:NO Number:[_model.score integerValue]];
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    
}

// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%ld",(long)number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%ld",(long)number) forState:UIControlStateSelected];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 在每次布局的时候添加tap手势
    [self insertTapGestureWithIcon:self.userIcon];
    
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.userIcon];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.userIcon);
            make.bottom.equalTo(self.userIcon);
        }];
    }
    self.userIcon.layer.cornerRadius = self.userIcon.width / 2.0;
    self.userIcon.layer.masksToBounds = YES;
    self.professionLabel.layer.masksToBounds = YES;
    self.professionLabel.layer.cornerRadius = 2.0;
}

- (void)insertTapGestureWithIcon:(UIImageView *)icon{
    icon.userInteractionEnabled = YES;
    if (icon.gestureRecognizers == nil || icon.gestureRecognizers.count == 0) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnUserIconWithGesture:)];
        [icon addGestureRecognizer:tap];
    }
    
}

- (void)clickOnUserIconWithGesture:(UITapGestureRecognizer *)tap{
    self.iconTapBlcok(self.model);
}

- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    self.block(self.model , sender.tag , sender.selected);
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
