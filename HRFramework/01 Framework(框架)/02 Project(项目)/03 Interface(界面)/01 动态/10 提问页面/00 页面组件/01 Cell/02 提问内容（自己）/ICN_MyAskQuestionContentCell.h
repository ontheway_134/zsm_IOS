//
//  ICN_MyAskQuestionContentCell.h
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_QuestionListModel;

typedef void(^QuestionCellBlock)(ICN_QuestionListModel *model , NSInteger buttonType , BOOL isSelected);

@interface ICN_MyAskQuestionContentCell : UITableViewCell

@property (nonatomic , strong)ICN_QuestionListModel *model;
@property (nonatomic , copy)QuestionCellBlock block;
@property (nonatomic , assign)BOOL spreadText; // 展开文本



- (void)callBackCellBlock:(QuestionCellBlock)block;


@end
