//
//  ICN_UserListQuestionContentCell.h
//  ICan
//
//  Created by albert on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_QuestionListModel;

typedef void(^QuestionCellBlock)(ICN_QuestionListModel *model , NSInteger buttonType , BOOL isSelected);
typedef void(^QuestionIconTapBlock)(ICN_QuestionListModel *model);

@interface ICN_UserListQuestionContentCell : UITableViewCell

@property (nonatomic , strong)ICN_QuestionListModel *model;
@property (nonatomic , copy)QuestionCellBlock block;
@property (nonatomic , copy)QuestionIconTapBlock iconTapBlcok;
@property (nonatomic , assign , getter = isEnterprise)BOOL EnterpriseUser; // 判断当前页面是不是企业用户登录的属性


- (void)callBackQuestionCellBlock:(QuestionCellBlock)block;
- (void)callBackQuestionIconTapBlock:(QuestionIconTapBlock)block;


@end
