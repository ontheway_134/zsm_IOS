//
//  ICN_UserQuestionListCell.m
//  ICan
//
//  Created by albert on 2017/3/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserQuestionListCell.h"
#import "ICN_QuestionListModel.h"

@interface ICN_UserQuestionListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; // 提问标题
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 提问正文


@end

@implementation ICN_UserQuestionListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ICN_QuestionListModel *)model{
    _model = model;
    self.titleLabel.text = SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode]);
    self.contentLabel.text = SF(@"%@",[_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
