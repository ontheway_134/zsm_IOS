//
//  ICN_OthersQuestionDetialPager.m
//  ICan
//
//  Created by albert on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_OthersQuestionDetialPager.h"
#import "ICN_QuestionContentCell.h" // 别人的提问详情Cell  210
#import "ICN_AnswerQuestionCell.h" // 自己的回答Cell   155
#import "ICN_ComSingleTitleSectionView.h" // 吐槽section 的头视图
#import "ICN_InputCommentView.h" // 通用的评论输出框（含代理）
#import "ICN_QuestionViewModel.h"// 提问需要的ViewModel
#import "ICN_QuestionListModel.h"
#import "ICN_AnswerListModel.h"
#import "ICN_TransmitToDynamicPager.h"          // 转发到动态页面
#import "ICN_MYAnswerQuestionPager.h" // 我来回答页面
#import "ICN_UserHomePagerVC.h" // 用户主页

#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model

@interface ICN_OthersQuestionDetialPager ()<UITableViewDelegate , UITableViewDataSource , QuestionConfigurationDelegate , ICN_DynWarnViewDelegate>

@property (strong , nonatomic)UITableView *tableView;
@property (nonatomic , strong)ICN_QuestionViewModel *viewModel;
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作

@end

@implementation ICN_OthersQuestionDetialPager

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.origin.y -= 64.0;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_QuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_AnswerQuestionCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_AnswerQuestionCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 0.0;
        _tableView.sectionHeaderHeight = 0.0;
        _tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        
        [self.view addSubview:_tableView];
        // 使用masnory设置tableview的尺寸
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        [self configTableViewRefreshHeaderFooterView];
    }
    return _tableView;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置显示默认的导航栏
    [self setHiddenDefaultNavBar:NO];
    // 初始化tableview
    [self tableView];
    // 设置标题
    self.naviTitle = @"提问详情";
    // 设置ViewModel
    self.viewModel = [[ICN_QuestionViewModel alloc] init];
    self.viewModel.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.tableView.mj_header) {
        if (![self.tableView.mj_header isRefreshing]) {
            // 当mj的header没有处于刷新状态的时候刷新
            [self.tableView.mj_header beginRefreshing];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 按钮响应事件 ----------


#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestQuestionDetialWithType:Question_Detial MemberId:self.contentId isLoad:NO];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestQuestionDetialWithType:Question_Detial MemberId:self.contentId isLoad:NO];
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MJRefreshAutoNormalFooter *footer = (MJRefreshAutoNormalFooter *)tableView.mj_footer;
        [footer endRefreshing];
    });
}

#pragma mark - ---------- 协议 ----------

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}


#pragma mark --- 提问网络处理代理 - QuestionConfigurationDelegate ---

- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    
    // 1. 获取失败需要弹窗
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
    }
    
    // 根据参数类型进行对应的处理
    switch (enumType) {
        case Question_Detial:{
            // 获取到提问详情
            [self endRefreshWithTableView:self.tableView];
            [self.tableView reloadData];
            break ;
        }
        case Question_DetialLikeUp:{
            // 获取回答点赞
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        case Question_DetialDisLikeUp:{
            // 获取回答取消点赞
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        case Question_ResponseLikeUp:{
            // 关注
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        case Question_ResponseDisLikeUp:{
            // 取消关注
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        case Question_DetialFocusPerson: {
            // 关注
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        case Question_DetialDisFocusPerson:{
            // 取消关注
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            return 1;
            break;
        }
            // 返回评论的数量
        default:
            return self.viewModel.answersModelArr.count;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            // 返回正文的Cell
            ICN_QuestionContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
            // 添加根据当前的用户状态判断是否显示功能按钮（针对企业用户）
            cell.EnterpriseUser = [self isCurrentEnterpriseUser];
            // 设置正文的展开状态
            cell.spreadText = YES;
            cell.model = self.viewModel.detialModel;
            [cell callBackQuestionIconTapBlock01:^(ICN_QuestionListModel *model) {
                ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
                pager.memberId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }];
            [cell callBackQuestionCollectedCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isCollected) {
                // 设置关注和取消关注的block
                if (isCollected) {
                    // 进行关注操作
                    [self.viewModel requestQuestionFunctionWithType:Question_DetialFocusPerson QuestionId:model.memberId Content:nil AnswerId:nil];
                }else{
                    // 进行取消关注操作
                    [self.viewModel requestQuestionFunctionWithType:Question_DetialDisFocusPerson QuestionId:model.memberId Content:nil AnswerId:nil];
                }
            }];
            [cell callBackCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isSelected) {
                switch (buttonType) {
                        // 收藏 / 取消收藏操作
                    case ICN_CellCollectType:{
                        if (isSelected) {
                            [self.viewModel requestQuestionFunctionWithType:Question_ResponseLikeUp QuestionId:self.contentId Content:nil AnswerId:nil];
                        }else{
                            [self.viewModel requestQuestionFunctionWithType:Question_ResponseDisLikeUp QuestionId:model.questionId Content:nil AnswerId:nil];
                        }
                        break ;
                    }
                        // 转发操作
                    case ICN_CellReplayType:{
                        //warn=== 提问的转发接口不存在需要修改
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_QuestionReplay ParamsKey:@"questionId" ModelId:model.questionId Title:@"i行提问" IconUrl:model.memberLogo Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.warnView];
                        break ;
                    }
                        // 回答操作 == 跳转到详情页面
                    case ICN_CellAnswerType:{
                        // 跳转到我来回答页面
                        if (![self isCurrentEnterpriseUser]) {
                            ICN_MYAnswerQuestionPager *pager = [[ICN_MYAnswerQuestionPager alloc] init];
                            pager.questionId = SF(@"%@",self.contentId);
                            [self currentPagerJumpToPager:pager];
                        }
                    }
                    default:{
                        break;
                    }
                }
            }];
            return cell;
            break;
        }
        default:{
            // 返回评论内容Cell
            ICN_AnswerQuestionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_AnswerQuestionCell class])];
            // 添加根据当前的用户状态判断是否显示功能按钮（针对企业用户）
            cell.EnterpriseUser = [self isCurrentEnterpriseUser];
            cell.model = self.viewModel.answersModelArr[indexPath.row];
            cell.hiddenPay = YES;
            [cell callBackAnswerIconTapBlock:^(ICN_AnswerListModel *model) {
                ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
                pager.memberId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }];
            // 进行对于评论内容相关的操作 -- 点赞
            [cell callBackCellBlock:^(ICN_AnswerListModel *model, NSInteger buttonType, BOOL isSelected) {
                if (isSelected) {
                    [self.viewModel requestQuestionFunctionWithType:Question_DetialLikeUp QuestionId:self.contentId Content:nil AnswerId:model.commentId];
                }else{
                    [self.viewModel requestQuestionFunctionWithType:Question_DetialDisLikeUp QuestionId:model.questionId Content:nil AnswerId:model.commentId];
                }
            }];
            return cell;
            break;
        }
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 1) {
        ICN_ComSingleTitleSectionView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        header.contentView.backgroundColor = RGB0X(0xf0f0f0);
        header.headerTitle = @"回答";
        return header;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // 默认的 section 0 没有头视图 section 1 有
    if (section == 1) {
        return 40.0;
    }
    if (section == 0) {
        return 0.0;
    }
    return 0.0;
    
}

@end
