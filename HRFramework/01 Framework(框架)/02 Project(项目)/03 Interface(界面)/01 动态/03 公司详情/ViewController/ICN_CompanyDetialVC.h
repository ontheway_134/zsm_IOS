//
//  ICN_CompanyDetialVC.h
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_DynStateContentModel;
@interface ICN_CompanyDetialVC : BaseViewController


@property (nonatomic , copy)NSString *companyId; // 公司ID

/*关注图片 2017 4 1 把企业端的关注按钮屏蔽掉*/
@property (weak, nonatomic) IBOutlet UIImageView *concernImage;

@end
