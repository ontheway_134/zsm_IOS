//
//  ICN_DynCompanyViewModel.m
//  ICan
//
//  Created by albert on 2017/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DynCompanyViewModel.h"
#import "ICN_DynCompantDetialModel.h"
#import "HRNetworkingManager+DynFirstPager.h"

@implementation ICN_DynCompanyViewModel

- (void)requestForCompanyDetialModelWithCompanyId:(NSString *)companyId{
    if (companyId) {
        [HRNetworkingManager requestDynCompanyDetialContentWithCompanyId:companyId Success:^(ICN_DynCompantDetialModel *model) {
            if (model.code == 0) {
                self.model = model;
                // 添加状态的网络请求
                NSDictionary *params = @{
                                         @"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],
                                         @"enterpriseId":self.model.memberId
                                         };
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynCompanyAttentionStatus params:params success:^(id result) {
                    BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                    if (model.code == 0) {
                        self.model.isConcern = [[result valueForKey:@"result"] valueForKey:@"concern"];
                    }
                    [self callBackWithCompantDetialRequestCode:model.code Error:model.info];
                } failure:^(NSDictionary *errorInfo) {
                    [self callBackWithCompantDetialRequestCode:1 Error:@"获取关注企业状态出错"];
                }];
            }
            [self callBackWithCompantDetialRequestCode:model.code Error:model.info];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithCompantDetialRequestCode:-1 Error:@"网络请求失败"];
        }];
    }
}


- (void)callBackWithCompantDetialRequestCode:(NSInteger)code Error:(NSString *)error{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseNetRequestWithSuccess:Error:)]) {
            if (code == 0) {
                [self.delegate responseNetRequestWithSuccess:YES Error:nil];
            }else{
                [self.delegate responseNetRequestWithSuccess:NO Error:error];
            }
        }
    }
}

- (void)requestForCompanyAttention:(BOOL)isAttention CompanyId:(NSString *)companyId{
    if (isAttention) {
        [HRNetworkingManager requestAddCompanyAttentionWithCompanyId:companyId Success:^(BaseOptionalModel *model) {
            [self callBackWithCompanyAttentionRequestCode:model.code Error:model.info Attention:isAttention];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithCompanyAttentionRequestCode:-1 Error:@"网络获取失败" Attention:isAttention];
        }];
    }else{
        [HRNetworkingManager requestDeleteCompanyAttentionWithCompanyId:companyId Success:^(BaseOptionalModel *model) {
            [self callBackWithCompanyAttentionRequestCode:model.code Error:model.info Attention:isAttention];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithCompanyAttentionRequestCode:-1 Error:@"网络获取失败" Attention:isAttention];
        }];
    }
}


- (void)callBackWithCompanyAttentionRequestCode:(NSInteger)code Error:(NSString *)error Attention:(BOOL)isAttention{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseCompanyAttention:Success:Error:)]) {
            if (code == 0) {
                [self.delegate responseCompanyAttention:isAttention Success:YES Error:error];
            }else{
                [self.delegate responseCompanyAttention:isAttention Success:NO Error:error];
            }
        }
    }
}

@end
