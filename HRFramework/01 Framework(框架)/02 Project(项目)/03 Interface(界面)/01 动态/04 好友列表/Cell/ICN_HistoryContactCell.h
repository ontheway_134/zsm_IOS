//
//  ICN_HistoryContactCell.h
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_NewFrident;
@interface ICN_HistoryContactCell : UITableViewCell

@property (nonatomic , strong)ICN_NewFrident *model;

@end
