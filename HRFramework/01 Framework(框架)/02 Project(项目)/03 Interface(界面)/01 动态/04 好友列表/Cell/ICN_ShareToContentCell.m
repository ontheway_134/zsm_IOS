//
//  ICN_ShareToContentCellTableViewCell.m
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ShareToContentCell.h"

@interface ICN_ShareToContentCell ()

@property (nonatomic , strong)UILabel *p_TitleLabel;
@property (nonatomic , strong)UIImageView *p_Imageview;

@end

@implementation ICN_ShareToContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _p_TitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _p_TitleLabel.userInteractionEnabled = YES;
        _p_TitleLabel.font = [UIFont systemFontOfSize:13.0];
        _p_TitleLabel.text = @"分享给联系人";
        [self.contentView addSubview:_p_TitleLabel];
        
        _p_Imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"右箭头"]];
        _p_Imageview.userInteractionEnabled = YES;
        [self.contentView addSubview:_p_Imageview];
    }
    return self;
}

- (void)setCellTitle:(NSString *)cellTitle{
    _cellTitle = cellTitle;
    if (_cellTitle) {
        self.p_TitleLabel.text = _cellTitle;
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self configCurrentViewUIWhileWillLayout];
    
}

// 在视图将要布局的时候添加约束
- (void)configCurrentViewUIWhileWillLayout{
    [self.p_TitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(11.0);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
    
    [self.p_Imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
