//
//  ICN_ShareToContentCellTableViewCell.h
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ShareToContentCell : UITableViewCell

@property (nonatomic , copy)NSString *cellTitle; // 当前Cell的标题

@end
