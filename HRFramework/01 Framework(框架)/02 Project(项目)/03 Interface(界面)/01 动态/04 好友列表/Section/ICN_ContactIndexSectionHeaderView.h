//
//  ICN_ContactIndexSectionHeaderView.h
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ContactIndexSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic , copy)NSString *indexTitle;



@end
