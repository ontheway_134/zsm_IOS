//
//  ICN_ContactIndexSectionHeaderView.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ContactIndexSectionHeaderView.h"

@interface ICN_ContactIndexSectionHeaderView ()

@property (nonatomic , strong)UILabel *titleLabel; // 标题标签

@end

@implementation ICN_ContactIndexSectionHeaderView


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_titleLabel];
        self.contentView.backgroundColor = RGB0X(0xf0f0f0);
        _titleLabel.font = [UIFont systemFontOfSize:11.0];
        _titleLabel.textColor = RGB0X(0x333333);
        _titleLabel.userInteractionEnabled = YES;
    }
    return self;
}

- (void)setIndexTitle:(NSString *)indexTitle{
    _indexTitle = indexTitle;
    _titleLabel.text = indexTitle;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self configCurrentViewUI];
}


- (void)configCurrentViewUI{
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.left.equalTo(self.contentView).offset(5.0);
    }];
}

@end
