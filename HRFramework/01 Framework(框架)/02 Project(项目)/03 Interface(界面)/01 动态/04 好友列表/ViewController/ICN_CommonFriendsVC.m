//
//  ICN_CommonFriendsVC.m
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonFriendsVC.h"
#import "ICN_FirendListCell.h" // 好友Cell

@interface ICN_CommonFriendsVC ()

@property (nonatomic , strong)UITableView *tableView; // 当前页面详情列表

@end

@interface ICN_CommonFriendsVC ()<UITableViewDelegate , UITableViewDataSource>

@end

@implementation ICN_CommonFriendsVC

#pragma mark - ---------- 懒加载 ----------

/** 需要将tableView的属性传递过来 */
- (UITableView *)tableView{
    if (_tableView == nil) {
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        [self.view addSubview:_tableView];
        // 设置tableView的注册Cell等内容
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_FirendListCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_FirendListCell class])];
    }
    return _tableView;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"共同好友";
    [self tableView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 代理 ----------

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_FirendListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_FirendListCell class])];
    cell.alreadyFriend = YES;
    return cell;
}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55.0;
}




@end
