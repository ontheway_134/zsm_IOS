//
//  ICN_ShareHistoryFriendsListVC.m
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ShareHistoryFriendsListVC.h"
#pragma mark - ---------- 控件头文件 ----------
#import "ICN_CommonSearchBar.h" // 通用搜索栏方法 - 需要添加代理
#import "ICN_ShareToContentCell.h" // 分享到联系人的Cell
#import "ICN_HistoryContactCell.h" // 历史联系人Cell
#import "ICN_ComSingleTitleSectionView.h" // 单行section header

#pragma mark - ---------- 跳转页面头文件 ----------
#import "ICN_ContactListVC.h" // 联系人列表VC


@interface ICN_ShareHistoryFriendsListVC ()<UITableViewDelegate , UITableViewDataSource , CommonSearchBarDelegate>

@property (nonatomic , strong)ICN_CommonSearchBar *searchbar; // 当前页面搜索栏
@property (nonatomic , strong)UITableView *tableView; // 当前页面列表


@end

@implementation ICN_ShareHistoryFriendsListVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_CommonSearchBar *)searchbar{
    if (_searchbar == nil) {
        _searchbar = XIB(ICN_CommonSearchBar);
        _searchbar.delegate = self;
        [self.view addSubview:_searchbar];
    }
    return _searchbar;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        [self.view addSubview:_tableView];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.sectionHeaderHeight = 0.0;
        _tableView.sectionFooterHeight = 0.0;
        
        // tableView 注册Cell
        [_tableView registerClass:[ICN_HistoryContactCell class] forCellReuseIdentifier:NSStringFromClass([ICN_HistoryContactCell class])];
        [_tableView registerClass:[ICN_ShareToContentCell class] forCellReuseIdentifier:NSStringFromClass([ICN_ShareToContentCell class])];
        
        // tableView 注册section header
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    }
    return _tableView;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self tableView];
    [self searchbar];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configCurrentPagerUIWhileViewWillAppear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 在视图将要出现的时候配置UI
- (void)configCurrentPagerUIWhileViewWillAppear{
    [self.searchbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.height.mas_equalTo(50.0);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchbar.mas_bottom).offset(-36.0);
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

#pragma mark - ---------- 代理 ----------

/** searchbar的代理方法 */
#pragma mark --- CommonSearchBarDelegate ---

- (void)searchBarDidBeginEditingWithText:(NSString *)content{
    
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    if (indexPath.section == 0) {
        ICN_ContactListVC *pager = [[ICN_ContactListVC alloc] init];
        [self currentPagerJumpToPager:pager];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else{
        return 12;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ShareToContentCell class])];
    }else{
        return [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_HistoryContactCell class])];
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return nil;
    }else{
        ICN_ComSingleTitleSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        view.headerTitle = @"最近消息";
        return view;
    }
    
    return nil;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        return 40.0;
    }else{
        return 55.0;
    }
    
    return 0.0;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return 0.0;
    }
    return 36.0;
}





@end
