//
//  ICN_LocationModel.m
//  ICan
//
//  Created by albert on 2017/1/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LocationModel.h"

@implementation ICN_LocationModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"provinceId" : @"id"} ];
}


@end

@implementation ICN_CityDetialModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"cityId" : @"id"} ];
}



@end
