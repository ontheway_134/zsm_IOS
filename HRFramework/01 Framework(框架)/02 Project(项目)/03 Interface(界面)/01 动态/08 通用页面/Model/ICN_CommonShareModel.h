//
//  ICN_CommonShareModel.h
//  ICan
//
//  Created by albert on 2017/2/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_CommonShareModel : NSObject

@property (nonatomic , copy)NSString *title;
@property (nonatomic , copy)NSString *detial;
@property (nonatomic , copy)NSString *imageUrl;

- (instancetype)initWithTitle:(NSString *)title Detial:(NSString *)detial ImageUrl:(NSString *)imageUrl;


@end
