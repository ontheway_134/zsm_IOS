//
//  ICN_DynLocationPickerView.h
//  ICan
//
//  Created by albert on 2017/1/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol DynLocationPickerViewDelegate <NSObject>

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success;

@end

@interface ICN_DynLocationPickerView : UIView

@property (nonatomic , weak)id<DynLocationPickerViewDelegate> delegate;
@property (nonatomic , strong)NSArray *locationsArr;


+ (instancetype)loadXibWithCurrentBound:(CGRect)bound;

@end

