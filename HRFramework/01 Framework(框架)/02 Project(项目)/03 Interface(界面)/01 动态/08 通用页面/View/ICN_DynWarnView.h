//
//  ICN_DynWaarnView.h
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ICN_BtnListIconsArr @[[UIImage imageNamed:@"i行动态"],[UIImage imageNamed:@"i行好友"],[UIImage imageNamed:@"微信"],[UIImage imageNamed:@"朋友圈1"],[UIImage imageNamed:@"微博"],[UIImage imageNamed:@"QQ"],[UIImage imageNamed:@"qq空间"]]

@protocol ICN_DynWarnViewDelegate <NSObject>

- (void)responsedButtonClickWithBtnType:(NSInteger)type;

@end

@interface ICN_DynWarnView : UIView

@property (nonatomic , weak)id<ICN_DynWarnViewDelegate> delegate;
@property (nonatomic , assign , getter=isQuestionBtnForbid)BOOL forbidQestionClick; // 屏蔽掉提问按钮


- (instancetype)loadXibWarnViewsWithIcons:(NSArray<UIImage *> *)iconsArr
                              TitleLabels:(NSArray <NSString *> *)titleLabelArrs
                             TabbarHidden:(BOOL)hidden;


@end
