//
//  YSJ_SecondSectionCollectionViewCell.m
//  YingSiJie
//
//  Created by xiaoming on 17/03/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import "YSJ_SecondSectionCollectionViewCell.h"


@interface YSJ_SecondSectionCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *coverView;
@property (weak, nonatomic) IBOutlet UIButton *addImgBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteImgBtn;

@end

@implementation YSJ_SecondSectionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setImgModel:(NSData*)imgModel {
  
       // [self controlOtherItemUI];
        [self.imgView setImage:[UIImage imageWithData:imgModel]];
    
}

//MARK: - 控制cell内容显示
- (void)controlLastItemUI {
    self.imgView.hidden = YES;
    self.addImgBtn.hidden = NO;
    self.deleteImgBtn.hidden = YES;
    self.coverView.hidden = YES;
}

- (void)controlOtherItemUI {
    self.imgView.hidden = NO;
    self.addImgBtn.hidden = YES;
    self.deleteImgBtn.hidden = NO;
    self.coverView.hidden = NO;
}

#pragma mark - ||============ Event action ============||
- (IBAction)touchButtonAction:(UIButton *)sender {
    switch (sender.tag) {
        case 11: {
            //添加
            if (self.AddImgBlock) {
                self.AddImgBlock();
            }
        }
            break;
        case 12: {
            //删除
            if (self.DeleteBlock) {
                self.DeleteBlock(self);
            }
        }
            break;
        default:
            break;
    }
}

@end
