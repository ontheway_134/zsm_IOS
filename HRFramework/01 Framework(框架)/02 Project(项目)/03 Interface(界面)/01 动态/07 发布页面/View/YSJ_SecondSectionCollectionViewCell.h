//
//  YSJ_SecondSectionCollectionViewCell.h
//  YingSiJie
//
//  Created by xiaoming on 17/03/20.
//  Copyright © 2017年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YSJ_ComplainImageModel;

@interface YSJ_SecondSectionCollectionViewCell : UICollectionViewCell

/**
 图片 mdoel
 */
@property (strong, nonatomic) NSData *imgModel;

/**
 添加图片
 */
@property (copy, nonatomic) void(^AddImgBlock)();

/**
 删除
 */
@property (copy, nonatomic) void(^DeleteBlock)(YSJ_SecondSectionCollectionViewCell *cell);

- (void)controlLastItemUI;
- (void)controlOtherItemUI;
@end
