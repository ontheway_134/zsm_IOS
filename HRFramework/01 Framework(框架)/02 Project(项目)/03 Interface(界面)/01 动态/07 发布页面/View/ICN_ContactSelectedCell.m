//
//  ICN_ContactSelectedCell.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ContactSelectedCell.h"
#import "ICN_NewFrident.h"

@interface ICN_ContactSelectedCell ()

@property (nonatomic , strong)UILabel *p_UserNameLabel;
@property (nonatomic , strong)UIImageView *p_Imageview;
@property (nonatomic , strong)UIButton *p_SelectBtn;


@end

@implementation ICN_ContactSelectedCell

- (UIButton *)p_SelectBtn{
    if (_p_SelectBtn == nil) {
        _p_SelectBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_p_SelectBtn];
        [_p_SelectBtn setImage:[[UIImage imageNamed:@"未选择"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_p_SelectBtn setImage:[UIImage imageNamed:@"选择"] forState:UIControlStateSelected];
        [_p_SelectBtn addTarget:self action:@selector(clickOnSelectedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _p_SelectBtn.selected = NO;
        
    }
    return _p_SelectBtn;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _p_UserNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _p_UserNameLabel.userInteractionEnabled = YES;
        _p_UserNameLabel.text = @"测试 - 夏一灿";
        _p_UserNameLabel.font = [UIFont systemFontOfSize:13.0];
        [self.contentView addSubview:_p_UserNameLabel];
        
        _p_Imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"头像"]];
        _p_Imageview.userInteractionEnabled = YES;
        
        [self p_SelectBtn];
        
        [self.contentView addSubview:_p_Imageview];
    }
    return self;
}

- (void)setModel:(ICN_NewFrident *)model{
    _model = model;
    if (model) {
        self.p_SelectBtn.selected = _model.selected;
        self.p_UserNameLabel.text = _model.memberNick;
        [self.p_Imageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"头像"]];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self configCurrentViewUIWhileWillLayout];
}

// 在视图将要布局的时候添加约束
- (void)configCurrentViewUIWhileWillLayout{
    
    [self.p_Imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    self.p_Imageview.layer.cornerRadius = self.p_Imageview.size.width / 2.0;
    self.p_Imageview.layer.masksToBounds = YES;
    
    [self.p_UserNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.p_Imageview.mas_right).offset(10.0);
        make.centerY.equalTo(self.p_Imageview);
    }];
    
    [self.p_SelectBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10.0);
        make.centerY.equalTo(self.contentView);
    }];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark - ---------- IBAction ----------
- (void)clickOnSelectedBtnAction:(UIButton *)sender{
    // 执行相关操作
}

@end
