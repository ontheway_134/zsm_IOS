//
//  ICN_UserPostMsgListPagerViewController.m
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserPostMsgListPagerViewController.h"

#pragma mark --- 业务处理ViewModel ---
#import "ICN_DynamicStateListViewModel.h" // 动态的ViewModel
#import "ICN_ComplainListViewModel.h" // 吐槽详情的ViewModel
#import "ICN_QuestionViewModel.h"     // 提问的ViewModel

#pragma mark --- 跳转的详情页面 ---
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_TransmitToDynamicPager.h"  // 转发到动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 动态详情页面
#import "ICN_ComplainDetialPager.h"    // 吐槽详情页面
#import "ICN_OthersQuestionDetialPager.h" // 别人的回答详情页面
#import "ICN_MyQuestionDetialPager.h" // 自己的回答详情页面
#import "ICN_UserReportDetialVC.h"    // 举报页面

#pragma mark --- Cell的样式 ---
#import "ICN_ComplainContentCell.h" // 吐槽列表页Cell 180（默认高度）
#import "ICN_UserListQuestionContentCell.h" // 提问列表页Cell 210（默认高度）
#import "ICN_NewReplyToStateCell.h" // 新的转发类型的Cell
#import "ICN_CommonPsersonDynamicCell.h" // 动态的显示样式Cell

#pragma mark --- Model的样式 ---
#import "ICN_DynStateContentModel.h" // 动态的Model
#import "ICN_ComplainListModel.h" // 吐槽列表的Model
#import "ICN_QuestionListModel.h" // 提问列表Model

#pragma mark --- 友盟分享相关 ---
#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model


@interface ICN_UserPostMsgListPagerViewController ()<UITableViewDelegate , UITableViewDataSource , ComplainListDelegate , QuestionConfigurationDelegate , ICN_DynamicStateListDelegate , ICN_DynWarnViewDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; // 标题
@property (weak, nonatomic) IBOutlet UILabel *noContentMsgLabel; // 没有内容信息的提示标签
@property (weak, nonatomic) IBOutlet UIButton *dynamicStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *wisdonStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *complainStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *askActionStateBtn;


// 用户计算位置的背景
@property (weak, nonatomic) IBOutlet UIView *delertSegmentView; // 菜单分隔栏 -- 下面的部分就是tableView
@property (weak, nonatomic) IBOutlet UIView *selectedDelertBackGround; // 选项卡背景板


#pragma mark - ---------- 其他属性 ----------

@property (nonatomic , strong)UIButton *currentSelectedBtn; // 当前选中的按钮
@property (nonatomic , strong)UITableView *tableView; // 当前页面的tableView
@property (nonatomic , strong)UIView *buttonSignView; // 设置按钮下方的切换栏
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 将动态转发为动态用Model
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作
@property (nonatomic , assign , getter=isFooterRefresh)BOOL footerRefresh; // 底部是否刷新方法 默认是否

#pragma mark --- 网络代理 ---
@property (nonatomic , strong) ICN_DynamicStateListViewModel *dynamicViewModel; // 动态 / 智讯网络代理
@property (nonatomic , strong)ICN_ComplainListViewModel *complainViewModel; // 吐槽列表的ViewModel
@property (nonatomic , strong)ICN_QuestionViewModel *questionViewModel; // 提问列表的ViewModel



@end

@implementation ICN_UserPostMsgListPagerViewController

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])]; // 注册吐槽Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserListQuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])]; // 注册提问Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NewReplyToStateCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])]; // 注册新动态转发Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])]; // 注册动态Cell
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
        // 设置在tableview的数据显示不全的时候不显示多余的行分割的方法
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        // 使用约束设置tableView
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.delertSegmentView.mas_bottom);
            make.bottom.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
        }];
        // 配置刷新加载
        [self configTableViewRefreshHeaderFooterView];
    }
    return _tableView;
}

- (UIView *)buttonSignView{
    if (_buttonSignView == nil) {
        _buttonSignView = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonSignView.backgroundColor = RGB0X(0x009dff);
        [self.selectedDelertBackGround addSubview:_buttonSignView];
    }
    return _buttonSignView;
}



#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.footerRefresh = NO;
    // 设置隐藏默认的导航栏
    [self setHiddenDefaultNavBar:YES];
    // 设置ViewModel
    self.complainViewModel = [[ICN_ComplainListViewModel alloc] init];
    self.complainViewModel.delegate = self;
    self.questionViewModel = [[ICN_QuestionViewModel alloc] init];
    self.questionViewModel.delegate = self;
    self.dynamicViewModel = [[ICN_DynamicStateListViewModel alloc] init];
    self.dynamicViewModel.delegate = self;
    // 配置tableView
    [self tableView];
    // 配置默认的选中按钮
    UIButton *button = [self.selectedDelertBackGround viewWithTag:self.currentSelectedTag];
    // 获取到正确的button的时候将其设置为选中状态
    if (button) {
        button.selected = YES;
        self.currentSelectedBtn = button;
    }else{
        HRLog(@"error===未获取到正确的切换按钮");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置默认的选中的按钮下面的切换选项卡
    [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
        make.bottom.equalTo(self.selectedDelertBackGround);
        make.centerX.equalTo(self.currentSelectedBtn);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}

// 根据选中的按钮的tag来切换选项卡的方法 -- 忽略页面刚载入时的处理
- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    // 选中按钮的时候先显示tableview
    self.tableView.hidden = NO;
    // 根据tag计算选中的是哪个按钮 - 并将其他按钮的选中状态改变
    if (tag != self.currentSelectedBtn.tag) {
        // 两次选中的按钮的tag不一样
        
        // 1. 设置之前选中的按钮取消选择
        self.currentSelectedBtn.selected = NO;
        // 2. 判断选中的按钮并设置currentSelectedTag
        self.currentSelectedTag = tag;
        // 3. 设置选中的按钮的状态
        switch (tag) {
            case PRIVATE_DynamicBtn:{
                self.dynamicStateBtn.selected = YES;
                self.currentSelectedBtn = self.dynamicStateBtn;
                break;
            }
            case PRIVATE_WisdomBtn:{
                self.wisdonStateBtn.selected = YES;
                self.currentSelectedBtn = self.wisdonStateBtn;
                break;
            }
            case PRIVATE_TopicBtn:{
                self.complainStateBtn.selected = YES;
                self.currentSelectedBtn = self.complainStateBtn;
                [self.tableView reloadData];
                break;
            }
            case PRIVATE_AskActionBtn:{
                self.askActionStateBtn.selected = YES;
                self.currentSelectedBtn = self.askActionStateBtn;
                [self.tableView reloadData];
                break;
            }
            default:
                break;
        }
        
        // 4. 设置切换选中状态的标签的跟随移动
        [UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.buttonSignView.center;
            center.x = self.currentSelectedBtn.centerX;
            self.buttonSignView.center = center;
        } completion:^(BOOL finished) {
            [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.selectedDelertBackGround);
                make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
                make.centerX.equalTo(self.currentSelectedBtn);
            }];
        }];
    }
    
    // 6. 根据选中的内容刷新页面
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            switch (self.currentSelectedTag) {
                case PRIVATE_DynamicBtn:{
                    // 先设置不是 ta的智讯
                    self.dynamicViewModel.mainWisdom = YES;
                    //此时刷新的是 ta的动态
                    self.dynamicViewModel.mainDynamic = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel refreshCurrentPageContentCells];
                    break;
                }
                case  PRIVATE_WisdomBtn:{
                    //此时刷新的是智讯
                    self.dynamicViewModel.mainWisdom = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel refreshCurrentPageContentCells];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 刷新吐槽操作
                    [self.complainViewModel requestComplainListWithType:Complain_ResponseUserIDList isLoad:NO MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                case PRIVATE_AskActionBtn:{
                    // 刷新提问列表操作
                    [self.questionViewModel requestQuestionListWithType:Question_ResponseUserIdList isLoad:NO MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                default:
                    break;
            }
            
            
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        if (!self.isFooterRefresh) {
            self.footerRefresh = YES;
            switch (self.currentSelectedTag) {
                case PRIVATE_DynamicBtn:{
                    // 首相设置不是 ta的智讯
                    self.dynamicViewModel.mainDynamic = YES;
                    //此时加载的是 ta的动态
                    self.dynamicViewModel.mainWisdom = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel loadNextPageContentCells];
                    break;
                }
                case  PRIVATE_WisdomBtn:{
                    //此时加载的是智讯
                    self.dynamicViewModel.mainWisdom = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel loadNextPageContentCells];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 加载吐槽操作
                    [self.complainViewModel requestComplainListWithType:Complain_ResponseUserIDList isLoad:YES MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                case PRIVATE_AskActionBtn:{
                    // 加载提问列表操作
                    [self.questionViewModel requestQuestionListWithType:Question_ResponseUserIdList isLoad:YES MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                default:
                    break;
            }
        }
    }];
    
    // 设置footer的标识
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


// 根据需要字段获取动态的Cell的方法
- (UITableViewCell *)requestCurrentDynamicCellWithTableView:(UITableView *)tableview Model:(ICN_DynStateContentModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    // 确认显示动态的样式是转发样式的类型
    if ([model.fromId integerValue] != 0) {
        // 存在则证明是最新类型的Model
        ICN_NewReplyToStateCell *cell = [tableview dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
        cell.model = model;
        [cell callBackWithNewDynReplyCellBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag, BOOL isLikeUp) {
            // 根据tag判断点击的按钮
            switch (btnTag) {
                case ICN_CellLikeActionBtnType:{
                    // 点赞操作 1：点赞，2：取消点赞
                    if (model.isPraise.integerValue == 0) {
                        // 进行点赞操作
                        [self.dynamicViewModel likeUpWithType:1 Model:model];
                    }else{
                        // 进行取消点赞操作
                        [self.dynamicViewModel likeUpWithType:2 Model:model];
                    }
                    break ;
                }
                case ICN_CellReportBtnType:{
                    // 举报操作
                    [self jumpToReportDetialPagerWithModel:model];
                    break ;
                }
                case ICN_CellCommentBtnType:{
                    // 评论操作
                    //相关逻辑 - 跳转到评论页面
                    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                    pager.model = model;
                    [self currentPagerJumpToPager:pager];
                    break ;
                }
                case ICN_CellReplayType:{
                    // 转发操作
                    // 1. 获取转发需要的两种Model
                    self.replayModel = model;
                    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                    // 2. 对于智讯的title进行处理
                    if (model.title == nil) {
                        self.transmitModel.title = @"i行动态";
                    }
                    // 3. 展开转发页面
                    [self.view addSubview:self.warnView];
                    break;
                }
                    // 点击下拉箭头的方法
                case ICN_CellPullBtnType:{
                    [self.tableView reloadData];
                    break ;
                }
                default:
                    break;
            }
        }];
        return cell;
    }else{
        
        ICN_CommonPsersonDynamicCell *cell = [tableview dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
        cell.model = model;
        // 添加对于Cell上点击事件的处理
        [cell callWhileCellBtnClick:^(NSInteger SenderTag, ICN_DynStateContentModel *model) {
            switch (SenderTag) {
                case ICN_CellLikeActionBtnType:{
                    NSInteger likeType = 0;
                    if (model.isPraise.integerValue == 0) {
                        likeType = 1;
                    }else{
                        likeType = 2;
                    }
                    [self.dynamicViewModel likeUpWithType:likeType Model:model];
                    break;
                }
                case ICN_CellCommentBtnType:{
                    //相关逻辑 - 跳转到动态详情页面
                    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                    pager.model = self.dynamicViewModel.modelsArr[indexPath.row];
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case ICN_CellReviewBtnType:{
                    // 转发
                    // 1. 获取转发需要的两种Model
                    self.replayModel = model;
                    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                    // 2. 对于智讯的title进行处理
                    if (model.title == nil) {
                        self.transmitModel.title = @"i行动态";
                    }
                    // 3. 展开转发页面
                    [self.view addSubview:self.warnView];
                    break;
                }
                case ICN_CellReportBtnType:{
                    // 举报操作
                    [self jumpToReportDetialPagerWithModel:model];
                    break ;
                }
                case ICN_CellPullBtnType:{
                    // 文本框下拉操作
                    [self.tableView reloadData];
                    break;
                }
                    
                default:
                    break;
            }
        }];
        [cell setListStyle:YES];
        return cell;
        
    }
    return nil;
}


// 举报跳转方法
- (void)jumpToReportDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    NSString *errorMsg ;
    if (model.canReport) {
        if ([model.canReport integerValue] == 1) {
            errorMsg = @"无法举报自己";
        }
        if ([model.canReport integerValue] == 2) {
            errorMsg = @"已经举报了";
        }
    }
    if (errorMsg) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:errorMsg];
    }else{
        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
        pager.matterId = model.matterId;
        [self currentPagerJumpToPager:pager];
    }
}


#pragma mark - ---------- IBAction ----------

// 点击返回按钮
- (IBAction)clickOnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 选中切换状态按钮的方法
- (IBAction)clickOnStateSelectedBtnAction:(UIButton *)sender {
    // 根据按钮的tag进行对应的切换操作
    [self changeSelectedContentLabelStatusWithSenderTage:sender.tag];
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            // 判断是自己的动态不转发
            if (self.replayModel != nil && self.replayModel.isMe.integerValue == 1) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无法转发自己的动态"];
                return ;
            }
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}

#pragma mark --- 动态与智讯相关代理 ---

- (void)responseWithDynamicStateListRequestSuccess:(BOOL)success Code:(NSInteger)code{
    // 设置尾视图刷新状态为否
    self.footerRefresh = NO;
    if (success) {
        if (code == 0 || code == 5) {
            [self.tableView reloadData];
        }
        if (code == 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该用户未登录"];
        }
    }else{
        // 网络请求失败
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败 - 请稍后重试"];
    }
    // 如果刷新失败 隐藏tableview
    switch (self.currentSelectedTag) {
        case PRIVATE_DynamicBtn:{
            if (self.dynamicViewModel.modelsArr.count == 0) {
                self.tableView.hidden = YES;
            }
            break ;
        }
        case PRIVATE_WisdomBtn:{
            if (self.dynamicViewModel.wisdomModelsArr.count == 0) {
                self.tableView.hidden = YES;
            }
            break ;
        }
        default:
            break;
    }
    
    // 第一步 结束tableview的刷新
    [self endRefreshWithTableView:self.tableView];
}

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"点赞失败"];
    }else{
        [self.tableView reloadData];
    }
}

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您无法为该用户点赞"];
}

/** 已经点赞后的回调 */
- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经为该用户点过赞"];
    [self.tableView reloadData];
}


#pragma mark --- 提问代理 ---
- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Question_ResponseUserIdList:{
            // 设置尾视图刷新状态为否
            self.footerRefresh = NO;
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 如果刷新失败 隐藏tableview
                if (self.questionViewModel.modelsArr.count == 0) {
                    self.tableView.hidden = YES;
                }
                [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}

#pragma mark --- 吐槽代理 ---
- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (!success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseDisLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (!success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseUserIDList:{
            // 设置尾视图刷新状态为否
            self.footerRefresh = NO;
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 如果刷新失败 隐藏tableview
                if (self.complainViewModel.modelsArr.count == 0) {
                    self.tableView.hidden = YES;
                }
                [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    switch (self.currentSelectedTag) {
        case PRIVATE_DynamicBtn:{
            //相关逻辑 - 跳转到动态详情页面
            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
            pager.model = self.dynamicViewModel.modelsArr[indexPath.row];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_WisdomBtn:{
            //相关逻辑 - 跳转到智讯详情页面
            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
            pager.model = self.dynamicViewModel.wisdomModelsArr[indexPath.row];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_TopicBtn:{
            // 吐槽详情
            ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
            pager.complainId = [self.complainViewModel.modelsArr[indexPath.row] complainId];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_AskActionBtn:{
            // 提问详情
            // 获取到需要的Model
            ICN_QuestionListModel *model = self.questionViewModel.modelsArr[indexPath.row];
            if (model.isMine != nil && [model.isMine integerValue] == 0) {
                // 是我发布的
                ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                pager.constentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }else{
                // 不是我发布的
                ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                pager.contentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }
            break ;
        }
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (self.currentSelectedTag) {
        case PRIVATE_TopicBtn:{
            // 吐槽
            return self.complainViewModel.modelsArr.count;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 提问
            return self.questionViewModel.modelsArr.count;
            break;
        }case PRIVATE_WisdomBtn:{
            return self.dynamicViewModel.wisdomModelsArr.count;
            break;
        }
        case PRIVATE_DynamicBtn:{
            return self.dynamicViewModel.modelsArr.count;
            break;
        }
        default:
            break;
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据选中按钮的tag来判断返回的是什么内容
    switch (self.currentSelectedBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 吐槽
            ICN_ComplainContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
            if (self.complainViewModel.modelsArr.count > indexPath.row) {
                cell.model = self.complainViewModel.modelsArr[indexPath.row];
            }
            [cell callBackWithCellBlock:^(ICN_ComplainListModel *model, NSInteger selectedType, BOOL isSelected) {
                switch (selectedType) {
                        // 点赞 / 取消点赞操作
                    case ICN_CellLikeActionBtnType:{
                        if (isSelected) {
                            [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }else{
                            [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseDisLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }
                        break ;
                    }
                        // 评论操作
                    case ICN_CellCommentBtnType:{
                        ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
                        pager.complainId = model.complainId;
                        [self currentPagerJumpToPager:pager];
                        break ;
                    }
                        // 转发操作
                    case ICN_CellReplayType:{
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:model.complainId Title:model.RandomName IconUrl:model.RandomPicture Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.warnView];
                        break ;
                        break ;
                    }
                        
                    default:
                        break;
                }
            }];
            return cell;
            break ;
        }
        case PRIVATE_AskActionBtn:{
            // 提问
            ICN_UserListQuestionContentCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])];
            if (self.questionViewModel.modelsArr.count > indexPath.row) {
                cell.model = self.questionViewModel.modelsArr[indexPath.row];
            }
            [cell callBackQuestionCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isSelected) {
                switch (buttonType) {
                    case ICN_CellReplayType:{
                        // 转发类型
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"questionId" ModelId:model.questionId Title:@"i行提问" IconUrl:model.memberLogo Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.warnView];
                    }
                        
                    default:
                        break;
                }
            }];
            return cell;
            break ;
        }
        case PRIVATE_DynamicBtn:
        case PRIVATE_WisdomBtn:{
            // 1. 获取到合理的数据源
            NSMutableArray *modelsArr ;
            if (self.currentSelectedBtn.tag == PRIVATE_DynamicBtn) {
                // 设置当前获取到动态的数据源
                modelsArr = self.dynamicViewModel.modelsArr;
            }else{
                // 设置当前获取到的是智讯的数据源
                modelsArr = self.dynamicViewModel.wisdomModelsArr;
            }
            // 根据数据源去部署Cell
            if (modelsArr.count > indexPath.row) {
                return [self requestCurrentDynamicCellWithTableView:tableView Model:modelsArr[indexPath.row] IndexPath:indexPath];
            }
            break;
        }
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

/** 逐条调节tableView的Cell高度 */
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    switch (self.currentSelectedBtn.tag) {
//        case PRIVATE_DynamicBtn:{
//            return 120;
//            break;
//        }
//        case PRIVATE_WisdomBtn:{
//            return 120;
//            break;
//        }
//        case PRIVATE_TopicBtn:{
//            // 返回吐槽的高度
//            return 180;
//            break;
//        }
//        case PRIVATE_AskActionBtn:{
//            // 返回提问的高度
//            return 210;
//            break;
//        }
//        default:
//            return 120;
//            break;
//    }
//
//    return 120;
//}





@end
