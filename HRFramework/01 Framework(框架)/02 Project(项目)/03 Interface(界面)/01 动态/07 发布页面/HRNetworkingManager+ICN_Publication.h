//
//  HRNetworkingManager+ICN_Publication.h
//  ICan
//
//  Created by albert on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager.h"

#pragma mark - ---------- Model ----------
#import "ICN_CommonImagesLoadModel.h" // 多张图片Model
#import "BaseOptionalModel.h" // 通用可选Model - 用于result无实际意义的时候
#import "ICN_NewFrident.h" // 好友列表的Model

typedef void(^WisdomUpLoadBlock)(BaseOptionalModel *model);
typedef void(^UpLoadImageBlock)(ICN_CommonImagesLoadModel *model);
typedef void(^ComFriendsBlcok)(NSMutableArray <ICN_NewFrident *> *modelsArr);

@interface HRNetworkingManager (ICN_Publication)

// 发布智讯接口
+ (void)postWisdomMessagePublicationTextContentWithToken:(NSString *)token
                                                   Title:(NSString *)title
                                                 Content:(NSString *)content
                                                     Pic:(NSString *)pic
                                                 Success:(WisdomUpLoadBlock)success
                                                 Failure:(ErrorBlock)failure;


// 发布动态接口
/** 
 1. token  用户密令		是
 2. content 动态内容		是
 3. pic    图片路径		否
 4. notIds  禁止查看用户ID（用逗号分隔）  否
 */
+ (void)postDynamicStatePublicationWithContent:(NSString *)content
                                           Pic:(NSString *)pics
                                        NotIds:(NSString *)notIds
                                       Success:(WisdomUpLoadBlock)success
                                       Failure:(ErrorBlock)failure;

// 获取用于标识谁不可看的好友列表
/** 
 1. token  用户密令  是
 2. status 好友状态 (2 已审核好友)  是
 3. page 显示第几个页面  是
 */
+ (void)requestCurrentUserFriendsWithPage:(NSInteger)page
                                  Success:(ComFriendsBlcok)success
                                  Failure:(ErrorBlock)failure;


// 多图上传接口
+ (void)uploadImagesWithImageArr:(NSArray <UIImage *>*)imageArr
                         Success:(UpLoadImageBlock)success
                         Failure:(ErrorBlock)failure;

// 单图上传接口
+ (void)uploadImagesWithImage:(UIImage*)image
                         Success:(UpLoadImageBlock)success
                         Failure:(ErrorBlock)failure;


@end
