//
//  ICN_ContactCanNotSeeVC.h
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_DynamicStatePublicationViewModel;
@interface ICN_ContactCanNotSeeVC : BaseViewController

@property (nonatomic , strong)ICN_DynamicStatePublicationViewModel *viewModel;


@end
