//
//  ICN_TopicPublicationVC.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_TopicPublicationVC.h"
#import "JKImagePickerController.h"
#import "TZImageManager.h"
#import "ICN_WisdomMessagePublicationViewModel.h"
#import "YSJ_SecondSectionCollectionViewCell.h"
static const NSInteger ICN_TopicImageAddMaxCount = 6; // 默认添加图片的最大数量

@interface ICN_TopicPublicationVC ()<JKImagePickerControllerDelegate , UITextFieldDelegate , UITextViewDelegate , ICN_WisdomMessagePublicationDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITextField *p_TitleTextF; // 标题文本框

@property (weak, nonatomic) IBOutlet UITextView *p_ContentTextView; // 正文文本域

@property (weak, nonatomic) IBOutlet UIScrollView *p_ImageScroll; // 图片选择器背景

@property (weak, nonatomic) IBOutlet UILabel *p_ImageCountLabel; // 限制选择图片数量label

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;

#pragma mark - ---------- 私有属性 ----------

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic , strong)JKImagePickerController *p_ImagePickerVC; // 跳转的图片选择器页面vc
@property (nonatomic , strong)NSMutableArray * imageListArr; // 当前图片列表数组
@property (nonatomic , strong)UIButton *addImageBtn; // 添加图片用btn
@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewListArr; // 当前相框列表

@property (nonatomic , strong)ICN_WisdomMessagePublicationViewModel *viewmodel;
@property (nonatomic, strong) NSMutableArray <NSData*>* imaDataArray;

@end

@implementation ICN_TopicPublicationVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_WisdomMessagePublicationViewModel *)viewmodel{
    if (_viewmodel == nil) {
        _viewmodel = [[ICN_WisdomMessagePublicationViewModel alloc] init];
        _viewmodel.delegate = self;
    }
    return _viewmodel;
}

- (UIButton *)addImageBtn{
    if (_addImageBtn == nil) {
        _addImageBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
        [_addImageBtn setBackgroundImage:[[UIImage imageNamed:@"加"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_p_ImageScroll addSubview:_addImageBtn];
        [_addImageBtn addTarget:self action:@selector(clickImagePickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageBtn;
}

- (NSMutableArray <NSData*>*)imaDataArray{
    if (!_imaDataArray) {
        _imaDataArray = [[NSMutableArray alloc]init];
        [_imaDataArray insertObject:[NSData data] atIndex:0];
    }
    return _imaDataArray;
}
- (NSMutableArray *)imageListArr{
    if (_imageListArr == nil) {
        _imageListArr = [NSMutableArray array];
    }
    return _imageListArr;
}

- (NSMutableArray<UIImageView *> *)imageViewListArr{
    if (_imageViewListArr == nil) {
        _imageViewListArr = [NSMutableArray array];
    }
    return _imageViewListArr;
}




#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewmodel];
    self.naviTitle = @"发布";
    self.p_TitleTextF.delegate = self;
    self.p_ContentTextView.delegate = self;
    // Do any additional setup after loading the view from its nib.
    [self.collectionView registerNib:[UINib nibWithNibName:@"YSJ_SecondSectionCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"YSJ_SecondSectionCollectionViewCell"];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self addImageBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnPublishAction:(UIButton *)sender {
    
    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    // 判断有没有标题，没有标题不允许上传
    if (self.p_TitleTextF.text == nil || [self.p_TitleTextF.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加标题后再发布智讯"];
    }else{
        if (self.p_ContentTextView.text == nil || [self.p_ContentTextView.text isEqualToString:@"正文内容"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加正文后再发布智讯"];
        }else{
            // 设置在该次请求有结果之前button不能点击
            sender.userInteractionEnabled = NO;
            if (self.imageListArr.count > 0) {
                [self.viewmodel upLoadImages:self.imageListArr];
                [MBProgressHUD ShowProgressToSuperView:self.view Message:@"智讯上传中"];
            }else{
                // 纯文本发布 - 测试
                [self postSingleTextWisdomMessageWithInfo:nil];
                [MBProgressHUD ShowProgressToSuperView:self.view Message:@"正在发布"];
            }
        }
    }
}

#pragma mark - ---------- 私有方法 ----------

- (void)postSingleTextWisdomMessageWithInfo:(NSDictionary *)info{
    NSString *token = @"ssss";
    NSString *title = self.p_TitleTextF.text;
    NSString *content = self.p_ContentTextView.text;
    NSString *pic = [info valueForKey:@"fileName"];
    [self.viewmodel postWisdomMessageWithToken:token Title:title Content:content Pic:pic];
}

- (void)clickImagePickAction:(UIButton *)sender{
    [self presentViewController:self.p_ImagePickerVC animated:YES completion:nil];
}



#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_WisdomMessagePublicationDelegate ---

- (void)responseWithMessageUploadSuccess:(BOOL)isSuccess ErrorCode:(NSInteger)errorCode Info:(NSDictionary *)info{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.publishBtn.userInteractionEnabled = YES;
    if (errorCode == ICN_FileLoadSuccess) {
        // 获取图片上传成功
        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"内容上传中"];
        // 开始post智讯内容
        [self postSingleTextWisdomMessageWithInfo:info];
        
    }
    
    switch (errorCode) {
        case ICN_NetSuccess:
        case ICN_FileLoadSuccess:
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"上传成功"];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case ICN_PostFailureWithNetState:
        case ICN_LoadFaileFailureWithNetState:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
            break;
        }
        case ICN_PostFailureWithMessageError:
        case ICN_LoadFaileFailureWithMessageError:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"服务器异常"];
            break;
        }
        default:
            break;
    }
    
    
}


#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.text == nil || [textField.text isEqualToString:@""]) {
        textField.text = @"";
        textField.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text == nil || [textField.text isEqualToString:@""]) {
        textField.text = nil;
        textField.textColor = RGB0X(0xb6b6b6);
    }
}




#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView isFirstResponder]) {
        if ([textView.text isEqualToString:@"正文内容"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"正文内容";
        textView.textColor = RGB0X(0xb6b6b6);
    }
}

#pragma mark --- TZImagePickerControllerDelegate ---

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.imaDataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WEAK(weakSelf);
    
    YSJ_SecondSectionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"YSJ_SecondSectionCollectionViewCell" forIndexPath:indexPath];
    if (!cell) {
        cell = [YSJ_SecondSectionCollectionViewCell new];
    }
    if (indexPath.row == 6) {
        [cell setHidden:YES];
    } else {
        [cell setHidden:NO];
    }
    if (_imaDataArray.count - indexPath.row == 1) {
        [cell controlLastItemUI];
    }else{
        [cell controlOtherItemUI];
    }
    cell.imgModel = self.imaDataArray[indexPath.row];
    cell.AddImgBlock = ^{
        //添加
        //            [self openPhotoAlbum];
        JKImagePickerController *picker = [[JKImagePickerController alloc] init];
        picker.selectMaxCount = (6 - (int)self.imageListArr.count) < 0 ? 0 : 6 - (int)self.imageListArr.count;
        picker.JKDelegate = self;
        [self presentViewController:picker animated:YES completion:nil];
    };
    cell.DeleteBlock = ^(YSJ_SecondSectionCollectionViewCell *cell){
        //删除
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        [weakSelf.imaDataArray removeObjectAtIndex:indexPath.row];
        if (weakSelf.imageListArr.count) {
            [weakSelf.imageListArr removeObjectAtIndex:indexPath.row];
        }
        [weakSelf.collectionView reloadData];
    };
    return cell;
    
}



- (void)imagePickerController:(JKImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    
    [photos enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSData *fileData = UIImageJPEGRepresentation(obj, 0.2);
        [self.imaDataArray insertObject:fileData atIndex:0];
        
        
        [self.collectionView reloadData];
        
    }];
    [self.imageListArr addObjectsFromArray:photos];
}




@end
