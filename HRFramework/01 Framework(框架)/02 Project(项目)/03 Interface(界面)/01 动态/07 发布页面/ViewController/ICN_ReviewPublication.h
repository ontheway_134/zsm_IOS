//
//  ICN_ReviewPublication.h
//  ICan
//
//  Created by albert on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_DynStateContentModel;
@class ICN_YouMengShareModel; // 标准转发Model 在未对页面完全替换之前暂时不删除原Model
@interface ICN_ReviewPublication : BaseViewController

@property (nonatomic , strong)ICN_DynStateContentModel *model;
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;

@end
