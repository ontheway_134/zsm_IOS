//
//  ICN_WisdomMessagePublicationViewModel.m
//  ICan
//
//  Created by albert on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_WisdomMessagePublicationViewModel.h"
#import "HRNetworkingManager+ICN_Publication.h" // 发布网络请求的类目


@implementation ICN_WisdomMessagePublicationViewModel

- (void)upLoadImages:(NSArray<UIImage *> *)images{
    
    [HRNetworkingManager uploadImagesWithImageArr:images Success:^(ICN_CommonImagesLoadModel *model) {
        if (model.code == 0) {
            // 成功后将路径信息传递过去
            HRLog(@"图片上传 - 网络请求成功");
            [self responseDelegateWithSuccess:YES ErrorCode:ICN_FileLoadSuccess Info:model.result];
            HRLog(@"图片上传 - 图片上传成功--图片路径通过info回传");
        }else{
            [self responseDelegateWithSuccess:NO ErrorCode:ICN_LoadFaileFailureWithMessageError Info:nil];
            HRLog(@"图片上传 - 图片上传失败");
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self responseDelegateWithSuccess:NO ErrorCode:ICN_PostFailureWithNetState Info:errorInfo];
        HRLog(@"图片上传 - 网络请求失败");
    }];
}


- (void)postWisdomMessageWithToken:(NSString *)token Title:(NSString *)title Content:(NSString *)content Pic:(NSString *)pic{
    
    [HRNetworkingManager postWisdomMessagePublicationTextContentWithToken:token Title:title Content:content Pic:pic Success:^(BaseOptionalModel *model) {
        HRLog(@"发布智讯 - 网络请求发送成功");
        if (model.code == 0) {
            HRLog(@"发布智讯 - 上传到服务器成功");
            [self responseDelegateWithSuccess:YES ErrorCode:ICN_NetSuccess Info:nil];
        }else{
            [self responseDelegateWithSuccess:NO ErrorCode:ICN_PostFailureWithMessageError Info:nil];
            HRLog(@"发布智讯 - 上传到服务器失败");
        }
        
        
    } Failure:^(NSDictionary *errorInfo) {
        HRLog(@"发布智讯 - 网络请求失败");
        [self responseDelegateWithSuccess:NO ErrorCode:ICN_PostFailureWithNetState Info:errorInfo];
    }];

}


- (BOOL)responseDelegateWithSuccess:(BOOL)isSuccess ErrorCode:(NSInteger)code Info:(NSDictionary *)info{
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithMessageUploadSuccess:ErrorCode:Info:)]) {
            [self.delegate responseWithMessageUploadSuccess:isSuccess ErrorCode:code Info:info];
        }
        return YES;
    }
    HRLog(@" -- 请添加智讯页面代理");
    return NO;
}

@end
