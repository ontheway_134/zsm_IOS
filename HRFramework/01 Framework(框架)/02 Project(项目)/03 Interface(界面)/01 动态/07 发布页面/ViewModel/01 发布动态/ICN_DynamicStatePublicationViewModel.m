//
//  ICN_DynamicStatePublicationViewModel.m
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStatePublicationViewModel.h"
#import "ICN_CommonImagesLoadModel.h"
#import "HRNetworkingManager+ICN_Publication.h"

@interface ICN_DynamicStatePublicationViewModel ()

@property (nonatomic , assign)NSInteger currentPage;
@end
@implementation ICN_DynamicStatePublicationViewModel
#pragma mark - ---------- 懒加载 ----------
- (NSMutableArray<ICN_NewFrident *> *)friendModelsArr{
    if (_friendModelsArr == nil) {
        _friendModelsArr = [NSMutableArray array];
    }
    return _friendModelsArr;
}
- (NSMutableArray<ICN_NewFrident *> *)selectedFriendModelsArr{
    if (_selectedFriendModelsArr == nil) {
        _selectedFriendModelsArr = [NSMutableArray array];
    }
    return _selectedFriendModelsArr;
}
- (void)updateSelectedFriendModelsList{
    if (_selectedFriendModelsArr) {
        [self.selectedFriendModelsArr removeAllObjects];
    }
    for (ICN_NewFrident *model in self.friendModelsArr) {
        if (model.isSelected) {
            [self.selectedFriendModelsArr addObject:model];
        }
    }
}
- (void)refreshCurrentUserFriendsList{
    self.currentPage = 1;
    [HRNetworkingManager requestCurrentUserFriendsWithPage:1 Success:^(NSMutableArray<ICN_NewFrident *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            // 数据获取成功
            if (_friendModelsArr == nil) {
                [self.friendModelsArr addObjectsFromArray:modelsArr];
            }else{
                [_friendModelsArr removeAllObjects];
                [self.friendModelsArr addObjectsFromArray:modelsArr];
            }
            [self callBackWithUserFrendsListRequestCode:0 Info:nil];
        }else{
            [self callBackWithUserFrendsListRequestCode:modelsArr.firstObject.code Info:modelsArr.firstObject.info];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithUserFrendsListRequestCode:-1 Info:@"网络请求失败"];
    }];
}
- (void)loadCurrentUserFriendsList{
    if (self.currentPage >= 1 && self.currentPage < 1000) {
        self.currentPage ++;
    }else{
        self.currentPage = 1;
    }
    [HRNetworkingManager requestCurrentUserFriendsWithPage:self.currentPage Success:^(NSMutableArray<ICN_NewFrident *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            // 数据获取成功
            [self.friendModelsArr addObjectsFromArray:modelsArr];
            [self callBackWithUserFrendsListRequestCode:0 Info:nil];
        }else{
            [self callBackWithUserFrendsListRequestCode:modelsArr.firstObject.code Info:modelsArr.firstObject.info];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithUserFrendsListRequestCode:-1 Info:@"网络请求失败"];
    }];

}

- (void)callBackWithUserFrendsListRequestCode:(NSInteger)code Info:(NSString *)info{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithUserFriendsListSuccess:Error:)]) {
            if (code == 0) {
                [self updateSelectedFriendModelsList];
                [self.delegate responseWithUserFriendsListSuccess:YES Error:nil];
            }else{
                [self.delegate responseWithUserFriendsListSuccess:NO Error:info];
            }
        }
    }
}

- (BOOL)responseDelegateWithSuccess:(BOOL)isSuccess ErrorCode:(NSInteger)code Info:(NSDictionary *)info{
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithMessageUploadSuccess:ErrorCode:Info:)]) {
            [self.delegate responseWithMessageUploadSuccess:isSuccess ErrorCode:code Info:info];
        }
        return YES;
    }
    HRLog(@" -- 请添加动态发布页面代理");
    return NO;
}



- (void)upLoadImages:(NSArray<UIImage *> *)images{
    
    [HRNetworkingManager uploadImagesWithImageArr:images Success:^(ICN_CommonImagesLoadModel *model) {
        if (model.code == 0) {
            // 成功后将路径信息传递过去
            HRLog(@"图片上传 - 网络请求成功");
            [self responseDelegateWithSuccess:YES ErrorCode:ICN_FileLoadSuccess Info:model.result];
            HRLog(@"图片上传 - 图片上传成功--图片路径通过info回传");
        }else{
            [self responseDelegateWithSuccess:NO ErrorCode:ICN_LoadFaileFailureWithMessageError Info:nil];
            HRLog(@"图片上传 - 图片上传失败");
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self responseDelegateWithSuccess:NO ErrorCode:ICN_PostFailureWithNetState Info:errorInfo];
        HRLog(@"图片上传 - 网络请求失败");
    }];
}

- (void)postDynamicStateWithContent:(NSString *)content Pic:(NSString *)pic NotIds:(NSString *)notIds{
    
    if (content == nil) {
        return ;
    }
    [HRNetworkingManager postDynamicStatePublicationWithContent:content Pic:pic NotIds:notIds Success:^(BaseOptionalModel *model) {
        // 添加发布成功之后的操作
        if (model.code == 0) {
            [self responseDelegateWithSuccess:YES ErrorCode:ICN_NetSuccess Info:nil];
        }else{
            [self responseDelegateWithSuccess:YES ErrorCode:ICN_PostFailureWithMessageError Info:nil];
        }
    } Failure:^(NSDictionary *errorInfo) {
        // 网络请求错误之后执行的操作
        [self responseDelegateWithSuccess:NO ErrorCode:ICN_PostFailureWithNetState Info:nil];
    }];
}


@end
