//
//  ICN_DynamicStatePublicationViewModel.h
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICN_NewFrident.h"

@protocol ICN_DynamicStatePublicationDelegate <NSObject>

@optional
- (void)responseWithMessageUploadSuccess:(BOOL)isSuccess
                               ErrorCode:(NSInteger)errorCode
                                    Info:(NSDictionary *)info;

- (void)responseWithUserFriendsListSuccess:(BOOL)isSuccess
                                     Error:(NSString *)error;

@end

@interface ICN_DynamicStatePublicationViewModel : NSObject

@property (nonatomic , weak)id<ICN_DynamicStatePublicationDelegate> delegate;
@property (nonatomic , strong)NSMutableArray <ICN_NewFrident *>* friendModelsArr; // 用户好友Model列表
@property (nonatomic , strong)NSMutableArray <ICN_NewFrident *>* selectedFriendModelsArr; // 被选中的好友列表


- (void)postDynamicStateWithContent:(NSString *)content Pic:(NSString *)pic
                             NotIds:(NSString *)notIds;

// 刷新谁不可看列表
- (void)refreshCurrentUserFriendsList;

// 加载谁不可看列表
- (void)loadCurrentUserFriendsList;

// 更新谁不可看好友列表
- (void)updateSelectedFriendModelsList;


/** 上传图片数组的方法 */
- (void)upLoadImages:(NSArray <UIImage *>*)images;


@end
