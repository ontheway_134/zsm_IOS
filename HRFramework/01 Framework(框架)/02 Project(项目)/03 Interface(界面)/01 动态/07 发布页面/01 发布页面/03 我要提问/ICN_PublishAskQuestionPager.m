//
//  ICN_PublishAskQuestionPager.m
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PublishAskQuestionPager.h"
#import "BaseOptionalModel.h"

static NSString * const DefaultContent = @"请写下你的问题，并用问号结尾"; // 文本框的默认内容

@interface ICN_PublishAskQuestionPager ()<UITextViewDelegate , UITextFieldDelegate>

@property (nonatomic , assign)NSInteger  rewardPointsCount; // 积分数量

@property (weak, nonatomic) IBOutlet UIButton *navBackButton; // 自定义 - 导航栏返回按钮
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel; // 自定义导航栏标签
@property (weak, nonatomic) IBOutlet UITextField *titleTextField; // 输入标题文本框
@property (weak, nonatomic) IBOutlet UITextView *contentTextView; // 内容 - 文本域输入框
@property (weak, nonatomic) IBOutlet UIButton *payButton; // 打赏按钮
@property (weak, nonatomic) IBOutlet UIButton *publishButton; // 用于发布的按钮
@property (weak, nonatomic) IBOutlet UITextField *freePayTextView; // 设置打赏积分的输入文本框

@end

@implementation ICN_PublishAskQuestionPager

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置默认的积分数量是0
    self.rewardPointsCount = -1;
    [self setHiddenDefaultNavBar:YES];
    self.contentTextView.delegate = self;
    self.freePayTextView.delegate = self;
    // 1. 获取用户积分数据 -- 默认设置如果五秒钟后没有回复的话退出当前页面
    [self requestForUserRewardPoints];
    // 2. 在获取积分之前一直弹出遮罩
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"获取积分"];
    
    // 5秒钟后还没有获取到用户积分数据则退出发布页面
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        
        // 延时后根据积分情况确认是否返回上级页面
        if (self.rewardPointsCount == -1) {
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"获取积分失败"];
            [self.navigationController popViewControllerAnimated:YES];
        }
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---------- 私有方法 ----------

// 请求积分数据
- (void)requestForUserRewardPoints{
    // 2. 判断token 没有token返回上级页面
    NSDictionary *params;
    if (![USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        params = @{@"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken]};
    }
    [[HRRequest alloc] POST_PATH:PATH_MyRewardPoints params:params success:^(id result) {
        
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            // 返回数据可用
            self.rewardPointsCount = [[[result valueForKey:@"result"] valueForKey:@"score"] integerValue];
            // 修改积分holder
            self.freePayTextView.placeholder = SF(@"打赏积分不超过%ld分",self.rewardPointsCount);
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"获取积分失败"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        
        // 积分获取失败直接返回页面
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"获取积分失败"];
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

// 发布提问数据的接口
- (void)publishQuestionWithNetRequest{
    
    // 判断需要提交的积分数值是否正确
    if ([self.freePayTextView.text integerValue] > self.rewardPointsCount) {
        [self showRewardPointsAlertaction];
        return ;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    [params setValue:self.titleTextField.text forKey:@"title"];
    [params setValue:self.contentTextView.text forKey:@"contant"];
    // 设置在积分没有添加的时候默认为0
    if (self.freePayTextView.text == nil || [self.freePayTextView.text isEqualToString:@""]) {
        [params setValue:@"0" forKey:@"score"];
    }else{
        [params setValue:self.freePayTextView.text forKey:@"score"];
    }

    [[HRRequest alloc] POST_PATH:PATH_PublishQuestion params:params success:^(id result) {
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"发布成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:baseModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"网络请求失败"];
    }];
}


// 展示积分信息提示窗
- (void)showRewardPointsAlertaction{
    
    UIAlertController *warnController = [UIAlertController alertControllerWithTitle:@"积分数量" message:SF(@"您输入的积分数量不能超过%ld",self.rewardPointsCount) preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 在点击完成之后弹出键盘
        [self.freePayTextView becomeFirstResponder];
    }];
    [warnController addAction:action];
    [self presentViewController:warnController animated:YES completion:nil];
    
}


#pragma mark - ---------- IBAction ----------
// 点击当前页面按钮的交互
- (IBAction)clickOnIButtonAction:(UIButton *)sender {
    
    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    
    if ([sender isEqual:self.navBackButton]) {
        // 点击的是返回按钮
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([sender isEqual:self.payButton]) {
        // 触发积分提示
        [self showRewardPointsAlertaction];
    }
    if ([sender isEqual:self.publishButton]) {
        // 点击的是发布按钮
        if (self.titleTextField.text == nil || [self.titleTextField.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入标题"];
            return ;
        }
        if ([self.contentTextView.text isEqualToString:DefaultContent] || [self.contentTextView.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入有效内容"];
            return ;
        }
        // 进行提交操作
        [self publishQuestionWithNetRequest];
    }
    
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- 输入框代理 ---

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    // 判断在输入打赏积分的时候只能输入数字
    if ([textField isEqual:self.freePayTextView]) {
        for (NSInteger i = 0; i < string.length; i = i+1) {
            if ([string characterAtIndex:i] <= '9' && [string characterAtIndex:i] >= '0') {
                //test 设置文本输入的时候不能输入超过数值的内容 == 用于控制积分
                if ([SF(@"%@%c",textField.text , [string characterAtIndex:i]) integerValue] > 500) {
                    return NO;
                }
                return YES;
            }
            return NO;
        }
    }
    return YES;
}

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}




@end
