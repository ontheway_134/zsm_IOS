//
//  ICN_TransmitToDynamicPager.m
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_TransmitToDynamicPager.h"
#import "ICN_CommonReplayViewModel.h" // 通用转发到动态ViewModel （智讯和动态转发到动态除外）

static NSString * const DefaultContent = @"这一刻的想法..."; // 文本框的默认内容

@interface ICN_TransmitToDynamicPager ()<UITextViewDelegate , CommonReplayDelegate>

#pragma mark --- IBProperty ---
@property (weak, nonatomic) IBOutlet UIButton *navBackButton; // 自定义 - 导航栏返回按钮
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel; // 自定义导航栏标签
@property (weak, nonatomic) IBOutlet UITextView *contentTextView; // 内容 - 文本域输入框
@property (weak, nonatomic) IBOutlet UIButton *publishButton; // 用于发布的按钮

// 转发内容
@property (weak, nonatomic) IBOutlet UIImageView *transmitContentIcon; // 转发内容的图片
@property (weak, nonatomic) IBOutlet UILabel *transmitContentLabel; // 转发内容正文

#pragma mark --- 逻辑属性 ---
@property (nonatomic , strong)ICN_CommonReplayViewModel *viewModel; // 页面逻辑处理ViewModel
@property (nonatomic , assign)ICN_ReplayType replyType; // 原内容的类型
@property (nonatomic , copy)NSString *contentId; // 原内容的id
@property (nonatomic , copy)NSString *content; // 原内容正文
@property (nonatomic , copy)NSString *iconStr; //头像网址

@end

@implementation ICN_TransmitToDynamicPager

- (instancetype)initWithReplyType:(ICN_ReplayType)type ContentId:(NSString *)contentId Content:(NSString *)content IconStr:(NSString *)icon{
    self = [super init];
    if (self) {
        _replyType = type;
        _contentId = contentId;
        _content = content;
        _iconStr = icon;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏默认导航栏
    [self setHiddenDefaultNavBar:YES];
    // 根据转发类型设置转发标题
    switch (_replyType) {
        case REPLAY_ComplainReplay:
            self.navTitleLabel.text = @"转发吐槽";
            break;
        case REPLAY_QuestionReplay:
            self.navTitleLabel.text = @"转发提问";
            break;
        case REPLAY_ActivityReplay:
            self.navTitleLabel.text = @"转发活动";
            break;
        case REPLAY_IndustryInfoReplay:
            self.navTitleLabel.text = @"转发行业资讯";
            break;
        case REPLAY_PositionReplay:
            self.navTitleLabel.text = @"转发职位";
            break;
        case REPLAY_LiveReplay:
            self.navTitleLabel.text = @"转发直播";
            break;
        default:
            break;
    }
    // 设置textview代理
    self.contentTextView.delegate = self;
    // 设置ViewModel
    self.viewModel = [[ICN_CommonReplayViewModel alloc] init];
    self.viewModel.delegate = self;
    // 设置内容
    NSString *title;
    NSString *content;
    if (_replyType == REPLAY_ComplainReplay) {
        // 吐槽的
        title = @"来自吐槽";
        content = SF(@"%@\n%@",title , _content ? _content : @"内容");
    }else if (_replyType == REPLAY_QuestionReplay){
        // 提问的
        title = @"来自提问";
        content = SF(@"%@\n%@",title , _content ? _content : @"内容");
    }else if (_content) {
        title = [[_content componentsSeparatedByString:@","] firstObject];
        content = [_content stringByReplacingOccurrencesOfString:SF(@"%@,",title) withString:SF(@"%@\n",title)];
    }else{
        self.transmitContentLabel.text = @"内容";
    }
    // 在判断有分行内容的时候分行显示
    if (title) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:content];
        [attributedString addAttribute:NSFontAttributeName// 字体
                                 value:[UIFont boldSystemFontOfSize:14.0]
                                 range:[content rangeOfString:SF(@"%@",title)]];
        [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                                 value:[UIColor blackColor]
                                 range:[content rangeOfString:SF(@"%@",title)]];
        self.transmitContentLabel.attributedText = attributedString;
        
    }
    [self.transmitContentIcon sd_setImageWithURL:[NSURL URLWithString:_iconStr] placeholderImage:[UIImage imageNamed:@"占位图"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------

// 点击了导航栏上的功能键按钮
- (IBAction)clickOnNavButtonAction:(UIButton *)sender {
    if ([sender isEqual:self.navBackButton]) {
        // 点击的是返回按钮
        [self.navigationController popViewControllerAnimated:YES];
    }
}

// 点击了发布按钮
- (IBAction)clickOnPublishBtnAction:(UIButton *)sender {
    // 进行发布逻辑的对应操作
    // 1. 先判断内容是否存在
    if (self.contentTextView.text) {
        // 当内容为默认字段的时候将默认字段清空
        if ([self.contentTextView.text isEqualToString:DefaultContent]) {
            self.contentTextView.text = @"";
        }
    }
    [self.viewModel requestWithEnumType:self.replyType MemberId:self.contentId Summary:self.contentTextView.text];
    // 添加转发中的MBP
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"转发中"];
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- viewModel代理 ---

// 获取到数据处理之后的回调
- (void)responseWithEnumType:(ICN_ReplayType)enumType Success:(BOOL)success Info:(NSString *)info{
    [MBProgressHUD hiddenHUDWithSuperView:self.view];
    [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:info];
    if (success) {
        // 发布转发内容成功 -- 退出当前页面
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}





@end
