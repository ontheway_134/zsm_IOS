//
//  ICN_TransmitToDynamicPager.h
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_TransmitToDynamicPager : BaseViewController

/**
 * ReplyType == 转发内容的类型
 * ContentId == 转发内容的id
   内容转发到动态的转发页面  初始化并注入必要属性
 */
- (instancetype)initWithReplyType:(ICN_ReplayType)type ContentId:(NSString *)contentId Content:(NSString *)content IconStr:(NSString *)icon;

@end
