//
//  ICN_CommonReplayViewModel.h
//  ICan
//
//  Created by albert on 2017/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CommonReplayDelegate <NSObject>

// 获取到数据处理之后的回调
- (void)responseWithEnumType:(ICN_ReplayType)enumType Success:(BOOL)success Info:(NSString *)info;

@end

@interface ICN_CommonReplayViewModel : NSObject

@property (nonatomic , weak)id<CommonReplayDelegate> delegate;

// 根据网络请求的类型以及用户的memeberId 进行对应的操作
- (void)requestWithEnumType:(ICN_ReplayType)enumType MemberId:(NSString *)memberId Summary:(NSString *)summary;



@end

