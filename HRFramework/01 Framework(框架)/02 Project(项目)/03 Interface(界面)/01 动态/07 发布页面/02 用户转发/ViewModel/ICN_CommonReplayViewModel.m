//
//  ICN_CommonReplayViewModel.m
//  ICan
//
//  Created by albert on 2017/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CommonReplayViewModel.h"
#import "BaseOptionalModel.h" // 基类基本信息Model

@implementation ICN_CommonReplayViewModel


- (void)requestWithEnumType:(ICN_ReplayType)enumType MemberId:(NSString *)memberId Summary:(NSString *)summary{
    
    // 第一步 判断token 此时不对token不存在的后续操作做任何校验
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        return ;
    }
    
    // 第二步 根据不同的网络请求类型生成不同的路径 和传入参数
    NSString *netPath ;
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    // 在参数字典中设置转发详情
    [params setValue:summary forKey:@"summary"];
    switch (enumType) {
        case REPLAY_DynamicReplay:{
            // 转发动态 到动态
            netPath = PATH_ReplyDynamicState;
            
            break;
        }
        case REPLAY_WisdomReplay:{
            // 转发智讯 到动态
            netPath = PATH_ReplyDynamicState;
            
            break;
        }
        case REPLAY_ComplainReplay:{
            // 转发吐槽 到动态
            netPath = PATH_ReplyComplainToDYN;
            [params setValue:memberId forKey:@"complaintsId"];
            break;
        }
        case REPLAY_QuestionReplay:{
            // 转发提问 到动态
            netPath = PATH_ReplyQuestionToDYN;
            [params setValue:memberId forKey:@"questionId"];
            break;
        }
        case REPLAY_ActivityReplay:{
            // 转发活动 到动态
            [params setValue:memberId forKey:@"activityId"];
            [params setValue:@"1" forKey:@"status"];
            netPath = PATH_ReplyLiveORActToDYN;
            break;
        }
        case REPLAY_LiveReplay:{
            // 转发直播 到动态
            [params setValue:memberId forKey:@"activityId"];
            [params setValue:@"2" forKey:@"status"];
            netPath = PATH_ReplyLiveORActToDYN;
            break;
        }
        case REPLAY_PositionReplay:{
            // 转发职位 到动态
            [params setValue:memberId forKey:@"releaseId"];
            netPath = PATH_ReplyPositionToDYN;
            break;
        }
        case REPLAY_IndustryInfoReplay:{
            // 转发行业资讯 到动态
            [params setValue:memberId forKey:@"informationId"];
            netPath = PATH_ReplyIndustryInfoToDYN;
            break;
        }
        default:
            break;
    }
    
    // 在确认ViewModel支持回调数据的时候添加网络请求
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
        // 开启网络请求并针对数据进行处理
        [[HRRequest manager] POST_PATH:netPath params:params success:^(id result) {
            // 1. 判断是否成功获取到数据
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                
                // 网络请求的结果成功
                // 进行数据处理成功的回调
                [self.delegate responseWithEnumType:enumType Success:YES Info:baseModel.info];
                
            }else{
                [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:enumType Success:NO Info:@"网络请求失败"];
        }];
    }else{
        HRLog(@"test=== 用户主页的更多信息没有被设置代理");
    }
    
    
    
}



@end

