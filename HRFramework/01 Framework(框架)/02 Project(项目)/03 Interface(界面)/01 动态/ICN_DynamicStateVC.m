//
//  ICN_DynamicStateVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#pragma mark --- 常用宏 ---
static CGFloat const ImageFooterHeight = 60.0; // 当前带有图片的尾视图的基准高度(去掉图片背景图高度后的)
#pragma mark --- 逻辑处理与类目头文件 ---
#import "ICN_DynamicStateVC.h"
#import "ICN_ShareManager.h"                    // 分享管理器
#import "ICN_YouMengShareTool.h"                // 分享工具类
#import <UMSocialCore/UMSocialPlatformConfig.h> // 友盟官方枚举头文件
#import "ICN_DynamicStateHeader.h"              // 动态的头文件
#import "ICN_ComplainListViewModel.h"           // 吐槽的ViewModel
#import "ICN_QuestionViewModel.h"               // 提问的ViewModel
#import "ICN_NearestPeopleViewModel.h"          // 周边雷达管理器
#import "ICN_DynamicStateFirstPagerViewModel.h" // 首页ViewModel
#import "ICN_DynamicStateVC+HXLogin.h"          // 环信登录类目
#import "HRNetworkingManager+DynFirstPager.h"   // 网络请求类目
#import "UILabel+ICN_ShowEmoji.h"

#pragma mark --- Model相关头文件 ---
#import "ICN_DynStateADModel.h"      // 广告专用Model
#import "ICN_DynStateContentModel.h" // 动态正文Model
#import "ICN_LocationModel.h"        // 城市选择列表Model
#import "ICN_ApplyModel.h"           // 友盟分享用Model
#import "ICN_ComplainListModel.h"    // 吐槽列表Model
#import "ICN_QuestionListModel.h"    // 提问列表Model
#import "ICN_YouMengShareModel.h"    // 转发专用类型转化Model


#pragma mark --- CellOrView相关头文件 ---
#import "ICN_QuestionContentCell.h"         // 首页提问Cell列表
#import "ICN_ComplainContentCell.h"         // 首页吐槽Cell列表
#import "ICN_DynStateTextSpreadCell.h"      // 可拓展文本长度的Cell
#import "ICN_ReplyingDynStateCell.h"        // 转发内容Cell
#import "ICN_NewReplyToStateCell.h"     // 新形式的转发的Cell
#import "ICN_DynStateSectionHeaderView.h"   // 默认section的头视图
#import "ICN_DynStateCompanySectionHeader.h"// 公司推广section的头视图
#import "ICN_DynStateSectionNormalFooter.h" // 默认section的尾视图
#import "ICN_DynStateSectionImagesFooter.h" // 带有图片的section尾视图
#import "ICN_DynStateSingleAdTextView.h"    // 单行广告头视图
#import "ICN_DynLocationPickerView.h"       // 首页城市选择器

#pragma mark - ---------- 跳转页面/视图 头文件 ----------
#import "ICN_DynWarnView.h"               // 信息提示窗界面
#import "ICN_PicBroswerView.h"            // 图片浏览器简化版
#import "ICN_UserHomePagerVC.h"           // 用户主页界面
//#import "ICN_StartViewController.h"       // 引导页
#import "ICN_GuiderPager.h"               // 新版引导页
#import "ICN_SignViewController.h"        // 用户登录页面（由游客登录跳转）
#import "ICN_DynSearchPagerVC.h"          // 搜索页面
#import "ICN_TopicPublicationVC.h"        // 跳转到发布话题页面
#import "ICN_DynamicStatePublicationVC.h" // 跳转到发布动态页面
#import "ICN_PublishComplainPager.h"      // 跳转到发布吐槽页面
#import "ICN_PublishAskQuestionPager.h"   // 跳转到发布提问页面
#import "ICN_TransmitToDynamicPager.h"    // 其他内容转发到动态页面
#import "ICN_ReviewPublication.h"         // 转发到I行动态页面
#import "ICN_UserDynamicStateDetialVC.h"  // 跳转到动态评论页面
#import "ICN_UserReportDetialVC.h"        // 举报页面
#import "ICN_CompanyDetialVC.h"           // 公司详情
#import "ICN_ComplainDetialPager.h"       // 跳转到吐槽详情页面
#import "ICN_MyQuestionDetialPager.h"     // 跳转到我的提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 跳转到用户的提问详情页面
#import "ICN_TransmitToDynamicPager.h" // 其他内容转发到动态页面
#import "ICN_MyfridentViewController.h"   //分享到I行好友

@interface ICN_DynamicStateVC ()<UITableViewDelegate , UITableViewDataSource , ICN_DynWarnViewDelegate , DynamicStateFirstPagerViewModelDelegate , DynLocationPickerViewDelegate,ComplainListDelegate , QuestionConfigurationDelegate>

#pragma mark - ---------- IBProperty ----------
@property (weak, nonatomic) IBOutlet UILabel *p_CurrentCityLabel; // 当前城市标签
@property (weak, nonatomic) IBOutlet UILabel *p_SearchContextTextF; // 搜索文本框
@property (weak, nonatomic) IBOutlet UITableView *tableView; // 详情内容列表
@property (weak, nonatomic) IBOutlet UIView *breakLineView; // 动态/智讯的背景分隔栏
// 选项卡按钮·（动态、智讯、话题、提问）
@property (weak, nonatomic) IBOutlet UIButton *dynamicSelectedBtn; // 选中动态按钮
@property (weak, nonatomic) IBOutlet UIButton *wisdomSelectedBtn; // 选中智讯按钮
@property (weak, nonatomic) IBOutlet UIButton *topicSelectedBtn; // 选中话题按钮
@property (weak, nonatomic) IBOutlet UIButton *askActionSelectedBtn; // 选中提问按钮
@property (nonatomic , strong) UIButton *selectedStateBtn; // 选中的状态按钮 - 默认是动态的
@property (nonatomic , assign , getter=isViewAppearAgain)BOOL viewAppearAgain; // 视图是否是再次出现（是否是只走viewwillappear）

#pragma mark - ---------- 视图/控件属性 ----------

@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗
@property (nonatomic , strong)ICN_DynLocationPickerView *citySelectedView; // 城市选择器
@property (nonatomic , strong)UIView *buttonSignView; // 用于button状态切换的uiview
@property (nonatomic , strong)ICN_PicBroswerView *pictureView; // 图片浏览视图
@property (nonatomic , strong)ICN_SignViewController *loginPager; // 登录页面

#pragma mark - ---------- 其他属性 ----------
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 想要转发的数据
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel; // 转发用Model
@property (nonatomic , strong)ICN_DynamicStateFirstPagerViewModel *viewModel; // 动态与智讯的ViewModel
@property (nonatomic , strong)ICN_ComplainListViewModel *complainViewModel; // 吐槽相关的ViewModel
@property (nonatomic , strong)ICN_QuestionViewModel *questionViewModel; // 提问相关的ViewModel
@property (nonatomic , assign , getter=isUserLogin)BOOL userLoginIn; // 用户是否登录
@property (nonatomic , assign , getter=isFooterLoading)BOOL mj_footerLoading; // 判断当前的mj_footer是否处于刷新状态
@property (nonatomic , strong)NSMutableArray *modelsArr; // Model数组
@property (nonatomic , strong)NSMutableArray <ICN_LocationModel *>* locationModelsArr;
#pragma mark --- 约束 ---
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;

@end

@implementation ICN_DynamicStateVC

#pragma mark - ---------- Lazy Loading（懒加载） ----------

- (UIView *)buttonSignView{
    if (_buttonSignView == nil) {
        _buttonSignView = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonSignView.backgroundColor = RGB0X(0x009dff);
        [self.breakLineView addSubview:_buttonSignView];
    }
    return _buttonSignView;
}

- (ICN_SignViewController *)loginPager{
    if (_loginPager == nil) {
        _loginPager = [[ICN_SignViewController alloc] init];
    }
    return _loginPager;
}

/** 根据用户的token判断用户是否是游客登录状态的方法 */
- (BOOL)isUserLoginIn{
    _userLoginIn = [self getCurrentUserLoginStatus];
    if (_userLoginIn == NO) {
        // 切换现在游客登录也不会默认跳转到智讯页面
    }
    return _userLoginIn;
}

/** 每次判断用户是否是游客登录之前都走一遍校验 */
- (BOOL)isUserLogin{
    // 自动赋值
    [self isUserLoginIn];
    return _userLoginIn;
}

- (NSMutableArray<ICN_LocationModel *> *)locationModelsArr{
    if (_locationModelsArr == nil) {
        _locationModelsArr = [NSMutableArray array];
    }
    return _locationModelsArr;
}

- (ICN_PicBroswerView *)pictureView{
    if (_pictureView == nil) {
        _pictureView = [[ICN_PicBroswerView alloc] initWithFrame:SCREEN_BOUNDS];
        _pictureView.hidden = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_pictureView];
    }
    return _pictureView;
}

- (ICN_DynLocationPickerView *)citySelectedView{
    if (_citySelectedView == nil && _locationModelsArr != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _citySelectedView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _citySelectedView.locationsArr = self.locationModelsArr;
        _citySelectedView.frame = frame;
        _citySelectedView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:_citySelectedView];
    }
    return _citySelectedView;
}

- (NSMutableArray *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

// 判定是企业用户的时候屏蔽掉发布提问的功能
- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:@[[UIImage imageNamed:@"发布动态"] , [UIImage imageNamed:@"发布智讯"]] TitleLabels:@[@"发布动态" , @"发布智讯"] TabbarHidden:NO];
        _warnView.delegate = self;
        if (self.isEnterpriseLogin) {
            // 隐藏提问按钮
            _warnView.forbidQestionClick = YES;
        }else{
            _warnView.forbidQestionClick = NO;
        }
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:NO];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}

#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置第一次加载的时候页面将要再次出现属性为no
    self.viewAppearAgain = NO;
    // 设置页面刚加载时候的mj_footer处于未刷新状态
    self.mj_footerLoading = NO;
    // 动态按钮默认为选中状态
    self.dynamicSelectedBtn.selected = YES;
    // 设置第一次进入页面的时候页面默认显示的按钮是动态按钮
    self.selectedStateBtn = self.dynamicSelectedBtn;
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    // 获取到用户是不是游客登录状态
    [self isUserLoginIn];
    // 配置ViewModel
    [self configViewModelWhileViewDidLoad];
    // 设置tableView的默认属性
    [self configTableviewWhileViewDidLoad];
    // 获取地理信息网络数据
    [self configLocationPickerWhileViewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isViewAppearAgain) {
        [self.tableView.mj_header beginRefreshing];
    }
    self.leftConstraint.constant = ICN_KScreen(67.0);
    self.rightConstraint.constant = ICN_KScreen(67.0);
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor whiteColor];  //改自己喜欢的颜色
    
    // 添加现在页面的四个选项卡下面的分隔栏的设置 -- 设置是位于选中按钮的居中
    [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
        make.bottom.equalTo(self.breakLineView);
        make.centerX.equalTo(self.selectedStateBtn);
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.viewAppearAgain = YES;
    if (_warnView) {
        [_warnView removeFromSuperview];
        _warnView = nil;
    }
    if (_replayView) {
        [_replayView removeFromSuperview];
        _replayView = nil;
    }
    if (_citySelectedView) {
        [_citySelectedView removeFromSuperview];
        _citySelectedView = nil;
    }
}

- (void)dealloc{
    if (_warnView) {
        _warnView.delegate = nil;
    }
    if (_replayView) {
        _replayView.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - ---------- Private Methods（私有方法） ----------

// 对于ViewModel的处理
- (void)configViewModelWhileViewDidLoad{
    // 初始化ViewModel
    self.viewModel = [[ICN_DynamicStateFirstPagerViewModel alloc] init];
    self.viewModel.delegate = self;
    self.complainViewModel = [[ICN_ComplainListViewModel alloc] init];
    self.complainViewModel.delegate = self;
    self.questionViewModel = [[ICN_QuestionViewModel alloc] init];
    self.questionViewModel.delegate = self;
}

// 对于tableview的配置
- (void)configTableviewWhileViewDidLoad{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 1)];
    self.tableView.sectionHeaderHeight = 1.0;
    [self configTableViewRegisterNessaryView];
    [self configTableViewRefreshHeaderFooterView];
}

// 对于地址选择器的配置
- (void)configLocationPickerWhileViewDidLoad{
    if (_locationModelsArr) {
        [_locationModelsArr removeAllObjects];
    }
    [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            [self.locationModelsArr addObjectsFromArray:modelsArr];
        }
        self.citySelectedView.hidden = YES;
    } Failure:^(NSDictionary *errorInfo) {
    }];
}

// 配置页面的tableview的刷新头尾视图
- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 任意刷新之后自动回复mj_footer显示
            self.tableView.mj_footer.hidden = NO;
            
            // 判断调用的ViewModel以及对应的逻辑处理
            switch (self.selectedStateBtn.tag) {
                case PRIVATE_DynamicBtn:
                case  PRIVATE_WisdomBtn:{
                    [self.viewModel refreshCurrentPageContentCells];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 刷新吐槽操作
                    [self.complainViewModel requestComplainListWithType:Complain_ResponseOthersList isLoad:NO MemberId:nil SearchContent:nil];
                    break ;
                }
                case PRIVATE_AskActionBtn:{
                    // 刷新提问列表操作
                    [self.questionViewModel requestQuestionListWithType:Question_ResponseOtherList isLoad:NO MemberId:nil SearchContent:nil];
                    break ;
                }
                default:
                    break;
            }
            
            
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        switch (self.selectedStateBtn.tag) {
            case PRIVATE_DynamicBtn:
            case  PRIVATE_WisdomBtn:{
                // 对于智讯的加载做线程安全和结束刷新重置状态的判断
                @synchronized (self) {
                    if (!self.isFooterLoading) {
                        [self.viewModel loadNextPageContentCells];
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [self.tableView.mj_footer endRefreshing];
                            self.mj_footerLoading = NO;
                        });
                    }
                }
                break ;
            }
            case PRIVATE_TopicBtn:{
                // 加载吐槽操作
                [self.complainViewModel requestComplainListWithType:Complain_ResponseOthersList isLoad:YES MemberId:nil SearchContent:nil];
                break ;
            }
            case PRIVATE_AskActionBtn:{
                // 加载提问列表操作
                [self.questionViewModel requestQuestionListWithType:Question_ResponseOtherList isLoad:YES MemberId:nil SearchContent:nil];
                break ;
            }
            default:
                break;
        }
        
    }];
    // 设置footer的标识
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    // 设置默认的隐藏
    [self.tableView.mj_header beginRefreshing];
}

// 举报跳转方法
- (void)jumpToReportDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    NSString *errorMsg ;
    if (model.canReport) {
        if ([model.canReport integerValue] == 1) {
            errorMsg = @"无法举报自己";
        }
        if ([model.canReport integerValue] == 2) {
            errorMsg = @"已经举报了";
        }
    }
    if (errorMsg) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:errorMsg];
    }else{
        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
        pager.matterId = model.matterId;
        [self currentPagerJumpToPager:pager];
    }
}

// 设置tableview的结束刷新动画方法
- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MJRefreshAutoNormalFooter *footer = (MJRefreshAutoNormalFooter *)tableView.mj_footer;
        [footer endRefreshing];
    });
}
#pragma mark --- 对于约定Cell的获取 ---

/** 获取标准类型的动态的正文部分Cell */
- (UITableViewCell *)requestWithCommonContentCellWith:(UITableView *)tableView Model:(ICN_DynStateContentModel *)model{
    if (model.isTransmit.integerValue == 1) {
        ICN_ReplyingDynStateCell *cell = (ICN_ReplyingDynStateCell *)[tableView dequeueReusableCellWithIdentifier:SF(@"%@" , ICN_ReplyingDynStateCellConstStr)];
        cell.model = model;
        return cell;
    }else{
        ICN_DynStateTextSpreadCell *cell = (ICN_DynStateTextSpreadCell *)[tableView dequeueReusableCellWithIdentifier:SF(@"%@" , ICN_SpreadTextCellConstStr)];
        cell.model = model;
        [cell callBackWithSpreadBlock:^(BOOL spread) {
            [self.tableView reloadData];
        }];
        return cell;
    }
}
/** 获取新的转发类型的Cell  （动态） */
- (UITableViewCell *)requestWithNewReplyCellWith:(UITableView *)tableView Model:(ICN_DynStateContentModel *)model{
    // 存在则证明是新转发类型的Model
    ICN_NewReplyToStateCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
    cell.model = model;
    [cell callBackNewIconTapBlock:^(ICN_DynStateContentModel *model) {
        // 先判断信息补全
        if ([self jumpToUserMessageSetUpPager]) {
            // 判断若需要跳转到补全页面则结束当前所有操作
            return ;
        }
        if (self.isUserLogin) {
            //相关逻辑 - 根据用户角色判断跳转的是用户主页还是公司详情
            if ([model.roleId integerValue] == 1) {
                // 跳转到用户主页
                ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
                pager.memberId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }else{
                ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                pager.companyId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }
        }else{
            self.hidesBottomBarWhenPushed = YES;
            [self currentPagerJumpToPager:self.loginPager];
        }
    }];
    [cell callBackWithNewDynReplyCellBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag, BOOL isLikeUp) {
        // 先判断信息补全
        if ([self jumpToUserMessageSetUpPager]) {
            // 判断若需要跳转到补全页面则结束当前所有操作
            return ;
        }
        // 在用户token不存在的时候 - 跳转到登录注册页面
        if (self.isUserLogin == NO) {
            self.hidesBottomBarWhenPushed = YES;
            [self currentPagerJumpToPager:self.loginPager];
            return ;
        }
        // 根据tag判断点击的按钮
        switch (btnTag) {
            case ICN_CellLikeActionBtnType:{
                // 点赞操作 1：点赞，2：取消点赞
                if (model.isPraise.integerValue == 0) {
                    // 进行点赞操作
                    [self.viewModel likeUpWithType:1 Model:model];
                }else{
                    // 进行取消点赞操作
                    [self.viewModel likeUpWithType:2 Model:model];
                }
                break ;
            }
            case ICN_CellReportBtnType:{
                // 举报操作
                [self jumpToReportDetialPagerWithModel:model];
                break ;
            }
            case ICN_CellCommentBtnType:{
                // 评论操作
                //相关逻辑 - 跳转到评论页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = model;
                [self currentPagerJumpToPager:pager];
                break ;
            }
            case ICN_CellReplayType:{
                // 转发操作
                // 1. 获取转发需要的两种Model
                self.replayModel = model;
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                // 2. 对于智讯的title进行处理
                if (model.title == nil) {
                    self.transmitModel.title = @"i行动态";
                }
                // 3. 展开转发页面
                [self.view addSubview:self.replayView];
                break;
                break ;
            }
            default:
                break;
        }
    }];
    return  cell;
}
/** 获取吐槽类型的Cell */
- (UITableViewCell *)requestWithComplainCellWith:(UITableView *)tableView Model:(ICN_ComplainListModel *)model{
    ICN_ComplainContentCell *testCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
    testCell.model = model;
    // 添加回调block的交互
    [testCell callBackWithCellBlock:^(ICN_ComplainListModel *model, NSInteger selectedType, BOOL isSelected) {
        // 先判断信息补全
        if ([self jumpToUserMessageSetUpPager]) {
            // 判断若需要跳转到补全页面则结束当前所有操作
            return ;
        }
        // 先测试能否进行其他操作 - 即判断是否是游客登录
        if (![self isUserLogin]) {
            self.hidesBottomBarWhenPushed = YES;
            [self currentPagerJumpToPager:self.loginPager];
            return ;
        }
        switch (selectedType) {
                // 点赞 / 取消点赞操作
            case ICN_CellLikeActionBtnType:{if (isSelected) {
                [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
            }else{
                [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseDisLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
            }
                break ;
            }
                // 评论操作
            case ICN_CellCommentBtnType:{
                // 跳转到详情页面
                ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
                pager.complainId = model.complainId;
                [self currentPagerJumpToPager:pager];
                break ;
            }
                // 转发操作
            case ICN_CellReplayType:{
                // 1. 生成转发Model
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:model.complainId Title:model.RandomName IconUrl:model.RandomPicture Content:model.contant];
                // 2. 弹出转发窗口
                [self.view addSubview:self.replayView];
                break ;
            }
            default:{
                break;
            }
        }
    }];
    return testCell;
}

/** 获取提问类型的Cell */
- (UITableViewCell *)requestWithQuestionCellWith:(UITableView *)tableView Model:(ICN_QuestionListModel *)model{
    ICN_QuestionContentCell *testCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
    // 根据用户是否是企业用户进行相关的操作
    testCell.EnterpriseUser = [self isCurrentEnterpriseUser];
    testCell.model = model;
    testCell.firstPager = YES;
    // 添加点击到用户主页的手势
    [testCell callBackQuestionIconTapBlock01:^(ICN_QuestionListModel *model) {
        // 先判断信息补全
        if ([self jumpToUserMessageSetUpPager]) {
            // 判断若需要跳转到补全页面则结束当前所有操作
            return ;
        }
        if (self.isUserLogin) {
            //相关逻辑 - 根据用户角色判断跳转的是用户主页还是公司详情
            ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
            pager.memberId = model.memberId;
            [self currentPagerJumpToPager:pager];
        }else{
            self.hidesBottomBarWhenPushed = YES;
            [self currentPagerJumpToPager:self.loginPager];
        }
        return ;
    }];
    
    [testCell callBackCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isSelected) {
        // 先判断信息补全
        if ([self jumpToUserMessageSetUpPager]) {
            // 判断若需要跳转到补全页面则结束当前所有操作
            return ;
        }
        // 先测试能否进行其他操作 - 即判断是否是游客登录
        if (![self isUserLogin]) {
            [self currentPagerJumpToPager:self.loginPager];
            return;
        }
        switch (buttonType) {
                // 收藏 / 取消收藏操作
            case ICN_CellCollectType:{if (isSelected) {
                [self.questionViewModel requestQuestionFunctionWithType:Question_ResponseLikeUp QuestionId:model.questionId Content:nil AnswerId:nil];
            }else{
                [self.questionViewModel requestQuestionFunctionWithType:Question_ResponseDisLikeUp QuestionId:model.questionId Content:nil AnswerId:nil];
            }
                break ;
            }
                // 转发操作
            case ICN_CellReplayType:{
                //warn=== 提问的转发接口不存在需要修改
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_QuestionReplay ParamsKey:@"questionId" ModelId:model.questionId Title:@"i行提问" IconUrl:model.memberLogo Content:model.contant];
                // 2. 弹出转发窗口
                [self.view addSubview:self.replayView];
                break ;
            }
                // 回答操作 == 跳转到详情页面
            case ICN_CellAnswerType:{
                // 需要判断是跳转到别人的提问还是自己的提问
                if ([model.isMine integerValue] == 0) {
                    // 是我发布的
                    ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                    pager.constentId = model.questionId;
                }else{
                    // 别人发布的
                    ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                    pager.contentId = model.questionId;
                    [self currentPagerJumpToPager:pager];
                }
                break ;
            }
                
            default:{
                break;
            }
        }
        
    }];
    return testCell;
}


/** 根据Model的详情跳转个人详情
 1. 动态详情页面
 2. 公司详情页面
 */
- (void)jumpToPersonalDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    // 先判断信息补全
    if ([self jumpToUserMessageSetUpPager]) {
        // 判断若需要跳转到补全页面则结束当前所有操作
        return ;
    }
    if (model != nil) {
        if (model.roleId.integerValue == 1) {
            // 个人主页
            ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
            pager.memberId = model.memberId;
            if (self.isUserLogin) {
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
        }else{
            // 企业主页
            if (self.isUserLogin) {
                ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                pager.companyId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
        }
    }
}

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                // 动态和智讯类型还是之前的跳转操作
                ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
                pager.model = self.replayModel;
                [self currentPagerJumpToPager:pager];
                break;
            }
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
        // 在转发之后无论成功失败清空相关Model
        self.replayModel = nil;
        self.transmitModel = nil;
    }
}



#pragma mark config control（布局控件）

/** 在tableView中注册必要的Cell */
- (void)configTableViewRegisterNessaryView{
    
    // 注册提问和吐槽Cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_QuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
    // 注册新的转发Cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NewReplyToStateCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
    
    UINib *nibCellOne = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateTextSpreadCell class]) bundle:nil];
    [self.tableView registerNib:nibCellOne forCellReuseIdentifier:SF(@"%@", ICN_SpreadTextCellConstStr)];
    UINib *nibCellTwo = [UINib nibWithNibName:NSStringFromClass([ICN_ReplyingDynStateCell class]) bundle:nil];
    [self.tableView registerNib:nibCellTwo forCellReuseIdentifier:SF(@"%@", ICN_ReplyingDynStateCellConstStr)];
    UINib *headerOne = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionHeaderView class]) bundle:nil];
    [self.tableView registerNib:headerOne forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_DefaultSectionHeaderConstStr)];
    UINib *headerTwo = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateCompanySectionHeader class]) bundle:nil];
    [self.tableView registerNib:headerTwo forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_CompanySectionHeaderConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSingleAdTextView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_SingleADTextHeaderConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionNormalFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_DefaultSectionFooterConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionImagesFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_ImagesListSectionFooterConstStr)];
    
}


#pragma mark actions （点击事件）

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}

#pragma mark IBActions （点击事件xib）

- (IBAction)clickOnCityChangeBtn:(UIButton *)sender {
    
    if (_locationModelsArr == nil || self.locationModelsArr.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未获取到地理信息列表"];
        [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
            if (modelsArr.firstObject.code == 0) {
                [self.locationModelsArr addObjectsFromArray:modelsArr];
            }
        } Failure:nil];
    }
    // 进行切换城市标签的操作
    if (self.citySelectedView.hidden == YES) {
        self.citySelectedView.hidden = NO;
    }else{
        self.citySelectedView.hidden = YES;
    }
}

- (IBAction)clickOnNavgationBtnAction:(UIButton *)sender {
    
    // 先判断信息补全
    if ([self jumpToUserMessageSetUpPager]) {
        // 判断若需要跳转到补全页面则结束当前所有操作
        return ;
    }
    
    switch (sender.tag) {
            // 点击首页的发布按钮
        case ICN_NavPublishBtnType:
        {
            if (self.isUserLogin == NO) {
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }else{
                
                // 在用户登录的状态判断是不是企业通过审核状态
                if (![self getCurrentUserBusinessAuditStatus]) {
                    // 未通过审核 -- 判断是不是企业用户
                    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
                        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
                            // 是企业用户 == 弹出提示窗
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未通过审核"];
                            return ;
                        }
                    }
                }
                self.warnView.frame = [UIScreen mainScreen].bounds;
                [self.view addSubview:self.warnView];
            }
            break;
        }
            // 点击首页搜索按钮
        case ICN_NavSearchBtnType:{
            if (self.isUserLogin) {
                ICN_DynSearchPagerVC *pager = [[ICN_DynSearchPagerVC alloc] init];
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
            break;
        }
            
        default:
            break;
    }
    
}


- (IBAction)clickOnSelectContentAction:(UIButton *)sender {
    
    [self changeSelectedContentLabelStatusWithSenderTage:sender.tag];
    
}

- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    // 根据tag计算选中的是哪个按钮 - 并将其他按钮的选中状态改变
    if (tag != self.selectedStateBtn.tag) {
        // 两次选中的按钮的tag不一样
        
        // 1. 设置之前选中的按钮取消选择
        self.selectedStateBtn.selected = NO;
        // 2. 判断选中的按钮并设置selectedStateBtn 为选中的按钮
        // 3. 设置选中的按钮的状态为yes
        switch (tag) {
            case PRIVATE_DynamicBtn:{
                self.dynamicSelectedBtn.selected = YES;
                self.selectedStateBtn = self.dynamicSelectedBtn;
                break;
            }
            case PRIVATE_WisdomBtn:{
                self.wisdomSelectedBtn.selected = YES;
                self.selectedStateBtn = self.wisdomSelectedBtn;
                break;
            }
            case PRIVATE_TopicBtn:{
                self.topicSelectedBtn.selected = YES;
                self.selectedStateBtn = self.topicSelectedBtn;
                break;
            }
            case PRIVATE_AskActionBtn:{
                self.askActionSelectedBtn.selected = YES;
                self.selectedStateBtn = self.askActionSelectedBtn;
                break;
            }
            default:
                break;
        }
        
        // 4. 设置切换选中状态的标签的跟随移动
        [UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.buttonSignView.center;
            center.x = self.selectedStateBtn.centerX;
            self.buttonSignView.center = center;
        } completion:^(BOOL finished) {
            [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.breakLineView);
                make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
                make.centerX.equalTo(self.selectedStateBtn);
            }];
        }];
    }
    // 5. 设置ViewModel中选中的刷新类型
    // 注现在需要将ViewModel中关于是动态还是智讯的相关内容修改为枚举四种选项均可的
    self.viewModel.currentSelectedType = tag;
    
    // 6. 根据选中的内容刷新页面
    [self.tableView.mj_header beginRefreshing];
    
    //test=== 在吐槽和提问是假数据的时候手动刷新数据
    [self.tableView reloadData];
    
}


#pragma mark - ---------- Protocol Methods（代理方法） ----------

#pragma mark --- DynLocationPickerViewDelegate ---

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (success) {
        // 将选中的城市提交给服务器
        
        // 修改当前显示的城市
        for (ICN_LocationModel *localModel in self.locationModelsArr) {
            for (ICN_CityDetialModel *detialModel in localModel.list) {
                if (detialModel.cityId.integerValue == code) {
                    self.p_CurrentCityLabel.text = detialModel.className;
                }
            }
        }
        
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未选中城市"];
    }
    self.citySelectedView.hidden = YES;
}

#pragma mark --- 提问相关代理 ---

- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Question_ResponseOtherList:{
            // 刷新列表页操作
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
                // 刷新失败之后隐藏加载视图
                self.tableView.mj_footer.hidden = YES;
            }else{
                [self.tableView reloadData];
            }
            break ;
            break;
        }
        case Question_ResponseLikeUp:{
            // 提问列表点赞操作
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break;
        }
        case Question_ResponseDisLikeUp:{
            // 提问列表取消点赞操作
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break;
        }
        default:
            break;
    }
}

#pragma mark --- 吐槽相关代理 ---

- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseDisLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseOthersList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}



#pragma mark --- DynamicStateFirstPagerViewModelDelegate ---

- (void)responseWithNetRequestError:(NSString *)error{
    [self endRefreshWithTableView:self.tableView];
    [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:error];
}

- (void)responseWithModelsArr:(NSMutableArray *)modesArr{
    [self endRefreshWithTableView:self.tableView];
    if (_modelsArr) {
        [_modelsArr removeAllObjects];
    }
    [self.modelsArr addObjectsFromArray:modesArr];
    [self.tableView reloadData];
}

- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
    }
}

- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经点过赞了"];
    [self.tableView reloadData];
}

- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您没有权限进行此操作"];
}

#pragma mark --- ICN_DynWarnViewDelegate ---

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

/** 配置分享给联系人的拓展字典 */
- (NSDictionary *)configSharePramsDicWithType:(NSInteger)shareType Pic:(NSString *)contentPic Content:(NSString *)content EXT:(NSString *)contentEXT ShareId:(NSString *)shareId{
    
    NSMutableDictionary *mutableParams = [@{
                                            @"shareType" : SF(@"%d",shareType),
                                            @"content" : SF(@"%@",content),
                                            @"shareId" : SF(@"%@",shareId),
                                            } mutableCopy];
    if (contentPic) {
        [mutableParams setValue:contentPic forKey:@"contentPic"];
    }
    if (contentEXT) {
        [mutableParams setValue:contentEXT forKey:@"contentEXT"];
    }
    return [NSDictionary dictionaryWithDictionary:mutableParams];
}

// 实现分享到i行好友的方法
- (void)shareToMyFriendPager{
    // 分享到 I行好友
    ICN_MyfridentViewController *myfrident=[[ICN_MyfridentViewController alloc]init];
    myfrident.model=self.replayModel;
    // 第一步判断类型
    switch (self.transmitModel.paramsType) {
        case REPLAY_DynamicReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:2 Pic:picUrl Content:self.replayModel.content EXT:nil ShareId:self.replayModel.matterId];
            break ;
        }
        case REPLAY_WisdomReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:3 Pic:picUrl Content:self.replayModel.content EXT:self.replayModel.title ShareId:self.replayModel.DynID];
            break ;
        }
        case REPLAY_ComplainReplay:{
            myfrident.ext = [self configSharePramsDicWithType:5 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        case REPLAY_QuestionReplay:{
            myfrident.ext = [self configSharePramsDicWithType:4 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        default:
            break;
    }
    self.replayModel = nil;
    [self currentPagerJumpToPager:myfrident];
}

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    //    1002331  动态  1002332 智讯
    switch (type) {
            // 点击发布动态
        case Publich_DynamicState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_DynamicBtn];
            ICN_DynamicStatePublicationVC *pager = [[ICN_DynamicStatePublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 点击发布智讯
        case Publich_WisdomState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_WisdomBtn];
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布吐槽
        case Publich_ComplainState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_TopicBtn];
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布提问
        case Publich_AskQuestion:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_AskActionBtn];
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态 == 需要判断是何种形式的转发到动态
            // 判断是自己的动态不转发
            if (self.replayModel != nil && self.replayModel.isMe.integerValue == 1) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无法转发自己的动态"];
                return ;
            }
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到i行好友功能
            [self shareToMyFriendPager];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到微信朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            NSLog(@"分享到QQ空间");
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            break;
        }
        default:
            break;
    }
    // 进行完操作后默认将视图移除出主页面
    [self.warnView removeFromSuperview];
    [self.replayView removeFromSuperview];
}



#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > 0) {
        // 上拉中 显示底部刷新视图
        self.tableView.mj_footer.hidden = NO;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // 先判断信息补全
    if ([self jumpToUserMessageSetUpPager]) {
        // 判断若需要跳转到补全页面则结束当前所有操作
        return ;
    }
    
    if (![self isUserLogin]) {
        self.hidesBottomBarWhenPushed = YES;
        [self currentPagerJumpToPager:self.loginPager];
        return ;
    }
    
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
            pager.complainId = [self.complainViewModel.modelsArr[indexPath.row] complainId];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case PRIVATE_AskActionBtn:{
            
            // 获取到需要的Model
            ICN_QuestionListModel *model = self.questionViewModel.modelsArr[indexPath.row];
            if ([model.isMine integerValue] == 0) {
                // 是我发布的
                ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                pager.constentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }else{
                // 不是我发布的
                ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                pager.contentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }
            break;
        }
        default:{
            // 在默认内容中对于 动态和智讯的处理
            if (self.isUserLogin) {
                //相关逻辑 - 跳转到评论页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = self.modelsArr[indexPath.section];
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            break;
        }
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            return self.complainViewModel.modelsArr.count;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是提问
            return self.questionViewModel.modelsArr.count;
            break;
        }
        default:
            break;
    }
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        return 1;
    }else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            if (self.complainViewModel.modelsArr.count > indexPath.row) {
                return [self requestWithComplainCellWith:tableView Model:self.complainViewModel.modelsArr[indexPath.row]];
            }
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是提问
            if (indexPath.row < self.questionViewModel.modelsArr.count) {
                return [self requestWithQuestionCellWith:tableView Model:[self.questionViewModel.modelsArr objectAtIndex:indexPath.row]];
            }
            break;
        }
        default:{
            // 对于动态以及转发类型的Cell的处理
            if ([self.modelsArr[indexPath.section] isKindOfClass:[ICN_DynStateContentModel class]]) {
                ICN_DynStateContentModel *model = self.modelsArr[indexPath.section];
                // 判断是否是最新类型的Model
                if ([model.fromId integerValue] != 0) {
                    return [self requestWithNewReplyCellWith:tableView Model:model];
                }
                else{
                    // 获取其他动态类型的Cell （正文）
                    return [self requestWithCommonContentCellWith:tableView Model:model];
                }
            }
        }
            break;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽
            return 1;
            break;
        }
        default:
            break;
    }
    return self.modelsArr.count;
}


/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return nil;
            break;
        }
        default:
            break;
    }
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateADModel class]]) {
        // 进行对于广告头视图的处理
        ICN_DynStateADModel *adModel = [self.modelsArr objectAtIndex:section];
        if (adModel.genre) {
            if (adModel.genre.integerValue == 0) {
                // 文字连接
                ICN_DynStateSingleAdTextView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_SingleADTextHeaderConstStr)];
                [view callBackWithTextTapBlock:^(ICN_DynStateADModel *model) {
                    if (self.isUserLogin) {
                        //相关逻辑 - 跳转到评论页面
                        ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                        pager.companyId = model.memberId;
                        [self currentPagerJumpToPager:pager];
                    }else{
                        self.hidesBottomBarWhenPushed = YES;
                        [self currentPagerJumpToPager:self.loginPager];
                    }
                }];
                view.model = adModel;
                return view;
            }else if (adModel.genre.integerValue == 1){
                ICN_DynStateCompanySectionHeader *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_CompanySectionHeaderConstStr)];
                [view callBackWithHeaderTapBlock:^(ICN_DynStateADModel *model) {
                    if (self.isUserLogin) {
                        //相关逻辑 - 跳转到对应的公司详情
                        ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                        pager.companyId = model.memberId;
                        [self currentPagerJumpToPager:pager];
                    }else{
                        self.hidesBottomBarWhenPushed = YES;
                        [self currentPagerJumpToPager:self.loginPager];
                    }
                }];
                view.model = adModel;
                return view;
            }
        }
    }
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        
        // 新转发类型也没有头尾视图
        if ([[self.modelsArr[section] fromId] integerValue] != 0) {
            return nil;
        }
        
        // 正文内容
        ICN_DynStateSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_DefaultSectionHeaderConstStr)];
        if (section == 0) {
            view.firstSection = YES;
        }else{
            view.firstSection = NO;
        }
        view.model = self.modelsArr[section];
        [view callBackWithTapBlock:^(ICN_DynStateContentModel *model, BOOL isHeaderIcon) {
            if (self.isUserLogin) {
                [self jumpToPersonalDetialPagerWithModel:model];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
        }];
        return view;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return nil;
            break;
        }
        default:
            break;
    }
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        
        // 新转发类型也没有头尾视图
        if ([[self.modelsArr[section] fromId] integerValue] != 0) {
            return nil;
        }
        
        // 当内容为动态的时候有尾视图 --- warn
        ICN_DynStateContentModel *model = self.modelsArr[section];
        if (model.pic && [model.pic containsString:@"."] && model.isTransmit.integerValue == 0) {
            // 有图片的时候调用有图片的pic;
            ICN_DynStateSectionImagesFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_ImagesListSectionFooterConstStr)];
            footer.model = model;
            [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                // 先判断信息补全
                if ([self jumpToUserMessageSetUpPager]) {
                    // 判断若需要跳转到补全页面则结束当前所有操作
                    return ;
                }
                switch (btnTag) {
                    case ICN_CellLikeActionBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        model.likeUp = !model.likeUp;
                        NSInteger type;
                        if (model.isPraise.integerValue == 0) {
                            type = 1;
                        }else{
                            type = 2;
                        }
                        [self.viewModel likeUpWithType:type Model:model];
                        break;
                    }
                    case ICN_CellReviewBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        // 1. 获取转发需要的两种Model
                        self.replayModel = model;
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                        // 2. 对于智讯的title进行处理
                        if (model.title == nil) {
                            self.transmitModel.title = @"i行动态";
                        }
                        // 3. 展开转发页面
                        [self.view addSubview:self.replayView];
                        break;
                    }
                    case ICN_CellCommentBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        //相关逻辑 - 跳转到评论页面
                        ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                        pager.model = model;
                        [self currentPagerJumpToPager:pager];
                        break;
                    }
                    case ICN_CellReportBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        // 执行举报按钮相关操作
                        // 举报跳转方法
                        [self jumpToReportDetialPagerWithModel:model];
                        break;
                    }
                    default:
                        // 返回的是image的索引值
                        if (self.pictureView.hidden == YES) {
                            NSArray *pathArr = [model.pic componentsSeparatedByString:@","];
                            self.pictureView.urlPathArr = pathArr;
                            self.pictureView.currentIndex = btnTag;
                            self.pictureView.hidden = NO;
                        }
                        break;
                }
            }];
            return footer;
        }else{
            ICN_DynStateSectionNormalFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_DefaultSectionFooterConstStr)];
            footer.model = model;
            [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                // 先判断信息补全
                if ([self jumpToUserMessageSetUpPager]) {
                    // 判断若需要跳转到补全页面则结束当前所有操作
                    return ;
                }
                if (self.isUserLogin == NO) {
                    self.hidesBottomBarWhenPushed = YES;
                    [self currentPagerJumpToPager:self.loginPager];
                }else{
                    // 根据tag判断点击的按钮
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isPraise.integerValue == 0) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 执行转发相关操作
                            // 1. 获取转发需要的两种Model
                            self.replayModel = model;
                            self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                            // 2. 对于智讯的title进行处理
                            if (model.title == nil) {
                                self.transmitModel.title = @"i行动态";
                            }
                            // 3. 展开转发页面
                            [self.view addSubview:self.replayView];
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            //相关逻辑 - 跳转到评论页面
                            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                            pager.model = model;
                            [self currentPagerJumpToPager:pager];
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            // 举报操作
                            [self jumpToReportDetialPagerWithModel:model];
                            break;
                        }
                        default:
                            break;
                    }
                    
                }
            }];
            return footer;
        }
    }
    
    return nil;
}

/** 逐条调节tableView的Cell高度 */

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return 0.0;
            break;
        }
        default:
            break;
    }
    
    CGFloat height = 0.0;
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        // 新转发类型也没有头尾视图
        if ([[self.modelsArr[section] fromId] integerValue] != 0) {
            return 0.0;
        }
        
        if (section == 0) {
            return ICN_DynStateSectionHeaderViewHeight - 5;
        }
        return ICN_DynStateSectionHeaderViewHeight;
    }else{
        ICN_DynStateADModel *model = self.modelsArr[section];
        if (model.genre.integerValue == 0) {
            return ICN_DynStateSingleAdTextViewHeight;
        }else{
            return ICN_DynStateCompanySectionHeaderHeight;
        }
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return 0.0;
            break;
        }
        default:
            break;
    }
    
    CGFloat height = 0.0;
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        
        // 新转发类型也没有头尾视图
        if ([[self.modelsArr[section] fromId] integerValue] != 0) {
            return 0.0;
        }
        
        ICN_DynStateContentModel *model = self.modelsArr[section];
        if (model.imageFooterHeight != 0.0 && model.isTransmit.integerValue == 0) {
            return ImageFooterHeight + model.imageFooterHeight;
        }else
            return ICN_DynStateSectionNormalFooterHeight;
    }
    return height;
}



@end

