//
//  UILabel+ALB_SizeFit.h
//  ICan
//
//  Created by albert on 2016/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (ALB_SizeFit)

+ (CGFloat)getTextHeight:(NSString *)text
                   width:(CGFloat)width
                fontSize:(CGFloat)fontSize;

@end
