//
//  ICN_DynamicStateFirstPagerModel.m
//  ICan
//
//  Created by albert on 2016/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStateFirstPagerViewModel.h"
#import "HRNetworkingManager+DynFirstPager.h" // 网络请求头文件
#import "ICN_DynStateContentModel.h"
#import "ICN_DynStateADModel.h"
#import "UILabel+ALB_SizeFit.h"

@interface ICN_DynamicStateFirstPagerViewModel ()

@property (nonatomic , strong)NSMutableArray *stateContentArr; // 动态内容数组
@property (nonatomic , strong)NSMutableArray *wisdomContentArr; // 智讯内容数组
@property (nonatomic , strong)NSMutableArray *advertisementArr; // 广告内容数组

@property (nonatomic , strong)ICN_DynStateContentModel *likeUpModel; // 点赞Model

@end

@implementation ICN_DynamicStateFirstPagerViewModel

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)stateContentArr{
    if (_stateContentArr == nil) {
        _stateContentArr = [NSMutableArray array];
    }
    return _stateContentArr;
}

- (NSMutableArray *)wisdomContentArr{
    if (_wisdomContentArr == nil) {
        _wisdomContentArr = [NSMutableArray array];
    }
    return _wisdomContentArr;
}

- (NSMutableArray *)advertisementArr{
    if (_advertisementArr == nil) {
        _advertisementArr = [NSMutableArray array];
    }
    return _advertisementArr;
}

#pragma mark - ---------- 公开方法 ----------

/** 初始化方法 */
- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
        _currentSelectedType = PRIVATE_DynamicBtn;
    }
    return self;
}

// 获取首页列表的网络请求的内容
- (void)requestWithDynamicFirstPagerContentList{
    // 根据当前的选中类型来判断数据的加载
    // 判断如果当前页面是动态或者是智讯则添加广告的功能 == 在获取到广告的时候不刷新数据
    if (self.currentSelectedType == PRIVATE_DynamicBtn || self.currentSelectedType == PRIVATE_WisdomBtn) {
        [HRNetworkingManager requestDynADContentWithPage:self.currentPage PageSize:HR_ComADPageSize Success:^(NSArray<ICN_DynStateADModel *> *array) {
            if (self.currentPage == 1) {
                if (_advertisementArr) {
                    [_advertisementArr removeAllObjects];
                }
            }
            [self.advertisementArr addObjectsFromArray:array];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithNetRequestError:@"网络请求失败"];
        }];
    }
    // 注：吐槽和问答没有广告
    switch (self.currentSelectedType) {
        case PRIVATE_DynamicBtn:{
            [HRNetworkingManager requestDynamicStateWithPage:self.currentPage PageSize:HR_ComContentPageSize Success:^(NSArray<ICN_DynStateContentModel *> *array) {
                // 判断当现在刷新页面的时候清空数组中的原有数据
                NSMutableArray *resultArr = [NSMutableArray array];
                if (_stateContentArr != nil && self.currentPage == 1) {
                    [_stateContentArr removeAllObjects];
                }else{
                    [resultArr addObjectsFromArray:_stateContentArr];
                }
                [resultArr addObjectsFromArray:array];
                [self callBackWithWholeNeededContentsArr:resultArr];
            } Failure:^(NSDictionary *errorInfo) {
                [self callBackWithNetRequestError:@"网络请求失败"];
            }];
            break;
        }
        case PRIVATE_WisdomBtn:{
            
            [HRNetworkingManager requestWidsomMessageWithPage:self.currentPage PageSize:HR_ComContentPageSize Success:^(NSArray<ICN_DynStateContentModel *> *array) {
                 // 判断当现在刷新页面的时候清空数组中的原有数据
                NSMutableArray *resultArr = [NSMutableArray array];
                if (_wisdomContentArr != nil && self.currentPage == 1) {
                    [_wisdomContentArr removeAllObjects];
                }else{
                    [resultArr addObjectsFromArray:self.wisdomContentArr];
                }
                [resultArr addObjectsFromArray:array];
                [self callBackWithWholeNeededContentsArr:resultArr];
            } Failure:^(NSDictionary *errorInfo) {
                [self callBackWithNetRequestError:@"网络请求失败"];
            }];
            
            break;
        }
        case PRIVATE_TopicBtn:{
            [self callBackWithNetRequestError:@"未添加吐槽接口"];
            break;
        }
        case PRIVATE_AskActionBtn:{
            [self callBackWithNetRequestError:@"未添加问答接口"];
            break;
        }
        default:
            break;
    }
    
}

- (void)refreshCurrentPageContentCells{
    self.currentPage = 1;
    [self requestWithDynamicFirstPagerContentList];
}


- (void)loadNextPageContentCells{
    self.currentPage ++;
    [self requestWithDynamicFirstPagerContentList];
}

- (void)likeUpWithType:(NSInteger)type Model:(ICN_DynStateContentModel *)model{
    NSString *typeStr = SF(@"%d",type);
    if (model) {
        self.likeUpModel = model;
    }
    [HRNetworkingManager updateDynamicLikeUpStateWithMatterId:model.matterId Type:typeStr Success:^(BaseOptionalModel *model) {
        if (model.code == 0) {
            if (self.likeUpModel.isPraise.integerValue == 0) {
                self.likeUpModel.praiseCount = SF(@"%d",self.likeUpModel.praiseCount.integerValue + 1);
                self.likeUpModel.isPraise = @"1";
            }else{
                self.likeUpModel.praiseCount = SF(@"%d",self.likeUpModel.praiseCount.integerValue - 1);
                self.likeUpModel.isPraise = @"0";
            }
            [self callBackWithLikeUpRequestSuccess:YES];
        }
        if (model.code == 2) {
            // 用户无权限点赞
            [self callBackWhileAuthorHasNoAuthority];
        }
        if (model.code == 3) {
            // 用户已经点赞现在无法点赞只能取消
            self.likeUpModel.isPraise = @"1";
            [self callBackWithAuthorAlreadyLikeUp];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithLikeUpRequestSuccess:NO];
    }];
    
}

#pragma mark - ---------- 私有方法 ----------

- (void)callBackWithNetRequestError:(NSString *)error{
    if(self.delegate){
        if ([self.delegate respondsToSelector:@selector(responseWithNetRequestError:)]) {
            [self.delegate responseWithNetRequestError:error];
        }
    }
}

- (void)callBackWithAuthorAlreadyLikeUp{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithAlreadyLikeUp)]) {
            [self.delegate responseWithAlreadyLikeUp];
        }
    }
}

- (void)callBackWhileAuthorHasNoAuthority{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWhileUserHasNoAuthority)]) {
            [self.delegate responseWhileUserHasNoAuthority];
        }
    }
}

- (void)callBackWithLikeUpRequestSuccess:(BOOL)success{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithLikeUpRequestSuccess:)]) {
            [self.delegate responseWithLikeUpRequestSuccess:success];
        }
    }
}

- (CGFloat)countImageFooterHeightWithPicCount:(NSInteger)count{
    NSInteger groups = count / 3;
    if (count < 3) {
        groups = 0;
    }
    if (count % 3 > 0) {
        groups ++;
    }
    // 获取5s尺寸下的imageview占用高度
    CGFloat countHeight = groups * (97.0 + 5.0) - 5.0;
    return ICN_KScreen(countHeight);
}

// 在获取到全部数据之后的回调方法 -- 内容可以是智讯、动态、提问和吐槽
- (void)callBackWithWholeNeededContentsArr:(NSArray *)contents{
    
    NSMutableArray *resultArr = [NSMutableArray array];
    NSArray *contentArr;
    // 根据选种类型对获取到的数据进行对应的处理
    switch (self.currentSelectedType) {
        case PRIVATE_DynamicBtn:{
            contentArr = [NSArray arrayWithArray:contents];
            // 对于原始数组加工并填插广告数组
            [self.stateContentArr removeAllObjects];
            [self.stateContentArr addObjectsFromArray:[self configContentListWithAdvertisement:contentArr]];
            resultArr = self.stateContentArr;
            break;
        }
        case PRIVATE_WisdomBtn:{
            contentArr = [NSArray arrayWithArray:contents];
            // 对于原始数组加工并填插广告数组
            [self.wisdomContentArr removeAllObjects];
            [self.wisdomContentArr addObjectsFromArray:[self configContentListWithAdvertisement:contentArr]];
            resultArr = self.wisdomContentArr;
            break;
        }
        case PRIVATE_TopicBtn:{
            break;
        }
        case PRIVATE_AskActionBtn:{
            break;
        }
        default:
            break;
    }
    
    if (self.delegate && resultArr.count >= 0) {
        if ([self.delegate respondsToSelector:@selector(responseWithModelsArr:)]) {
            [self.delegate responseWithModelsArr:resultArr];
        }
    }
    
}

// 私有方法 -- 根据Model计算智讯和动态的必要属性并将广告页面添加
- (NSMutableArray *)configContentListWithAdvertisement:(NSArray *)contentArr{
    NSMutableArray *resultArr = [NSMutableArray array];
    for (NSInteger i = 0; i < contentArr.count; i++) {
        
        if (![contentArr[i] isKindOfClass:[ICN_DynStateADModel class]]) {
            // 在把动态添加到想要显示的数组的时候将背景图的尺寸计算好
            ICN_DynStateContentModel *model = contentArr[i];
            NSArray *array = [model.pic componentsSeparatedByString:@","];
            if ([model.pic containsString:@"."]) {
                model.imageFooterHeight = [self countImageFooterHeightWithPicCount:array.count];
                
            }else
                model.imageFooterHeight = 0.0;
            model.contentSpreadHeight = [UILabel getTextHeight:model.content width:[UIScreen mainScreen].bounds.size.width - 20.0 fontSize:13.0];
            [resultArr addObject:model];
        }else{
            [resultArr addObject:[contentArr objectAtIndex:i]];
        }
        
        
        
        if (i >= 2 && i % 3 == 2 && self.advertisementArr.count > i / 3) {
            // 判断下一张是不是广告 -- 如果不是则插入广告
            if (i < contentArr.count - 1) {
                if (![contentArr[i + 1] isKindOfClass:[ICN_DynStateADModel class]]) {
                    [resultArr addObject:self.advertisementArr[i / 3]];
                }
            }
        }
    }
    return resultArr;
}

@end
