//
//  UILabel+ALB_SizeFit.m
//  ICan
//
//  Created by albert on 2016/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "UILabel+ALB_SizeFit.h"

@implementation UILabel (ALB_SizeFit)

+ (CGFloat)getTextHeight:(NSString *)text
                   width:(CGFloat)width
                fontSize:(CGFloat)fontSize{
    
    CGRect rectToFit = [text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName :[UIFont systemFontOfSize:fontSize]} context:nil];
    return rectToFit.size.height;
}


@end
