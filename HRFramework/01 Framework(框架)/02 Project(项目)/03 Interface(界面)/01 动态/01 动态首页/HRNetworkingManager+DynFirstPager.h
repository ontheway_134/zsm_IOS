//
//  HRNetworkingManager+DynFirstPager.h
//  ICan
//
//  Created by albert on 2016/12/17.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager.h"

#pragma mark - ---------- 需求相关Model ----------
#import "ICN_DynStateADModel.h" // 广告Model
#import "ICN_DynStateContentModel.h" // 正文Model
#import "ICN_DynamicStateCommentModel.h" // 评论内容Model
#import "ICN_DynCompantDetialModel.h" // 公司详情Model
#import "ICN_LocationModel.h" // 地理信息Model

#pragma mark - ---------- 需求相关block ----------
typedef void(^ADContentArrBlock)(NSArray<ICN_DynStateADModel *> *array);
typedef void(^StateArrBlock)(NSArray <ICN_DynStateContentModel *> *array);
typedef void(^CommentBlock)(NSArray <ICN_DynamicStateCommentModel *> *array);
typedef void(^LocationsBlock)(NSArray<ICN_LocationModel *> *modelsArr); // 返回地理位置信息数组的Model
typedef void(^BaseModelBlock)(BaseOptionalModel *model); // 通用不考虑返回值得block
typedef void(^CompantDetialModelBlock)(ICN_DynCompantDetialModel *model); // 通用不考虑返回值得block

@interface HRNetworkingManager (DynFirstPager)

/** 直接根据网址获取地理信息的接口 */
+ (void)requestDynamicLocationSuccess:(LocationsBlock)success
                              Failure:(ErrorBlock)failure;

/** 根据token - 自动从userdefault中获取,page(页数),pagesize(每页显示数量) 获取动态内容数组 */
+ (void)requestDynamicStateWithPage:(NSInteger)page
                           PageSize:(NSInteger)size
                            Success:(StateArrBlock)success
                            Failure:(ErrorBlock)failure;

/** page(页数),pagesize(每页显示数量) 获取智讯内容数组 */
+ (void)requestWidsomMessageWithPage:(NSInteger)page
                            PageSize:(NSInteger)size
                             Success:(StateArrBlock)success
                             Failure:(ErrorBlock)failure;


/** 根据page(页数),pagesize(每页显示数量) 获取动态首页广告内容数组 */
+ (void)requestDynADContentWithPage:(NSInteger)page
                           PageSize:(NSInteger)size
                            Success:(ADContentArrBlock)success
                            Failure:(ErrorBlock)failure;

/** 根据动态Id ,page(页数),pagesize(每页显示数量) 获取动态详情页面评论数组 */
+ (void)requestDynamicStateCommentsWithPage:(NSInteger)page
                                   PageSize:(NSInteger)size
                                   MatterId:(NSString *)matterId
                                    Success:(CommentBlock)success
                                    Failure:(ErrorBlock)failure;

#pragma mark - ---------- 企业用户相关接口 ----------

/** 根据公司ID获取公司详情数据接口 */
+ (void)requestDynCompanyDetialContentWithCompanyId:(NSString *)companyId
                                            Success:(CompantDetialModelBlock)success
                                            Failure:(ErrorBlock)failure;
/** 根据公司ID关注公司的数据接口 */
+ (void)requestAddCompanyAttentionWithCompanyId:(NSString *)companyId
                                        Success:(BaseModelBlock)success
                                        Failure:(ErrorBlock)failure;
/** 根据公司ID取消公司关注的数据接口 */
+ (void)requestDeleteCompanyAttentionWithCompanyId:(NSString *)companyId
                                        Success:(BaseModelBlock)success
                                        Failure:(ErrorBlock)failure;


#pragma mark - ---------- 动态与智讯相关功能接口 ----------

/** 根据动态Id,需要@的人 评论内容 ,page(页数),pagesize(每页显示数量) 发布评论 */
+ (void)publishDynamicStateCommentWithMatterId:(NSString *)matterId
                                       Replyer:(NSString *)replyer
                                       Content:(NSString *)content
                                       Success:(SuccessBlock)success
                                       Failure:(ErrorBlock)failure
                                        Wisdom:(BOOL)isWisdom;

/** 点赞，取消点赞
 token  用户密令				  是
 matterId 动态ID				  是
 type    类型1：点赞，2：取消点赞	  是
 */

+ (void)updateDynamicLikeUpStateWithMatterId:(NSString *)matterId
                                        Type:(NSString  *)type
                                     Success:(BaseModelBlock)success
                                     Failure:(ErrorBlock)failure;

/** 根据token ， 动态ID 动态评论 发布转发动态 */
+ (void)publichReplayDyanmicStateWithMatterId:(NSString *)matterId
                                      Summary:(NSString *)summary
                                      Success:(BaseModelBlock)success
                                      Failure:(ErrorBlock)failure;








@end
