//
//  ICN_ReplyingDynStateCell.h
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateContentModel;
@interface ICN_ReplyingDynStateCell : UITableViewCell

@property (nonatomic , strong)ICN_DynStateContentModel *model;


@end
