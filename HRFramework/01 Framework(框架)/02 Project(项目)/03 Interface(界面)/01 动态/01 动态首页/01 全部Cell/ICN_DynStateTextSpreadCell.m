//
//  ICN_DynStateTestSpreadCell.m
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateTextSpreadCell.h"
#import "ICN_DynStateContentModel.h"

/** 未展开时候的标准高度是115.5 */

@interface ICN_DynStateTextSpreadCell ()

@property (weak, nonatomic) IBOutlet UILabel *p_ContentSpreadLabel;

@property (weak, nonatomic) IBOutlet UIButton *p_PullButton;

@property (weak, nonatomic) IBOutlet UIView *p_SpreadView; // 分隔栏


@end

@implementation ICN_DynStateTextSpreadCell

- (void)callBackWithSpreadBlock:(SpreadContentBlock)block{
    self.block = block;
}

- (void)setContentDetial:(BOOL)contentDetial{
    _contentDetial = contentDetial;
    if (_contentDetial) {
        self.p_ContentSpreadLabel.numberOfLines = 20;
        self.p_PullButton.hidden = YES;
    }
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    if (self.model.contentSpreadHeight < 60.0) {
        self.p_PullButton.hidden = YES;
        self.p_SpreadView.hidden = YES;
    }else{
        self.p_PullButton.hidden = NO;
        self.p_SpreadView.hidden = NO;
    }
    if (_model.isSpread) {
        self.p_ContentSpreadLabel.numberOfLines = 20;
    }else
        self.p_ContentSpreadLabel.numberOfLines = 4;
    // 执行数据添加的工作 如果是智讯需要添加部分内容
    if (_model.isWidsom) {
        // 显示的内容是智讯
        NSString *nickContentStr = SF(@"%@\n%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode],[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:nickContentStr];
        [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                                 value:[UIColor blackColor]
                                 range:[nickContentStr rangeOfString:SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode])]];
        [attributedString addAttribute:NSFontAttributeName// 字体
                                 value:[UIFont boldSystemFontOfSize:14.0]
                                 range:[nickContentStr rangeOfString:SF(@"%@",[_model.title stringByReplacingEmojiCheatCodesWithUnicode])]];
        self.p_ContentSpreadLabel.attributedText = attributedString;
    }else{
        self.p_ContentSpreadLabel.text = SF(@"%@",[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickUpInsideBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.p_PullButton]) {
        //执行下拉按钮的对应操作
        if (self.block) {
            if (self.model) {
                self.model.contentSpread = !self.model.contentSpread;
                if (self.model.contentSpread) {
                    self.p_ContentSpreadLabel.numberOfLines = 20;
                }else
                    self.p_ContentSpreadLabel.numberOfLines = 4;
                self.block(self.model.contentSpread);
            }
        }
    }
    
    
}



@end
