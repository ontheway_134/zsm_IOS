//
//  ICN_NewReplyToStateCell.h
//  ICan
//
//  Created by albert on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateContentModel;

typedef void(^NewDynReplyCellBlock)(ICN_DynStateContentModel *model , NSInteger btnTag , BOOL isLikeUp);
typedef void(^NewIconTapBlock)(ICN_DynStateContentModel *model);

@interface ICN_NewReplyToStateCell : UITableViewCell

@property (nonatomic , strong)ICN_DynStateContentModel *model;

@property (nonatomic , copy)NewDynReplyCellBlock block;
@property (nonatomic , strong)NewIconTapBlock imageTapBlock;


- (void)callBackWithNewDynReplyCellBlock:(NewDynReplyCellBlock)block;

- (void)callBackNewIconTapBlock:(NewIconTapBlock)block;
@property (weak, nonatomic) IBOutlet UIButton *reportBtn; // 举报按钮
@end
