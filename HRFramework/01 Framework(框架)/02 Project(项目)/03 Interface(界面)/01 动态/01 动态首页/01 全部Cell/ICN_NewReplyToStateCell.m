//
//  ICN_NewReplyToStateCell.m
//  ICan
//
//  Created by albert on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_NewReplyToStateCell.h"
#import "ICN_DynStateContentModel.h"

@interface ICN_NewReplyToStateCell ()

@property (nonatomic , strong)UIImageView *vplusIcon; // 加v的时候使用的图片

@property (weak, nonatomic) IBOutlet UILabel *contentLebl;

@property (weak, nonatomic) IBOutlet UILabel *userNickLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *professionLabel; // 行业标签
@property (weak, nonatomic) IBOutlet UIImageView *companyIcon; // 公司图标
@property (weak, nonatomic) IBOutlet UIImageView *positionIcon; // 职位图标

@property (weak, nonatomic) IBOutlet UILabel *companyLabel; // 公司标签  在没有公司时 学校标签
@property (weak, nonatomic) IBOutlet UILabel *positionLabel; // 职位标签  在没有职位的时候是专业
@property (weak, nonatomic) IBOutlet UILabel *commentNumberLabel; // 评论人数
@property (weak, nonatomic) IBOutlet UILabel *likeupCountLabel; // 点赞人数
@property (weak, nonatomic) IBOutlet UIImageView *likeUpIcon; // 点赞图标

@property (weak, nonatomic) IBOutlet UIView *grayBackView; // 底部灰色分隔栏

#pragma mark --- 可变图片源的约束属性 ---
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentPictureWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentPictureHeightConstraint;

#pragma mark --- 内容源的IB属性 ---
@property (weak, nonatomic) IBOutlet UIImageView *contentPicture; // 内容源 图片
@property (weak, nonatomic) IBOutlet UILabel *contentTitleLabel; // 内容源 标题

#pragma mark --- 按钮属性 ---

@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评论按钮
@property (weak, nonatomic) IBOutlet UIButton *revplyBtn; // 转发按钮

@end

@implementation ICN_NewReplyToStateCell

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = YES; // 默认隐藏图标
        [self addSubview:_vplusIcon];
    }
    return _vplusIcon;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    if (_model.memberLogo){
        NSRange range = NSMakeRange(0, 4);
        NSString *temp = [model.memberLogo substringWithRange:range];
        NSLog(@"%@",temp);
        if ([temp isEqualToString:@"http"]) {
            [self.userIcon sd_setImageWithURL:[NSURL URLWithString:(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }else{
            [self.userIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }
    }
    if (_model.memberNick)
        self.userNickLabel.text = SF(@"%@",_model.memberNick);
    if (_model.createDate)
        self.timeLabel.text = _model.createDate;
    if (_model.commentCount)
        self.commentNumberLabel.text = SF(@"%@",_model.commentCount);
    if (_model.memberProfession){
        self.professionLabel.text = SF(@"%@" , _model.memberProfession);
    }else{
        self.professionLabel.text = @"";
    }
    // 判定 summary 不存在则置空
    if (!_model.summary)
        _model.summary = @"";
    self.contentLebl.text = SF(@"%@",_model.summary);
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    // 根据用户Model的isPrise字段设置Model的计算属性
    if ([_model.isPraise integerValue] == 0) {
        // 未点赞
        self.model.likeUp = NO;
    }else{
        self.model.likeUp = YES;
    }
    // 处理点赞相关的业务逻辑 点赞未选
    if (_model.isLikeUp) {
        // 已经点赞
        [self.likeUpIcon setImage:[UIImage imageNamed:@"点赞"]];
    }else{
        // 未点赞
        [self.likeUpIcon setImage:[UIImage imageNamed:@"点赞未选"]];
    }
    self.likeupCountLabel.text = SF(@"%@",_model.praiseCount);
    
    // 根据数据信息获取用户的行业标签等数据
    if ([_model.workStatus integerValue] == 0) {
        // 未就业
        self.companyLabel.text = SF(@"%@",_model.memberSchool);
        self.positionLabel.text = SF(@"%@" , _model.memberMajor);
    }else{
        // 已就业
        self.companyLabel.text = SF(@"%@",_model.companyName);
        self.positionLabel.text = SF(@"%@",_model.memberPosition);
    }
    
    // 根据用户角色对于是否显示用户状态做相应的处理
    switch ([_model.roleId integerValue]) {
        case 1:{
            // 普通用户
            self.companyLabel.hidden = NO;
            self.companyIcon.hidden = NO;
            self.positionIcon.hidden = NO;
            self.positionLabel.hidden = NO;
            break ;
        }
        case 2:{
            // 企业用户
            self.companyLabel.hidden = YES;
            self.companyIcon.hidden = YES;
            self.positionIcon.hidden = YES;
            self.positionLabel.hidden = YES;
            break ;
        }
        default:
            break;
    }
    
    // 设置与内容源相关的属性
    if ([_model.fromId integerValue] == -1) {
        //warn === 获取到活动的类型之后需要修改内容源图片的尺寸
    }
    [self.contentPicture sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.RandomPicture)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    // 设置正文内容
    NSString *title;
    NSString *content;
    // 如果Model中类型为吐槽提问分开处理 其他类型用逗号分隔找内容
    if (_model.fromId) {
        switch (_model.fromType.integerValue) {
                // 吐槽提问等直接写的
            case 1:
            case 2:{
                
                if (_model.fromType.integerValue == 1) {
                    title = @"来自吐槽";
                }else{
                    title = @"来自提问";
                }
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                
                break ;
            }
            case 3:{
                title = _model.RandomName ? _model.RandomName : @"来自职位";
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                break ;
            }
            case 4:{
                title = _model.RandomName ? _model.RandomName : @"来自行业资讯";
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                break ;
            }
            case 5:{
                title = _model.RandomName ? _model.RandomName : @"来自活动";
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                break ;
            }
            case 6:{
                title = _model.RandomName ? _model.RandomName : @"来自直播";
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                break ;
            }
            case 7:{
                title = _model.RandomName ? _model.RandomName : @"来自职场规划";
                content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
                break ;
            }
            default:
                break;
        }
    }else{
        title = @"标题";
        content = SF(@"%@\n%@",title,[_model.content stringByReplacingEmojiCheatCodesWithUnicode]);
    }
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:content];
    [attributedString addAttribute:NSFontAttributeName// 字体
                             value:[UIFont boldSystemFontOfSize:14.0]
                             range:[content rangeOfString:SF(@"%@",title)]];
    [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                             value:[UIColor blackColor]
                             range:[content rangeOfString:SF(@"%@",title)]];
    self.contentTitleLabel.attributedText = attributedString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self insertTapGestureWithIcon:self.userIcon];
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.userIcon];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.userIcon);
            make.bottom.equalTo(self.userIcon);
        }];
    }
    self.userIcon.layer.masksToBounds = YES;
    self.userIcon.layer.cornerRadius = self.userIcon.width / 2.0;
    
    // 根据用户角色对于是否显示用户状态做相应的处理
    switch ([_model.roleId integerValue]) {
        case 1:{
            // 普通用户
            [self.professionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.companyLabel.mas_right).offset(5);
                make.centerY.equalTo(self.companyLabel);
            }];
            break ;
        }
        case 2:{
            // 企业用户
            [self.professionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.userNickLabel);
                make.top.equalTo(self.userNickLabel.mas_bottom).offset(5);
            }];
            break ;
        }
        default:
            break;
    }
    self.professionLabel.layer.masksToBounds = YES;
    self.professionLabel.layer.cornerRadius = 2.0;
    
    
    
}

- (void)callBackWithNewDynReplyCellBlock:(NewDynReplyCellBlock)block{
    self.block = block;
}

- (void)callBackNewIconTapBlock:(NewIconTapBlock)block{
    self.imageTapBlock = block;
}

- (void)insertTapGestureWithIcon:(UIImageView *)icon{
    icon.userInteractionEnabled = YES;
    if (icon.gestureRecognizers == nil || icon.gestureRecognizers.count == 0) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnUserIconWithGesture:)];
        [icon addGestureRecognizer:tap];
    }
    
}

- (void)clickOnUserIconWithGesture:(UITapGestureRecognizer *)tap{
    if (self.imageTapBlock) {
        self.imageTapBlock(self.model);
    }
}


// 点击Cell按钮的响应事件
- (IBAction)clickOnCellButtonAction:(UIButton *)sender {
    // 如果block不存在的话不需要进行任何操作
    if (!self.block) {
        return ;
    }
    // 对于点赞需要根据状态来设置Model的默认值
    switch (sender.tag) {
        case ICN_CellLikeActionBtnType:{
            if (self.model.isLikeUp) {
                // 已经点赞 - 取消点赞操作
                self.block(self.model , sender.tag , NO);
            }else{
                // 未点赞 - 点赞操作
                self.block(self.model , sender.tag , YES);
            }
            break;
        }
        default:{
            // 点击除点赞之外的按钮的操作 -- 回传tag以及Model
            self.block(self.model , sender.tag , NO);
            break;
        }
    }
    
}


@end
