//
//  ICN_DynStateTestSpreadCell.h
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SpreadContentBlock)(BOOL spread);

@class ICN_DynStateContentModel;
@interface ICN_DynStateTextSpreadCell : UITableViewCell

@property (nonatomic , strong)ICN_DynStateContentModel *model;
@property (nonatomic , copy)SpreadContentBlock block;
@property (nonatomic , assign , getter=idDetial)BOOL contentDetial; // 设置是否是详情Cell的属性

- (void)callBackWithSpreadBlock:(SpreadContentBlock)block;


@end
