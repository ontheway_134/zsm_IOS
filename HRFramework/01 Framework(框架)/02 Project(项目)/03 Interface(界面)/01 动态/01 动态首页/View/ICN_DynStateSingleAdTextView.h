//
//  ICN_ DynStateSingleAdTextView.h
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateADModel;

typedef void(^TextTapBlock)(ICN_DynStateADModel *model);

@interface ICN_DynStateSingleAdTextView : UITableViewHeaderFooterView

@property (nonatomic , strong)ICN_DynStateADModel *model;
@property (nonatomic , copy)TextTapBlock adTextBlock; // 文本广告block


- (void)callBackWithTextTapBlock:(TextTapBlock)block;


@end
