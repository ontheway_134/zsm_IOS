//
//  ICN_DynStateSectionImagesFooter.m
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateSectionImagesFooter.h"
#import "ICN_DynStateContentModel.h"

@interface ICN_DynStateSectionImagesFooter ()

@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewArr;
@property (nonatomic , strong)NSMutableArray <UITapGestureRecognizer *>* imageTapListArr; // imageview的轻拍手势数组

@property (weak, nonatomic) IBOutlet UILabel *p_CommentNumberLabel; // 评论数量标签

@property (weak, nonatomic) IBOutlet UILabel *p_LikeCountLabel; // 点赞数量标签
@property (weak, nonatomic) IBOutlet UIImageView *p_LikeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *p_RepotrIcon;  // 举报标签图片
@property (weak, nonatomic) IBOutlet UIButton *p_ReportBtn;  // 举报按钮



@property (weak, nonatomic) IBOutlet UIView *p_ImagesBackGroundView; // 放置图片的背景图

@property (weak, nonatomic) IBOutlet UIButton *replayBtn; // 转发按钮

@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评论按钮

@property (weak, nonatomic) IBOutlet UIButton *likeBtn; // 点赞按钮


@end

@implementation ICN_DynStateSectionImagesFooter

- (NSMutableArray *)imageViewArr{
    if (_imageViewArr == nil) {
        _imageViewArr = [NSMutableArray array];
    }
    return _imageViewArr;
}

- (NSMutableArray<UITapGestureRecognizer *> *)imageTapListArr{
    if (_imageTapListArr == nil) {
        _imageTapListArr = [NSMutableArray array];
    }
    return _imageTapListArr;
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    // 根据用户Model的isPrise字段设置Model的计算属性
    if ([_model.isPraise integerValue] == 0) {
        // 未点赞
        self.model.likeUp = NO;
    }else{
        self.model.likeUp = YES;
    }
    
    // 根据举报状态判断是否显示举报按钮
    if (_model.canReport.integerValue == 1) {
        // 是自己的动态/智讯，隐藏按钮
        self.p_RepotrIcon.hidden = YES;
        self.p_ReportBtn.hidden = YES;
    }else{
        // 不是自己的动态/智讯
        self.p_RepotrIcon.hidden = NO;
        self.p_ReportBtn.hidden = NO;
    }
    
    // 执行数据添加的工作
    self.p_LikeCountLabel.text = _model.praiseCount;
    self.p_CommentNumberLabel.text = _model.commentCount;
    if (self.model.isLikeUp) {
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞"];
    }else{
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞未选"];
    }
    
    if (_imageViewArr) {
        for (UIImageView *item in self.imageViewArr) {
            [item removeFromSuperview];
            [item removeGestureRecognizer:self.imageTapListArr[[self.imageViewArr indexOfObject:item]]];
            
        }
        [_imageTapListArr removeAllObjects];
        [_imageViewArr removeAllObjects];
    }
    NSArray *picArr = [_model.pic componentsSeparatedByString:@","];
    for (NSInteger i = 0; i < picArr.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(picArr[i])] placeholderImage:[UIImage imageNamed:@"占位图"]];
        imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnImageViewWithTapGesture:)];
        [imageView addGestureRecognizer:tap];
        [self.imageTapListArr addObject:tap];
        [self.imageViewArr addObject:imageView];
        [self.p_ImagesBackGroundView addSubview:imageView];
    }
    [self setNeedsLayout];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (_imageViewArr) {
        
        if (_imageViewArr.count <= 3) {
            for (NSInteger i = 0; i < self.imageViewArr.count; i++) {
                if (i == 0) {
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.p_ImagesBackGroundView);
                        make.top.equalTo(self.p_ImagesBackGroundView);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                    
                }else{
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.imageViewArr[i - 1].mas_right).offset(ICN_KScreen(5.0));
                        make.top.equalTo(self.p_ImagesBackGroundView);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                }
            }
        }else{
            
            for (NSInteger i = 0; i < self.imageViewArr.count; i++) {
                if (i == 0 || i % 3 == 0) {
                    if (i == 0) {
                        [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.p_ImagesBackGroundView);
                            make.top.equalTo(self.p_ImagesBackGroundView);
                            make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                        }];
                    }else{
                        [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.p_ImagesBackGroundView);
                            make.top.equalTo(self.imageViewArr[i / 3 - 1].mas_bottom).offset(ICN_KScreen(5.0));
                            make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                        }];
                    }
                }else{
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.imageViewArr[i - 1].mas_right).offset(ICN_KScreen(5.0));
                        make.top.equalTo(self.imageViewArr[i - 1]);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                }
            }
            
            
        }
        
        
        
    }
    
}

- (void)callBackWithBtnBlock:(CellBtnBlock)block{
    self.block = block;
}

// 将Model和选中的索引传过去
- (void)tapOnImageViewWithTapGesture:(UITapGestureRecognizer *)recognizer{
    if (self.block) {
        self.block(self.model , [self.imageTapListArr indexOfObject:recognizer]);
    }
}

- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    // 先判断是不是举报按钮，在判断能不能进行操作
    if ([sender isEqual:self.p_ReportBtn]) {
        if (self.model.canReport.integerValue == 2) {
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"已举报"];
            return ;
        }
    }
    
    if (self.block) {
        self.block(self.model , sender.tag);
    }
    
}



@end
