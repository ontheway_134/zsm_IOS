//
//  ICN_DynStateSectionImagesFooter.h
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateContentModel;

typedef void(^CellBtnBlock)(ICN_DynStateContentModel *model , NSInteger btnTag);

@interface ICN_DynStateSectionNormalFooter : UITableViewHeaderFooterView

@property (nonatomic , strong)ICN_DynStateContentModel *model;

@property (nonatomic , copy)CellBtnBlock block;



- (void)callBackWithBtnBlock:(CellBtnBlock)block;

@end
