//
//  ICN_DynStateCompanySectionHeader.m
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

/** 标准高度125.0 */
#import "ICN_DynStateCompanySectionHeader.h"
#import "ICN_DynStateADModel.h"

@interface ICN_DynStateCompanySectionHeader ()

@property (weak, nonatomic) IBOutlet UIImageView *p_CompanyBackGround; // 介绍公司的背景图片

@property (weak, nonatomic) IBOutlet UILabel *p_CompanyNameLabel; // 公司名称

@property (weak, nonatomic) IBOutlet UIImageView *p_CompanyIcon; // 公司标识 - icon

@property (weak, nonatomic) IBOutlet UILabel *p_CompanySubmitLabel; // 公司口号

@property (nonatomic , strong)UITapGestureRecognizer *contentViewTap; // 响应整个广告头视图的跳转



@end

@implementation ICN_DynStateCompanySectionHeader

#pragma mark - ---------- 懒加载 ----------

- (UITapGestureRecognizer *)contentViewTap{
    if (_contentViewTap == nil) {
        _contentViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callBackWhileContentTapped)];
        [self addGestureRecognizer:_contentViewTap];
    }
    return _contentViewTap;
}

- (void)callBackWithHeaderTapBlock:(HeaderTapBlock)block{
    self.block = block;
}

// 在手势被点击的时候回调
- (void)callBackWhileContentTapped{
    if (self.block) {
        self.block(self.model);
    }
}

- (void)dealloc{
    if (_contentViewTap) {
        [self.contentView removeGestureRecognizer:_contentViewTap];
        _contentViewTap = nil;
    }
}

- (void)setModel:(ICN_DynStateADModel *)model{
    _model = model;
    
    // 执行数据添加的工作
    [self contentViewTap];
    [self.p_CompanyIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.pic)] placeholderImage:[UIImage imageNamed:@"公司logo"]];
    if (_model.memberNick) {
        self.p_CompanyNameLabel.text = _model.memberNick;
    }else{
        self.p_CompanyNameLabel.text = @"默认标题";
    }
    self.p_CompanySubmitLabel.text = _model.content;
    [self contentViewTap];
    [self layoutIfNeeded];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.p_CompanyIcon.width / 2.0;
    self.p_CompanyIcon.layer.cornerRadius = width;
    self.p_CompanyIcon.layer.masksToBounds = YES;
}




@end
