//
//  ICN_DynStateSectionImagesFooter.m
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateSectionNormalFooter.h"
#import "ICN_DynStateContentModel.h"

/** 标准高度45.5 */

@interface ICN_DynStateSectionNormalFooter ()

@property (weak, nonatomic) IBOutlet UILabel *p_CommentNumberLabel; // 评论数量标签

@property (weak, nonatomic) IBOutlet UILabel *p_LikeCountLabel; // 点赞数量标签
@property (weak, nonatomic) IBOutlet UIImageView *p_LikeIcon;

@property (weak, nonatomic) IBOutlet UIImageView *p_RepotrIcon;  // 举报标签图片
@property (weak, nonatomic) IBOutlet UIButton *p_ReportBtn;  // 举报按钮





@end

@implementation ICN_DynStateSectionNormalFooter

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    // 执行数据添加的工作
    self.p_CommentNumberLabel.text = _model.commentCount;
    self.p_LikeCountLabel.text = _model.praiseCount;
    
    // 根据举报状态判断是否显示举报按钮
    if (_model.canReport.integerValue == 1) {
        // 是自己的动态/智讯，隐藏按钮
        self.p_RepotrIcon.hidden = YES;
        self.p_ReportBtn.hidden = YES;
    }else{
        // 不是自己的动态/智讯
        self.p_RepotrIcon.hidden = NO;
        self.p_ReportBtn.hidden = NO;
    }
    
    // 根据用户Model的isPrise字段设置Model的计算属性
    if ([_model.isPraise integerValue] == 0) {
        // 未点赞
        self.model.likeUp = NO;
    }else{
        self.model.likeUp = YES;
    }
    if (self.model.isLikeUp) {
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞"];
    }else{
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞未选"];
    }

}

- (void)callBackWithBtnBlock:(CellBtnBlock)block{
    self.block = block;
}

- (IBAction)cellBtnClickAction:(UIButton *)sender {
    
    // 先判断是不是举报按钮，在判断能不能进行操作
    if ([sender isEqual:self.p_ReportBtn]) {
        if (self.model.canReport.integerValue == 2) {
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"已举报"];
            return ;
        }
    }
    
    if (self.block) {
        self.block(self.model , sender.tag);
    }
}


@end
