//
//  ICN_DynStateSectionHeaderView.m
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateSectionHeaderView.h"
#import "ICN_DynStateContentModel.h"

/** 头部的标准高度是71.0 */

@interface ICN_DynStateSectionHeaderView ()


@property (nonatomic , strong)UILabel *workStatusLabel; // 用于动态添加的工作状态label
@property (nonatomic , strong)UITapGestureRecognizer *userIconTap; // 用户头像轻拍手势
@property (nonatomic , strong)UITapGestureRecognizer *contentViewTap; // 页面contentView轻拍手势

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UIImageView *p_UserIconImageView; // 用户头像图片

@property (weak, nonatomic) IBOutlet UILabel *p_UserNameLabel; // 用户名

@property (weak, nonatomic) IBOutlet UILabel *p_MajorTitleLabel; // 用户专业名称 - 用户职位

@property (weak, nonatomic) IBOutlet UIImageView *p_MajorIconImageView; // 用户专业图标 - 用户职位

@property (weak, nonatomic) IBOutlet UIImageView *p_UserUnivercityIconImageView; // 用户大学图标 - 用户公司

@property (weak, nonatomic) IBOutlet UILabel *p_UserUnivercityNameLabel; // 用户大学名称 - 用户公司

@property (weak, nonatomic) IBOutlet UILabel *p_CurrentTimeLabel; // 当前时间标签

@property (nonatomic , strong)UIImageView *vplusIcon; // 加v的时候使用的图片

// 用于添加顶部分隔条的约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *TopGrayBreakLineConstraint;


@end

@implementation ICN_DynStateSectionHeaderView

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = YES; // 默认隐藏图标
        [self.p_UserIconImageView addSubview:_vplusIcon];
    }
    return _vplusIcon;
}

- (void)setFirstSection:(BOOL)firstSection{
    _firstSection = firstSection;
    if (firstSection) {
        self.TopGrayBreakLineConstraint.constant = 0.0;
    }else{
        self.TopGrayBreakLineConstraint.constant = 5.0;
    }
}

- (UILabel *)workStatusLabel{
    if (_workStatusLabel == nil) {
        _workStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _workStatusLabel.userInteractionEnabled = YES;
        _workStatusLabel.font = [UIFont systemFontOfSize:11.0];
        _workStatusLabel.adjustsFontSizeToFitWidth = YES;
        _workStatusLabel.minimumScaleFactor = 0.5;
        _workStatusLabel.backgroundColor = RGB0X(0xf87591);
        _workStatusLabel.textColor = [UIColor whiteColor];
        _workStatusLabel.layer.cornerRadius = 2.0;
        _workStatusLabel.layer.masksToBounds = YES;
        
        [self addSubview:_workStatusLabel];
    }
    return _workStatusLabel;
}

- (UITapGestureRecognizer *)userIconTap{
    if (_userIconTap == nil) {
        _userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnUserIcon)];
        [self.p_UserIconImageView addGestureRecognizer:_userIconTap];
    }
    return _userIconTap;
}

- (UITapGestureRecognizer *)contentViewTap{
    if (_contentViewTap == nil) {
        _contentViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnContentView)];
        [self addGestureRecognizer:_contentViewTap];
    }
    return _contentViewTap;
}

- (void)tapOnUserIcon{
    if (self.block) {
        self.block(self.model , YES);
    }
}

- (void)tapOnContentView{
    if (self.block) {
        self.block(self.model , NO);
    }
}

- (void)callBackWithTapBlock:(TapBlock)block{
    self.block = block;
}

- (void)setDetialModel:(ICN_DynDetialModel *)detialModel{
    _detialModel = detialModel;
    [self userIconTap];
    [self contentViewTap];
    // 通用属性处理
    self.p_CurrentTimeLabel.text = detialModel.createDate;
    if ([detialModel.memberLogo hasPrefix:@"http"]) {
        [self.p_UserIconImageView sd_setImageWithURL:[NSURL URLWithString:(detialModel.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    }else{
        [self.p_UserIconImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(detialModel.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    }
    self.workStatusLabel.text = detialModel.memberProfession;
    // 关于是否加v的处理
    if (detialModel.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    // 执行数据添加的工作
    if (detialModel.roleId) {
        if (detialModel.roleId.integerValue == 1) {
            // 设置必要标签的显示隐藏
            self.p_UserUnivercityNameLabel.hidden = NO;
            self.p_UserUnivercityIconImageView.hidden = NO;
            self.p_MajorIconImageView.hidden = NO;
            self.p_MajorTitleLabel.hidden = NO;
            // 角色为普通
            self.p_UserNameLabel.text = detialModel.memberNick;
            if (detialModel.workStatus.integerValue == 0) {
                // 未就业
                self.p_MajorTitleLabel.text = detialModel.memberMajor;
                self.p_UserUnivercityNameLabel.text = detialModel.memberSchool;
            }else if (detialModel.workStatus.integerValue == 1){
                // 已就业
                self.p_MajorTitleLabel.text = detialModel.memberSchool;
                self.p_UserUnivercityNameLabel.text = detialModel.companyName; // 对应公司按钮
                self.p_UserUnivercityIconImageView.image = [UIImage imageNamed:@"公司"];
                self.p_MajorIconImageView.image = [UIImage imageNamed:@"职位拷贝"];
            }else{
                HRLog(@"用户首页动态数据异常 --- 缺少用户就业状态");
            }
        }else if (detialModel.roleId.integerValue == 2){
            self.p_UserUnivercityNameLabel.hidden = YES;
            self.p_UserUnivercityIconImageView.hidden = YES;
            self.p_MajorIconImageView.hidden = YES;
            self.p_MajorTitleLabel.hidden = YES;
            // 角色为公司
            self.p_UserNameLabel.text = detialModel.companyName;
        }
    }else{
        HRLog(@"用户首页动态数据异常 --- 缺少用户角色类型");
    }
    
    [self layoutIfNeeded];
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    [self configDynStateModelDefaultConfiguration:model]; // 为不完整的测试数据添加必要字段
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    [self userIconTap];
    [self contentViewTap];
    // 通用属性处理
    self.p_CurrentTimeLabel.text = _model.createDate;
    if ([model.memberLogo hasPrefix:@"http"]) {
         [self.p_UserIconImageView sd_setImageWithURL:[NSURL URLWithString:(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    }else{
         [self.p_UserIconImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    }
   
    // 设置状态标签
    if (self.isTransmit) {
        self.workStatusLabel.text = _model.amemberProfession;
    }else{
        self.workStatusLabel.text = _model.memberProfession;
    }
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    // 执行数据添加的工作
    if (_model.roleId) {
        if (_model.roleId.integerValue == 1) {
            // 设置必要标签的显示隐藏
            self.p_UserUnivercityNameLabel.hidden = NO;
            self.p_UserUnivercityIconImageView.hidden = NO;
            self.p_MajorIconImageView.hidden = NO;
            self.p_MajorTitleLabel.hidden = NO;
            // 角色为普通
            if (self.isTransmit) {
                [self.p_UserIconImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
                self.p_UserNameLabel.text = _model.amemberNick;
            }else{
                self.p_UserNameLabel.text = _model.memberNick;
            }
            if (model.workStatus.integerValue == 0) {
                // 未就业
                if (self.isTransmit) {
                    // 显示的是转发人的详情
                    self.p_MajorTitleLabel.text = _model.amemberMajor;
                    self.p_UserUnivercityNameLabel.text = _model.amemberSchool;
                }else{
                    self.p_MajorTitleLabel.text = _model.memberMajor;
                    self.p_UserUnivercityNameLabel.text = _model.memberSchool;
                }
            }else if (model.workStatus.integerValue == 1){
                // 已就业
                if (self.isTransmit) {
                    self.p_MajorTitleLabel.text = _model.amemberSchool;
                    self.p_UserUnivercityNameLabel.text = _model.acompanyName; // 对应公司按钮
                }else{
                    self.p_UserUnivercityNameLabel.text = _model.companyName; // 对应公司按钮
                    self.p_MajorTitleLabel.text = _model.memberPosition;
                }
                self.p_UserUnivercityIconImageView.image = [UIImage imageNamed:@"公司"];
                self.p_MajorIconImageView.image = [UIImage imageNamed:@"职位拷贝"];
            }else{
                HRLog(@"用户首页动态数据异常 --- 缺少用户就业状态");
            }
        }else if (_model.roleId.integerValue == 2){
            self.p_UserUnivercityNameLabel.hidden = YES;
            self.p_UserUnivercityIconImageView.hidden = YES;
            self.p_MajorIconImageView.hidden = YES;
            self.p_MajorTitleLabel.hidden = YES;
            if (self.isTransmit) {
                // 角色为公司
                self.p_UserNameLabel.text = _model.acompanyName;
            }else{
                // 角色为公司
                self.p_UserNameLabel.text = _model.companyName;
            }
        }
    }else{
        HRLog(@"用户首页动态数据异常 --- 缺少用户角色类型");
    }
    
    [self layoutIfNeeded];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGFloat width = self.p_UserIconImageView.height / 2.0;
    self.p_UserIconImageView.layer.cornerRadius = width;
    self.p_UserIconImageView.layer.masksToBounds = YES;
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.p_UserIconImageView];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.p_UserIconImageView);
            make.bottom.equalTo(self.p_UserIconImageView);
        }];
    }
    // 添加状态label  当存在detialModel的时候依据detialModel的数据判断
    if (_detialModel) {
        if (_detialModel.roleId.integerValue == 1) {
            // 普通用户
            [self.workStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.p_UserNameLabel.mas_right).offset(15.0);
                make.centerY.equalTo(self.p_UserNameLabel);
                make.right.lessThanOrEqualTo(self.p_CurrentTimeLabel.mas_left).offset(-5.0);
            }];
        }else{
            [self.workStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.p_UserNameLabel);
                make.top.equalTo(self.p_UserNameLabel.mas_bottom).offset(5.0);
                make.right.lessThanOrEqualTo(self).offset(-5.0);
            }];
        }

    }else{
        if (_model) {
            if (_model.roleId.integerValue == 1) {
                // 普通用户
                [self.workStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.p_UserNameLabel.mas_right).offset(15.0);
                    make.centerY.equalTo(self.p_UserNameLabel);
                    make.right.lessThanOrEqualTo(self.p_CurrentTimeLabel.mas_left).offset(-5.0);
                }];
            }else{
                [self.workStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.p_UserNameLabel);
                    make.top.equalTo(self.p_UserNameLabel.mas_bottom).offset(5.0);
                    make.right.lessThanOrEqualTo(self).offset(-5.0);
                }];
            }
            
        }
    }
    
}

- (void)dealloc{
    
    if (_userIconTap) {
        [self.p_UserIconImageView removeGestureRecognizer:_userIconTap];
        _userIconTap = nil;
    }
    
    if (_contentViewTap) {
        [self.contentView removeGestureRecognizer:_contentViewTap];
        _contentViewTap = nil;
    }
}

#pragma mark --- 私有方法 ---

- (void)configDynStateModelDefaultConfiguration:(ICN_DynStateContentModel *)model{
    // 针对数据不全的现象添加的默认数据处理
    if (model.isTransmit == nil) {
        model.isTransmit = @"0";
    }else{
        if (model.isTransmit.integerValue == 1) {
            // 是转发Model
            if (model.amemberNick == nil) {
                model.amemberNick = @"原作者昵称";
            }
            if (model.amemberProfession == nil) {
                model.amemberProfession = @"";
            }
            if (model.aworkStatus) {
                // 是否就业登记过 登记过则根据等级的字段判断赋值内容是否存在
                switch (model.aworkStatus.integerValue) {
                    case 0:{
                        // 未就业
                        if (model.amemberSchool == nil || [model.amemberSchool isEqualToString:@""]) {
                            model.amemberSchool = @"无学校";
                        }
                        if (model.amemberMajor == nil || [model.amemberMajor isEqualToString:@""]) {
                            model.amemberMajor = @"无专业";
                        }
                        break;
                    }
                    case 1:{
                        // 已工作
                        if (model.acompanyName == nil || [model.acompanyName isEqualToString:@""]) {
                            model.acompanyName = @"无公司";
                        }
                        if (model.amemberPosition == nil || [model.amemberPosition isEqualToString:@""]) {
                            model.amemberPosition = @"无职位";
                        }
                        break;
                        
                    }
                    default:
                        break;
                }
            }else{
                model.aworkStatus = @"0"; // 否则默认设置为未登记
                [self configDynStateModelDefaultConfiguration:model];
            }
        }
    }
    if (model.roleId) {
        if (model.memberNick == nil || [model.memberNick isEqualToString:@""]) {
            model.memberNick = @"用户";
        }
        switch (model.roleId.integerValue) {
            case 1:
            case 2:{
                if (model.workStatus) {
                    // 是否就业登记过 登记过则根据等级的字段判断赋值内容是否存在
                    if (model.memberProfession == nil) {
                        model.memberProfession = @"";
                    }
                    switch (model.workStatus.integerValue) {
                        case 0:{
                            // 未就业
                            if (model.memberSchool == nil || [model.memberSchool isEqualToString:@""]) {
                                model.memberSchool = @"无学校";
                            }
                            if (model.memberMajor == nil || [model.memberMajor isEqualToString:@""]) {
                                model.memberMajor = @"无专业";
                            }
                            break;
                        }
                        case 1:{
                            // 已工作
                            if (model.companyName == nil || [model.companyName isEqualToString:@""]) {
                                model.companyName = @"无公司";
                            }
                            if (model.memberPosition == nil || [model.memberPosition isEqualToString:@""]) {
                                model.memberPosition = @"无职位";
                            }
                            break;
                            
                        }
                        default:
                            break;
                    }
                }else{
                    model.workStatus = @"0"; // 否则默认设置为未登记
                    [self configDynStateModelDefaultConfiguration:model];
                }
                break;
            }
            default:
                break;
        }
    }else{
        model.roleId = @"1";
        // 设置默认值后递归调用
        [self configDynStateModelDefaultConfiguration:model];
    }
    
}


@end
