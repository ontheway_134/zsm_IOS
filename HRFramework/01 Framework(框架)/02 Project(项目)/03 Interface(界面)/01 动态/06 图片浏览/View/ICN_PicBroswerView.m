//
//  ICN_PicBroswerView.m
//  ICan
//
//  Created by albert on 2017/1/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PicBroswerView.h"

#define PHolderImage [UIImage imageNamed:@"占位图"]

@interface ICN_PicBroswerView ()<UIScrollViewDelegate>

@property (nonatomic , strong)UITapGestureRecognizer *viewTap; // 页面的轻拍手势
@property (nonatomic , strong)UIScrollView *scroll; // 当前页面用于滚动的scroll
@property (nonatomic , strong)UIPageControl *pageControl; // 页面指示器

@property (nonatomic , strong)NSMutableArray <UIImageView *>* imageViewListArr; // 当前图片列表

@end

@implementation ICN_PicBroswerView

- (UITapGestureRecognizer *)viewTap{
    if (_viewTap == nil) {
        _viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnTapViewHiddenAction)];
        [self addGestureRecognizer:_viewTap];
    }
    return _viewTap;
}

- (UIPageControl *)pageControl{
    if (_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
        _pageControl.hidesForSinglePage = YES;
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

- (NSMutableArray<UIImageView *> *)imageViewListArr{
    if (_imageViewListArr == nil) {
        _imageViewListArr = [NSMutableArray array];
    }
    return _imageViewListArr;
}

- (UIScrollView *)scroll{
    if (_scroll == nil) {
        _scroll = [[UIScrollView alloc] initWithFrame:SCREEN_BOUNDS];
        [self addSubview:_scroll];
        _scroll.backgroundColor = [UIColor blackColor];
        _scroll.delegate = self;
        _scroll.scrollEnabled = YES;
        _scroll.userInteractionEnabled = YES;
        _scroll.pagingEnabled = YES;
        _scroll.showsVerticalScrollIndicator = NO;
        _scroll.showsHorizontalScrollIndicator = NO;
    }
    return _scroll;
}

- (void)setUrlPathArr:(NSArray *)urlPathArr{
    _urlPathArr = urlPathArr;
    if (urlPathArr != nil && urlPathArr.count > 0) {
        
        // 先清空之前的内容
        [self clearImageViewListFounction];
        self.scroll.contentSize = CGSizeMake(SCREEN_WIDTH * _urlPathArr.count, SCREEN_HEIGHT);
        for (NSInteger i = 0; i < _urlPathArr.count; i++) {
            UIImageView *imageView = [self createImageViewWithPath:_urlPathArr[i]];
            [self.imageViewListArr addObject:imageView];
            [self.scroll addSubview:imageView];
        }
        [self setNeedsLayout];
    }
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor blackColor];
        [self viewTap];
    }
    return self;
}

- (UIImageView *)createImageViewWithPath:(NSString *)path{
    UIImageView *imageView = [[UIImageView alloc]  initWithFrame:CGRectZero];
    [imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(path)] placeholderImage:PHolderImage];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = YES;
    return imageView;
}

// 用于清空imageview列表的方法
- (void)clearImageViewListFounction{
    if (self.imageViewListArr.count > 0) {
        for (UIImageView *imageView in self.imageViewListArr) {
            [imageView removeFromSuperview];
        }
        [_imageViewListArr removeAllObjects];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (self.imageViewListArr.count > 0) {
        // 开始对于imageview布局
        for (NSInteger i = 0; i < self.imageViewListArr.count; i++) {
            UIImageView *imageView = self.imageViewListArr[i];
            [imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.scroll);
                make.width.equalTo(self);
            }];
            if (i == 0) {
                [imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.scroll);
                }];
            }else{
                [imageView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.imageViewListArr[i - 1].mas_right);
                }];
            }
        }
        
        // 调节pagecontroll
        self.pageControl.numberOfPages = self.imageViewListArr.count;
        self.pageControl.size = CGSizeMake(self.imageViewListArr.count * 26.5, 20);
        [self addSubview:self.pageControl];
        [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(self.bottom).offset(-42.0);
            make.centerX.equalTo(self.mas_centerX);
        }];
        
        // 将页面跳转到需要的位置
        [self.scroll setContentOffset:CGPointMake(SCREEN_WIDTH * self.currentIndex, 0) animated:NO];
        self.pageControl.currentPage = self.currentIndex;

    }
}

// 在页面隐藏之后做的清空历史内容操作
- (void)clearCurrentPageContentWhileViewWillHidden{
    self.currentIndex = 0;
    self.urlPathArr = nil;
    [self.pageControl removeFromSuperview];
    _pageControl = nil;
    [self clearImageViewListFounction];
}

// 点击隐藏当前页面的按钮
- (void)clickOnTapViewHiddenAction{
    if (self.hidden == NO) {
        self.hidden = YES;
        [self clearCurrentPageContentWhileViewWillHidden];
    }
}

#pragma mark --- UIScrollViewDelegate ---

/** 停止减速的时候 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    self.pageControl.currentPage = index;
}

@end
