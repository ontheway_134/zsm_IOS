//
//  ICN_PictureBrowserView.h
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_PictureBrowserView : UIView

@property (nonatomic , strong)NSArray<NSString *> * urlListArr;
@property (nonatomic , strong)NSArray <UIImage *> * imageListArr;

- (instancetype)initWithFrame:(CGRect)frame
                 ImageUrlList:(NSArray <NSString *>*)strUrlArr;

- (instancetype)initWithFrame:(CGRect)frame
                   ImagesList:(NSArray <UIImage *>*)imagesArr;



@end
