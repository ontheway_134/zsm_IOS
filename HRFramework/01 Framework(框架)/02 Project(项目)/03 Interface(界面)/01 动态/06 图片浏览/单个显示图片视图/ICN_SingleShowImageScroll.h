//
//  ICN_SingleShowImageScroll.h
//  ICan
//
//  Created by albert on 2017/1/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_SingleShowImageScroll : UIScrollView

- (instancetype)initWithFrame:(CGRect)frame ImagePath:(NSString *)path;

@end
