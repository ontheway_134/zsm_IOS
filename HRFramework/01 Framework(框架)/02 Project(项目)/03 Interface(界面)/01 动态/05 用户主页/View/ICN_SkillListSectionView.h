//
//  ICN_SkillListSectionView.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ICN_SkillTagModel;
@interface ICN_SkillListSectionView : UITableViewHeaderFooterView

@property (nonatomic , strong)NSMutableArray <ICN_SkillTagModel *> *modelsArr;

@end
