//
//  ICN_UserReviewCell.h
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//  用户评论Cell
//

#import <UIKit/UIKit.h>

@class ICN_DynamicStateCommentModel;

typedef void(^ModelBlock)(ICN_DynamicStateCommentModel *model , BOOL tap);

@interface ICN_UserReviewCell : UITableViewCell

@property (nonatomic , assign , getter=isFirstCell)BOOL firstCell;


@property (nonatomic , strong)ICN_DynamicStateCommentModel *model;
@property (nonatomic , copy)ModelBlock block;


- (void)callBackWithBtnBlock:(ModelBlock)block;

@end
