//
//  ICN_UserMoreFounctionViewModel.h
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//  用户主页的更多功能按钮 - 在进入时需要获取上级页面的基本信息

#import <Foundation/Foundation.h>

@class ICN_DynUserBaseMSGModel;

// 设置私有方法类枚举
typedef enum : NSUInteger {
    PRIVATE_UpdateBaseMsg = 13304450, // 更新基本信息 13304450
    PRIVATE_ForbidUserDynamicRight,   // 不让谁 看我的动态
    PRIVATE_CancerForbidUserDynamicRight,
    PRIVATE_DontSeeUsersDynamic,      // 我不看谁 的动态
    PRIVATE_CancerDontSeeUsersDynamic,
    PRIVATE_Defriend,                 // 拉黑
    PRIVATE_ReFriend,                 // 取消拉黑
    PRIVATE_DeleteFriend,             // 删除好友
    PRIVATE_AddFriend,
} UserNetWorkResponseType;

@protocol UserMoreFounctionDelegate <NSObject>

// 获取到数据处理之后的回调
- (void)responseWithEnumType:(UserNetWorkResponseType)enumType Success:(BOOL)success Info:(NSString *)info;

@end

@interface ICN_UserMoreFounctionViewModel : NSObject

@property (nonatomic , strong)ICN_DynUserBaseMSGModel *baseModel; // 用户基本信息的Model
@property (nonatomic , weak)id<UserMoreFounctionDelegate> delegate;

// 根据网络请求的类型以及用户的memeberId 进行对应的操作
- (void)requestWithEnumType:(UserNetWorkResponseType)enumType MemberId:(NSString *)memberId;


@end



