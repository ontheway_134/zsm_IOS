//
//  ICN_UserMoreFounctionViewModel.m
//  ICan
//
//  Created by albert on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserMoreFounctionViewModel.h"
#import "BaseOptionalModel.h" // 基类基本信息Model
#import "ICN_DynUserBaseMSGModel.h" // 用户基本信息Model

@implementation ICN_UserMoreFounctionViewModel

- (void)requestWithEnumType:(UserNetWorkResponseType)enumType MemberId:(NSString *)memberId{
    
    // 第一步 判断token 此时不对token不存在的后续操作做任何校验
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        return ;
    }
    
    // 第二步 根据不同的网络请求类型生成不同的路径 和传入参数
    NSString *netPath ;
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    
    switch (enumType) {
        case PRIVATE_Defriend:{
            // 拉黑好友
            netPath = PATH_SingleDeFriend;
            [params setValue:memberId forKey:@"blackId"];
            break;
        }
        case PRIVATE_DeleteFriend:{
            // 删除好友
            netPath = PATH_DELETEFRIDENT;
            [params setValue:memberId forKey:@"friendId"];
            break;
        }
        case PRIVATE_AddFriend:{
            // 添加好友
            netPath = PATH_ADDFRIDENT;
            [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"fromId"];
            [params setValue:nil forKey:@"token"];
            [params setValue:memberId forKey:@"toId"];
            break;
        }
        case PRIVATE_ReFriend:{
            // 取消拉黑好友
            netPath = PATH_CancerSingleDeFriend;
            [params setValue:memberId forKey:@"sideId"];
            break;
        }
        case PRIVATE_UpdateBaseMsg:{
            // 更新基本信息
            netPath = PATH_UserMainMsg;
            [params setValue:memberId forKey:@"sideId"];
            break;
        }
        case PRIVATE_DontSeeUsersDynamic:{
            // 不看 谁的动态
            netPath = PATH_DontSeeUserDyn;
            [params setValue:memberId forKey:@"passId"];
            break;
        }
        case PRIVATE_CancerDontSeeUsersDynamic:{
            // 取消 不看 谁的动态
            netPath = PATH_CancerDontSeeUserDyn;
            [params setValue:memberId forKey:@"sideId"];
            break;
        }
        case PRIVATE_ForbidUserDynamicRight:{
            // 不允许谁 看我的动态
            netPath = PATH_ForbidUserDynRight;
            [params setValue:memberId forKey:@"unlookId"];
            break;
        }
        case PRIVATE_CancerForbidUserDynamicRight:{
            // 取消 不允许谁 看我的动态
            netPath = PATH_CancerForbidUserDynRight;
            [params setValue:memberId forKey:@"sideId"];
            break;
        }
        default:
            break;
    }
    
    // 在确认ViewModel支持回调数据的时候添加网络请求
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
        
        // 开启网络请求并针对数据进行处理
        [[HRRequest manager] POST_PATH:netPath params:params success:^(id result) {
            // 1. 判断是否成功获取到数据
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                
                // 网络请求的结果成功 -- 判断是不是对于基本信息的处理
                if (enumType == PRIVATE_UpdateBaseMsg) {
                    // 更新用户基本信息成功
                    self.baseModel = [[ICN_DynUserBaseMSGModel alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
                }
                // 进行数据处理成功的回调
                [self.delegate responseWithEnumType:enumType Success:YES Info:baseModel.info];
                
            }else{
                [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
            }

        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:enumType Success:NO Info:@"网络请求失败"];
        }];
        
    }else{
        HRLog(@"test=== 用户主页的更多信息没有被设置代理");
    }
}



@end
