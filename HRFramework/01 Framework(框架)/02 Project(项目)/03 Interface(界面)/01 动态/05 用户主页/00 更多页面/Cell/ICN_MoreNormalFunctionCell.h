//
//  ICN_MoreNormalFunctionCell.h
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^FunctionBlock)(NSInteger index , BOOL isSelected); // Cell的位置 带有是否选中属性的按钮

@interface ICN_MoreNormalFunctionCell : UITableViewCell

@property (nonatomic , assign , getter=isButtonHidden)BOOL hiddenBtn; // 是否隐藏按钮的属性
@property (nonatomic , copy)NSString *title; // 标签名
@property (nonatomic , assign)NSInteger cellIndex; // Cell当前的索引位置
@property (weak, nonatomic) IBOutlet UIButton *functionButton; // 功能按钮
@property (nonatomic , copy)FunctionBlock block;



- (void)callBackFunctionBlock:(FunctionBlock)block;


@end
