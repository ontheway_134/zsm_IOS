//
//  ICN_MoreJumpFounctionCell.m
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MoreJumpFounctionCell.h"

@interface ICN_MoreJumpFounctionCell ()

@property (weak, nonatomic) IBOutlet UILabel *moreTitleLabel; // 标题标签

@property (weak, nonatomic) IBOutlet UIButton *moreDetialButton; // 跳转按钮

@property (weak, nonatomic) IBOutlet UIImageView *moreDetialIcon; // 跳转按钮图标



// 分隔栏相关 =====
@property (weak, nonatomic) IBOutlet UIView *topSegmentView;

@property (weak, nonatomic) IBOutlet UIView *bottomSegmentView;



@end

@implementation ICN_MoreJumpFounctionCell

- (void)setTopHidden:(BOOL)topHidden{
    _topHidden = topHidden;
    self.topSegmentView.hidden = _topHidden;
}

- (void)setBottomHidden:(BOOL)bottomHidden{
    _bottomHidden = bottomHidden;
    self.bottomSegmentView.hidden = _bottomHidden;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    if (_title) {
        self.moreTitleLabel.text = _title;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOnButtonAction:(UIButton *)sender {
    
}


@end
