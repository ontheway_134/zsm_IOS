//
//  ICN_MoreJumpFounctionCell.h
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_MoreJumpFounctionCell : UITableViewCell

@property (nonatomic , assign)BOOL topHidden; // 顶部隐藏分隔栏

@property (nonatomic , assign)BOOL bottomHidden; // 底部隐藏分隔栏

@property (nonatomic , copy)NSString *title; // 标签名



@end
