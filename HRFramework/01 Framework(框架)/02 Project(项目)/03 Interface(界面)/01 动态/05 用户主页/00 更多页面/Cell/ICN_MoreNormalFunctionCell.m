//
//  ICN_MoreNormalFunctionCell.m
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MoreNormalFunctionCell.h"

@interface ICN_MoreNormalFunctionCell ()

@property (weak, nonatomic) IBOutlet UILabel *functionTitleLabel; // 方法名的标签




@end

@implementation ICN_MoreNormalFunctionCell

- (void)setHiddenBtn:(BOOL)hiddenBtn{
    _hiddenBtn = hiddenBtn;
    self.functionButton.hidden = _hiddenBtn;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    if (_title) {
        self.functionTitleLabel.text = _title;
    }
}

- (IBAction)clickOnButtonAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    // 把按钮将要进行的状态传递
    self.block(self.cellIndex , sender.selected);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)callBackFunctionBlock:(FunctionBlock)block{
    self.block = block;
}




@end
