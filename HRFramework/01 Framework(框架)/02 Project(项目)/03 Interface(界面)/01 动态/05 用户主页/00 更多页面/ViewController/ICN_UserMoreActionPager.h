//
//  ICN_UserMoreActionPager.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"


@interface ICN_UserMoreActionPager : BaseViewController

@property (nonatomic , copy)NSString *memberId; // 用户id

@property (nonatomic, copy) NSString * memberName;

@property (nonatomic, copy) NSString * pic;

@property (nonatomic , copy) NSString *canReport;

@end

