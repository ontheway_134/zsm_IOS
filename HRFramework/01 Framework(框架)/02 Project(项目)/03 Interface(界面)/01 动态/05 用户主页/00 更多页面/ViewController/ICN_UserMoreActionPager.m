//
//  ICN_UserMoreActionPager.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#pragma mark --- 私有宏 ---
#define PRIVATE_TitleContentArr @[@"推荐给好友" , @"不让Ta看我的动态" , @"不看Ta的动态" , @"删除好友" , @"加入黑名单" , @"举报"]

#import "ICN_UserMoreActionPager.h"
#import "ICN_DynUserBaseMSGModel.h" // 用户基本数据Model
#import "ICN_UserMoreFounctionViewModel.h" // 用户的更多处理的ViewModel
// 导入Cell相关头文件 --
#import "ICN_MoreJumpFounctionCell.h" // 跳转到详情页面按钮的Cell
#import "ICN_MoreNormalFunctionCell.h" // 其他Cell
// 跳转页面
#import "ICN_UserReportDetialVC.h" // 用户举报详情页面
#import "ICN_MyfridentViewController.h" // 我的好友页面


@interface ICN_UserMoreActionPager ()<UITableViewDelegate , UITableViewDataSource , UserMoreFounctionDelegate>

@property (nonatomic , strong)UITableView *tableView;
@property (nonatomic , strong)NSArray <NSString *>* contentTltleArr; // 用户存储标签名称的数组
@property (nonatomic , strong)ICN_UserMoreFounctionViewModel *viewModel;


@end

@implementation ICN_UserMoreActionPager

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_MoreJumpFounctionCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_MoreJumpFounctionCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_MoreNormalFunctionCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_MoreNormalFunctionCell class])];
        
        // 用于去掉当tableview的Cell不能不满屏的时候显示的空白分割Cell
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        self.tableView.backgroundColor = RGB(235, 236, 237);
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

#pragma mark - ---------- 声明周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"更多";
    self.hiddenDefaultNavBar = NO;
    [self tableView]; // 配置tableView
    // 初始化标签名称数组
    self.contentTltleArr = PRIVATE_TitleContentArr;
    // 初始化ViewModel
    self.viewModel = [[ICN_UserMoreFounctionViewModel alloc] init];
    self.viewModel.delegate = self;
    // 刷新基本信息
    [self.viewModel requestWithEnumType:PRIVATE_UpdateBaseMsg MemberId:self.memberId];
    
    // Do any additional setup after loading the view from its nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

/** 添加好友的弹窗 */
- (void)responseWithAddCurrentFriendAction{
    UIAlertController *pager = [UIAlertController alertControllerWithTitle:@"确认添加好友" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmBtn = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 实行添加好友的相关操作
        [self.viewModel requestWithEnumType:PRIVATE_AddFriend MemberId:self.memberId];
    }];
    UIAlertAction *cancerBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [pager addAction:confirmBtn];
    [pager addAction:cancerBtn];
    [self presentViewController:pager animated:YES completion:nil];
}

/** 用于弹出删除好友弹窗的方法 */
- (void)responseWithDeleteCurrentFriendAction{
    
    UIAlertController *pager = [UIAlertController alertControllerWithTitle:@"确认删除好友" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmBtn = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 实行删除好友的相关操作
        [self.viewModel requestWithEnumType:PRIVATE_DeleteFriend MemberId:self.memberId];
    }];
    UIAlertAction *cancerBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [pager addAction:confirmBtn];
    [pager addAction:cancerBtn];
    [self presentViewController:pager animated:YES completion:nil];
}

// 举报跳转方法
- (void)jumpToReportDetialPager{
    NSString *errorMsg ;
    if (self.canReport) {
        if ([self.canReport integerValue] == 1) {
            errorMsg = @"无法举报自己";
        }
        if ([self.canReport integerValue] == 2) {
            errorMsg = @"已经举报了";
        }
    }
    if (errorMsg) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:errorMsg];
    }else{
        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
        pager.memberId = self.memberId;
        [self currentPagerJumpToPager:pager];
    }
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- ViewModel的相关代理 ---

- (void)responseWithEnumType:(UserNetWorkResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    // 因为在发送网络请求之前就已经改变了状态 ， 所以当网络请求失败之后需要重置状态
    // 并且为了记录之前改变成功的状态 在成功改变状态之后需要重新请求基本信息接口
    switch (enumType) {
        case PRIVATE_UpdateBaseMsg:{
            // 之前进行的是刷新基本信息操作
            if (success) {
                // 在更新数据成功之后 刷新数据
                [self.tableView reloadData];
            }else{
                // 只有在更新失败的时候弹窗提示原因
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }
            break ;
        }
        // 其他状态的网络请求在失败的时候通过刷新数据重置状态 成功则刷新基本信息数据
        default:{
            if (success) {
                // 成功
                [self.viewModel requestWithEnumType:PRIVATE_UpdateBaseMsg MemberId:self.memberId];
                // 弹出HUD
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                // 失败
                [self.tableView reloadData];
            }
            break;
        }
    }
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //相关逻辑 -- 根据选中的Cell添加对应的功能
    switch (indexPath.row) {
        case 0:{
            // 推荐给好友 == 跳转到对应页面
            ICN_MyfridentViewController *pager = [[ICN_MyfridentViewController alloc] init];
            NSDictionary *ext = @{@"shareType":@"1",@"shareId":self.memberId,@"contentPic":ICN_IMG(self.pic) ,@"content":self.memberName};
            pager.ext = ext;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case 3:{
            // 对于好友的操作
            if (self.viewModel.baseModel.delStatus == nil || [self.viewModel.baseModel.delStatus isEqualToString:@""]) {
                // 添加好友的操作
                [self responseWithAddCurrentFriendAction];
            }
            if ([self.viewModel.baseModel.delStatus integerValue]) {
                if ([self.viewModel.baseModel.delStatus integerValue] == 1) {
                    // 删除好友
                    [self responseWithDeleteCurrentFriendAction];
                }
            }
            break;
        }
        case 4:{
            // 加入黑名单 / 取消加入黑名单 == 需要判断
            //test=== 未完成需要判断
            if ([self.viewModel.baseModel.isBlack integerValue] == 1) {
                // ta在我的黑名单列表中 显示取消黑名单 / 进行取消黑名单操作
                [self.viewModel requestWithEnumType:PRIVATE_ReFriend MemberId:self.memberId];
            }else{
                // ta不在我的黑名单列表中 显示添加黑名单 / 进行添加到黑名单操作
                [self.viewModel requestWithEnumType:PRIVATE_Defriend MemberId:self.memberId];
            }
            break;
        }
        case 5:{
            // 举报好友 == 跳转到对应页面
            [self jumpToReportDetialPager];
            break;
        }
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据索引值判断需要返回的Cell
    NSInteger tag = indexPath.row;
    switch (tag) {
        // 显示能跳转到详情页面的Cell
        case 0:
        case 5:{
            ICN_MoreJumpFounctionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_MoreJumpFounctionCell class])];
            // 推荐给好友隐藏顶部分隔栏
            if (tag == 0) {
                cell.topHidden = YES;
                cell.bottomHidden = NO;
            }
            // 举报隐藏底部分隔栏
            if (tag == 5) {
                cell.topHidden = NO;
                cell.bottomHidden = YES;
            }
            // 赋值字段名
            cell.title = self.contentTltleArr[tag];
            return cell;
            break;
        }
        case 1:
        case 2:
        case 3:
        case 4:{
            ICN_MoreNormalFunctionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_MoreNormalFunctionCell class])];
            // 设置Cell的标题字段
            cell.title = self.contentTltleArr[tag];
            // 让不让用户看动态的两种状态
            if (tag == 1 || tag == 2) {
                cell.hiddenBtn = NO;
                // 1. 不让他看我的动态  2. 不看他的动态
                if (tag == 1) {
                    // 1. 不让他看我的动态
                    cell.functionButton.selected = [self.viewModel.baseModel.isUnlook integerValue] == 0 ? NO : YES;
                }
                if (tag == 2) {
                    // 2. 不看他的动态
                    cell.functionButton.selected = [self.viewModel.baseModel.isPass integerValue] == 0 ? NO : YES;
                }
            }
            // 删除好友还是加入黑名单的状态
            if (tag == 3 || tag == 4) {
                cell.hiddenBtn = YES;
                if (tag == 3 && [self.viewModel.baseModel.delStatus integerValue]) {
                    // 判断删除好友显示的字段
                    cell.title = [self.viewModel.baseModel.delStatus integerValue] == 1 ? self.contentTltleArr[tag] : @"已删除";
                }
                // 判断添加好友的状态
                if (tag == 3 && ([self.viewModel.baseModel.delStatus isEqualToString:@""] || self.viewModel.baseModel.delStatus == nil)) {
                    cell.title = @"添加好友";
                }
            }
            // 处理是否加入黑名单
            if (tag == 4) {
                if ([self.viewModel.baseModel.isBlack integerValue] != 1) {
                    // ta在我的黑名单列表中 显示取消黑名单 / 进行取消黑名单操作
                    cell.title = self.contentTltleArr[tag];
                }else{
                    // ta不在我的黑名单列表中 显示添加黑名单 / 进行添加到黑名单操作
                    cell.title = @"取消黑名单";
                }
            }
            
            // Cell的回调方法
            [cell callBackFunctionBlock:^(NSInteger index, BOOL isSelected) {
                if (tag == 1) {
                    // 1. 不让他看我的动态
                    if (isSelected) {
                        [self.viewModel requestWithEnumType:PRIVATE_ForbidUserDynamicRight MemberId:self.memberId];
                    }else{
                        // 开启 他看我动态的权限
                        [self.viewModel requestWithEnumType:PRIVATE_CancerForbidUserDynamicRight MemberId:self.memberId];
                    }
                }
                if (tag == 2) {
                    // 2. 不看他的动态
                    cell.functionButton.selected = [self.viewModel.baseModel.isUnlook integerValue] == 0 ? NO : YES;
                    if (isSelected) {
                        [self.viewModel requestWithEnumType:PRIVATE_DontSeeUsersDynamic MemberId:self.memberId];
                    }else{
                        // 开启 看ta动态的权限
                        [self.viewModel requestWithEnumType:PRIVATE_CancerDontSeeUsersDynamic MemberId:self.memberId];
                    }
                
                }
            }];
            // 设置Cell的当前索引
            cell.cellIndex = tag;
            return cell;
            
            break;
        }
        default: break;
    }
    return nil;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 上下是45.0 区域都是40
    if (indexPath.row == 0 || indexPath.row == 5) {
        return 45.0;
    }
    return 40.0;
}



@end
