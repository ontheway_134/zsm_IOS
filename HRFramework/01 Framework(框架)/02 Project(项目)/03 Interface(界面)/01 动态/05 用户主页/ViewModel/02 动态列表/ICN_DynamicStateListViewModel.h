//
//  ICN_DynamicStateListViewModel.h
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_DynStateContentModel;
@protocol ICN_DynamicStateListDelegate <NSObject>

// =========== 动态列表相关回调
/** 在动态列表网络请求获取之后的回调
    1. success ： 网络请求是否成功
    2. code ： 网络请求的状态码 ： 用于判断请求成功后是否获得想要的数据
 */
- (void)responseWithDynamicStateListRequestSuccess:(BOOL)success
                                              Code:(NSInteger)code;

// ===========  点赞相关回调
/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success;

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority;

/** 已经点赞后的回调 */
- (void)responseWithAlreadyLikeUp;

@end

@interface ICN_DynamicStateListViewModel : NSObject

@property (nonatomic , strong)NSMutableArray *modelsArr;
@property (nonatomic , strong)NSMutableArray *wisdomModelsArr;
@property (nonatomic , weak)id<ICN_DynamicStateListDelegate> delegate;
@property (nonatomic , assign , getter=isMyDynamic)BOOL mainDynamic;
@property (nonatomic , assign , getter=isMyWisdom)BOOL mainWisdom;
@property (nonatomic , copy)NSString *memberId; // 根据用户Id获取动态等信息


// 点赞功能
- (void)likeUpWithType:(NSInteger)type
                 Model:(ICN_DynStateContentModel *)model;

// 刷新当前页面的网络请求
- (void)refreshCurrentPageContentCells;


/*辛大大的网络请求 2017 3 16*/
- (void)refreshDynamicIntelligenceAF;
/*辛大大的网络请求 2017 3 16*/
-(void)XDDAF;


// 加载下一个页面的网络请求
- (void)loadNextPageContentCells;

@end
