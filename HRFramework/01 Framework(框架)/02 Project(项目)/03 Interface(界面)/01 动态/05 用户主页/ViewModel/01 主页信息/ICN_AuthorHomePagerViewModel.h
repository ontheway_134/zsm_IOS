//
//  ICN_AuthorHomePagerViewModel.h
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICN_AuthorHomePagerHeader.h" // 添加用户主页头文件
#import "ICN_DynUserBaseMSGModel.h" // 用户主要信息Model

@protocol ICN_AuthorHomePagerDelegate <NSObject>

/** 将用户主页需要的全部数据返回 */
- (void)responseWithModelsDic:(NSMutableDictionary *)modesDic;

/** 在获取数据失败之后将获取失败的数据的key返回 */
- (void)responseNetRequestFailedWithKey:(NSString *)key;

/** 在获取到用户主页基本信息数据后响应的方法 */
- (void)responseWithUserBaseMassageRequest:(BOOL)success Error:(NSString *)error;

/** 在用户添加或者删除好友后的回调 */
- (void)responseWithAddFriend:(BOOL)isAdd Success:(BOOL)success Error:(NSString *)error;

@end

@interface ICN_AuthorHomePagerViewModel : NSObject

@property (nonatomic , weak)id<ICN_AuthorHomePagerDelegate> delegate;

@property (nonatomic , strong)NSMutableDictionary *modelsDic; // 用户主页页面的全部Model字典
@property (nonatomic , strong)ICN_DynUserBaseMSGModel *userBaseModel; // 用户基本信息Model

@property (nonatomic , assign , getter=isMe)BOOL mine; // 当前显示的主页是不是自己的。默认不是

// 刷新当前页面的网络请求
- (void)refreshCurrentPageContentCellsWithMemberId:(NSString *)memberId;

// 加载当前用户的基本信息的网络请求 参数 ： sideID（用户id）
- (void)loadUserBaseMessageWithUserId:(NSString *)sideId;

// 添加/删除好友调用的接口 参数 isAdd(添加还是删除) memberId(处理的好友的id)
- (void)requestFriendAdd:(BOOL) isAdd WithMemberId:(NSString *)memberId;


@end
