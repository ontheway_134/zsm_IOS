//
//  ICN_AuthorHomePagerHeader.h
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#ifndef ICN_AuthorHomePagerHeader_h
#define ICN_AuthorHomePagerHeader_h

#pragma mark - ---------- 常用常量 ----------

static NSString *const KEY_SignTitle = @"SignTitle"; // 个性签名
static NSString *const KEY_SameFriends = @"SameFriends"; // 共同好友
static NSString *const KEY_DynamicStates = @"DynamicStates"; // 动态列表
static NSString *const KEY_WisdomStates = @"WisdomStates"; // 智讯列表
static NSString *const KEY_ComplainStates = @"ComplainStates"; // 吐槽列表
static NSString *const KEY_QuestionStates = @"QuestionStates"; // 提问列表
static NSString *const KEY_SkillSigns = @"SkillSigns"; // 技能标签
static NSString *const KEY_EduExperience = @"EducationExperience"; // 教育经历
static NSString *const KEY_WorkExperience = @"WorkExperience"; // 工作经历
static NSString *const KEY_ApplyActivity = @"ApplyActivity"; // 参加的活动
static NSString *const KEY_ApplyGroups = @"ApplyGroups"; // 参加的小组



#endif /* ICN_AuthorHomePagerHeader_h */
