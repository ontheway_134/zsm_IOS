//
//  ICN_ExperiencessListVC.m
//  ICan
//
//  Created by albert on 2017/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ExperiencessListVC.h"
#import "ICN_DynEducationModel.h"
#import "ICN_DynWorkExperienceModel.h"
#import "ICN_DynExperienceCell.h"

@interface ICN_ExperiencessListVC ()<UITableViewDelegate , UITableViewDataSource>

@property (nonatomic , strong)UITableView *tableView;

@end

@implementation ICN_ExperiencessListVC

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = self.view.frame;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynExperienceCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        // 设置在列表页面不能铺满的时候显示分隔行的问题
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenDefaultNavBar = NO;
    if (self.modelsArr) {
        if ([self.modelsArr.firstObject isKindOfClass:[ICN_DynWorkExperienceModel class]]) {
            // 显示的是工作经历
            self.naviTitle = @"工作经历";
        }else{
            self.naviTitle = @"教育经历";
        }
        [self tableView];
    }
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置tableview的约束
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_DynExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
    if ([self.modelsArr.firstObject isKindOfClass:[ICN_DynWorkExperienceModel class]]) {
        cell.workModel = self.modelsArr[indexPath.row];
    }else{
        cell.eduModel = self.modelsArr[indexPath.row];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}




@end
