//
//  ICN_UserDynamicStateListVC.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserDynamicStateListVC.h"
#import "ICN_CommonPsersonDynamicCell.h" // 个人动态Cell
#import "ICN_UserDynamicStateDetialVC.h" // 个人动态详情VC
#import "ICN_DynWarnView.h" // 提示窗页面
#import "ICN_DynamicStateListViewModel.h"
#import "ICN_DynStateContentModel.h"

#import "ICN_ShareManager.h"
#import "ICN_ApplyModel.h"

#pragma mark - ---------- 跳转页面 ----------
#import "ICN_ReviewPublication.h" // 跳转到转发发布页面


@interface ICN_UserDynamicStateListVC ()<UITableViewDelegate , UITableViewDataSource , ICN_DynWarnViewDelegate , ICN_DynamicStateListDelegate>

@property (nonatomic , retain)UITableView *tableView;

@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
@property (nonatomic , strong)ICN_DynamicStateListViewModel *viewModel;
@property (nonatomic , strong)ICN_DynStateContentModel *replyModel; // 转发Model;


@end

@implementation ICN_UserDynamicStateListVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = RGB(235, 236, 237);
    }
    return _tableView;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[ICN_DynamicStateListViewModel alloc] init];
    self.viewModel.delegate = self;
    if (self.isMyDynamic) {
        self.naviTitle = @"我的动态";
        self.viewModel.mainDynamic = YES;
    }else{
        self.naviTitle = @"他的动态";
        self.viewModel.mainDynamic = NO;
        self.viewModel.memberId = self.memberId; // 如果要获取的是用户的Id的动态列表需要将其传过去
    }
    if (self.isMyWisdom) {
        self.naviTitle = @"我的智讯";
        self.viewModel.mainWisdom = self.isMyWisdom;
    }else{
        self.viewModel.mainWisdom = NO;
    }
    [self tableView];
    [self configTableViewRefreshHeaderFooterView];
    
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_warnView) {
        [_warnView removeFromSuperview];
    }
}

- (void)dealloc{
    if (_warnView) {
        _warnView.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---------- 私有方法 ----------

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self.viewModel refreshCurrentPageContentCells];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.viewModel loadNextPageContentCells];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


#pragma mark - ---------- 代理 ----------


#pragma mark --- ICN_DynamicStateListDelegate ---

- (void)responseWithDynamicStateListRequestSuccess:(BOOL)success Code:(NSInteger)code{
    if (success) {
        if (code == 0) {
            [self.tableView reloadData];
        }
        if (code == 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该用户未登录"];
        }
    }else{
        // 网络请求失败
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败 - 请稍后重试"];
    }

}

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"点赞失败"];
    }else{
        [self.tableView reloadData];
    }
}

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您无法为该用户点赞"];
}

/** 已经点赞后的回调 */
- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经为该用户点过赞"];
    [self.tableView reloadData];
}

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";


- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            // 判断是自己的动态不转发
            if (self.replyModel != nil && self.replyModel.isMe.integerValue == 1) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无法转发自己的动态"];
                return ;
            }
            ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
            pager.model = self.replyModel;
            self.replyModel = nil;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
            
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            
            NSLog(@"分享到微信朋友圈");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            
            NSLog(@"分享到QQ空间");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        default:
            [self.warnView removeFromSuperview];
            break;
    }
}

- (void)cofigShareContent{
    if (self.replyModel.title == nil || [self.replyModel.title isEqualToString:@""]) {
        title = self.replyModel.memberNick;
    }else{
        title = self.replyModel.memberNick;
    }
    if (self.replyModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replyModel.memberLogo);
    }
    if (self.replyModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replyModel.summary == nil || [self.replyModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replyModel.summary;
        }
    }else{
        if (self.replyModel.content == nil || [self.replyModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replyModel.content;
        }
    }
    
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 - 跳转到动态详情页面
    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
    pager.model = self.viewModel.modelsArr[indexPath.row];
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_CommonPsersonDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
    cell.model = self.viewModel.modelsArr[indexPath.row];
    // 点击Cell的下拉按钮
    // 添加对于Cell上点击事件的处理
    [cell callWhileCellBtnClick:^(NSInteger SenderTag , ICN_DynStateContentModel *model) {
        switch (SenderTag) {
            case ICN_CellLikeActionBtnType:{
                model.likeUp = !model.likeUp;
                NSInteger likeType = 0;
                if (model.isLikeUp) {
                    likeType = 1;
                }else{
                    likeType = 2;
                }
                [self.viewModel likeUpWithType:likeType Model:model];
                break;
            }
            case ICN_CellCommentBtnType:{
                //相关逻辑 - 跳转到动态详情页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = self.viewModel.modelsArr[indexPath.row];
                [self currentPagerJumpToPager:pager];
                break;
            }
            case ICN_CellReviewBtnType:{
                // 转发
                self.replyModel = model;
                [self.view addSubview:self.warnView];
                break;
            }
            case ICN_CellPullBtnType:{
                [self.tableView reloadData];
                break;
            }
            default:
                break;
        }
    }];
    [cell setListStyle:YES];
    return cell;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DynStateContentModel *model = self.viewModel.modelsArr[indexPath.row];
    
    if (model.isTransmit.integerValue == 1) {
        // 是转发的Cell
        if (model.contentSpreadHeight > 57.5) {
            return 230 + 100;
        }else{
            return 330 - model.contentSpreadHeight;
        }
    }
    
    if (model.contentSpread == NO) {
        if (model.imageFooterHeight > 0) {
            return 210 + model.imageFooterHeight;
        }
        if (model.contentSpreadHeight > 57.5) {
            return 210;
        }
    }
    
    if (model.imageFooterHeight > 0) {
        return 210 + model.imageFooterHeight + model.contentSpreadHeight - 57.5;
    }else{
        return 210 + model.contentSpreadHeight - 57.5;
    }
    
    return 210;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.navigationItem.title isEqualToString:@"我的智讯"]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (editingStyle ==UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
    return UITableViewCellEditingStyleDelete;
}

//如果想在侧拉的时候显示是中文的删除，只需要用下面的方法替换掉上面的方法就好了
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath

{
    UITableViewRowAction *rowAction;
    
    rowAction = [UITableViewRowAction
                 rowActionWithStyle:UITableViewRowActionStyleNormal
                 title:@"删除"
                 handler:^(UITableViewRowAction * _Nonnull action,
                           NSIndexPath * _Nonnull indexPath)
                 {
                     
                     ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] init];
                     model = _viewModel.modelsArr[indexPath.row];
                     
                     NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                     dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                     dic[@"matterId"] = model.DynID;
                     [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_delTopic params:dic success:^(id result) {
                         
                         [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
                         [self.tableView.mj_header beginRefreshing];
                     } failure:^(NSDictionary *errorInfo) {
                         NSLog(@"请求数据失败");
                     }];
                 }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}


@end
