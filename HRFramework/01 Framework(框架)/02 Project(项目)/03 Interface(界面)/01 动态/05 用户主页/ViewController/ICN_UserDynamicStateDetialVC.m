//
//  ICN_UserDynamicStateDetialVC.m
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#pragma mark --- 静态常量 ---
static CGFloat const ImageFooterHeight = 60.0;        // 当前带有图片的尾视图的基准高度(去掉图片背景图高度后的)
static NSString * const PRIVATE_DefaultHolder = @"这一刻的想法...";

#import "ICN_UserDynamicStateDetialVC.h"
#import "ICN_DynStateContentModel.h"
#import "ICN_DynamicStateCommentViewModel.h"    // ViewModel头文件
#import "ICN_UserHomePagerVC.h"                 // 用户主页
#import "ICN_UserReportDetialVC.h"              // 举报页面
#import "ICN_PicBroswerView.h"                  // 图片浏览头文件
#import "ICN_ReviewPublication.h"               //转发详情页面
#import "ICN_CompanyDetialVC.h"                 //公司详情

#import "ICN_ShareManager.h"                    // 分享管理器
#import "ICN_ApplyModel.h"                      // 分享用Model
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model

#pragma mark - ---------- Cell相关头文件 ----------

#import "ICN_DynStateTextSpreadCell.h"      // 动态正文Cell头文件
#import "ICN_NewReplyToStateCell.h"         // 活动等内容转发的Cell
#import "ICN_DynStateSectionHeaderView.h"   // 动态正文sectionheader
#import "ICN_DynStateSectionImagesFooter.h" // 带有图片的动态正文section footer
#import "ICN_DynStateSectionNormalFooter.h" // 不带图片的动态正文section footer
#import "ICN_ComSingleTitleSectionView.h"   // 评论内容的section header
#import "ICN_UserReviewCell.h"              // 评论Cell
#import "ICN_DynWarnView.h"                 // 评论弹出视图

#pragma mark - ---------- 相关头文件 ----------

#import "ICN_ComplainDetialPager.h" // 吐槽详情页面
#import "ICN_MyQuestionDetialPager.h" // 我得提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 别人的提问详情页面
#import "ICN_PositionNextOneViewController.h" // 职位详情页面
#import "ICN_IndustryNextViewController.h" // 行业资讯详情 url传id (职场规划也是这个详情)
#import "ICN_LiveDetialViewController.h" // 直播详情  liveID; /*直播id **/
#import "ICN_ActivityDetialViewController.h" // 活动详情  packID
#import "ICN_MyfridentViewController.h" // 好友页面（分享到好友）controller



@interface ICN_UserDynamicStateDetialVC ()<UITableViewDelegate , UITableViewDataSource , DynamicStateCommentViewModelDelegate , UITextFieldDelegate , ICN_DynWarnViewDelegate>

#pragma mark - ---------- IBProperty ----------
@property (weak, nonatomic) IBOutlet UITableView *tableView; // 评论详情的tableView
@property (weak, nonatomic) IBOutlet UITextField *p_ReviewContentTextF; // 评论内容的文本框


#pragma mark - ---------- 内容属性 ----------
@property (nonatomic , strong)ICN_DynamicStateCommentViewModel *viewModel;
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 回复Model
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , retain)NSArray *p_TestModelsArr;        // 测试用模型数组
@property (nonatomic , copy)NSString *replyerId;               // 回复@的用户ID
@property (nonatomic , strong)ICN_PicBroswerView *pictureView; // 图片浏览器
@property (nonatomic , strong)ICN_DynWarnView *replayView;     // 评论弹出视图
@property (weak, nonatomic) IBOutlet UIView *commentTextBackGround; // 评论文本框背景图
@property (nonatomic , copy)NSString *currentHolder; // 如果有currentHolder证明需要修改，没有使用默认的搜索holder


@end

@implementation ICN_UserDynamicStateDetialVC

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = self.view.bounds;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}


#pragma mark - ---------- 懒加载 ----------

- (ICN_PicBroswerView *)pictureView{
    if (_pictureView == nil) {
        _pictureView = [[ICN_PicBroswerView alloc] initWithFrame:SCREEN_BOUNDS];
        _pictureView.hidden = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_pictureView];
    }
    return _pictureView;
}


- (NSArray *)p_TestModelsArr{
    if (_p_TestModelsArr == nil) {
        _p_TestModelsArr = @[
                             @[@1,@"ICN_DynStateSectionHeaderView" ,
                               @"ICN_DynStateTextSpreadCell" ,
                               @"ICN_DynStateSectionImagesFooter" ],
                             @[@3,@"ICN_ComSingleTitleSectionView" ,@"ICN_UserReviewCell"]
                             ];
    }
    return _p_TestModelsArr;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"动态详情";
    self.viewModel = [[ICN_DynamicStateCommentViewModel alloc] init];
    if (self.model) {
        self.viewModel.Wisdom = self.model.isWidsom;
        if (self.viewModel.Wisdom) {
            self.naviTitle = @"智讯详情";
        }
    }else{
        if (self.dynamicId) {
            // 动态
            self.naviTitle = @"动态详情";
            self.viewModel.Wisdom = NO;
        }else{
            self.naviTitle = @"智讯详情";
            self.viewModel.Wisdom = YES;
        }
    }
    self.viewModel.delegate = self;
    self.p_ReviewContentTextF.delegate = self;
    // 配置当前页面的tableView
    [self configCurrentPageContentTableView];
    //[self configTableViewRefreshHeaderFooterView];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 如果是企业登录则隐藏评论区域
    if ([self isCurrentEnterpriseUser]) {
        self.commentTextBackGround.hidden = YES;
    }
    [self configTableViewRefreshHeaderFooterView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                // 动态和智讯类型还是之前的跳转操作
                ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
                pager.model = self.replayModel;
                [self currentPagerJumpToPager:pager];
                break;
            }
            default:
                break;
        }
    }
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            if (self.viewModel.detialModel == nil) {
                if (self.model) {
                    [self.viewModel loadDynamicStateDetialWithMatterId:self.model.matterId];
                }else{
                    NSString *contentId = self.dynamicId != nil ? self.dynamicId : SF(@"%@",self.wisdomId);
                    [self.viewModel loadDynamicStateDetialWithMatterId:contentId];
                }
            }
            if (self.model) {
                [self.viewModel refreshCurrentPageContentCellsWithMatterId:self.model.matterId];
            }else{
                NSString *contentId = self.dynamicId != nil ? self.dynamicId : SF(@"%@",self.wisdomId);
                [self.viewModel refreshCurrentPageContentCellsWithMatterId:contentId];
            }
            
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    // 设置footer的标识 MJRefreshAutoNormalFooter
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (self.model) {
            [self.viewModel loadNextPageContentCellsWithMatterId:self.model.matterId];
        }else{
            NSString *contentId = self.dynamicId != nil ? self.dynamicId : SF(@"%@",self.wisdomId);
            [self.viewModel loadNextPageContentCellsWithMatterId:contentId];
        }
    }];
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    // 设置延迟0.5秒之后结束刷新
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [tableView.mj_header endRefreshing];
        [tableView.mj_footer endRefreshing];
    });
}


- (void)configCurrentPageContentTableView{
    
    // 签订协议
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // 注册Cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateTextSpreadCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_DynStateTextSpreadCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NewReplyToStateCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserReviewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserReviewCell class])];
    // 注册section
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionHeaderView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionHeaderView class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionImagesFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionImagesFooter class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionNormalFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionNormalFooter class])];
    // 设置group类型的相关属性
    self.tableView.backgroundView = nil;
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.01)];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0.01)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

// 举报跳转方法
- (void)jumpToReportDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    NSString *errorMsg ;
    if (model.canReport) {
        if ([model.canReport integerValue] == 1) {
            errorMsg = @"无法举报自己";
        }
        if ([model.canReport integerValue] == 2) {
            errorMsg = @"已经举报了";
        }
    }
    if (errorMsg) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:errorMsg];
    }else{
        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
        pager.matterId = model.matterId;
        [self currentPagerJumpToPager:pager];
    }
}

#pragma mark --- 获取约定Cell的方法 ---

/** 获取新的动态转发类型的Cell */
- (UITableViewCell *)requestWithNewReplyCellWith:(UITableView *)tableView Model:(ICN_DynStateContentModel *)model{
    // 存在则证明是新转发类型的Model
    ICN_NewReplyToStateCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
    cell.model = model;
    [cell callBackNewIconTapBlock:^(ICN_DynStateContentModel *model) {
        if ([model.roleId integerValue] == 1) {
            // 跳转到用户主页
            ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
            pager.memberId = model.memberId;
            [self currentPagerJumpToPager:pager];
        }else{
            ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
            pager.companyId = model.memberId;
            [self currentPagerJumpToPager:pager];
        }
    }];
    [cell callBackWithNewDynReplyCellBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag, BOOL isLikeUp) {
        // 根据tag判断点击的按钮
        switch (btnTag) {
            case ICN_CellLikeActionBtnType:{
                // 点赞操作 1：点赞，2：取消点赞
                if (model.isPraise.integerValue == 0) {
                    // 进行点赞操作
                    [self.viewModel likeUpWithType:1 Model:model];
                }else{
                    // 进行取消点赞操作
                    [self.viewModel likeUpWithType:2 Model:model];
                }
                break ;
            }
            case ICN_CellReportBtnType:{
                // 举报操作
                [self jumpToReportDetialPagerWithModel:model];
                break ;
            }
            case ICN_CellCommentBtnType:{
                // 评论操作
                // 执行回复评论的相关操作
                if (![self isCurrentEnterpriseUser]) {
                    self.replyerId = nil;
                    [self.p_ReviewContentTextF becomeFirstResponder];
                }
                break ;
            }
            case ICN_CellReplayType:{
                // 转发操作
                // 1. 获取转发需要的两种Model
                self.replayModel = model;
                self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                // 2. 对于智讯的title进行处理
                if (model.title == nil) {
                    self.transmitModel.title = @"i行动态";
                }
                // 3. 展开转发页面
                [self.view addSubview:self.replayView];
                break;
                break ;
            }
            default:
                break;
        }
    }];
    return  cell;
}

#pragma mark - ---------- IBAction ----------

// 添加评论内容的action
- (IBAction)sendReviewContentAction:(UIButton *)sender {
    if (self.p_ReviewContentTextF.text != nil && ![self.p_ReviewContentTextF.text isEqualToString:@""]) {
        [self.viewModel postDynamicStateCommentContentWithMatterId:self.model.matterId Replyer:self.replyerId Content:self.p_ReviewContentTextF.text];
    }
    self.replyerId = nil;
    self.p_ReviewContentTextF.text = @"";
    [self.viewModel refreshCurrentPageContentCellsWithMatterId:self.model.matterId];
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_DynWarnViewDelegate ---

/** 配置分享给联系人的拓展字典 */
- (NSDictionary *)configSharePramsDicWithType:(NSInteger)shareType Pic:(NSString *)contentPic Content:(NSString *)content EXT:(NSString *)contentEXT ShareId:(NSString *)shareId{
    
    NSMutableDictionary *mutableParams = [@{
                                            @"shareType" : SF(@"%d",shareType),
                                            @"content" : SF(@"%@",content),
                                            @"shareId" : SF(@"%@",shareId),
                                            } mutableCopy];
    if (contentPic) {
        [mutableParams setValue:contentPic forKey:@"contentPic"];
    }
    if (contentEXT) {
        [mutableParams setValue:contentEXT forKey:@"contentEXT"];
    }
    return [NSDictionary dictionaryWithDictionary:mutableParams];
}


// 实现分享到i行好友的方法
- (void)shareToMyFriendPager{
    // 分享到 I行好友
    ICN_MyfridentViewController *myfrident=[[ICN_MyfridentViewController alloc]init];
    myfrident.model=self.replayModel;
    // 第一步判断类型
    switch (self.transmitModel.paramsType) {
        case REPLAY_DynamicReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:2 Pic:picUrl Content:self.replayModel.content EXT:nil ShareId:self.replayModel.matterId];
            break ;
        }
        case REPLAY_WisdomReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:3 Pic:picUrl Content:self.replayModel.content EXT:self.replayModel.title ShareId:self.replayModel.DynID];
            break ;
        }
        case REPLAY_ComplainReplay:{
            myfrident.ext = [self configSharePramsDicWithType:5 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        case REPLAY_QuestionReplay:{
            myfrident.ext = [self configSharePramsDicWithType:4 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        default:
            break;
    }
    self.replayModel = nil;
    [self currentPagerJumpToPager:myfrident];
}


- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.replayView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            // 判断是自己的动态不转发
            if (self.replayModel != nil && self.replayModel.isMe.integerValue == 1) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无法转发自己的动态"];
                return ;
            }
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到i行好友功能
            [self shareToMyFriendPager];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.replayView removeFromSuperview];
}

- (void)cofigShareContentWithTitle:(NSString *)title IconUrl:(NSString *)iconUrl Content:(NSString *)content{
    if (self.replayModel.title == nil || [self.replayModel.title isEqualToString:@""]) {
        title = self.replayModel.memberNick;
    }else{
        title = self.replayModel.memberNick;
    }
    if (self.replayModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replayModel.memberLogo);
    }
    if (self.replayModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replayModel.summary == nil || [self.replayModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.summary;
        }
    }else{
        if (self.replayModel.content == nil || [self.replayModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.content;
        }
    }
    
}



#pragma mark --- DynamicStateCommentViewModelDelegate ---

// 获取到用户动态详情时的回调
- (void)responseWithUserDynamicDetial{
    
    // 根据获取到的DetialModel生成新的Model并刷新数据
    if (self.viewModel.detialModel) {
        self.model = [[ICN_DynStateContentModel alloc] init];
        if (self.viewModel.detialModel.title) {
            self.model.widsom = YES;
        }
        self.model.imageFooterHeight = self.viewModel.detialModel.imageFooterHeight;
        self.model.canReport = self.viewModel.detialModel.canReport;
        self.model.contentSpread = self.viewModel.detialModel.contentSpread;
        self.model.contentSpreadHeight = self.viewModel.detialModel.contentSpreadHeight;
        self.model.DynID = self.viewModel.detialModel.DYNId;
        self.model.matterId = self.model.DynID;
        self.model.isPraise = self.viewModel.detialModel.isPraise;
        self.model.title = self.viewModel.detialModel.title;
        self.model.memberId = self.viewModel.detialModel.memberId;
        self.model.content = self.viewModel.detialModel.content;
        self.model.pic = self.viewModel.detialModel.pic;
        self.model.praiseCount = self.viewModel.detialModel.praiseCount;
        self.model.transmitCount = self.viewModel.detialModel.transmitCount;
        self.model.createDate = self.viewModel.detialModel.createDate;
        self.model.type = self.viewModel.detialModel.type;
        self.model.roleId = self.viewModel.detialModel.roleId;
        self.model.memberNick = self.viewModel.detialModel.memberNick;
        self.model.memberLogo = self.viewModel.detialModel.memberLogo;
        self.model.workStatus = self.viewModel.detialModel.workStatus;
        self.model.companyName = self.viewModel.detialModel.companyName;
        self.model.memberPosition = self.viewModel.detialModel.memberPosition;
        self.model.memberSchool = self.viewModel.detialModel.memberSchool;
        self.model.memberMajor = self.viewModel.detialModel.memberMajor;
        self.model.memberProfession = self.viewModel.detialModel.memberProfession;
        self.model.summary = self.viewModel.detialModel.summary;
        self.model.plusv = self.viewModel.detialModel.plusv;
        // 设置默认的类型是不转发
        self.model.isTransmit = @"0";
        // 设置fromtype一系
        self.model.fromId = self.viewModel.detialModel.fromId;
        self.model.fromType = self.viewModel.detialModel.fromType;
        self.model.RandomPicture = self.viewModel.detialModel.RandomPicture;
        self.model.RandomName = self.viewModel.detialModel.RandomName;
    }
    
    [self.tableView reloadData];
}

- (void)responseWhileCommentCommitSuccess:(BOOL)success error:(NSString *)error{
    if (success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"评论提交成功"];
        if ([self.model.commentCount integerValue]) {
            self.model.commentCount = SF(@"%ld",self.model.commentCount.integerValue + 1);
        }
        [self.p_ReviewContentTextF resignFirstResponder];
        [self.tableView.mj_header beginRefreshing];
    }else{
        // 在发送失败的时候将一切内容置空
        [self.p_ReviewContentTextF resignFirstResponder];
        self.p_ReviewContentTextF.text = @"";
        self.replyerId = nil;
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseWithStateCommentModelsArrSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
        [self endRefreshWithTableView:self.tableView];
    }else{
        [self endRefreshWithTableView:self.tableView];
    }
}

- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
    }
}

- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经点过赞了"];
    [self.tableView reloadData];
}

- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您没有权限进行此操作"];
}


#pragma mark --- UITextFieldDelegate ---

- (void)textFieldDidEndEditing:(UITextField *)textField{
    // 在结束编辑的时候将一切评论相关内容置空
    self.replyerId = nil;
    // 重置holder
    self.p_ReviewContentTextF.placeholder = PRIVATE_DefaultHolder;
    self.currentHolder = nil;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    // 判断没有需要@的人的时候显示默认holder
    self.p_ReviewContentTextF.placeholder = self.currentHolder ? self.currentHolder : PRIVATE_DefaultHolder;
    return YES;
}




/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    if (indexPath.section == 0) {
        // 点击的是Cell详情 判断是不是新动态
        if ([self.viewModel.detialModel.fromId integerValue] != 0) {
            // 是新动态
            switch ([self.viewModel.detialModel.fromType integerValue]) {
                case 1:{
                    // 吐槽
                    ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
                    pager.complainId = self.viewModel.detialModel.fromId;
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case 2:{
                    // 提问
                    if ([self.viewModel.detialModel.myQuestion integerValue] == 1) {
                        // 是自己的提问
                        ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                        pager.constentId = self.viewModel.detialModel.fromId;
                        [self currentPagerJumpToPager:pager];
                    }else{
                        // 不是自己的提问
                        ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                        pager.contentId = self.viewModel.detialModel.fromId;
                        [self currentPagerJumpToPager:pager];
                    }
                    break;
                }
                case 3:{
                    // 职位
                    ICN_PositionNextOneViewController *pager = [[ICN_PositionNextOneViewController alloc] init];
                    pager.url = self.viewModel.detialModel.fromId;
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case 4:{
                    // 行业资讯
                    ICN_IndustryNextViewController *pager = [[ICN_IndustryNextViewController alloc] init];
                    pager.url = self.viewModel.detialModel.fromId;
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case 5:{
                    // 活动
                    ICN_ActivityDetialViewController *pager = [[ICN_ActivityDetialViewController alloc] init];
                    pager.packID = self.viewModel.detialModel.fromId;
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case 6:{
                    // 直播
                    ICN_LiveDetialViewController *pager = [[ICN_LiveDetialViewController alloc] init];
                    if ([self.viewModel.detialModel.fromId containsString:@","]) {
                        // 否则不合法
                        NSArray *array = [self.viewModel.detialModel.fromId componentsSeparatedByString:@","];
                        pager.liveID = array[0];
                        pager.actID = array[1];
                        [self currentPagerJumpToPager:pager];
                    }else{
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"直播id不合法"];
                    }
                    break;
                }
                case 7:{
                    // 职场规划
                    ICN_IndustryNextViewController *pager = [[ICN_IndustryNextViewController alloc] init];
                    pager.url = self.viewModel.detialModel.fromId;
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                default:
                    break;
            }
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        // 判断没有Model的时候返回0
        if (_model == nil) {
            return 0;
        }
        // 返回Cell的数量
        return 1;
    }else{
        // 返回评论Cell的数量
        return self.viewModel.modelsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        // 根据Cell的内容确定使用的Cell - 正文或者是转发
        // 判断1. Model存在的时候按照有Model的方式处理
        // 判断2. Model不存在的时候不生成内容
        if (_model == nil) {
            return nil;
        }else{
            // 新动态的转发样式
            if (_model.fromType != nil && ![_model.fromType isEqualToString:@"0"]) {
                // 是新的转发类型
                return [self requestWithNewReplyCellWith:tableView Model:_model];
            }
            // 默认的显示样式
            ICN_DynStateTextSpreadCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynStateTextSpreadCell class])];
            cell.model = self.model;
            cell.contentDetial = YES;
            return cell;
        }
        
    }else{
        // 评论的Cell
        ICN_UserReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserReviewCell class])];
        if (indexPath.row == 0) {
            cell.firstCell = NO;
        }else{
            cell.firstCell = NO;
        }
        cell.model = self.viewModel.modelsArr[indexPath.row];
        [cell callBackWithBtnBlock:^(ICN_DynamicStateCommentModel *model, BOOL tap) {
            if (tap) {
                // 点击用户头像的操作
                ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
                // 点击评论头像跳转到评论Model对应的memberid
                pager.memberId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }else{
                // 根据Model的数据去评论
                if (![self isCurrentEnterpriseUser]) {
                    self.replyerId = model.memberId;
                    // 清空之前显示的输出数据
                    self.p_ReviewContentTextF.text = nil;
                    // 设置holder的内容
                    NSString *name = _model.memberNick ? _model.memberNick : @"匿名";
                    // 设置需要显示的holder
                    self.currentHolder = SF(@"@%@",name);
                    // 使键盘弹出
                    [self.p_ReviewContentTextF becomeFirstResponder];
                }
            }
        }];
        return cell;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            
            // 判断没有Model的时候返回nil
            if (_model == nil) {
                return nil;
            }
            // 获取到新的转发Model没有section
            if (_model.fromType != nil && ![_model.fromType isEqualToString:@"0"]) {
                return nil;
            }
            
            // 正文内容
            ICN_DynStateSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionHeaderView class])];
            if (section == 0) {
                view.firstSection = YES;
            }
            view.model = self.model;
            if (self.model.isTransmit.integerValue == 0) {
                view.transmit = NO;
            }else{
                view.transmit = YES;
                if (self.viewModel.detialModel) {
                    view.transmit = NO;
                    self.viewModel.detialModel.imageFooterHeight = self.model.imageFooterHeight;
                    self.viewModel.detialModel.contentSpread = self.model.contentSpread;
                    self.viewModel.detialModel.contentSpreadHeight = self.model.contentSpreadHeight;
                    self.viewModel.detialModel.widsom = self.model.widsom;
                    self.viewModel.detialModel.likeUp = self.model.likeUp;
                    view.detialModel = self.viewModel.detialModel;
                }
            }
            return view;
        }
    }else{
        ICN_ComSingleTitleSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        view.contentView.backgroundColor = RGB0X(0xf0f0f0);
        view.headerTitle = @"评论";
        return view;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            
            // 判断没有Model的时候返回nil
            if (_model == nil) {
                return nil;
            }
            
            // 获取到新的转发Model没有section
            if (_model.fromType != nil && ![_model.fromType isEqualToString:@"0"]) {
                return nil;
            }
            
            // 当内容为动态的时候有尾视图
            ICN_DynStateContentModel *model = self.model;
            if ([model.pic containsString:@"."]) {
                // 有图片的时候调用有图片的pic;
                ICN_DynStateSectionImagesFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionImagesFooter class])];
                footer.model = model;
                [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            // 根据tag判断点击的按钮
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isPraise.integerValue == 0) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 转发相关的操作
                            // 1. 获取转发需要的两种Model
                            self.replayModel = model;
                            self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                            // 2. 对于智讯的title进行处理
                            if (model.title == nil) {
                                self.transmitModel.title = @"i行动态";
                            }
                            // 3. 展开转发页面
                            [self.view addSubview:self.replayView];
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            if (![self isCurrentEnterpriseUser]) {
                                self.replyerId = nil;
                                [self.p_ReviewContentTextF becomeFirstResponder];
                            }
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            [self jumpToReportDetialPagerWithModel:model];
                            break;
                        }
                        default:
                            // 返回的是image的索引值
                            if (self.pictureView.hidden == YES) {
                                NSArray *pathArr = [model.pic componentsSeparatedByString:@","];
                                self.pictureView.urlPathArr = pathArr;
                                self.pictureView.currentIndex = btnTag;
                                self.pictureView.hidden = NO;
                            }
                            break;
                    }
                }];
                return footer;
            }else{
                ICN_DynStateSectionNormalFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionNormalFooter class])];
                footer.model = model;
                [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            // 根据tag判断点击的按钮
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isPraise.integerValue == 0) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 执行转发相关操作
                            // 1. 获取转发需要的两种Model
                            self.replayModel = model;
                            self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                            // 2. 对于智讯的title进行处理
                            if (model.title == nil) {
                                self.transmitModel.title = @"i行动态";
                            }
                            // 3. 展开转发页面
                            [self.view addSubview:self.replayView];
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            if (![self isCurrentEnterpriseUser]) {
                                self.replyerId = nil;
                                [self.p_ReviewContentTextF becomeFirstResponder];
                            }
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            [self jumpToReportDetialPagerWithModel:model];
                            break;
                        }
                        default:
                            
                            break;
                    }
                }];
                
                return footer;
            }
        }
    }
    
    return nil;
    
}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        
        // 判断没有Model的时候返回0.0
        if (_model == nil) {
            return 0.0;
        }
        
        // 返回 可能存在的动态详情的头视图高度
        // 获取到新的转发Model没有section
        if (_model.fromType != nil && ![_model.fromType isEqualToString:@"0"]) {
            return 0.0;
        }
        
        return ICN_DynStateSectionHeaderViewHeight;
    }
    return 35.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    
    // 判断没有Model的时候返回0.0
    if (_model == nil) {
        return 0.0;
    }
    // 获取到新的转发Model没有section
    if (_model.fromType != nil && ![_model.fromType isEqualToString:@"0"]) {
        return 0.0;
    }
    if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
        ICN_DynStateContentModel *model = self.model;
        if (model.imageFooterHeight != 0.0) {
            return ImageFooterHeight + model.imageFooterHeight;
        }else
            return ICN_DynStateSectionNormalFooterHeight;
    }
    
    return 0.0;
}





@end
