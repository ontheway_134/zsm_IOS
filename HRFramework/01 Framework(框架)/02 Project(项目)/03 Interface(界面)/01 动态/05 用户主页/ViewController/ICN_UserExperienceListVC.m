//
//  ICN_UserExperienceListVC.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserExperienceListVC.h"
#import "ICN_DynExperienceCell.h"

@interface ICN_UserExperienceListVC ()<UITableViewDelegate , UITableViewDataSource>

@property (nonatomic , retain)UITableView *tableView;


@end

@implementation ICN_UserExperienceListVC

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynExperienceCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"他的经历";
    
    [self tableView];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_DynExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
    [cell setListStyle:YES];
    return cell;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 106.0;
}


@end
