//
//  ICN_UserDynamicStateListVC.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_UserDynamicStateListVC : BaseViewController

@property (nonatomic , copy)NSString *memberId; // 用户的ID
@property (nonatomic , assign , getter=isMyDynamic)BOOL mineDynamic; // 是否是自己的动态
@property (nonatomic , assign , getter=isMyWisdom)BOOL mineWisdom; // 是否是我的智讯页面

@end
