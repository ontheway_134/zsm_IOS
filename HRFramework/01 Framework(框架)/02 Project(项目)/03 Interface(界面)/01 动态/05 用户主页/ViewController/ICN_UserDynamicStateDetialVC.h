//
//  ICN_UserDynamicStateDetialVC.h
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_DynStateContentModel;
@interface ICN_UserDynamicStateDetialVC : BaseViewController

@property (nonatomic , strong)ICN_DynStateContentModel *model;
@property (nonatomic , copy)NSString *dynamicId; // 传入的动态id
@property (nonatomic , copy)NSString *wisdomId;  // 传入的智讯id

@property (nonatomic,strong) NSString * canReport;


/** 是否是转发属性 */
@property (nonatomic , assign , getter=isTransmit)BOOL transmit;


@end
