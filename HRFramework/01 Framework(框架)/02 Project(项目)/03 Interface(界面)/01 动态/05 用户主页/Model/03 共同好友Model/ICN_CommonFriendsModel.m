//
//  ICN_CommonFriendsModel.m
//  ICan
//
//  Created by albert on 2016/12/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonFriendsModel.h"

@implementation ICN_CommonFriendsModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"CommonFriendId" : @"id"} ];
}

@end
