//
//  ICN_DynGroupModel.h
//  ICan
//
//  Created by albert on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynGroupModel : BaseOptionalModel

@property (nonatomic , copy)NSString *groupLogo; // 小组头像
@property (nonatomic , copy)NSString *groupName; // 小组名称
@property (nonatomic , copy)NSString *isopen; // 是否公开
@property (nonatomic , copy)NSString *number; // 小组上限人数
@property (nonatomic , copy)NSString *summary; // 小组描述
@property (nonatomic , copy)NSString *groupId; // 小组id
@property (nonatomic , copy)NSString *memberId; // 创建者
@property (nonatomic , copy)NSString *status; // 小组状态(0:正常  1:解散)
@property (nonatomic , copy)NSString *groupAssessment; // 小组热度
@property (nonatomic , copy)NSString *memberCount; // 小组当前人数


@end
