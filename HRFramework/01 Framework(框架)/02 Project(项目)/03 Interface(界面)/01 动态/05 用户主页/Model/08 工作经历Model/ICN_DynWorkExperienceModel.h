//
//  ICN_DynWorkExperienceModel.h
//  ICan
//
//  Created by albert on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynWorkExperienceModel : BaseOptionalModel

#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)CGFloat CellHeight;



@property (nonatomic , copy)NSString *workExperienceId; // 用户工作经历记录ID
@property (nonatomic , copy)NSString *entryDate; // 入职时间
@property (nonatomic , copy)NSString *outDate; // 离职时间
@property (nonatomic , copy)NSString *companyName; // 公司名称
@property (nonatomic , copy)NSString *position; // 职位
@property (nonatomic , copy)NSString *summary; // 工作经历介绍

@end
