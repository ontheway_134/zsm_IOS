//
//  ICN_DynUserBaseMSGModel.h
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynUserBaseMSGModel : BaseOptionalModel

@property (nonatomic , copy)NSString *level; // 好友纬度数量
@property (nonatomic , copy)NSString *friendNum; // 好友总数
@property (nonatomic , copy)NSString *isMe; //是不是自己的账号
@property (nonatomic , copy)NSString *memberLogo; // 用户logo
@property (nonatomic , copy)NSString *memberNick; // 用户昵称
@property (nonatomic , copy)NSString *memberGender; // 性别(1男，2女)
@property (nonatomic , copy)NSString *companyName; // 公司名称
@property (nonatomic , copy)NSString *memberPosition; // 职位
@property (nonatomic , copy)NSString *memberProfession; // 行业
@property (nonatomic , copy)NSString *memberSchool; // 学校
@property (nonatomic , copy)NSString *memberMajor; // 专业
@property (nonatomic , copy)NSString *personalizeSignature; // 个性签名
@property (nonatomic , copy)NSString *plusv; // 是否认证用户(加V验证0未验证1已验证)
@property (nonatomic , copy)NSString *staus; // 好友关系类型[没关系则为null]
@property (nonatomic , copy)NSString *delStatus; // 好友的删除关系 [1. 未删除好友 2. 已删除好友 null 没有好友关系]
@property (nonatomic , copy)NSString *isBlack; // 是否为黑名单关系 [0. 没有黑名单关系 1. 对方在我的黑名单列表中 2. 我在对方的黑名单列表中]
@property (nonatomic , copy)NSString *isUnlook; // 被查看用户是否在当前用户 谁不可看我的动态 名单中  0:否,  1:是
@property (nonatomic , copy)NSString *isPass; // 被查看用户是否在当前用户 不看ta的动态状态 名单中  0:否,  1:是
@property (nonatomic , copy)NSString *canReport; // 0 可以举报 1无法举报自己 2已经举报过



@end
