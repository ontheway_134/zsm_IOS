//
//  ICN_DynDetialModel.h
//  ICan
//
//  Created by albert on 2017/1/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynDetialModel : BaseOptionalModel

#pragma mark - ---------- 其他内容转发到动态相关属性 ----------
@property (nonatomic , copy)NSString *fromType; // 来源类型（吐槽；提问；职位；职业规划；行业资讯；活动；直播）
@property (nonatomic , copy)NSString *fromId; // 来源对应id
@property (nonatomic , copy)NSString *RandomPicture; // 来源对应图片
@property (nonatomic , copy)NSString *RandomName; // 来源对应标题
@property (nonatomic , copy)NSString *myQuestion; // 是否为自己的提问  1:是  0:否


#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)CGFloat imageFooterHeight; // 带图片的尾视图高度
@property (nonatomic , assign , getter=isSpread)BOOL contentSpread; // 文本是否延展
@property (nonatomic , assign)CGFloat contentSpreadHeight; // 文本展开的高度
@property (nonatomic , assign , getter=isLikeUp)BOOL likeUp; // 是否被点赞属性
@property (nonatomic , assign , getter=isWidsom)BOOL widsom; // 是否是智讯

@property (nonatomic , copy)NSString *isPraise; // 未点赞 0 已点赞 1
@property (nonatomic , copy)NSString *DYNId; // 动态id
@property (nonatomic , copy)NSString *title;
@property (nonatomic , copy)NSString *memberId;
@property (nonatomic , copy)NSString *content;
@property (nonatomic , copy)NSString *pic;
@property (nonatomic , copy)NSString *praiseCount;
@property (nonatomic , copy)NSString *transmitCount;
@property (nonatomic , copy)NSString *isOpened;
@property (nonatomic , copy)NSString *isRecommit;
@property (nonatomic , copy)NSString *status;
@property (nonatomic , copy)NSString *createDate;
@property (nonatomic , copy)NSString *type;
@property (nonatomic , copy)NSString *roleId;
@property (nonatomic , copy)NSString *memberNick;
@property (nonatomic , copy)NSString *memberLogo;
@property (nonatomic , copy)NSString *workStatus;
@property (nonatomic , copy)NSString *companyName;
@property (nonatomic , copy)NSString *memberPosition;
@property (nonatomic , copy)NSString *memberSchool;
@property (nonatomic , copy)NSString *memberMajor;
@property (nonatomic , copy)NSString *memberProfession;
@property (nonatomic , copy)NSString *plusv;
@property (nonatomic , copy)NSString *summary;
@property (copy,nonatomic)  NSString  *canReport;

@end
