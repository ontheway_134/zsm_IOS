//
//  ICN_SkillTagModel.h
//  ICan
//
//  Created by albert on 2016/12/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_SkillTagModel : BaseOptionalModel

#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)CGFloat CellHeight; // Cell的总高度
@property (nonatomic , assign)NSInteger rowCount; // 单行能够安装的标签数量




@property (nonatomic , copy)NSString *skillSignId; // 技能标签ID
@property (nonatomic , copy)NSString *skillLabel; // 技能标签内容


@end
