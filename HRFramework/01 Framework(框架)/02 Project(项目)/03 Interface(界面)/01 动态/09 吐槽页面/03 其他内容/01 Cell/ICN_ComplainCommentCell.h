//
//  ICN_ComplainCommentCell.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_ComplainCommentModel;

typedef void(^CellBlock)(ICN_ComplainCommentModel *model , NSInteger buttonType , BOOL isSelected);
@interface ICN_ComplainCommentCell : UITableViewCell

@property (nonatomic , assign)BOOL hiddenSegmentView; // 是否隐藏分隔栏
@property (nonatomic , strong)ICN_ComplainCommentModel *model; // 评论Model
@property (nonatomic , copy)CellBlock block;


- (void)callBackCellBlock:(CellBlock)block;

@end
