//
//  ICN_ComplainCommentCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainCommentCell.h"
#import "ICN_ComplainCommentModel.h"

@interface ICN_ComplainCommentCell ()


@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 评论内容label
@property (weak, nonatomic) IBOutlet UIButton *clikeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIView *segmentGrayView; // 分割Cell用灰条 -- 只有最后一个不显示
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;



@end

@implementation ICN_ComplainCommentCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setHiddenSegmentView:(BOOL)hiddenSegmentView{
    _hiddenSegmentView = hiddenSegmentView;
    _segmentGrayView.hidden = _hiddenSegmentView;
}

- (void)setModel:(ICN_ComplainCommentModel *)model{
    _model = model;
    self.nickNameLabel.text = _model.RandomName;
    if (_model.replyerName && ![_model.replyerName isEqualToString:@""]) {
        // 评论别人的评论内容
        NSString *nickContentStr = SF(@"回复%@：%@",_model.replyerName , [_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:nickContentStr];
        [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                                 value:RGB0X(0x009dff)
                                 range:[nickContentStr rangeOfString:SF(@"%@",_model.replyerName)]];
        self.contentLabel.attributedText = attributedString;
    }else{
        self.contentLabel.text = [_model.contant stringByReplacingEmojiCheatCodesWithUnicode];
    }
    self.timeLabel.text = _model.createTime;
    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:_model.RandomPicture] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    [self changeButton:self.clikeUpBtn WithSelected:NO Number:[_model.ThumbsupNum integerValue]];
    if (_model.ismakeComplaintsSeltucao) {
        if ([_model.ismakeComplaintsSeltucao integerValue] == 0) {
            [self changeButton:self.clikeUpBtn WithSelected:YES Number:[_model.ThumbsupNum integerValue]];
        }
    }
}

// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%ld",number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%ld",number) forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)callBackCellBlock:(CellBlock)block{
    self.block = block;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.userLogoIcon.layer.masksToBounds = YES;
    self.userLogoIcon.layer.cornerRadius = self.userLogoIcon.width / 2.0;
}

- (IBAction)clickOnComplainCommentAction:(UIButton *)sender {
    
    if (self.block) {
        // 有block之后对其进行处理 -- 对于点赞和评论做预处理
        switch (sender.tag) {
            case ICN_CellLikeActionBtnType:{
                sender.selected = !sender.selected;
                if (sender.selected) {
                    _model.ismakeComplaintsSeltucao = @"0";
                    _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] + 1);
                }else{
                    _model.ismakeComplaintsSeltucao = @"1";
                    _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] - 1);
                }
                [self changeButton:sender WithSelected:sender.selected Number:[_model.ThumbsupNum integerValue]];
                break;
            }
            default:
                break;
        }
        
        self.block(self.model , sender.tag , sender.selected);
    }

}


@end
