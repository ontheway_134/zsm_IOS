//
//  ICN_PositionDetialModel.h
//  ICan
//
//  Created by albert on 2017/3/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_PositionDetialModel : BaseOptionalModel

@property (nonatomic , copy)NSString *positionId; // 企业发布的职位ID,
@property (nonatomic , copy)NSString *positionTitle; // 职位名称,
@property (nonatomic , copy)NSString *industryName; // 行业标签
@property (nonatomic , copy)NSString *provinceName; // 省份名
@property (nonatomic , copy)NSString *cityName; // 城市,
@property (nonatomic , copy)NSString *qualification; // 学历,
@property (nonatomic , copy)NSString *qualificationName; // 要求学历的名称
@property (nonatomic , copy)NSString *workExperience; // 需要的工作经验,
@property (nonatomic , copy)NSString *workExperienceName; // 需要的工作经验字段
@property (nonatomic , copy)NSString *workType; // 工作类型（1全职，2兼职，3实习）,
@property (nonatomic , copy)NSString *personCount; // 招聘人数,
@property (nonatomic , copy)NSString *minSalary; // 最低薪资,
@property (nonatomic , copy)NSString *maxSalary; // 最高薪资,
@property (nonatomic , copy)NSString *summary; // 职位详情



@end
