//
//  ICN_UserComplainListCell.h
//  ICan
//
//  Created by albert on 2017/3/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_ComplainListModel;

@interface ICN_UserComplainListCell : UITableViewCell

@property (nonatomic , strong)ICN_ComplainListModel *model;


@end
