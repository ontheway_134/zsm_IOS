//
//  ICN_ComplainDetialCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainDetialCell.h"
#import "ICN_ComplainDetialModel.h"

@interface ICN_ComplainDetialCell ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间label
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 正文内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评价按钮



@end

@implementation ICN_ComplainDetialCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)callBackCellBlock:(CellDetialBlock)block{
    self.block = block;
}

- (void)setModel:(ICN_ComplainDetialModel *)model{
    _model = model;
    self.timeLabel.text = _model.createTime;
    self.contentLabel.text = [_model.contant stringByReplacingEmojiCheatCodesWithUnicode];
    // 如果判断是否点赞的属性存则做是否点赞处理否则处理
    // 默认为未点赞
    [self changeButton:self.likeUpBtn WithSelected:NO Number:[_model.ThumbsupNum integerValue]];
    if (_model.ismakeComplaints) {
        if ([_model.ismakeComplaints integerValue]== 0){
            // 以点赞
            [self changeButton:self.likeUpBtn WithSelected:YES Number:[_model.ThumbsupNum integerValue]];
        }
    }
    [self changeButton:self.commentBtn WithSelected:NO Number:[_model.commentNum integerValue]];
}

// 根据是否选中和数量去修改一个按钮的值
- (void)changeButton:(UIButton *)button WithSelected:(BOOL)isSelected Number:(NSInteger)number{
    button.selected = isSelected;
    [button setTitle:SF(@"%ld",number) forState:UIControlStateNormal];
    [button setTitle:SF(@"%ld",number) forState:UIControlStateSelected];
}

- (IBAction)clickOnComplainButtonAction:(UIButton *)sender {
    if (self.block) {
        // 有block之后对其进行处理 -- 对于点赞和评论做预处理
        switch (sender.tag) {
            case ICN_CellLikeActionBtnType:{
                sender.selected = !sender.selected;
                if (sender.selected) {
                    _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] + 1);
                    _model.ismakeComplaints = @"0";
                }else{
                    _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] - 1);
                    _model.ismakeComplaints = @"1";
                }
                [self changeButton:sender WithSelected:sender.selected Number:[_model.ThumbsupNum integerValue]];
                break;
            }
            case ICN_CellCommentBtnType:{
                _model.commentNum = SF(@"%ld",[_model.commentNum integerValue] + 1);
                break;
            }
            default:
                break;
        }
        
        self.block(self.model , sender.tag , sender.selected);
    }
}

@end
