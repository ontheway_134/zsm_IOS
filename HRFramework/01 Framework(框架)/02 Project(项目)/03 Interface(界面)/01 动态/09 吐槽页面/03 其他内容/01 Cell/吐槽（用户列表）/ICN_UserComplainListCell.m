//
//  ICN_UserComplainListCell.m
//  ICan
//
//  Created by albert on 2017/3/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserComplainListCell.h"
#import "ICN_ComplainListModel.h"

@interface ICN_UserComplainListCell ()

@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 吐槽详情label
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏label


@end

@implementation ICN_UserComplainListCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ICN_ComplainListModel *)model{
    _model = model;
    self.contentLabel.text = SF(@"%@",_model.contant);
    self.timeLabel.text = SF(@"%@",_model.createTime);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
