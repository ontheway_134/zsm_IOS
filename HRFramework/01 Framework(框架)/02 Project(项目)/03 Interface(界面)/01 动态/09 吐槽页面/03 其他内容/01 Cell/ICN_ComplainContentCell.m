//
//  ICN_ComplainContentCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainContentCell.h"
#import "ICN_ComplainListModel.h"

@interface ICN_ComplainContentCell ()

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 正文内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评价按钮



@end


@implementation ICN_ComplainContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_ComplainListModel *)model{
    _model = model;
    self.nickNameLabel.text = SF(@"%@",_model.RandomName);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:SF(@"%@",_model.RandomPicture)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    self.contentLabel.text = SF(@"%@",[_model.contant stringByReplacingEmojiCheatCodesWithUnicode]);
    if (_model.ismakeComplaints != nil) {
        if ([_model.ismakeComplaints integerValue] == 0) {
            // 已点赞
            self.likeUpBtn.selected = YES;
        }else{
            self.likeUpBtn.selected = NO;
        }
        
    }
    [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateNormal];
    [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateSelected];
    [self.commentBtn setTitle:SF(@"%@",_model.commentNum) forState:UIControlStateNormal];
    [self.commentBtn setTitle:SF(@"%@",_model.commentNum) forState:UIControlStateSelected];
}

- (void)callBackWithCellBlock:(CellBlock)block{
    self.block = block;
}

- (IBAction)clickOnComplainButtonAction:(UIButton *)sender {
    // 是否选中按钮选中的是与当前按钮选中状态相反的状态 因为回调的是相反的操作（按钮是点赞状态（选中） 返回未选中状态（取消点赞））
    if (self.block) {
        if (!sender.selected) {
            _model.ismakeComplaints = @"0";
            _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] + 1);
        }else{
            _model.ismakeComplaints = @"1";
            _model.ThumbsupNum = SF(@"%ld",[_model.ThumbsupNum integerValue] - 1);
        }
        if (sender.tag == ICN_CellLikeActionBtnType) {
            [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateNormal];
            [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateSelected];
        }
        self.block(self.model , sender.tag , !sender.selected);
    }
    sender.selected = !sender.selected;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.userLogoIcon.layer.masksToBounds = YES;
    self.userLogoIcon.layer.cornerRadius = self.userLogoIcon.width / 2.0;
}



@end
