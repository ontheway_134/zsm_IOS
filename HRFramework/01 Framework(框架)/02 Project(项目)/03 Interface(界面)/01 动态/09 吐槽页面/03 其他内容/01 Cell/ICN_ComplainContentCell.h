//
//  ICN_ComplainContentCell.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_ComplainListModel;

typedef void(^CellBlock)(ICN_ComplainListModel *model , NSInteger selectedType , BOOL isSelected); // 选中按钮的回调block

@interface ICN_ComplainContentCell : UITableViewCell

@property (nonatomic , strong) ICN_ComplainListModel *model;

@property (nonatomic , copy)CellBlock block; // Cell的block


- (void)callBackWithCellBlock:(CellBlock)block;

@end
