//
//  ICN_ComplainDetialCell.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_ComplainDetialModel;

typedef void(^CellDetialBlock)(ICN_ComplainDetialModel *model , NSInteger buttonType , BOOL isSelected);
@interface ICN_ComplainDetialCell : UITableViewCell

@property (nonatomic , strong)ICN_ComplainDetialModel *model;
@property (nonatomic , copy)CellDetialBlock block;


- (void)callBackCellBlock:(CellDetialBlock)block;

@end
