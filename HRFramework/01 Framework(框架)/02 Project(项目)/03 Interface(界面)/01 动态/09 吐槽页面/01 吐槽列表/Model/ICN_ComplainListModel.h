//
//  ICN_ComplainListModel.h
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_ComplainListModel : BaseOptionalModel

@property (nonatomic , copy)NSString *complainId; // 吐槽ID,
@property (nonatomic , copy)NSString *memberId; // 发布吐槽者ID,
@property (nonatomic , copy)NSString *RandomName; // 随机昵称,
@property (nonatomic , copy)NSString *RandomPicture; // 随机头像,
@property (nonatomic , copy)NSString *contant; // 吐槽内容,
@property (nonatomic , copy)NSString *commentNum; // 评论数,
@property (nonatomic , copy)NSString *ThumbsupNum; // 点赞数,
@property (nonatomic , copy)NSString *createTime; // 发布时间,
@property (nonatomic , copy)NSString *status; // 发布时间,
@property (nonatomic , copy)NSString *ismakeComplaints; // 是否点赞 0为已经点赞 1为未点赞（状态对token  即APP用户）,

@end
