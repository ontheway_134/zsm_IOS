//
//  ICN_ComplainListViewModel.h
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_ComplainListModel;
@class ICN_ComplainDetialModel;
@class ICN_ComplainCommentModel;

typedef enum : NSUInteger {
    Complain_ResponseMineList = 133300011, // 类型是获取到用户自己吐槽列表
    Complain_ResponseOthersList, // 类型是获取到吐槽列表
    Complain_ResponseUserIDList, // 获取执行id的用户的吐槽列表
    Complain_ResponseSearchList, // 搜索吐槽列表的回调
    Complain_ResponseLikeUp, // 类型是获取用户点赞的回调
    Complain_ResponseDisLikeUp, // 类型是获取到用户取消点赞的回调
    ComPlain_ResponsePublishComment, // 发布吐槽评论的回调
    ComPlain_ResponseDetial, // 吐槽详情的回调（返回详情内容和评论）
    ComPlain_ResponseCommentLikeUp, // 吐槽评论的点赞的回调
    ComPlain_ResponseCommentDisLikeUp, // 吐槽评论的取消点赞的回调
    Complain_ResponseDelete, // 删除吐槽的回调
} ComplainResponseType;

@protocol ComplainListDelegate <NSObject>

// 获取到数据处理之后的回调
- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info;

@end

@interface ICN_ComplainListViewModel : NSObject

@property (nonatomic , weak)id<ComplainListDelegate> delegate;
@property (nonatomic , strong)NSMutableArray<ICN_ComplainListModel *> *modelsArr; // 数据列表数组 -- 列表页的结果
@property (nonatomic , strong)NSMutableArray<ICN_ComplainCommentModel *> *commentListArr; // 数据列表数组 -- 评论内容
@property (nonatomic , strong)ICN_ComplainDetialModel *detialModel; // 吐槽详情Model


@property (nonatomic , assign)NSInteger currentPage; // 当前加载的分页数 默认是1



/** 获取吐槽列表数据的方法 */
- (void)requestComplainListWithType:(ComplainResponseType)enumType isLoad:(BOOL)isload MemberId:(NSString *)memberId SearchContent:(NSString *)searchContent;

/** 进行其他不获取到用户数据列表的操作 */
- (void)requestComplainFunctionWithType:(ComplainResponseType)enumType ComplainId:(NSString *)complainId ReplayerId:(NSString *)replayerId Content:(NSString *)content CommentId:(NSString *)commentId;

/** 进行对于吐槽详情的相关操作 */
- (void)requestComplainDetialWithType:(ComplainResponseType)enumType MemberId:(NSString *)memberId isLoad:(BOOL)isLoad;



@end
