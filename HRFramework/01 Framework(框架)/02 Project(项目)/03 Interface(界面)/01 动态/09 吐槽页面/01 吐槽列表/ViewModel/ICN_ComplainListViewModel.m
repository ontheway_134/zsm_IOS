//
//  ICN_ComplainListViewModel.m
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainListViewModel.h"
#import "BaseOptionalModel.h"
#import "ICN_ComplainListModel.h" // 吐槽列表Model
#import "ICN_ComplainDetialModel.h" // 吐槽详情Model
#import "ICN_ComplainCommentModel.h" // 评论内容Model

@implementation ICN_ComplainListViewModel

- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
    }
    return self;
}

- (NSMutableArray<ICN_ComplainListModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (NSMutableArray<ICN_ComplainCommentModel *> *)commentListArr{
    if (_commentListArr == nil) {
        _commentListArr = [NSMutableArray array];
    }
    return _commentListArr;
}

- (void)requestComplainFunctionWithType:(ComplainResponseType)enumType ComplainId:(NSString *)complainId ReplayerId:(NSString *)replayerId Content:(NSString *)content CommentId:(NSString *)commentId{
    // 1. 获取用户的token 不过不存在直接返回失败
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([self insertUserTokenWithDictionary:params] == NO) {
        return ;
    }
    
    // 将吐槽id添加在字典中
    [params setValue:SF(@"%@",complainId) forKey:@"id"];
    
    // 生成路径 并对于字典中的特殊字段处理
    NSString *netPath ;
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            // 吐槽点赞
            netPath = PATH_ComplainLikeUp;
            break;
        }
        case ComPlain_ResponseCommentLikeUp:{
            // 吐槽评论点赞
            netPath = PATH_ComplainCommentLikeUp;
            [params setValue:commentId forKey:@"commentId"];
            break;
        }
        case Complain_ResponseDisLikeUp:{
            // 吐槽取消点赞
            netPath = PATH_ComplainDisLikeUp;
            break;
        }
        case ComPlain_ResponseCommentDisLikeUp:{
            // 吐槽评论取消点赞
            netPath = PATH_ComplainCommentDisLikeUp;
            [params setValue:commentId forKey:@"commentId"];
            break;
        }
        case ComPlain_ResponsePublishComment:{
            // 吐槽发布评论
            netPath = PATH_PublishComplainComment;
            [params setValue:SF(@"%@",content) forKey:@"contant"];
            if (replayerId) {
                [params setValue:SF(@"%@",replayerId) forKey:@"replyer"];
            }
            break;
        }
        case Complain_ResponseDelete:{
            // 删除吐槽评论
            netPath = PATH_ComplainDelete;
            break;
        }
        default:
            break;
    }
    
    // 调用接口
    if (netPath) {
        [[HRRequest alloc] POST_PATH:netPath params:params success:^(id result) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
                // 正确操作
            }else{
                HRLog(@"warn====没有处理吐槽列表ViewModel的代理");
                return ;
            }
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                // 数据处理成功
                [self.delegate responseWithEnumType:enumType Success:YES Info:baseModel.info];
            }else{
                // 数据处理失败
                [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
            }
        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:enumType Success:NO Info:@"网络请求失败"];
        }];
    }
    
}

- (void)requestComplainDetialWithType:(ComplainResponseType)enumType MemberId:(NSString *)memberId isLoad:(BOOL)isLoad{
    
    // 根据是否加载的属性判断页面传入的currentPage
    if (isLoad) {
        self.currentPage ++;
    }else{
        self.currentPage = 1;
    }
    
    // 创建参数字典和路径变量
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:SF(@"%@",memberId) forKey:@"id"];
    [params setValue:SF(@"%ld",(long)self.currentPage) forKey:@"page"];
    NSString *netPath;
    
    // 对于参数中的token做校验
    if ([self insertUserTokenWithDictionary:params] == NO) {
        return ;
    }
    
    // 添加路径
    if (enumType == ComPlain_ResponseDetial) {
        netPath = PATH_ComplainDetial;
    }
    
    // 在确认ViewModel支持回调数据的时候添加网络请求
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
        // 开启网络请求并针对数据进行处理
        [[HRRequest manager] POST_PATH:netPath params:params success:^(id result) {
            // 1. 判断是否成功获取到数据
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                
                // 网络请求的结果成功
                // 进行数据处理成功的回调
                if (baseModel.code == 0) {
                    // 获取到有效数据的时候
                    NSDictionary *resultDic = [result valueForKey:@"result"];
                    // 1. 获取吐槽详情的内容
                    self.detialModel = [[ICN_ComplainDetialModel alloc] initWithDictionary:[resultDic valueForKey:@"top"] error:nil];
                    // 2. 获取吐槽评论数组
                    NSArray *commentDicArr = [resultDic valueForKey:@"comment"];
                    // 判断是刷新还是加载
                    if (isLoad == NO) {
                        // 刷新
                        [self.commentListArr removeAllObjects];
                    }
                    // 循环遍历获取Model
                    for (NSDictionary *dic in commentDicArr) {
                        ICN_ComplainCommentModel *model = [[ICN_ComplainCommentModel alloc] initWithDictionary:dic error:nil];
                        [self.commentListArr addObject:model];
                    }
                    
                    // 数据获取完毕后回调
                    [self.delegate responseWithEnumType:enumType Success:YES Info:baseModel.info];
                }else{
                    [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
                }
                
            }else{
                [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:enumType Success:NO Info:@"网络请求失败"];
        }];
    }else{
        HRLog(@"test=== 用户主页的更多信息没有被设置代理");
    }

}

- (BOOL)insertUserTokenWithDictionary:(NSMutableDictionary *)params{
    
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        HRLog(@"缺少必要的用户token");
        return NO;
    }
    [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    return YES;
}

- (void)requestComplainListWithType:(ComplainResponseType)enumType isLoad:(BOOL)isload MemberId:(NSString *)memberId SearchContent:(NSString *)searchContent{
    
    // 根据是否加载的属性判断页面传入的currentPage
    if (isload) {
        self.currentPage ++;
    }else{
        self.currentPage = 1;
    }
    
    // 第一步 根据类型判断是否需要token 若不需要则不添加token
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:SF(@"%ld",(long)self.currentPage) forKey:@"page"];
    
    // 第二步 根据不同的网络请求类型生成不同的路径 和传入参数
    NSString *netPath ;
    
    switch (enumType) {
        case Complain_ResponseMineList:{
            // 获取自己的吐槽列表 没有token则直接返回
            if ([self insertUserTokenWithDictionary:params] == NO) {
                return ;
            }
            netPath = PATH_ComplainMineList;
            break;
        }
        case Complain_ResponseOthersList:{
            // 获取首页的吐槽列表
            netPath = PATH_ComplainOthersList;
            break;
        }
        case Complain_ResponseSearchList:{
            // 获取模糊搜索的吐槽列表
            if ([self insertUserTokenWithDictionary:params] == NO) {
                return ;
            }
            [params setValue:SF(@"%@",searchContent) forKey:@"contant"];
            netPath = PATH_ComplainSearchList;
            break;
        }
        case Complain_ResponseUserIDList:{
            // 根据id获取用户的吐槽列表 不需要token
            [params setValue:SF(@"%@",memberId) forKey:@"memberId"];
            netPath = PATH_ComplainUserIDList;
            break;
        }
        default:
            break;
    }
    
    // 在确认ViewModel支持回调数据的时候添加网络请求
    if (self.delegate && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
        // 开启网络请求并针对数据进行处理
        [[HRRequest manager] POST_PATH:netPath params:params success:^(id result) {
            // 1. 判断是否成功获取到数据
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                
                // 网络请求的结果成功
                // 进行数据处理成功的回调
                if (baseModel.code == 0) {
                    // 获取到有效数据的时候
                    NSArray *resultArr = [result valueForKey:@"result"];
                    if (!isload) {
                        // 刷新操作
                        [self.modelsArr removeAllObjects];
                    }
                    for (NSDictionary *paramsDic in resultArr) {
                        ICN_ComplainListModel *model = [[ICN_ComplainListModel alloc] initWithDictionary:paramsDic error:nil];
                        [self.modelsArr addObject:model];
                    }
                    
                    [self.delegate responseWithEnumType:enumType Success:YES Info:baseModel.info];
                }else{
                    [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
                }
                
            }else{
                // 在code = 1并且为刷新数据的时候 设置历史数据为空
                if (baseModel.code == 1 && self.currentPage == 1) {
                    [self.modelsArr removeAllObjects];
                    [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
                }else{
                    // 其他 直接弹出提示信息
                    [self.delegate responseWithEnumType:enumType Success:NO Info:baseModel.info];
                }
            }
            
        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:enumType Success:NO Info:@"网络请求失败"];
        }];
    }else{
        HRLog(@"test=== 用户主页的更多信息没有被设置代理");
    }
    

}






@end




