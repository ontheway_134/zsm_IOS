//
//  ICN_ComplainDetialPager.m
//  ICan
//
//  Created by albert on 2017/3/1.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainDetialPager.h"
#import "ICN_TransmitToDynamicPager.h"     // 转发到动态页面
#import "ICN_ComplainCommentCell.h"        // 吐槽中评论你的Cell
#import "ICN_ComplainDetialCell.h"         // 吐槽中的详情Cell
#import "ICN_ComSingleTitleSectionView.h"  // 吐槽section 的头视图
#import "ICN_InputCommentView.h"           // 通用的评论输出框（含代理）
#import "ICN_ComplainListViewModel.h"      // 吐槽相关的ViewModel
#import "ICN_ComplainCommentModel.h"       // 评论Model
#import "ICN_ComplainDetialModel.h"        // 吐槽内容Model

#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model

@interface ICN_ComplainDetialPager ()<UITableViewDelegate , UITableViewDataSource , CommentTextDelegate , ComplainListDelegate , ICN_DynWarnViewDelegate>

@property (nonatomic , copy)NSString *replyerId; // 对ta评论的（评论人id）
@property (nonatomic , strong)ICN_ComplainListViewModel *viewModel; // 当前页面的ViewModel
@property (strong , nonatomic)UITableView *tableView;
@property (nonatomic , strong)ICN_InputCommentView *commentTextView; // 提交评论的输出框

@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作

@end

@implementation ICN_ComplainDetialPager

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.origin.y -= 64.0;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainCommentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainCommentCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainDetialCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainDetialCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 0.00001;
        _tableView.sectionHeaderHeight = 0.000001;
        _tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        
        [self.view addSubview:_tableView];
        // 使用masnory设置tableview的尺寸
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return _tableView;
}

- (ICN_InputCommentView *)commentTextView{
    if (_commentTextView == nil) {
        _commentTextView = [ICN_InputCommentView loadCommentView];
        _commentTextView.delegate = self;
        // 设置约束
        [self.view addSubview:_commentTextView];
        [_commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(49.0);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
    }
    return _commentTextView;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置显示默认的导航栏
    [self setHiddenDefaultNavBar:NO];
    // 初始化tableview
    [self tableView];
    // 设置标签
    self.naviTitle = @"吐槽详情";
    // 加载评论框页面
    [self commentTextView];
    // 设置ViewModel
    self.viewModel = [[ICN_ComplainListViewModel alloc] init];
    self.viewModel.delegate = self;
    [self.viewModel requestComplainDetialWithType:ComPlain_ResponseDetial MemberId:self.complainId isLoad:NO];
    // 配置页面的刷新加载
    [self configTableViewRefreshHeaderFooterView];
    if ([self isCurrentEnterpriseUser]) {
        self.commentTextView.hidden = YES;
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestComplainDetialWithType:ComPlain_ResponseDetial MemberId:self.complainId isLoad:NO];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        
        if (!self.tableView.mj_footer.isRefreshing) {
            // 判断调用的ViewModel以及对应的逻辑处理
            [self.viewModel requestComplainDetialWithType:ComPlain_ResponseDetial MemberId:self.complainId isLoad:YES];
        }
        
        
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}



#pragma mark - ---------- 按钮响应事件 ----------


#pragma mark - ---------- 协议 ----------

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}


/** 评论输出框的相关代理 */
#pragma mark --- CommentTextDelegate ---

// 获取到评论信息的回调
- (void)responseWithCommitComment:(NSString *)comment{
    if (comment) {
        [self.viewModel requestComplainFunctionWithType:ComPlain_ResponsePublishComment ComplainId:self.complainId ReplayerId:self.replyerId Content:comment CommentId:nil];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入有效评论内容"];
    }
}


#pragma mark - ---------- 吐槽ViewModel的回调代理 ----------

- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    
    // 第一步，，如果失败输出失败原因 停止页面刷新
    [self endRefreshWithTableView:self.tableView];
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
    }
    
    switch (enumType) {
        case Complain_ResponseLikeUp:
        case Complain_ResponseDisLikeUp:
        case ComPlain_ResponseCommentLikeUp:
        case ComPlain_ResponseCommentDisLikeUp:{
            // 对于评论点赞 -- 成功不刷新数据，失败刷新历史数据重置点赞样式
            if (success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        case ComPlain_ResponsePublishComment:{
            // 先使评论按钮失去详情
            [self.commentTextView.commentTextField resignFirstResponder];
            
            // 成功 直接刷新评论页面数据 -- 会失去之前显示的数据
            [self.viewModel requestComplainDetialWithType:ComPlain_ResponseDetial MemberId:self.complainId isLoad:NO];
            break ;
        }
        case ComPlain_ResponseDetial:{
            // 获取详情并刷新评论区 -- 成功刷新失败不刷（取消mj刷新动画）
            if (success) {
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
    
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 设置一共有12个假的评论数据
    switch (section) {
        case 0:{
            return 1;
            break;
        }
        // 返回评论的数量
        default:
            return self.viewModel.commentListArr.count;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.section) {
        case 0:{
            // 返回正文的Cell
            ICN_ComplainDetialCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainDetialCell class])];
            cell.model = self.viewModel.detialModel;
            [cell callBackCellBlock:^(ICN_ComplainDetialModel *model, NSInteger buttonType, BOOL isSelected) {
                switch (buttonType) {
                    case ICN_CellLikeActionBtnType:{
                        if (isSelected) {
                            [self.viewModel requestComplainFunctionWithType:Complain_ResponseLikeUp ComplainId:self.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }else{
                            [self.viewModel requestComplainFunctionWithType:Complain_ResponseDisLikeUp ComplainId:self.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }
                        break;
                    }
                    case ICN_CellCommentBtnType:{
                        // 回复评论操作
                        if (![self isCurrentEnterpriseUser]) {
                            [self.commentTextView.commentTextField becomeFirstResponder];
                        }
                        break;
                    }
                    case ICN_CellReplayType:{
                        // 进行转发操作
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:model.complainId Title:@"吐槽" IconUrl:model.RandomPicture Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.warnView];
                        break ;
                    }
                    default:
                        break;
                }
            }];
            return cell;
            break;
        }
        default:{
            // 返回评论内容Cell
            ICN_ComplainCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainCommentCell class])];
            cell.model = [self.viewModel.commentListArr objectAtIndex:indexPath.row];
            [cell callBackCellBlock:^(ICN_ComplainCommentModel *model, NSInteger buttonType, BOOL isSelected) {
                switch (buttonType) {
                    case ICN_CellLikeActionBtnType:{
                        if (isSelected) {
                            [self.viewModel requestComplainFunctionWithType:ComPlain_ResponseCommentLikeUp ComplainId:self.complainId ReplayerId:nil Content:nil CommentId:model.commentId];
                        }else{
                            [self.viewModel requestComplainFunctionWithType:ComPlain_ResponseCommentDisLikeUp ComplainId:self.complainId ReplayerId:nil Content:nil CommentId:model.commentId];
                        }
                        break;
                    }
                    case ICN_CellCommentBtnType:{
                        // 回复评论操作
                        if (![self isCurrentEnterpriseUser]) {
                            self.replyerId = model.memberId;
                            [self.commentTextView.commentTextField becomeFirstResponder];
                        }
                        break;
                    }
                    case ICN_CellReplayType:{
                        // 进行转发操作
                        // 进行转发操作
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:self.complainId Title:self.viewModel.detialModel.contant IconUrl:model.RandomPicture Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.warnView];
                    }
                    default:
                        break;
                }
            }];
            return cell;
            break;
        }
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 1) {
        ICN_ComSingleTitleSectionView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        header.contentView.backgroundColor = RGB0X(0xf0f0f0);
        header.headerTitle = @"评论";
        return header;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}


/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // 默认的 section 0 没有头视图 section 1 有
    if (section == 1) {
        return 40.0;
    }
    if (section == 0) {
        return 0.0;
    }
    return 0.0;
    
}



@end
