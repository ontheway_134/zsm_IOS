//
//  ICN_ComplainDetialModel.m
//  ICan
//
//  Created by albert on 2017/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainDetialModel.h"

@implementation ICN_ComplainDetialModel

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"complainId" : @"id"}];
}


@end
