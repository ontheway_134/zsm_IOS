//
//  ICN_ComplainCommentModel.h
//  ICan
//
//  Created by albert on 2017/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_ComplainCommentModel : BaseOptionalModel

//commentNum": 无意义,
//"ThumbsupNum": 该评论点赞数,
//"createTime": 发布吐槽评论时间,
//"contant": 评论回复内容,
//"replyer": 回复者,（如果是对该吐槽进行评论 则该值为0）
//"replyerName": 回复者昵称,（如果是对该吐槽进行评论 则该值为null）
//"memberId": 发布吐槽评论者id,
//"makeComplaintsId": 无意义（吐槽id）,
//"RandomPicture": 发布吐槽评论者头像,
//"RandomName": 发布吐槽评论者昵称
//"commentId": 评论ID
//"ismakeComplaints": 是否点赞 0为已经点赞 1为未点赞

@property (nonatomic , copy)NSString *ThumbsupNum; //"ThumbsupNum": 该评论点赞数,
@property (nonatomic , copy)NSString *createTime; //"createTime": 发布吐槽评论时间,
@property (nonatomic , copy)NSString *contant; //"contant": 评论回复内容,
@property (nonatomic , copy)NSString *replyer; //"replyer": 回复者,（如果是对该吐槽进行评论 则该值为0）
@property (nonatomic , copy)NSString *replyerName; //"replyerName": 回复者昵称,（如果是对该吐槽进行评论 则该值为null）
@property (nonatomic , copy)NSString *memberId; //"memberId": 发布吐槽评论者id,
@property (nonatomic , copy)NSString *RandomPicture; //"RandomPicture": 发布吐槽评论者头像,
@property (nonatomic , copy)NSString *RandomName; //"RandomName": 发布吐槽评论者昵称
@property (nonatomic , copy)NSString *commentId; //"commentId": 评论ID
@property (nonatomic , copy)NSString *ismakeComplaintsSeltucao; //"ismakeComplaintsSeltucao": 是否点赞 0为已经点赞 1为未点赞



@end
