//
//  ICN_ComplainDetialModel.h
//  ICan
//
//  Created by albert on 2017/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_ComplainDetialModel : BaseOptionalModel

//"complainId": 吐槽id,
//"contant": 吐槽内容,
//"commentNum": 吐槽评论数,
//"ThumbsupNum": 吐槽点赞数,
//"createTime": 吐槽发布时间,
//"RandomPicture": 发布吐槽者头像,
//"RandomName": 发布吐槽者昵称,
//"memberId": "259"


@property (nonatomic , copy)NSString *complainId; //"id": 吐槽id,
@property (nonatomic , copy)NSString *contant; //"contant": 吐槽内容,
@property (nonatomic , copy)NSString *commentNum; //"commentNum": 吐槽评论数,
@property (nonatomic , copy)NSString *ThumbsupNum; //"ThumbsupNum": 吐槽点赞数,
@property (nonatomic , copy)NSString *createTime; //"createTime": 吐槽发布时间,
@property (nonatomic , copy)NSString *RandomPicture; //"RandomPicture": 发布吐槽者头像,
@property (nonatomic , copy)NSString *RandomName; //"RandomName": 发布吐槽者昵称,
@property (nonatomic , copy)NSString *memberId; //"memberId": "259"

@property (nonatomic , copy)NSString *ismakeComplaints;//"ismakeComplaints": 是否点赞 0为已经点赞 1为未点赞


@end
