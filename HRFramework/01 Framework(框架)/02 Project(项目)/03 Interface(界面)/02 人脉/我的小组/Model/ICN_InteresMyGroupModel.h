//
//  ICN_InteresMyGroupModel.h
//  ICan
//
//  Created by shilei on 17/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_InteresMyGroupModel : BaseOptionalModel

@property(nonatomic,strong)NSString *groupLogo;    //  头像
@property(nonatomic,strong)NSString *groupName;     //组的名字
@property(nonatomic,strong)NSString *companyName;    //公司名
@property(nonatomic,strong)NSString *joinType;      //职务类型
@property(nonatomic,strong)NSString *groupId;       //组id

@property(nonatomic,strong)NSString *number;

@property(nonatomic,strong)NSString *summary;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *groupAssessment;   //小组人数
@property(nonatomic,strong)NSString *isopen;   //是否公开

@end
