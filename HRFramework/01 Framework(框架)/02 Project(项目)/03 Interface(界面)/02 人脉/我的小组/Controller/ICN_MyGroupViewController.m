//
//  ICN_MyGroupViewController.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyGroupViewController.h"
#import "ICN_NewFridentTableViewCell.h"
#import "ICN_CreateGroupView.h"
#import "ICN_GroupDetialViewController.h"
#import "ICN_ApplyCommitRequestViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_MyGroupModel.h"
#import "ICN_InteresMyGroupModel.h"
#import "ICN_ApplyRecodeModels.h"
#import "ICN_HeadImageSelView.h"
#import "ICN_CameraBallView.h"    //相机蒙板
#import "ICN_loadHeadImageModel.h"  //上传图片

@interface ICN_MyGroupViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *MyGroupTabelView;
@property(nonatomic,strong)ICN_CreateGroupView *create;

@property(nonatomic,strong)NSMutableArray *MyGroupData;
@property(nonatomic,strong)NSMutableArray *InteresData;

@property(nonatomic,assign)BOOL isRecord;    //是否为申请记录的判断
@property(nonatomic,strong)NSMutableArray *recordData;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,assign)NSInteger recoedpage;
@property(nonatomic,strong)NSMutableArray *ImageData;

@property(nonatomic,strong)UIImageView *ima;  //无数据imag
@property(nonatomic,strong)UILabel *la;   //无数据 标签

@property (nonatomic , copy)NSString *selectGroupId;


@property (weak, nonatomic) IBOutlet UIView *myView;

@end

@implementation ICN_MyGroupViewController


#pragma mark - --- 网络请求 ---

//上图图片的网络请求
-(void)loadHeadImageView{
    
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"小组创建中...."];
     [[[HRNetworkingManager alloc]init] UPLOAD_IMAGES:self.ImageData Path:PATH_MultipleImagesUpload progress:^(int64_t bytesProgress, int64_t totalBytesProgress) {
        
     } success:^(id responseObject) {
         ICN_loadHeadImageModel *model = [[ICN_loadHeadImageModel alloc] initWithDictionary:responseObject error:nil];
         if (model.code == 0) {
             // 获取图片成功上传其他资料
             [self httpCreateGroup:model.result.fileName];
             [MBProgressHUD hiddenHUDWithSuperView:self.view];
         }
        
     } failure:^(NSDictionary *errorInfo) {
         [MBProgressHUD hiddenHUDWithSuperView:self.view];
         [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"图片上传失败"];
         NSLog(@"请求 失败");
     }];

}

//申请记录的网络请求
-(void)httpRecodepage:(NSInteger)page{
    
    NSDictionary *dic=@{@"memberId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"page":[NSString stringWithFormat:@"%ld",(long)page],@"pagenum":@10};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_XXCCCCCC params:dic success:^(id result) {
        
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (base.code ==0) {
            
            if (self.recoedpage == 1) {
                [self.recordData removeAllObjects];
            }
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_ApplyRecodeModels *model = [[ICN_ApplyRecodeModels alloc] initWithDictionary:dic error:nil];
                //这里的申请记录只需要添加status状态 0 1 2 的状态的意思分别为以同意和以拒绝
                if (model.status.integerValue <= 2) {
                    [self.recordData addObject:model];
                }
            }
            [self.MyGroupTabelView reloadData];
            
        }
        [self.MyGroupTabelView.mj_header endRefreshing];
        [self.MyGroupTabelView.mj_footer endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
        [self.MyGroupTabelView.mj_header endRefreshing];
        [self.MyGroupTabelView.mj_footer endRefreshing];
        
    }];

}

//退出小组对
-(void)httpQuitGroupWithGroupId:(NSString *)groupId{
    NSDictionary *dic=@{@"groupId":groupId , @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken]};
    self.selectGroupId = groupId;
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_WUITGTROUP params:dic success:^(id result) {
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (base.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"退出成功"];
            [self.MyGroupTabelView reloadData];
            [self httpMyGroupNetworing];
        }
    
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求失败");
    }];

}

//创建小组的接口
-(void)httpCreateGroup:(NSString *)file{
    
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupLogo":file,@"groupName":self.create.GroupNameTextFiled.text,@"summary":self.create.GroupIntroTextView.text,@"number":self.create.GroupCountTextfiled.text,@"open":[NSString stringWithFormat:@"%ld",(long)self.create.openNumber]};
   
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CREATEGROUP params:dic success:^(id result) {
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (base.code == 0) {
            // 隐藏之前的HUD
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"创建成功"];
            [self httpMyGroupNetworing];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        NSLog(@"请求失败");
    }];
}

//我的小组的网络请求
-(void)httpMyGroupNetworing{
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MYGROUP params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"page" :@1, @"pagenum" : @10} success:^(id result) {
        [self.MyGroupData removeAllObjects];
          BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
          if (base.code ==0) {

              for (NSDictionary *dic in [result valueForKey:@"result"]) {
                  
                  
                  ICN_MyGroupModel *model = [[ICN_MyGroupModel alloc] initWithDictionary:dic error:nil];
#warning 我自己创建的小组不添加任何限制，别人不可发现我的小组
                  if (model.isopen.integerValue == 0 || model.isopen.integerValue == 1) {
                      [self.MyGroupData addObject:model];
                  }
              }
              [self.MyGroupTabelView reloadData];

          }
        
      } failure:^(NSDictionary *errorInfo) {
        
          [self.MyGroupTabelView.mj_header endRefreshing];
          [self.MyGroupTabelView.mj_footer endRefreshing];
      }];
}
//发现更多的小组
-(void)httpMyInterestingNetworingpage:(NSInteger)page{

    //当前用户id 只有1有数据，此处数据和UI图对不上,这里正常应该有pagenum参数，加上返回1，不加返回0
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MYMOREGROUP params:@{@"page" :[NSString stringWithFormat:@"%ld",(long)page], @"pagenum" : @10,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (base.code ==0) {
        
            if (page == 1) {
                [self.InteresData removeAllObjects];
            }
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_InteresMyGroupModel *model = [[ICN_InteresMyGroupModel alloc] initWithDictionary:dic error:nil];
                
                if (model.isopen.integerValue == 0) {
                    [self.InteresData addObject:model];
                }
                
            }
            [self.MyGroupTabelView reloadData];
        }
        
        [self.MyGroupTabelView.mj_header endRefreshing];
        [self.MyGroupTabelView.mj_footer endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
        
        [self.MyGroupTabelView.mj_header endRefreshing];
        [self.MyGroupTabelView.mj_footer endRefreshing];
    }];

}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.MyGroupTabelView registerNib:[UINib nibWithNibName:@"ICN_NewFridentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NewFridentTableViewCell"];
    self.MyGroupTabelView.delegate=self;
    self.MyGroupTabelView.dataSource=self;
    self.MyGroupTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self frefsh];
    self.myView.frame=CGRectMake(SCREEN_WIDTH*35/320, 60, SCREEN_WIDTH*55/320, 1);
    
//    NSArray *arr = [[ChatDemoHelper shareHelper] getMyGroups];
    
 }

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [self frefsh];

}

#pragma mark - --- Protocol ---

#pragma mark - --- UIImagePickerControllerDelegate ---

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
  
    [self.view addSubview:self.create];
    [self.create mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(64);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    UIImage *newPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.create.headImageViews.image = newPhoto;
  
    [self.ImageData addObject:self.create.headImageViews.image];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.isRecord) {
        return self.recordData.count;
    }
    else{
        switch (section) {
            case 0:
                return self.MyGroupData.count;
                break;
                
            case 1:
                return self.InteresData.count;
                
                break;
        }
    }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.isRecord) {
        return 1;
    }else{
       return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_NewFridentTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_NewFridentTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_NewFridentTableViewCell" owner:self options:nil] lastObject];
       
    }
    // 判断显示的是申请记录的Cell
    if (self.isRecord) {
        cell.applymodel = self.recordData[indexPath.row];

    }else{
        
        cell.TureBtn.hidden = YES;
        cell.FalseBtn.hidden = YES;
        switch (indexPath.section) {
            case 0:{
                NSLog(@"我的小组的");
                cell.mygroupmodel=self.MyGroupData[indexPath.row];
             
            }
                break;
                
            case 1:{
                NSLog(@"有趣小组的");
                cell.InteresModel=self.InteresData[indexPath.row];
            }
                break;
                
        }
      
}
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.isRecord) {
        return 0.01f;
    }else{
        
        switch (section) {
            case 0:{
                if (self.MyGroupData.count == 0) {
                     return 0.01f;
                }else{
                   return 36;
                }
            }
                break;
            case 1:{
                if (self.InteresData.count == 0) {
                    return 0.01f;
                }else{
                    return 36;
                }
            
            }
                break;
            
        }
        
        return 0;
        
       
    }
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section{
    
    if (self.isRecord) {
        return @"";
    }
    else{
        switch (section) {
            case 0:{
                if (self.MyGroupData.count == 0) {
                    return @"";
                }else{
                     return @"我的小组";
                }
    
            }
               
                break;
                
            case 1:{
            
                if (self.InteresData.count == 0) {
                    return @"";
                }else{
                    return @"发现更多小组";
                }
            }
               
                break;
        }
        
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isRecord) {
        NSLog(@"申请记录的跳转");
    }else{
        if (indexPath.section == 0) {
            ICN_GroupDetialViewController *detial=[[ICN_GroupDetialViewController alloc]init];
            detial.groupId = [self.MyGroupData[indexPath.row] groupId];
            
            detial.isShow=YES;
            [self.navigationController pushViewController:detial animated:YES];
            return ;
        }
        
        if (indexPath.section == 1) {
            ICN_GroupDetialViewController *detial=[[ICN_GroupDetialViewController alloc]init];
            
            detial.groupId = [self.InteresData[indexPath.row] groupId];
            detial.isShow = NO;
            [self.navigationController pushViewController:detial animated:YES];
            return ;
        }
    }
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 
    if (self.isRecord) {
        return NO;
    }else{
    
        switch (indexPath.section) {
            case 0:
                return YES;
                break;
                
            case 1:
                return NO;
                break;
        }
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

     if (editingStyle == UITableViewCellEditingStyleDelete) {
         
         switch (indexPath.section) {
             case 0:{
        
                 NSLog(@"%@",[self.MyGroupData[indexPath.row] groupId]);
                 [self httpQuitGroupWithGroupId:[self.MyGroupData[indexPath.row] groupId]];
                 
             }
                 break;
         }
    
     }
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
   // 这里需要增加字断判断是否是组长，组长返回的为解散，组员返回的为退出
    return @"退出";
}


#pragma mark - --- IBActions ---

#pragma mark 上面的三个按钮的点击事件

- (IBAction)ClickBtnActions:(UIButton *)sender {
    switch ([sender tag]) {
        case 0:{
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        case 1:{
            
            if (self.create !=nil) {
                [self.create removeFromSuperview];
            }

            self.myView.frame=CGRectMake(SCREEN_WIDTH*230/320, 60, SCREEN_WIDTH*55/320, 1);
            [self.view addSubview:_create];
            [_create mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(64);
                make.left.mas_equalTo(0);
                make.right.mas_equalTo(0);
                make.bottom.mas_equalTo(0);
            }];
            
        }
            break;
        // 跳转到申请记录
        case 2:{
            if (self.create !=nil) {
                [self.create removeFromSuperview];
            }
            
            self.myView.frame=CGRectMake(SCREEN_WIDTH*133/320, 60, SCREEN_WIDTH*55/320, 1);
            self.isRecord = YES;
            [self.MyGroupTabelView reloadData];
            if (!self.MyGroupTabelView.mj_header.isRefreshing) {
            [self frefsh];
            }

        }
            break;
        case 3:{

            if (self.create !=nil) {
                [self.create removeFromSuperview];
            }
            
            self.myView.frame=CGRectMake(SCREEN_WIDTH*35/320, 60, SCREEN_WIDTH*55/320, 1);
            self.isRecord = NO;
            [self.MyGroupTabelView reloadData];
            if (!self.MyGroupTabelView.mj_header.isRefreshing) {
                [self frefsh];
            }

        }
            break;
    }
}

#pragma mark - --- 自定义方法 ---

//提交按钮的判断
-(void)ClickremoveView{
    
    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    
    if ([self.create.headImageViews.image isEqual:[UIImage imageNamed:@"占位图"]]) {
        [MBProgressHUD ShowProgressWithBaseView:self.create Message:@"请选择图片"];
        return;
    }
    
    if ([self.create.GroupNameTextFiled.text length] == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.create Message:@"请输入小组名字"];
        return;
       
    }
    
    if ([self.create.GroupIntroTextView.text length] == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.create Message:@"请输入小组简介"];
        return;
    }
    
    if ([self.create.GroupCountTextfiled.text length] == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.create Message:@"请输入小组人数"];
        return;

    }
    // 小组介绍非必填 -- 不填可以接受
    if ([self.create.GroupIntroTextView.text isEqualToString:@"请介绍小组"] ) {
        self.create.GroupIntroTextView.text = @"";
    }
    
    [self.create removeFromSuperview];
    [self loadHeadImageView];
    
}

//创建小组头像上面手势的添加
-(void)Gesture{
    
    ICN_CameraBallView *camera=[[ICN_CameraBallView alloc]init];
    camera=[[[NSBundle mainBundle]loadNibNamed:@"ICN_CameraBallView" owner:nil options:nil] lastObject];
    [[UIApplication sharedApplication].keyWindow addSubview:camera];
    [camera mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    [camera returnLocationPhoto:^{
        [self.create removeFromSuperview];
        [camera removeFromSuperview];
        UIImagePickerController *controller=[[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        [controller setDelegate:self];
        [self presentViewController:controller animated:YES completion:nil];

    }];
    [camera returnTakingPhoto:^{
      
        UIImagePickerController *controller = [[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypeCamera;
        [controller setDelegate:self];
        [self presentViewController:controller animated:YES completion:nil];
        [camera removeFromSuperview];
    }];

}



//无数据时背景图片的显示
-(void)customMethod{

    if (self.MyGroupData.count == 0 && self.InteresData.count==0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无小组信息";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
}


//刷新
-(void)frefsh{

    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
       
        //为isRecord 时是申请记录的
        if (self.isRecord) {
            self.recoedpage = 1;
            [self httpRecodepage:self.recoedpage];
        }else{
            self.page = 1;
            [self httpMyInterestingNetworingpage:self.page];
            [self httpMyGroupNetworing];

        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    
    
    self.MyGroupTabelView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        if (self.isRecord) {
            self.recoedpage += 1;
            [self httpRecodepage:self.recoedpage];
        }else{
            self.page+=1;
            [self httpMyInterestingNetworingpage:self.page];
        }
    }];
    // 设置footer的标识 MJRefreshAutoNormalFooter
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.MyGroupTabelView.mj_footer = footer;
    [self.MyGroupTabelView.mj_header beginRefreshing];
    

}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)MyGroupData{
    if (!_MyGroupData) {
        _MyGroupData=[NSMutableArray array];
    }
    return _MyGroupData;
}

-(NSMutableArray *)InteresData{
    if (!_InteresData) {
        _InteresData=[NSMutableArray array];
    }
    return _InteresData;
}

-(NSMutableArray *)recordData{
    if (!_recordData) {
        _recordData=[NSMutableArray array];
    }
    return _recordData;
}

-(NSMutableArray *)ImageData{
    if (!_ImageData) {
        _ImageData=[NSMutableArray array];
    }
    return _ImageData;
}

//创建小组的蒙板
-(ICN_CreateGroupView *)create{
    if (!_create) {
        _create=[[[NSBundle mainBundle] loadNibNamed:@"ICN_CreateGroupView" owner:self options:nil] firstObject];
        [_create.CommitBtnAction addTarget:self action:@selector(ClickremoveView) forControlEvents:UIControlEventTouchUpInside];
        _create.headImageViews.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Gesture)];
        [_create.headImageViews addGestureRecognizer:tap];
    }
    return _create;
}


@end
