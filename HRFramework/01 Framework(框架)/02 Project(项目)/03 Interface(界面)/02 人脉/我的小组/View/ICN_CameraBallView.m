//
//  ICN_CameraBallView.m
//  ICan
//
//  Created by shilei on 17/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CameraBallView.h"

@implementation ICN_CameraBallView
- (IBAction)LocationAction:(UIButton *)sender {
    if (self.locationPhoto) {
        self.locationPhoto(sender);
    }
}

- (IBAction)TakingAction:(UIButton *)sender {
    if (self.returnTakingPhoto) {
        self.returnTakingPhoto(sender);
    }
}


-(void)returnLocationPhoto:(ReturnLocationPhotos)block{
    self.locationPhoto=block;
}
-(void)returnTakingPhoto:(ReturnTakingPhotos)block{
    self.returnTakingPhoto= block;
}
- (IBAction)removeView:(id)sender {
    [self removeFromSuperview];
}


@end
