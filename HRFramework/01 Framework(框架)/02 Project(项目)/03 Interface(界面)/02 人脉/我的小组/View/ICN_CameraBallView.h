//
//  ICN_CameraBallView.h
//  ICan
//
//  Created by shilei on 17/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ReturnLocationPhotos)();//本地
typedef void (^ReturnTakingPhotos)();   //拍照


@interface ICN_CameraBallView : UIView

@property (strong,nonatomic)ReturnLocationPhotos locationPhoto;
@property (strong,nonatomic)ReturnTakingPhotos returnTakingPhoto;

-(void)returnLocationPhoto:(ReturnLocationPhotos)block;
-(void)returnTakingPhoto:(ReturnTakingPhotos)block;


@end
