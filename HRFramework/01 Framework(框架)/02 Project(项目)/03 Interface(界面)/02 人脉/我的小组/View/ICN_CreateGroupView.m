//
//  ICN_CreateGroupView.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CreateGroupView.h"
#import "BaseOptionalModel.h"

@interface ICN_CreateGroupView()<UITextViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *CommitBtn;

@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancerBtn;



@end


@implementation ICN_CreateGroupView


//验证小组是否存在小组

-(void)httpVerify{
    
    NSDictionary *dic = @{@"groupName":self.GroupNameTextFiled.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_VERIFY params:dic success:^(id result) {
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (base.code ==1) {
            [MBProgressHUD ShowProgressWithBaseView:self Message:@"小组名已存在"];
            self.GroupNameTextFiled.text = @"";
            
        }else{
          //  [MBProgressHUD ShowProgressWithBaseView:self Message:base.info];
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"创建失败");
    }];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backView.layer.cornerRadius = 5.0;
    self.backView.layer.masksToBounds = YES;
    self.CommitBtn.layer.cornerRadius = 5.0;
    self.CommitBtn.layer.masksToBounds = YES;
    self.headImageViews.layer.cornerRadius = 25.0;
    self.headImageViews.layer.masksToBounds = YES;
    self.GroupIntroTextView.delegate =self;
    self.GroupNameTextFiled.delegate=self;
    
    self.GroupCountTextfiled.delegate =self;
    NSString  * nsTextContent=self.GroupIntroTextView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
}

- (IBAction)removeView:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}


//是否公开
- (IBAction)openBtnAction:(UIButton *)sender {

    sender.selected =!sender.selected;
    self.confirmBtn.selected = YES;

    if ([sender isEqual:self.confirmBtn]) {

        self.confirmBtn.selected = YES;
        self.cancerBtn.selected = NO;
        self.openNumber = 0;
        
    }else{
        self.cancerBtn.selected = YES;
        self.confirmBtn.selected = NO;
        self.openNumber = 1;
    }
   
}

#pragma mark UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    CGRect frame = textView.frame;
    float  height = [ self heightForTextView:textView WithText:textView.text];
    frame.size.height = height;
    [UIView animateWithDuration:0.5 animations:^{
        
        textView.frame = frame;
        
    } completion:nil];
    
    
    if (range.location>=60){
        return  NO;
    }
    else{
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
    
//    if (![self.GroupIntroTextView.text isEqualToString:@"请介绍小组"]) {
//        self.GroupIntroTextView.text = self.GroupIntroTextView.text;
//        return;
//    }
    
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    if ([self.GroupIntroTextView.text isEqualToString:@"请介绍小组"]) {
        self.GroupIntroTextView.text = @"";
    }
    
}

- (float) heightForTextView: (UITextView *)textView WithText: (NSString *) strText{
    CGSize constraint = CGSizeMake(textView.contentSize.width , CGFLOAT_MAX);
    CGRect size = [strText boundingRectWithSize:constraint
                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                     attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]}
                                        context:nil];
    float textHeight = size.size.height + 22.0;
    return textHeight;
}


#pragma mark UITextFieldDelegate


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self httpVerify];
    
    if (textField == self.GroupCountTextfiled) {
    
        if ([self.GroupCountTextfiled.text integerValue]<=0) {
            [MBProgressHUD ShowProgressWithBaseView:self Message:@"请输入正确的人数"];
            self.GroupCountTextfiled.text = @"";
        }
//        if ([self.GroupCountTextfiled.text integerValue]>50) {
//            [MBProgressHUD ShowProgressWithBaseView:self Message:@"最大人数为50人"];
//            self.GroupCountTextfiled.text = @"";
//        }
        
    }
    
}


@end
