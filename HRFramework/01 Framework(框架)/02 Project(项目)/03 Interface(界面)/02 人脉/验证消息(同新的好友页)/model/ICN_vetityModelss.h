//
//  ICN_vetityModelss.h
//  ICan
//
//  Created by shilei on 17/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface ICN_vetityModelss : BaseOptionalModel

@property(nonatomic,strong)NSString *memberId;
@property(nonatomic,strong)NSString *memberLogo;
@property(nonatomic,strong)NSString *companyName;
@property(nonatomic,strong)NSString *memberPosition;
@property(nonatomic,strong)NSString *memberSchool;
@property(nonatomic,strong)NSString *plusv;
@property(nonatomic,strong)NSString *content;
@property(nonatomic,strong)NSString *memberNick;

@end
