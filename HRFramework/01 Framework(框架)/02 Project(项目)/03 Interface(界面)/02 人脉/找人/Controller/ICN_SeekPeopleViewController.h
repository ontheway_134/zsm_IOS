//
//  ICN_SeekPeopleViewController.h
//  ICan
//
//  Created by shilei on 17/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_SeekPeopleViewController : BaseViewController

@property (nonatomic , copy)NSString *searchContent;

@end
