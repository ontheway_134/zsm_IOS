//
//  ICN_NewContactErQiViewController.m
//  ICan
//
//  Created by shilei on 17/3/1.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_NewContactErQiViewController.h"
#import "ICN_InterestTableViewCell.h"
#import "HandleAddressBook.h"
#import "phoneIstexit.h"
#import "BaseOptionalModel.h"
#import "ICN_contactModel.h"
#import "ICN_NoteModel.h"   //result为字符串的model 用于发送短信的接口

@interface ICN_NewContactErQiViewController ()<UITableViewDelegate,UITableViewDataSource,ICN_BtnSelectedAddDelegate>

@property (weak, nonatomic) IBOutlet UITableView *ContactTableView;
@property (weak, nonatomic) IBOutlet UIImageView *noneImageIcon; // 无图片
@property (weak, nonatomic) IBOutlet UILabel *noneContantLabel; // 无内容标签



@property(nonatomic,strong)NSMutableArray *dataSource;

@property(nonatomic,strong)NSMutableArray *items;   //分页显示的数组

@property(nonatomic,strong)PersonInfoModel *contectmodel;

@property(nonatomic,assign)NSInteger page;

@property(nonatomic,strong)NSString *str;

@property(nonatomic,strong)NSMutableArray *contactArr;

@property(nonatomic,assign)NSInteger *index;



@end

@implementation ICN_NewContactErQiViewController


#pragma mark - --- 网络请求 ---

//获取所有的数据的请求
-(void)httpcontactPerson{
    
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"mobile":self.str};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CONTACTPERSON params:dic success:^(id result) {
        BaseOptionalModel *optionmodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        
        if (optionmodel.code == 0) {
            //获取的姓名在显示出来
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                //获取的数据
                ICN_contactModel *contact=[[ICN_contactModel alloc]initWithDictionary:dic error:nil];
                [self.contactArr addObject:contact];
            }
            [self.ContactTableView reloadData];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
    
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.page = 0;
    [self.navigationController setNavigationBarHidden:YES];
    [self.ContactTableView registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.ContactTableView.delegate = self;
    self.ContactTableView.dataSource = self;
    [self refresh];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    // 将无内容的显示插在下面
    [self.view insertSubview:self.noneImageIcon belowSubview:self.ContactTableView];
    [self.view insertSubview:self.noneContantLabel belowSubview:self.ContactTableView];
}

#pragma mark - --- Protocol ---

#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    if ([cell.addFridentBtn.titleLabel.text isEqualToString:@"邀请"]) {
        //发送短信的接口数据，这里面没有对电话的座机做任何的处理，短信验证码可以成功
        NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"mobile":cell.contectmodel.mobile};
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_NOTE params:dic success:^(id result) {
            ICN_NoteModel *note=[[ICN_NoteModel alloc]initWithDictionary:result error:nil];
            if (note.code == 0) {
                //这里面信息发送成功，数据没有做相应的改变
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息发送成功"];
            }
        } failure:^(NSDictionary *errorInfo) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息发送失败"];
        }];
        
    }
    
    if ([cell.addFridentBtn.titleLabel.text isEqualToString:@"加好友"]) {
        NSLog(@"走添加好友的接口");
        NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.model.memberId};
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
            
            BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                [btn setTitle:@"等待验证" forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
                
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
        
        
    }
    
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger count = [self.contactArr count];
    return  count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    cell.delegate =self;
    
    if([indexPath row] >= ([self.contactArr count])) {
        
        
    }else {
        
        //为注册的需要获取电话号的姓名的信息
        for (ICN_contactModel *mode in self.contactArr) {
            if (mode.memberId ==nil) {
                PersonInfoModel *model = self.items[indexPath.row];
                cell.nickNameLabel.text = model.personName;
                
            }
        }
        //注册的走model，显示后台返回的数据
        {
            cell.contectmodel = self.contactArr[indexPath.row];
            
        }
    }
    
    return cell;
}



#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 57;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - --- 自定义方法 ---

- (void)initDatas{
    
    [HandleAddressBook addressBookAuthorization:^(NSMutableArray<PersonInfoModel *> *personInfoArray) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.dataSource = personInfoArray;
            
            self.items=[[NSMutableArray alloc] initWithCapacity:0];
            
            NSString *concact;
            //如果电话号码小于10个的时候
            
            // 判断如果·dataSource 没有数据 直接退出 (隐藏通讯录tableview)
            if (self.dataSource.count == 0) {
                [self.ContactTableView.mj_header endRefreshing];
                self.ContactTableView.hidden = YES;
                return ;
            }
            for (NSInteger i = 0; i < (self.dataSource.count > 10 ? 10 : self.dataSource.count); i++) {
                // 获取联系人信息
                PersonInfoModel *contactmodels=self.dataSource[i];
                // 拼接电话
                if (contactmodels.personPhone != nil && contactmodels.personPhone.count > 0) {
                    // 在self的items中添加有意义的电话的信息
                    [self.items addObject:contactmodels];
                    if (concact == nil) {
                        concact = SF(@"%@",contactmodels.personPhone[0]);
                    }else{
                        concact = SF(@"%@,%@",concact , contactmodels.personPhone[0]);
                    }
                }
            }
            //字符串的替换
            self.str = [concact stringByReplacingOccurrencesOfString:@"-" withString:@""];
            //获取到数据之后在去请求网络的数据；
            [self httpcontactPerson];
            
            [self.ContactTableView.mj_header endRefreshing];
            [self.ContactTableView reloadData];
            
            //            for (int i=0; i<10; i++) {
            //                if (self.dataSource.count <= i) {
            //                    // dataSource数组中没有i的索引
            //                    break ;
            //                }else{
            //                    [self.items addObject:self.dataSource[i]];
            //                }
            //                PersonInfoModel *contactmodels=self.dataSource[i];
            //                // 判断么有数组的情况下返回
            //                if (_contectmodel.personPhone.count == 0) {
            //                    [self.ContactTableView.mj_header endRefreshing];
            //                    self.ContactTableView.hidden = YES;
            //                    return ;
            //                }
            //                NSLog(@"%@",contactmodels.personPhone[0]);
            //
            //                //这里的str是经过拼装得到的str;最后多一个逗号
            //                if (i==9) {
            //                    concact = [concact stringByAppendingFormat:@"%@", contactmodels.personPhone[0]];
            //
            //                }else{
            //                     concact = [concact stringByAppendingFormat:@"%@,", contactmodels.personPhone[0]];
            //                }
            //
            //            }
            //
            //            //字符串的替换
            //            self.str = [concact stringByReplacingOccurrencesOfString:@"-" withString:@""];
            //            NSLog(@"%@",self.str);
        });
    }];
}

//数据的刷新
-(void)refresh{
    
    
    self.page = 0;
    
    [self initDatas];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page=self.page+10;
        [self loadMore:self.page];
        
    }];
    // 设置footer的标识 MJRefreshAutoNormalFooter
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.ContactTableView.mj_footer = footer;
    [self.ContactTableView.mj_header beginRefreshing];
    
}

-(void)loadMore:(NSInteger)pagecount{
    
    //添加新的数据
    NSMutableArray *more;
    more=[[NSMutableArray alloc] initWithCapacity:0];
    
    NSString *concact=@"";
    
    if (pagecount + 10 > self.dataSource.count - 1) {
        [self.ContactTableView.mj_footer endRefreshingWithNoMoreData];
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"没有更多数据"];
        return ;
    }
    
    for (NSInteger i=pagecount; i<pagecount+10; i++) {
        
        if (self.dataSource.count < i) {
            break;
        }
        
        [more addObject:self.dataSource[i]];
        
        PersonInfoModel *contactmodels=self.dataSource[i];
        NSLog(@"%@",contactmodels.personPhone[0]);
        
        //这里的str是经过拼装得到的str;最后多一个逗号
        if (i==pagecount+9) {
            concact = [concact stringByAppendingFormat:@"%@", contactmodels.personPhone[0]];
        }else{
            concact = [concact stringByAppendingFormat:@"%@,", contactmodels.personPhone[0]];
        }
        
    }
    
    self.str = @"";
    self.str = [concact stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSLog(@"%@",self.str);
    //获取到数据之后在去请求网络的数据；
    //加载你的数据
    [self performSelectorOnMainThread:@selector(appendTableWith:) withObject:more waitUntilDone:NO];
    
}
-(void)appendTableWith:(NSMutableArray *)data
{
    for (int i=0;i<[data count];i++) {
        [self.items addObject:[data objectAtIndex:i]];
    }
    NSMutableArray *insertIndexPaths = [NSMutableArray arrayWithCapacity:10];
    for (int ind = 0; ind < [data count]; ind++) {
        NSIndexPath    *newPath =  [NSIndexPath indexPathForRow:[self.items indexOfObject:[data objectAtIndex:ind]] inSection:0];
        [insertIndexPaths addObject:newPath];
    }
    
    [self httpcontactPerson];
    [self.ContactTableView.mj_footer endRefreshing];
    
}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource=[NSMutableArray array];
    }
    return _dataSource;
}

-(NSMutableArray *)contactArr{
    if (!_contactArr) {
        _contactArr=[NSMutableArray array];
    }
    return _contactArr;
    
}


@end
