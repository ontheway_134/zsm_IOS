//
//  ICN_NoteModel.h
//  ICan
//
//  Created by shilei on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ICN_NoteModel : JSONModel

@property(nonatomic,assign)NSInteger code;

@property(nonatomic,strong)NSString *info;

@property(nonatomic,strong)NSDictionary *result;

@end
