//
//  ICN_SearcherActivityModel.h
//  ICan
//
//  Created by shilei on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"


@interface ICN_SearcherContent : BaseOptionalModel


@property(nonatomic,strong)NSString *ActivityID;
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *content;
@property(nonatomic,strong)NSString *pic;
@property(nonatomic,strong)NSString *canNum;
@property(nonatomic,strong)NSString *subNum;
@property(nonatomic,strong)NSString *price;
@property(nonatomic,strong)NSString *beginDate;
@property(nonatomic,strong)NSString *endDate;
@property(nonatomic,strong)NSString *createDate;
@property(nonatomic,strong)NSString *address;
@property(nonatomic,strong)NSString *isAdopt;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *activityId;
@property(nonatomic,strong)NSString *memberId;
@property(nonatomic,strong)NSString *memberLogo;
@property(nonatomic,strong)NSString *memberNick;
@property(nonatomic,strong)NSString *companyName;
@property(nonatomic,strong)NSString *memberPosition;
@property(nonatomic,strong)NSString *hasBegin;
@property(nonatomic,strong)NSString *hasBeginId;

@end



@interface ICN_SearcherActivityModel : BaseOptionalModel

@property(nonatomic,strong)NSString *type;
//@property(nonatomic,strong)ICN_SearcherContent *info;


@end
