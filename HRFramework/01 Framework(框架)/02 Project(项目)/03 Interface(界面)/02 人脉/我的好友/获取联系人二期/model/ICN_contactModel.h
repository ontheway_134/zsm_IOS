//
//  ICN_contactModel.h
//  ICan
//
//  Created by shilei on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_contactModel : BaseOptionalModel

@property(nonatomic,strong)NSString *mobile;   //手机号

@property(nonatomic,strong)NSString *status;

@property(nonatomic,strong)NSString *memberId;   //会员的id

@property(nonatomic,strong)NSString *memberSchool;   //会员的学校

@property(nonatomic,strong)NSString *memberMajor;   //会员的专业

@property(nonatomic,strong)NSString *memberLogo;    //会员的logo

@property(nonatomic,strong)NSString *companyName;    //会员的公司

@property(nonatomic,strong)NSString *memberPosition;   //会员的职位

@property(nonatomic,strong)NSString *memberNick;   //会员的昵称


@end
