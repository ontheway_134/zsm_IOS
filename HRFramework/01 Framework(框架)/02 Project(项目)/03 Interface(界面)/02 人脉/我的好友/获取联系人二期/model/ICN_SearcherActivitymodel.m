//
//  ICN_SearcherActivityModel.m
//  ICan
//
//  Created by shilei on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SearcherActivityModel.h"

@implementation ICN_SearcherContent

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ActivityID" : @"id"}];
}

@end


@implementation ICN_SearcherActivityModel

@end
