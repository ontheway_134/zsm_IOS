//
//  ICN_SelsectVIew.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ConfirmBlock)(NSString *frinedId);
typedef void(^CancelBlock)();

@interface ICN_SelsectVIew : UIView
@property (weak, nonatomic) IBOutlet UILabel *titlabel;

@property (weak, nonatomic) IBOutlet UIButton *confrimBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (strong, nonatomic) ConfirmBlock confirmBlock;
@property (strong, nonatomic) CancelBlock cancelBlock;
@property (nonatomic , copy) NSString *friendId;

- (void)confirmClickBlock:(ConfirmBlock)block;
- (void)cancelClickBlock:(CancelBlock)block;

@end
