//
//  ICN_ALBAddressModel.h
//  ICan
//
//  Created by albert on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//  私有Model ， 不需要与外界对接

#import "BaseOptionalModel.h"

@interface ICN_ALBAddressModel : NSObject

@property (nonatomic , copy)NSString *indexStr; // 对应的索引字段
@property (nonatomic , copy)NSString *name; // 联系人名
@property (nonatomic , copy)NSString *headUrl; // 头像（nil 使用默认的i行头像）
@property (nonatomic , copy)NSString *phoneNumber; // 电话号码
@property (nonatomic , copy)NSString *friendStatus; // 好友状态（0 ： 与当前用户无好友关系 ； 1 与当前用户是好友）默认是0
@property (nonatomic , copy)NSString *memberStatus; // 用户状态（0.未注册过i行会员  1. 是i行会员）默认是0
@property (nonatomic , copy)NSString *memberId; // 用户的id
@property (nonatomic , copy)NSString *memberWorkMsg; // 用户的标签相关信息



/** 给私有Model创建实例并赋值的方法 */
+ (instancetype)modelWithIndexStr:(NSString *)indexStr
                             name:(NSString *)name
                          headUrl:(NSString *)headUrl
                     friendStatus:(NSString *)friendStatus
                     memberStatus:(NSString *)memberStatus
                      phoneNumber:(NSString *)phoneNumber;

@end
