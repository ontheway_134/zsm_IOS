//
//  ICN_ALBAddressModel.m
//  ICan
//
//  Created by albert on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ALBAddressModel.h"

@implementation ICN_ALBAddressModel


- (instancetype)initWithIndexStr:(NSString *)indexStr name:(NSString *)name headUrl:(NSString *)headUrl friendStatus:(NSString *)friendStatus memberStatus:(NSString *)memberStatus phoneNumber:(NSString *)phoneNumber{
    if (self = [super init]) {
        _indexStr = indexStr;
        _name = name ? name : @"匿名";
        _headUrl = headUrl;
        _friendStatus = friendStatus ? friendStatus : @"0";
        _memberStatus = memberStatus ? memberStatus : @"0";
        _phoneNumber = phoneNumber;
    }
    return self;
}

+ (instancetype)modelWithIndexStr:(NSString *)indexStr name:(NSString *)name headUrl:(NSString *)headUrl friendStatus:(NSString *)friendStatus memberStatus:(NSString *)memberStatus phoneNumber:(NSString *)phoneNumber{
    return [[super alloc] initWithIndexStr:indexStr name:name headUrl:headUrl friendStatus:friendStatus memberStatus:memberStatus phoneNumber:phoneNumber];
}

@end
