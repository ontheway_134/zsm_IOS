//
//  ICN_ContactViewController.m
//  ICan
//
//  Created by shilei on 17/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ContactViewController.h"
#import "ICN_ConcatTableViewCell.h"
#import "PPPersonModel.h"
#import "PPGetAddressBook.h"
#import "ICN_InterestTableViewCell.h" // Cell头文件
#import "NSString+Regular.h" // 正则校验类目
#import "HandleAddressBook.h" // 获取通信录授权
#import "ICN_ALBAddressModel.h" // 导入联系人信息Model
#import "ICN_NoteModel.h"

#pragma mark - ---------- 私有宏 ----------

#define PRIVATE_IndexStrList @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"⭐️"] // 索引字段默认顺序宏

#pragma mark - ---------- 私有常量 ----------

static NSString *const indexKey = @"indexKey"; // 用户获取索引字段的key值
@interface ICN_ContactViewController ()<UITableViewDataSource,UITableViewDelegate , ICN_BtnSelectedAddDelegate , UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ContactTableView;
@property (nonatomic, copy) NSDictionary *contactPeopleDict;
@property (nonatomic, copy) NSArray *keys;
@property (weak, nonatomic) IBOutlet UITextField *searcherTextfiled;
@property (nonatomic , strong)NSMutableDictionary *addressMdic; // 以联系人索引为键值对存储联系人信息的数组
@property (nonatomic , strong)NSMutableArray *indexStrListMArr; // 用户顺序存储联系人索引位置的数组 - （用户展示索引）
@property (nonatomic , strong)NSMutableDictionary *searchContentsDic; // 搜索内容字典
@property (nonatomic , strong)NSMutableArray *searchIndexMArr; // 搜索内容索引

@end

@implementation ICN_ContactViewController

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)indexStrListMArr{
    if (_indexStrListMArr == nil) {
        _indexStrListMArr = [NSMutableArray arrayWithArray:PRIVATE_IndexStrList];
    }
    return _indexStrListMArr;
}

- (NSMutableArray *)searchIndexMArr{
    if (_searchIndexMArr == nil) {
        _searchIndexMArr = [NSMutableArray array];
    }
    return _searchIndexMArr;
}

- (NSMutableDictionary *)addressMdic{
    if (_addressMdic == nil) {
        _addressMdic = [NSMutableDictionary dictionary];
    }
    return _addressMdic;
}

- (NSMutableDictionary *)searchContentsDic{
    if (_searchContentsDic == nil) {
        _searchContentsDic = [NSMutableDictionary dictionary];
    }
    return _searchContentsDic;
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES];
    // 配置当前页面tableview
    [self configWithCurrentTableView];
    // 获取通讯录信息
    [self handleWithCurrentContactAddressBookList];
    // 设置搜索栏代理
    self.searcherTextfiled.delegate = self;
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - ---------- 私有方法 ----------

#pragma mark --- 网络请求 ---

//获取所有的数据的请求
-(void)httpRequestWithContentMsg{
    
    // 获取必要的联系人数据
    NSString *contactStr = nil;
    for (NSString *key in self.addressMdic) {
        NSArray * items = [self.addressMdic valueForKey:key];
        for (ICN_ALBAddressModel *model in items) {
            contactStr = contactStr ? SF(@"%@,%@",contactStr,model.phoneNumber) : model.phoneNumber;
        }
    }
    
    if (contactStr == nil) {
        // 没有数据不进行网络请求
        return ;
    }
    
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"mobile":contactStr};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CONTACTPERSON params:dic success:^(id result) {
        BaseOptionalModel *optionmodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        
        if (optionmodel.code == 0) {
            //获取的姓名在显示出来
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                //获取的数据
                ICN_contactModel * dataModel =[[ICN_contactModel alloc]initWithDictionary:dic error:nil];
                
                // 根据新获取到的数据刷新数据源
                for (NSString *indexStrKey in self.addressMdic) {
                    NSArray *indexModelsArr = [self.addressMdic valueForKey:indexStrKey];
                    for (ICN_ALBAddressModel *model in indexModelsArr) {
                        if ([model.phoneNumber isEqualToString:dataModel.mobile]) {
                            // 找到的需要的数据源
                            if (dataModel.memberId) {
                                // 该用户在公司注册过
                                model.memberId = dataModel.memberId;
                                model.memberStatus = @"1";
//                                // 设置用户昵称
//                                model.name = dataModel.memberNick ? dataModel.memberNick : model.name;
                                // 设置用户的专业和状态
                                model.memberWorkMsg = SF(@"%@ | %@",dataModel.companyName ? dataModel.companyName : dataModel.memberSchool ? dataModel.memberSchool : @"未知" , dataModel.memberPosition ? dataModel.memberPosition : dataModel.memberMajor ? dataModel.memberMajor : @"未知");
                                // 设置用户头像
                                model.headUrl = dataModel.memberLogo ? dataModel.memberLogo : nil;
                                // 判断用户的好友关系
                                if (dataModel.status != nil && ![dataModel.status isEqualToString:@""]) {
                                    // 如果状态是2则两者是好友
                                    if (dataModel.status.integerValue == 2) {
                                        model.friendStatus = @"1";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            [self.ContactTableView reloadData];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
    
}
#pragma mark --- 配制方法 ---
/** 配置当前页面的tableview */
- (void)configWithCurrentTableView{
    [self.ContactTableView registerNib:[UINib nibWithNibName:@"ICN_ConcatTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ConcatTableViewCell"];
    // 注册通信录Cell
    [self.ContactTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_InterestTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_InterestTableViewCell class])];
    self.ContactTableView.delegate= self;
    self.ContactTableView.dataSource = self;
    self.ContactTableView.backgroundView = [[UIView alloc] init];
    self.ContactTableView.backgroundColor = RGB0X(0xf0f0f0);
    self.ContactTableView.tableFooterView = [[UIView alloc] init];
    // 配置索引颜色
    self.ContactTableView.sectionIndexColor = RGB0X(0x717171);
    // 配置索引的背景色
    self.ContactTableView.sectionIndexBackgroundColor = RGB0X(0xf0f0f0);
}

/** 处理手机通信录信息 
    为了处理多种不同的逻辑在通信录中添加自定义异常数据
    1. 之后电话号码的手机号
    2. 同名手机号
    3. 异常字符手机号
    4. 数字命名手机号
    显示规则：（A-Z顺序索引显示 - 其他全部放在⭐️中）
 */
- (void)handleWithCurrentContactAddressBookList{
    
    // 获取手机通信录授权
    [HandleAddressBook addressBookAuthorization:^(NSMutableArray<PersonInfoModel *> *personInfoArray) {
        
        
        [PPGetAddressBook getOrderAddressBook:^(NSDictionary<NSString *,NSArray *> *addressBookDict, NSArray *nameKeys) {
            //装着所有联系人的字典
            self.contactPeopleDict = addressBookDict;
            //联系人分组按拼音分组的Key值
            self.keys = nameKeys;
            
            // 1. 构建需要的索引数组 - 并将不符合要求的数据从数据源中移除
            //  遍历addressBookDict根据indexStr做对应的处理
            for (NSString *indexStr in addressBookDict) {
                // 如果indexStr 是# 则修改为⭐️
                NSString *indexStrKey = indexStr; // 用户记录索引key值的字段
                NSString *phoneNumber = @""; // 用户保存电话号码的字段
                NSMutableArray *contactItems = [NSMutableArray array]; // 当前索引下的联系人数组
                if ([indexStr isEqualToString:@"#"]) {
                    indexStrKey = @"⭐️";
                }
                // 获取到当前索引下的数据源数组
                NSArray *array = [addressBookDict valueForKey:indexStr];
                // 遍历数组 - 筛选出符合要求的数据
                if (array) {
                    for (PPPersonModel *addressModel in array) {
                        if (addressModel.mobileArray) {
                            phoneNumber = addressModel.mobileArray.firstObject;
                            if ([phoneNumber checkTelephoneNumber]) {
                                // 获取到必要的电话号码信息
                                NSString *name = addressModel.name ? addressModel.name : @"匿名";
                                ICN_ALBAddressModel *model = [ICN_ALBAddressModel modelWithIndexStr:indexStrKey name:name headUrl:nil friendStatus:@"0" memberStatus:@"0" phoneNumber:phoneNumber];
                                [contactItems addObject:model];
                            }else{
                                continue ;
                            }
                        }else{
                            continue;
                        }
                    }
                }else{
                    continue;
                }
                // 获取到必要的结果数组，如果结果数组中有数据则添加到数据源中
                if (contactItems.count > 0) {
                    [self.addressMdic setObject:contactItems forKey:indexStrKey];
                }
            }
            // 获取到处理好的数据源 - 根据结果数据源简历排好序的索引数组
            if (self.addressMdic.allKeys) {
                NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:PRIVATE_IndexStrList];
                // 获取到没有内容的索引字段数组
                [deleteArr removeObjectsInArray:self.addressMdic.allKeys];
                // 获取到正确的排序数组
                [self.indexStrListMArr removeObjectsInArray:deleteArr];
            }else{
                // 如果数据源中没有数据的话，清空排序索引数组
                [self.indexStrListMArr removeAllObjects];
            }
            // 刷新tableview
            [self.ContactTableView reloadData];
            // 进行网络请求获取数据信息
            [self httpRequestWithContentMsg];
        } authorizationFailure:^{
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:@"请在iPhone的“设置-隐私-通讯录”选项中，允许PPAddressBook访问您的通讯录" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancer = [UIAlertAction actionWithTitle:@"知道了" style:UIAlertActionStyleCancel handler:nil];
            [alertController addAction:cancer];
            if (personInfoArray == nil) {
                [self presentViewController:alertController animated:YES completion:nil];
                self.ContactTableView.hidden = YES;
            }
        }];
        
        
        
    }];
}

#pragma mark - --- Protocol ---

#pragma mark --- 搜索代理 ---

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    // 移除搜索数组
    [self.searchIndexMArr removeAllObjects];
    [self.searchContentsDic removeAllObjects];
    [self.ContactTableView reloadData];
    [self.searcherTextfiled resignFirstResponder];
    return YES;
}

#pragma mark ICN_BtnSelectedAddDelegate

/** 点击Cell的相关回调代理 */
-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    if ([cell.addFridentBtn.titleLabel.text isEqualToString:@"邀请"]) {
        //发送短信的接口数据，这里面没有对电话的座机做任何的处理，短信验证码可以成功
        NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"mobile":cell.contactAddressModel.phoneNumber};
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_NOTE params:dic success:^(id result) {
            ICN_NoteModel *note=[[ICN_NoteModel alloc]initWithDictionary:result error:nil];
            if (note.code == 0) {
                //这里面信息发送成功，数据没有做相应的改变
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息发送成功"];
            }
        } failure:^(NSDictionary *errorInfo) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息发送失败"];
        }];
        
    }
    
    if ([cell.addFridentBtn.titleLabel.text isEqualToString:@"加好友"]) {
        NSLog(@"走添加好友的接口");
        NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.contactAddressModel.memberId};
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
            
            BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                [btn setTitle:@"等待验证" forState:UIControlStateNormal];
                [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
                
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
        
        
    }
    
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.searchIndexMArr.count > 0) {
        return self.searchIndexMArr.count;
    }
    return self.indexStrListMArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.searchIndexMArr.count > 0) {
        NSArray *array = [self.searchContentsDic valueForKey:self.searchIndexMArr[section]];
        return array.count;
    }
    
    NSArray *array = [self.addressMdic valueForKey:self.indexStrListMArr[section]];
    return array.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if (self.searchIndexMArr.count > 0) {
        return self.searchIndexMArr[section];
    }
    
    return self.indexStrListMArr[section];
}

//右侧的索引
- (NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView{
    
    if (self.searchIndexMArr.count > 0) {
        return self.searchIndexMArr;
    }
    
    return self.indexStrListMArr;
}

//索引列点击事件

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index

{
    
    
    //点击索引，列表跳转到对应索引的行
    [tableView
     
     scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]
     
     atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    
    return index;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    cell.delegate =self;
    ICN_ALBAddressModel *model ;
    if (self.searchIndexMArr.count > 0) {
        NSArray *array = [self.searchContentsDic valueForKey:self.searchIndexMArr[indexPath.section]];
        model = array[indexPath.row];
    }else{
        NSArray *array = [self.addressMdic valueForKey:self.indexStrListMArr[indexPath.section]];
        model = array[indexPath.row];
    }
    cell.contactAddressModel = model;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}



#pragma mark - --- IBActions ---

- (IBAction)BackBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)click{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能还未开通"];
}

- (IBAction)searchBtn:(id)sender {
//    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能尚未开通"];
//    [self.view endEditing:YES];
    // 判断搜索是否有值
    if (self.searcherTextfiled.text != nil && ![self.searcherTextfiled.text isEqualToString:@""]) {
        // 搜索内容有意义
        if ([self.searchIndexMArr containsObject:self.searcherTextfiled.text]) {
            // 覆盖的是索引内容 - 直接显示索引
            [self.searchIndexMArr removeAllObjects];
            // 添加搜索索引
            [self.searchIndexMArr addObject:self.searcherTextfiled.text];
            // 添加搜索内容
            [self.searchContentsDic removeAllObjects];
            [self.searchContentsDic setValue:[self.addressMdic valueForKey:self.searcherTextfiled.text] forKey:self.searcherTextfiled.text];
        }
        // 使用匹配的方式搜索
        NSMutableArray *resultArr = [NSMutableArray array];
        for (NSString *key in self.addressMdic) {
            NSArray *items = [self.addressMdic valueForKey:key];
            for (NSInteger i = 0; i < items.count; i++) {
                ICN_ALBAddressModel *model = items[i];
                if ([model.name containsString:self.searcherTextfiled.text]) {
                    // 匹配到内容
                    [resultArr addObject:model];
                }
            }
            // 判断结果数组有值则添加索引
            if (resultArr.count > 0) {
                [self.searchIndexMArr addObject:key];
                [self.searchContentsDic setValue:[NSArray arrayWithArray:resultArr] forKey:key];
            }
            //清空结果数组
            [resultArr removeAllObjects];
        }
        
        // 如果搜索有内容则小时，否则显示之前内容
        if (self.searchIndexMArr.count > 0) {
            // 有内容
            // 获取到顺序索引
            NSMutableArray *deleteArr = [NSMutableArray arrayWithArray:PRIVATE_IndexStrList];
            [deleteArr removeObjectsInArray:self.searchIndexMArr];
            self.searchIndexMArr = [NSMutableArray arrayWithArray:PRIVATE_IndexStrList];
            [self.searchIndexMArr removeObjectsInArray:deleteArr];
            // 刷新数据
            [self.ContactTableView reloadData];
        }else{
            // 无内容
            [self.searchIndexMArr removeAllObjects];
            [self.searchContentsDic removeAllObjects];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未搜索到内容"];
        }
    }else{
        // 搜索数据不合法
        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"请输入有效内容"];
        // 移除搜索数组
        [self.searchIndexMArr removeAllObjects];
        [self.searchContentsDic removeAllObjects];
        [self.ContactTableView reloadData];
    }
    // 收回键盘
    [self.searcherTextfiled resignFirstResponder];
}





@end
