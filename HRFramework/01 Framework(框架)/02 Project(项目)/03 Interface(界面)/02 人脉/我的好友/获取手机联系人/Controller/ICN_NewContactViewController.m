//
//  ICN_NewContactViewController.m
//  ICan
//
//  Created by shilei on 17/2/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_NewContactViewController.h"
#import "HandleAddressBook.h"
#import "SortAlphabetically.h"
#import "ICN_InterestTableViewCell.h"
#import "PersonInfoModel.h"

@interface ICN_NewContactViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,ICN_BtnSelectedAddDelegate>
@property (nonatomic, strong) NSMutableArray *dataSource; /**< 数据源*/
@property (nonatomic, strong) NSMutableArray *indexArray; /**< 索引数据源*/
@property (nonatomic, strong) NSMutableDictionary *sortDict; /**< 排序后的数据源*/
@property (nonatomic, strong) NSMutableArray *searchArray; /**< 搜索结果*/

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIView *statueView;
@property (weak, nonatomic) IBOutlet UITableView *contactTableView;


@end

@implementation ICN_NewContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"通讯录";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.contactTableView registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.contactTableView.delegate = self;
    self.contactTableView.dataSource = self;

    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.view addSubview:self.contactTableView];
    [self.view addSubview:self.searchBar];
    [self initDatas];
    
}


#pragma mark - --- Protrocol ---

#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能尚未开通"];

}

#pragma mark -- UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 57;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

#pragma mark -- UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(self.searchBar.text.length > 0) return 1;
    return self.indexArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.searchBar.text.length > 0) return self.searchArray.count;
    return [self.sortDict[self.indexArray[section]] count];
}

// 区头
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(self.searchBar.text.length > 0) return nil;
    
    return self.indexArray[section];
}

// 右侧索引
- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if(self.searchBar.text.length > 0) return nil;
    
    return self.indexArray;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_InterestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if(cell == nil){
        cell = [[ICN_InterestTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ICN_InterestTableViewCell"];
    }
    cell.delegate =self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(self.searchBar.text.length > 0){
        PersonInfoModel *model = self.searchArray[indexPath.row];
        cell.nickNameLabel.text = model.personName;
    }else{
        PersonInfoModel *model =  self.sortDict[self.indexArray[indexPath.section]][indexPath.row];
        cell.nickNameLabel.text = model.personName;
    }
    return cell;
}



#pragma mark -- UISearchBarDelegate

// 开始编辑搜索框
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.navigationController setNavigationBarHidden:YES];
        self.searchBar.frame = CGRectMake(0, 20, SCREEN_WIDTH, 44);
        self.contactTableView.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
        self.searchBar.showsCancelButton = YES;
    }];
    [self.view addSubview:self.statueView];
    self.statueView.hidden = NO;
    
    
    searchBar.showsCancelButton = YES;
    for (id cencelButton in [searchBar.subviews[0] subviews])
    {
        if([cencelButton isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cencelButton;
            [btn setTitle:@"取消"  forState:UIControlStateNormal];
        }
    }
}

// 点击取消按钮
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.navigationController setNavigationBarHidden:NO];
        self.statueView.hidden = YES;
        self.searchBar.frame = CGRectMake(0, 0, SCREEN_WIDTH, 44);
        self.contactTableView.frame = CGRectMake(0, 44, SCREEN_WIDTH, SCREEN_HEIGHT -44);
        self.searchBar.showsCancelButton = NO;
        [self.searchBar resignFirstResponder];
    }];
}

// 搜索框内容变化
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self.searchArray removeAllObjects];
    if(searchText.length == 0){
        [self.contactTableView reloadData];
        return;
    }
    
    
    
    self.searchArray = [[SortAlphabetically shareSortAlphabetically] blurrySearchFromDataArray:[[SortAlphabetically shareSortAlphabetically] fetchAllValuesFromSortDict:self.sortDict] propertyName:@"personName" searchString:searchText];
    if(self.searchArray.count == 0){
        NSLog(@"你所搜索的内容不存在");
    }
    [self.contactTableView reloadData];
}

#pragma mark - --- 自定义方法 ---

- (void)initDatas
{
    [HandleAddressBook addressBookAuthorization:^(NSMutableArray<PersonInfoModel *> *personInfoArray) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            self.dataSource = personInfoArray;
            
            
            
            // key是索引 value是数据源
            self.sortDict = [[SortAlphabetically shareSortAlphabetically] sortAlphabeticallyWithDataArray:self.dataSource propertyName:@"personName"];
            // 字典是无序的 不能直接取key当索引
            self.indexArray = [[SortAlphabetically shareSortAlphabetically] sortAllKeysFromDictKey:self.sortDict.allKeys];
            
            [self.contactTableView reloadData];
        });
    }];
}

#pragma mark - --- 懒加载 ---

- (NSMutableArray *)dataSource{
    if(!_dataSource){
        _dataSource = [[NSMutableArray alloc] init];
    }
    return _dataSource;
}

- (NSMutableArray *)indexArray{
    if(!_indexArray){
        _indexArray = [[NSMutableArray alloc] init];
    }
    return _indexArray;
}

- (NSMutableDictionary *)sortDict{
    if(!_sortDict){
        _sortDict = [[NSMutableDictionary alloc] init];
    }
    return _sortDict;
}

- (NSMutableArray *)searchArray{
    if(!_searchArray){
        _searchArray = [NSMutableArray array];
    }
    return _searchArray;
}

- (UISearchBar *)searchBar{
    if(!_searchBar){
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"搜索";
    
}
    return _searchBar;
}

- (UIView *)statueView{
    if(!_statueView){
        _statueView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
        _statueView.backgroundColor = [UIColor colorWithRed:201/255.0 green:201/255.0 blue:206/255.0 alpha:1];
    }
    return _statueView;
}


@end
