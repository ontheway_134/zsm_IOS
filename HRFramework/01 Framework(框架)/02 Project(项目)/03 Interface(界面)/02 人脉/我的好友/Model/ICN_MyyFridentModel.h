//
//  ICN_MyyFridentModel.h
//  ICan
//
//  Created by shilei on 17/1/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyyFridentModel : BaseOptionalModel

//
//@property(nonatomic,strong)NSString *memberLogo;   //会员头像
//@property(nonatomic,strong)NSString *memberNick;  //会员昵称
//@property(nonatomic,strong)NSString *memberPosition;   //职务
//@property(nonatomic,strong)NSString *memberMajor;


@property(nonatomic,strong)NSString *authStatus;
@property(nonatomic,strong)NSString *companyName;
@property(nonatomic,strong)NSString *friendId;
@property(nonatomic,strong)NSString *memberId;
@property(nonatomic,strong)NSString *memberLogo;
@property(nonatomic,strong)NSString *memberMajor;
@property(nonatomic,strong)NSString *memberMobile;
@property(nonatomic,strong)NSString *memberNick;
@property(nonatomic,strong)NSString *memberPosition;
@property(nonatomic,strong)NSString *memberSchool;
@property(nonatomic,strong)NSString *plusv;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *isMe;

@property(nonatomic,strong)NSString *level;






@end
