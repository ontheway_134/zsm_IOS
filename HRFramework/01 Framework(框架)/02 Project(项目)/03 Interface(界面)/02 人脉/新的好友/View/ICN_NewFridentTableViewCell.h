//
//  ICN_NewFridentTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_NewFrident.h"
#import "ICN_MyGroupModel.h"
#import "ICN_InteresMyGroupModel.h"
#import "ICN_ApplyRecodeModels.h"
#import "ICN_vetityModelss.h"
#import "ICN_DynGrooupListModel.h"
#import "ICN_JoinGroupModel.h"
#import "ICN_bearbymodel.h"   //附近的人的model

@class ICN_NewFridentTableViewCell;
typedef void (^ReturnLocationPhoto)(ICN_NewFridentTableViewCell *cell);//本地
typedef void (^ReturnTakingPhoto)(ICN_NewFridentTableViewCell *cell);   //拍照



@interface ICN_NewFridentTableViewCell : UITableViewCell

@property (nonatomic , strong)ICN_DynGrooupListModel *searchModel;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *comPanyLabel;

@property (weak, nonatomic) IBOutlet UIButton *TureBtn;
@property (weak, nonatomic) IBOutlet UIButton *FalseBtn;

@property(nonatomic,strong)ICN_NewFrident *model;//新的好友的model

@property(nonatomic,strong)ICN_MyGroupModel *mygroupmodel;   //我的小组

@property(nonatomic,strong)ICN_InteresMyGroupModel *InteresModel;   //发现更多小组

@property(nonatomic,strong)ICN_vetityModelss *vetitymodel;

@property(nonatomic,strong)ICN_ApplyRecodeModels *applymodel;

@property (weak, nonatomic) IBOutlet UILabel *introFridentLabel;  //介绍好友

@property (strong,nonatomic)ReturnLocationPhoto locationPhoto;
@property (strong,nonatomic)ReturnTakingPhoto returnTakingPhoto;

-(void)returnLocationPhoto:(ReturnLocationPhoto)block;
-(void)returnTakingPhoto:(ReturnTakingPhoto)block;

@property(nonatomic,strong)ICN_JoinGroupModel *joinmodel;


@property(nonatomic,strong)ICN_bearbymodel *bearbymodel;





@end
