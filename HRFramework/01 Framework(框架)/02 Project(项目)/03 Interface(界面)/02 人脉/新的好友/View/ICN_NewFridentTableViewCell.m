//
//  ICN_NewFridentTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_NewFridentTableViewCell.h"

#define AgreeBGColor RGB0X(0x009dff)  //等待验证也为这个颜色
#define DisAgreeBGColor RGB0X(0x7e94a2)

@interface ICN_NewFridentTableViewCell ()

@property (nonatomic , strong)UILabel *friendStatusLabel; // 好友状态按钮
@property (nonatomic , copy)NSString *currentStatus; // 当前Model当前Cell的status
@property (nonatomic , copy)NSString *groupApplyStatus; // 当前Model的小组申请status
@property (nonatomic , assign , getter=isNewFriend)BOOL judgeFriend; // 当前Model是不是校验新的好友的Model
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;


@end

@implementation ICN_NewFridentTableViewCell

- (UILabel *)friendStatusLabel{
    if (_friendStatusLabel == nil) {
        _friendStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_friendStatusLabel];
        _friendStatusLabel.textAlignment = NSTextAlignmentCenter;
        _friendStatusLabel.hidden = YES;
        _friendStatusLabel.font = [UIFont systemFontOfSize:11.0];
        _friendStatusLabel.userInteractionEnabled = YES;
    }
    return _friendStatusLabel;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headImageView.layer.cornerRadius=17.5;
    self.headImageView.layer.masksToBounds = YES;
    self.levelLabel.layer.cornerRadius = 3.0;
    self.levelLabel.layer.masksToBounds = YES;
}


-(void)returnLocationPhoto:(ReturnLocationPhoto)block{
    self.locationPhoto=block;
}

-(void)returnTakingPhoto:(ReturnTakingPhoto)block{
    self.returnTakingPhoto= block;
}
- (IBAction)locationBtnAction:(UIButton *)sender {
    if (self.locationPhoto) {
        self.locationPhoto(self);
    }
    
}
- (IBAction)takingBtnAction:(UIButton *)sender {
    if (self.returnTakingPhoto) {
        self.returnTakingPhoto(self);
    }
}


//新的好友的model，避免共用model
-(void)setModel:(ICN_NewFrident *)model{

    _model=model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = model.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",model.companyName,model.memberPosition];
    self.comPanyLabel.text=str;
    self.currentStatus = model.status;
    [self setNeedsLayout];
    if (model.companyName == nil) {
        
        NSString *str=[NSString stringWithFormat:@"| %@",model.memberPosition];
        self.comPanyLabel.text=str;
        
    }
    
    if (model.memberPosition == nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | ",model.companyName];
        self.comPanyLabel.text=str;
    }
    if (model.memberPosition ==nil && model.companyName ==nil){
        
        self.comPanyLabel.text=@"";
        
    }
    if (model.memberPosition !=nil && model.companyName !=nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | %@",model.companyName,model.memberPosition];
        self.comPanyLabel.text=str;
    }
    
    
    if ([model.level isEqualToString:@"1"]) {
        self.levelLabel.text = @"1度";
    }
    if ([model.level isEqualToString:@"2"]) {
        self.levelLabel.text = @"2度";
    }
    if ([model.level isEqualToString:@"3"]) {
        self.levelLabel.text = @"3度";
    }
    
    // 默认处理 -- 设置好友关系状态
    self.judgeFriend = YES;
    self.TureBtn.hidden = NO;
    self.FalseBtn.hidden = NO;
    self.friendStatusLabel.hidden = YES;


}



-(void)setApplymodel:(ICN_ApplyRecodeModels *)applymodel{
    _applymodel = applymodel;

    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_applymodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_applymodel.groupName;
    self.introFridentLabel.text=_applymodel.summary;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_applymodel.memberCount];
    //这里判断根据status去判断，具体应该进入到哪个页面。
    self.groupApplyStatus = _applymodel.status;
    

}

-(void)setMygroupmodel:(ICN_MyGroupModel *)mygroupmodel{
    _mygroupmodel = mygroupmodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_mygroupmodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_mygroupmodel.groupName;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_mygroupmodel.number];
    self.introFridentLabel.text =_mygroupmodel.summary;
    self.currentStatus = _mygroupmodel.status;
    
}

-(void)setInteresModel:(ICN_InteresMyGroupModel *)InteresModel{
    _InteresModel = InteresModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_InteresModel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_InteresModel.groupName;
    
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_InteresModel.number];
    self.introFridentLabel.text=_InteresModel.summary;
    self.currentStatus = _InteresModel.status;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 先判断是不是新的好友页面的Cell
    if (self.judgeFriend) {
        // 是新的好友页面 不做任何处理
    }else{
        // 不是新的好友页面的Cell
        // 判断在小组申请状态存在的时候不调用当前状态判断
        if (self.groupApplyStatus) {
            [self changeFriendStatsuLabelWithGroupApplyStatus:self.groupApplyStatus];
        }else{
            if (self.currentStatus) {
                [self changeFriendStatusLabelWithStatus:self.currentStatus];
            }
        }
    }
}

#pragma mark --- 私有方法 ---

// 根小组申请记录的状态来修改标签
- (void)changeFriendStatsuLabelWithGroupApplyStatus:(NSString *)statsu{
    // "status":   状态：0正常(即已审核)，1离开  2已申请(即还未审核)
    self.friendStatusLabel.hidden = NO;
    [self.friendStatusLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(57, 20));
        make.centerY.equalTo(_comPanyLabel);
    }];
    switch (statsu.integerValue) {
        case 0:{
            // 已同意
            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"已同意"];
            break ;
        }
        case 1:{
            // 已退群
            [self changeFriendsStatusLabelWithColor:DisAgreeBGColor Text:@"已退群"];
            break;
        }
        case 2:{
            // 待审核
            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"待审核"];
            break ;
        }
        default:{
            // 默认处理
            self.TureBtn.hidden = NO;
            self.FalseBtn.hidden = NO;
            self.friendStatusLabel.hidden = YES;
            break;
        }
    }
    // 状态归零
    self.groupApplyStatus = nil;
}

- (void)changeFriendStatusLabelWithStatus:(NSString *)status{
//    1:未审核(memberId为申请者)  2:已审核   3:被拒绝  4:已删除  5:待审核(memberId为被申请者) 6:已拒绝
    self.friendStatusLabel.hidden = NO;
    [self.friendStatusLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(57, 20));
        make.centerY.equalTo(_comPanyLabel);
    }];
    switch (status.integerValue) {
        case 2:{
            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"已同意"];
        }
            break;
            
            
        case 5:{
            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"待审核"];
        }
            break;

//        这里的4和1的状态是根据附近的人来判断的
//        case 4:{
//            [self changeFriendsStatusLabelWithColor:DisAgreeBGColor Text:@"加好友"];
//            break;
//        }
//        case 1:{
//            [self changeFriendsStatusLabelWithColor:DisAgreeBGColor Text:@"等待验证"];
//            break;
//        }
//        case 5:{
//            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"等待验证"];
//            break;
//        }
        case 0:{
            
          [self changeFriendsStatusLabelWithColor:[UIColor whiteColor] Text:@""];
        }
            break;
       
        default:{
            // 默认处理
            self.TureBtn.hidden = NO;
            self.FalseBtn.hidden = NO;
            self.friendStatusLabel.hidden = YES;
            
        }
            break;
    }
    self.currentStatus = nil;
}

- (void)setSearchModel:(ICN_DynGrooupListModel *)searchModel{
    _searchModel = searchModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_searchModel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = _searchModel.groupName;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_searchModel.number];
    
    self.introFridentLabel.text = _searchModel.summary;

    [self setNeedsLayout];
    
}


- (void)changeFriendsStatusLabelWithColor:(UIColor *)color Text:(NSString *)text{
    // 清空之前的选项按钮
    self.TureBtn.hidden = YES;
    self.FalseBtn.hidden = YES;
    self.friendStatusLabel.text = text;
    self.friendStatusLabel.textColor = color;
    self.friendStatusLabel.layer.borderColor = color.CGColor;
    self.friendStatusLabel.layer.borderWidth = 1.0;
    self.friendStatusLabel.layer.cornerRadius = 2.0;
}

//我的小组加入新加入的组员的验证消息
-(void)setVetitymodel:(ICN_vetityModelss *)vetitymodel{
    _vetitymodel= vetitymodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_vetitymodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_vetitymodel.memberNick;
    NSString * headerData = _vetitymodel.content;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@" "];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    // 设置公司职位等信息内容
    NSString *companyContent = SF(@"%@|%@",vetitymodel.companyName ? vetitymodel.companyName : vetitymodel.memberSchool ? vetitymodel.memberSchool : @"无内容" , vetitymodel.memberPosition ? vetitymodel.memberPosition : @"无内容");
    self.comPanyLabel.text = companyContent;
    self.currentStatus = _searchModel.status;
    
    // 设置申请记录的文字内容
    self.introFridentLabel.text = _vetitymodel.content ? headerData : @"申请加入你的小组";

}

//参加的小组的小组的model
-(void)setJoinmodel:(ICN_JoinGroupModel *)joinmodel{
    _joinmodel = joinmodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_joinmodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_joinmodel.groupName;
    
    
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_joinmodel.number];
  
    
    self.introFridentLabel.text =_joinmodel.summary;

}


//附近的人的model  不再使用

-(void)setBearbymodel:(ICN_bearbymodel *)bearbymodel{
    _bearbymodel = bearbymodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(bearbymodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=bearbymodel.memberNick;
    
    NSString *str=[NSString stringWithFormat:@"%@|%@",bearbymodel.companyName,bearbymodel.memberPosition];
    
    self.comPanyLabel.text=str;
    self.introFridentLabel.text =[NSString stringWithFormat:@"距您%@m",bearbymodel.distance];
//    self.currentStatus = bearbymodel.status;
}

@end
