//
//  ICN_bearbymodel.h
//  ICan
//
//  Created by shilei on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_bearbymodel : BaseOptionalModel

@property(nonatomic,strong)NSString *memberId;
@property(nonatomic,strong)NSString *memberLogo;
@property(nonatomic,strong)NSString *memberNick;
@property(nonatomic,strong)NSString *companyName;
@property(nonatomic,strong)NSString *memberPosition;
@property(nonatomic,strong)NSString *memberSchool;
@property(nonatomic,strong)NSString *memberMajor;
@property(nonatomic,strong)NSString *distance;
@property(nonatomic,strong)NSString *status ; // 状态表示
@property(nonatomic,strong)NSString *level ;

@end
