//
//  ICN_bearbyViewController.m
//  ICan
//
//  Created by shilei on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_bearbyViewController.h"
#import "ICN_bearbyTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKMapView.h>
#import "ICN_bearbymodel.h"
#import "BaseOptionalModel.h"
#import "ICN_UserHomePagerVC.h"   //用户的个人主页

@interface ICN_bearbyViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,ICN_BtnSelectedAddBearDelegate>{
    
    MKMapView           *mapView;
    CLGeocoder          *geocoder;
    CLLocationManager   *locationManager;
}

@property (weak, nonatomic) IBOutlet UITableView *bearbyTableView;
@property(nonatomic,strong)NSMutableArray *bearbyArr;

@property(nonatomic,strong)CLLocationManager *locationmanager;
@property(nonatomic,assign)NSInteger page;   //页数

@end

@implementation ICN_bearbyViewController

#pragma mark - --- 网络请求 ---

-(void)httpNetworingLat:(CGFloat)lat andLng:(CGFloat)lng andPage:(NSInteger)page{
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"lat":[NSString stringWithFormat:@"%f",lat],@"lng":[NSString stringWithFormat:@"%f",lng],@"page":[NSString stringWithFormat:@"%ld",(long)page],@"pagenum":@10};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_BEARBYPENPRER params:dic success:^(id result) {
        
        BaseOptionalModel *optionmodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        
        //页数为1时，移除所有的对象
        if (self.page == 1) {
            [self.bearbyArr removeAllObjects];
        }
        
        if (optionmodel.code == 0) {
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_bearbymodel *model=[[ICN_bearbymodel alloc]initWithDictionary:dic error:nil];
                [self.bearbyArr addObject:model];
            }
            
        }else{
            //            [MBProgressHUD ShowProgressToSuperView:self.view Message:@""];
            if (self.page > 1 && [optionmodel.info isEqualToString:@"数据为空"]) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"没有更多"];
            }else{
                NSString *info = optionmodel.info ? optionmodel.info : @"";
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }
            
        }
        [self.bearbyTableView reloadData];
        [self.bearbyTableView.mj_header endRefreshing];
        [self.bearbyTableView.mj_footer endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
        [self.bearbyTableView.mj_header endRefreshing];
        [self.bearbyTableView.mj_footer endRefreshing];
    }];
    
    
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    
    [self.bearbyTableView registerNib:[UINib nibWithNibName:@"ICN_bearbyTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_bearbyTableViewCell"];
    self.bearbyTableView.delegate = self;
    self.bearbyTableView.dataSource = self;
    self.bearbyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self startLocation];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- Protocol ---

#pragma mark ICN_BtnSelectedAddBearDelegate

-(void)ICN_BtnSelectedAddBearDelegate:(UIButton *)btn and:(ICN_bearbyTableViewCell *)cell{
    
    //添加好友的接口
    NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.model.memberId};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
        
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [btn setTitle:@"等待验证" forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  self.bearbyArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_bearbyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_bearbyTableViewCell"];
    if (cell == nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_bearbyTableViewCell" owner:self options:nil] lastObject];
    }
    cell.delegate =self;
    cell.model = self.bearbyArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
    ICN_bearbyTableViewCell *cell= (ICN_bearbyTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    userhome.memberId=cell.model.memberId;
    [self.navigationController pushViewController:userhome animated:YES];
    
    
}


#pragma mark - --- IBActions ---

- (IBAction)backBtnActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//清除坐标并且退出
- (IBAction)clearBtnActions:(id)sender {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"提示" message:@"清除位置并退出" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *sureAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSDictionary *dic = @{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CLEARBEARBY params:dic success:^(id result) {
            BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
            if (basemodel.code == 0) {
                
                NSString *str= [[result valueForKey:@"result"] valueForKey:@"scalar"];
                if (str !=nil) {
                    // 提示清除位置
                    [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"清除成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
            }
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
    }];
    UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [controller addAction:sureAction];
    [controller addAction:cancerAction];
    [self presentViewController:controller animated:YES completion:nil];
    
    
}

#pragma mark - ---------- Private Method ----------

- (void)startLocation {
    geocoder = [[CLGeocoder alloc]init];
    locationManager = [[CLLocationManager alloc]init];
    if(![CLLocationManager locationServicesEnabled]||[CLLocationManager authorizationStatus]!= kCLAuthorizationStatusAuthorizedWhenInUse){
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = 100;
    [locationManager requestAlwaysAuthorization];//添加这句
    [locationManager startUpdatingLocation];
}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)bearbyArr{
    if (_bearbyArr == nil) {
        _bearbyArr = [NSMutableArray array];
    }
    return _bearbyArr;
}

#pragma mark CoreLocation

-(void)getAddressByLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude{
    CLLocation *location=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
    }];
}

//跟踪定位代理方法，每次位置发生变化即会执行（只要定位到相应位置）
/**通过经纬度实施定位*/
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *apple_currLocation = [locations lastObject];
    // 生成当前页面的百度经纬度
    CLLocationCoordinate2D apple_location2D = CLLocationCoordinate2DMake(apple_currLocation.coordinate.latitude, apple_currLocation.coordinate.longitude);
    //转换 google地图、soso地图、aliyun地图、mapabc地图和amap地图所用坐标至百度坐标
    NSDictionary* testdic = BMKConvertBaiduCoorFrom(apple_location2D,BMK_COORDTYPE_COMMON);
    //转换GPS坐标至百度坐标
    testdic = BMKConvertBaiduCoorFrom(apple_location2D,BMK_COORDTYPE_GPS);
    CLLocationCoordinate2D currentLocationCoordinate2D =BMKCoorDictionaryDecode(testdic);
    // 打印获取转码后的经纬度
    NSLog(@"=========%f,%f",currentLocationCoordinate2D.latitude,currentLocationCoordinate2D.longitude);
    //warn？？？ 不知道这一步是干嘛的
    [self getAddressByLatitude:apple_currLocation.coordinate.latitude longitude:apple_currLocation.coordinate.longitude];
    
    [locationManager stopUpdatingLocation];
    
    if (self.bearbyTableView.mj_header.isRefreshing) {
        NSLog(@"dd");
    }else{
        
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            self.page = 1;
            // 输出获取到的百度经纬度
            [self httpNetworingLat:currentLocationCoordinate2D.latitude andLng:currentLocationCoordinate2D.longitude andPage:self.page];
            
        }];
        header.stateLabel.hidden = YES;
        header.lastUpdatedTimeLabel.hidden = YES;
        
        [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
        NSMutableArray *imagelist = [NSMutableArray array];
        for (NSInteger i = 1; i <= 35; i++) {
            [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
        }
        [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
        [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
        [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
        [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
        self.bearbyTableView.mj_header = header;
//        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
//            self.page+=1;
//            [self httpNetworingLat:currentLocationCoordinate2D.latitude andLng:currentLocationCoordinate2D.longitude andPage:self.page];
//            
//        }];
//        // 设置footer的标识 MJRefreshAutoNormalFooter
//        [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
//        [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
//        [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
//        [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
//        self.bearbyTableView.mj_footer = footer;
        [self.bearbyTableView.mj_header beginRefreshing];
        
    }
    
    
}


@end
