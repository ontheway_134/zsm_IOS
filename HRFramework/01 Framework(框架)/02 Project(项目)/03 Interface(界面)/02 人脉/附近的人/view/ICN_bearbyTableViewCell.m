//
//  ICN_bearbyTableViewCell.m
//  ICan
//
//  Created by shilei on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_bearbyTableViewCell.h"


@interface ICN_bearbyTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *membeelogoimageview;
@property (weak, nonatomic) IBOutlet UILabel *nickLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *cpmpanylabel;
@property (weak, nonatomic) IBOutlet UILabel *distacelabel;

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@end

@implementation ICN_bearbyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.membeelogoimageview.layer.cornerRadius = 17.5;
    self.membeelogoimageview.layer.masksToBounds = YES;
    self.levelLabel.layer.cornerRadius = 3;
    self.levelLabel.layer.masksToBounds = YES;
}
- (IBAction)addBrnAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(ICN_BtnSelectedAddBearDelegate:and:)]) {
        [self.delegate ICN_BtnSelectedAddBearDelegate:sender and:self];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(ICN_bearbymodel *)model{
    
    
    _model = model;
    
#warning 这里需要判断是否显示添加好友的按钮用model.status判断状态
    
    [self.membeelogoimageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickLabel.text=model.memberNick;
    
    NSString *str= @"";
    if (model.companyName == nil) {
        // 设置学校和专业
        if (model.memberSchool == nil) {
            // 设置全未选择
            str = @"未选择|未选择";
        }else{
            str = [NSString stringWithFormat:@"%@|%@",model.memberSchool,model.memberMajor];
        }
    }else{
        str = [NSString stringWithFormat:@"%@|%@",model.companyName,model.memberPosition];
    }
    self.cpmpanylabel.text=str;
    // 根据具体的距离处理字段
    /** 
     1. 小于100米显示精确到米
     2. 大于100米 小于1000米显示精确到百米
     3. 大于1000米显示精确到千米（两位小数点）
     */
    CGFloat distanceKM = [model.distance floatValue];
    if (distanceKM < 1.0) {
        // 小于1千米
        if (distanceKM > 0.1) {
            // 大于100 米
            NSInteger distanceHM = (distanceKM * 1000) / 100;
            self.distacelabel.text =[NSString stringWithFormat:@"距您%d00m",distanceHM];
        }else{
            // 小于100米显示精确到米
            NSInteger distanceM = (distanceKM * 1000) / 1;
            self.distacelabel.text =[NSString stringWithFormat:@"距您%dm",distanceM];
        }
    }else{
        self.distacelabel.text =[NSString stringWithFormat:@"距您%.2fkm",distanceKM];
    }
    if ([model.level isEqualToString:@"1"]) {
        self.levelLabel.text = @"1度";
        // 设置加好友的按钮小时
        self.addBtn.hidden = YES;
    }else{
        self.addBtn.hidden = NO;
    }
    if ([model.level isEqualToString:@"2"]) {
        self.levelLabel.text = @"2度";
    }
    if ([model.level isEqualToString:@"3"]) {
        self.levelLabel.text = @"3度";
    }
    if ([model.status isEqualToString:@"1"]) {
        [self.addBtn setTitle:@"等待验证" forState:UIControlStateNormal];
        [self.addBtn setBackgroundImage:nil forState:UIControlStateNormal];
    }
    if ([model.status isEqualToString:@"4"]) {
        NSLog(@"4的状态为添加好友");
    }

}

@end
