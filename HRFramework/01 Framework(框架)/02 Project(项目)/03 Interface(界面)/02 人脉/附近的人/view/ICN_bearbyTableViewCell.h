//
//  ICN_bearbyTableViewCell.h
//  ICan
//
//  Created by shilei on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_bearbymodel.h"
@class ICN_bearbyTableViewCell;

@protocol ICN_BtnSelectedAddBearDelegate <NSObject>

-(void)ICN_BtnSelectedAddBearDelegate:(UIButton *)btn and:(ICN_bearbyTableViewCell *)cell;

@end

@interface ICN_bearbyTableViewCell : UITableViewCell

@property(nonatomic,copy)ICN_bearbymodel *model;
@property(nonatomic,weak)id<ICN_BtnSelectedAddBearDelegate>delegate;

@end
