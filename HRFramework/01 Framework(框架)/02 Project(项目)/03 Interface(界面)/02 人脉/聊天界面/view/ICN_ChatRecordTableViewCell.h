//
//  ICN_ChatRecordTableViewCell.h
//  ICan
//
//  Created by zjk on 2017/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ChatRecordTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *headImageView;
@property (strong, nonatomic) IBOutlet UIButton *headImageBtn;
@property (strong, nonatomic) IBOutlet UILabel *nickLab;
@property (strong, nonatomic) IBOutlet UILabel *newsLab;
@property (strong, nonatomic) IBOutlet UILabel *dateLab;


@end
