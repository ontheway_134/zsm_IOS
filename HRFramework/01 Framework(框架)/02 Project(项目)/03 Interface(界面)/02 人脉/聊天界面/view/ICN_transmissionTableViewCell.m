//
//  ICN_transmissionTableViewCell.m
//  ICan
//
//  Created by shilei on 17/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_transmissionTableViewCell.h"

@implementation ICN_transmissionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/**］
 属性合成

 @param model 聊天model
 */
- (void)setModel:(id<IMessageModel>)model
{
    [super setModel:model];
    if (model.avatarURLPath) {
        [self.headImageView sd_setImageWithURL:[NSURL URLWithString:model.avatarURLPath] placeholderImage:model.avatarImage];
    } else {
        self.headImageView.image = model.avatarImage;
    }
    self.titleLabel.text = model.nickname;
    self.contentLabel.text = @"hahahhahah";
}

@end
