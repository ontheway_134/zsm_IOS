//
//  ICN_transmissionTableViewCell.h
//  ICan
//
//  Created by shilei on 17/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMessageModel.h"
@interface ICN_transmissionTableViewCell : EaseBaseMessageCell<IModelChatCell>
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (strong, nonatomic) id<IMessageModel> model;


@end
