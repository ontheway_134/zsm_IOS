

#import <Foundation/Foundation.h>

//#import "MainViewController.h"
#import "ChatViewController.h"

#define kHaveUnreadAtMessage    @"kHaveAtMessage"
#define kAtYouMessage           1
#define kAtAllMessage           2

typedef void (^HX_Success)(id data);
typedef void (^HX_Failure)(id data);

typedef enum : NSUInteger {
    NOLogin = 0,//没有登录
    LoginSuccess,//登录成功
    LoginFailure,//登陆失败
    KickedOut//被踢出
    
} HY_LoginType;

@interface ChatDemoHelper : NSObject <EMClientDelegate,EMChatManagerDelegate,EMContactManagerDelegate,EMGroupManagerDelegate,EMChatroomManagerDelegate>

@property (assign, nonatomic) HY_LoginType loginType;

@property (nonatomic, weak) ChatViewController *chatVC;

@property (assign, nonatomic) NSInteger allUnReadCount;

+ (instancetype)shareHelper;


/**
 登陆环信
 
 @param userName 账号
 @param password 密码
 @param success  成功
 @param failure  失败
 */
- (void)loginHyphenateWithUserName:(NSString *)userName password:(NSString *)password success:(HX_Success)success failure:(HX_Failure)failure;

/** 登出项目
 
 remove ： 是否解绑token
 
 */
- (void)HXLogoutWithDeviceToken:(BOOL)remove;

/**
 创建群组
 
 @param name        群名称
 @param description 群描述
 @param invitees    群成员
 @param success     成功(返回群组ID)
 @param failure     失败
 */
- (void)createGroupOptionsWithName:(NSString *)name description:(NSString *)description invitees:(NSArray *)invitees success:(HX_Success)success failure:(HX_Failure)failure;


/**
 添加好友
 
 @param contact 好友id
 @param message 添加附带信息
 @param success 成功
 @param failure 失败
 */
- (void)addContact:(NSString *)contact message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure;

/**
 同意添加好友

 @param contact         被同意人的id
 @param success         成功
 @param failure         失败
 */
- (void)acceptInvitation:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure;

/**
 拒绝添加好友

 @param contact 被拒绝人的id
 @param success 成功
 @param failure 失败
 */
- (void)declineInvitation:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure;

/**
 删除好友

 @param contact 被删除人id
 @param success 成功
 @param failure 失败
 */
- (void)deleteContact:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure;

/**
 邀请进群
 
 @param occupants 被邀请人id
 @param group     群id
 @param message   附带信息
 @param success   成功
 @param failure   失败
 */
- (void)addOccupants:(NSArray *)occupants toGroup:(NSString *)group message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure;

/**
 加入群组

 @param groupId 群组id
 @param message 附带信息
 @param success 成功
 @param failure 失败
 */
- (void)JoinPublicGroup:(NSString *)groupId message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure;

/**
 批准入群申请

 @param groupId  群组id
 @param username 申请人
 @param success  成功
 @param failure  失败
 */
- (void)acceptJoin:(NSString *)groupId
          username:(NSString *)username success:(HX_Success)success failure:(HX_Failure)failure;

/**
 拒绝入群申请

 @param groupId  群组id
 @param username 申请人
 @param reason   附带信息
 @param success  成功
 @param failure  失败
 */
- (void)declineJoinGroup:(NSString *)groupId userName:(NSString *)username reason:(NSString *)reason success:(HX_Success)success failure:(HX_Failure)failure;

/**
 离开群组

 @param groupId 群组id
 @param success 成功
 @param failure 失败
 */
- (void)leaveGroup:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure;

/**
 解散群组
 
 @param groupId 群组id
 @param success 成功
 @param failure 失败
 */
- (void)destroyGroup:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure;

/**
 移除群组（管理员调用）

 @param occupants 被移除人
 @param groupId   群组id
 @param success   成功
 @param failure   失败
 */
- (void)removeOccupants:(NSArray *)occupants groupId:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure;


/**
 退出登录

 @param success 成功
 @param failure 失败
 */
- (void)signOutSuccess:(HX_Success)success failure:(HX_Failure)failure;

/**
 *  获取数据库中所有会话未读数量
 */
- (void)getConversationReadCount:(void(^)(NSInteger allUnReadCount))ReadCount;

/**
 获取好友列表
 
 @return 好友列表
 */
- (NSArray *)getContactsList;

/**
 获取黑名单
 
 @return 黑名单列表
 */
- (NSArray *)getBlackList;

/**
 获取关于自己的群组
 
 @return 群组列表
 */
- (NSArray *)getMyGroups;


@end
