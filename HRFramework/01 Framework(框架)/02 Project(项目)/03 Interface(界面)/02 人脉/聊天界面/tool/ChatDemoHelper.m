

#import "ChatDemoHelper.h"
#import "ICN_huanxinModel.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_MyPersonalHeaderModel.h"

#import "AppDelegate.h"
//#import "ApplyViewController.h"
#import "MBProgressHUD.h"

#import "EaseSDKHelper.h"

#ifdef REDPACKET_AVALABLE
#import "RedpacketOpenConst.h"
#import "RedPacketUserConfig.h"
#endif

#if DEMO_CALL == 1
#import "DemoCallManager.h"
#endif

static ChatDemoHelper *helper = nil;

@implementation ChatDemoHelper

+ (instancetype)shareHelper
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[ChatDemoHelper alloc] init];
    });
    return helper;
}

- (void)dealloc
{
    [[EMClient sharedClient] removeDelegate:self];
    [[EMClient sharedClient].groupManager removeDelegate:self];
    [[EMClient sharedClient].contactManager removeDelegate:self];
    [[EMClient sharedClient].roomManager removeDelegate:self];
    [[EMClient sharedClient].chatManager removeDelegate:self];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initHelper];
    }
    return self;
}

#pragma mark - init

- (void)initHelper
{
    
    [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].groupManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].contactManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].roomManager addDelegate:self delegateQueue:nil];
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
}

- (void)loginHyphenateWithUserName:(NSString *)userName password:(NSString *)password success:(HX_Success)success failure:(HX_Failure)failure {
    
    // 判断 环信是否自动登录的方法 -- 测试阶段自动登录之后不注册新用户
    BOOL isAutoLogin = [EMClient sharedClient].options.isAutoLogin;
    if (!isAutoLogin) {
        [[EMClient sharedClient] loginWithUsername:userName
                                          password:password
                                        completion:^(NSString *aUsername, EMError *aError) {
                                            if (!aError) {
                                                NSLog(@"登陆成功");
                                                self.loginType = LoginSuccess;
                                                if (success) {
                                                    success(@"登陆成功");
                                                }
                                            } else {
                                                NSLog(@"登陆失败");
                                                self.loginType = LoginFailure;
                                                if (failure) {
                                                    failure(@"登陆失败");
                                                }
                                            }
                                        }];
    }
    
    
}

/** 环信账号的主动登出 - 是否解绑token */
- (void)HXLogoutWithDeviceToken:(BOOL)remove{
    EMError *error = [[EMClient sharedClient] logout:remove];
    if (!error) {
        NSLog(@"退出成功");
    }else{
        NSLog(@"错误原因 == %@",error.errorDescription);
    }
}


- (void)createGroupOptionsWithName:(NSString *)name description:(NSString *)description invitees:(NSArray *)invitees success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = nil;
    EMGroupOptions *setting = [[EMGroupOptions alloc] init];
    setting.maxUsersCount = 500;
    setting.style = EMGroupStylePublicJoinNeedApproval;// 创建不同类型的群组，这里需要才传入不同的类型
    EMGroup *group = [[EMClient sharedClient].groupManager createGroupWithSubject:name description:description invitees:invitees message:@"邀请您加入群组" setting:setting error:&error];
    if(!error){
        if (success) {
            success(group.groupId);
        }
        NSLog(@"创建成功 -- %@",group);
    } else {
        if (failure) {
            failure(@"创建失败");
        }
    }
}

- (void)addContact:(NSString *)contact message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient].contactManager addContact:contact message:message];
    if (!error) {
        NSLog(@"添加成功");
        if (success) {
            success(@"添加成功");
        }
    } else {
        if (failure) {
            failure(@"添加失败");
        }
    }
}

- (void)acceptInvitation:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient].contactManager acceptInvitationForUsername:contact];
    if (!error) {
        NSLog(@"发送同意成功");
        if (success) {
            success(@"同意添加成功");
        }
    } else {
        if (failure) {
            failure(@"同意添加失败");
        }
    }
}

- (void)declineInvitation:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient].contactManager declineInvitationForUsername:contact];
    if (!error) {
        NSLog(@"发送拒绝成功");
        if (success) {
            success(@"拒绝成功");
        }
    } else {
        if (failure) {
            failure(@"拒绝失败");
        }
    }
}

- (void)deleteContact:(NSString *)contact success:(HX_Success)success failure:(HX_Failure)failure {
    // 删除好友
    EMError *error = [[EMClient sharedClient].contactManager deleteContact:contact];
    if (!error) {
        NSLog(@"删除成功");
        if (success) {
            success(@"删除成功");
        }
    } else {
        if (failure) {
            failure(@"删除失败");
        }
    }
}

- (void)addOccupants:(NSArray *)occupants toGroup:(NSString *)group message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = nil;
    [[EMClient sharedClient].groupManager addOccupants:@[@"user1"] toGroup:@"groupId" welcomeMessage:@"message" error:&error];
    if (!error) {
        if (success) {
            success(@"邀请进群成功");
        }
    } else {
        if (failure) {
            failure(@"邀请进群失败");
        }
    }
}

- (void)JoinPublicGroup:(NSString *)groupId message:(NSString *)message success:(HX_Success)success failure:(HX_Failure)failure {
    // 申请加入需要审核的公开群组
    EMError *error = nil;
    [[EMClient sharedClient].groupManager applyJoinPublicGroup:groupId message:message error:&error];
    if (!error) {
        if (success) {
            success(@"申请成功");
        }
    } else {
        if (failure) {
            failure(@"申请失败");
        }
    }
}

- (void)acceptJoin:(NSString *)groupId
          username:(NSString *)username success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient].groupManager acceptJoinApplication:groupId applicant:username];
    if (!error) {
        if (success) {
            success(@"批准成功");
        }
    } else {
        if (failure) {
            failure(@"批准失败");
        }
    }
}

- (void)declineJoinGroup:(NSString *)groupId userName:(NSString *)username reason:(NSString *)reason success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient].groupManager declineJoinApplication:groupId applicant:username reason:reason];
    if (!error) {
        if (success) {
            success(@"拒绝成功");
        }
    } else {
        if (failure) {
            failure(@"拒绝失败");
        }
    }
}

- (void)leaveGroup:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = nil;
    [[EMClient sharedClient].groupManager leaveGroup:@"1410329312753" error:&error];
    if (!error) {
        if (success) {
            success(@"退出成功");
        }
    } else {
        if (failure) {
            failure(@"退出失败");
        }
    }
}

- (void)destroyGroup:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = nil;
    [[EMClient sharedClient].groupManager destroyGroup:@"groupId" error:&error];
    if (!error) {
        NSLog(@"解散成功");
        if (success) {
            success(@"解散成功");
        }
    } else {
        if (failure) {
            failure(@"解散失败");
        }
    }
}

- (void)removeOccupants:(NSArray *)occupants groupId:(NSString *)groupId success:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = nil;
    [[EMClient sharedClient].groupManager removeOccupants:occupants fromGroup:groupId error:&error];
    if (!error) {
        NSLog(@"成功");
        if (success) {
            success(@"移除成功");
        }
    } else {
        if (failure) {
            failure(@"移除失败");
        }
    }
}

- (void)signOutSuccess:(HX_Success)success failure:(HX_Failure)failure {
    EMError *error = [[EMClient sharedClient] logout:YES];
    if (!error) {
        if (success) {
            success(@"登陆成功");
        }
    } else {
        if (failure) {
            failure(@"移除失败");
        }
    }
}

/**
 *  获取数据库中所有会话(未定)
 */
- (void)getConversationReadCount:(void(^)(NSInteger allUnReadCount))ReadCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    [conversations enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        EMConversation * conversation = obj;
        if (![conversation.conversationId isEqualToString:@"screenshot"]) {
            self.allUnReadCount = self.allUnReadCount + conversation.unreadMessagesCount;
        }
        if (stop) {
            if (ReadCount) {
                ReadCount(self.allUnReadCount);
            }
        }
    }];
}

- (void)getConversation {
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
}

- (NSArray *)getContactsList {
    EMError *error = nil;
    NSArray *userlist = [[EMClient sharedClient].contactManager getContactsFromServerWithError:&error];
    if (!error) {
        NSLog(@"获取成功 -- %@",userlist);
        return userlist;
    } else {
        return [[EMClient sharedClient].contactManager getContacts];
    }
}

- (NSArray *)getBlackList {
    EMError *error = nil;
    NSArray *blacklist = [[EMClient sharedClient].contactManager getBlackListFromServerWithError:&error];
    if (!error) {
        NSLog(@"获取成功 -- %@",blacklist);
        return blacklist;
    } else {
        return [[EMClient sharedClient].contactManager getBlackList];
    }
}

- (NSArray *)getMyGroups {
    NSArray *groupList = [[EMClient sharedClient].groupManager getJoinedGroups];
    if (groupList) {
        return groupList;
    }
    EMError *error = nil;
    NSArray *myGroups = [[EMClient sharedClient].groupManager getMyGroupsFromServerWithError:&error];
    if (!error) {
        NSLog(@"获取成功 -- %@",myGroups);
        return myGroups;
    }
    return groupList;
}

#pragma mark - EMClientDelegate

// 网络状态变化回调
- (void)didConnectionStateChanged:(EMConnectionState)connectionState
{
    //    [self.mainVC networkChanged:connectionState];
}

- (void)autoLoginDidCompleteWithError:(EMError *)error
{
#warning 自动登录失败
    self.loginType = LoginFailure;
}

#pragma mark - 当前登录账号在其它设备登录
- (void)userAccountDidLoginFromOtherDevice
{
    [[NSNotificationCenter defaultCenter] postNotificationName:HR_USERLOGINOUT_NOTIFICATION object:nil];
}

- (void)userAccountDidRemoveFromServer
{
    
}

- (void)userDidForbidByServer
{
    
}

#pragma mark - EMChatManagerDelegate

- (void)conversationListDidUpdate:(NSArray *)aConversationList {
    
}

- (void)messagesDidReceive:(NSArray *)aMessages {
    for (EMMessage *message in aMessages) {
        EMMessageBody *msgBody = message.body;
        switch (msgBody.type) {
            case EMMessageBodyTypeText:
            {
                // 收到的文字消息
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *txt = textBody.text;
                NSLog(@"收到的文字是 txt -- %@",txt);
            }
                break;
            case EMMessageBodyTypeImage:
            {
                // 得到一个图片消息body
                EMImageMessageBody *body = ((EMImageMessageBody *)msgBody);
                NSLog(@"大图remote路径 -- %@"   ,body.remotePath);
                NSLog(@"大图local路径 -- %@"    ,body.localPath); // // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"大图的secret -- %@"    ,body.secretKey);
                NSLog(@"大图的W -- %f ,大图的H -- %f",body.size.width,body.size.height);
                NSLog(@"大图的下载状态 -- %lu",body.downloadStatus);
                
                
                // 缩略图sdk会自动下载
                NSLog(@"小图remote路径 -- %@"   ,body.thumbnailRemotePath);
                NSLog(@"小图local路径 -- %@"    ,body.thumbnailLocalPath);
                NSLog(@"小图的secret -- %@"    ,body.thumbnailSecretKey);
                NSLog(@"小图的W -- %f ,大图的H -- %f",body.thumbnailSize.width,body.thumbnailSize.height);
                NSLog(@"小图的下载状态 -- %lu",body.thumbnailDownloadStatus);
            }
                break;
            case EMMessageBodyTypeLocation:
            {
                EMLocationMessageBody *body = (EMLocationMessageBody *)msgBody;
                NSLog(@"纬度-- %f",body.latitude);
                NSLog(@"经度-- %f",body.longitude);
                NSLog(@"地址-- %@",body.address);
            }
                break;
            case EMMessageBodyTypeVoice:
            {
                // 音频sdk会自动下载
                EMVoiceMessageBody *body = (EMVoiceMessageBody *)msgBody;
                NSLog(@"音频remote路径 -- %@"      ,body.remotePath);
                NSLog(@"音频local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在（音频会自动调用）
                NSLog(@"音频的secret -- %@"        ,body.secretKey);
                NSLog(@"音频文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"音频文件的下载状态 -- %lu"   ,body.downloadStatus);
                NSLog(@"音频的时间长度 -- %lu"      ,body.duration);
            }
                break;
            case EMMessageBodyTypeVideo:
            {
                EMVideoMessageBody *body = (EMVideoMessageBody *)msgBody;
                
                NSLog(@"视频remote路径 -- %@"      ,body.remotePath);
                NSLog(@"视频local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"视频的secret -- %@"        ,body.secretKey);
                NSLog(@"视频文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"视频文件的下载状态 -- %u"   ,body.downloadStatus);
                NSLog(@"视频的时间长度 -- %lu"      ,body.duration);
                NSLog(@"视频的W -- %f ,视频的H -- %f", body.thumbnailSize.width, body.thumbnailSize.height);
                
                // 缩略图sdk会自动下载
                NSLog(@"缩略图的remote路径 -- %@"     ,body.thumbnailRemotePath);
                NSLog(@"缩略图的local路径 -- %@"      ,body.thumbnailLocalPath);
                NSLog(@"缩略图的secret -- %@"        ,body.thumbnailSecretKey);
                NSLog(@"缩略图的下载状态 -- %lu"      ,body.thumbnailDownloadStatus);
            }
                break;
            case EMMessageBodyTypeFile:
            {
                EMFileMessageBody *body = (EMFileMessageBody *)msgBody;
                NSLog(@"文件remote路径 -- %@"      ,body.remotePath);
                NSLog(@"文件local路径 -- %@"       ,body.localPath); // 需要使用sdk提供的下载方法后才会存在
                NSLog(@"文件的secret -- %@"        ,body.secretKey);
                NSLog(@"文件文件大小 -- %lld"       ,body.fileLength);
                NSLog(@"文件文件的下载状态 -- %lu"   ,body.downloadStatus);
            }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - EMGroupManagerDelegate

- (void)didReceiveLeavedGroup:(EMGroup *)aGroup
                       reason:(EMGroupLeaveReason)aReason
{
    if (aReason == EMGroupLeaveReasonBeRemoved) {
        NSLog(@"你被从%@踢出",aGroup.subject);
    } else if(aReason == EMGroupLeaveReasonUserLeave) {
        NSLog(@"你离开了%@",aGroup.subject);
    } else {
        NSLog(@"%@已解散",aGroup.subject);
    }
}

- (void)didReceiveJoinGroupApplication:(EMGroup *)aGroup
                             applicant:(NSString *)aApplicant
                                reason:(NSString *)aReason
{
    NSLog(@"%@申请加入%@：%@",aApplicant,aGroup.subject,aReason);
}

- (void)didJoinedGroup:(EMGroup *)aGroup
               inviter:(NSString *)aInviter
               message:(NSString *)aMessage
{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:[NSString stringWithFormat:@"%@ invite you to group: %@ [%@]", aInviter, aGroup.subject, aGroup.groupId] delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//    [alertView show];
}

- (void)didReceiveDeclinedJoinGroup:(NSString *)aGroupId
                             reason:(NSString *)aReason
{
    NSLog(@"你的入群申请被拒绝");
    if (!aReason || aReason.length == 0) {
        aReason = [NSString stringWithFormat:NSLocalizedString(@"group.beRefusedToJoin", @"be refused to join the group\'%@\'"), aGroupId];
    }
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:aReason delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//    [alertView show];
}

- (void)didReceiveAcceptedJoinGroup:(EMGroup *)aGroup
{
    NSLog(@"群组已通过了你的入群申请，你已加入了%@",aGroup.subject);
//    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"group.agreedAndJoined", @"agreed to join the group of \'%@\'"), aGroup.subject];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//    [alertView show];
}

- (void)didReceiveGroupInvitation:(NSString *)aGroupId
                          inviter:(NSString *)aInviter
                          message:(NSString *)aMessage
{
    if (!aGroupId || !aInviter) {
        return;
    }
}

#pragma mark - EMContactManagerDelegate

- (void)didReceiveAgreedFromUsername:(NSString *)aUsername
{
    NSString *msgstr = [NSString stringWithFormat:@"%@同意了加好友申请", aUsername];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msgstr delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alertView show];
}

- (void)didReceiveDeclinedFromUsername:(NSString *)aUsername
{
    NSString *msgstr = [NSString stringWithFormat:@"%@拒绝了加好友申请", aUsername];
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:msgstr delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alertView show];
}

- (void)friendshipDidRemoveByUser:(NSString *)aUsername
{
    NSLog(@"%@解除了与你的好友关系",aUsername);
}

- (void)friendshipDidAddByUser:(NSString *)aUsername
{
    NSLog(@"添加好友成功");
}
#pragma mark - 收到好友申请
- (void)friendRequestDidReceiveFromUser:(NSString *)aUsername
                                message:(NSString *)aMessage
{
    if (!aUsername) {
        return;
    }
    NSLog(@"%@请求添加你为好友：%@",aUsername,aMessage);
}

#pragma mark - EMChatroomManagerDelegate

- (void)didReceiveUserJoinedChatroom:(EMChatroom *)aChatroom
                            username:(NSString *)aUsername
{
    NSLog(@"%@加入了聊天室",aUsername);
}

- (void)didReceiveUserLeavedChatroom:(EMChatroom *)aChatroom
                            username:(NSString *)aUsername
{
    NSLog(@"%@离开了聊天室",aUsername);
}

- (void)didReceiveKickedFromChatroom:(EMChatroom *)aChatroom
                              reason:(EMChatroomBeKickedReason)aReason
{
    if (aReason == EMChatroomBeKickedReasonBeRemoved) {
        NSLog(@"你被踢出了聊天室");
    } else {
        NSLog(@"聊天室已解散");
    }
}
#pragma mark - public

#pragma mark - private
- (BOOL)_needShowNotification:(NSString *)fromChatter
{
    BOOL ret = YES;
    NSArray *igGroupIds = [[EMClient sharedClient].groupManager getGroupsWithoutPushNotification:nil];
    for (NSString *str in igGroupIds) {
        if ([str isEqualToString:fromChatter]) {
            ret = NO;
            break;
        }
    }
    return ret;
}

- (ChatViewController*)_getCurrentChatView
{
    //    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:_mainVC.navigationController.viewControllers];
    ChatViewController *chatViewContrller = nil;
    //    for (id viewController in viewControllers)
    //    {
    //        if ([viewController isKindOfClass:[ChatViewController class]])
    //        {
    //            chatViewContrller = viewController;
    //            break;
    //        }
    //    }
    return chatViewContrller;
}

- (void)_clearHelper
{
    self.chatVC = nil;
    
    [[EMClient sharedClient] logout:NO];
}

- (void)_handleReceivedAtMessage:(EMMessage*)aMessage
{
    if (aMessage.chatType != EMChatTypeGroupChat || aMessage.direction != EMMessageDirectionReceive) {
        return;
    }
    
    NSString *loginUser = [EMClient sharedClient].currentUsername;
    NSDictionary *ext = aMessage.ext;
    EMConversation *conversation = [[EMClient sharedClient].chatManager getConversation:aMessage.conversationId type:EMConversationTypeGroupChat createIfNotExist:NO];
    if (loginUser && conversation && ext && [ext objectForKey:kGroupMessageAtList]) {
        id target = [ext objectForKey:kGroupMessageAtList];
        if ([target isKindOfClass:[NSString class]] && [(NSString*)target compare:kGroupMessageAtAll options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSNumber *atAll = conversation.ext[kHaveUnreadAtMessage];
            if ([atAll intValue] != kAtAllMessage) {
                NSMutableDictionary *conversationExt = conversation.ext ? [conversation.ext mutableCopy] : [NSMutableDictionary dictionary];
                [conversationExt removeObjectForKey:kHaveUnreadAtMessage];
                [conversationExt setObject:@kAtAllMessage forKey:kHaveUnreadAtMessage];
                conversation.ext = conversationExt;
            }
        }
        else if ([target isKindOfClass:[NSArray class]]) {
            if ([target containsObject:loginUser]) {
                if (conversation.ext[kHaveUnreadAtMessage] == nil) {
                    NSMutableDictionary *conversationExt = conversation.ext ? [conversation.ext mutableCopy] : [NSMutableDictionary dictionary];
                    [conversationExt setObject:@kAtYouMessage forKey:kHaveUnreadAtMessage];
                    conversation.ext = conversationExt;
                }
            }
        }
    }
}

@end
