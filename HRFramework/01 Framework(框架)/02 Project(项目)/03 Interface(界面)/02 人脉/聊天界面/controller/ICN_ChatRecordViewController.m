//
//  ICN_ChatRecordViewController.m
//  ICan
//
//  Created by zjk on 2017/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ChatRecordViewController.h"
#import "ICN_ChatRecordTableViewCell.h"
#import "BaseOptionalModel.h"
#import "ToolAboutTime.h"

// 添加两个搜索用的常量key
static NSString * const private_GroupNameKey = @"groupName"; // 群组名称key
static NSString * const private_userIDKey = @"userID"; // 用户IDkey
static NSString * const private_userIConKey = @"userIcon"; // 用户头像key
static NSString * const private_userNameKey = @"userName"; // 用户名称key

@interface ICN_ChatRecordViewController ()<UITableViewDelegate,UITableViewDataSource , UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mainTableview;

@property (strong, nonatomic) NSArray *conversations; // 当前页面全部的会话数组
@property (strong , nonatomic) NSMutableArray *searchConversationsArr; /// 当前页面搜索结果的会话数组

@property (nonatomic , strong)NSMutableArray *searchContentMArr;
@property (nonatomic , strong)NSMutableArray *singleChatList; // 单聊需要的头像等数据
@property (weak, nonatomic) IBOutlet UITextField *searchTextF;


@end

@implementation ICN_ChatRecordViewController

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)singleChatList{
    if (_singleChatList == nil) {
        _singleChatList = [NSMutableArray array];
    }
    return _singleChatList;
}

- (NSMutableArray *)searchContentMArr{
    if (_searchContentMArr == nil) {
        _searchContentMArr = [NSMutableArray array];
    }
    return _searchContentMArr;
}

- (NSMutableArray *)searchConversationsArr{
    if (_searchConversationsArr == nil) {
        _searchConversationsArr = [NSMutableArray array];
    }
    return _searchConversationsArr;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mainTableview registerNib:[UINib nibWithNibName:@"ICN_ChatRecordTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ChatRecordTableViewCell"];
    
    self.naviTitle = @"聊天记录";
    // 设置搜索框代理
    self.searchTextF.delegate = self;
    [self configDataSoure];
}

#pragma mark - ---------- 私有方法 ----------

// 根据单聊的id配置数据源的方法
- (void)configDataSoure {
    
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 没有网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    
    // 配置数据源之前添加遮罩
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"请求数据"];
    
    [self.singleChatList removeAllObjects];
    self.conversations = [[EMClient sharedClient].chatManager getAllConversations];
    // 获取到全部单聊需要显示的信息 -- id，头像，昵称
    NSString *idStr = nil;
    // 用户承接所有单聊id（服务器id）的数组
    NSMutableArray *arr = [NSMutableArray array];
    for (EMConversation *conversation in self.conversations) {
        // type类型为 0 表示是单聊
        if (conversation.type == 0) {
            // 去掉i行标识
            NSString *str = [conversation.conversationId substringFromIndex:5];
            [arr addObject:str];
        }
    }
    // 将获取到的id整合成一个“，”分割的字符串
    idStr = [arr componentsJoinedByString:@","];
    if (idStr.length > 0) {
        // 有值 - 请根据获取到的id请求接口
        
        [HTTPManager POST_PATH:PATH_hxMemberInfo params:@{@"token" : SF(@"%@",[USERDEFAULT valueForKey:HR_CurrentUserToken]) , @"memberIds" : idStr} success:^(id responseObject) {
            // 清空历史数据 -- 并关闭遮罩
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            
            NSArray *resultArr = responseObject;
            for (NSDictionary *item in resultArr) {
                NSDictionary *dic = @{
                                      private_userIDKey : [item valueForKey:@"memberId"],
                                      private_userNameKey : [item valueForKey:@"memberNick"],
                                      private_userIConKey : ICN_IMG([item valueForKey:@"memberLogo"]),
                                      };
                [self.singleChatList addObject:dic];
            }
            
            // 配置好了之后刷新数据
            [self.mainTableview reloadData];
            
            
        } failure:^(NSError *error) {
            // 请求失败提示错误信息
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败"];
        }];
    }
    
    // 在三秒钟后自动关闭遮罩
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 在3秒钟后自动关闭遮罩
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
    });
    
    // 在获取到全部消息之后自动获取搜索索引字段
    [self configSearchContentArrWithConversations];
    [self.mainTableview reloadData];
}

// 在每次获取到会话之后自动配置搜索索引
- (void)configSearchContentArrWithConversations{
    
    // 会话列表没有数据时自动返回
    if (!self.conversations) {
        return ;
    }
    
    if (self.searchContentMArr.count > 0) {
        [self.searchContentMArr removeAllObjects];
    }
    // 重新获取筛选数据的索引
    for (EMConversation *conversation in self.conversations) {
        NSString *userName = @"";
        NSString *groupName = @"";
        switch (conversation.type) {
            case 0:{
                // 单聊
                userName = [conversation.ext valueForKey:@"username"] ? [conversation.ext valueForKey:@"username"] : @"";
                break ;
            }
            case 1:{
                // 群聊
                groupName = [conversation.ext valueForKey:@"groupname"] ? [conversation.ext valueForKey:@"groupname"] : @"";
                break ;
            }
            default:
                break;
        }
        // 设置好需要的字段之后将其放入索引
        NSDictionary *dic = @{
                              private_userNameKey:userName,
                              private_GroupNameKey:groupName
                              };
        [self.searchContentMArr addObject:dic];
    }
    
}


#pragma mark - ---------- IBAction ----------
// 点击聊天记录页面搜索方法
- (IBAction)clickOnSearchButtonAction:(UIButton *)sender {
    
    // 判断搜索内容是否符合条件 - 符合则开始搜索
    NSString *searchContent = self.searchTextF.text ? self.searchTextF.text : @"";
    // 获取到索引数组
    NSMutableArray *indexArr = [NSMutableArray array];
    if ([searchContent isEqualToString:@""]) {
        // 数据不合法
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入有效搜索内容"];
        return ;
    }else{
        // 搜索内容有效 -- 开始搜索
        if (self.searchContentMArr.count == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"没有聊天记录"];
        }else{
            for (NSDictionary *item in self.searchContentMArr) {
                // 符合条件则添加索引
                if ([[item valueForKey:private_userNameKey] containsString:searchContent] || [[item valueForKey:private_GroupNameKey] containsString:searchContent]) {
                    [indexArr addObject:SF(@"%ld",[self.searchContentMArr indexOfObject:item])];
                }
            }
        }
    }
    // 如果索引有数据则刷新显示
    [self.searchConversationsArr removeAllObjects];
    if (indexArr.count > 0) {
        for (NSString *index in indexArr) {
            [self.searchConversationsArr addObject:self.conversations[index.integerValue]];
        }
    }
    //收回键盘
    [self.searchTextF resignFirstResponder];
    // 刷新数据
    [self.mainTableview reloadData];
    
}



#pragma mark - ---------- 代理 ----------

#pragma mark - ---------- 搜索框代理 - UITextFieldDelegate ----------

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    // 在文本框点击清空按钮的时候调用回复数据操作
    
    // 清空搜索数据源
    [self.searchConversationsArr removeAllObjects];
    //收回键盘
    [self.searchTextF resignFirstResponder];
    // 刷新数据
    [self.mainTableview reloadData];
    return YES;
}

#pragma mark --- tableview的代理 - UITableViewDelegate,UITableViewDataSource ---
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchConversationsArr.count > 0) {
        return self.searchConversationsArr.count;
    }
    return self.conversations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ICN_ChatRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRecordTableViewCell" forIndexPath:indexPath];
    EMConversation *conversation ;
    if (self.searchConversationsArr.count > 0) {
        conversation = self.searchConversationsArr[indexPath.row];
    }else
        conversation = self.conversations[indexPath.row];
    
    // 现在获取到全部需要的数据
    
    // 单聊 - 获取预存数据留待刷新网络数据
    if (conversation.type == 0) {
        // 设置缓冲头像
        cell.headImageView.image = [UIImage imageNamed:@"修改头像"];
        // 设置缓冲用户名
        cell.nickLab.text = @"单聊用户";
    }
    
    // 群聊 - 直接赋值
    if (conversation.type == 1) {
        // 获取群标签
        cell.nickLab.text = [conversation.ext valueForKey:@"groupname"] ? [conversation.ext valueForKey:@"groupname"] : ([conversation.latestMessage.ext valueForKey:@"groupname"] ? [conversation.latestMessage.ext valueForKey:@"groupname"] : @"组名");
        // 获取群头像
        // 获取群头像
        NSString *groupIcon;
        if ([conversation.ext valueForKey:@"groupheadUrl"]) {
            // 进行对于头像的处理
            if ([[conversation.ext valueForKey:@"groupheadUrl"] containsString:@"http"]) {
                // 全网址
                groupIcon = [conversation.ext valueForKey:@"groupheadUrl"];
            }else{
                // 网址需要拼接
                groupIcon = ICN_IMG([conversation.ext valueForKey:@"groupheadUrl"]);
            }
        }else{
            // 将群组头像置为nil
            groupIcon = @"";
        }
        // 根据是否获取到群头像进行对应的处理
        [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:groupIcon] placeholderImage:[UIImage imageNamed:@"修改头像"]];
        
    }
    
    // 根据配置好的单聊内容获取头像等信息（在判断赋值的是单聊类型的时候在操作）
    if (self.singleChatList.count > 0 && conversation.type == 0) {
        NSDictionary *dic ;
        // 根据id需找到需要的单聊数据源
        NSString *userId = [conversation.conversationId substringFromIndex:5];
        for (NSDictionary *temp in self.singleChatList) {
            if ([[temp valueForKey:private_userIDKey] isEqualToString:userId]) {
                // 找到符合要求的单聊数据字典
                dic = temp;
            }
        }
        
        // 根据会话的类型判断显示样式
        if (dic) {
            // 有限判断是否存在需要的数据源
            switch (conversation.type) {
                case 0:{
                    // 单聊Cell
                    [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:(dic[private_userIConKey])] placeholderImage:[UIImage imageNamed:@"修改头像"]];
                    NSLog(@"%@",ICN_IMG(dic[private_userIConKey]));
                    cell.nickLab.text = dic[private_userNameKey];
                    break ;
                }
                default:{
                    break;
                }
            }
        }
        
    }
    
    // 添加正文的信息内容
    EMMessageBody *msgBody = conversation.latestMessage.body;
    switch (msgBody.type) {
        case EMMessageBodyTypeText:
        {
            // 收到的文字消息
            EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
            cell.newsLab.attributedText = [[NSAttributedString alloc] initWithString:textBody.text];
        }
            break;
        case EMMessageBodyTypeImage:
        {
            cell.newsLab.text = @"图片";
        }
            break;
        case EMMessageBodyTypeVoice:
        {
            cell.newsLab.text = @"语音";
        }
            break;
            
            
        default:
            break;
    }
    cell.dateLab.text = [ToolAboutTime getTimeStrByTimeSp:[NSString stringWithFormat:@"%lld",conversation.latestMessage.timestamp]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EMConversation *conversation = self.conversations[indexPath.row];
    ChatViewController *chatView;
    // 单聊相关的透传赋值
    if (conversation.type == 0) {
        
        // 获取到需要的数据源字典
        NSDictionary *dic ;
        // 根据id需找到需要的单聊数据源
        NSString *userId = [conversation.conversationId substringFromIndex:5];
        for (NSDictionary *temp in self.singleChatList) {
            if ([[temp valueForKey:private_userIDKey] isEqualToString:userId]) {
                // 找到符合要求的单聊数据字典
                dic = temp;
            }
        }
        // 根据dic的内容进行透传赋值
        if (dic) {
            chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"username":[dic valueForKey:private_userNameKey],@"headUrl":[dic valueForKey:private_userIConKey]}];
            chatView.title = [dic valueForKey:private_userNameKey];
        }else{
            // 随意赋值
            chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"username":@"匿名",@"headUrl":@""}];
            chatView.title = @"匿名";
        }
    // 群聊相关赋值
    } else {
        NSLog(@"%@",conversation.ext[@"groupheadUrl"]);
        chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"groupname":conversation.latestMessage.ext[@"groupname"]?conversation.latestMessage.ext[@"groupname"]:@"",@"groupheadUrl":conversation.latestMessage.ext[@"groupheadUrl"]?conversation.latestMessage.ext[@"groupheadUrl"]:@""}];
        chatView.title = conversation.ext[@"groupname"];
    }
    
    
    [chatView setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:chatView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}


@end
