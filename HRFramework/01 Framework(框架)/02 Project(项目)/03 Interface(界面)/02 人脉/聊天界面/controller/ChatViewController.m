

#import "ChatViewController.h"
#import "EaseEmotionManager.h"
#import "EaseEmoji.h"
#import "ChatDemoHelper.h"
#import "ICN_transmissionTableViewCell.h"
#import "BSBusinessCardCell.h"
#import "EaseMessageModel.h"
#import "EaseMessageReadManager.h"


#import "ICN_YouMengShareTool.h"                // 分享工具类
#import "ICN_ShareManager.h" // 分享管理器

#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_IntegralView.h"
#import "ICN_DynWarnView.h"


#import "ICN_TransmitToDynamicPager.h"
#import "ICN_PublishComplainPager.h" // 跳转到发布吐槽页面
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_PublishAskQuestionPager.h" // 跳转到发布提问页面
#import "ICN_MyQuestionDetialPager.h" // 跳转到我的提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 跳转到用户的提问详情页面
#import "ICN_TopicPublicationVC.h" // 跳转到发布话题页面
#import "ICN_DynamicStatePublicationVC.h" // 跳转到发布动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 跳转到动态评论页面
#import "ICN_MyfridentViewController.h"// 分享到动态
#pragma mark ----------------详情页----------------------------------------------
#import "ICN_ActivityDetialViewController.h"   //活动详情
#import "ICN_LiveDetialViewController.h"       //直播详情
#import "ICN_IndustryNextViewController.h"     //行业详情
#import "ICN_PositionNextOneViewController.h"  //职场详情
#import "ICN_UserHomePagerVC.h"                //用户主页
#import "CardCell.h"
#import "ICN_ComplainDetialPager.h" // 吐槽详情页面
#import "ICN_MyQuestionDetialPager.h" // 我得提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 别人的提问详情页面
#import "ICN_UserDynamicStateDetialVC.h"      //资讯

static NSString *const PATH_getMemberInfo = @"Member/Member/getMemberInfo";
@interface ChatViewController ()<UIAlertViewDelegate,IModelChatCell,ICN_DynWarnViewDelegate,EMClientDelegate,EMChatToolbarDelegate,UITextViewDelegate,UITextFieldDelegate>
{
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    UIMenuItem *_transpondMenuItem;
}

@property (nonatomic) BOOL isPlayingAudio;

@property (nonatomic) NSMutableDictionary *emotionDic;

@property (nonatomic,strong) ICN_YouMengShareModel * transmitModel;

@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗
@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面


@end

@implementation ChatViewController
- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                
                break;
            }
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self.navigationController pushViewController:pager animated:YES];
            }
            default:
                break;
        }
        // 在转发之后无论成功失败清空相关Model
        
        self.transmitModel = nil;
    }
}

//活动分享
- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    
    //    1002331  动态  1002332 智讯
    
    switch (type) {
            // 点击发布动态
        case Publich_DynamicState:{
            [self transmitSourceDataToDynamicReview];
        }
            break;
            // 点击发布智讯
        case Publich_WisdomState:{
            
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self.navigationController pushViewController:pager animated:YES];
            break;
        }
            // 发布吐槽
        case Publich_ComplainState:{
            
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self.navigationController pushViewController:pager animated:YES];
            break;
        }
            // 发布提问
        case Publich_AskQuestion:{
            HRLog(@"点击的是右边的按钮");
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self.navigationController pushViewController:pager animated:YES];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            ICN_MyfridentViewController *MVC = [[ICN_MyfridentViewController alloc]init];
            NSDictionary *dic = @{@"shareId":self.transmitModel.modelId,@"contentPic":self.transmitModel.iconUrl,@"content":self.transmitModel.content};
            MVC.ext = dic;
            [self.navigationController pushViewController:MVC animated:YES];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            
            // 分享到 QQ
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            
            // 分享到微信朋友圈
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            NSLog(@"分享到QQ空间");
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            break;
        }
        default:
            break;
    }
    [self.warnView removeFromSuperview];
    [self.replayView removeFromSuperview];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.showRefreshHeader = YES;
    
    self.delegate = self;
    self.dataSource = self;
    
    //判断如果分享 自动发一条消息
    NSString *tempStr = self.ext[@"shareType"];
    
    if (self.ext) {
       
        if ([tempStr integerValue] == 1) {
            [self sendTextMessage:@"哈哈" withExt:self.ext];
        }else
        {
            
            [[HRRequest manager]POST_PATH:PATH_getMemberInfo params:nil success:^(id result) {
                BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (model.code == 0) {
                    // 获取数据成功
                    NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithDictionary:self.ext];
                    [dicttt setValue:([result valueForKeyPath:@"result.memberLogo"] ? [result valueForKeyPath:@"result.memberLogo"] : @"") forKey:@"headUrl"];
                    [dicttt setValue:([result valueForKeyPath:@"result.memberNick"] ? [result valueForKeyPath:@"result.memberNick"] : @"匿名") forKey:@"username"];
                    // 将获取到的消息透传
                    [self sendTextMessage:@"分享ixing动态" withExt:dicttt];
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:model.info];
                }

            } failure:^(NSDictionary *errorInfo) {
                 [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络获取失败"];
            }];
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
//                
//                BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
//                if (model.code == 0) {
//                    // 获取数据成功
//                    NSMutableDictionary *dicttt = [NSMutableDictionary dictionaryWithDictionary:self.ext];
//                    [dicttt setValue:([result valueForKeyPath:@"result.memberLogo"] ? [result valueForKeyPath:@"result.memberLogo"] : @"") forKey:@"headUrl"];
//                    [dicttt setValue:([result valueForKeyPath:@"result.memberNick"] ? [result valueForKeyPath:@"result.memberNick"] : @"匿名") forKey:@"username"];
//                    // 将获取到的消息透传
//                    [self sendTextMessage:@"分享ixing动态" withExt:dicttt];
//                    
//                }else{
//                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:model.info];
//                }
//                
//                
//            } failure:^(NSDictionary *errorInfo) {
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络获取失败"];
//            }];
            
        }
    }
    
    
    //注册消息回调
    //[[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    [self _setupBarButtonItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAllMessages:) name:KNOTIFICATIONNAME_DELETEALLMESSAGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitGroup) name:@"ExitGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertCallMessage:) name:@"insertCallMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCallNotification:) name:@"callOutWithChatter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCallNotification:) name:@"callControllerClose" object:nil];
    
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc
{
    if (self.conversation.type == EMConversationTypeChatRoom)
    {
        //退出聊天室，删除会话
        if (self.isJoinedChatroom) {
            NSString *chatter = [self.conversation.conversationId copy];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                EMError *error = nil;
                [[EMClient sharedClient].roomManager leaveChatroom:chatter error:&error];
                if (error !=nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Leave chatroom '%@' failed [%@]", chatter, error.errorDescription] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alertView show];
                    });
                }
            });
        }
        else {
            [[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:YES completion:nil];
        }
    }
    //[[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:YES completion:nil];
    [[EMClient sharedClient] removeDelegate:self];
    //移除消息回调
    // [[EMClient sharedClient].chatManager removeDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    if (self.conversation.type == EMConversationTypeGroupChat) {
        NSDictionary *ext = self.conversation.ext;
        if ([[ext objectForKey:@"subject"] length])
        {
            self.title = [ext objectForKey:@"subject"];
        }
        
        if (ext && ext[kHaveUnreadAtMessage] != nil)
        {
            NSMutableDictionary *newExt = [ext mutableCopy];
            [newExt removeObjectForKey:kHaveUnreadAtMessage];
            self.conversation.ext = newExt;
        }
    }
    
    
    
    
    
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //    self.view.frame = [UIScreen mainScreen].bounds;
}

#pragma mark - setup subviews

- (void)_setupBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 9, 44)];
    backButton.accessibilityIdentifier = @"back";
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.navigationItem setLeftBarButtonItem:backItem];
    
    //单聊
    if (self.conversation.type == EMConversationTypeChat) {
        UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        clearButton.accessibilityIdentifier = @"clear_message";
        [clearButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [clearButton addTarget:self action:@selector(deleteAllMessages:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearButton];
    }
    else{//群聊
        UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        detailButton.accessibilityIdentifier = @"detail";
        [detailButton setImage:[UIImage imageNamed:@"i行群组"] forState:UIControlStateNormal];
        [detailButton addTarget:self action:@selector(showGroupDetailAction) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex != buttonIndex) {
        self.messageTimeIntervalTag = -1;
        [self.conversation deleteAllMessages:nil];
        [self.dataArray removeAllObjects];
        [self.messsagesSource removeAllObjects];
        
        [self.tableView reloadData];
    }
}


#pragma mark - EaseMessageViewControllerDataSource

- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController
                           modelForMessage:(EMMessage *)message
{
    id<IMessageModel> model = nil;
    // 使用EaseMessageModel的方式将message以Model的形式显示
    model = [[EaseMessageModel alloc] initWithMessage:message];
#warning 消息的透传
    // 透传消息存在的情况下根据透传消息显示
    NSString *messageIcon = @"";
    NSString *messageNick = @"";
    NSString *groupName = @"";
    NSDictionary *ext ; // 需要根据情况判断昵称是用会话的还是消息的(单聊时候用会话的群聊时候用当时消息的)
    if (self.conversation.type == 0) {
        // 单聊
        ext = self.conversation.ext;
    }else{
        ext = message.ext;
    }
    if (ext) {
        messageNick = [ext valueForKey:@"username"] ? [ext valueForKey:@"username"] : @"用户";
        if ([[ext valueForKey:@"headUrl"] containsString:@"http"]) {
            // 头像网址是全网址
            messageIcon = [ext valueForKey:@"headUrl"];
        }else{
            // 头像网址需要拼接
            messageIcon = ICN_IMG([ext valueForKey:@"headUrl"]);
        }
        groupName = [self.conversation.ext valueForKey:@"groupname"] ? [self.conversation.ext valueForKey:@"groupname"] : ([message.ext valueForKey:@"groupname"] ? [message.ext valueForKey:@"groupname"] : @"组名");
        // 校验需要获取到的信息
        HRLog(@"%@ - %@",[self.conversation.ext valueForKey:@"groupname"] , [message.ext valueForKey:@"groupname"]);
        HRLog(@"%@ - %@ - %@" , messageNick , messageIcon , groupName);
        model.nickname = messageNick;
        model.avatarURLPath = messageIcon;
    }
    
    // 根据会话类型赋值 -- 必须赋值的是用户名和昵称
    //如果是自己发的消息
    if (model.message.direction == 0) {
        NSLog(@"%@",[USERDEFAULT valueForKey:ICN_UserNick]);
        model.nickname = [USERDEFAULT valueForKey:ICN_UserNick];
        model.avatarURLPath = [USERDEFAULT valueForKey:ICN_UserIconUrl];
    }
    
    
    switch (self.conversation.type) {
            
        case EMConversationTypeGroupChat:{
            // 获取到的是群聊的样式 == 需要设置群组昵称
            self.navigationItem.title = groupName;
            break ;
        }
            
        default:
            break;
    }
    
    
    
    model.failImageName = @"imageDownloadFail";
    return model;
}

- (NSArray*)emotionFormessageViewController:(EaseMessageViewController *)viewController
{
    NSMutableArray *emotions = [NSMutableArray array];
    for (NSString *name in [EaseEmoji allEmoji]) {
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:name emotionThumbnail:name emotionOriginal:name emotionOriginalURL:@"" emotionType:EMEmotionDefault];
        [emotions addObject:emotion];
    }
    EaseEmotion *temp = [emotions objectAtIndex:0];
    EaseEmotionManager *managerDefault = [[EaseEmotionManager alloc] initWithType:EMEmotionDefault emotionRow:3 emotionCol:7 emotions:emotions tagImage:[UIImage imageNamed:temp.emotionId]];
    
    NSMutableArray *emotionGifs = [NSMutableArray array];
    _emotionDic = [NSMutableDictionary dictionary];
    NSArray *names = @[@"icon_002",@"icon_007",@"icon_010",@"icon_012",@"icon_013",@"icon_018",@"icon_019",@"icon_020",@"icon_021",@"icon_022",@"icon_024",@"icon_027",@"icon_029",@"icon_030",@"icon_035",@"icon_040"];
    int index = 0;
    for (NSString *name in names) {
        index++;
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:[NSString stringWithFormat:@"[示例%d]",index] emotionId:[NSString stringWithFormat:@"em%d",(1000 + index)] emotionThumbnail:[NSString stringWithFormat:@"%@_cover",name] emotionOriginal:[NSString stringWithFormat:@"%@",name] emotionOriginalURL:@"" emotionType:EMEmotionGif];
        [emotionGifs addObject:emotion];
        [_emotionDic setObject:emotion forKey:[NSString stringWithFormat:@"em%d",(1000 + index)]];
    }
    EaseEmotionManager *managerGif= [[EaseEmotionManager alloc] initWithType:EMEmotionGif emotionRow:2 emotionCol:4 emotions:emotionGifs tagImage:[UIImage imageNamed:@"icon_002_cover"]];
    
    return @[managerDefault,managerGif];
}

- (BOOL)isEmotionMessageFormessageViewController:(EaseMessageViewController *)viewController
                                    messageModel:(id<IMessageModel>)messageModel
{
    BOOL flag = NO;
    if ([messageModel.message.ext objectForKey:MESSAGE_ATTR_IS_BIG_EXPRESSION]) {
        return YES;
    }
    return flag;
}

- (EaseEmotion*)emotionURLFormessageViewController:(EaseMessageViewController *)viewController
                                      messageModel:(id<IMessageModel>)messageModel
{
    NSString *emotionId = [messageModel.message.ext objectForKey:MESSAGE_ATTR_EXPRESSION_ID];
    EaseEmotion *emotion = [_emotionDic objectForKey:emotionId];
    if (emotion == nil) {
        emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:emotionId emotionThumbnail:@"" emotionOriginal:@"" emotionOriginalURL:@"" emotionType:EMEmotionGif];
    }
    return emotion;
}

- (NSDictionary*)emotionExtFormessageViewController:(EaseMessageViewController *)viewController
                                        easeEmotion:(EaseEmotion*)easeEmotion
{
    return @{MESSAGE_ATTR_EXPRESSION_ID:easeEmotion.emotionId,MESSAGE_ATTR_IS_BIG_EXPRESSION:@(YES)};
}

- (void)messageViewControllerMarkAllMessagesAsRead:(EaseMessageViewController *)viewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setupUnreadMessageCount" object:nil];
}

#pragma mark - EaseMob

#pragma mark - EMClientDelegate

- (void)userAccountDidLoginFromOtherDevice
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

- (void)userAccountDidRemoveFromServer
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

- (void)userDidForbidByServer
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

#pragma mark - action

- (void)backAction
{
    [[EMClient sharedClient].chatManager removeDelegate:self];
    [[EMClient sharedClient].roomManager removeDelegate:self];
    [[ChatDemoHelper shareHelper] setChatVC:nil];
    
    if (self.deleteConversationIfNull) {
        //判断当前会话是否为空，若符合则删除该会话
        EMMessage *message = [self.conversation latestMessage];
        if (message == nil) {
            [[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:NO completion:nil];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark 群聊点击页面右上角按钮
#warning 群聊点击页面右上角按钮
- (void)showGroupDetailAction
{
    [self.view endEditing:YES];
    if (self.conversation.type == EMConversationTypeGroupChat) {
        
    }
    else if (self.conversation.type == EMConversationTypeChatRoom)
    {
        
    }
}

- (void)deleteAllMessages:(id)sender
{
    if (self.dataArray.count == 0) {
        [self showHint:NSLocalizedString(@"message.noMessage", @"no messages")];
        return;
    }
    
    if ([sender isKindOfClass:[NSNotification class]]) {
        NSString *groupId = (NSString *)[(NSNotification *)sender object];
        BOOL isDelete = [groupId isEqualToString:self.conversation.conversationId];
        if (self.conversation.type != EMConversationTypeChat && isDelete) {
            self.messageTimeIntervalTag = -1;
            [self.conversation deleteAllMessages:nil];
            [self.messsagesSource removeAllObjects];
            [self.dataArray removeAllObjects];
            
            [self.tableView reloadData];
            [self showHint:NSLocalizedString(@"message.noMessage", @"no messages")];
        }
    }
    else if ([sender isKindOfClass:[UIButton class]]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"sureToDelete", @"please make sure to delete") delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
        [alertView show];
    }
}

- (void)transpondMenuAction:(id)sender
{
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
#warning 转发
    }
    self.menuIndexPath = nil;
}

- (void)copyMenuAction:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
        pasteboard.string = model.text;
    }
    
    self.menuIndexPath = nil;
}

- (void)deleteMenuAction:(id)sender
{
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
        NSMutableIndexSet *indexs = [NSMutableIndexSet indexSetWithIndex:self.menuIndexPath.row];
        NSMutableArray *indexPaths = [NSMutableArray arrayWithObjects:self.menuIndexPath, nil];
        
        [self.conversation deleteMessageWithId:model.message.messageId error:nil];
        [self.messsagesSource removeObject:model.message];
        
        if (self.menuIndexPath.row - 1 >= 0) {
            id nextMessage = nil;
            id prevMessage = [self.dataArray objectAtIndex:(self.menuIndexPath.row - 1)];
            if (self.menuIndexPath.row + 1 < [self.dataArray count]) {
                nextMessage = [self.dataArray objectAtIndex:(self.menuIndexPath.row + 1)];
            }
            if ((!nextMessage || [nextMessage isKindOfClass:[NSString class]]) && [prevMessage isKindOfClass:[NSString class]]) {
                [indexs addIndex:self.menuIndexPath.row - 1];
                [indexPaths addObject:[NSIndexPath indexPathForRow:(self.menuIndexPath.row - 1) inSection:0]];
            }
        }
        
        [self.dataArray removeObjectsAtIndexes:indexs];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        if ([self.dataArray count] == 0) {
            self.messageTimeIntervalTag = -1;
        }
    }
    
    self.menuIndexPath = nil;
}

#pragma mark - notification
- (void)exitGroup
{
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)insertCallMessage:(NSNotification *)notification
{
    id object = notification.object;
    if (object) {
        EMMessage *message = (EMMessage *)object;
        [self addMessageToDataSource:message progress:nil];
        [[EMClient sharedClient].chatManager importMessages:@[message] completion:nil];
    }
}

- (void)handleCallNotification:(NSNotification *)notification
{
    id object = notification.object;
    if ([object isKindOfClass:[NSDictionary class]]) {
        //开始call
        self.isViewDidAppear = NO;
    } else {
        //结束call
        self.isViewDidAppear = YES;
    }
}

#pragma mark - private

- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(EMMessageBodyType)messageType
{
    if (self.menuController == nil) {
        self.menuController = [UIMenuController sharedMenuController];
    }
    
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"delete", @"Delete") action:@selector(deleteMenuAction:)];
    }
    
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"copy", @"Copy") action:@selector(copyMenuAction:)];
    }
    
    if (_transpondMenuItem == nil) {
        _transpondMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"transpond", @"Transpond") action:@selector(transpondMenuAction:)];
    }
    
    if (messageType == EMMessageBodyTypeText) {
        [self.menuController setMenuItems:@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem]];
    } else if (messageType == EMMessageBodyTypeImage){
        [self.menuController setMenuItems:@[_deleteMenuItem,_transpondMenuItem]];
    } else {
        [self.menuController setMenuItems:@[_deleteMenuItem]];
    }
    [self.menuController setTargetRect:showInView.frame inView:showInView.superview];
    [self.menuController setMenuVisible:YES animated:YES];
}
#pragma mark ----------------EaseMessageViewControllerDelegate---------------------
- (UITableViewCell *)messageViewController:(UITableView *)tableView cellForMessageModel:(id<IMessageModel>)messageModel
{
    NSDictionary *dic = messageModel.message.ext;
    
    //样例为如果消息是文本消息显示用户自定义cell
    if(messageModel.bodyType == EMMessageBodyTypeText &&
       [[messageModel text] hasPrefix:@"分享"] && (messageModel.message.ext != nil)) {
        
        if ( [dic[@"shareType"] integerValue] != 1) {
            NSString *CellIdentifier = @"哈哈";
            //CustomMessageCell为用户自定义cell,继承了EaseBaseMessageCell
            BSBusinessCardCell *cell = (BSBusinessCardCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[BSBusinessCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier model:messageModel];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.model = messageModel;
            return cell;
            
        }else{
            NSString *CellIdentifier = @"个人名片";
            CardCell *cell = (CardCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[CardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier model:messageModel];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.model = messageModel;
            return cell;
            
        }
        
    }else if (messageModel.bodyType == EMMessageBodyTypeText &&
              [dic[@"shareType"] integerValue] == 1 && (messageModel.message.ext != nil)){
        NSString *CellIdentifier = @"个人名片";
        CardCell *cell = (CardCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier model:messageModel];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.model = messageModel;
        return cell;
        
    }
    return nil;
    
    
    
    
    
}
/// 遵守协议，为红包，名片提供自定义高度
- (CGFloat)messageViewController:(EaseMessageViewController *)viewController
           heightForMessageModel:(id<IMessageModel>)messageModel
                   withCellWidth:(CGFloat)cellWidth
{
    NSDictionary *dic = messageModel.message.ext;
    
    
    //样例为如果消息是文本消息使用用户自定义cell的高度
    if (messageModel.bodyType == EMMessageBodyTypeText &&
        [[messageModel text] hasPrefix:@"分享"]) {
        //CustomMessageCell为用户自定义cell,继承了EaseBaseMessageCell
        if ([dic[@"shareType"] integerValue] != 1) {
            return [BSBusinessCardCell cellHeightWithModel:messageModel];
        }else{
            return [CardCell cellHeightWithModel:messageModel];
        }
        
    }else if (messageModel.bodyType == EMMessageBodyTypeText &&
              [dic[@"shareType"] integerValue] == 1 && (messageModel.message.ext != nil)){
        return [CardCell cellHeightWithModel:messageModel];
    }
    
    return 0.f;
}



// 点击事件
- (BOOL)messageViewController:(EaseMessageViewController *)viewController
        didSelectMessageModel:(id<IMessageModel>)messageModel
{
    
    
    
    BOOL flag = NO;
    if (messageModel.message.ext) {
        NSLog(@"cbjaknjaajvklanvklanvkla");
        NSDictionary *ext = messageModel.message.ext;
        //分享类型
        NSInteger type = [ext[@"shareType"] integerValue];
        switch (type) {
            case 6:
            {
                ICN_ActivityDetialViewController *MVC = [[ICN_ActivityDetialViewController alloc]init];
                MVC.packID = ext[@"shareId"];  //传入活动id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
                
            case 7:
            {
                ICN_PositionNextOneViewController *MVC = [[ICN_PositionNextOneViewController alloc]init];
                MVC.url = ext[@"shareId"];  //传入职场id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
            case 8:
            {
                ICN_IndustryNextViewController *MVC = [[ICN_IndustryNextViewController alloc]init];
                MVC.url = ext[@"shareId"];  //传入资讯id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
            case 10:
            {
                ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                MVC.liveID = ext[@"shareId"];  //传入直播id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
            case 1:
            {
                ICN_UserHomePagerVC *MVC = [[ICN_UserHomePagerVC alloc]init];
                MVC.memberId = ext[@"shareId"];  //传入个人id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
            case 5:
            {
                ICN_ComplainDetialPager *MVC = [[ICN_ComplainDetialPager alloc]init];
                MVC.complainId = ext[@"shareId"];  //传入吐槽id
                [self.navigationController pushViewController:MVC animated:YES];
                
            }
                break;
            case 4:
            {
                ICN_OthersQuestionDetialPager *MVC = [[ICN_OthersQuestionDetialPager alloc]init];
                MVC.contentId = ext[@"shareId"];  //传入提问id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
                //动态
            case 2:
            {
                ICN_UserDynamicStateDetialVC *MVC = [[ICN_UserDynamicStateDetialVC alloc]init];
                MVC.dynamicId = ext[@"shareId"];  //传入动态id
                [self.navigationController pushViewController:MVC animated:YES];
            }
                break;
                //智讯
            case 3:
            {
                ICN_UserDynamicStateDetialVC *MVC = [[ICN_UserDynamicStateDetialVC alloc]init];
                MVC.wisdomId = ext[@"shareId"];  //传入动态id
                [self.navigationController pushViewController:MVC animated:YES];
                
            }
                break;
                
            default:
                break;
        }
        
    }
    
    //样例为如果消息是文件消息用户自定义处理选中逻辑
    switch (messageModel.bodyType) {
       
        case EMMessageBodyTypeLocation:
        case EMMessageBodyTypeImage:
        case EMMessageBodyTypeVideo:
        case EMMessageBodyTypeVoice:{
            flag = NO;
            NSLog(@"用户自定义实现");
            
        }
            break;
        case EMMessageBodyTypeFile:
        {
            flag = YES;
            NSLog(@"用户自定义实现");
        }
            break;
        default:
            break;
    }
    return flag;
}

@end
