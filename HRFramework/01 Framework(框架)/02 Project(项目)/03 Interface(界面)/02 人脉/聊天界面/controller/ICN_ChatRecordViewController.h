//
//  ICN_ChatRecordViewController.h
//  ICan
//
//  Created by zjk on 2017/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//  尝试将聊天页面变成集成环信聊天页面的内容

#import "BaseViewController.h"
#import "EaseConversationListViewController.h"

@interface ICN_ChatRecordViewController : BaseViewController
//额外信息
@property (nonatomic,strong) NSDictionary * ext;

@end
