//
//  BSBusinessCardCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BSBusinessCardCell.h"
#import "UIImageView+EMWebCache.h"
#import "EaseBubbleView+BusinessCard.h"
static const CGFloat kCellHeight = 110.0f;

@implementation BSBusinessCardCell

#pragma mark - IModelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier model:(id<IMessageModel>)model
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier model:model];
    
    if (self) {
        self.hasRead.hidden = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}

- (BOOL)isCustomBubbleView:(id<IMessageModel>)model
{
    return YES;
}
- (void)setCustomModel:(id<IMessageModel>)model
{
    UIImage *image = model.image;
    if (!image) {
        [self.bubbleView.imageView sd_setImageWithURL:[NSURL URLWithString:model.fileURLPath] placeholderImage:[UIImage imageNamed:model.failImageName]];
    } else {
        _bubbleView.imageView.image = image;
    }
    
    if (model.avatarURLPath) {
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatarURLPath] placeholderImage:model.avatarImage];
    } else {
        self.avatarView.image = model.avatarImage;
    }
}

- (void)setCustomBubbleView:(id<IMessageModel>)model
{
    [_bubbleView setupBusinessCardBubbleView];
    
    _bubbleView.imageView.image = [UIImage imageNamed:@"新的好友"];
    
}

- (void)updateCustomBubbleViewMargin:(UIEdgeInsets)bubbleMargin model:(id<IMessageModel>)model
{
    [_bubbleView updateBusinessCardMargin:bubbleMargin];
    _bubbleView.translatesAutoresizingMaskIntoConstraints = YES;
    
    CGFloat bubbleViewHeight = 75;// 气泡背景图高度
    CGFloat nameLabelHeight = 15;// 昵称label的高度
    
    if (model.isSender) {
        _bubbleView.frame =
        CGRectMake([UIScreen mainScreen].bounds.size.width - 273.5, nameLabelHeight, 213, bubbleViewHeight);
        //        self.bubbleView.userHeaderImageView.frame = CGRectMake(13, 19, 26, 34);
        //        self.bubbleView.userNameLabel.frame = CGRectMake(48, 19, 156, 15);
        //        self.bubbleView.userPhoneLabel.frame = CGRectMake(48, 41, 49, 12);
        //        self.bubbleView.line.frame = CGRectMake(13, 73, 200, 1);
        //        self.bubbleView.tipsLabel.frame = CGRectMake(145, 73, 80, 20);
    }else{
        _bubbleView.frame = CGRectMake(55, nameLabelHeight, 213, bubbleViewHeight);
        //        self.bubbleView.userHeaderImageView.frame = CGRectMake(20, 19, 26, 34);
        //        self.bubbleView.userNameLabel.frame = CGRectMake(55, 19, 156, 15);
        //        self.bubbleView.userPhoneLabel.frame = CGRectMake(55, 41, 49, 12);
        //        self.bubbleView.line.frame = CGRectMake(20, 73, 200, 1);
        //        self.bubbleView.tipsLabel.frame = CGRectMake(152, 73, 80, 20);
        
    }
    // 这里强制调用内部私有方法
    [_bubbleView _setupConstraintsXX];
    
}

+ (NSString *)cellIdentifierWithModel:(id<IMessageModel>)model
{
    return @"哈哈";
    //    return model.isSender ? @"__redPacketCellSendIdentifier__" : @"__redPacketCellReceiveIdentifier__";
}

+ (CGFloat)cellHeightWithModel:(id<IMessageModel>)model
{
    return kCellHeight;
}

- (void)setModel:(id<IMessageModel>)model
{
    [super setModel:model];
    
    NSDictionary *dict = model.message.ext;
    //    [model nickname]; // 谁发送的这个名片，谁向你推荐一张名片
    /*
     {
     cardUserAvatarURLString = xs;
     cardUserName = xs;
     cardUserPhone = xs;
     }
     
     */
    self.bubbleView.userNameLabel.text = dict[@"content"];
    self.bubbleView.userPhoneLabel.text =  dict[@"title"];
    NSString *str = dict[@"contentPic"];
    if (!dict[@"contentPic"]) {
         self.bubbleView.userHeaderImageView.image = [UIImage imageNamed:@"发布图"];
    }else
    {
        [self.bubbleView.userHeaderImageView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage: [UIImage imageNamed:@"发布图"]];
    }
   
    _hasRead.hidden = YES;//名片消息不显示已读
    //    _nameLabel = nil;// 不显示姓名
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSString *imageName = self.model.isSender ? @"RedpacketCellResource.bundle/redpacket_sender_bg" : @"RedpacketCellResource.bundle/redpacket_receiver_bg";
    UIImage *image = self.model.isSender ? [[UIImage imageNamed:imageName] stretchableImageWithLeftCapWidth:30 topCapHeight:35] :
    [[UIImage imageNamed:imageName] stretchableImageWithLeftCapWidth:20 topCapHeight:35];
    // 等待接入名片的背景图片
    //    self.bubbleView.backgroundImageView.image = image;
}
@end
