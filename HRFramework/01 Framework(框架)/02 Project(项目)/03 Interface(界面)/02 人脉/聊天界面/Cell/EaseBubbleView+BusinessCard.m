//
//  EaseBubbleView+BusinessCard.m
//  ICan
//
//  Created by 何壮壮 on 17/3/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "EaseBubbleView+BusinessCard.h"
#import <objc/runtime.h>

static char _userHeaderImageView_;
static char _userNameLabel_;
static char _userPhoneLabel_;
static char _line_;
static char _tipsLabel_;
@implementation EaseBubbleView (BusinessCard)
- (void)_setupConstraintsXX
{
    [self.marginConstraints removeAllObjects];
    
    //userHeaderImageView
    NSLayoutConstraint *userHeaderImageViewTopConstraint =
    [NSLayoutConstraint constraintWithItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.backgroundImageView
                                 attribute:NSLayoutAttributeTop
                                multiplier:1.0
                                  constant:10];
    
    NSLayoutConstraint *userHeaderImageViewLeadingConstraint =
    [NSLayoutConstraint constraintWithItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeLeading
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.backgroundImageView
                                 attribute:NSLayoutAttributeLeading
                                multiplier:1.0
                                  constant:10];
    
    [self.marginConstraints addObject:userHeaderImageViewTopConstraint];
    [self.marginConstraints addObject:userHeaderImageViewLeadingConstraint];
    
    NSLayoutConstraint *userHeaderImageViewHeightConstraint =
    [NSLayoutConstraint constraintWithItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:0.0
                                  constant:50];
    
    NSLayoutConstraint *userHeaderImageViewWidthConstraint =
    [NSLayoutConstraint constraintWithItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:0.0
                                  constant:70];
    
    [self.userHeaderImageView addConstraint:userHeaderImageViewHeightConstraint];
    [self.userHeaderImageView addConstraint:userHeaderImageViewWidthConstraint];
    
    // userNameLabel
    NSLayoutConstraint *userNameLabelWithMarginTopConstraint =
    [NSLayoutConstraint constraintWithItem:self.userNameLabel
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeTop
                                multiplier:1.0
                                  constant:5];
    
    NSLayoutConstraint *userNameLabelWithMarginRightConstraint =
    [NSLayoutConstraint constraintWithItem:self.userNameLabel
                                 attribute:NSLayoutAttributeTrailing
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.backgroundImageView
                                 attribute:NSLayoutAttributeTrailing
                                multiplier:1.0
                                  constant:-self.margin.right];
    
    NSLayoutConstraint *userNameLabelWithMarginLeftConstraint =
    [NSLayoutConstraint constraintWithItem:self.userNameLabel
                                 attribute:NSLayoutAttributeLeading
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.userHeaderImageView
                                 attribute:NSLayoutAttributeTrailing
                                multiplier:1.0
                                  constant:10];
    NSLayoutConstraint *userNameLabelWithMarginHeightConstraint =
    [NSLayoutConstraint constraintWithItem:self.userNameLabel
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:0.0
                                  constant:50];

    [self.marginConstraints addObject:userNameLabelWithMarginHeightConstraint];
    [self.marginConstraints addObject:userNameLabelWithMarginRightConstraint];
    [self.marginConstraints addObject:userNameLabelWithMarginTopConstraint];
    [self.marginConstraints addObject:userNameLabelWithMarginLeftConstraint];
    
    // userPhoneLabel
//    NSLayoutConstraint *userPhoneLabelTopConstraint =
//    [NSLayoutConstraint constraintWithItem:self.userPhoneLabel
//                                 attribute:NSLayoutAttributeTop
//                                 relatedBy:NSLayoutRelationEqual
//                                    toItem:self.userHeaderImageView
//                                 attribute:NSLayoutAttributeTop
//                                multiplier:5.0
//                                  constant:1];
//    
//    NSLayoutConstraint *userPhoneLabelLeftConstraint =
//    [NSLayoutConstraint constraintWithItem:self.userPhoneLabel
//                                 attribute:NSLayoutAttributeLeading
//                                 relatedBy:NSLayoutRelationEqual
//                                    toItem:self.userNameLabel
//                                 attribute:NSLayoutAttributeLeading
//                                multiplier:1.0
//                                  constant:0];
//    NSLayoutConstraint *userPhoneLabelRightConstraint =
//    [NSLayoutConstraint constraintWithItem:self.userPhoneLabel
//                                 attribute:NSLayoutAttributeTrailing
//                                 relatedBy:NSLayoutRelationEqual
//                                    toItem:self.backgroundImageView
//                                 attribute:NSLayoutAttributeTrailing
//                                multiplier:1.0
//                                  constant:-self.margin.right];
//    
//    [self.marginConstraints addObject:userPhoneLabelTopConstraint];
//    [self.marginConstraints addObject:userPhoneLabelLeftConstraint];
//    [self.marginConstraints addObject:userPhoneLabelRightConstraint];
    
    [self.userPhoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.userNameLabel.mas_left);
        make.top.mas_equalTo(self.userHeaderImageView.mas_top).offset(-8);
        make.right.mas_equalTo(self.userNameLabel.mas_right);
    }];
    
    
      [self addConstraints:self.marginConstraints];
    
    NSLayoutConstraint *backImageConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0f constant:260];
    
    [self.superview addConstraint:backImageConstraint];
    
    /*
     UIView *backImageView = self.backgroundImageView;
     NSArray *backgroundConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[backImageView]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(backImageView)];
     NSArray *backgroundConstraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[backImageView(==200)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(backImageView)];
     
     [self addConstraints:backgroundConstraints];
     [self addConstraints:backgroundConstraints2];
     */
}


#pragma mark - public
- (void)setupBusinessCardBubbleView {
    
    self.imageView.backgroundColor = [UIColor redColor];
    // 头像
    self.userHeaderImageView = [UIImageView new];
    [self.userHeaderImageView setImage:[UIImage imageNamed:@"新的好友"]];
    self.userHeaderImageView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.backgroundImageView addSubview:self.userHeaderImageView];
    // 内容
    self.userNameLabel = [UILabel new];
    self.userNameLabel.font = [UIFont systemFontOfSize:13.0f];
    self.userNameLabel.textColor = RGB0X(0X333333);
    self.userNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.userNameLabel.numberOfLines = 0;
    [self.backgroundImageView addSubview:self.userNameLabel];
    // 手机号
    self.userPhoneLabel = [UILabel new];
    self.userPhoneLabel.font = [UIFont systemFontOfSize:13.0f];
    self.userPhoneLabel.textColor = [UIColor darkGrayColor];
    self.userPhoneLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.backgroundImageView addSubview:self.userPhoneLabel];

    
    [self _setupConstraintsXX];
}
- (void)updateBusinessCardMargin:(UIEdgeInsets)margin
{
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    
    [self removeConstraints:self.marginConstraints];
    [self _setupConstraintsXX];
}

#pragma mark - getter and setter

- (void)setUserHeaderImageView:(UIImageView *)userHeaderImageView
{
    objc_setAssociatedObject(self, &_userHeaderImageView_, userHeaderImageView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIImageView *)userHeaderImageView
{
    return objc_getAssociatedObject(self, &_userHeaderImageView_);
}

- (void)setUserNameLabel:(UILabel *)userNameLabel
{
    objc_setAssociatedObject(self, &_userNameLabel_, userNameLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)userNameLabel
{
    return objc_getAssociatedObject(self, &_userNameLabel_);
}

- (void)setUserPhoneLabel:(UILabel *)userPhoneLabel
{
    objc_setAssociatedObject(self, &_userPhoneLabel_, userPhoneLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)userPhoneLabel
{
    return objc_getAssociatedObject(self, &_userPhoneLabel_);
}

- (void)setLine:(UIView *)line
{
    objc_setAssociatedObject(self, &_line_, line, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)line
{
    return objc_getAssociatedObject(self, &_line_);
}

- (void)setTipsLabel:(UILabel *)tipsLabel
{
    objc_setAssociatedObject(self, &_tipsLabel_, tipsLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel *)tipsLabel
{
    return objc_getAssociatedObject(self, &_tipsLabel_);
}
@end
