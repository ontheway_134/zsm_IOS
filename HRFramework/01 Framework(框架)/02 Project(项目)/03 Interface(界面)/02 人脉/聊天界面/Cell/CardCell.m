//
//  CardCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/22.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "CardCell.h"
#import "UIImageView+EMWebCache.h"
#import "EaseBubbleView.h"
static const CGFloat kCellHeight = 110.0f;
@interface CardCell()
@property (nonatomic,strong) UIImageView * headImgView;  //头像
@property (nonatomic,strong) UILabel * contentLabel;     //内容label
@property (nonatomic,strong) UILabel * cardLabel;        //个人名片
@property (nonatomic,strong) UIView * lineView;        //线
@property (nonatomic,strong) UILabel * personageLabel;    //个人名片

@end
@implementation CardCell
#pragma mark - IModelCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier model:(id<IMessageModel>)model
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier model:model];
    
    if (self) {
        self.hasRead.hidden = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return self;
}
- (BOOL)isCustomBubbleView:(id<IMessageModel>)model
{
    return YES;
}
- (void)setCustomModel:(id<IMessageModel>)model
{
    UIImage *image = model.image;
    if (!image) {
        [self.bubbleView.imageView sd_setImageWithURL:[NSURL URLWithString:model.fileURLPath] placeholderImage:[UIImage imageNamed:model.failImageName]];
    } else {
        _bubbleView.imageView.image = image;
    }
    
    if (model.avatarURLPath) {
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatarURLPath] placeholderImage:model.avatarImage];
    } else {
        self.avatarView.image = model.avatarImage;
    }
}

- (void)setCustomBubbleView:(id<IMessageModel>)model
{
    // 头像
    self.headImgView = [UIImageView new];
    [self.headImgView setImage:[UIImage imageNamed:@"新的好友"]];
    self.headImgView.translatesAutoresizingMaskIntoConstraints = NO;
    self.headImgView.layer.cornerRadius = 12.5;
    self.headImgView.layer.masksToBounds = YES;
    [self.bubbleView.backgroundImageView addSubview:self.headImgView];
    // 内容
    self.contentLabel = [UILabel new];
    self.contentLabel.font = [UIFont systemFontOfSize:13.0f];
    self.contentLabel.textColor = RGB0X(0X333333);
    self.contentLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.contentLabel.numberOfLines = 0;
    [self.bubbleView.backgroundImageView addSubview:self.contentLabel];
    //线
    self.lineView = [UIView new];
    self.lineView.backgroundColor=[UIColor lightGrayColor];
    self.lineView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bubbleView.backgroundImageView addSubview:self.lineView];
    //个人名片
    self.personageLabel = [UILabel new];
    self.personageLabel.font = [UIFont systemFontOfSize:13.0f];
    self.personageLabel.textColor = RGB0X(0X333333);
    self.personageLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.personageLabel.numberOfLines = 0;
    self.personageLabel.text = @"个人名片";
    [self.bubbleView.backgroundImageView addSubview:self.personageLabel];
    

    

    
}
- (void)updateCustomBubbleViewMargin:(UIEdgeInsets)bubbleMargin model:(id<IMessageModel>)model
{
    
    _bubbleView.translatesAutoresizingMaskIntoConstraints = YES;
    //头像
    [self.headImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bubbleView.backgroundImageView.mas_top).offset(10);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(25);
        make.left.mas_equalTo(self.bubbleView.backgroundImageView.mas_left).offset(10);
        
    }];
    
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bubbleView.backgroundImageView.mas_top).offset(10);
        make.left.mas_equalTo(self.bubbleView.backgroundImageView.mas_left).offset(50);
    }];
    

    
    [self.personageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bubbleView.backgroundImageView.mas_bottom).offset(-20);
        make.left.mas_equalTo(self.bubbleView.backgroundImageView.mas_left).offset(10);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.bubbleView.backgroundImageView.mas_top).offset(40);
        make.left.mas_equalTo(self.bubbleView.backgroundImageView.mas_left).offset(10);
        
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(self.bubbleView.backgroundImageView.mas_right).offset(-20);
        
    }];
    
    CGFloat bubbleViewHeight = 65;// 气泡背景图高度
    CGFloat nameLabelHeight = 15;// 昵称label的高度
    
    if (model.isSender) {
        _bubbleView.frame =
        CGRectMake([UIScreen mainScreen].bounds.size.width - 273.5+40/BASESCREENPX_WIDTH, nameLabelHeight, 170, bubbleViewHeight);
        //        self.bubbleView.userHeaderImageView.frame = CGRectMake(13, 19, 26, 34);
        //        self.bubbleView.userNameLabel.frame = CGRectMake(48, 19, 156, 15);
        //        self.bubbleView.userPhoneLabel.frame = CGRectMake(48, 41, 49, 12);
        //        self.bubbleView.line.frame = CGRectMake(13, 73, 200, 1);
        //        self.bubbleView.tipsLabel.frame = CGRectMake(145, 73, 80, 20);
    }else{
        _bubbleView.frame = CGRectMake(55, nameLabelHeight, 150, bubbleViewHeight);
        //        self.bubbleView.userHeaderImageView.frame = CGRectMake(20, 19, 26, 34);
        //        self.bubbleView.userNameLabel.frame = CGRectMake(55, 19, 156, 15);
        //        self.bubbleView.userPhoneLabel.frame = CGRectMake(55, 41, 49, 12);
        //        self.bubbleView.line.frame = CGRectMake(20, 73, 200, 1);
        //        self.bubbleView.tipsLabel.frame = CGRectMake(152, 73, 80, 20);
        
    }
    // 这里强制调用内部私有方法
   // [_bubbleView _setupConstraintsXX];
    
}
+ (NSString *)cellIdentifierWithModel:(id<IMessageModel>)model
{
    return @"个人名片";
    //    return model.isSender ? @"__redPacketCellSendIdentifier__" : @"__redPacketCellReceiveIdentifier__";
}

+ (CGFloat)cellHeightWithModel:(id<IMessageModel>)model
{
    return kCellHeight;
}
- (void)setModel:(id<IMessageModel>)model
{
    [super setModel:model];
    
    NSDictionary *dict = model.message.ext;
    //    [model nickname]; // 谁发送的这个名片，谁向你推荐一张名片
    /*
     {
     cardUserAvatarURLString = xs;
     cardUserName = xs;
     cardUserPhone = xs;
     }
     
     */
    self.contentLabel.text = dict[@"content"];
    
    NSString *str = dict[@"contentPic"];
    if (!dict[@"contentPic"]) {
        self.headImgView.image = [UIImage imageNamed:@"发布图"];
    }else
    {
        [self.headImgView sd_setImageWithURL:[NSURL URLWithString:str] placeholderImage: [UIImage imageNamed:@"发布图"]];
    }
    
    _hasRead.hidden = YES;//名片消息不显示已读
    //    _nameLabel = nil;// 不显示姓名
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSString *imageName = self.model.isSender ? @"RedpacketCellResource.bundle/redpacket_sender_bg" : @"RedpacketCellResource.bundle/redpacket_receiver_bg";
    UIImage *image = self.model.isSender ? [[UIImage imageNamed:imageName] stretchableImageWithLeftCapWidth:30 topCapHeight:85] :
    [[UIImage imageNamed:imageName] stretchableImageWithLeftCapWidth:20 topCapHeight:85];
    // 等待接入名片的背景图片
    //    self.bubbleView.backgroundImageView.image = image;
}

@end
