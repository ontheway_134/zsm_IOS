//
//  EaseBubbleView+BusinessCard.h
//  ICan
//
//  Created by 何壮壮 on 17/3/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "EaseBubbleView.h"

@interface EaseBubbleView (BusinessCard)
/**
 *  用户的头像
 */
@property (strong, nonatomic) UIImageView *userHeaderImageView;
/**
 *  用户的昵称
 */
@property (strong, nonatomic) UILabel *userNameLabel;
/**
 *  用户的手机号
 */
@property (strong, nonatomic) UILabel *userPhoneLabel;
/**
 *  分隔线
 */
@property (strong, nonatomic) UIView  *line;
/**
 *  提示字 “个人名片”
 */
@property (strong, nonatomic) UILabel *tipsLabel;

- (void)setupBusinessCardBubbleView;

- (void)updateBusinessCardMargin:(UIEdgeInsets)margin;

- (void)_setupConstraintsXX;
@end
