//
//  ICN_IndustryandModel.h
//  ICan
//
//  Created by shilei on 17/3/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_IndustryandModel : BaseOptionalModel

@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *infoid;
@property(nonatomic,strong)NSString *pic;
@property(nonatomic,strong)NSString *createDate;
@property(nonatomic,strong)NSString *pageViews;

@end
