//
//  ICN_PersonHubChatVCs.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PersonHubChatVCs.h"
#import "ICN_NewFridendsTableViewCell.h"
#import "ICN_MyTableViewCell.h"
#import "ICN_InterestTableViewCell.h"
#import "ICN_NewFriendViewController.h"
#import "ICN_MyfridentViewController.h"
#import "ICN_MyGroupViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_InterersPeople.h"
#import "ICN_NewFrident.h"
#import "ICN_StartViewController.h"
#import "ICN_GuiderPager.h" // 新版引导页
#import "ICN_SignViewController.h" // 登录注册页面
#import "ICN_UserHomePagerVC.h"
#import "ICN_ChatRecordViewController.h"   //聊天记录
#import "ICN_bearbyViewController.h"   //附近的人
#import "ICN_SeekPeopleViewController.h"  //找人
#import "ICN_DynSearchPagerVC.h"     //首页的搜索

@interface ICN_PersonHubChatVCs ()<UITableViewDelegate,UITableViewDataSource,ICN_BtnSelectedAddDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ContencTableView;
@property(nonatomic,strong)NSMutableArray *interesPeopleDate;
@property(nonatomic,strong)NSMutableArray *RecommendDate;
@property(nonatomic,strong)NSMutableArray *headDate;   //我的好友数组
@property(nonatomic,strong)NSMutableArray *headerArr;   //拼接后的数组
@property(nonatomic,strong)ICN_NewFridendsTableViewCell *cell;
@property(nonatomic,assign)NSInteger page;
@property(nonatomic,assign)NSInteger pages;
@property(nonatomic,strong)BaseOptionalModel *basemodel;
@property (nonatomic,strong) NSMutableArray * dataSourceArr; //临时数组
@end

@implementation ICN_PersonHubChatVCs
#pragma mark ----------------<#type#>---------------------
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
#pragma mark - --- 网络请求 ---
-(void)RecommendAF:(NSInteger)page{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    self.RecommendDate = nil;
    NSDictionary * dic = @{@"token":token};
    [[HRRequest manager]POST:PATH_SYSTEM para:dic success:^(id data) {
        NSLog(@"%@",data);
        if (self.RecommendDate.count == 0||self.RecommendDate == nil) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:obj error:nil];
                [self.RecommendDate addObject:model];
            }];
        }
        
        [self httpNetworingPage:page];
        NSLog(@"%@",self.RecommendDate);
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"感兴趣的人获取失败"];
    }];
}
//可能感兴趣的人
-(void)httpNetworingPage:(NSInteger)page{
    NSLog(@"%@",self.interesPeopleDate);
    NSDictionary *dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"page" :[NSString stringWithFormat:@"%ld",(long)page], @"pagenum" : @10};
    
    [[[HRNetworkingManager alloc] init ] POST_PATH:PATH_INTERPEOPLE params:dic success:^(id result) {
        
        self.basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (self.basemodel.code == 0) {
            
            if (self.page == 1) {
                [self.interesPeopleDate removeAllObjects];
                self.dataSourceArr = nil;
            }
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:dic error:nil];
                [self.dataSourceArr addObject:model];
            }
            NSLog(@"%@",self.interesPeopleDate);
            NSMutableArray *tempArr = [NSMutableArray array];
            tempArr = self.RecommendDate;
            [tempArr addObjectsFromArray:self.dataSourceArr];
            self.interesPeopleDate = tempArr;
            
            NSLog(@"%@",self.RecommendDate);
            [self.ContencTableView reloadData];
            
        }
        else{
            
        }
        [self.ContencTableView.mj_header endRefreshing];
        [self.ContencTableView.mj_footer endRefreshing];
    } failure:^(NSDictionary *errorInfo) {
        [self.ContencTableView.mj_header endRefreshing];
        [self.ContencTableView.mj_footer endRefreshing];
    }];
    
}
//新的好友
-(void)httpNetWorings:(NSInteger)page {
    
    NSDictionary *dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"status":@5,@"page" :[NSString stringWithFormat:@"%ld",(long)page], @"pagenum" : @3,@"black":@2};
    
    [[[HRNetworkingManager alloc] init ] POST_PATH:PATH_NEWFRIEND params:dic success:^(id result) {
        
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            self.headDate = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_NewFrident *model = [[ICN_NewFrident alloc] initWithDictionary:dic error:nil];
                [self.headDate addObject:SF(@"%@",model.memberLogo)];
            }
            [self.headerArr removeAllObjects];
            [self.headDate enumerateObjectsUsingBlock:^(NSString *imgStr, NSUInteger idx, BOOL * _Nonnull stop) {
                [self.headerArr addObject:ICN_IMG(imgStr)];
            }];
            //图片的一张张的赋值；
            if (self.headerArr.count == 1) {
                [self.cell.oneHeadInamgView sd_setImageWithURL:self.headerArr[0] placeholderImage:[UIImage imageNamed:@"占位图"]];
                self.cell.twoImageView.image = nil;
                self.cell.threeImageview.image = nil;
                return ;
            }
            if (self.headerArr.count == 2) {
                
                [self.cell.oneHeadInamgView sd_setImageWithURL:self.headerArr[0] placeholderImage:[UIImage imageNamed:@"占位图"]];
                [self.cell.twoImageView sd_setImageWithURL:self.headerArr[1] placeholderImage:[UIImage imageNamed:@"头像"]];
                self.cell.threeImageview.image = nil;
                return;
            }
            
            if (self.headerArr.count == 3) {
                [self.cell.oneHeadInamgView sd_setImageWithURL:self.headerArr[0] placeholderImage:[UIImage imageNamed:@"占位图"]];
                [self.cell.twoImageView sd_setImageWithURL:self.headerArr[1] placeholderImage:[UIImage imageNamed:@"头像"]];
                [self.cell.threeImageview sd_setImageWithURL:self.headerArr[2] placeholderImage:[UIImage imageNamed:@"占位图"]];
                return;
            }
            [self.ContencTableView reloadData];
        }
        else{
            
            self.cell.oneHeadInamgView.image = nil;
            self.cell.twoImageView.image = nil;
            self.cell.threeImageview.image = nil;
            [self.ContencTableView.mj_header endRefreshing];
            [self.ContencTableView.mj_footer endRefreshing];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
    }];
    
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
    } else{
        
        [self.navigationController setNavigationBarHidden:YES];
        [self.ContencTableView registerNib:[UINib nibWithNibName:@"ICN_NewFridendsTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NewFridendsTableViewCell"];
        self.ContencTableView.delegate=self;
        self.ContencTableView.dataSource=self;
        self.ContencTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        //[self refresh];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
    } else{
        
        [self.navigationController setNavigationBarHidden:YES];
        [self refresh];
    }
    
}

#pragma mark - --- Protrocol ---

#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.model.memberId};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
        
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [btn setTitle:@"等待验证" forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 5;
            break;
        case 1:
            return self.interesPeopleDate.count;
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            
            switch (indexPath.row) {
                case 0:{
                    self.cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_NewFridendsTableViewCell"];
                    if (self.cell==nil) {
                        self.cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_NewFridendsTableViewCell" owner:self options:nil] lastObject];
                    }
                    return self.cell;
                }
                    break;
                    
                case 1:{
                    ICN_MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_MyTableViewCell"];
                    if (cell==nil) {
                        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_MyTableViewCell" owner:self options:nil] lastObject];
                    }
                    cell.MyImageView.image=[UIImage imageNamed:@"我的好友"];
                    cell.MyLabel.text=@"我的好友";
                    return cell;
                }
                    break;
                case 2:{
                    ICN_MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_MyTableViewCell"];
                    if (cell==nil) {
                        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_MyTableViewCell" owner:self options:nil] lastObject];
                    }
                    
                    cell.MyImageView.image=[UIImage imageNamed:@"我的小组"];
                    cell.MyLabel.text=@"我的小组";
                    return cell;
                    
                }
                    break;
                case 3: {
                    ICN_MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_MyTableViewCell"];
                    if (cell==nil) {
                        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_MyTableViewCell" owner:self options:nil] lastObject];
                    }
                    
                    cell.MyImageView.image=[UIImage imageNamed:@"附近的人"];
                    cell.MyLabel.text=@"附近的人";
                    return cell;
                }
                    break;
                    
                    
                case 4: {
                    ICN_MyTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_MyTableViewCell"];
                    if (cell==nil) {
                        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_MyTableViewCell" owner:self options:nil] lastObject];
                    }
                    
                    cell.MyImageView.image=[UIImage imageNamed:@"聊天记录"];
                    cell.MyLabel.text=@"聊天记录";
                    return cell;
                }
                    break;
                    
            }
        }
            
            break;
            
        case 1:{
            
            ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
            if (cell==nil) {
                cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
            }
            cell.model=self.interesPeopleDate[indexPath.row];
            cell.delegate=self;
            return cell;
        }
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            
            switch (indexPath.row) {
                case 0:
                    return 60;
                    break;
                    
                case 1:
                case 2:
                case 3:
                case 4:
                    return 45;
                    break;
            }
        }
            break;
            
        case 1:{
            return 57;
            
        }
            break;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            
            switch (indexPath.row) {
                case 0:{
                    ICN_NewFriendViewController *newFrident=[[ICN_NewFriendViewController alloc]init];
                    [self.navigationController pushViewController:newFrident animated:YES];
                }
                    break;
                    
                case 1:{
                    
                    ICN_MyfridentViewController *myfrident=[[ICN_MyfridentViewController alloc]init];
                    [self.navigationController pushViewController:myfrident animated:YES];
                    
                }
                    break;
                case 2:{
                    
                    ICN_MyGroupViewController *group = [[ICN_MyGroupViewController alloc]init];
                    [self.navigationController pushViewController:group animated:YES];
                    
                }
                    break;
                case 3:{
                    
                    NSLog(@"附近的人");
                    ICN_bearbyViewController *bearby=[[ICN_bearbyViewController alloc]init];
                    [self.navigationController pushViewController:bearby animated:YES];
                }
                    break;
                case 4:{
                    // 判断没有网不跳转
                    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
                        // 没有网络
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
                        return ;
                    }
                    ICN_ChatRecordViewController *record = [[ICN_ChatRecordViewController alloc] init];
                    [self.navigationController pushViewController:record animated:YES];
                }
                    break;
            }
            
        }
            
            break;
            
        case 1:{
            
            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
            ICN_InterestTableViewCell *cell= (ICN_InterestTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            if (cell.model.memberId == nil) {
                userhome.memberId= cell.model.packID;
            }else{
                userhome.memberId= cell.model.memberId;
            }
            
            [self.navigationController pushViewController:userhome animated:YES];
        }
            break;
            
    }
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return @"可能感兴趣的人";
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    if (section == 1) {
        UITableViewHeaderFooterView *v = (UITableViewHeaderFooterView *)view;
        v.backgroundView.backgroundColor = RGB0X(0xf0f0f0);
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0.01f;
            break;
            
        case 1:
            return 32;
            break;
    }
    return 0;
}

#pragma mark - --- 自定义方法 ---

-(void)refresh{
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        self.page = 1;
        self.pages = 1;
       // [self httpNetworingPage:self.page];
        [self httpNetWorings:self.pages];
       
        /*推荐的人接口*/
        [self RecommendAF:1];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.ContencTableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        self.page+=1;
       // [self httpNetworingPage:self.page];
        /*推荐的人接口*/
        [self RecommendAF:self.page];
    }];
    // 设置footer的标识 MJRefreshAutoNormalFooter
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.ContencTableView.mj_footer = footer;
    [self.ContencTableView.mj_header beginRefreshing];
    
}

#pragma mark - --- IBActions ---

//找人
- (IBAction)searcherBtnAction:(id)sender {
    ICN_DynSearchPagerVC *seekpeople=[[ICN_DynSearchPagerVC alloc]init];
    [self.navigationController pushViewController:seekpeople animated:YES];
}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)interesPeopleDate{
    if (!_interesPeopleDate) {
        _interesPeopleDate=[NSMutableArray array];
    }
    return _interesPeopleDate;
}
-(NSMutableArray *)RecommendDate{
    if (!_RecommendDate) {
        _RecommendDate=[NSMutableArray array];
    }
    return _RecommendDate;
}

-(NSMutableArray *)headDate{
    if (!_headDate) {
        _headDate=[NSMutableArray array];
    }
    return _headDate;
}

-(NSMutableArray *)headerArr{
    if (!_headerArr) {
        _headerArr=[NSMutableArray array];
    }
    return _headerArr;
    
}


@end
