//
//  ICN_NewFridendsTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_NewFridendsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *oneHeadInamgView;
@property (weak, nonatomic) IBOutlet UIImageView *twoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *threeImageview;

@end
