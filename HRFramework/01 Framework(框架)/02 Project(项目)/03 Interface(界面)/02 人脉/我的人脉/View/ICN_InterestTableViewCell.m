//
//  ICN_InterestTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_InterestTableViewCell.h"

@interface ICN_InterestTableViewCell ()

@property (nonatomic , strong) UITapGestureRecognizer *iconTapGes;

@end

@implementation ICN_InterestTableViewCell

- (UITapGestureRecognizer *)iconTapGes{
    if (_iconTapGes == nil) {
        _iconTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnHeaderImageView)];
        [self.headImageview addGestureRecognizer:_iconTapGes];
    }
    return _iconTapGes;
}

// 点击用户头像触发的方法
- (void)tapOnHeaderImageView{
    if (self.iconBlock != nil && self.model) {
        self.iconBlock(SF(@"%@",self.model.friendId));
    }
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headImageview.layer.cornerRadius = 17.5;
    self.headImageview.layer.masksToBounds = YES;
    self.level.layer.cornerRadius = 3.0;
    self.level.layer.masksToBounds = YES;
}


- (IBAction)AddClickBtn:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(ICN_BtnSelectedAddDelegate:and:)]) {
        [self.delegate ICN_BtnSelectedAddDelegate:sender and:self];
    }
}


//我的好友
-(void)setMymodel:(ICN_MyyFridentModel *)mymodel{
    _mymodel = mymodel;
    self.level.hidden = YES;
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(mymodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    // 添加用户头像的点击手势
    [self iconTapGes];
    //    self.nickNameLabel.text=mymodel.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",mymodel.companyName ? mymodel.companyName : mymodel.memberSchool,mymodel.memberPosition ? mymodel.memberPosition : mymodel.memberMajor];
    self.companyLabel.text=str;
}

//可能感兴趣的人
-(void)setModel:(ICN_InterersPeople *)model{
    _model=model;
    // 添加用户头像的点击手势
    [self iconTapGes];
    if ([_model.plusv isEqualToString:@"0"]) {
        self.memberImageView.hidden = YES;
        
    }else{
        self.memberImageView.hidden = NO;
    }
    
    // 设置默认按钮是可以点击的
    self.addFridentBtn.userInteractionEnabled = YES;
    
    // _model.isMe 存在的话证明是一种Model 在里面进行全部逻辑判断
    if (_model.isMe) {
        
        // 显示添加好友按钮
        self.addFridentBtn.hidden = NO;
        // 判断是否显示加好友的按钮 - 已经是好友则不显示
        if ([_model.status isEqualToString:@"2"]) {
            // 如果已添加的话不能再次点击这个按钮
            self.addFridentBtn.hidden = YES;
        }else{
            // 其他状态需要显示加好友的按钮
            self.addFridentBtn.hidden = NO;
        }
        // 根据isMe属性进行判断
        if (model.isMe.integerValue == 1) {
            // 是自己 - 隐藏状态按钮
            self.addFridentBtn.hidden = YES;
        }else{
            self.addFridentBtn.hidden = NO;
        }
    }
    
    // 在添加好友的功能按钮没有隐藏的情况下做如下判断
    if (self.addFridentBtn.hidden == NO) {
        if (_model.friendStatus == nil) {
            if ([_model.status isEqualToString:@"1"]) {
                self.addFridentBtn.userInteractionEnabled = NO;
                [self.addFridentBtn setTitle:@"等待验证" forState:UIControlStateNormal];
            }
        }
        if ([_model.friendStatus isEqualToString:@"1"]) {
            self.addFridentBtn.userInteractionEnabled = NO;
            [self.addFridentBtn setTitle:@"等待验证" forState:UIControlStateNormal];
        }
        
        if ([_model.status isEqualToString:@"2"]) {
            // 如果已添加的话不能再次点击这个按钮
            self.addFridentBtn.userInteractionEnabled = NO;
            [self.addFridentBtn setTitle:@"已添加" forState:UIControlStateNormal];
        }
        
        if ([_model.status isEqualToString:@"5"]) {
            self.addFridentBtn.hidden=YES;
        }else{
            self.addFridentBtn.hidden=NO;
        }

    }

    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_model.memberNick;
    // 设置公司职位标签的内容
    NSString *companyText = @"";
    if (_model.companyName != nil && ![_model.companyName isEqualToString:@""]) {
        companyText = SF(@"%@ | %@",_model.companyName , _model.memberPosition ? _model.memberPosition : @"无职位");
    }else if (_model.memberSchool != nil && ![_model.memberSchool isEqualToString:@""]){
        companyText = SF(@"%@ | %@",_model.memberSchool , _model.memberMajor ? _model.memberMajor : @"无专业");
    }else{
        companyText = @"无 | 无";
    }
    self.companyLabel.text = companyText;
    // 设置好友纬度为位置
    if ([_model.level isEqualToString:@"1"]) {
        self.level.text = @"1度";
    }
    if ([_model.level isEqualToString:@"2"]) {
        self.level.text = @"2度";
    }
    if ([_model.level isEqualToString:@"3"]) {
        self.level.text = @"3度";
    }
    
}

//我的小组详情页面我的组员model
-(void)setGroupDdetialmodel:(ICN_GroupDetialModel *)groupDdetialmodel{
    _groupDdetialmodel=groupDdetialmodel;
    
    //判断是否是会员
    if ([_groupDdetialmodel.plusv isEqualToString:@"1"]) {
        self.memberImageView.hidden = NO;
        
    }else{
        self.memberImageView.hidden = YES;
    }
    //判断是否是好友 关系(1:未审核[申请者]  2:已审核   3:被拒绝  4:已删除  5:待审核[被申请者] 6:已拒绝 )[没关系则没有此数据]
    
    
    // 判断该组员信息是不是自己（本人）
    if (_groupDdetialmodel.isMe != nil && _groupDdetialmodel.isMe.integerValue == 1) {
        // 是自己 - 隐藏状态按钮
        self.addFridentBtn.hidden = YES;
    }else{
#warning make sure groupItems Status(while is not me) 这里的数据为了和前面的数据同意 只确定1的状态为等待验证
        
        self.addFridentBtn.hidden = NO;
        if ([groupDdetialmodel.friendStatus isEqualToString:@"2"]) {
            [self.addFridentBtn setTitle:@"已是好友关系" forState:UIControlStateNormal];
            
            self.addFridentBtn.userInteractionEnabled = NO;
        }
        if ([_groupDdetialmodel.friendStatus isEqualToString:@"3"]) {
            [self.addFridentBtn setTitle:@"被拒绝" forState:UIControlStateNormal];
            self.addFridentBtn.userInteractionEnabled = NO;
        }
        if ([_groupDdetialmodel.friendStatus isEqualToString:@"4"]) {
            [self.addFridentBtn setTitle:@"已删除" forState:UIControlStateNormal];
            self.addFridentBtn.userInteractionEnabled = NO;
        }
        if ([_groupDdetialmodel.friendStatus isEqualToString:@"1"]) {
            [self.addFridentBtn setTitle:@"等待审核" forState:UIControlStateNormal];
            self.addFridentBtn.userInteractionEnabled = NO;
        }
        if ([_groupDdetialmodel.friendStatus isEqualToString:@"6"]) {
            [self.addFridentBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
            self.addFridentBtn.userInteractionEnabled = NO;
        }
        
    }
    
    
    
    
    
    
    
    
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_groupDdetialmodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_groupDdetialmodel.memberNick;
    
    
    if (_groupDdetialmodel.companyName == nil) {
        
        NSString *str=[NSString stringWithFormat:@"| %@",_groupDdetialmodel.memberPosition];
        self.companyLabel.text=str;
        NSLog(@"大家啊");
    }
    
    // 这里根据是否已经工作是根据公司的状态去判断的，正常应该是根据某个字段判断具体显示哪个，这块还需要做修改
    if ([_groupDdetialmodel.companyName isEqualToString:@""] && _groupDdetialmodel.memberSchool !=nil) {
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_groupDdetialmodel.memberSchool,_groupDdetialmodel.memberMajor];
        self.companyLabel.text=str;
        return;
    }
    
    
    
    if (_groupDdetialmodel.memberPosition == nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | ",_groupDdetialmodel.companyName];
        self.companyLabel.text=str;
    }
    if (_groupDdetialmodel.memberPosition ==nil && _groupDdetialmodel.companyName ==nil){
        
        self.companyLabel.text=@"";
        
    }
    if (_groupDdetialmodel.memberPosition !=nil && _groupDdetialmodel.companyName !=nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_groupDdetialmodel.companyName,_groupDdetialmodel.memberPosition];
        self.companyLabel.text=str;
    }
    
    
    
}

-(void)setCommonFridentModel:(ICN_CommonFriendsModel *)commonFridentModel{
    _commonFridentModel = commonFridentModel;
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_commonFridentModel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = _commonFridentModel.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",_commonFridentModel.companyName,_commonFridentModel.memberPosition];
    self.companyLabel.text=str;
    
}

//联系人的model

- (void)setContactAddressModel:(ICN_ALBAddressModel *)contactAddressModel{
    _contactAddressModel = contactAddressModel;
    
    // 隐藏牛人认证按钮
    self.memberImageView.hidden = YES;
    // 设置用户昵称
    self.nickNameLabel.text = _contactAddressModel.name;
    // 判断是不是会员
    if (_contactAddressModel.memberStatus.integerValue == 0) {
        // 不是会员
        [self.addFridentBtn setTitle:@"邀请" forState:UIControlStateNormal];
        self.companyLabel.text=@"";
        self.headImageview.image = [UIImage imageNamed:@"logo"];
    }else{
        // 是会员
        if (_contactAddressModel.friendStatus.integerValue == 0) {
            // 不是好友
            [self.addFridentBtn setTitle:@"加好友" forState:UIControlStateNormal];
        }else{
            self.addFridentBtn.hidden = YES;
        }
        [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_contactAddressModel.headUrl)] placeholderImage:[UIImage imageNamed:@"logo"]];
        self.companyLabel.text = _contactAddressModel.memberWorkMsg;
    }
    
    
}

-(void)setContectmodel:(ICN_contactModel *)contectmodel{
    _contectmodel=contectmodel;
    
    self.memberImageView.hidden = YES;
    if (contectmodel.memberId ==nil) {
        [self.addFridentBtn setTitle:@"邀请" forState:UIControlStateNormal];
        self.companyLabel.text=@"";
    }else{
        
        [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(contectmodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        self.nickNameLabel.text = contectmodel.memberNick;
        NSString *str=[NSString stringWithFormat:@"%@ | %@",contectmodel.companyName,contectmodel.memberPosition];
        self.companyLabel.text=str;
    }
    
}

// 点击头像的回调block
- (void)callBackIconTapBlock:(IconTapBlock)block{
    self.iconBlock = block;
}

@end
