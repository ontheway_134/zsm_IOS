//
//  ICN_InterestTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_InterersPeople.h"
#import "ICN_MyyFridentModel.h"
#import "ICN_GroupDetialModel.h"
#import "ICN_CommonFriendsModel.h"
#import "ICN_ALBAddressModel.h" // 联系人Model

#import "ICN_contactModel.h"    //联系人的model

@class ICN_InterestTableViewCell;

typedef void(^IconTapBlock)(NSString *memberId); // 点击头像的回调block

@protocol ICN_BtnSelectedAddDelegate <NSObject>

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell;

@end

@interface ICN_InterestTableViewCell : UITableViewCell

@property (nonatomic , copy)IconTapBlock iconBlock;


@property (weak, nonatomic) IBOutlet UILabel *level;   //水平
@property (weak, nonatomic) IBOutlet UIImageView *headImageview;   //头像
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;       //昵称
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;        //公司名
@property (weak, nonatomic) IBOutlet UIImageView *memberImageView;    //是否事会员

@property (weak, nonatomic) IBOutlet UIButton *addFridentBtn;      //添加按钮

@property(nonatomic,weak)id<ICN_BtnSelectedAddDelegate>delegate;

@property(nonatomic,strong)ICN_MyyFridentModel *mymodel;

@property(nonatomic,strong)ICN_InterersPeople *model;  //可能感兴趣的人的model
@property (nonatomic , strong)ICN_ALBAddressModel *contactAddressModel; // 联系人数据Model



@property(nonatomic,strong)ICN_GroupDetialModel *groupDdetialmodel;

@property(nonatomic,strong)ICN_CommonFriendsModel *commonFridentModel;   //共同好友的model

@property(nonatomic,strong)ICN_contactModel *contectmodel;   //联系人的model

- (void)callBackIconTapBlock:(IconTapBlock)block;

@end
