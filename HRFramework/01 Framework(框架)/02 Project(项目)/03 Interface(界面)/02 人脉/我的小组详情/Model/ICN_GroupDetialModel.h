//
//  ICN_GroupDetialModel.h
//  ICan
//
//  Created by shilei on 16/12/30.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_GroupDetialModel : BaseOptionalModel

@property (nonatomic , copy)NSString *isMe; // 组员信息是不是自己(0 不是 - 1 是)，是的话做对应的操作

@property(nonatomic,strong)NSString *companyName;  //公司名称
@property(nonatomic,strong)NSString *groupId;   //组id
@property(nonatomic,strong)NSString *groupLogo;   //组logo
@property(nonatomic,strong)NSString *groupName;   //组名字
@property(nonatomic,strong)NSString *memberLogo;   //会员的logo
@property(nonatomic,strong)NSString *memberNick;    //会员的昵称
@property(nonatomic,strong)NSString *memberSchool;   //会员的学校
@property(nonatomic,strong)NSString *memberPosition;    //会员的职位
@property(nonatomic,strong)NSString *number;
@property(nonatomic,strong)NSString *summary;

@property(nonatomic,strong)NSString *groupRole;      //组员角色 小组成员角色：1创建者，2管理员，3普通成员

@property(nonatomic,strong)NSString *memberCount;    //小组人数

@property(nonatomic,strong)NSString *plusv;

@property(nonatomic,strong)NSString *friendStatus;     //与当前好友的状态

@property(nonatomic,strong)NSString *memberMajor;


@end
