//
//  ICN_EditGroupMessageView.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EditGroupMessageView.h"
#import "BaseOptionalModel.h"

@interface ICN_EditGroupMessageView()<UITextViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIButton *CommitBtn;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end


@implementation ICN_EditGroupMessageView

//判断新最大组员数是否小于当前组员数接口
//此处还没有验证修改的小组名是否存在，创建小组时有验证

-(void)httpcheckGroupPeopleCount{
    
    //用[NSUserDefaults standardUserDefaults] 是为了获取到controller上面的groupid
    NSString *groupid=[[NSUserDefaults standardUserDefaults] objectForKey:@"groupID"];
    
    NSDictionary *dic=@{@"number":self.groupCount.text,@"groupId":groupid};
    
   [[[HRNetworkingManager alloc] init] POST_PATH:PATH_JUDEGGROPU params:dic success:^(id result) {
       
       BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
       if (basemodel.code == 0) {
           
       }else{
           [MBProgressHUD ShowProgressWithBaseView:self Message:@"修改人数不能小于之前的人数"];
       }
       
   } failure:^(NSDictionary *errorInfo) {
       NSLog(@"请求失败");
   }];

}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.backView.layer.cornerRadius = 5.0;
    self.backView.layer.masksToBounds = YES;
    self.CommitBtn.layer.cornerRadius = 5.0;
    self.CommitBtn .layer.masksToBounds = YES;
    self.headImageView.layer.cornerRadius = 25.0;
    self.headImageView.layer.masksToBounds = YES;
    self.GroupIntroView.delegate=self;
    self.groupCount.delegate =self;
    NSString  * nsTextContent=self.GroupIntroView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
    
}

#pragma mark - --- IBActions ---

//是否公开的选择事件
- (IBAction)selectedBtn:(UIButton *)sender {
    
    sender.selected =!sender.selected;
    
    if ([sender isEqual:self.confirmBtn]) {
        
        self.confirmBtn.selected = YES;
        self.cancerBtn.selected = NO;
        self.openNumber = 0;
        
    }else{
        
        self.cancerBtn.selected = YES;
        self.confirmBtn.selected = NO;
        self.openNumber = 1;
    }

}


- (IBAction)removeBtn:(id)sender {
    [self removeFromSuperview];
}

- (IBAction)CommitBtnActions:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(ICN_BtnSCommitDelegate:)]) {
        [self.delegate ICN_BtnSCommitDelegate:sender];
    }
}

#pragma mark UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
   
    CGRect frame = textView.frame;
    float  height = [ self heightForTextView:textView WithText:textView.text];
    frame.size.height = height;
    [UIView animateWithDuration:0.5 animations:^{
        
        textView.frame = frame;
        
    } completion:nil];
    
    if (range.location>=60){
        return  NO;
    }
    else{
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
}

- (float) heightForTextView: (UITextView *)textView WithText: (NSString *) strText{
    CGSize constraint = CGSizeMake(textView.contentSize.width , CGFLOAT_MAX);
    CGRect size = [strText boundingRectWithSize:constraint
                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                     attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14]}
                                        context:nil];
    float textHeight = size.size.height + 22.0;
    return textHeight;
}


#pragma mark UITextFieldDelegate


-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self httpcheckGroupPeopleCount];
}


@end
