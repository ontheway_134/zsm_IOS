//
//  ICN_GroupDetialHeaderView.h
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_GroupDetialModel.h"


@class ICN_GroupDetialHeaderView;

@protocol ICN_BtnSelectedDelegate <NSObject>

-(void)ICN_BtnSelectedDelegate:(UIButton *)btn and:(ICN_GroupDetialHeaderView *)detialHeader;

@end

@interface ICN_GroupDetialHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *groupImage;//组的logo

@property (weak, nonatomic) IBOutlet UILabel *groupNick;//组的昵称

@property (weak, nonatomic) IBOutlet UILabel *groupPeopleCount;

@property (weak, nonatomic) IBOutlet UILabel *groupIntroduce;   //组的介绍

@property (weak, nonatomic) IBOutlet UIButton *editBtn;  //为了隐藏显示

@property (weak, nonatomic) IBOutlet UIButton *addGroupBtn;   //为了显示隐藏无其他作用


@property(nonatomic,weak)id<ICN_BtnSelectedDelegate>delegate;

@property(nonatomic,strong)ICN_GroupDetialModel *model;
@property (weak, nonatomic) IBOutlet UIView *GroupDetialView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupAuto;

@property (weak, nonatomic) IBOutlet UIImageView *imageview01;
@property (weak, nonatomic) IBOutlet UIImageView *imageview02;
@property (weak, nonatomic) IBOutlet UIImageView *imageview03;





@end
