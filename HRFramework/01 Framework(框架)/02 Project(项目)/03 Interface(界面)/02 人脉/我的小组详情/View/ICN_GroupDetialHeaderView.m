//
//  ICN_GroupDetialHeaderView.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_GroupDetialHeaderView.h"

@implementation ICN_GroupDetialHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.groupImage.layer.cornerRadius = 25.0f;
    self.groupImage.layer.masksToBounds = YES;
    self.imageview01.layer.cornerRadius = 14.0f;
    self.imageview01.layer.masksToBounds = YES;
    self.imageview02.layer.cornerRadius = 14.0f;
    self.imageview02.layer.masksToBounds = YES;
    self.imageview03.layer.cornerRadius = 14.0f;
    self.imageview03.layer.masksToBounds = YES;
}


- (IBAction)ClickBtnActions:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(ICN_BtnSelectedDelegate:and:)]) {
        [self.delegate ICN_BtnSelectedDelegate:sender and:self];
    }
}

-(void)setModel:(ICN_GroupDetialModel *)model{
    _model=model;
    //[self.groupImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.groupLogo)] placeholderImage:[UIImage imageNamed:@"头像"]];
    self.groupNick.text=_model.groupName;
    self.groupPeopleCount.text=_model.number;
    self.groupIntroduce.text=_model.summary;
}

@end
