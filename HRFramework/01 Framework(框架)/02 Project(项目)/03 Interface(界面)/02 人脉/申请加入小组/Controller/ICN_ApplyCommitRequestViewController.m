//
//  ICN_ApplyCommitRequestViewController.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ApplyCommitRequestViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_ApplyModel.h"
#import "ICN_buyaogao.h"

@interface ICN_ApplyCommitRequestViewController ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *CommitTextView;
@property (weak, nonatomic) IBOutlet UIButton *CommitBtn;

@end

@implementation ICN_ApplyCommitRequestViewController

#pragma mark - --- 网络请求 ---

-(void)httpApplyGroup{

    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupId":self.groupId,@"content":self.CommitTextView.text};

    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_APPLYGROUP params:dic success:^(id result) {
        ICN_buyaogao *basemodel=[[ICN_buyaogao alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [self.navigationController popViewControllerAnimated:YES];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"申请成功"];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.result];
        }

    } failure:^(NSDictionary *errorInfo) {

    }];
}



#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.CommitBtn.layer.cornerRadius = 5.0f;
    self.CommitBtn.layer.masksToBounds=YES;
    [self.navigationController setNavigationBarHidden:YES];
    self.CommitTextView.layer.borderColor = RGB0X(0xf0f0f0).CGColor;
    NSString *str=[NSString stringWithFormat:@"申请加入%@",self.gtoupname];
    self.applyLabel.text=str;
    self.CommitTextView.delegate =self;
    NSString  * nsTextContent=self.CommitTextView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}

#pragma mark - --- Protocol ---

#pragma mark UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=60){
        return  NO;
    }
    else{
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.countLabel.text=[NSString stringWithFormat:@"%ld/60",existTextNum];
}


#pragma mark - --- IBActions ---

- (IBAction)backBtnActions:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)CommitClickActions:(id)sender {
     //正常这里应该判断是否添加了申请的信息,
    [self httpApplyGroup];
}



@end
