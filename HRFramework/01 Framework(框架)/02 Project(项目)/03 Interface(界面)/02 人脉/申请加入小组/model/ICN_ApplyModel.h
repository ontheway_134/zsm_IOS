//
//  ICN_ApplyModel.h
//  ICan
//
//  Created by shilei on 17/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"
@protocol ICN_ApplyResultModel
@end
@interface ICN_ApplyResultModel:BaseOptionalModel
@property (nonatomic , copy)NSString *pic;
@property (nonatomic , copy)NSString *src;
@property (nonatomic , copy)NSString *title;

@end
@interface ICN_ApplyModel : BaseOptionalModel


@property(nonatomic,strong)ICN_ApplyResultModel<ICN_ApplyResultModel> *result;

@end
