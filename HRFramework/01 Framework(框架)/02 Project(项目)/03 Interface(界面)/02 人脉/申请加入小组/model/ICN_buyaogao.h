//
//  ICN_buyaogao.h
//  ICan
//
//  Created by shilei on 17/1/22.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ICN_buyaogao : JSONModel

@property(nonatomic,strong)NSString *info;
@property(nonatomic,strong)NSString *result;
@property(nonatomic,assign)NSInteger code;

@end
