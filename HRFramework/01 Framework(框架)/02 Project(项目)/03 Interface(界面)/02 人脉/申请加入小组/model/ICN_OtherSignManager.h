//
//  ICN_OtherSignManager.h
//  ICan
//
//  Created by shilei on 17/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

typedef void (^Complete)(NSString *token);


@interface ICN_OtherSignManager : JSONModel


+ (instancetype)sharedInstance;
//微信
- (void)weChatSign:(UIViewController *)object comple:(Complete)comple;
/** QQ登录 */
- (void)QQSign:(UIViewController *)object comple:(Complete)comple;
//微博
-(void)Sina:(UIViewController *)object comple:(Complete)comple;

@end
