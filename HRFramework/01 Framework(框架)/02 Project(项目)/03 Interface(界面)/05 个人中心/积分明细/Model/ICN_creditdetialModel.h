//
//  ICN_creditdetialModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_creditdetialModel : BaseOptionalModel
@property (strong,nonatomic)NSString * createTime;
@property (strong,nonatomic)NSString * intro;
@property (strong,nonatomic)NSString * score;
@property (strong,nonatomic)NSString * type;
@property (strong,nonatomic)NSString * memberId;
@end
