//
//  ICN_creditdetialViewController.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_creditdetialViewController.h"
#import "ICN_CreditTableViewCell.h"

@interface ICN_creditdetialViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (weak, nonatomic) IBOutlet UILabel *PromptLabel;/*提示文字*/
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;

@property (weak, nonatomic) IBOutlet UITableView *creditDetialTableView;

@property(nonatomic,strong)NSMutableArray *creditArr;
@end
@implementation ICN_creditdetialViewController

#pragma mark - --- 生命周期 ---
- (NSMutableArray *)baseSource{
    if (!_baseSource) {
        _baseSource = [NSMutableArray array];
    }
    return _baseSource;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.creditDetialTableView.hidden = NO;
    self.creditDetialTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self netWorkRequest:TYPE_RELOADDATA_UP];
    }];
    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    self.creditDetialTableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.creditDetialTableView.mj_footer.hidden = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}
#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 积分明细网络请求
- (void)netWorkRequest:(BOOL)isMore{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_ScoreRecordList para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_creditdetialModel * model=[[ICN_creditdetialModel alloc]initWithDictionary:obj error:nil];
                [self.baseSource addObject:model];
            }];
            if (self.baseSource.count == 0) {
                [self.creditDetialTableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.baseSource = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [[HRRequest manager]POST:PATH_ScoreRecordList para:paramDic success:^(id data) {
            
           
            NSLog(@"%@",data);
            /*判断data是什么类型*/
            if ([data isKindOfClass:[NSMutableArray class]]) {
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSLog(@"%@",obj);
                    
                    ICN_creditdetialModel * model=[[ICN_creditdetialModel alloc]initWithDictionary:obj error:nil];
                    [self.baseSource addObject:model];
                }];
            }
            BaseOptionalModel * model = [[BaseOptionalModel alloc]initWithDictionary:data error:nil];
            NSLog(@"%ld",model.code);
            NSLog(@"%@",self.baseSource);
            if (self.baseSource.count == 0) {
                self.creditDetialTableView.hidden = YES;
            }
            [self endRefresh];
            [self.creditDetialTableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.PromptLabel.hidden = NO;
            self.noDataImage.hidden = NO;
            self.creditDetialTableView.hidden = YES;
            self.currentPage = @"1";
            [self endRefresh];
            [self.creditDetialTableView reloadData];
        }];
    }
}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.creditDetialTableView.mj_footer endRefreshing];
    [self.creditDetialTableView.mj_header endRefreshing];
}
#pragma mark - --- Protocol ---
#pragma mark UITableViewDataSource

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.creditDetialTableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.creditDetialTableView.mj_footer.hidden) {
                self.creditDetialTableView.mj_footer.hidden = NO;
            }
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.baseSource.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.creditDetialTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_CreditTableViewCell * addCell = XIB(ICN_CreditTableViewCell);
    addCell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    addCell.selectionStyle = UITableViewCellSelectionStyleNone;
    /*赋值*/
    addCell.model = self.baseSource[indexPath.row];
    return  addCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 67;
}
@end
