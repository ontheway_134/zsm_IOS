//
//  ICN_CreditTableViewCell.h
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_creditdetialModel.h"
@interface ICN_CreditTableViewCell : UITableViewCell

/**
 明细头文字
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/**
 时间文字
 */
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (strong,nonatomic)ICN_creditdetialModel * model;
@end
