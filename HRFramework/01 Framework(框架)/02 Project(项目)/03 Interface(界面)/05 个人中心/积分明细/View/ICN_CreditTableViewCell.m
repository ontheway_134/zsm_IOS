//
//  ICN_CreditTableViewCell.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CreditTableViewCell.h"
#import "NSString+Date.h"
@implementation ICN_CreditTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_creditdetialModel *)model{
    _model = model;
    self.titleLabel.text = model.intro;
    self.timeLabel.text = model.createTime;
    self.timeLabel.text = [NSString conversionTimeStamp:model.createTime];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
