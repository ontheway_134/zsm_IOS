//
//  ICN_MyMyOrderingViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyMyOrderingViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_MyOrderingTableViewCell.h"
#import "ICN_MyOrdEventDetailsViewController.h"
#import "ICN_MyMyOrderingFuFeiModel.h"
#import "ICN_ActivityDetialViewController.h"
#import "ICN_ActivityParticularsViewController.h"
#import "ICN_LiveDetialViewController.h"/*直播详情*/
#import "AliVcMoiveViewController.h"
#import "MJDIYAutoFooter.h"
#import "ICN_GuiderPager.h" // 新版引导页
@interface ICN_MyMyOrderingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@end

@implementation ICN_MyMyOrderingViewController
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (instancetype)init{
    self = [super init];
    if (self) {
        _deleteAction = YES;
    }
    return self;
}

- (void)zj_viewWillAppearForIndex:(NSInteger)index {
    if (!self.tableView.mj_header.isRefreshing) {
        //[self.tableView.mj_header beginRefreshing];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[self.tableView.mj_header beginRefreshing];
    //    [self configData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    /*需要判断当前订单是不是自己的订单 0 表示是自己的订单 1 代表不是自己的订单*/
    if (self.isMy == 0) {
        self.tableView.hidden = NO;
        self.noDataImage.hidden = YES;
        self.noDataLabel.hidden = YES;
        self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
            [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        }];
        self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
            [self netWorkRequest:TYPE_RELOADDATA_UP];
        }];
        [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        [self.tableView.mj_header beginRefreshing];
    }else if (self.isMy == 1){
        self.tableView.hidden = NO;
        self.noDataImage.hidden = YES;
        self.noDataLabel.hidden = YES;
        self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
            [self noIsMynetWorkRequest:TYPE_RELOADDATA_DOWM];
        }];
        self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
            [self noIsMynetWorkRequest:TYPE_RELOADDATA_UP];
        }];
        [self noIsMynetWorkRequest:TYPE_RELOADDATA_DOWM];
        [self.tableView.mj_header beginRefreshing];
    }
    //[self.tableView.mj_header beginRefreshing];
    // Do any additional setup after loading the view.
}


#pragma mark 别人订单的网络请求
- (void)noIsMynetWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"memberId":self.memId,
                                   @"page":self.currentPage,
                                   @"isPrice":@"1",
                                   };
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_otherActivityOrderList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyMyOrderingFuFeiModel * model=[[ICN_MyMyOrderingFuFeiModel alloc]initWithDictionary:obj error:nil];
                [self.dataArr addObject:model];
            }];
            if (self.dataArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.dataArr = nil;
        NSDictionary *paramDic = @{
                                   @"memberId":self.memId,
                                   @"page":self.currentPage,
                                   @"isPrice":@"1",
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_otherActivityOrderList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyMyOrderingFuFeiModel * model=[[ICN_MyMyOrderingFuFeiModel alloc]initWithDictionary:obj error:nil];
                [self.dataArr addObject:model];
            }];
            NSLog(@"%@",self.dataArr);
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            [self endRefresh];
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            [self.tableView reloadData];
        }];
    }
}


#pragma mark 我的订单网络请求
- (void)netWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   @"isPrice":@"1",
                                   };
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_ActivityOrderList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyMyOrderingFuFeiModel * model=[[ICN_MyMyOrderingFuFeiModel alloc]initWithDictionary:obj error:nil];
                [self.dataArr addObject:model];
            }];
            if (self.dataArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.dataArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   @"isPrice":@"1",
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_ActivityOrderList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyMyOrderingFuFeiModel * model=[[ICN_MyMyOrderingFuFeiModel alloc]initWithDictionary:obj error:nil];
                [self.dataArr addObject:model];
            }];
            NSLog(@"%@",data);
            NSLog(@"%@",self.dataArr);
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            [self endRefresh];
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            [self.tableView reloadData];
        }];
    }
}


#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
- (void)zj_viewDidLoadForIndex:(NSInteger)index {
    //    NSLog(@"%@",self.view);
    //    NSLog(@"%@", self.zj_scrollViewController);
    
    
}



- (void)zj_viewDidAppearForIndex:(NSInteger)index {
    // NSLog(@"viewDidAppear-----");
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 140;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_MyOrderingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_MyOrderingTableViewCell" forIndexPath:indexPath];
    //    ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
    //    cell.model = model;
    if (!(self.dataArr.count == 0)) {
        cell.model = self.dataArr[indexPath.row];
    }
    
    if (self.isAbleToDelete == NO) {
        cell.delButton.hidden = YES;
    }else{
        cell.delButton.hidden = NO;
    }
    [cell.delButton addAction:^(NSInteger tag) {
        // 删除按钮需要添加提示窗
        ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
        UIAlertController *deleteWarnView = [UIAlertController alertControllerWithTitle:model.infoList.title message:@"是否确认删除订单" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
            [dic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
            dic[@"activityId"] = model.infoList.activityId;
            
            //            [[HRRequest manager]POST:PATH_Activitydel para:dic success:^(id data) {
            //                NSLog(@"%@",data);
            //                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
            //                [self.tableView reloadData];
            //            } faiulre:^(NSString *errMsg) {
            //                NSLog(@"%@",errMsg);
            //                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
            //            }];
            
            //
            
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_Activitydel params:dic success:^(id result) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
                [self netWorkRequest:TYPE_RELOADDATA_DOWM];
                
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据请求失败"];
            }];
        }];
        [deleteWarnView addAction:cancerAction];
        [deleteWarnView addAction:confirmAction];
        [self presentViewController:deleteWarnView animated:YES completion:nil];
        
        
        //        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        //        ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
        //
        //        [dic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
        //        dic[@"activityId"] = model.infoList.baseId;
        //
        //        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_Activitydel params:dic success:^(id result) {
        //            [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        //            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
        //        } failure:^(NSDictionary *errorInfo) {
        //            NSLog(@"%@",errorInfo);
        //        }];
    }];
    return cell;
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
    //
    //    if (model.type == 1) {
    //        /*是直播*/
    //        ICN_LiveDetialViewController * vc  = [[ICN_LiveDetialViewController alloc]init];
    //        vc.liveID = model.infoList.liveId;
    //        vc.actID = model.infoList.activityId;
    //        [self.navigationController pushViewController:vc animated:YES];
    //    }else{
    //        ICN_ActivityDetialViewController *vc = [[ICN_ActivityDetialViewController alloc] init];
    //        vc.packID = model.infoList.activityId;
    //        [self.navigationController pushViewController:vc animated:YES];
    //        /*给详情一个属性 代表是从这个页面传递过去的*/
    //        NSString*str =@"1";
    //        vc.judgeStr = str;
    //
    //    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
    
    //0，多直播；1，单直播
    if (model.type == 0) {
        BOOL isLogin =  [self getCurrentUserLoginStatus];
        //已登录
        // if (isLogin) {
        ICN_ActivityDetialViewController *MVC = [[ICN_ActivityDetialViewController alloc]init];
        MVC.packID = model.infoList.activityId;
        [self.navigationController pushViewController:MVC animated:YES];
        //未登录
        // }else{
        
        // }
    }else
    {
        
        
        //未开始
        if ([model.infoList.hasBegin integerValue] == 0) {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.infoList.liveId;
            MVC.status = model.infoList.memberStatus;
            MVC.actID = model.infoList.activityId;
            
            [self.navigationController pushViewController:MVC animated:YES];
            //已开始
        }else{
            
            BOOL isLogin = [self getCurrentUserLoginStatus];
            if (!isLogin) {
                ICN_GuiderPager *starPager = [[ICN_GuiderPager alloc] init]; // 欢迎页面
                ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
                // 先静默从欢迎页跳转到登录页
                [nav pushViewController:pager animated:NO];
                [self presentViewController:nav animated:YES completion:nil];
            }
            
            NSInteger memberStatus = [model.infoList.memberStatus integerValue];
            switch (memberStatus) {
                case 1:
                {
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.liveId;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                }
                    break;
                case 2:
                {
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.liveId;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                }
                    break;
                case 3:
                {
                    
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.liveId;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                }
                    break;
                case 4:
                {
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.liveId;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                }
                    break;
                case 5:
                {
                    if (model.infoList.liveUrl == nil) {
                        //测试
                        //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                        NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.infoList.streamName)];
                        AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                        MVC.liveID = model.infoList.liveId;
                        //MVC.model = model.infoList;
                        [MVC SetMoiveSource:url];
                        [MVC setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:MVC animated:YES];
                    }else{
                        //测试
                        //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                        NSURL* url = [NSURL URLWithString:model.infoList.liveUrl];
                        AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                        MVC.liveID = model.infoList.liveId;
                        //MVC.model = model.infoList;
                        [MVC SetMoiveSource:url];
                        [MVC setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:MVC animated:YES];
                    }
                    
                    
                }
                    break;
                case 6:
                {
                    ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
                    MVC.liveID = model.infoList.liveId;
                    MVC.status = model.infoList.memberStatus;
                    MVC.actID = model.infoList.activityId;
                    [self.navigationController pushViewController:MVC animated:YES];
                    
                }
                    break;
                    
                    
                default:
                    
                    break;
            }
            
            
            
        }
        
        
        
        
    }
    
}
- (void)creationView{
    
    self.navigationItem.title = @"我的订单";

}
- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyOrderingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MyOrderingTableViewCell"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
