//
//  ICN_MyMyOrderingViewController.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@interface ICN_MyMyOrderingViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>

@property (nonatomic , assign , getter=isAbleToDelete)BOOL deleteAction; // 能否删除
@property (assign,nonatomic)NSInteger  isMy;/*判断是不是自己的订单 0 是自己的订单 1 不是自己的订单*/
@property (nonatomic,copy) NSString *memId;
@end
