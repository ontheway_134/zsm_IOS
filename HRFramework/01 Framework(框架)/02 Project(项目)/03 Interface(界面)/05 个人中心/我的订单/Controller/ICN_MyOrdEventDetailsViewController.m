//
//  ICN_MyOrdEventDetailsViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrdEventDetailsViewController.h"
#import "ICN_MyOrderingDownModel.h"
#import "ICN_MyOrderingDetailsTopModel.h"
#import "ICN_MyOrdEventDetailsTableViewCell.h"
#import "ICN_MyOrderingDetailsView.h"


@interface ICN_MyOrdEventDetailsViewController ()
<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *topDataArr;
@property(nonatomic,strong)NSMutableArray *dataArr;



@end

@implementation ICN_MyOrdEventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    // Do any additional setup after loading the view from its nib.
}
- (void)regeditCell{

        [_tableView registerClass:[ICN_MyOrdEventDetailsTableViewCell class] forCellReuseIdentifier:@"cell"];

}
-(void)creationView{
    
    self.naviTitle = @"活动详情";
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 39) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_tableView];
    ICN_MyOrderingDetailsView *viewTop = [[ICN_MyOrderingDetailsView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 248)];
    _tableView.tableHeaderView = viewTop;

    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGB(16, 134, 254);
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
        make.height.mas_equalTo(39);
    }];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIButton *btn = [[UIButton alloc]init];
    [btn setImage:[UIImage imageNamed:@"立即报名.png"] forState:UIControlStateNormal];
    [btn setTitle:@"申请退款" forState:UIControlStateNormal];
    [btn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(39);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
    }];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0,-15, 0, 0);
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
//    [self datas];
    
}

-(void)btnAction:(UIButton *)btn{
    
    //虚化view
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.5;
    view.tag = 111;
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];
    
    
    //支付view
    
    UIView *view1 = [[UIView alloc]init];
    view1.backgroundColor = RGB0X(0Xffffff);
    view1.layer.cornerRadius = 5;
    view1.tag = 112;
    view1.layer.masksToBounds = YES;
    [currentWindow addSubview:view1];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(250);
        make.height.mas_equalTo(193);
        
        make.centerX.mas_equalTo(view);
        make.top.mas_equalTo(150);
    }];
    
    UILabel *label = [[UILabel alloc]init];
    label.textColor = RGB0X(0X000000);
    [view1 addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(20);
        make.height.mas_equalTo(13);
        make.centerX.mas_equalTo(view1);
    }];
    label.text = @"请选择退款理由";
    label.font = [UIFont systemFontOfSize:13];
    
    
    UILabel *label2 = [[UILabel alloc]init];
    label2.textColor = RGB0X(0X333333);
    [view1 addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(view1);
        make.top.mas_equalTo(label.mas_bottom).offset(26);
        make.height.mas_equalTo(12);
        
    }];
    label2.text = @"活动开始前三天不支持退款";
    label2.font = [UIFont systemFontOfSize:12];
    
    
    UIButton *btn1 = [[UIButton alloc]init];
    [btn1 setImage:[UIImage imageNamed:@"未选中.png"] forState:UIControlStateNormal];
    [btn1 setImage:[UIImage imageNamed:@"退款理由选中.png"] forState:UIControlStateSelected];
    [view1 addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label2.mas_bottom).offset(15);
        make.left.mas_equalTo(20);
        
    }];
    

    UILabel *wxLabel = [[UILabel alloc]init];
    wxLabel.textColor = RGB0X(0X333333);
    [view1 addSubview:wxLabel];
    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(btn1.mas_right).offset(5);
        make.top.mas_equalTo(label2.mas_bottom).offset(18);
        make.height.mas_equalTo(12);
    }];
    wxLabel.text = @"没有时间";
    wxLabel.font = [UIFont systemFontOfSize:12];
    
    //支付宝
    UILabel *zfbLabel =[[UILabel alloc]init];
    zfbLabel.textColor = RGB0X(0X333333);
    [view1 addSubview:zfbLabel];
    [zfbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.top.mas_equalTo(label2.mas_bottom).offset(18);
        make.height.mas_equalTo(12);
        
    }];
    zfbLabel.text =@"有事冲突";
    zfbLabel.font = [UIFont systemFontOfSize:12];
    
    
    
    UIButton *btn2 = [[UIButton alloc]init];
    [btn2 setImage:[UIImage imageNamed:@"未选中.png"] forState:UIControlStateNormal];
    [view1 addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(zfbLabel.mas_left).offset(-15);
        make.top.mas_equalTo(label2.mas_bottom).offset(15);
    }];
    
    
    UIButton *zfBtn = [[UIButton alloc]init];
    [zfBtn setTitle:@"提交申请" forState:UIControlStateNormal];
    [zfBtn setTintColor:RGB0X(0Xffffff)];
    zfBtn.layer.cornerRadius = 5;
    zfBtn.layer.masksToBounds = YES;
    zfBtn.backgroundColor = RGB(16, 134, 254);
    zfBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [view1 addSubview:zfBtn];
    [zfBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-23);
        make.top.mas_equalTo(wxLabel.mas_bottom).offset(35);
    }];
    
    
    
    UIButton *quxiaoBtn = [[UIButton alloc]init];
    [quxiaoBtn setImage:[UIImage imageNamed:@"弹窗关闭.png"] forState:UIControlStateNormal];
    [quxiaoBtn addTarget:self action:@selector(quxiaoBtn:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:quxiaoBtn];
    [quxiaoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(view1.mas_bottom).offset(20);
        make.centerX.mas_equalTo(view1);
        
    }];
    
    
    
    
}
-(void)quxiaoBtn:(UIButton *)btn{
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    UIView *view = [currentWindow viewWithTag:111];
    [view removeFromSuperview];
    
    UIView *view2= [currentWindow viewWithTag:112];
    [view2 removeFromSuperview];
    
    
    
}
-(void)datas{
    
    NSDictionary *dic = @{@"id":self.url};
    [[[HRRequestManager alloc]init]POST_URL:@"http://zsm.4pole.cn/index.php/MyPosition/Activity/Activitydetail" params:dic success:^(id result) {
        NSDictionary *result1 = result[@"result"];
        NSDictionary *dic0 = result1[@"0"];
        _topDataArr = [[NSMutableArray alloc]init];
        //for (NSDictionary *dic in [dic0 allValues]) {
        ICN_MyOrderingDetailsTopModel *model = [[ICN_MyOrderingDetailsTopModel alloc]init];
        [model setValuesForKeysWithDictionary:dic0];
        //  [_topDataArr addObject:model];
        // }
        
        
        ICN_MyOrderingDetailsView *view = [[ICN_MyOrderingDetailsView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 248)];
        _tableView.tableHeaderView = view;
        
        // ICN_ActivityParticularsTopModel *model = [[ICN_ActivityParticularsTopModel alloc]init];
        view.model = model;
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://zsm.4pole.cn/index.php/MyPosition/Activity/Activitydetail" params:dic success:^(id result) {
        NSDictionary *result1 = result[@"result"];
        NSArray *includecontent = result1[@"includecontent"];
        _dataArr = [[NSMutableArray alloc]init];
        for (NSDictionary *dic in includecontent) {
            ICN_MyOrderingDownModel *model = [[ICN_MyOrderingDownModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        [_tableView reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 4;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_MyOrdEventDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
//    ICN_MyOrderingDownModel *model = _dataArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.model = model;
    
    return cell;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 125;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //加上此句，返回时直接就是非选中状态。
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
