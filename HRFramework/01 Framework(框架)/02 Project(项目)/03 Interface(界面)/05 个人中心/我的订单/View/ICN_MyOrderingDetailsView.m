//
//  ICN_MyOrderingDetailsView.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrderingDetailsView.h"
#import "ICN_MyOrderingDetailsTopModel.h"
@implementation ICN_MyOrderingDetailsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        _picImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 130)];
        _picImageView.image = [UIImage imageNamed:@"图1"];
        [self addSubview:_picImageView];
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"sadf";
        [self addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(_picImageView.mas_bottom).offset(14);
            make.height.mas_equalTo(13);
            
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:16];
        
        _rmbLabel = [[UILabel alloc]init];
        _rmbLabel.text = @"asdfasd";
        [self addSubview:_rmbLabel];
        [_rmbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.top.mas_equalTo(_picImageView.mas_bottom).offset(14);
            make.height.mas_equalTo(14);
        }];
        _rmbLabel.textColor = RGB0X(0Xff561b);
        _rmbLabel.font = [UIFont systemFontOfSize:14];
        
        
        UILabel *label = [[UILabel alloc]init];
        
        [self addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(9);
            make.right.mas_equalTo(-9);
            make.top.mas_equalTo(_picImageView.mas_bottom).offset(40);
            make.height.mas_equalTo(1);
        }];
        label.backgroundColor = RGB(236, 236, 236);
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.image = [UIImage imageNamed:@"报名人数"];
        [self addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(label.mas_bottom).offset(16);
            
        }];
        
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.text = @"sadfsadf";
        [self addSubview:_numberLabel];
        [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageV.mas_right).offset(7);
            make.top.mas_equalTo(label.mas_bottom).offset(16);
            make.height.mas_equalTo(11);
            make.width.mas_equalTo(80);
        }];
        _numberLabel.textColor = RGB0X(0X666666);
        _numberLabel.font = [UIFont systemFontOfSize:13];
        
        _createdateLabel = [[UILabel alloc]init];
        _createdateLabel.text = @"qwerqwe";
        [self addSubview:_createdateLabel];
        [_createdateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-9);
            make.top.mas_equalTo(label.mas_bottom).offset(16);
            make.height.mas_equalTo(9);
            make.width.mas_equalTo(65);
        }];
        _createdateLabel.textColor = RGB0X(0Xb6b6b6);
        _createdateLabel.font = [UIFont systemFontOfSize:11];
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 212, self.frame.size.width, 36)];
        view.backgroundColor = RGB(236, 236, 236);
        [self addSubview:view];
        
        UILabel *label2 = [[UILabel alloc]init];
        [view addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(12);
            
        }];
        label2.text = @"包含内容";
        label2.textColor = RGB0X(0X333333);
        label2.font = [UIFont systemFontOfSize:14];
        
        
    }
    return self;
    
}
//-(void)setModel:(ICN_MyOrderingDetailsTopModel *)model{
//    
//    [_picImageView sd_setImageWithURL:[NSURL URLWithString:model.pic]];
//    _rmbLabel.text = [NSString stringWithFormat:@"¥%@",model.price];
//    _titleLabel.text= model.title;
//    _numberLabel.text = [NSString stringWithFormat:@"%@/%@人",model.subnum,model.cannum];
//    
//    _createdateLabel.text = model.createdate;
//    
//}

@end
