//
//  ICN_MyOrderingTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrderingTableViewCell.h"
#import "ICN_MyMyOrderingFuFeiModel.h"
#import "NSString+Date.h"
@implementation ICN_MyOrderingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_MyMyOrderingFuFeiModel *)model{
    _model = model;
    
    

    /*判断是单直播 还是多直播 单直播 1 多直播 0*/
    if (model.type == 1) {
        /*更改pic的属性*/
        self.widthPic.constant = 71.5;
        self.pic.layer.cornerRadius= 35.5;
        self.pic.layer.masksToBounds = YES;
        self.pic.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
        self.pic.layer.borderWidth = 0.5;
        [self.pic sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
        self.beginDate.text = [NSString stringWithFormat:@"%@-%@",[NSString conversionTimeStamp:model.infoList.beginDate],[NSString conversionTimeStamp:model.infoList.endDate]];
        self.XDLabel.text = [NSString stringWithFormat:@"%@/%@",model.infoList.subNum,model.infoList.canNum];
        self.title.text = model.infoList.title;
        self.include.text = model.infoList.memberNick;
        if (kIs_phone6plus ) {
            self.beginDataWidth.constant = 250;
        }
        if (kIs_phone6 ) {
            self.beginDataWidth.constant = 200;
        }
        if ([model.infoList.price isEqualToString:@"0"]) {
            self.price.text = @"免费";
        }else {
            NSString *str = @"¥";
            self.price.text = [str stringByAppendingString:model.infoList.price];
        }
    }else if (model.type == 0){
        
        self.urlA.hidden = YES;
        self.timeUrl.hidden = YES;
        self.XDLabel.hidden = YES;
        [self.pic sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
        self.title.text = model.infoList.title;
        self.include.text = model.infoList.content;
        
        self.beginDate.text = [NSString stringWithFormat:@"%@",[NSString conversionTimeStamp:model.infoList.beginDate]];
        if (kIs_phone6plus ) {
            self.beginDataWidth.constant = 250;
        }
        if (kIs_phone6 ) {
            self.beginDataWidth.constant = 200;
        }
    }
}
@end
