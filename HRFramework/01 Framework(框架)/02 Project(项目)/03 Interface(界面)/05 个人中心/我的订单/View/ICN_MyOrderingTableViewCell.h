//
//  ICN_MyOrderingTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyMyOrderingFuFeiModel;

@interface ICN_MyOrderingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *pic;
@property (weak, nonatomic) IBOutlet UILabel *include;
@property (weak, nonatomic) IBOutlet UILabel *beginDate;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *XDLabel;
@property (weak, nonatomic) IBOutlet UIButton *delButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthPic;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightPic;
@property (nonatomic, strong) ICN_MyMyOrderingFuFeiModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *urlA;

@property (weak, nonatomic) IBOutlet UIImageView *timeUrl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *beginDataWidth;

@end
