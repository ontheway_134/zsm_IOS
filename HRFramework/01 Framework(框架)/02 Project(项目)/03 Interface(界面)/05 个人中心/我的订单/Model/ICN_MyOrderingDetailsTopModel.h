//
//  ICN_MyOrderingDetailsTopModel.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_MyOrderingDetailsTopModel : NSObject
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *price;
@property(nonatomic)NSString *subnum;
@property(nonatomic)NSString *cannum;
@property(nonatomic)NSString *createdate;
@property(nonatomic)NSString *activityid;

@property(nonatomic)NSString *aid;
@end
