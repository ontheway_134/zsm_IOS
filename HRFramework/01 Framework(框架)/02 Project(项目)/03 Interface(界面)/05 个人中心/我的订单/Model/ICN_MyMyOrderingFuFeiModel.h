//
//  ICN_MyMyOrderingFuFeiModel.h
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@protocol FuFeiModel <NSObject>
@end
@interface  FuFeiModel: BaseOptionalModel
@property (nonatomic, copy) NSString *subNum;//
@property (nonatomic, copy) NSString *canNum;//
@property (nonatomic, copy) NSString *title;//
@property (nonatomic, copy) NSString *activityId;//
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *orderStatus;
@property (strong,nonatomic)NSString * beginDate;
@property (strong,nonatomic)NSString * activityNum;
@property (strong,nonatomic)NSString * hasBegin;
@property (strong,nonatomic)NSString * endDate;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * hasBeginId;
@property (strong,nonatomic)NSString * content;
@property (strong,nonatomic)NSString * baseId;
@property (nonatomic,strong)NSString * memberStatus;
@property (nonatomic,strong)NSString * liveUrl;
@property (nonatomic,strong)NSString * streamName;
@property (nonatomic,strong) NSString * memberNick;

@property (nonatomic,strong) NSString * liveId;


@end
@interface ICN_MyMyOrderingFuFeiModel : JSONModel
@property (nonatomic,strong) FuFeiModel<FuFeiModel> * infoList;
@property (nonatomic,assign) NSInteger type;
@end
