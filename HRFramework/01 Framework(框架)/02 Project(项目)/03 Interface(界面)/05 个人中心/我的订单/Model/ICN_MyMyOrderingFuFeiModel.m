//
//  ICN_MyMyOrderingFuFeiModel.m
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyMyOrderingFuFeiModel.h"

@implementation FuFeiModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"baseId" : @"id"}];
}

@end
@implementation ICN_MyMyOrderingFuFeiModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"infoList" : @"info"}];
}

@end
