//
//  ICN_MyUserTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyUserModel.h"
typedef void (^ReturnCacalBtnClick)(NSString * modelID);
@interface ICN_MyUserTableViewCell : UITableViewCell
@property (strong,nonatomic)ICN_MyUserModel * model;
@property (strong,nonatomic)NSString * userID;/*用来解除关注的ID 传值用*/
/**
 名称人
 */
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

/**
 公司名称 + 职位
 */
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;

/**
 是否认证的判断图片
 */
@property (weak, nonatomic) IBOutlet UIImageView *isCertification;
@property (strong,nonatomic)NSString * isGuan;/*判断是否关注*/
/**
 头像
 */
@property (weak, nonatomic) IBOutlet UIImageView *headImageurl;
/**
 灰色圆角 放度数的
 */
@property (weak, nonatomic) IBOutlet UIView *degreeView;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;

@property (strong,nonatomic)ReturnCacalBtnClick  returnCacalBtnClick;
-(void)returnCacalBtnClick:(ReturnCacalBtnClick)block;
@end
