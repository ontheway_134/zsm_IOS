//
//  ICN_MyConcerningTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyConcerningModel;
typedef void (^ReturnCacalBtnClick)(NSString *modelId);


@interface ICN_MyConcerningTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *companyContentLab;
@property (weak, nonatomic) IBOutlet UILabel *companyNumberLab;
@property (weak, nonatomic) IBOutlet UILabel *industryName;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (strong,nonatomic)NSString * ConcerningID;/*用来解除关注的ID 传值用*/
@property (nonatomic, strong) ICN_MyConcerningModel *model;

@property (strong,nonatomic)ReturnCacalBtnClick  returnCacalBtnClick;
-(void)returnCacalBtnClick:(ReturnCacalBtnClick)block;
@end
