//
//  ICN_MyConcerningTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyConcerningTableViewCell.h"
#import "ICN_MyConcerningModel.h"
@implementation ICN_MyConcerningTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (IBAction)quXiao:(UIButton *)sender {
    
    if (self.returnCacalBtnClick) {
        self.returnCacalBtnClick(self.ConcerningID);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)returnCacalBtnClick:(ReturnCacalBtnClick)block{
    self.returnCacalBtnClick = block;
}
- (void)setModel:(ICN_MyConcerningModel *)model{

    _model = model;
    [self.logoImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
    self.logoImage.layer.cornerRadius = self.logoImage.bounds.size.width*0.5;
    self.logoImage.layer.masksToBounds = YES;
    NSString *str = [model.companyNatureName.description stringByAppendingString:@"|"];
    self.companyNumberLab.text = [str stringByAppendingString:SF(@"%@人",model.companyScale)];
    self.industryName.text = model.companyIndustryName;
    self.companyContentLab.text = model.companyName;
    /*将ID取出方便取消关注用*/
    self.ConcerningID = model.enterpriseId;

}
@end
