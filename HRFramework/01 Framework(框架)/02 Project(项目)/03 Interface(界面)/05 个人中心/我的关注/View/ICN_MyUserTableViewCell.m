//
//  ICN_MyUserTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyUserTableViewCell.h"

@implementation ICN_MyUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    /*设置圆角*/
    self.degreeView.layer.cornerRadius= 6;
    self.degreeView.layer.masksToBounds = YES;
    self.degreeView.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
    self.degreeView.layer.borderWidth = 0.5;
}
#pragma mark 取消按钮
- (IBAction)cancelBtnClick:(UIButton *)sender {
    if (self.returnCacalBtnClick) {
        self.returnCacalBtnClick(self.userID);
    }
}
-(void)returnCacalBtnClick:(ReturnCacalBtnClick)block{
    self.returnCacalBtnClick = block;
}
- (void)setModel:(ICN_MyUserModel *)model{
    _model = model;
    
    self.nameLabel.text = model.memberNick;
    
    [self.headImageurl sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    /*需要判断有没有v 加V验证0未验证1已验证2验证中*/
    if ([model.plusv isEqualToString:@"1"]) {
        self.isCertification.hidden = YES;
    }
    self.levelLabel.text = [NSString stringWithFormat:@"%@度",model.level];
    self.userID = model.enterpriseId;
    /*职位*/
    NSString * memberPositionStr = [[NSString alloc]init];
    memberPositionStr = model.memberPosition;
    self.companyLabel.text = [NSString stringWithFormat:@"%@|%@",model.companyName,memberPositionStr];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
