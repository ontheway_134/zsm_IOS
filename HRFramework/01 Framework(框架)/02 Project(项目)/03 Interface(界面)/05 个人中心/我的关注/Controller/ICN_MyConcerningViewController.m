//
//  ICN_MyConcerningViewController.m
//  ICan
//
//  Created by 辛忠志 on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyConcerningViewController.h"
#import "ICN_MyConcerningTableViewCell.h"
#import "ICN_MyUserTableViewCell.h"
#import "ICN_MyConcerningModel.h"/*公司model*/
#import "ICN_MyUserModel.h"/*个人model*/
#import "ICN_CompanyDetialVC.h"/*公司详情*/
#import "ICN_UserHomePagerVC.h"/*个人详情*/
#import "MJDIYAutoFooter.h"
typedef NS_ENUM(NSInteger,kCouponType){
    kCouponable   =100,         //公司
    kCouponEnable,             //用户
    
};
@interface ICN_MyConcerningViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
/**
 公司按钮
 */
@property (weak, nonatomic) IBOutlet UIButton *couponBtn;
/**
 蓝色线
 */
@property (weak, nonatomic) IBOutlet UIView *redLine;

@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic,assign) NSInteger  selectIndex;/*辛*/
@end

@implementation ICN_MyConcerningViewController

#pragma mark - ---------- 生命周期 ----------
- (NSMutableArray *)userSourceArr{
    if (!_userSourceArr) {
        _userSourceArr = [NSMutableArray array];
    }
    return _userSourceArr;
}
- (NSMutableArray *)dataSourceArr{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /*进来给一个默认样式 公司*/
    self.couponBtn.selected = YES;
    
//    self.selectIndex = 0;
    
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        /*网络请求 公司*/
        [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
        /*网络请求 公司*/
        [self netWorkRequest:TYPE_RELOADDATA_UP];
    }];
    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
}
#pragma mark - ---------- IBActions ----------
#pragma mark 返回按钮 辛
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 切换按钮(用户 公司)辛
- (IBAction)switchBtnClick:(UIButton *)sender {
    for (int i = 0; i<2 ; i++) {
        if (sender.tag == 100 + i) {
            sender.selected = YES;
            
            [UIView animateWithDuration:0.1 animations:^{
                self.redLine.frame = CGRectMake((SCREEN_WIDTH/2-36)/2.0+SCREEN_WIDTH/2.0*i, self.couponBtn.frame.origin.y+self.couponBtn.frame.size.height-2.0, 32, 1);
            }];
            
            continue;
        }
        UIButton *btn = (UIButton *)[self.view viewWithTag:100+i];
        btn.selected = NO;
    }
    self.selectIndex = sender.tag-100;
    
    switch (self.selectIndex) {
        case 0:
        {
            self.tableView.hidden = NO;
            self.noDataImage.hidden = YES;
            self.noDataLabel.hidden = YES;
            /*网络请求 公司*/
            [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
        case 1:
        {
            self.tableView.hidden = NO;
            self.noDataImage.hidden = YES;
            self.noDataLabel.hidden = YES;
            /*网络请求 个人*/
            [self userNetWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
            
        default:
            break;
    }
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark 公司列表数据网络请求--
-(void)netWorkRequest:(BOOL)isMore{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_MemberConcern para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyConcerningModel * model=[[ICN_MyConcerningModel alloc]initWithDictionary:obj error:nil];
                [self.dataSourceArr addObject:model];
            }];
            if (self.dataSourceArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.dataSourceArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_MemberConcern para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyConcerningModel * model=[[ICN_MyConcerningModel alloc]initWithDictionary:obj error:nil];
                [self.dataSourceArr addObject:model];
            }];
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无公司关注信息";
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }

    
}
#pragma mark 个人关注列表数据网络请求
-(void)userNetWorkRequest:(BOOL)isMore{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{@"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_UserFollowList para:paramDic success:^(id data) {
            
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyUserModel * model = [[ICN_MyUserModel alloc]initWithDictionary:obj error:nil];
                [self.userSourceArr addObject:model];
            }];
            NSLog(@"%@",self.userSourceArr);
            if (self.userSourceArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.userSourceArr = nil;
        NSDictionary *paramDic = @{@"token":token,
                                   @"page":self.currentPage,
                                   };
        /*避免连点*/
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_UserFollowList para:paramDic success:^(id data) {
            
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyUserModel * model = [[ICN_MyUserModel alloc]initWithDictionary:obj error:nil];
                [self.userSourceArr addObject:model];
            }];
            NSLog(@"%@",self.userSourceArr);
            //            NSLog(@"%@",model);
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无个人关注信息";
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
#pragma mark --- UITableViewDataSource ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    switch (self.selectIndex) {
        case 0:
        {
            return self.dataSourceArr.count;
        }
            break;
        case 1:
        {
            return self.userSourceArr.count;
        }
            break;
        default:
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    switch (self.selectIndex) {
        case 0:
        {
            ICN_MyConcerningTableViewCell * cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_MyConcerningTableViewCell" owner:nil options:nil].lastObject;
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            /*赋值*/
            cell.model = self.dataSourceArr[indexPath.row];
            /*取消关注 用ID解除公司的关注*/
            [cell returnCacalBtnClick:^(NSString *modelId) {
                /*初始化token*/
                NSString *token;
                if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                    token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                }
                NSDictionary *paramDic = @{@"token":token,
                                           @"enterpriseId":modelId,
                                           };
                [[HRRequest manager]POST:PATH_doDeleteConcern para:paramDic success:^(id data) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消成功"];
                    /*网络请求 公司*/
                    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
                }
                                 faiulre:^(NSString *errMsg) {
                                     [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消失败"];
                                 }];
            }];
            return  cell;
        }
            break;
        case 1:
        {
            ICN_MyUserTableViewCell * cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_MyUserTableViewCell" owner:nil options:nil].lastObject;
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            /*赋值*/
            cell.model = self.userSourceArr[indexPath.row];
            /*取消关注 用ID解除个人的关注*/
            [cell returnCacalBtnClick:^(NSString *modelId) {
                /*初始化token*/
                NSString *token;
                if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                    token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                }
                NSDictionary *paramDic = @{@"token":token,
                                           @"sideId":modelId,
                                           };
                [[HRRequest manager]POST:PATH_doFollowDel para:paramDic success:^(id data) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消成功"];
                    /*网络请求 个人*/
                    [self userNetWorkRequest:TYPE_RELOADDATA_DOWM];
                }
                                 faiulre:^(NSString *errMsg) {
                                     [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消失败"];
                                 }];
                
            }];
            return  cell;
        }
            break;
            
        default:
            break;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.selectIndex) {
        case 0:
        {
            return 83;
        }
            break;
        case 1:
        {
            return 55;
        }
            break;
            
        default:
            break;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.selectIndex) {
        case 0:
        {
            ICN_CompanyDetialVC * vc = [[ICN_CompanyDetialVC alloc]init];
            ICN_MyConcerningModel * model =self.dataSourceArr[indexPath.row];
            
            vc.companyId = model.enterpriseId;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:
        {
            ICN_UserHomePagerVC * vc =[[ICN_UserHomePagerVC alloc]init];
            ICN_MyUserModel * model = self.userSourceArr[indexPath.row];
            vc.memberId = model.enterpriseId;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
}
@end
