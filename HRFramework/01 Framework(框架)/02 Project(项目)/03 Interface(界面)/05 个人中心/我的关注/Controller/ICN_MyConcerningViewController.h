//
//  ICN_MyConcerningViewController.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_MyConcerningModel;
@interface ICN_MyConcerningViewController : BaseViewController
@property (nonatomic, strong) ICN_MyConcerningModel *model;

/******************网络数据部分*********************/
@property (strong,nonatomic)NSMutableArray * dataSourceArr;/*公司数据源*/
@property (strong,nonatomic)NSMutableArray * userSourceArr;/*个人数据源*/
@end
