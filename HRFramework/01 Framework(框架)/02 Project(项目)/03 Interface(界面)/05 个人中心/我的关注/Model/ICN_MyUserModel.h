//
//  ICN_MyUserModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyUserModel : BaseOptionalModel
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * enterpriseId;
@property (strong,nonatomic)NSString * type;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * concernTime;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * memberMajor;
@property (strong,nonatomic)NSString * memberSchool;
@property (strong,nonatomic)NSString * memberPosition;
@property (strong,nonatomic)NSString * companyName;
@property (strong,nonatomic)NSString * level;
@property (strong,nonatomic)NSString * plusv;
@end
