//
//  ICN_MyConcerningModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyConcerningModel.h"

@implementation ICN_MyConcerningModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ConcerningID" : @"id"}];
}
@end
