//
//  ICN_MyConcerningModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyConcerningModel : BaseOptionalModel
@property (strong,nonatomic)NSString  * companyIndustry;
@property (strong,nonatomic)NSString  * companyIndustryName;
@property (strong,nonatomic)NSString  * companyLogo;
@property (strong,nonatomic)NSString  * companyName;
@property (strong,nonatomic)NSString  * companyNature;
@property (strong,nonatomic)NSString  * companyNatureName;
@property (strong,nonatomic)NSString  * companyScale;
@property (strong,nonatomic)NSString  * enterpriseId;
@property (strong,nonatomic)NSString  * ConcerningID;
@property (strong,nonatomic)NSString  * memberId;
@end
