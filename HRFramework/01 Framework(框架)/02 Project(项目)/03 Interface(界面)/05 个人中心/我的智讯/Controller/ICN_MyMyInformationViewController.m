//
//  ICN_MyMyInformationViewController.m
//  ICan
//
//  Created by apple on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyMyInformationViewController.h"
#import "ICN_CommonPsersonDynamicCell.h" // 个人动态Cell
#import "ICN_UserDynamicStateDetialVC.h" // 个人动态详情VC
#import "ICN_DynWarnView.h" // 提示窗页面
#import "ICN_DynamicStateListViewModel.h"
#import "ICN_DynStateContentModel.h"
@interface ICN_MyMyInformationViewController ()<UITableViewDelegate , UITableViewDataSource , ICN_DynWarnViewDelegate , ICN_DynamicStateListDelegate>

@property (nonatomic , retain)UITableView *tableView;

@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
@property (nonatomic , strong)ICN_DynamicStateListViewModel *viewModel;
@property (nonatomic , strong)ICN_DynStateContentModel *replyModel; // 转发Model;
@end

@implementation ICN_MyMyInformationViewController

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        frame.size.height -= 64.0;
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = RGB(235, 236, 237);
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的动态";
    self.viewModel = [[ICN_DynamicStateListViewModel alloc] init];
    self.viewModel.delegate = self;
    [self tableView];
    [self configTableViewRefreshHeaderFooterView];
    
    if (self.isMyDynamic) {
        self.naviTitle = @"我的动态";
        self.viewModel.mainDynamic = YES;
    }else{
        self.naviTitle = @"我的智讯";
        self.viewModel.mainDynamic = YES;
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_warnView) {
        [_warnView removeFromSuperview];
    }
}

- (void)dealloc{
    if (_warnView) {
        _warnView.delegate = nil;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---------- 私有方法 ----------

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self.viewModel refreshCurrentPageContentCells];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    self.tableView.mj_header = header;
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.viewModel loadNextPageContentCells];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


#pragma mark - ---------- 代理 ----------


#pragma mark --- ICN_DynamicStateListDelegate ---

- (void)responseWithDynamicStateListRequestSuccess:(BOOL)success Code:(NSInteger)code{
    if (success) {
        if (code == 0) {
            [self.tableView reloadData];
        }
        if (code == 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该用户未登录"];
        }
    }else{
        // 网络请求失败
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败 - 请稍后重试"];
    }
    [self endRefreshWithTableView:self.tableView];
    
}

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"点赞失败"];
    }else{
        [self.tableView reloadData];
    }
}

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您无法为该用户点赞"];
}

/** 已经点赞后的回调 */
- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经为该用户点过赞"];
    [self.tableView reloadData];
}

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        default:
            [self.warnView removeFromSuperview];
            break;
    }
}


#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 - 跳转到动态详情页面
    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
    pager.model = self.viewModel.modelsArr[indexPath.row];
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_CommonPsersonDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
    cell.model = self.viewModel.modelsArr[indexPath.row];
    // 点击Cell的下拉按钮
    // 添加对于Cell上点击事件的处理
    [cell callWhileCellBtnClick:^(NSInteger SenderTag , ICN_DynStateContentModel *model) {
        switch (SenderTag) {
            case ICN_CellLikeActionBtnType:{
                model.likeUp = !model.likeUp;
                NSInteger likeType = 0;
                if (model.isLikeUp) {
                    likeType = 1;
                }else{
                    likeType = 2;
                }
                [self.viewModel likeUpWithType:likeType Model:model];
                break;
            }
            case ICN_CellCommentBtnType:{
                //相关逻辑 - 跳转到动态详情页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = self.viewModel.modelsArr[indexPath.row];
                [self currentPagerJumpToPager:pager];
                break;
            }
            case ICN_CellReviewBtnType:{
                // 转发
                self.replyModel = model;
                [self.view addSubview:self.warnView];
                break;
            }
            case ICN_CellPullBtnType:{
                [self.tableView reloadData];
                break;
            }
            default:
                break;
        }
    }];
    [cell setListStyle:YES];
    return cell;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DynStateContentModel *model = self.viewModel.modelsArr[indexPath.row];
    
    if (model.isTransmit.integerValue == 1) {
        // 是转发的Cell
        if (model.contentSpreadHeight > 57.5) {
            return 230 + 100;
        }else{
            return 330 - model.contentSpreadHeight;
        }
    }
    
    if (model.contentSpread == NO) {
        if (model.imageFooterHeight > 0) {
            return 230 + model.imageFooterHeight;
        }
        if (model.contentSpreadHeight > 57.5) {
            return 230;
        }
    }
    
    if (model.imageFooterHeight > 0) {
        return 230 + model.imageFooterHeight + model.contentSpreadHeight - 57.5;
    }else{
        return 230 + model.contentSpreadHeight - 57.5;
    }
    
    return 230.0;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if (editingStyle ==UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath]withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
    return UITableViewCellEditingStyleDelete;
}

//如果想在侧拉的时候显示是中文的删除，只需要用下面的方法替换掉上面的方法就好了
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath

{
    UITableViewRowAction *rowAction;
    
    rowAction = [UITableViewRowAction
                 rowActionWithStyle:UITableViewRowActionStyleNormal
                 title:@"删除"
                 handler:^(UITableViewRowAction * _Nonnull action,
                           NSIndexPath * _Nonnull indexPath)
                 {
                     
                     ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] init];
                     model = _viewModel.modelsArr[indexPath.row];
                     
                     NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                     dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                     dic[@"id"] = model.DynID;
                     [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_delDynamic params:dic success:^(id result) {
                         
                         [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
                         [self.tableView.mj_header beginRefreshing];
                     } failure:^(NSDictionary *errorInfo) {
                         NSLog(@"请求数据失败");
                     }];
                 }];
    rowAction.backgroundColor = [UIColor redColor];
    NSArray *arr = @[rowAction];
    return arr;
}

@end
