//
//  ICN_MyMyInformationViewController.h
//  ICan
//
//  Created by apple on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_MyMyInformationViewController : BaseViewController
@property (nonatomic , copy)NSString *memberId; // 用户的ID
@property (nonatomic , assign , getter=isMyDynamic)BOOL mineDynamic; // 是否是自己的动态

@end
