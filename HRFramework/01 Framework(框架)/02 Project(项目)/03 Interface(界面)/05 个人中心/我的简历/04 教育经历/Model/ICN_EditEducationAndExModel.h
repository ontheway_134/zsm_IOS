//
//  ICN_EditEducationAndExModel.h
//  ICan
//
//  Created by apple on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_EditEducationAndExModel : BaseOptionalModel
@property (nonatomic, copy) NSString *enrolDate;
@property (nonatomic, copy) NSString *graduationDate;
@property (nonatomic, copy) NSString *school;
@property (nonatomic, copy) NSString *qualification;
@property (nonatomic, copy) NSString *major;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *memberQualificationName;

@end
