//
//  ICN_EducationAndExTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_EducationAndExperienceModel;
typedef void(^EditBlock)(ICN_EducationAndExperienceModel *model);

@interface ICN_EducationAndExTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *editEducationExBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolLab;
@property (weak, nonatomic) IBOutlet UILabel *qualificationAndMajor;
@property (nonatomic, strong) ICN_EducationAndExperienceModel *model;


@property (nonatomic , copy) EditBlock block;


- (void)callBackEditBlock:(EditBlock)block;

@end
