//
//  ICN_EducationAndExperienceViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EducationAndExperienceViewController.h"
#import "ICN_EducationAndExTableViewCell.h"
#import "ICN_EditEducationAndExViewController.h"
#import "ICN_AddEducationAndExViewController.h"
#import "ICN_EducationAndExperienceModel.h"
#import "ICN_MyResumeEducationExperienceModel.h"
@interface ICN_EducationAndExperienceViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *arrOfEducation;

@end

@implementation ICN_EducationAndExperienceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creationView];
    [self regeditCell];
    // Do any additional setup after loading the view.
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrOfEducation.count;

}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    self.dataArr = [[NSMutableArray alloc] init];
    
    ICN_EducationAndExTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_EducationAndExTableViewCell" forIndexPath:indexPath];
    
    [cell callBackEditBlock:^(ICN_EducationAndExperienceModel *model) {
        [self editWorkExperienceWithModel:model];
    }];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
    ICN_EducationAndExperienceModel *model = self.arrOfEducation[indexPath.row];
    cell.model = model;
    return cell;
    
    
}
- (void)configData{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/EducationExperience/educationExperienceList" params:dic success:^(id result) {
        
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfEducation = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_EducationAndExperienceModel *model = [[ICN_EducationAndExperienceModel alloc]init];
            
            
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfEducation addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    
    
}



- (void)creationView{
    
    self.navigationItem.title = @"教育经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    self.tableView.tableFooterView.userInteractionEnabled = YES;
    UIButton *buttonAddWork = [[UIButton alloc] init];
    [buttonAddWork addTarget:self action:@selector(buttonAddEducation) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView.tableFooterView addSubview:buttonAddWork];
    buttonAddWork.backgroundColor = RGB(38, 135, 250);
    buttonAddWork.layer.cornerRadius = 5.0f;
    buttonAddWork.layer.masksToBounds = YES;
    buttonAddWork.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonAddWork mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.tableFooterView).with.offset(35);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(40);
        
    }];
    [buttonAddWork setTitle:@"添加教育经历" forState:UIControlStateNormal];
    
    
    
    
    
    
    
    
}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_EducationAndExTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_EducationAndExTableViewCell"];
    
    
}
//编辑教育
- (void)editWorkExperienceWithModel:(ICN_EducationAndExperienceModel *)model{
    

    ICN_EditEducationAndExViewController *vc = [[ICN_EditEducationAndExViewController alloc] init];
    vc.modelEd = model;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
//添加教育经历
- (void)buttonAddEducation{
    ICN_AddEducationAndExViewController *vc = [[ICN_AddEducationAndExViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
