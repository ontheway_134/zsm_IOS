//
//  ICN_AddEducationAndExViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_AddEducationAndExViewController.h"
#import "ICN_NickNameTableViewCell.h"
#import "ICN_WorkingContentTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_EducationView.h"
#import "ICN_EducationAndExperienceModel.h"

@interface ICN_AddEducationAndExViewController ()<UITableViewDelegate, UITableViewDataSource,birthdayDelegate,EducationDelegate>

{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *tempOutTime;
    NSString *educationHighest;
    NSString *tempBirthdayout;
    
    
    ICN_NickNameTableViewCell *schoolcell;
    ICN_NickNameTableViewCell *zhuanyecell;
    
    ICN_NoImageTableCell *timecell1;
    ICN_NoImageTableCell *timecell2;
    
    ICN_NoImageTableCell *xuelicell;
    
    ICN_WorkingContentTableViewCell *neirongcell;
}

@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (strong, nonatomic) ICN_EducationView *educationView;
@property (nonatomic, strong) NSMutableDictionary *dict;



@end

@implementation ICN_AddEducationAndExViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 获取根据学历获取id的网络请求
    [self creationView];
    [self regeditCell];
    self.dict = [[NSMutableDictionary alloc] init];
}



#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

        return 5;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 40;
    }else {
        
        return 150;
        
    }
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

        
    if (indexPath.row == 0) {
        schoolcell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
        schoolcell.leftLabel.text = @"学校名称";
        return schoolcell;
    } else if (indexPath.row == 1) {
        zhuanyecell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
        
        if (!([USERDEFAULT objectForKey:@"所学专业"] == nil)) {
            [self.dict setObject:[USERDEFAULT objectForKey:@"所学专业"] forKey:@"major"];
        }
        zhuanyecell.leftLabel.text = @"所学专业";
        
        return zhuanyecell;
        
    }else if (indexPath.row == 2) {
        timecell1 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
        timecell1.leftLabel.text = @"入学时间";
        timecell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return timecell1;
    } else  if (indexPath.row == 3){
        timecell2 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
        timecell2.leftLabel.text = @"毕业时间";
        timecell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return timecell2;
    } else {
        xuelicell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
        xuelicell.leftLabel.text = @"学历";
        
        xuelicell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return xuelicell;
    }
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            [self setUserBirthday];
        }else if (indexPath.row == 3) {
            [self setUserquit];
            
            
            
        }else if (indexPath.row == 4) {
            
            
            [self  setUserEducation];
            
            
        }
        
    }
    
}
- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}
- (void)dismissContactView {
    [grayView removeFromSuperview];
}
#pragma mark - ---------- 入职时间 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)birthdayChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:2 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempBirthdayStr == NULL) {
        tempBirthdayStr = @"1966年1月";
    }
    cell.rightLabel.text = tempBirthdayStr;
    [self.dict setObject:tempBirthdayStr forKey:@"enrolDate"];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1960年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
    tempOutTime = [NSString stringWithFormat:@"%@%@",year,month];
}

#pragma mark - ---------- 离职时间 ----------
- (void)setUserquit {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(quitChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)quitChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:3 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempBirthdayout == NULL) {
        tempBirthdayout = @"1960年1月";
    }
    cell.rightLabel.text = tempOutTime;
    [self.dict setObject:cell.rightLabel.text forKey:@"graduationDate"];
    [grayView removeFromSuperview];
}
#pragma mark - ---------- 学历选择 ----------
- (void)setUserEducation{
    
    [self setGrayView];
    self.educationView = XIB(ICN_EducationView);
    self.educationView.delegate = self;
    self.educationView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.educationView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.educationView];
    [self.educationView.confirmButton addTarget:self action:@selector(educationChoose) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
- (void)educationChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:4 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
   // if (educationHighest == NULL) {
      //  educationHighest = @"小学";
    //}
    cell.rightLabel.text = educationHighest;
    //[self.dict setObject:cell.rightLabel.text forKey:@"memberQualification"];
    [grayView removeFromSuperview];
}
//- (void)getEducation:(NSString *)education andID:(NSString *)ID{
    
  //  if (education == NULL) {
   //     education = @"小学";
   // }
    
   // educationHighest = [NSString stringWithFormat:@"%@",education];
    //if (educationHighest == nil) {
    //    [MBProgressHUD ShowProgressWithBaseView:self.view //Message:@"请选择学历"];
  // }else {
    //    [self.dict setObject:ID forKey:@"memberQualification"];
        
   // }
    //[self.dict setObject:ID forKey:@"memberQualification"];
//}



- (void)getEducation:(NSString *)education andID:(NSString *)ID temp:(NSString *)strID{
    
  
    
    educationHighest = [NSString stringWithFormat:@"%@",education];
    
    if (educationHighest == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择学历"];
    }else {
        [self.dict setObject:ID forKey:@"memberQualification"];
        
    }
}




- (void)creationView{
    
    self.navigationItem.title = @"添加教育经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
}
- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NickNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NickNameTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_WorkingContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_WorkingContentTableViewCell"];
        [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NoImageTableCell" bundle:nil] forCellReuseIdentifier:@"ICN_NoImageTableCell"];
}

//保存
- (void)saveAction{

    NSLog(@"zzzzzzzzzzzzzzzzzzzzzz%@", self.dict);
    //18740018407
//    school major
    
    if (schoolcell.rightTextField.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入学校"];
    } else if (zhuanyecell.rightTextField.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入专业"];
    } else if ([timecell1.rightLabel.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入入学时间"];
    } else if ([timecell2.rightLabel.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入毕业时间"];
    } else if ([xuelicell.rightLabel.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入学历"];
    }
//    else if (neirongcell.signTextView.text.length == 0) {
//        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入内容"];
//    }
    else {
    
        NSInteger year1 = [[timecell1.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
        NSInteger month1 = timecell1.rightLabel.text.length == 7 ? [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
        
        NSInteger year2 = [[timecell2.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
        NSInteger month2 = timecell2.rightLabel.text.length == 7 ? [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
        if (year1>year2) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
        } else if (year1 == year2 && month1 > month2) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
        } else {

        
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            [self.dict setObject:token forKey:@"token"];
            [self.dict setObject:schoolcell.rightTextField.text forKey:@"school"];
            [self.dict setObject:zhuanyecell.rightTextField.text forKey:@"major"];
            
            
             [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyEducationExperience params:self.dict success:^(id result) {
                
            [self.navigationController popViewControllerAnimated:YES];
            
            } failure:^(NSDictionary *errorInfo) {
                
                
            }];
    
    
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
