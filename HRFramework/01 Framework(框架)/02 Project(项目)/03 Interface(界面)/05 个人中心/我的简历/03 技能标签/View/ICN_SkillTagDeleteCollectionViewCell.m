//
//  ICN_SkillTagDeleteCollectionViewCell.m
//  ICan
//
//  Created by apple on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SkillTagDeleteCollectionViewCell.h"
#import "ICN_SkillTagAllsModel.h"
@implementation ICN_SkillTagDeleteCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_SkillTagAllsModel *)model{
    
    self.contentLab.text = model.skillLabel;
    
}

@end
