//
//  ICN_SkillTagCollectionViewCell.h
//  ICan
//
//  Created by apple on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_SkillTagAllsModel;
@interface ICN_SkillTagCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (nonatomic, strong) ICN_SkillTagAllsModel *model;

@end
