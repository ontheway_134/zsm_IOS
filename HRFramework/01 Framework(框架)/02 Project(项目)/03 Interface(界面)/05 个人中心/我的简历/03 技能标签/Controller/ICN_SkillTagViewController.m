//
//  ICN_SkillTagViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SkillTagViewController.h"
#import "ICN_SkillTagCollectionViewCell.h"
#import "ICN_SkillTagAllsModel.h"
#import "ICN_SkillTagDeleteCollectionViewCell.h"
@interface ICN_SkillTagViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, strong) UICollectionView *allCollectionView;
@property (nonatomic, strong) UICollectionView *topCollectionView;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *arrOfSkill;
@property (nonatomic, strong) NSMutableDictionary *dic;

@property (nonatomic, strong) NSMutableArray *topdataArr;
@property (nonatomic, strong) NSString *index;
@property (nonatomic, strong) UILabel *labelOfNum1;


@property (nonatomic,strong)NSMutableArray *selectLabelArray;
@end

@implementation ICN_SkillTagViewController
- (NSMutableArray *)topdataArr
{
    if (!_topdataArr) {
        _topdataArr = [NSMutableArray array];
    }
    return _topdataArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"技能标签";
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    self.dic = [[NSMutableDictionary alloc] init];
    
    [self configData];
    [self configdataChoice];
    
    [self creationView];
    
}

- (void)saveAction{
    //warn=== 现阶段出现:下次点击保存的时候保存的是上次点击的内容
    
    if (self.index == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未选中标签或您的标签已经选择"];
        return ;
    }
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    [self.dic setObject:token forKey:@"token"];
    [self.dic setObject:self.index forKey:@"labelId"];
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySkillEditTrainEx params:self.dic success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // Code == 0 的时候只是返回添加成功需要手动在项目中将选中的Model添加
            for (ICN_SkillTagAllsModel *selectedModel in self.dataArr) {
                if ([selectedModel.id isEqualToString: self.index]) {
                    // 将之前选中的Model在添加接口走通之后添加在 上层 数组
                    [self.topdataArr addObject:selectedModel];
                }
                NSLog(@"%@",self.topdataArr);
            }
            self.index = nil;
            [self.topCollectionView reloadData];
            
        }else{
            self.index = nil;
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:firstModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        self.index = nil;
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
    }];
    if(self.topdataArr.count < 6){
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MemberSkillList params:@{@"token":token} success:^(id result) {
            NSLog(@"feewfejfopewjeqwopj%@", result);
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (firstModel.code == 0) {
                self.topdataArr = nil;
                for (NSDictionary *dic in [result valueForKey:@"result"]) {
                    ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                    
                    [self.topdataArr addObject:model];
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.topCollectionView reloadData];
                });
            }else{
                
            }
        } failure:^(NSDictionary *errorInfo) {
            
            
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"最多6个"];
    }
}
- (void)creationView{
    
    UILabel *labelOfTag = [[UILabel alloc] init];
    [self.view addSubview:labelOfTag];
    labelOfTag.text = @"已选标签";
    labelOfTag.font = [UIFont systemFontOfSize:13];
    labelOfTag.textColor = RGB0X(0x000000);
    [labelOfTag mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(15);
        make.left.equalTo(self.view).with.offset(10);
        make.height.offset(20);
        make.width.offset(100);
        
    }];
    self.labelOfNum1 = [[UILabel alloc] init];
    [self.view addSubview:self.labelOfNum1];
    
    
    //    self.labelOfNum1.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.topdataArr.count];
    //    NSLog(@"AAAAAAAAAAAAAAAAAA%ld", self.topdataArr.count);
    
    
    self.labelOfNum1.textColor = RGB0X(0x009dff);
    self.labelOfNum1.font = [UIFont systemFontOfSize:12];
    [self.labelOfNum1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(15);
        make.height.offset(20);
        make.width.offset(10);
        make.right.equalTo(self.view).with.offset(-30);
        
    }];
    UILabel *labelOfNum2 = [[UILabel alloc] init];
    [self.view addSubview:labelOfNum2];
    labelOfNum2.text = @"/ 6";
    labelOfNum2.textColor = RGB0X(0xb6b6b6);
    labelOfNum2.font = [UIFont systemFontOfSize:12];
    [labelOfNum2 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(15);
        make.height.offset(20);
        make.left.equalTo(self.labelOfNum1.mas_right);
        make.right.equalTo(self.view).with.offset(-10);
        
    }];
    
    UILabel *labelOfOne = [[UILabel alloc] init];
    [self.view addSubview:labelOfOne];
    labelOfOne.backgroundColor =RGB0X(0xb6b6b6);
    [labelOfOne mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(150);
        make.left.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset(-10);
        make.height.offset(1);
        
        
    }];
    
    UILabel *labelAllTag = [[UILabel alloc] init];
    [self.view addSubview:labelAllTag];
    labelAllTag.text = @"全部标签";
    labelAllTag.font = [UIFont systemFontOfSize:13];
    [labelAllTag mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfOne.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(10);
        make.height.offset(20);
        make.width.offset(100);
    }];
    
    
    
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.itemSize = CGSizeMake((SCREEN_WIDTH - 50) / 4, 25);
    
    
    
    
    
    //直接布局 上下左右
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    
    
    
    UICollectionViewFlowLayout *layout1 = [[UICollectionViewFlowLayout alloc] init];
    layout1.minimumLineSpacing = 10;
    layout1.minimumInteritemSpacing = 10;
    layout1.itemSize = CGSizeMake((SCREEN_WIDTH) / 5, 25);

    //直接布局 上下左右
    layout1.sectionInset = UIEdgeInsetsMake(0, 0, 10, 10);
    
    
    self.topCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 45, SCREEN_WIDTH, 60) collectionViewLayout:layout1];
    
    
    [self.view addSubview:self.topCollectionView];
    [self.topCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfTag.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(10);
        make.right.equalTo(self.view).with.offset(0);
        make.height.offset(80);
        make.width.offset(200);
    }];
    [self.topCollectionView layoutIfNeeded];
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    self.topCollectionView.pagingEnabled = NO;
    self.topCollectionView.delegate = self;
    self.topCollectionView.dataSource = self;
    
    [self.topCollectionView registerNib:[UINib nibWithNibName:@"ICN_SkillTagDeleteCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ICN_SkillTagDeleteCollectionViewCell"];
    self.topCollectionView.showsHorizontalScrollIndicator = NO;
    
    //    self.collectionView.bounces = NO;
    self.topCollectionView.backgroundColor = [UIColor whiteColor];
    
    
    self.allCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 180, SCREEN_WIDTH, 200) collectionViewLayout:layout];
    [self.view addSubview:self.allCollectionView];
    //
    [self.allCollectionView layoutIfNeeded];
    //    self.automaticallyAdjustsScrollViewInsets = NO;
    self.allCollectionView.pagingEnabled = NO;
    self.allCollectionView.delegate = self;
    self.allCollectionView.dataSource = self;
    
    [self.allCollectionView registerNib:[UINib nibWithNibName:@"ICN_SkillTagCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ICN_SkillTagCollectionViewCell"];
    self.allCollectionView.showsHorizontalScrollIndicator = NO;
    
    //    self.collectionView.bounces = NO;
    self.allCollectionView.backgroundColor = [UIColor whiteColor];
    
    
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    [collectionView.collectionViewLayout invalidateLayout];
    if (self.allCollectionView == collectionView) {
        return self.dataArr.count;
    }else if(self.topCollectionView == collectionView)
    {
        self.labelOfNum1.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.topdataArr.count];
        return self.topdataArr.count;
    }
    return 0;
    
    
    
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.allCollectionView == collectionView) {
        ICN_SkillTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ICN_SkillTagCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = RGB0X(0x7e94a2).CGColor;
        cell.layer.cornerRadius = 4;
        
        ICN_SkillTagAllsModel *model = self.dataArr[indexPath.row];
        
        if ([_selectLabelArray containsObject:model]) {
            cell.contentLab.textColor = [UIColor blueColor];
        } else {
            cell.contentLab.textColor = [UIColor grayColor];
        }
        cell.model = model;
        
        return cell;
    }else if(collectionView == self.topCollectionView)
    {
        ICN_SkillTagDeleteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ICN_SkillTagDeleteCollectionViewCell" forIndexPath:indexPath];
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = RGB0X(0x7e94a2).CGColor;
        cell.layer.cornerRadius = 4;
        NSLog(@"%@",self.topdataArr);
        //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            cell.model = self.topdataArr[indexPath.row];
        //});
        
        return cell;
    }
    return nil;
}
-(void)stopAnimation{

}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.allCollectionView) {
        ICN_SkillTagAllsModel *model = self.dataArr[indexPath.row];
        self.index = model.id;
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请点击确认确认选定标签"];
        
        _selectLabelArray = [NSMutableArray arrayWithObject:model];
        NSLog(@"%@",_selectLabelArray);
        [collectionView reloadData];
    }else
    {
        ICN_SkillTagAllsModel *model = self.topdataArr[indexPath.row];
        NSString *str = model.id;
        
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        [self.dic setObject:token forKey:@"token"];
        [self.dic setObject:str forKey:@"skillId"];
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySkillDeleteTrainEx params:self.dic success:^(id result) {
            NSLog(@"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL%@", self.dic);
            
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MemberSkillList params:@{@"token":token} success:^(id result) {
                NSLog(@"feewfejfopewjeqwopj%@", result);
                BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (firstModel.code == 0) {
                    self.topdataArr = nil;
                    for (NSDictionary *dic in [result valueForKey:@"result"]) {
                        ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                        
                        [self.topdataArr addObject:model];
                    }
                    [self.topCollectionView reloadData];
                }else{
                    self.topdataArr = nil;
                    [self.topCollectionView reloadData];
                }
            } failure:^(NSDictionary *errorInfo) {
                
                
            }];
        } failure:^(NSDictionary *errorInfo) {
        }];
        
        
        
    }
}
//删除标签
- (void)delButton{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    [self.dic setObject:token forKey:@"token"];
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySkillEditTrainEx params:self.dic success:^(id result) {
        NSLog(@"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL%@", self.dic);
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_SkillTagAllsModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                
                [resultArr removeObject:model];
            }
            
        }else{
            
        }
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
    
}
//用户标签
- (void) configdataChoice{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberSkill/memberSkillList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfSkill = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc]init];
            model.userId = result[@"id"];
            [self.dic setObject:model.userId forKey:@"labelId"];
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfSkill addObject:model];
            
        }
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
}
//全部标签
- (void)configData{
    
    
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *diction = [[NSMutableDictionary alloc] init];
    [diction setObject:token forKey:@"token"];
    NSString *str = @"1";
    [diction setObject:str forKey:@"page"];
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MemberSkillList params:@{@"token":token} success:^(id result) {
        NSLog(@"feewfejfopewjeqwopj%@", result);
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            //            NSMutableArray<ICN_SkillTagAllsModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                
                [self.topdataArr addObject:model];
            }
            [self.topCollectionView reloadData];
        }else{
            
        }
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberSkills/skillList" params:diction success:^(id result) {
        NSLog(@"ededededededededededededededededdddde%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc]init];
            
            [dic setValue:model.userId forKey:@"labelId"];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        
        
        [self.allCollectionView reloadData];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
