//
//  ICN_EditTrainAndExViewController.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_TrainAndExperienceModel;
@interface ICN_EditTrainAndExViewController : BaseViewController
@property (nonatomic, strong) ICN_TrainAndExperienceModel *modelTrain;
@end
