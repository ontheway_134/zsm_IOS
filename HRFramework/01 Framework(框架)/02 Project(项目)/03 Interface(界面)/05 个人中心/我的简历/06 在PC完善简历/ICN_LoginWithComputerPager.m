//
//  ICN_LoginWithComputerPager.m
//  ICan
//
//  Created by albert on 2017/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LoginWithComputerPager.h"

@interface ICN_LoginWithComputerPager ()

@end

@implementation ICN_LoginWithComputerPager

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置标题
    self.naviTitle = @"网页登录";
    [self setHiddenDefaultNavBar:NO];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
