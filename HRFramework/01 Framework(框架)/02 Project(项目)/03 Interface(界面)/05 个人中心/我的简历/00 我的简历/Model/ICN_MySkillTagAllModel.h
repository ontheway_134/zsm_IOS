//
//  ICN_MySkillTagAllModel.h
//  ICan
//
//  Created by apple on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MySkillTagAllModel : BaseOptionalModel
@property (nonatomic, copy) NSString *skillLabel;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *userId;
@end
