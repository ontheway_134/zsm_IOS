//
//  ICN_MyPerfectsModel.h
//  ICan
//
//  Created by apple on 2017/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyPerfectsModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *isOpened;
@property (nonatomic, copy) NSString *toFriend;
@property (nonatomic, copy) NSString *completeness;
@property (nonatomic, copy) NSString *refreshDate;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *isEducation;
@property (nonatomic, copy) NSString *isProfile;
@property (nonatomic, copy) NSString *isSkill;
@property (nonatomic, copy) NSString *isWork;



@end
