//
//  ICN_MyResumeTrainExperienceModel.h
//  ICan
//
//  Created by apple on 2016/12/29.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyResumeTrainExperienceModel : BaseOptionalModel
@property (nonatomic, copy) NSString *expId;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *summary;
@end
