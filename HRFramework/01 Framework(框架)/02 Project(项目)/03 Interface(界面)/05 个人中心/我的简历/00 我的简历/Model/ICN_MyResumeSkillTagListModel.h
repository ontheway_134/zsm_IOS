//
//  ICN_MyResumeSkillTagListModel.h
//  ICan
//
//  Created by apple on 2017/1/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyResumeSkillTagListModel : BaseOptionalModel
@property (nonatomic, copy) NSString *skillLabel;
@property (nonatomic, copy) NSString *userId;
@end
