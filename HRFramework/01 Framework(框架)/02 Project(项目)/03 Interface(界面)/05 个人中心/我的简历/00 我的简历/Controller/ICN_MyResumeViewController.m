//
//  ICN_MyResumeViewController.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyResumeViewController.h"
#import "ICN_PerfectTableViewCell.h"
#import "ICN_Basic informationTableViewCell.h"
#import "ICN_SkillTagTableViewCell.h"
#import "ICN_ResumeWorkExperienceTableViewCell.h"
#import "ICN_ResumeEducationExperienceTableViewCell.h"
#import "ICN_ResumeTrainTableViewCell.h"
#import "ICN_ResumeHideTableViewCell.h"
#import "ICN_PreviewResumeViewController.h"
#import "ICN_SetUserInfoViewController.h"
#import "ICN_SkillTagViewController.h"
#import "ICN_WorkExperienceViewController.h"
#import "ICN_EducationAndExperienceViewController.h"
#import "ICN_TrainAndExperienceViewController.h"
#import "ICN_MyResumeSection.h"
#import "ICN_LoginWithComputerPager.h" // 去PC端完善简历UIpager


#import "ICN_MyResumeWorkExModel.h"
#import "ICN_MyResumeEducationExperienceModel.h"
#import "ICN_MyResumeTrainExperienceModel.h"
#import "ICN_SkillTagAllsModel.h"
#import "ICN_MyResumeBasicInformModel.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_MyPerfectsModel.h"

@interface ICN_MyResumeViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    ICN_SkillTagTableViewCell *skillCell;
    NSString * isOpenStr;/*判断当前简历是否是打开的*/
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrOfSkill;
@property (nonatomic, strong) NSMutableArray *arrOfWork;
@property (nonatomic, strong) NSMutableArray *arrOfEducation;
@property (nonatomic, strong) NSMutableArray *arrOfTrain;
@property (nonatomic, strong) NSMutableArray *arrOfXiao;
@property (nonatomic, strong) NSMutableArray *arrOfPerfect;

@property (nonatomic, strong) NSMutableDictionary *dic;

@property (nonatomic,strong)  NSMutableArray *topdataArr;

@property (nonatomic, strong)ICN_MyResumeSection *sectionWork;
@property (nonatomic, strong)NSString *typeTypeStr;
@end

@implementation ICN_MyResumeViewController



#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
    self.dic = [[NSMutableDictionary alloc] init];
}
- (void)setMJRefresh {
    //下拉刷新
    //    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(configData)];
    //上拉加载更多
    //    self.ResumeManageTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(mjMoreNetRequest)];
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configDataWork];
        [self configDataEducation];
        [self configDataTrain];
        [self configDataxiaoxi];
        [self configDataPerfect];
        [self getDataSkill];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark ----------------懒加载---------------------
- (NSMutableArray *)topdataArr
{
    if (!_topdataArr) {
        _topdataArr = [NSMutableArray array];
    }
    return _topdataArr;
}
- (void)getDataSkill{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MemberSkillList params:@{@"token":token} success:^(id result) {
        NSLog(@"feewfejfopewjeqwopj%@", result);
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            self.topdataArr = nil;
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                
                [self.topdataArr addObject:model];
            }
            [self.tableView reloadData];
        }

    } failure:^(NSDictionary *errorInfo) {
        
        
    }];

}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7;
}
#pragma mark - ---------- Section的内容 ----------
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        self.sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];

        self.sectionWork.titleLabel.text = @"基本信息";

        self.typeTypeStr = @"1";
        [self.sectionWork callBackWithClickBlock:^(NSString *title) {
            if ([title isEqualToString:@"基本信息"]) {
                [self basicInformationClick];
            }
        }];
        self.sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return self.sectionWork;
        
    }else if  (section == 2) {
    
        self.sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        self.sectionWork.titleLabel.text = @"技能标签";
        self.sectionWork.bianjiButton.tag = 2;

        self.typeTypeStr = @"2";

        [self.sectionWork callBackWithClickBlock:^(NSString *title) {
            if ([title isEqualToString:@"技能标签"]) {
                [self skillTagClick];
            }
        }];
        self.sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return self.sectionWork;
        
    }else if  (section == 3) {
        
        self.sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        self.sectionWork.titleLabel.text = @"工作经历";
        self.sectionWork.bianjiButton.tag = 3;
        [self.sectionWork callBackWithClickBlock:^(NSString *title) {
            if ([title isEqualToString:@"工作经历"]) {
                [self workExperienceClick];
            }
        }];
        self.sectionWork.contentView.backgroundColor = [UIColor whiteColor];

        
        return self.sectionWork;
    }else if  (section == 4) {
    
        self.sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        self.sectionWork.titleLabel.text = @"教育经历";
        self.sectionWork.bianjiButton.tag = 4;
        [self.sectionWork callBackWithClickBlock:^(NSString *title) {
            if ([title isEqualToString:@"教育经历"]) {
                [self educationExClick];
            }
        }];

        self.typeTypeStr = @"4";
        self.sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return self.sectionWork;
        
    
    }else if  (section == 5) {
    
        self.sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        self.sectionWork.backgroundColor = [UIColor whiteColor];
        
        self.sectionWork.titleLabel.text = @"培训经历";
        self.sectionWork.bianjiButton.tag = 5;
        //跳转
        [self.sectionWork callBackWithClickBlock:^(NSString *title) {
            if ([title isEqualToString:@"培训经历"]) {
                [self editTrainClick];
            }
        }];

        self.typeTypeStr = @"5";
        self.sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return self.sectionWork;
    
    } else  {
    
        return nil;
    }
    
        
}
//section 不随tableView滑动
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.tableView)
        
    {
        
        CGFloat sectionHeaderHeight = 64; //header高度
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }
        
    }
    
    
    
}
    
#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 5;
    } else if (section == 6) {
    
        return 5;
    }else {
    
        return 45;
        
    }
}
#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 3){
    
        return self.arrOfWork.count;
        
    }else if (section == 4){
    
        return self.arrOfEducation.count;
    }else if (section == 5){
    
    
        return self.arrOfTrain.count;
    }else {
    
        return 1;
    }
    
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.section == 0) {
        return 100;
    }else if (indexPath.section == 1){
    
        return 75;
    }else  if (indexPath.section == 2){
    
        return 87;
        
    }else if (indexPath.section == 3) {
    
        return 100;
    
    }else if (indexPath.section == 4) {
    
        return 80;
    
    } else if (indexPath.section == 5) {
    
        return 80;
    
    }else {
    
    
        return 44;
    }
    
   
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
    if (indexPath.section == 0) {
        ICN_PerfectTableViewCell *cell = [self.tableView                                 dequeueReusableCellWithIdentifier:@"ICN_PerfectTableViewCell"
                                                                                                              forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ICN_MyPerfectsModel *model = self.arrOfPerfect.firstObject;
        
        cell.model = model;
        [cell callBackCallPCPagerBlock:^{
           // 添加跳转到PC端详情
            ICN_LoginWithComputerPager *pager = [[ICN_LoginWithComputerPager alloc] init];
            [self currentPagerJumpToPager:pager];
        }];
        
        return cell;
    }else if (indexPath.section == 1) {
    
        ICN_Basic_informationTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_Basic informationTableViewCell" forIndexPath:indexPath];

         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ICN_MyResumeBasicInformModel *model = self.arrOfXiao.firstObject;
        cell.model = model;
        
        
        return cell;
    
    }else if (indexPath.section == 2) {
    //技能标签
        skillCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_SkillTagTableViewCell" forIndexPath:indexPath];

        skillCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        skillCell.dataSource = self.topdataArr;
        return skillCell;
        
    
    }else if (indexPath.section == 3) {
    //工作经验
        ICN_ResumeWorkExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeWorkExperienceTableViewCell" forIndexPath:indexPath];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ICN_MyResumeWorkExModel *model = self.arrOfWork[indexPath.row];
        cell.modelWork = model;
        return cell;
    
    }else if (indexPath.section == 4){
    //教育经历
        ICN_ResumeEducationExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeEducationExperienceTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        ICN_MyResumeEducationExperienceModel *model = self.arrOfEducation[indexPath.row];
        cell.modelEducation = model;
        return cell;
    
    
    }else if (indexPath.section == 5) {
    
    //培训经历
        ICN_ResumeTrainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeTrainTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        ICN_MyResumeTrainExperienceModel *model = self.arrOfTrain[indexPath.row];
        cell.modeloOfTrain = model;
        

        return cell;
    
    } else {
    //隐藏简历
        ICN_ResumeHideTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeHideTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        ICN_MyPerfectsModel *model = [[ICN_MyPerfectsModel alloc] init];
        model = self.arrOfPerfect[0];
        /*0 的时候代表隐藏 1 代表打开 */
        if ([model.isOpened isEqualToString:@"0"]) {
            cell.hiddenButton.selected = NO;
            cell.hiddenLabel.text = @"隐藏简历";
        }else if ([model.isOpened isEqualToString:@"1"]){
            cell.hiddenButton.selected = YES;
            cell.hiddenLabel.text = @"公开简历";
        }
        NSLog(@"%d",cell.hiddenButton.selected);
        [cell.hiddenButton addAction:^(NSInteger tag) {
            /*初始化token*/
            NSString *token;
            
            
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            /*传值之前需要判断 当前按钮状态是什么 如果是打开的传0 如果是关闭的传1 */
            if (cell.hiddenButton.selected) {
                isOpenStr = @"0";
            }else{
                isOpenStr = @"1";
            }
            NSDictionary *paramDic = @{
                                       @"token":token,
                                       @"isOpened":isOpenStr,
                                       };
            [[HRRequest manager]POST:PATH_isResumeOpened para:paramDic success:^(id data) {
                NSLog(@"%@",data);
                /*这个地方需要判断更改之前isOpen的属性是什么状态的*/
                if ([isOpenStr isEqualToString:@"0"]) {
                    cell.hiddenButton.selected = NO;
                    cell.hiddenLabel.text = @"隐藏简历";
                }
                else if ([isOpenStr isEqualToString:@"1"]){
                    cell.hiddenButton.selected = YES;
                    cell.hiddenLabel.text = @"公开简历";
                }
            } faiulre:^(NSString *errMsg) {
                NSLog(@"%@",errMsg);
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"状态更改失败"];
            }];
            
        }];
        return cell;
    
    }
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)creationView{
    
    self.navigationItem.title = @"我的简历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.backgroundView = nil;
    self.view.backgroundColor = RGB(235, 236, 237);
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- 103);


    UIButton *buttonResume = [[UIButton alloc] init];
    [self.view addSubview:buttonResume];
    [buttonResume addTarget:self action:@selector(previewResumeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [buttonResume mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.offset(39);
        
    }];
    buttonResume.backgroundColor = RGB(41, 141, 250);
    UIImageView *imagePreview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"预览简历"]];
    [buttonResume addSubview:imagePreview];
    [imagePreview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(buttonResume).with.offset(-35);
        make.centerY.equalTo(buttonResume);
        make.width.offset(20);
        make.height.offset(imagePreview.width);
        
    }];
    UILabel *label = [[UILabel alloc] init];
    [buttonResume addSubview:label];
    label.text = @"预览简历";
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(buttonResume).with.offset(35);
        make.centerY.equalTo(buttonResume);
        make.width.offset(100);
        make.height.offset(20);
        
    }];
    
    
    
    
}
- (void)regeditCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_PerfectTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_PerfectTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_Basic informationTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_Basic informationTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_SkillTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_SkillTagTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeWorkExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeWorkExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeEducationExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeEducationExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeTrainTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeTrainTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeHideTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeHideTableViewCell"];
    

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyResumeSection" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ICN_MyResumeSection"];


}
//处理技能标签接口
- (void)configDataSkill{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberSkill/memberSkillList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfSkill = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc]init];
            model.userId = result[@"id"];
            [self.dic setObject:model.userId forKey:@"labelId"];
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfSkill addObject:model];
            
        }
        
        
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"%@",errorInfo);
    }];

}
//处理工作经历接口
- (void)configDataWork{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/WorkExperience/workExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        
        self.arrOfWork = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeWorkExModel *model = [[ICN_MyResumeWorkExModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfWork addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

//处理教育经历接口
- (void)configDataEducation{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/EducationExperience/educationExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfEducation = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeEducationExperienceModel *model = [[ICN_MyResumeEducationExperienceModel alloc]init];
            [dic setValue:model.expID forKey:@"id"];
            
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfEducation addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}
//培训经历
- (void)configDataTrain{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/TrainExperience/trainExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfTrain = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeTrainExperienceModel *model = [[ICN_MyResumeTrainExperienceModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfTrain addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
    } failure:^(NSDictionary *errorInfo) {
        
    }]; 
}
//基本消息接口
- (void)configDataxiaoxi{

    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
        
        NSLog(@"ededededededededededededededededdddde%@", result);
        
        NSDictionary *dic1 =result[@"result"];
        
        NSLog(@"111111111111111111111111%@",dic1);
        _arrOfXiao = [[NSMutableArray alloc]init];

        
        ICN_MyResumeBasicInformModel *model = [[ICN_MyResumeBasicInformModel alloc] initWithDictionary:dic1 error:nil];
        [_arrOfXiao addObject:model];

        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    

    
}
//简历完善度
- (void)configDataPerfect{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberResume/getResumeStatus" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaasadfasda%@", result);
        NSDictionary *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        
        self.arrOfPerfect = [[NSMutableArray alloc]init];
        
//            ICN_MyPerfectsModel *model = [[ICN_MyPerfectsModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [self.arrOfPerfect addObject:model];
        ICN_MyPerfectsModel *model = [[ICN_MyPerfectsModel alloc] initWithDictionary:dic1 error:nil];
        [self.arrOfPerfect addObject:model];
            
            
        
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}


//预览简历
- (void)previewResumeBtnClick{

    ICN_PreviewResumeViewController *vc = [[ICN_PreviewResumeViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    

}
//基本信息编辑
- (void)basicInformationClick{

        ICN_SetUserInfoViewController *vc1 = [[ICN_SetUserInfoViewController alloc] init];
        [self.navigationController pushViewController:vc1 animated:YES];
   
}
//技能标签编辑
- (void)skillTagClick{

    ICN_SkillTagViewController *vc2 = [[ICN_SkillTagViewController alloc] init];
    [self.navigationController pushViewController:vc2 animated:YES];
    

}
//工作经历
- (void)workExperienceClick{

    ICN_WorkExperienceViewController *vc3 = [[ICN_WorkExperienceViewController alloc] init];
    [self.navigationController pushViewController:vc3 animated:YES];
    
}
//教育经历
- (void)educationExClick{
    
    ICN_EducationAndExperienceViewController *vc4 = [[ICN_EducationAndExperienceViewController alloc] init];
    [self.navigationController pushViewController:vc4 animated:YES];
    
}
//培训经历
- (void)editTrainClick{
    ICN_TrainAndExperienceViewController *vc5 = [[ICN_TrainAndExperienceViewController alloc] init];
    [self.navigationController pushViewController:vc5 animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
