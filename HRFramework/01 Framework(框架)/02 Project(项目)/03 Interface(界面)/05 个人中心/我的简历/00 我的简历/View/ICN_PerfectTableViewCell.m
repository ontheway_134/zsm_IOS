//
//  ICN_PerfectTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PerfectTableViewCell.h"
#import "ICN_MyPerfectsModel.h"
@implementation ICN_PerfectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.rootView.layer.cornerRadius = 4.0f;
    self.rootView.layer.masksToBounds = YES;
    
}

- (void)callBackCallPCPagerBlock:(CallPCPagerBlock)block{
    self.block = block;
}

- (IBAction)PCResumeButton:(UIButton *)sender {
    
    self.block();
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_MyPerfectsModel *)model{

    if ((model.completeness.integerValue) == 25) {
        self.imageViewResume.image = [UIImage imageNamed:@"25%"];
        
    } else if ((model.completeness.integerValue) == 50) {
    
        self.imageViewResume.image = [UIImage imageNamed:@"50%"];
        
    } else if ((model.completeness.integerValue) == 75) {
    
        self.imageViewResume.image = [UIImage imageNamed:@"75%"];

        
    } else if ((model.completeness.integerValue) == 100) {
    
         self.imageViewResume.image = [UIImage imageNamed:@"100%"];
    }

}
@end
