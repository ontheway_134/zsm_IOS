//
//  ICN_SkillTagTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MySkillTagAllModel;
@interface ICN_SkillTagTableViewCell : UITableViewCell

@property (nonatomic, strong)ICN_MySkillTagAllModel *model;
@property (nonatomic,strong) NSMutableArray * dataSource;
@end
