//
//  ICN_ResumeTrainTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ResumeTrainTableViewCell.h"
#import "ICN_MyResumeTrainExperienceModel.h"
@implementation ICN_ResumeTrainTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModeloOfTrain:(ICN_MyResumeTrainExperienceModel *)modeloOfTrain{

    NSString *str = [modeloOfTrain.startDate.description stringByAppendingString:@"-"];
    self.timeTrainLan.text = [str stringByAppendingString:modeloOfTrain.endDate];
    self.trainSummary.text = modeloOfTrain.summary;
    

}
@end
