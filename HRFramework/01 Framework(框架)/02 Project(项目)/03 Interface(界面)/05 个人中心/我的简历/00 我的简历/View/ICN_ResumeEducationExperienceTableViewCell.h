//
//  ICN_ResumeEducationExperienceTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyResumeEducationExperienceModel;
@interface ICN_ResumeEducationExperienceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UILabel *schoolLab;
@property (weak, nonatomic) IBOutlet UILabel *qualificationAndMajorLab;
@property (nonatomic, strong) ICN_MyResumeEducationExperienceModel *modelEducation;

@end
