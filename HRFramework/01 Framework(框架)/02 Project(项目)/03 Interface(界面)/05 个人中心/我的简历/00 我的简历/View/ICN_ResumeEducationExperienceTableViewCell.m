//
//  ICN_ResumeEducationExperienceTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ResumeEducationExperienceTableViewCell.h"
#import "ICN_MyResumeEducationExperienceModel.h"
@implementation ICN_ResumeEducationExperienceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModelEducation:(ICN_MyResumeEducationExperienceModel *)modelEducation{

    NSString *str = [modelEducation.enrolDate.description stringByAppendingString:@"-"];
    self.timeLab.text = [str stringByAppendingString:modelEducation.graduationDate];
    self.schoolLab.text = modelEducation.school;
    NSString *str2 = [modelEducation.qualificationName.description stringByAppendingString:@"|"];
    self.qualificationAndMajorLab.text = [str2 stringByAppendingString:modelEducation.major];
    

}
@end
