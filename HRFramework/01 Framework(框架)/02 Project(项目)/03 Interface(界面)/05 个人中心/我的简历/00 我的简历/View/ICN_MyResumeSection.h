//
//  ICN_MyResumeSection.h
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickBlock)(NSString *title);

@interface ICN_MyResumeSection : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *bianjiButton;
@property (nonatomic , copy)ClickBlock block;


- (void)callBackWithClickBlock:(ClickBlock)block;

@end
