//
//  ICN_ResumeWorkExperienceTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ResumeWorkExperienceTableViewCell.h"
#import "ICN_MyResumeWorkExModel.h"
@implementation ICN_ResumeWorkExperienceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModelWork:(ICN_MyResumeWorkExModel *)modelWork{

    NSString *str1 = [modelWork.entryDate stringByAppendingString:@"-"];
    self.timeLab.text = SF(@"%@%@",str1,modelWork.outDate);
    NSString *strOfName = [modelWork.companyName stringByAppendingString:@"/"];
    self.companyNameAndPosition.text = [strOfName stringByAppendingString:modelWork.position];
    
    self.summaryWork.text = modelWork.summary;
    

}
@end
