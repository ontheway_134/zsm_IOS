//
//  ICN_EditWorkingExModel.h
//  ICan
//
//  Created by apple on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_EditWorkingExModel : BaseOptionalModel
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *entryDate;
@property (nonatomic, copy) NSString *outData;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *summary;
@end
