//
//  ICN_EditWorkingExperienceViewController.h
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_WorkingExperienceModel;
@interface ICN_EditWorkingExperienceViewController : BaseViewController

@property (nonatomic , strong)ICN_WorkingExperienceModel *model;

@end
