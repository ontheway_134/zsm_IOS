//
//  ICN_AddWorkingExperienceViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_AddWorkingExperienceViewController.h"
#import "ICN_NickNameTableViewCell.h"
#import "ICN_WorkingContentTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_AddWorkingUploadModel.h"
#import "BaseOptionalModel.h"
#import "ICN_AddWorkingUploadModel.h"

@interface ICN_AddWorkingExperienceViewController ()<UITableViewDelegate, UITableViewDataSource,birthdayDelegate, UITextFieldDelegate>
{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *tempOutTime;
    ICN_NickNameTableViewCell *nickcell;
    ICN_NickNameTableViewCell *positioncell;
    ICN_WorkingContentTableViewCell *jlcell;
    ICN_NoImageTableCell *timecell1;
    ICN_NoImageTableCell *timecell2;
}
@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (nonatomic, strong) NSMutableDictionary *dic;
@property (nonatomic, strong) ICN_AddWorkingUploadModel *viewmodel;

@property (nonatomic, strong)NSString *typeTypeStr;
@end

@implementation ICN_AddWorkingExperienceViewController
#pragma mark - ---------- 生命周期 ----------


- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    self.dic = [[NSMutableDictionary alloc] init];
    
    
    // Do any additional setup after loading the view.
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 4;
    }else {
        
        return 1;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 40;
    }else {
        
        return 150;
        
    }
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    if (indexPath.section == 0) {
        
        
        if (indexPath.row == 0) {
            
            nickcell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
            nickcell.leftLabel.text = @"公司名称";
            
            if (!([USERDEFAULT objectForKey:@"公司名称"] == nil)) {
                [self.dic setObject:[USERDEFAULT objectForKey:@"公司名称"] forKey:@"companyName"];
            }
            
            

            return nickcell;
        } else if (indexPath.row == 1) {
            positioncell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
            if (!([USERDEFAULT objectForKey:@"你的职位"] == nil)) {
                [self.dic setObject:[USERDEFAULT objectForKey:@"你的职位"] forKey:@"position"];
            }
            positioncell.leftLabel.text = @"你的职位";
            
            

            return positioncell;
            
        }else if (indexPath.row == 2) {
            timecell1 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
            timecell1.leftLabel.text = @"入职时间";
            timecell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return timecell1;
        } else {
            timecell2 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
            timecell2.leftLabel.text = @"离职时间";
            timecell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return timecell2;
        }
        
    }else {
        
        jlcell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_WorkingContentTableViewCell" forIndexPath:indexPath];
        jlcell.ContentLab.text = @"工作内容";
        jlcell.signTextView.text = @"";
        
        return jlcell;
        
    }
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // 在结束编辑的时候移除键盘
    [jlcell.signTextView resignFirstResponder];
    [positioncell.rightTextField resignFirstResponder];
    [nickcell.rightTextField resignFirstResponder];

    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 2) {
            
            [self setUserBirthday];
        }else if (indexPath.row == 3) {
            
            [self setUserquit];
            
            
            
        }
    }
    
    
    NSLog(@"wwwwwwwwwwwwwww%@", self.dic);
    
}
- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}
- (void)dismissContactView {
    [grayView removeFromSuperview];
}
#pragma mark - ---------- 入职时间 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)birthdayChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:2 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempBirthdayStr == NULL) {
        tempBirthdayStr = @"1960年1月";
    }
    cell.rightLabel.text = tempBirthdayStr;
    [self.dic setObject:tempBirthdayStr forKey:@"entryDate"];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1960年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
    tempOutTime = [NSString stringWithFormat:@"%@%@",year,month];
}

#pragma mark - ---------- 离职时间 ----------
- (void)setUserquit {
    [self setGrayView];
    
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(quitChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)quitChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:3 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempOutTime == NULL) {
        tempOutTime = @"1966年1月";
    }
    cell.rightLabel.text = tempOutTime;
    [self.dic setObject:tempOutTime forKey:@"outDate"];
    
    [grayView removeFromSuperview];
}




- (void)creationView{
    
    self.navigationItem.title = @"添加工作经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        
    
    
}
- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NickNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NickNameTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_WorkingContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_WorkingContentTableViewCell"];
      [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NoImageTableCell" bundle:nil] forCellReuseIdentifier:@"ICN_NoImageTableCell"];
}



//保存
- (void)saveAction{
    
    
    NSString *token;
    // 在结束编辑的时候移除键盘
    [jlcell.signTextView resignFirstResponder];
    [positioncell.rightTextField resignFirstResponder];
    [nickcell.rightTextField resignFirstResponder];
    
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        
        [self.dic setObject:token forKey:@"token"];

        if (nickcell.rightTextField.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写公司名称"];
        } else if (positioncell.rightTextField.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写职位"];
        } else if (timecell1.rightLabel.text.length == 0 || [timecell1.rightLabel.text isEqualToString:@"请输入"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写入职时间"];
        } else if (timecell2.rightLabel.text.length == 0 || [timecell2.rightLabel.text isEqualToString:@"请输入"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写离职时间"];
        } else if (jlcell.signTextView.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写工作经历"];
        } else {
            NSInteger year1 = [[timecell1.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
            NSInteger month1 = timecell1.rightLabel.text.length == 7 ? [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
            
            NSInteger year2 = [[timecell2.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
            NSInteger month2 = timecell2.rightLabel.text.length == 7 ? [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
            if (year1>year2) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
            } else if (year1 == year2 && month1 > month2) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
            } else {
                
                [self.dic setObject:nickcell.rightTextField.text forKey:@"companyName"];
                [self.dic setObject:positioncell.rightTextField.text forKey:@"position"];
                [self.dic setObject:jlcell.signTextView.text forKey:@"summary"];
                [self.dic setObject:timecell1.rightLabel.text forKey:@"entryDate"];
                [self.dic setObject:timecell2.rightLabel.text forKey:@"outDate"];
                
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyWorkingExperience params:self.dic success:^(id result) {
                    
                    
                    [self.navigationController popViewControllerAnimated:YES];
                } failure:^(NSDictionary *errorInfo) {
                    
                    
                }];
            }
    
        }

    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
