//
//  ICN_WorkingContentTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_WorkingContentTableViewCell.h"
#import "ICN_EditTrainAndExModel.h"
@implementation ICN_WorkingContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.signTextView.delegate = self;
    if (self.signTextView.text.length == 13 && [self.signTextView.text containsString:@"."] && [self.signTextView.text containsString:@"描述"]) {
        self.numberLabel.text = SF(@"%d/500",0);
    }else{
        self.numberLabel.text = SF(@"%ld/500",self.signTextView.text.length);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if (textView.text != nil && [textView.text isEqualToString:@"描述一下自己的工作内容..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        self.numberLabel.text=[NSString stringWithFormat:@"%d/500",0];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if (textView.text == nil || [textView.text isEqualToString:@""]) {
        textView.text = @"描述一下自己的工作内容...";
        textView.textColor = RGB0X(0x555555);
        self.numberLabel.text = [NSString stringWithFormat:@"%d/500",0];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    // 添加文本域将要改变时候的方法用于判断文本域可不可以发生改变
    
    if (textView.text != nil && textView.text.length <= 500) {
        if (text.length + textView.text.length > 500) {
            return NO;
        }
        return YES;
    }
    
    if (textView.text == nil && text.length <= 500) {
        return YES;
    }else
        return NO;
    
}


- (void)textViewDidChange:(UITextView *)textView
{
    NSString  * nsTextContent = textView.text;
//    self.signTextView.text = textView.text;
    long existTextNum=[nsTextContent length];
    self.numberLabel.text = [NSString stringWithFormat:@"%ld/500",existTextNum];
    
}
//培训经历
- (void)setEditTrainModel:(ICN_EditTrainAndExModel *)editTrainModel{

    self.signTextView.text = editTrainModel.summary;

}



@end
