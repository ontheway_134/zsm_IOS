//
//  ICN_WorkExperienceTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_WorkExperienceTableViewCell.h"
#import "ICN_WorkingExperienceModel.h"
@implementation ICN_WorkExperienceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)callBackEditBlock:(EditBlock)block{
    self.block = block;
}

- (IBAction)editBtnAction:(UIButton *)sender {
    
    if (self.block) {
        self.block(self.model);
    }

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_WorkingExperienceModel *)model{
    _model = model;
    self.contentLabel.text = model.summary;
    NSString *str = [model.entryDate.description stringByAppendingString:@"-"];
    
    NSString *str2 = model.outDate;
    self.workTimeLab.text = [str.description stringByAppendingString:str2];
    NSString *strOfCompany = [model.companyName.description stringByAppendingString:@"/"];
    
    self.contentWorkLab.text = [strOfCompany.description stringByAppendingString:model.position];
    
    self.contentLabel.text = model.summary;

}
@end
