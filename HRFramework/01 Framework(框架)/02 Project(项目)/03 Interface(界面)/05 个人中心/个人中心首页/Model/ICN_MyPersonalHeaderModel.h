//
//  ICN_MyPersonalHeaderModel.h
//  ICan
//
//  Created by apple on 2017/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyPersonalHeaderModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberLogo;
@property (nonatomic, copy) NSString *memberNick;
@property (nonatomic, copy) NSString *memberGender;
@property (nonatomic, copy) NSString *isAuthStatus;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * score;/*积分*/
@property (strong,nonatomic)NSString * message;
@property (strong,nonatomic)NSString * plusv;
@property (strong,nonatomic)NSString * roleId;/*判断是不是企业账号 2 企业 1 个人*/
@end
