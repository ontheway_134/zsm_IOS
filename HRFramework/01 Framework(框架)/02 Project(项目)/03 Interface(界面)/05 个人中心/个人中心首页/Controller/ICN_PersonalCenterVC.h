//
//  ICN_PersonalCenterVC.h
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_PersonalCenterVC : BaseViewController
@property (strong,nonatomic)NSString * memberID;/*唯一标识*/
@property (strong,nonatomic)NSString * scoreStr;/*积分*/
@property (strong,nonatomic)NSString * messageType;/*判断是否有消息*/
@property (strong,nonatomic)NSString * roleType;/*判断是不是企业账号 2是企业 1 是个人*/
@end
