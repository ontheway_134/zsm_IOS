//
//  NSObject+Unicode.m
//  ICan
//
//  Created by apple on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "NSObject+Unicode.h"

@implementation NSObject (Unicode)
- (NSString*)my_description {
    NSString *desc = [self my_description];
    desc = [NSString stringWithCString:[desc cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding];
    return desc;
}
@end
