//
//  ICN_PersonalCenterVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PersonalCenterVC.h"
#import "ICN_SetUserInfoViewController.h"
#import "ICN_PersonHeaderView.h"
#import "ICN_SettingViewController.h"
#import "ICN_MessageCenterViewController.h"
#import "ICN_MyResumeViewController.h"
#import "ICN_DeliveryFeedbackViewController.h"
#import "ICN_MyOrderingViewController.h"
#import "ICN_MyConcerningViewController.h"
#import "ICN_UserDynamicStateListVC.h"
#import "ICN_MyCollectingViewController.h"
#import "ICN_MyCertificationViewController.h"
#import "NormalTableViewCell.h"
#import "ICN_MyMyInformationViewController.h"
#import "ICN_UserDynamicStateListVC.h"
#import "ICN_MyReleasedViewController.h"/*我发布的 辛*/
#import "ICN_MyPersonalHeaderModel.h"
#import "ICN_StartViewController.h"
#import "ICN_CreatCodeViewController.h"   //生成我的二维码
#import "HMScannerController.h"           //扫一扫的三方导入
#import "ICN_creditdetialViewController.h"   //积分明细
#import "ICN_MyLiveViewController.h"/*我的直播*/
#import "ICN_UserHomePagerVC.h"   //用户主页
#import "ICN_SetUserInfoModel.h"
#import "NSString+BlankString.h"
#import "ICN_GuiderPager.h" // 新版引导页
@interface ICN_PersonalCenterVC ()<UITableViewDelegate, UITableViewDataSource>
{
    ICN_MyPersonalHeaderModel *model;
}
@property (strong, nonatomic)UITableView *tableView;
@property (strong, nonatomic)ICN_PersonHeaderView *headerView;
@property (nonatomic , assign , getter=isApproved)BOOL personalApproved; // 是否认证审核中（牛人认证）默认是未被认证
@property (strong,nonatomic)NSString * personalType ;
@property (strong,nonatomic)NSString * plusv;
@end

@implementation ICN_PersonalCenterVC

#pragma  mark - headerView懒加载
- (ICN_PersonHeaderView *)headerView {
    if (!_headerView)
    {
        _headerView = [[NSBundle mainBundle] loadNibNamed:@"ICN_PersonHeaderView" owner:self options:nil].lastObject;
        [_headerView.setUserInfoButton addTarget:self action:@selector(setUserInfo) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.edittingBtn addTarget:self action:@selector(setUserInfo) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.clearButton addTarget:self action:@selector(userMessageClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.getUserMessageButton addTarget:self action:@selector(userMessageClick) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.CertificationButton addTarget:self action:@selector(certificationClickBtn) forControlEvents:UIControlEventTouchUpInside];
        [_headerView.codeBtn addTarget:self action:@selector(codeClickBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _headerView;
}
//index.php/Member/Member/getIndexInfo

#pragma mark 现在变成扫一扫功能，之前的为信息的功能，二期修改
- (void)userMessageClick{
    
    
    
    // 跳转到个人信息补全页面的方法
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
        
        
        NSDictionary *dic1 =result[@"result"];
        
        ICN_SetUserInfoModel *model1 = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        NSLog(@"%@",model1);
        
        if (model1.memberNick == nil || model1.memberGender == nil|| model1.memberLogo == nil
            || model1.hopePositionName == nil|| model1.workStatus == nil||[model1.workStatus isEqualToString:@"1"]||[model1.workStatus isEqualToString:@"2"]){
            
            if ([model1.workStatus isEqualToString:@"1"]){
                if (model1.companyName == nil||model1.memberPosition == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                    
                }else{
                    NSString *cardName = @"天涯刀哥 - 傅红雪";//
                    UIImage *avatar = [UIImage imageNamed:@"avatar"];
                    
                    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
                        
                        NSLog(@"%@",stringValue);
                        
                        // 判断二维码正确性
                        if ([stringValue containsString:@"ixing"]) {
                            // 正确
                            //扫一扫之后做相应的处理
                            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
                            userhome.memberId=[stringValue substringFromIndex:5];
                            [self.navigationController pushViewController:userhome animated:YES];
                        }else{
                           [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"二维码非法"];
                        }
                        
                        
                    }];
                    
                    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
                    [self showDetailViewController:scanner sender:nil];
                    return ;
                }
            }
            
            /*这是个人*/
            if ([model1.workStatus isEqualToString:@"2"]){
                if (model1.memberSchool ==nil ||model1.memberMajor == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                    
                }else{
                    NSString *cardName = @"天涯刀哥 - 傅红雪";//
                    UIImage *avatar = [UIImage imageNamed:@"avatar"];
                    
                    HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
                        
                        NSLog(@"%@",stringValue);
                        
                        // 判断二维码正确性
                        if ([stringValue containsString:@"ixing"]) {
                            // 正确
                            //扫一扫之后做相应的处理
                            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
                            userhome.memberId=[stringValue substringFromIndex:5];
                            [self.navigationController pushViewController:userhome animated:YES];
                        }else{
                            
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"二维码非法"];
                        }
                        
                        
                    }];
                    
                    [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
                    [self showDetailViewController:scanner sender:nil];
                    return ;
                }
            }
            
            ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            NSString *cardName = @"天涯刀哥 - 傅红雪";//
            UIImage *avatar = [UIImage imageNamed:@"avatar"];
            
            HMScannerController *scanner = [HMScannerController scannerWithCardName:cardName avatar:avatar completion:^(NSString *stringValue) {
                
                NSLog(@"%@",stringValue);
                
                // 判断二维码正确性
                if ([stringValue containsString:@"ixing"]) {
                    // 正确
                    //扫一扫之后做相应的处理
                    ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
                    userhome.memberId=[stringValue substringFromIndex:5];
                    [self.navigationController pushViewController:userhome animated:YES];
                }else{
                  [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"二维码非法"];
                }
                
                
            }];
            
            [scanner setTitleColor:[UIColor whiteColor] tintColor:[UIColor greenColor]];
            [self showDetailViewController:scanner sender:nil];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        // 信息校验失败则设置为游客登录
        
    }];

    
    
    
    
    
    
    
}
- (void)setUserInfo {
    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc]init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)certificationClickBtn{
    //=== 跳转到牛人认证页面 yes 时候进来
    if ([self.personalType isEqualToString:@"1"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已认证成功"];
        return ;
    }
    if ([self.personalType isEqualToString:@"2"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您正在审核中"];
        return ;
    }
    ICN_MyCertificationViewController *vc = [[ICN_MyCertificationViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];

}

-(void)datas{

    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"数据加载中"];
    [[HRRequest manager]POST:PATH_getIndexInfo para:dic success:^(id data) {
        NSLog(@"%@",data);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        model = [[ICN_MyPersonalHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:data];
        NSLog(@"%@",model);
        /*这里我需要取出memberID来让我的发布进行使用*/
        self.memberID = model.memberId;
        self.messageType = model.message;
        NSLog(@"%@",model.score);
        self.scoreStr = model.score;
        /*判断是不是牛人*/
        self.plusv = model.plusv;
        if ([self.plusv isEqualToString:@"1"]) {
            [self.headerView.CertificationButton setImage:[UIImage imageNamed:@"牛人"] forState:UIControlStateNormal];
            
        }
        [self.tableView reloadData];
        if (model.memberLogo == nil) {
            self.headerView.memberLogo.image = [UIImage imageNamed:@"占位图"];
        }else {
            [self.headerView.memberLogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
        }
        if (model.memberNick) {
            self.headerView.memberNick.text = model.memberNick;
        }
        if ([model.memberGender isEqualToString:@"1"]) {
            _headerView.memberGender.image = [UIImage imageNamed:@"性别男"];
        } else {
            _headerView.memberGender.image = [UIImage imageNamed:@"性别女"];
        }
        if ([model.plusv isEqualToString:@"0"]) {
            [self.headerView.CertificationButton setImage:[UIImage imageNamed:@"未认证"] forState:UIControlStateNormal];
            /*只有未认证的时候才能进入编辑*/
            self.personalApproved = NO;
            self.personalType = @"0";
        }else if ([model.plusv isEqualToString:@"1"]) {
            [self.headerView.CertificationButton setImage:[UIImage imageNamed:@"已认证"] forState:UIControlStateNormal];
            self.personalApproved = YES;
            self.personalType = @"1";
        }else if ([model.plusv isEqualToString:@"2"]){
            // 认证中
            [self.headerView.CertificationButton setImage:[UIImage imageNamed:@"未认证"] forState:UIControlStateNormal];
            self.personalApproved = YES;
            self.personalType = @"2";
        }
        /*这个地方需要根据当前状态来判断不是事企业中心*/
        self.roleType = model.roleId;
        
        //在这里把用户的头像和nick存起来
        [[NSUserDefaults standardUserDefaults] setValue:@"mylogo" forKey:@"mylogokey"];
        [[NSUserDefaults standardUserDefaults] setValue:@"mynike" forKey:@"mynikekey"];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息获取失败"];
    }];

}

#pragma mark - --- IBActions ---

#pragma mark 生成二维码的点击事件

-(void)codeClickBtn:(UIButton *)btn{

    /*在生成二维码的时候需要 需要判断当前用户信息是否填写完成 如果没有填写完成跳转到完成信息页面 */
    // 跳转到个人信息补全页面的方法
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
        
        
        NSDictionary *dic1 =result[@"result"];
        
        ICN_SetUserInfoModel *model1 = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        NSLog(@"%@",model1);
        
        if (model1.memberNick == nil || model1.memberGender == nil|| model1.memberLogo == nil
            || model1.hopePositionName == nil|| model1.workStatus == nil||[model1.workStatus isEqualToString:@"1"]||[model1.workStatus isEqualToString:@"2"]){
            
            if ([model1.workStatus isEqualToString:@"1"]){
                if (model1.companyName == nil||model1.memberPosition == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                    
                }else{
                    ICN_CreatCodeViewController *creatCode=[[ICN_CreatCodeViewController alloc]init];
                    creatCode.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:creatCode animated:YES];
                    return ;
                }
            }
            
            /*这是个人*/
            if ([model1.workStatus isEqualToString:@"2"]){
                if (model1.memberSchool ==nil ||model1.memberMajor == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                    
                }else{
                    ICN_CreatCodeViewController *creatCode=[[ICN_CreatCodeViewController alloc]init];
                    creatCode.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:creatCode animated:YES];
                    return ;
                }
            }
            ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }else{
            ICN_CreatCodeViewController *creatCode=[[ICN_CreatCodeViewController alloc]init];
            creatCode.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:creatCode animated:YES];
        }
    } failure:^(NSDictionary *errorInfo) {
        // 信息校验失败则设置为游客登录
    }];
}


#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //如果没登录，则弹出
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
    } else {
        UIImageView *imageHea = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg"]];
        [self.headerView insertSubview:imageHea atIndex:0];
        imageHea.frame = self.headerView.frame;
        [self datas];
    }
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.personalApproved = NO;
    [self configUI];
    [self regeditCell];
}
-(void)setUserInfoAF:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 跳转到个人信息补全页面的方法
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
        
        
        NSDictionary *dic1 =result[@"result"];
        
        ICN_SetUserInfoModel *model1 = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        NSLog(@"%@",model1);
        
        if (model1.memberNick == nil || model1.memberGender == nil|| model1.memberLogo == nil
            || model1.hopePositionName == nil|| model1.workStatus == nil||[model1.workStatus isEqualToString:@"1"]||[model1.workStatus isEqualToString:@"2"]){
            
             if ([model1.workStatus isEqualToString:@"1"]){
                if (model1.companyName == nil||model1.memberPosition == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                }
            }
            /*这是个人*/
             if ([model1.workStatus isEqualToString:@"2"]){
                if (model1.memberSchool ==nil ||model1.memberMajor == nil) {
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                    return ;
                }
            }
            
            
             if(indexPath.section == 0){
                if (indexPath.row == 0) {
                    NSLog(@"点击了我的积分");
                    ICN_creditdetialViewController *credit=[[ICN_creditdetialViewController alloc]init];
                    credit.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:credit animated:YES];
                    return;
                }else{
                    NSLog(@"我的消息");
                    
                    ICN_MessageCenterViewController *vc = [[ICN_MessageCenterViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }
                
            }
            else if(indexPath.section == 1){
                
                
                if (indexPath.row == 0) {
                    
                    ICN_MyResumeViewController *vc = [[ICN_MyResumeViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                    //我的工作
                }else {
                    //投递反馈
                    ICN_DeliveryFeedbackViewController *vc = [[ICN_DeliveryFeedbackViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }
                
            }
            else if (indexPath.section == 2) {
                
                if (indexPath.row == 0) {
                    //我的订单
                    ICN_MyOrderingViewController *vc = [[ICN_MyOrderingViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    vc.memId = model.memberId;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                    
                }else if (indexPath.row == 1){
                    //我的关注
                    ICN_MyConcerningViewController *vc = [[ICN_MyConcerningViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }else if (indexPath.row == 2) {
                    //我的收藏
                    ICN_MyCollectingViewController * vc = [[ICN_MyCollectingViewController alloc]init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }else if(indexPath.row == 3) {
                    //我发布的
                    ICN_MyReleasedViewController * vc =[[ICN_MyReleasedViewController alloc]init];
                    vc.hidesBottomBarWhenPushed = YES;
                    vc.memberId = self.memberID;
                    vc.currentSelectedTag = 1002331;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                    
                }else if (indexPath.row ==4) {
                    //我的直播
                    ICN_MyLiveViewController *vc = [[ICN_MyLiveViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    vc.plusv= self.plusv;
                    [self.navigationController pushViewController:vc animated:YES];
                    return;
                }
            }
            ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        else if(indexPath.section == 0){
            if (indexPath.row == 0) {
                NSLog(@"点击了我的积分");
                ICN_creditdetialViewController *credit=[[ICN_creditdetialViewController alloc]init];
                credit.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:credit animated:YES];
            }else{
                NSLog(@"我的消息");
                
                ICN_MessageCenterViewController *vc = [[ICN_MessageCenterViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
        else if(indexPath.section == 1){
            
            
            if (indexPath.row == 0) {
                
                ICN_MyResumeViewController *vc = [[ICN_MyResumeViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
                //我的工作
            }else {
                //投递反馈
                ICN_DeliveryFeedbackViewController *vc = [[ICN_DeliveryFeedbackViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            
        }
        else if (indexPath.section == 2) {
            
            if (indexPath.row == 0) {
                //我的订单
                ICN_MyOrderingViewController *vc = [[ICN_MyOrderingViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.memId = model.memberId;
                [self.navigationController pushViewController:vc animated:YES];
                
                
            }else if (indexPath.row == 1){
                //我的关注
                ICN_MyConcerningViewController *vc = [[ICN_MyConcerningViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                
                [self.navigationController pushViewController:vc animated:YES];
                
            }else if (indexPath.row == 2) {
                //我的收藏
                ICN_MyCollectingViewController * vc = [[ICN_MyCollectingViewController alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            }else if(indexPath.row == 3) {
                //我发布的
                ICN_MyReleasedViewController * vc =[[ICN_MyReleasedViewController alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.memberId = self.memberID;
                vc.currentSelectedTag = 1002331;
                [self.navigationController pushViewController:vc animated:YES];
                
                
            }else if (indexPath.row ==4) {
                //我的直播
                ICN_MyLiveViewController *vc = [[ICN_MyLiveViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.plusv= self.plusv;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        // 信息校验失败则设置为游客登录
        
    }];
}
#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return 2;
    }
    else if(section == 1) {
        return 2;
    }
    else if(section == 2){
        return 5;
    }else{
        return 1;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /*去掉系统线 辛*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    NormalTableViewCell *cell = [self.tableView
                                 dequeueReusableCellWithIdentifier:@"NormalTableViewCell"
                                 forIndexPath:indexPath];
    NSArray *sectionOneArr = @[@"我的积分",@"我的消息"];
    NSArray *sectionTwoArr = @[@"我的简历",@"投递反馈"];
    NSArray *sectionThreeeArr = @[@"我的订单",@"我的关注",@"我的收藏",@"我发布的", @"我的直播"];

    if (indexPath.section == 0) {
        cell.leftLabel.text = sectionOneArr[indexPath.row];
        if (indexPath.row == 0) {
            cell.leftImage.image = [UIImage imageNamed:@"我的积分"];
            /*展示积分数量 辛*/
            if ([self.scoreStr isEqualToString:@""]) {
                
                cell.integralLabel.hidden = NO;
                cell.integralLabel.text = self.scoreStr;
            }else{
               
                cell.integralLabel.hidden = NO;
                cell.integralLabel.text = self.scoreStr;
            }
        }else if (indexPath.row == 1) {
            if ([self.messageType isEqualToString:@"0"]) {
                cell.pointimageView.hidden = YES;
            }else if([self.messageType isEqualToString:@"1"]){
                cell.pointimageView.hidden = NO;
            }
            cell.leftImage.image = [UIImage imageNamed:@"我的消息"];
        }
    }
    else if(indexPath.section == 1){
        cell.leftLabel.text = sectionTwoArr[indexPath.row];
        if (indexPath.row == 0) {
            cell.leftImage.image = [UIImage imageNamed:@"我的工作-"];
            
            
        }else if (indexPath.row == 1) {
            
            cell.leftImage.image = [UIImage imageNamed:@"投递反馈-"];
        }
    }
    else if(indexPath.section == 2) {
        cell.leftLabel.text = sectionThreeeArr[indexPath.row];
        if (indexPath.row == 0) {
            cell.leftImage.image = [UIImage imageNamed:@"我的订单-"];
        }else if (indexPath.row == 1) {
            
            cell.leftImage.image = [UIImage imageNamed:@"我的关注-"];
        }else if (indexPath.row == 2) {
            
            
            cell.leftImage.image = [UIImage imageNamed:@"我的收藏-"];
            
        }else if (indexPath.row == 3) {
            
            cell.leftImage.image = [UIImage imageNamed:@"我的动态-"];
        }else {
            cell.leftImage.image = [UIImage imageNamed:@"我的直播-"];
        }
    }
    else {
        cell.leftLabel.text = @"设置";
        cell.leftImage.image = [UIImage imageNamed:@"设置-"];
    }
    return cell;
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    
    if(indexPath.section != 3) {
        [self setUserInfoAF:(NSIndexPath *)indexPath];
    }
    else{
        //设置
        ICN_SettingViewController *vc = [[ICN_SettingViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)configUI {
    self.hiddenDefaultNavBar = YES;
    
    _tableView=[[UITableView alloc]init];
    _tableView.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 49);
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [self.view addSubview:_tableView];
    
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = RGB(235, 236, 237);
} 

- (void)regeditCell {
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"UBI_MyOrderTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"UBI_MyOrderTableViewCell"];
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"NormalTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"NormalTableViewCell"];
}

@end
