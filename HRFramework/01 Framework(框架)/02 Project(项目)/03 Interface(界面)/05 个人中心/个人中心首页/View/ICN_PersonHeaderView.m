//
//  PersonHeaderView.m
//  UbiTalk
//
//  Created by Lym on 2016/11/23.
//  Copyright © 2016年 Risenb. All rights reserved.
//
#import "ICN_PersonHeaderView.h"
#import "ICN_MyPersonalHeaderModel.h"
#import "AlivcLiveViewController.h"
#import "UIView+Responder.h"
@implementation ICN_PersonHeaderView
- (void)awakeFromNib {
    [super awakeFromNib];
    self.memberLogo.layer.cornerRadius = self.memberLogo.frame.size.width * 0.5;
    self.memberLogo.layer.masksToBounds = YES;
    
}
NSString *pushUrl = @"rtmp://video-center.alivecdn.com/ixtest/liveTest?vhost=live.1ican.com";
#pragma mark 开始直播
- (IBAction)liveViewAction:(UIButton *)sender {
    if (![pushUrl containsString:@"rtmp://"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"推流地址格式错误，无法直播" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    AlivcLiveViewController *live = [[AlivcLiveViewController alloc] initWithNibName:@"AlivcLiveViewController" bundle:nil url:pushUrl];
    live.isScreenHorizontal = NO;
    live.hidesBottomBarWhenPushed = YES;
    [self.viewController.navigationController pushViewController:live animated:YES];
}
#pragma mark 进入直播间
- (IBAction)playerVideoAction:(UIButton *)sender {
    [self.viewController.navigationController pushViewController:[[NSClassFromString(@"VideoListViewController") alloc] init] animated:YES];
}
- (void)setModel:(ICN_MyPersonalHeaderModel *)model{

    [self.memberLogo sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    self.memberNick.text = model.memberNick;
    
    if ([model.memberGender isEqualToString:@"1"]) {
        self.memberGender.image = [UIImage imageNamed:@"性别男"];
        
    }else if ([model.memberGender isEqualToString:@"2"]) {
    
        self.memberGender.image = [UIImage imageNamed:@"性别女"];
    }
    
    
    if ([model.isAuthStatus isEqualToString:@"0"]) {
        [self.CertificationButton setImage:[UIImage imageNamed:@"未认证"] forState:UIControlStateNormal];
    }else if ([model.isAuthStatus isEqualToString:@"1"]) {
    
        [self.CertificationButton setImage:[UIImage imageNamed:@"已认证"] forState:UIControlStateNormal];
    }
}
@end
