//
//  ICN_CreatCodeViewController.m
//  ICan
//
//  Created by shilei on 17/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CreatCodeViewController.h"
#import "HMScannerController.h"
#import "ICN_MyPersonalHeaderModel.h"

@interface ICN_CreatCodeViewController (){
    ICN_MyPersonalHeaderModel *model;
}

@property(nonatomic,strong)UIImage *personIma;
@property(nonatomic,strong)NSData *data;
@property (weak, nonatomic) IBOutlet UIImageView *personlogo;

@end

@implementation ICN_CreatCodeViewController


#pragma mark - --- 网络请求 ---

-(void)httpNetworing{
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];

    
    [[HRRequest manager]POST:PATH_getIndexInfo para:dic success:^(id data) {
        NSLog(@"%@",data);
        model = [[ICN_MyPersonalHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:data];
        [self.personlogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
        NSString *cardName = SF(@"ixing%@",model.memberId);
        UIImage *avatar = self.personlogo.image;
        
        [HMScannerController cardImageWithCardName:cardName avatar:avatar scale:0.2 completion:^(UIImage *image) {
            
            self.personlogo.hidden = YES;
            self.codeImageView.image = image;
        }];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息获取失败"];
    }];
    
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getIndexInfo" params:dic success:^(id result) {
//            
//        NSDictionary *dict = result[@"result"]
//        ;
//       
//         model = [[ICN_MyPersonalHeaderModel alloc]init];
//        [model setValuesForKeysWithDictionary:dict];
//        
//        [self.personlogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
//        NSString *cardName = model.memberId;
//        UIImage *avatar = self.personlogo.image;
//        
//        [HMScannerController cardImageWithCardName:cardName avatar:avatar scale:0.2 completion:^(UIImage *image) {
//            
//            self.personlogo.hidden = YES;
//            self.codeImageView.image = image;
//        }];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
//
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
   
    [self httpNetworing];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}


#pragma mark - --- IBActiond ---

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


@end
