//
//  ICN_MyCertificationViewController.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCertificationViewController.h"
#import "STPhotoKitController.h"
#import "UIImagePickerController+ST.h"
#import "ICN_HeadImageSelView.h"
#import "HRNetworkingManager+ICN_Publication.h"
#import "HRNetworkingManager+ICN_Publication.h"
#import "NSString+BlankString.h"/*辛*/
#import "UIImage+scale.h"
@interface ICN_MyCertificationViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, STPhotoKitDelegate>

{
    UIView *grayView;
}


@property (weak, nonatomic) IBOutlet UIButton *imageButton; // 上传职业身份按钮
@property (weak, nonatomic) IBOutlet UIButton *skillImageButton; // 上传专业证件
@property (nonatomic , assign)NSInteger selectIndex; // 修改的是上面按钮的图片还是下面按钮的 0上面 1下面

/**
 填写真实姓名 辛
 */
@property (weak, nonatomic) IBOutlet UITextField *nameTF;

@property (strong, nonatomic) ICN_HeadImageSelView *changeHeadImageView;
@property (nonatomic, strong) NSMutableDictionary *dict;

@property (nonatomic, strong) UIImage *image; // 职业身份图片
@property (nonatomic , strong) UIImage *skillImage; // 专业证件图片
@end

@implementation ICN_MyCertificationViewController

#pragma mark - ---------- 懒加载 ----------
- (ICN_HeadImageSelView *)changeHeadImageView {
    if (!_changeHeadImageView)
    {
        _changeHeadImageView = [[NSBundle mainBundle] loadNibNamed:@"ICN_HeadImageSelView" owner:self options:nil].lastObject;
    }
    return _changeHeadImageView;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectIndex = -1;
    self.naviTitle = @"牛人认证";

}
- (IBAction)certificationPic:(UIButton *)sender {
    if ([sender isEqual:self.imageButton]) {
        self.selectIndex = 0;
    }else{
        self.selectIndex = 1;
    }
    [self editImageSelectedWithSender:sender];
}

#pragma mark - ---------- IBActions ----------
#pragma mark 提交按钮 辛
- (IBAction)SubmitClick:(UIButton *)sender {
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    if (self.image == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"缺少身份证"];
        return ;
    }
    if (self.skillImage == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"缺少专业证件"];
        return ;
    }
    if ([self.nameTF.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请写您的真实姓名"];
        return ;
    }
    
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"图片上传中"];
    [HRNetworkingManager uploadImagesWithImageArr:@[self.image , self.skillImage] Success:^(ICN_CommonImagesLoadModel *model) {
        NSLog(@"%@",model.result);
        ///
        NSString *photeStr = model.result[@"fileName"];
        [self.dict setObject:photeStr forKey:@"memberLogo"];
        
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_AddApprove params:@{@"token":token,@"pic":photeStr} success:^(id result) {
            NSLog(@"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLL%@", result);
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"上传成功"];
            // 设置一次审核之后不能再提交了
            [USERDEFAULT setValue:@"1" forKey:@"niu"];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        } failure:^(NSDictionary *errorInfo) {
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"上传失败"];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        
    } Failure:^(NSDictionary *errorInfo) {
        
    }];
}
#pragma mark - --- delegate 视图委托 ---

#pragma mark - 1.STPhotoKitDelegate的委托

- (void)photoKitController:(STPhotoKitController *)photoKitController resultImage:(UIImage *)resultImage
{
    // [self.imageButton setImage:resultImage forState:UIControlStateNormal];
    if (self.selectIndex == 0) {
//         [self.imageButton setBackgroundImage:resultImage forState:UIControlStateNormal];
        [resultImage scaleImage:resultImage toScale:2.0];
        [self.imageButton setImage:resultImage forState:UIControlStateNormal];
        self.image = resultImage;
    }else if(self.selectIndex == 1){
        [self.skillImageButton setImage:resultImage forState:UIControlStateNormal];
//        [self.skillImageButton setBackgroundImage:resultImage forState:UIControlStateNormal];
        self.skillImage = resultImage;
    }
    
    
}

#pragma mark - 2.UIImagePickerController的委托

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *imageOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
        STPhotoKitController *photoVC = [STPhotoKitController new];
        [photoVC setDelegate:self];
        [photoVC setImageOriginal:imageOriginal];
        
        [photoVC setSizeClip:CGSizeMake(self.imageButton.frame.size.width, self.imageButton.frame.size.width)];
        
        
        [self presentViewController:photoVC animated:YES completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark - --- event response 事件相应 ---
- (void)editImageSelectedWithSender:(UIButton *)sender
{

    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
    
    [grayView addSubview:self.changeHeadImageView];
    [self.changeHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 160));
    }];
    self.changeHeadImageView.layer.cornerRadius = 8;
    [self.changeHeadImageView.takePhotoButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        [grayView removeFromSuperview];
        if ([controller isAvailableCamera] && [controller isSupportTakingPhotos]) {
            [controller setDelegate:self];
            [self presentViewController:controller animated:YES completion:nil];
        }else {
            NSLog(@"%s %@", __FUNCTION__, @"相机权限受限");
        }
    }];
    [self.changeHeadImageView.localAlbumButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [controller setDelegate:self];
        [grayView removeFromSuperview];
        if ([controller isAvailablePhotoLibrary]) {
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];
    [self.changeHeadImageView.closeViewButton addAction:^(NSInteger tag) {
        [self dismissContactView];
    }];
}

- (void)dismissContactView {
    [grayView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
