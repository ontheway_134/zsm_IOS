//
//  UIImage+scale.h
//  ICan
//
//  Created by 辛忠志 on 2017/4/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (scale)
//-(UIImage *)TransformtoSize:(CGSize)Newsize;
- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize;
@end
