//
//  UIImage+scale.m
//  ICan
//
//  Created by 辛忠志 on 2017/4/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "UIImage+scale.h"
#import <UIKit/UIKit.h>
@implementation UIImage (scale)

- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize

{
    
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
                                [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
                                UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
                                UIGraphicsEndImageContext();
                                
                                return scaledImage;
                                
                                }
@end
