//
//  ICN_DeliveFeedbackTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_DeliveryAndFeedBackModel;
@class ICN_DeliveAndFeedBackReadModel;
@class ICN_DeliveAndFeedBackAppropriateModel;
@interface ICN_DeliveFeedbackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headerImage;
@property (weak, nonatomic) IBOutlet UILabel *positionLab;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *salary;
@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *workType;
@property (weak, nonatomic) IBOutlet UILabel *companyIndustry;
@property (nonatomic, strong) ICN_DeliveryAndFeedBackModel *model;
@property (weak, nonatomic) IBOutlet UILabel *Fitted;

@property (nonatomic, strong) ICN_DeliveAndFeedBackReadModel *modelRead;
@property (nonatomic, strong)ICN_DeliveAndFeedBackAppropriateModel *modelApp;
@end
