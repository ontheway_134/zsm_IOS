
//
//  ICN_DeliveryAndFeedBackModel.m
//  ICan
//
//  Created by apple on 2016/12/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DeliveryAndFeedBackModel.h"

@implementation ICN_DeliveryAndFeedBackModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"baseId" : @"id"}];
}

@end
