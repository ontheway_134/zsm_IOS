//
//  ICN_DeliveryAndFeedBackViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DeliveryAndFeedBackViewController.h"
#import "ICN_DeliveFeedbackTableViewCell.h"
#import "ICN_DeliveryAndFeedBackModel.h"
#import "ICN_PositionNextOneViewController.h"
#import "ICN_MyCollcetPositionModel.h"



@interface ICN_DeliveryAndFeedBackViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@end

@implementation ICN_DeliveryAndFeedBackViewController


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.hidden = NO;
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
    //申请记录
    
    // Do any additional setup after loading the view.
}
- (void)configData{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
    [[HRRequest manager]POST:PATH_resumeDeliveryList para:dic success:^(id data) {
        NSLog(@"%@",data);
        _dataArr = [[NSMutableArray alloc]init];
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_DeliveryAndFeedBackModel *model = [[ICN_DeliveryAndFeedBackModel alloc]initWithDictionary:obj error:nil];
            [_dataArr addObject:model];
        }];
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无数据"];
        self.tableView.hidden = YES;
        self.noDataImage.hidden = NO;
        self.noDataLabel.hidden = NO;
        [self.tableView.mj_header endRefreshing];
    }];
    
    
    
    //    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberResume/resumeDeliveryList" params:dic success:^(id result) {
    //        NSLog(@"QQQQQQQQQQQQQQQQQQQQQQQQQQQ%@", result);
    //        NSArray *dic1 =result[@"result"];
    //        NSLog(@"%@",dic1);
    //        _dataArr = [[NSMutableArray alloc]init];
    //
    //        for (NSDictionary *dic in dic1 ) {
    //            ICN_DeliveryAndFeedBackModel *model = [[ICN_DeliveryAndFeedBackModel alloc]initWithDictionary:dic error:nil];
    //
    //            [_dataArr addObject:model];
    //        }
    //
    //        [_tableView reloadData];
    //        [self.tableView.mj_header endRefreshing];
    //
    //
    //
    //    } failure:^(NSDictionary *errorInfo) {
    //
    //    }];
    
    
    
    
}


- (void)zj_viewDidLoadForIndex:(NSInteger)index {
    //    NSLog(@"%@",self.view);
    //    NSLog(@"%@", self.zj_scrollViewController);
    
    
}
- (void)zj_viewWillAppearForIndex:(NSInteger)index {
    // NSLog(@"viewWillAppear------");
    
}


- (void)zj_viewDidAppearForIndex:(NSInteger)index {
    // NSLog(@"viewDidAppear-----");
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DeliveFeedbackTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_DeliveFeedbackTableViewCell" forIndexPath:indexPath];
    ICN_DeliveryAndFeedBackModel *model = self.dataArr[indexPath.row];
    self.tableView.contentSize = CGSizeMake(self.tableView.frame.size.width,self.dataArr.count*80+64);
    cell.model = model;
    
    return cell;
    
    
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
    icn.hidesBottomBarWhenPushed = YES;
    ICN_DeliveryAndFeedBackModel *model = self.dataArr[indexPath.row];
    icn.url = model.positionId;
    NSLog(@"YYYYYYYYYYYYYYYYYYYYYYYYYYYY%@", icn.url);
    [self.navigationController pushViewController:icn animated:YES];
    
    
}


#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        ICN_DeliveryAndFeedBackModel *model = self.dataArr[indexPath.row];
        dic[@"id"] = model.baseId;
        
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_doDeleteResume params:dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell
    }
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return @"删除";
}




- (void)creationView{
    
    self.navigationItem.title = @"投递简历";
        
}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_DeliveFeedbackTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_DeliveFeedbackTableViewCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMJRefresh {
    //下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    [self.tableView.mj_header beginRefreshing];}
@end
