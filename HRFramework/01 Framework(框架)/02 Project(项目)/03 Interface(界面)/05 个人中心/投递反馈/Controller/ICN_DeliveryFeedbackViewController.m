//
//  ICN_DeliveryFeedbackViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DeliveryFeedbackViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_DeliveryAndFeedBackViewController.h"
#import "ICN_DeliveryAndFeedBackReadViewController.h"
#import "ICN_DeliveryAndFeedBackAppropriateViewController.h"
@interface ICN_DeliveryFeedbackViewController ()
<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController *> *childVcs;


@end

@implementation ICN_DeliveryFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"投递反馈";
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    //显示滚动条
    style.showLine = YES;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    style.scrollTitle = YES;
    style.autoAdjustTitlesWidth = YES;
    style.adjustCoverOrLineWidth = YES;
    style.selectedTitleColor = RGB0X(0X009dff);
    style.scrollLineColor = RGB0X(0X009dff);
    style.segmentHeight = 40;
    self.titles = @[@"申请记录",
                    @"被查看",
                    @"合适",
                    ];
    
    self.childVcs = [self setupChildVc];
    // 初始化
    ZJScrollPageView *scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.0) segmentStyle:style titles:self.titles parentViewController:self delegate:self];
    [scrollPageView setSelectedIndex:self.index animated:YES];
    scrollPageView.contentView.scrollView.scrollEnabled = NO;
    [self.view addSubview:scrollPageView];
    
}




#pragma mark ----------------ZJScrollPageViewDelegate---------------------
- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}
- (NSArray *)setupChildVc {
    
    ICN_DeliveryAndFeedBackViewController *vc1 = [ICN_DeliveryAndFeedBackViewController new];
    
    ICN_DeliveryAndFeedBackReadViewController *vc2 = [ICN_DeliveryAndFeedBackReadViewController new];
    ICN_DeliveryAndFeedBackAppropriateViewController *vc3 = [ICN_DeliveryAndFeedBackAppropriateViewController new];
    
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2, vc3, nil];
    return childVcs;
}


- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        childVc = self.childVcs[index];
    }
    
    
    
    
    return childVc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
