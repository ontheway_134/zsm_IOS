//
//  ICN_PrivacySetModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_PrivacySetModel : BaseOptionalModel

@property (strong,nonatomic)NSString * isopen;
@property (strong,nonatomic)NSString * justLookFriend;
@property (strong,nonatomic)NSString * passId;
@property (strong,nonatomic)NSString * blackId;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * unlookId;
@property (strong,nonatomic)NSString * justFriend;
@end
