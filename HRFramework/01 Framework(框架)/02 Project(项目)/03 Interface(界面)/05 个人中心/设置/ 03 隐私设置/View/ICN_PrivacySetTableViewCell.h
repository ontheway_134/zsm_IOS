//
//  ICN_PrivacySetTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ReturnBlackBlock)();
typedef void (^ReturnOwnerBlock)();
typedef void (^ReturnIsOpenBlock)(UIButton * btn , UIButton * btnT);
typedef void (^ReturnJustFriendBlock)(UIButton * btn , UIButton * btnT);
typedef void (^ReturnJustLookFriendBlock)(UIButton * btn);

@interface ICN_PrivacySetTableViewCell : UITableViewCell
/*isopen 判断*/
@property (weak, nonatomic) IBOutlet UIButton *isOpenBtn;
/*justFriend*/
@property (weak, nonatomic) IBOutlet UIButton *justFriendBtn;
/*justLookFriend 看好友动态*/
@property (weak, nonatomic) IBOutlet UIButton *justLookFriendBtn;
/*近看好友动态 高度*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *justLookFriendHeight;
/*仅好友可见*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *justFriendHeight;
/*上面的高度*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headHeight;

@property (weak, nonatomic) IBOutlet UIView *justFriendView;

@property (weak, nonatomic) IBOutlet UIView *justLookFriendView;


-(void)ReturnOwnerBlock:(ReturnOwnerBlock)block;
@property (strong,nonatomic)ReturnOwnerBlock ReturnOwnerBlock;

-(void)returnBlackBlock:(ReturnBlackBlock)block;
@property (strong,nonatomic)ReturnBlackBlock returnBlackBlock;

-(void)returnIsOpenBlock:(ReturnIsOpenBlock)block;
@property (strong,nonatomic)ReturnIsOpenBlock returnIsOpenBlock;


-(void)returnJustFriendBlock:(ReturnJustFriendBlock)block;
@property (strong,nonatomic)ReturnJustFriendBlock returnJustFriendBlock;

-(void)returnJustLookFriendBlock:(ReturnJustLookFriendBlock)block;
@property (strong,nonatomic)ReturnJustLookFriendBlock returnJustLookFriendBlock;
@end
