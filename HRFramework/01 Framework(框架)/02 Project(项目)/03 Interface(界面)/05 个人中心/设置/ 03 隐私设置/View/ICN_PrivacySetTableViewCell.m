//
//  ICN_PrivacySetTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PrivacySetTableViewCell.h"

@implementation ICN_PrivacySetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)swichBtnClick:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    /*仅自己可见*/
    if (sender.tag == 100) {
        if (self.returnIsOpenBlock) {
            self.returnIsOpenBlock(self.isOpenBtn,self.justFriendBtn);
        }
    }
    /*仅好友可见*/
    else if (sender.tag==101){
        if (self.returnJustFriendBlock) {
            self.returnJustFriendBlock(self.justFriendBtn,self.isOpenBtn);
        }
    }
    /*仅好友动态*/
    else if (sender.tag==102){
        if (self.returnJustLookFriendBlock) {
            self.returnJustLookFriendBlock(self.justLookFriendBtn);
        }
    }else{
        
    }
}
#pragma mark 仅自己可见
- (IBAction)ownerTap:(UITapGestureRecognizer *)sender {
    if (self.ReturnOwnerBlock) {
        self.ReturnOwnerBlock(sender);
    }
}
#pragma mark 黑名单
- (IBAction)blackTap:(UITapGestureRecognizer *)sender {
    if (self.returnBlackBlock) {
        self.returnBlackBlock(sender);
    }
}
-(void)ReturnOwnerBlock:(ReturnOwnerBlock)block{
    self.ReturnOwnerBlock = block;
}
-(void)returnBlackBlock:(ReturnBlackBlock)block{
    self.returnBlackBlock = block;
}
-(void)returnIsOpenBlock:(ReturnIsOpenBlock)block{
    self.returnIsOpenBlock = block;
}
-(void)returnJustFriendBlock:(ReturnJustFriendBlock)block{
    self.returnJustFriendBlock = block;
}
-(void)returnJustLookFriendBlock:(ReturnJustLookFriendBlock)block{
    self.returnJustLookFriendBlock = block;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
