//
//  ICN_PrivacySetViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_PrivacySetViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSString * isOpenStr;/*判断是不是自己可见状态*/
@property (strong,nonatomic)NSString * isFriendStr;/*判断是不是自己可见状态*/
@property (strong,nonatomic)NSString * isJustLookFriendStr;/*判断是不是自己可见状态*/
@property (strong,nonatomic)NSString * roleId;
@end
