//
//  ICN_PrivacySetViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PrivacySetViewController.h"
#import "ICN_PrivacySetTableViewCell.h"/*隐私设置*/
#import "ICN_OwnerViewController.h"/*谁不可以看*/
#import "ICN_BlackViewController.h"/*黑名单*/
#import "ICN_PrivacySetModel.h"/*隐私设置model*/
@interface ICN_PrivacySetViewController ()
@property(strong,nonatomic)ICN_PrivacySetTableViewCell * cell;
@end

@implementation ICN_PrivacySetViewController
#pragma mark - ---------- 懒加载 ----------
#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    /*隐私设置*/
    [self AFSetting];
    
}

#pragma mark - ---------- IBActions ----------
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
-(void)AFSetting{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               };
    [[HRRequest manager]POST:PATH_MyPrivacySettings para:paramDic success:^(id data) {
        NSLog(@"%@",data);
        ICN_PrivacySetModel * model = [[ICN_PrivacySetModel alloc]init];
        [model setValuesForKeysWithDictionary:data];
        NSLog(@"%@",model);
        /*取值来判断当前展示状态*/
        /*isopen 1:公开,2:仅自己可见*/
        if ([model.isopen isEqualToString:@"1"]) {
            self.cell.isOpenBtn.selected = NO;
        }else if ([model.isopen isEqualToString:@"2"]){
            self.cell.isOpenBtn.selected = YES;
        }
        /*justFriend 仅好友可见   1:是   2:否*/
        if ([model.justFriend isEqualToString:@"1"]) {
            self.cell.justFriendBtn.selected = YES;
        }else if ([model.justFriend isEqualToString:@"2"]){
            self.cell.justFriendBtn.selected = NO;
        }
        /*justLookFriend": 只看好友动态  1:是  2:否*/
        if ([model.justLookFriend isEqualToString:@"1"]) {
            self.cell.justLookFriendBtn.selected = YES;
        }else if ([model.justLookFriend isEqualToString:@"2"]){
            self.cell.justLookFriendBtn.selected = NO;
        }
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
    }];
}
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.roleId isEqualToString:@"2"]) {
        return 141;
    }else{
        return  212;
    }
}
#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.cell = XIB(ICN_PrivacySetTableViewCell);
    self.cell.accessoryType = UITableViewCellAccessoryNone;
    
    /*需要判断当前状态是不是企业中心 如果是 不展示 好友可见 仅好友动态*/
    if ([self.roleId isEqualToString:@"2"]) {
        self.cell.headHeight.constant = 46;
        self.cell.justFriendView.hidden = YES;
        self.cell.justLookFriendView.hidden = YES;
        self.cell.justFriendHeight.constant = 0;
        self.cell.justLookFriendHeight.constant = 0;
        
    }
    /*去掉系统点击阴影*/
    self.cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /*仅自己可见*/
    [self.cell ReturnOwnerBlock:^{
        ICN_OwnerViewController * vc = [[ICN_OwnerViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    /*黑名单*/
    [self.cell returnBlackBlock:^{
        ICN_BlackViewController * vc = [[ICN_BlackViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    /*仅自己可见*/
    [self.cell returnIsOpenBlock:^(UIButton *btn,UIButton* btnT) {
        
        /*在这需要判断仅好友可见是不是开着的 如果是开着的进行网络请求*/
        if (btnT.selected) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请先关闭仅好友可见"];
            btn.selected = NO;
        }else{
            if (btn.selected) {
                /*当状态开着时候是仅好友可见状态 状态为2*/
                self.isOpenStr = @"2";
            }else{
                self.isOpenStr = @"1";
            }
            
            NSDictionary *paramDic = @{@"token":token,
                                       @"status":self.isOpenStr,
                                       };
            [[HRRequest manager]POST:PATH_UserIsopen para:paramDic success:^(id data) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改成功" ];
            } faiulre:^(NSString *errMsg) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改失败" ];
            }];
        }
    }];
    /*仅好友可见*/
    [self.cell returnJustFriendBlock:^(UIButton *btn,UIButton * btnT) {
        
        
        /*在这需要判断仅好友可见是不是开着的 如果是开着的进行网络请求*/
        if (btnT.selected) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请先关闭自己可见"];
            btn.selected = NO;
        }else{
            if (btn.selected) {
                /*当状态开着时候是仅好友可见状态 状态为2*/
                self.isFriendStr = @"1";
            }else{
                self.isFriendStr = @"2";
            }
            NSDictionary *paramDic = @{@"token":token,
                                       @"status":self.isFriendStr,
                                       };
            [[HRRequest manager]POST:PATH_UserjustFriend para:paramDic success:^(id data) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改成功" ];
            } faiulre:^(NSString *errMsg) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改失败" ];
            }];
        }
    }];
    /*仅看好友动态*/
    [self.cell returnJustLookFriendBlock:^(UIButton *btn) {
        if (btn.selected) {
            /*当状态开着时候是仅好友可见状态 状态为2*/
            self.isJustLookFriendStr = @"1";
        }else{
            self.isJustLookFriendStr = @"2";
        }
        NSDictionary *paramDic = @{@"token":token,
                                   @"status":self.isJustLookFriendStr,
                                   };
        [[HRRequest manager]POST:PATH_UserjustLookFriend para:paramDic success:^(id data) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改成功" ];
        } faiulre:^(NSString *errMsg) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"更改失败" ];
        }];
    }];
    return  self.cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
