//
//  ICN_UserProtocolViewController.m
//  ICan
//
//  Created by apple on 2017/1/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserProtocolViewController.h"
#import "ICN_SettingTableViewCell.h"

#import "ICN_AboutUsPersonProtoclViewController.h"
#import "ICN_AboutUsEnterpriseProcolViewController.h"

@interface ICN_UserProtocolViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ICN_UserProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"使用协议";
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self regeditCell];
    // Do any additional setup after loading the view from its nib.
}

//#pragma mark - ---------- Section的数量 ----------
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//#pragma mark - ---------- 每个Section的高度 ----------
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 1;
//}
#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    ICN_SettingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_SettingTableViewCell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.leftLabel.text = @"个人用户协议";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }else {
    
        cell.leftLabel.text = @"企业用户协议";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
        
    }
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        ICN_AboutUsPersonProtoclViewController *vc = [[ICN_AboutUsPersonProtoclViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
    
        ICN_AboutUsEnterpriseProcolViewController *vc = [[ICN_AboutUsEnterpriseProcolViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    
    

}


- (void)regeditCell {
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_SettingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_SettingTableViewCell"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
