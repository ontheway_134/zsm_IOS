//
//  ICN_AboutUsPersonProtoclViewController.m
//  ICan
//
//  Created by apple on 2017/1/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_AboutUsPersonProtoclViewController.h"
#import "ICN_AboutUsNomalTableViewCell.h"
@interface ICN_AboutUsPersonProtoclViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;


@end

@implementation ICN_AboutUsPersonProtoclViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"个人用户协议";
    [self creationView];
    [self creationCell];
   
    

    // Do any additional setup after loading the view.
}
- (void)creationView{
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    
//    UILabel *labelOfAboutUs = [[UILabel alloc] init];
//    labelOfAboutUs.frame = CGRectMake(10, 0, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 64);
//    [self.view addSubview:labelOfAboutUs];
//    self.view.backgroundColor = [UIColor whiteColor];
//    labelOfAboutUs.textColor = RGB(140, 141, 141);
//    labelOfAboutUs.backgroundColor = [UIColor whiteColor];
//    //    labelOfAboutUs.frame = CGRectMake(0, 0, SCREEN_WIDTH, 400);
//    labelOfAboutUs.font = [UIFont systemFontOfSize:13];
//    [labelOfAboutUs setNumberOfLines:0];
//    labelOfAboutUs.text = @"        首先，我们要明确一点，同步和异步都是在线程中使用的。在iOS开发中，比如网络请求数据时，若使实打实费迪南德爱上的话费卡就收到货啊刷卡机发卡上就和刷卡机沙发看机会客户奥斯卡积分撒娇和看撒谎客户卡视角开始计划开始计划卡上框架好卡好卡加咖啡和卡号卡好卡好卡黑科技好卡建行卡按计划开花结实空间和开发计划的空间化考试计划看撒娇和地方开始交电话费看机会撒地方科技萨科技风沙发的离开家撒垃圾拉屎的离开房间杀戮空间的来刷卡缴费爱上了空间发的拉斯卡积分拉斯减肥爱上了咖啡姐了萨科技风老师的风景垃圾开发了;按实际开发了;是会计法律;咖啡豆了;看进来撒;看风景了;沙发接口拉萨复健科拉萨的接口费老师大家看法拉萨的会计法老师大家看法拉萨的会计法拉萨看风景了;圣诞节开发拉萨科技福是豆腐干豆腐告诉对方公司的风格利送。";
//    //初始化段落，设置段落风格
//    
//    
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
//    paragraphStyle.lineSpacing = 10;
//    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont systemFontOfSize:14], NSParagraphStyleAttributeName:paragraphStyle};
//    labelOfAboutUs.attributedText = [[NSAttributedString alloc]initWithString:labelOfAboutUs.text attributes:attributes];
//    
    
    
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    self.tableView.estimatedRowHeight = 2000;
//    self.tableView.rowHeight = self.tableView.estimatedRowHeight;
//    return self.tableView.rowHeight;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 2000;
    return self.tableView.rowHeight;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    ICN_AboutUsNomalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_AboutUsNomalTableViewCell" forIndexPath:indexPath];

    return cell;
    

}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    
    
}


- (void)creationCell{

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_AboutUsNomalTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_AboutUsNomalTableViewCell"];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
