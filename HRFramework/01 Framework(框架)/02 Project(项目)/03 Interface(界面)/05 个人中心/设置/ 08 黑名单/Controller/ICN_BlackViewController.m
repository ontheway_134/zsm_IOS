//
//  ICN_BlackViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BlackViewController.h"
#import "ICN_OwnerTableViewCell.h"
#import "NSString+BlankString.h"
@interface ICN_BlackViewController ()
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (strong,nonatomic) NSMutableArray * arrID;;
@property (strong,nonatomic) NSString *strID;
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;

@end

@implementation ICN_BlackViewController
#pragma mark - ---------- 懒加载 ----------
#pragma mark 存ID
- (NSMutableArray *)arrID{
    if (!_arrID) {
        _arrID = [NSMutableArray array];
    }
    return _arrID;
}
#pragma mark 数据源
- (NSMutableArray *)baseSourceArr{
    if (!_baseSourceArr) {
        _baseSourceArr = [NSMutableArray array];
    }
    return  _baseSourceArr;
}

#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView .hidden = NO;
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self blackNetWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        
        [self blackNetWorkRequest:TYPE_RELOADDATA_UP];
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - ---------- IBActions ----------

#pragma mark 完成按钮
- (IBAction)finishBtnClick:(UIButton *)sender {
    if ([self.searchTF.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
        return;
    }
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    self.baseSourceArr = nil;
    /*下面是搜索的网络请求*/
    NSDictionary *paramDic = @{@"token":token,
                               @"content":self.searchTF.text,
                               @"status":@"1",
                               };
    [[HRRequest manager]POST:PATH_UserSearch para:paramDic success:^(id data) {
        
        NSLog(@"%@",data);
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_OwnerModel * model = [[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
            [self.baseSourceArr addObject:model];
        }];
        NSLog(@"%@",self.baseSourceArr);
        if (self.baseSourceArr.count == 0) {
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
        }
        [self endRefresh];
        [self.tableView reloadData];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        self.tableView.hidden = YES;
        self.noDataImage.hidden = NO;
        self.noDataLabel.hidden = NO;
        [self endRefresh];
    }];
}
#pragma mark 搜索按钮
- (IBAction)searchTap:(UITapGestureRecognizer *)sender {
    
}

#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 取消黑名单
- (IBAction)cacelBlackBtnClick:(UIButton *)sender {
    /*下面是搜索的网络请求*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (![self.strID isEqualToString:@""]&&!(self.strID == nil)) {
        NSDictionary *paramDic = @{@"blackId":self.strID,
                                   @"token":token,
                                   };
        [[HRRequest manager]POST:PATH_UserCanalBlackList para:paramDic success:^(id data) {
            NSLog(@"成功");
            [self blackNetWorkRequest:TYPE_RELOADDATA_DOWM];
            
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            NSLog(@"失败");
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您还没有选择取消成员"];
    }
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark 黑名单数据网络请求
-(void)blackNetWorkRequest:(BOOL)isMore{

    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_UserGetBlack para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_OwnerModel * model=[[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
                [self.baseSourceArr addObject:model];
            }];
            if (self.baseSourceArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                self.tableView.hidden = YES;
                self.noDataImage.hidden = NO;
                self.noDataLabel.hidden = NO;
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.baseSourceArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_UserGetBlack para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_OwnerModel * model=[[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
                [self.baseSourceArr addObject:model];
            }];
            if (self.baseSourceArr.count == 0) {
                self.tableView.hidden = YES;
                self.noDataImage.hidden = NO;
                self.noDataLabel.hidden = NO;
            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }

}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.baseSourceArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_OwnerTableViewCell * cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_OwnerTableViewCell" owner:nil options:nil].lastObject;
    cell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /*赋值*/
    cell.model = self.baseSourceArr[indexPath.row];
    /*批量操作cell*/
    [cell returnSelectedBlock:^(NSString *modelID, UIButton *btn) {
        if (!btn.selected) {
            [self.arrID removeObject:modelID];
        }else{
            [self.arrID addObject:modelID];
            NSLog(@"%@",self.arrID);
        }
        self.strID = [self.arrID componentsJoinedByString:@","];
        NSLog(@"%@",self.strID);

    }];
    return  cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
