//
//  ICN_OwnerTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_OwnerTableViewCell.h"

@implementation ICN_OwnerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark 选择按钮
- (IBAction)selectedBtnClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.returnSelectedBlock) {
        self.returnSelectedBlock(self.strID,sender);
    }
}
-(void)returnSelectedBlock:(ReturnSelectedBlock)block{
    self.returnSelectedBlock = block;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setModel:(ICN_OwnerModel *)model{
    _model = model;
    self.nickName.text = model.memberNick;
    [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.strID = model.memberId;
}
@end
