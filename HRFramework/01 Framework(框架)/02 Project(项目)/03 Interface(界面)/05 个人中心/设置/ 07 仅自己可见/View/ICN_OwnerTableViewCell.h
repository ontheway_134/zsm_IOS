//
//  ICN_OwnerTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_OwnerModel.h"
typedef void (^ReturnSelectedBlock)(NSString * modelID,UIButton *btn);
@interface ICN_OwnerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UIImageView *url;
@property (strong,nonatomic)NSString * strID;/*谁不可以看的ID*/
@property (strong,nonatomic)ReturnSelectedBlock returnSelectedBlock;
-(void)returnSelectedBlock:(ReturnSelectedBlock)block;
@property (weak, nonatomic) IBOutlet UIButton *Dbtn;/*选中的点点*/
@property (strong,nonatomic)ICN_OwnerModel * model;
@end
