//
//  ICN_OwnerViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_OwnerViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTF;
@property (strong,nonatomic)NSMutableArray * baseSourceArr;/*数据源*/
@property (strong,nonatomic)NSString * generalID;/*通用iD*/
@end
