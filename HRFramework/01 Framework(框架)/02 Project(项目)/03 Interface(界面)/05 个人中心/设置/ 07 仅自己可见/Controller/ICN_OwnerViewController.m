//
//  ICN_OwnerViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_OwnerViewController.h"
#import "ICN_OwnerTableViewCell.h"/*谁不可以看*/
#import "NSString+BlankString.h"
#import "ICN_OwnerModel.h"/**/
@interface ICN_OwnerViewController (){
    /*用来存ID接口获取的数组*/
}
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (strong,nonatomic) NSMutableArray * arrID;
@property (strong,nonatomic) NSString *strID;
@property (weak, nonatomic) IBOutlet UILabel *noReshLabel;/*没数据label*/
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;

@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headViewHeight;/*我的发布 动态 过来的时候需要隐藏上面的view*/
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIView *headView;/*头部放置搜索条的view*/

@end

@implementation ICN_OwnerViewController
#pragma mark - ---------- 懒加载 ----------
#pragma mark 存ID
- (NSMutableArray *)arrID{
    if (!_arrID) {
        _arrID = [NSMutableArray array];
    }
    return _arrID;
}
#pragma mark 数据源
- (NSMutableArray *)baseSourceArr{
    if (!_baseSourceArr) {
        _baseSourceArr = [NSMutableArray array];
    }
    return  _baseSourceArr;
}
#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.hidden = NO;
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        //判断是否是动态传递过来的
        if ([self.generalID isEqualToString:@""]||(self.generalID == nil)) {
            [self userNetWorkRequest:TYPE_RELOADDATA_DOWM];
        }else{
            /*隐藏完成和搜索 还有取消谁不可看的功能*/
            self.cancelBtn.hidden = NO;
            self.finishBtn.hidden = YES;
            self.headView.hidden = YES;
            self.headViewHeight.constant = 0;
            /*进行刷行 我的发布 动态 谁不可看 */
            [self dynamicCannot];
        }
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        //判断是否是动态传递过来的
        if ([self.generalID isEqualToString:@""]||(self.generalID == nil)) {
            [self userNetWorkRequest:TYPE_RELOADDATA_UP];
        }else{
            [self endRefresh];
        }
    }];
    
    //    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    [self.tableView.mj_header beginRefreshing];
}
#pragma mark - ---------- IBActions ----------
#pragma mark 搜索按钮
- (IBAction)searchBtnClick:(UIButton *)sender {
    
}
#pragma mark 取消谁不可看按钮
- (IBAction)cancelBtnClick:(UIButton *)sender {
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if ([self.generalID isEqualToString:@""]||(self.generalID == nil)) {
        /*判断如果没有选中则不能进行完成搜索操作*/
        if (![self.strID isEqualToString:@""]&&!(self.strID == nil)) {
            NSDictionary *paramDic = @{@"unlookId":self.strID,
                                       @"token":token,
                                       };
            [[HRRequest manager]POST:PATH_UserUnlook para:paramDic success:^(id data) {
                NSLog(@"成功");
                [self userNetWorkRequest:TYPE_RELOADDATA_DOWM];
                //        [self.tableView reloadData];
            } faiulre:^(NSString *errMsg) {
                NSLog(@"%@",errMsg);
                NSLog(@"失败");
            }];
        }
        else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您还没有选择取消成员"];
        }
    }
    /*动态过来的*/
    else{
        /*初始化token*/
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        if (![self.strID isEqualToString:@""]&&!(self.strID == nil)) {
            NSDictionary *paramDic = @{@"memberId":self.strID,
                                       @"token":token,
                                       @"matterId":self.generalID
                                       };
            NSLog(@"%@",paramDic);
            
            [[HRRequest manager]POST:PATH_delMatterNotLook para:paramDic success:^(id data) {
                NSLog(@"%@",data);
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消成功"];
                /*在刷新一次*/
                [self dynamicCannot];
            } faiulre:^(NSString *errMsg) {
                NSLog(@"%@",errMsg);
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消失败"];
            }];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择要取消的人"];
        }
        
    }
    
}
#pragma mark 动态谁不可看刷新
-(void)dynamicCannot{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    self.baseSourceArr = nil;
    /*下面是搜索的网络请求*/
    NSDictionary *paramDic = @{@"token":token,
                               @"matterId":self.generalID,
                               };
    [[HRRequest manager]POST:PATH_ReleaseGetMatterBanList para:paramDic success:^(id data) {
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_OwnerModel * model = [[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
            [self.baseSourceArr addObject:model];
        }];
        if (self.baseSourceArr.count == 0) {
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
            self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noReshLabel.hidden = NO;
            return ;
        }else{
            [self endRefresh];
            [self.tableView reloadData];
        }
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [self endRefresh];
        
        self.tableView.hidden = YES;
        self.noDataImage.hidden = NO;
        self.noReshLabel.hidden = NO;
        [self.tableView reloadData];
    }];
    
}
#pragma mark 完成按钮
- (IBAction)finishBtnClick:(UIButton *)sender {
    if ([self.searchTF.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
        return;
    }
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    self.baseSourceArr = nil;
    /*下面是搜索的网络请求*/
    NSDictionary *paramDic = @{@"token":token,
                               @"content":self.searchTF.text,
                               @"status":@"2",
                               };
    [[HRRequest manager]POST:PATH_UserSearch para:paramDic success:^(id data) {
        
        NSLog(@"%@",data);
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_OwnerModel * model = [[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
            [self.baseSourceArr addObject:model];
        }];
        NSLog(@"%@",self.baseSourceArr);
        if (self.baseSourceArr.count == 0) {
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noReshLabel.hidden = NO;
        }
        [self endRefresh];
        [self.tableView reloadData];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        self.tableView.hidden = YES;
        self.noDataImage.hidden = NO;
        self.noReshLabel.hidden = NO;
        [self endRefresh];
    }];
}
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark 谁不可以看列表网络请求
-(void)userNetWorkRequest:(BOOL)isMore{
    
    
    
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_UserGetUnlook para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_OwnerModel * model=[[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
                [self.baseSourceArr addObject:model];
            }];
            if (self.baseSourceArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                self.tableView.hidden = YES;
                self.noDataImage.hidden = NO;
                self.noReshLabel.hidden = NO;
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.baseSourceArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_UserGetUnlook para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_OwnerModel * model=[[ICN_OwnerModel alloc]initWithDictionary:obj error:nil];
                [self.baseSourceArr addObject:model];
            }];
            if (self.baseSourceArr.count == 0) {
                self.tableView.hidden = YES;
                self.noDataImage.hidden = NO;
                self.noReshLabel.hidden = NO;
            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden = YES;
            self.noDataImage.hidden = NO;
            self.noReshLabel.hidden = NO;
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
    
}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
    self.tableView.mj_footer.hidden = YES;
}
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.baseSourceArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_OwnerTableViewCell * cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_OwnerTableViewCell" owner:nil options:nil].lastObject;
    cell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    /*赋值*/
    cell.model = self.baseSourceArr[indexPath.row];
    /*选择按钮*/
    [cell returnSelectedBlock:^(NSString *modelID, UIButton *btn) {
        if (!btn.selected) {
            [self.arrID removeObject:modelID];
        }else{
            [self.arrID addObject:modelID];
            NSLog(@"%@",self.arrID);
        }
        self.strID= [self.arrID componentsJoinedByString:@","];
        NSLog(@"%@",self.strID);
    }];
    return  cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

@end
