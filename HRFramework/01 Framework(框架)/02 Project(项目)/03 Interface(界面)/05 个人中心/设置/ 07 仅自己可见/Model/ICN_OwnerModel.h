//
//  ICN_OwnerModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_OwnerModel : BaseOptionalModel
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * memberId;

@end
