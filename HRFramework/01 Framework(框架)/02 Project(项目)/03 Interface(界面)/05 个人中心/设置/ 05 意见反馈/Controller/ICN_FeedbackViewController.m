//
//  ICN_FeedbackViewController.m
//  ICan
//
//  Created by apple on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//
#define kApplicationShared  [UIApplication sharedApplication]
#import "ICN_FeedbackViewController.h"
#import "ICN_SsettingFeedBackModel.h"
#import "SQJudgeInfomation.h"
@interface ICN_FeedbackViewController ()<UITextViewDelegate>
@property (nonatomic, strong) UITextField *nameTF;
@property (nonatomic, strong) UITextField *phoneNumTF;
@property (nonatomic, strong) UITextView *opinionTV;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UITextField *emailTF;
@property (nonatomic, strong) UITextField *qqTF;
@property (nonatomic, strong) NSMutableDictionary *dic;
@end

@implementation ICN_FeedbackViewController

#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated{

    self.opinionTV.delegate = self;
    self.numberLabel.text = SF(@"%ld/150",self.opinionTV.text.length);
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"意见反馈";
    self.view.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [self creationView];
}
- (void)creationView{

    self.dic = [[NSMutableDictionary alloc] init];
    
    UIView *viewOfHead = [[UIView alloc] init];
    [self.view addSubview:viewOfHead];
    viewOfHead.backgroundColor = [UIColor whiteColor];
    [viewOfHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.offset(280);
        
    }];
    UIImageView *imageViewOfEmail = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"联系姓名"]];
    [viewOfHead addSubview:imageViewOfEmail];
    
    [imageViewOfEmail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead).with.offset(12);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.height.offset(15);
        make.width.equalTo(imageViewOfEmail.mas_height);
    }];
    self.nameTF = [[UITextField alloc] init];
    [viewOfHead addSubview:self.nameTF];
//    self.nameTF.textColor = RGB0X(0xb6b6b6); 
    self.nameTF.placeholder = @"请输入联系姓名";
    self.nameTF.font = [UIFont systemFontOfSize:12];
    [self.nameTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead).with.offset(10);
        make.left.equalTo(imageViewOfEmail.mas_right).with.offset(10);
        make.height.offset(21);
        make.right.equalTo(viewOfHead).with.offset(-20);
        
    }];
    
    
    UILabel *labelOfEmail = [[UILabel alloc] init];
    [viewOfHead addSubview:labelOfEmail];
    labelOfEmail.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelOfEmail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameTF.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
    }];
    
    
    UIImageView *imageViewOfPhone = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"我的账号"]];
    [viewOfHead addSubview:imageViewOfPhone];
    [imageViewOfPhone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfEmail.mas_bottom).with.offset(12);
        make.left.equalTo(viewOfHead).with.offset(12);
        make.height.offset(15);
        make.width.offset(10);
    }];
    self.phoneNumTF = [[UITextField alloc] init];
    [viewOfHead addSubview:self.phoneNumTF];
//    self.phoneNumTF.textColor = RGB0X(0xb6b6b6);
    self.phoneNumTF.placeholder = @"请输入联系手机号";
    self.phoneNumTF.font = [UIFont systemFontOfSize:12];
    [self.phoneNumTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfEmail.mas_bottom).with.offset(10);
        make.left.equalTo(imageViewOfPhone.mas_right).with.offset(10);
        make.height.offset(21);
        make.right.equalTo(viewOfHead).with.offset(-20);
        
    }];
    
    UILabel *labelOfPhone = [[UILabel alloc] init];
    [viewOfHead addSubview:labelOfPhone];
    labelOfPhone.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelOfPhone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneNumTF.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
    }];
    
    UIImageView *imageViewOfE = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"邮箱"]];
    [viewOfHead addSubview:imageViewOfE];

    [imageViewOfE mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfPhone.mas_bottom).with.offset(12);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.height.offset(12);
        make.width.offset(15);
    }];
    
    self.emailTF = [[UITextField alloc] init];
    [viewOfHead addSubview:self.emailTF];
//    self.emailTF.textColor = RGB0X(0xb6b6b6);
    self.emailTF.placeholder = @"请输入联系邮箱";
    self.emailTF.font = [UIFont systemFontOfSize:12];
    [self.emailTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfPhone.mas_bottom).with.offset(10);
        make.left.equalTo(imageViewOfE.mas_right).with.offset(10);
        make.height.offset(21);
        make.right.equalTo(viewOfHead).with.offset(-20);
        
    }];
    
    UILabel *labelemail = [[UILabel alloc] init];
    [viewOfHead addSubview:labelemail];
    labelemail.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelemail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.emailTF.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
    }];
    
    UIImageView *imageViewOfQQ = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"QQ号"]];
    [viewOfHead addSubview:imageViewOfQQ];
    
    [imageViewOfQQ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelemail.mas_bottom).with.offset(12);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.height.offset(15);
        make.width.equalTo(imageViewOfQQ.mas_height);
    }];
    
    self.qqTF = [[UITextField alloc] init];
    [viewOfHead addSubview:self.qqTF];
//    self.qqTF.textColor = RGB0X(0xb6b6b6);
    self.qqTF.placeholder = @"请输入联系QQ号";
    self.qqTF.font = [UIFont systemFontOfSize:12];
    [self.qqTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelemail.mas_bottom).with.offset(10);
        make.left.equalTo(imageViewOfQQ.mas_right).with.offset(10);
        make.height.offset(21);
        make.right.equalTo(viewOfHead).with.offset(-20);
        
    }];
    
    
    UILabel *labelQQ = [[UILabel alloc] init];
    [viewOfHead addSubview:labelQQ];
    labelQQ.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelQQ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.qqTF.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
    }];
    
    
    
    
    
    
//textview runtime 找到光标的属性
    self.opinionTV = [[UITextView alloc] init];
    [viewOfHead addSubview:self.opinionTV];
    self.opinionTV.font = [UIFont systemFontOfSize:13];
    self.opinionTV.layer.borderWidth = 1.0f;
    self.opinionTV.layer.borderColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1].CGColor;
    self.opinionTV.layer.cornerRadius = 5;
    self.opinionTV.layer.masksToBounds = YES;

    self.opinionTV.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.opinionTV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelQQ).with.offset(15);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(-10);
        make.height.offset(90);
    }];
    
    UILabel *placeHolderLabel = [[UILabel alloc] init];
    placeHolderLabel.text = @"请输入您的意见或建议";
    placeHolderLabel.numberOfLines = 0;
    placeHolderLabel.font = [UIFont systemFontOfSize:13];
    
    placeHolderLabel.textColor = RGB0X(0xb6b6b6);    [placeHolderLabel sizeToFit];
    [self.opinionTV addSubview:placeHolderLabel];
    
    [self.opinionTV setValue:placeHolderLabel forKey:@"_placeholderLabel"];
    
    
    self.numberLabel = [[UILabel alloc] init];
    [self.opinionTV addSubview:self.numberLabel];
    self.numberLabel.textColor = RGB0X(0xb6b6b6);
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(viewOfHead).with.offset(-10);
        make.bottom.equalTo(viewOfHead).with.offset(-10);
        make.height.offset(20);
    }];
    
//提交按钮
    UIButton *buttonOfSubmit = [[UIButton alloc] init];
    [self.view addSubview:buttonOfSubmit];
    buttonOfSubmit.backgroundColor = [UIColor colorWithRed:41 / 255.0f green:141 / 255.0f blue:250 / 255.0f alpha:1];
    buttonOfSubmit.layer.cornerRadius = 5;
    buttonOfSubmit.layer.masksToBounds = YES;
    [buttonOfSubmit setTitle:@"提交" forState:UIControlStateNormal];
    buttonOfSubmit.titleLabel.font = [UIFont systemFontOfSize:13];
    [buttonOfSubmit addTarget:self action:@selector(submitButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [buttonOfSubmit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead.mas_bottom).with.offset(30);
        make.left.equalTo(self.view).with.offset(12);
        make.right.equalTo(self.view).with.offset(-12);
        make.height.offset(40);
    }];
    
    
    
    
 
}
//提交按钮
- (void)submitButtonClick{

    
    if (_nameTF.text.length == 0 || _phoneNumTF.text.length == 0 || _opinionTV.text.length == 0 ||
        _emailTF.text.length == 0 || _qqTF.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写完整的信息"];
    } else {
        
        if (![SQJudgeInfomation isMobileNumber:_phoneNumTF.text]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写正确的手机号"];
            return;
        }
        if (![self isValidateEmail:_emailTF.text]) {
            [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写正确的邮箱"];
            
            return;
        }
        if (![self isValidateQQ:_qqTF.text]) {
             [MBProgressHUD windowShowProgressWithBaseView:kApplicationShared.keyWindow Message:@"请填写正确的QQ"];
            return;
        }
        
        
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        [self.dic setObject:token forKey:@"token"];
        [self.dic setObject:_opinionTV.text forKey:@"noteContent"];
        [self.dic setObject:self.nameTF.text forKey:@"memberName"];
        [self.dic setObject:self.phoneNumTF.text forKey:@"memberMobile"];
        [self.dic setObject:self.emailTF.text forKey:@"memberEmail"];
        [self.dic setObject:self.qqTF.text forKey:@"memberQQ"];
        NSLog(@"%@",self.dic);
        
        if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
            // 当前无网络
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
            return ;
        }
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyFeedBack params:self.dic success:^(id result) {
            
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            NSMutableArray<ICN_SsettingFeedBackModel *> *resultArr = [NSMutableArray array];
            if (firstModel.code == 0) {
                NSArray *array = [result valueForKey:@"result"];
                NSLog(@"FFFFFFFFFFFFFFFF%@", array);
                for (NSDictionary *item in array) {
                    ICN_SsettingFeedBackModel *model = [[ICN_SsettingFeedBackModel alloc] initWithDictionary:item error:nil];
                    
                    [resultArr addObject:model];
                }
                // 数据获取完成
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"意见反馈提交成功"];
                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
                
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"意见反馈提交失败"];
                
            }
        } failure:^(NSDictionary *errorInfo) {
            
            
        }];

    }

}

#pragma mark - --- 判断textview字数 ---

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=150)
    {
        return  NO;
    }
    else
    {
        return YES;
    }
}
//textview字数改变 label跟随一起变
- (void)textViewDidChange:(UITextView *)textView
{
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.numberLabel.text=[NSString stringWithFormat:@"%ld/150",existTextNum];
}
//隐藏键盘
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{


    [self.nameTF resignFirstResponder];
    [self.phoneNumTF resignFirstResponder];
    [self.opinionTV resignFirstResponder];
    [self.qqTF resignFirstResponder];
    




}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//手机号码的正则表达式
- (BOOL)isValidateMobile:(NSString *)mobile{
    //手机号以13、15、18开头，八个\d数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

//邮箱地址的正则表达式
- (BOOL)isValidateEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//QQ的正则表达式
- (BOOL)isValidateQQ:(NSString *)qq{
    NSString *qqRegex = @"[1-9][0-9]{4,14}";
    NSPredicate *qqTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", qqRegex];
    return [qqTest evaluateWithObject:qq];
}

@end
