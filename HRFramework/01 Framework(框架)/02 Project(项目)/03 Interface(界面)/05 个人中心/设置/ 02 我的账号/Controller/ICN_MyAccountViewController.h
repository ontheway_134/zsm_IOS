//
//  ICN_MyAccountViewController.h
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_MySettingMyAccountModel;
@interface ICN_MyAccountViewController : BaseViewController

@property (nonatomic , copy)NSString *token;

@property (weak, nonatomic) IBOutlet UITextField *phoneNumTF;

@property (weak, nonatomic) IBOutlet UIButton *verificationCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTF;

@property (nonatomic, strong) ICN_MySettingMyAccountModel *model;


@end
