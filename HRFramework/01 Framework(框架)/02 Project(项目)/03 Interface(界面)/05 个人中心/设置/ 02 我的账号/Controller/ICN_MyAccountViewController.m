//
//  ICN_MyAccountViewController.m
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyAccountViewController.h"
#import "ICN_MySettingMyAccountModel.h"
#import "YHCountDownButton.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"

@interface ICN_MyAccountViewController ()
@property (nonatomic, strong) NSMutableDictionary *dic;


@property (nonatomic,assign) BOOL triplicitiesToken;
@end

@implementation ICN_MyAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.token == nil) {
        self.token = @" ";
    }
    [self creationView];
    
    self.triplicitiesToken = NO;
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置返回按钮的状态
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
    [btn addTarget:self action:@selector(barBackAction) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = leftItem3;
}

- (void)barBackAction{
    if ([self.navigationController.childViewControllers.firstObject isEqual:self]) {
        // 没有默认的导航栏
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)creationView{

    self.navigationItem.title = @"我的账号";
    self.confirmBtn.layer.cornerRadius = 5;
    self.confirmBtn.layer.masksToBounds = YES;
    self.verificationCodeBtn.layer.borderColor = RGB0X(0x009dff).CGColor;
    self.verificationCodeBtn.layer.borderWidth = 1.0f;
    self.verificationCodeBtn.layer.cornerRadius = 4;
    self.verificationCodeBtn.layer.masksToBounds = YES;
    self.dic = [[NSMutableDictionary alloc] init];
    
    
    

}
//获取验证码
- (IBAction)CodeBtnActions:(YHCountDownButton *)sender {
    [self.view endEditing:YES];
    YHCountDownButton *sender_btn = sender;
    
    if ([self.phoneNumTF.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
        return;
    }
    else{
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CODE params:@{@"mobile":self.phoneNumTF.text} success:^(id result) {
            BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (codemodel.code == 0) {
                sender_btn.enabled = NO;
                [sender_btn startWithSecond:CODE_S];
                [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                    NSString *title = [NSString stringWithFormat:@"%.2d秒",second];
                    return title;
                }];
                [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                    countDownButton.enabled = YES;
                    return @"重新获取验";
                }];
            }
            else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }
}

//收回键盘
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [self.phoneNumTF resignFirstResponder];
    [self.verificationCodeTF resignFirstResponder];


}
- (IBAction)SubmitButton:(UIButton *)sender {
    
    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    
    if (self.token == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"用户账号异常"];
        return ;
    }else{
        if ([self.token isEqualToString:@" "]) {
            // 证明不是从欢迎页面跳转的
            self.token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            if (self.token == nil) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"用户账号异常"];
            }
        }
    }
    [self.dic setObject:self.token forKey:@"token"];
    [self.dic setObject:self.phoneNumTF.text forKey:@"mobile"];
    NSLog(@"AAAAAAAAAAAAAAAAAAAAAAAAA%@", self.dic);
    self.dic[@"verify"] = _verificationCodeTF.text;
    
    if (_phoneNumTF.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写手机号"];
        return;
    }
    
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyPhoneBinding params:self.dic success:^(id result) {
    
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
        NSString *tempStr = SF(@"%@",result[@"code"]);
        
        if ([tempStr isEqualToString:@"0"]) {
            
            
            
            self.triplicitiesToken = YES;
            
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
                
                

                NSDictionary *dic1 =result[@"result"];
                
                ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                NSLog(@"%@",model);
                if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                    || model.hopePositionName == nil|| model.workStatus == nil) {
                    
                    ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                    vc.isFirst = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];

            
   
            
            
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if (!self.triplicitiesToken) {
         [[NSUserDefaults standardUserDefaults] setValue:nil forKey:HR_CurrentUserToken];
    }
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
