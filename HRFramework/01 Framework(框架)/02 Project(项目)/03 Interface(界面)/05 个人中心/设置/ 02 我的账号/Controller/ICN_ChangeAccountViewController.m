//
//  ICN_ChangeAccountViewController.m
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ChangeAccountViewController.h"
#import "ICN_MySettingMyAccountModel.h"
#import "YHCountDownButton.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
#import "ICN_StartViewController.h" // 引导页

@interface ICN_ChangeAccountViewController ()
@property (weak, nonatomic) IBOutlet UIButton *verificationCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *changeConfirmBtn;
@property (nonatomic, strong) NSMutableDictionary *dic;

@end

@implementation ICN_ChangeAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self creationView];
    
    
    
}
- (void)creationView{

    self.navigationItem.title = @"更换绑定手机";
    self.changeConfirmBtn.layer.cornerRadius = 5;
    self.changeConfirmBtn.layer.masksToBounds = YES;
    self.verificationCodeBtn.layer.borderColor = RGB0X(0x009dff).CGColor;
    self.verificationCodeBtn.layer.borderWidth = 1.0f;
    self.verificationCodeBtn.layer.cornerRadius = 4;
    self.verificationCodeBtn.layer.masksToBounds = YES;
    self.dic = [[NSMutableDictionary alloc] init];
    [self.dic setObject:self.phoneNumTF.text forKey:@"mobile"];
    

    self.phoneNumLab.text = self.phoneStr;
}
//获取验证码
- (IBAction)CodeBtnActions:(YHCountDownButton *)sender {
    [self.view endEditing:YES];
    YHCountDownButton *sender_btn = sender;
    
    if ([self.phoneNumTF.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
        return;
    }
    else{
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CODE params:@{@"mobile":self.phoneNumTF.text} success:^(id result) {
            BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (codemodel.code == 0) {
                sender_btn.enabled = NO;
                [sender_btn startWithSecond:CODE_S];
                [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                    NSString *title = [NSString stringWithFormat:@"%.2d秒",second];
                    return title;
                }];
                [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                    countDownButton.enabled = YES;
                    return @"重新获取验";
                }];
            }
            else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }
}

- (IBAction)ChangeConfirmButton:(UIButton *)sender {
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        [self.dic setObject:token forKey:@"token"];
        [self.dic setObject:self.phoneNumTF.text forKey:@"mobile"];
        NSLog(@"AAAAAAAAAAAAAAAAAAAAAAAAA%@", self.dic);
        self.dic[@"verify"] = _verificationCodeTF.text;
        
        
        if (_phoneNumTF.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写手机号"];
            return;
        }
        
        
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyPhoneBinding params:self.dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            NSString *tempStr = SF(@"%@",result[@"code"]);
            
            if ([tempStr isEqualToString:@"0"]) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
                
            }
        } failure:^(NSDictionary *errorInfo) {
            
            
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"未获取正确的用户信息"];
        //未获取正确信息应该跳转到登录页面
        if (self.navigationController) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    
    

    
    
    
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [self.verificationCodeTF resignFirstResponder];
    [self.phoneNumTF resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
