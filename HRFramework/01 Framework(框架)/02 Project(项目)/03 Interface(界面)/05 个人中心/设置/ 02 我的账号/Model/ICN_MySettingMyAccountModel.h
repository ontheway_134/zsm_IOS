//
//  ICN_MySettingMyAccountModel.h
//  ICan
//
//  Created by apple on 2017/1/11.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MySettingMyAccountModel : BaseOptionalModel
@property (nonatomic, copy) NSString *mobile;

@end
