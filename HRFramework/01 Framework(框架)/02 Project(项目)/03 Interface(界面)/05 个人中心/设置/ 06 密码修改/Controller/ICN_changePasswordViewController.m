//
//  ICN_changePasswordViewController.m
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_changePasswordViewController.h"
#import "ICN_MyChangePasswordModel.h"
@interface ICN_changePasswordViewController ()
@property (nonatomic, strong) NSMutableDictionary *dic;
@end

@implementation ICN_changePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"修改密码";
    [self creationView];
    self.view.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    
}
- (void)creationView{

    self.dic = [[NSMutableDictionary alloc] init];
    
    UIView *viewOfHead = [[UIView alloc] init];
    [self.view addSubview:viewOfHead];
    viewOfHead.backgroundColor = [UIColor whiteColor];
    [viewOfHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.offset(134);
        
    }];
    
    UIImageView *oldPasswordImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"密码"]];
    [viewOfHead addSubview:oldPasswordImg];

    [oldPasswordImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead).with.offset(15);
        make.left.equalTo(viewOfHead).with.offset(12);
        make.height.offset(12);
        make.width.equalTo(oldPasswordImg.mas_height);
    }];
    
    self.oldPassword = [[UITextField alloc] init];
    [viewOfHead addSubview:self.oldPassword];
    self.oldPassword.placeholder = @"请输入您的旧密码";
    
    self.oldPassword.font = [UIFont systemFontOfSize:13];
    self.oldPassword.secureTextEntry = YES;
    [self.oldPassword mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead).with.offset(10);
        make.left.equalTo(oldPasswordImg.mas_right).with.offset(10);
        make.height.offset(24);
        make.right.equalTo(viewOfHead).with.offset(20);
        
    }];
    UILabel *labelOfOld = [[UILabel alloc] init];
    [viewOfHead addSubview:labelOfOld];
    labelOfOld.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelOfOld mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.oldPassword.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
        
    }];
    
    UIImageView *newPasswordImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"密码"]];
    [viewOfHead addSubview:newPasswordImg];

    [newPasswordImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfOld.mas_bottom).with.offset(15);
        make.left.equalTo(viewOfHead).with.offset(12);
        make.height.offset(12);
        make.width.equalTo(newPasswordImg.mas_height);
    }];
    
    self.passwordNew = [[UITextField alloc] init];
    [viewOfHead addSubview:self.passwordNew];
    self.passwordNew.placeholder = @"设置新密码";
    
    self.passwordNew.font = [UIFont systemFontOfSize:13];
    self.passwordNew.secureTextEntry = YES;
    [self.passwordNew mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfOld.mas_bottom).with.offset(10);
        make.left.equalTo(newPasswordImg.mas_right).with.offset(10);
        make.height.offset(24);
        make.right.equalTo(viewOfHead).with.offset(20);
        
    }];

    UILabel *labelOfNew = [[UILabel alloc] init];
    [viewOfHead addSubview:labelOfNew];
    labelOfNew.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [labelOfNew mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordNew.mas_bottom).with.offset(10);
        make.left.equalTo(viewOfHead).with.offset(10);
        make.right.equalTo(viewOfHead).with.offset(10);
        make.height.offset(1);
        
    }];
    
    UIImageView *confirmPasswordImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"密码"]];
    [viewOfHead addSubview:confirmPasswordImg];
    [confirmPasswordImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfNew.mas_bottom).with.offset(15);
        make.left.equalTo(viewOfHead).with.offset(12);
        make.height.offset(12);
        make.width.equalTo(confirmPasswordImg.mas_height);
    }];
    
    self.confirmPassword = [[UITextField alloc] init];
    [viewOfHead addSubview:self.confirmPassword];
    self.confirmPassword.placeholder = @"确认新密码";
    self.confirmPassword.font = [UIFont systemFontOfSize:13];
    
    
    self.confirmPassword.secureTextEntry = YES;
    [self.confirmPassword mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfNew.mas_bottom).with.offset(10);
        make.left.equalTo(confirmPasswordImg.mas_right).with.offset(10);
        make.height.offset(24);
        make.right.equalTo(viewOfHead).with.offset(20);
        
    }];
    
    UIButton *submitBtn = [[UIButton alloc] init];
    [self.view addSubview:submitBtn];
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    submitBtn.backgroundColor = [UIColor colorWithRed:41 / 255.0f green:141 / 255.0f blue:250 / 255.0f alpha:1];
    [submitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(viewOfHead.mas_bottom).with.offset(36);
        make.left.equalTo(self.view).with.offset(12);
        make.right.equalTo(self.view).with.offset(-12);
        make.height.offset(39);
        
    }];

    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitButtonClick) forControlEvents:UIControlEventTouchUpInside];
    submitBtn.layer.cornerRadius = 5;
    submitBtn.layer.masksToBounds = YES;
    
    

}
- (void)submitButtonClick{
    

    if (![self.passwordNew.text isEqualToString:self.confirmPassword.text]) {
        
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"两次密码不同请重新输入" preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
        
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
    [alert addAction:action];
    [alert addAction:cancel];
    [self showDetailViewController:alert sender:nil];
    } else {
    
        [self date];
        
        
    
    
    }


}
- (void)date{

    if (_oldPassword.text.length == 0 || _passwordNew.text.length == 0 || _confirmPassword.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写完整"];
    } else {
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        [self.dic setObject:token forKey:@"token"];
        [self.dic setObject:self.oldPassword.text forKey:@"old"];
        [self.dic setObject:self.passwordNew.text forKey:@"password"];
        [self.dic setObject:self.confirmPassword.text forKey:@"rePassword"];
        
        if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
            // 当前无网络
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
            return ;
        }
        
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyChangePassword params:self.dic success:^(id result) {
            
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            NSMutableArray<ICN_MyChangePasswordModel *> *resultArr = [NSMutableArray array];
            if (firstModel.code == 0) {
                NSArray *array = [result valueForKey:@"result"];
                
                for (NSDictionary *item in array) {
                    ICN_MyChangePasswordModel *model = [[ICN_MyChangePasswordModel alloc] initWithDictionary:item error:nil];
                    [resultArr addObject:model];
                }
                
                // 数据获取完成
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"密码修改成功"];
                [self.navigationController popViewControllerAnimated:YES];
                
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"密码修改失败"];
                
            }
        } failure:^(NSDictionary *errorInfo) {
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
