//
//  ICN_changePasswordViewController.h
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_changePasswordViewController : BaseViewController
@property (nonatomic, strong) UITextField *oldPassword;
@property (nonatomic, strong) UITextField *passwordNew;
@property (nonatomic, strong) UITextField *confirmPassword;
@end
