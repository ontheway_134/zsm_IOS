//
//  ICN_MessageCenterViewController.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MessageCenterViewController.h"
#import "ICN_MessageDetailsViewController.h"
#import "ICN_MessageDetailsMModel.h"
#import "ICN_MessageCenterTableViewCell.h"
#import "ICN_IndustryNextViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_MyMessageDetailsModel.h"
@interface ICN_MessageCenterViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSInteger pageNum;
}

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/

@end

@implementation ICN_MessageCenterViewController
#pragma mark - ---------- 生命周期 ----------
- (NSMutableArray *)dataArr{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
//    [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
    self.tableView.hidden = NO;
}
#pragma mark 消息接口
- (void)AllnetWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_memberMessageList para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MessageDetailsMModel * model=[[ICN_MessageDetailsMModel alloc]initWithDictionary:obj error:nil];
                [self.dataArr addObject:model];
            }];
            if (self.dataArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.dataArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_memberMessageList para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            /*判断data是什么类型*/
            if ([data isKindOfClass:[NSMutableArray class]]) {
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSLog(@"%@",obj);
                    
                    ICN_MessageDetailsMModel * model=[[ICN_MessageDetailsMModel alloc]initWithDictionary:obj error:nil];
                    [self.dataArr addObject:model];
                }];
            }
            NSLog(@"%@",self.dataArr);
            if (self.dataArr.count == 0) {
                self.tableView.hidden =YES;
                self.noDataImage.hidden = NO;
                self.noDataLabel.hidden = NO;
                self.noDataLabel.text = @"暂无消息信息";
            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
    
}

#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
//- (void)configData{
//    
//    pageNum = 1;
//    /*初始化token*/
//    NSString *token;
//    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
//        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
//    }
//    
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//    [dic setObject:token forKey:@"token"];
//    [[HRRequest manager]POST:PATH_memberMessageList para:nil success:^(id data) {
//        NSLog(@"%@",data);
////        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//////            ICN_MessageDetailsMModel *model = [[ICN_MessageDetailsMModel alloc]init];
//////            [model setValuesForKeysWithDictionary:obj];
//////            [self.dataArr addObject:model];
//////            ICN_MessageDetailsMModel * model = [[ICN_MessageDetailsMModel alloc]init];
////            ICN_MessageDetailsMModel * model=[[ICN_MessageDetailsMModel alloc]initWithDictionary:obj error:nil];
////            [self.dataArr addObject:model];
////        }];
//        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            ICN_MessageDetailsMModel * model=[[ICN_MessageDetailsMModel alloc]initWithDictionary:obj error:nil];
//            [self.dataArr addObject:model];
//
//        }];
//        NSLog(@"%@",self.dataArr);
//        [self.tableView reloadData];
//        [self.tableView.mj_header endRefreshing];
//        
//    } faiulre:^(NSString *errMsg) {
//        NSLog(@"%@",errMsg);
//        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"加载中"];
//        self.tableView.hidden = YES;
//        self.noDataLabel.hidden = NO;
//        self.noDataImage.hidden = NO;
//    }];
//}
- (void)creationView{
    self.navigationItem.title = @"我的消息";
    
    pageNum = 1;
}
- (void)regeditCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MessageCenterTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MessageCenterTableViewCell"];
}
#pragma mark - ---------- Section的数量 ----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
#pragma mark - ---------- Cell的数量 ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArr.count;
    //    return  3;
    
}

#pragma mark - ---------- 每个cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_MessageCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_MessageCenterTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone]; //去除点击阴影
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_MessageDetailsMModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    
    
    
    return cell;
    
    
}
#pragma mark - ---------- cell的高度 ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
    
    
}

#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (editingStyle == UITableViewCellEditingStyleDelete)
    //    {
    
    
    
    
    UIAlertController *Sign=[UIAlertController alertControllerWithTitle:@"您确定要删除信息吗？" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *yes=[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        ICN_MessageDetailsMModel *model = self.dataArr[indexPath.row];
        dic[@"id"] = model.id;
        
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_MyDeletedoDeleteMessage params:dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        
        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell
        
    }];
    UIAlertAction * cacel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [Sign addAction:yes];
    [Sign addAction:cacel];
    [self presentViewController:Sign animated:YES completion:nil];
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_MessageDetailsViewController *vc = [[ICN_MessageDetailsViewController alloc] init];
    ICN_MessageDetailsMModel *model = _dataArr[indexPath.row];
    
    //
    //    ICN_IndustryModel *model = _dataArr[indexPath.row];
    
    vc.url = model.id;
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc
                                         animated:YES];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setMJRefresh {
    //下拉刷新
    
    
    
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    //    self.tableView.mj_footer.hidden = NO;
    
    //上拉加载更多
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreNetRequest)];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;
    
}

- (void)moreNetRequest {
    
    pageNum = pageNum + 1;
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [dic setObject:SF(@"%ld", pageNum) forKey:@"page"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberMessage/memberMessageList" params:dic success:^(id result) {
        NSLog(@"sssssssssssssssssssssssssssss%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@" sssssssssssssssssssssssssss%@",dic1);
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MessageDetailsMModel *model = [[ICN_MessageDetailsMModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        
        [_tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
        
        //        [self.tableView.mj_footer endRefreshingWithNoMoreData];
        
    } failure:^(NSDictionary *errorInfo) {
    }];
}
#pragma mark 弹窗提示
-(void)showMessageXzz:(NSString *)sign
{
    
    
    
}
@end
