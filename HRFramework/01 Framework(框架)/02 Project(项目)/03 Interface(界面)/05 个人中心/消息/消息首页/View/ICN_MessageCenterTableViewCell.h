//
//  ICN_MessageCenterTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MessageDetailsMModel;
@interface ICN_MessageCenterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *announcementImage;
@property (weak, nonatomic) IBOutlet UIImageView *DimageView;
@property (weak, nonatomic) IBOutlet UILabel *subjectLab;
@property (weak, nonatomic) IBOutlet UILabel *createDateLab;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (nonatomic, strong) ICN_MessageDetailsMModel *model;
@property (weak, nonatomic) IBOutlet UILabel *isRead;

@end
