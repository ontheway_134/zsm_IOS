//
//  ICN_MessageDetailsMModel.h
//  ICan
//
//  Created by apple on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseOptionalModel.h"
@interface ICN_MessageDetailsMModel : BaseOptionalModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subject;
@property (nonatomic, copy) NSString *createDate;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *type;
@property (strong,nonatomic)NSString * readStatus;
@end
