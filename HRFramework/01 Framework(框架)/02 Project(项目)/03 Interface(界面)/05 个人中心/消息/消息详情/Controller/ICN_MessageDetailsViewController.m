//
//  ICN_MessageDetailsViewController.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MessageDetailsViewController.h"
#import "ICN_MyMessageDetailsModel.h"
#import "ToolAboutTime.h"
@interface ICN_MessageDetailsViewController ()<UIGestureRecognizerDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *wenziLabel;
@property (nonatomic , strong)UIView *alongView;
@property (nonatomic , strong)UIImageView *downImgView;
@property (nonatomic , strong)UITapGestureRecognizer *tap;

@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *dataLabel;
@property(nonatomic)UIImageView *picImageView;
@property(nonatomic)UILabel *summaryLabel;

@property(nonatomic)NSString *collect;

@end

@implementation ICN_MessageDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createUI];
    [self datas];
    //self.tap.delegate = self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
}
-(void)btnAction:(UIButton *)btn{
    
    NSDictionary *dic = @{@"informationId":_url,@"memberID":@"9688b18ac092f2084c113f1ceb79a2eb"};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsReport" params:dic success:^(id result) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"举报成功"];
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}
//收藏
-(void)collectionAction:(UIButton *)btn{
    ///btn.selected = !btn.selected;
    if ([_collect isEqualToString:@"0"]) {
        NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url]};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollect" params:dic success:^(id result) {
            
            if ([_collect isEqualToString:@"0"]) {
                [btn setTitle:@"取消收藏" forState:UIControlStateNormal];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
            }
            
            [self datas];
            
            
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }else{
        NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url]};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollectDel" params:dic success:^(id result) {
            
            
            if ([_collect isEqualToString:@"1"]) {
                [btn setTitle:@"收藏" forState:UIControlStateNormal];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏"];
            }
            
            
            
            
            [self datas];
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
        
    }
    
    
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [APPLICATION.keyWindow removeGestureRecognizer:_tap];
}

- (void)onClick:(UIButton *)button{
    
    if (_alongView) {
        _alongView.hidden = YES;
        _downImgView.hidden = YES;
        return ;
    }
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    view.tag = 111;
    _alongView = view;
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];
    
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.userInteractionEnabled = YES;
    imageV.image = [UIImage imageNamed:@"下拉.png"];
    imageV.tag = 112;
    [currentWindow addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(60);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(118);
    }];
    UIButton *btn = [[UIButton alloc]init];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"举报" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [imageV addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(11);
        make.height.mas_equalTo(35);
        
    }];
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *btn2 = [[UIButton alloc]init];
    if ([_collect isEqualToString:@"0"]) {
        [btn2 setTitle:@"收藏" forState:UIControlStateNormal];
    }else{
        
        [btn2 setTitle:@"取消收藏" forState:UIControlStateNormal];
    }
    
    btn2.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn2 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(collectionAction:) forControlEvents:UIControlEventTouchUpInside];
    [imageV addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    _downImgView = imageV;
    UILabel *label2 = [[UILabel alloc]init];
    label2.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn2.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *btn3 = [[UIButton alloc]init];
    [btn3 setTitle:@"分享" forState:UIControlStateNormal];
    btn3.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn3 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [imageV addSubview:btn3];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label2.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    // 单击的 Recognizer
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap:)];
    //点击的次数
    //    singleRecognizer.numberOfTapsRequired = 1; // 单击
    _tap = singleRecognizer;
    singleRecognizer.delegate = self;
    //给self.view添加一个手势监测；
    
    [_alongView addGestureRecognizer:singleRecognizer];
    
    
    
    
}
-(void)SingleTap:(UITapGestureRecognizer*)recognizer  {
    
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    _alongView = nil;
    
    
    
}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
}
-(void)createUI{
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"消息详情";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:16],
       NSForegroundColorAttributeName:RGB0X(0Xffffff)}];
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
    
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = leftItem3;
    
    
    
    
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    
    [self.view addSubview:_scrollView];
    
    _scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self datas];
    }];
    
    UIView *contentView = [[UIView alloc]init];
    
    [_scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView);
    }];
    
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont systemFontOfSize:14];
    _titleLabel.text = @"asdaaaaaaadasd";
    _titleLabel.numberOfLines = 0;
    _titleLabel.textColor = RGB0X(0X000000);
    [contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        //make.width.mas_equalTo(100);
        
        make.top.mas_equalTo(15);
        
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"时间.png"];
    
    [contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        
    }];
    
    _dataLabel =[[UILabel alloc]init];
    _dataLabel.text = @"123123123213";
    _dataLabel.textColor = RGB0X(0X666666);
    _dataLabel.font = [UIFont systemFontOfSize:11];
    [contentView addSubview:_dataLabel];
    [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(5);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(11);
    }];
    
    UILabel *xiantiaoLabel = [[UILabel alloc]init];
    xiantiaoLabel.backgroundColor = RGB(236, 236, 236);
    [contentView addSubview:xiantiaoLabel];
    
    [xiantiaoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_dataLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(1);
        
    }];
    
    _picImageView = [[UIImageView alloc]init];
//    _picImageView.image = [UIImage imageNamed:@"图片.png"];
    [contentView addSubview:_picImageView];
    [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(xiantiaoLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(160);
    }];
    
    _summaryLabel = [[UILabel alloc]init];
    _summaryLabel.text = @"";
    _summaryLabel.textColor = RGB0X(0X333333);
    //wenziLabel.font = [UIFont systemFontOfSize:12];
    _summaryLabel.numberOfLines = 0;
    _summaryLabel.font = [UIFont systemFontOfSize:12];
    [contentView addSubview:_summaryLabel];
    [_summaryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_picImageView.mas_bottom).offset(10);
        make.bottom.mas_equalTo(contentView.mas_bottom).offset(0);
        
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)datas{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [dic setObject:self.url forKey:@"id"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberMessage/getMessage" params:dic success:^(id result) {
        

        ICN_MyMessageDetailsModel *model = [[ICN_MyMessageDetailsModel alloc]initWithDictionary:result[@"result"] error:nil];
        if ([model.img isEqualToString:@"0"]) {
            [_picImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.img)] placeholderImage:[UIImage imageNamed:@"占位图"]];
            [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.left.mas_equalTo(self.view.mas_left).offset(10);
//                make.right.mas_equalTo(self.view.mas_right).offset(-10);
//                make.top.mas_equalTo(xiantiaoLabel.mas_bottom).offset(15);
                make.height.mas_equalTo(0);
            }];
        }
        else{
            [_picImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.img)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }
        
        _titleLabel.text = model.title;
        if (model.createDate == nil) {
            model.createDate = @"";
        }
        _dataLabel.text = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
        _summaryLabel.text = model.content;
        
        [_scrollView.mj_header endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
}

@end
