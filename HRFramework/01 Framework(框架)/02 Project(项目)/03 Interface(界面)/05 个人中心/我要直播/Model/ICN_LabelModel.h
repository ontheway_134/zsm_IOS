//
//  ICN_LabelModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_LabelModel : BaseOptionalModel
@property (strong,nonatomic)NSString *label ;
@property (strong,nonatomic)NSString *pic ;
@property (strong,nonatomic)NSString *LabelID ;
@property (strong,nonatomic)NSString *sort ;
@end
