//
//  ICN_LabelModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LabelModel.h"

@implementation ICN_LabelModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"LabelID" : @"id"}];
}
@end

