//
//  ICN_WantLiveTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_WantLiveTableViewCell.h"

@implementation ICN_WantLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //开始编辑
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beginediting:) name:UITextViewTextDidBeginEditingNotification object:self.titleLiveLabel];
    //停止编辑
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endediting:) name:UITextViewTextDidEndEditingNotification object:self.titleLiveLabel];
    /*改变*/
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doingEditing:) name:UITextViewTextDidChangeNotification object:self.titleLiveLabel];
    self.titleLiveLabel.delegate = self;
}
#pragma mark 开始编译
-(void)beginediting:(NSNotification *)notification
{
    self.placeHoderLabel.hidden = YES;
}
#pragma mark 停止编译
-(void)endediting:(NSNotification *)notification
{
    NSLog(@"停止编译");
}
#pragma mark 正在编译
-(void)doingEditing:(NSNotification *)notification
{
    if (self.titleLiveLabel.text.length == 0) {
        /*停止编辑*/
        self.placeHoderLabel.hidden = NO;
    }
    else{
        self.placeHoderLabel.hidden = YES;
    }
}
#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
}
#pragma mark 选择标签
- (IBAction)label:(UITapGestureRecognizer *)sender {
    if (self.returnLabelBlock) {
        self.returnLabelBlock(sender);
    }
}
#pragma mark 直播开始时间
- (IBAction)startTimeBtnTap:(UITapGestureRecognizer *)sender {
    if (self.returnStartBlock) {
        self.returnStartBlock(sender);
    }
}

#pragma mark 直播结束时间
- (IBAction)finishTimeBtnTap:(UITapGestureRecognizer *)sender {
    if (self.returnFinishBlock) {
        self.returnFinishBlock(sender);
    }
}
-(void)returnStartBlock:(ReturnStartBlock)block{
    self.returnStartBlock = block;
}
-(void)ReturnLabelBlock:(ReturnLabelBlock)block{
    self.returnLabelBlock = block;
}
-(void)ReturnFinishBlock:(ReturnFinishBlock)block{
    self.returnFinishBlock = block;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
