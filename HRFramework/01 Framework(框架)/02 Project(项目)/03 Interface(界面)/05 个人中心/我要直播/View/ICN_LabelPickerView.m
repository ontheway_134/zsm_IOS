//
//  ICN_LabelPickerView.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//
#define kSCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define kSCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define kUIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define kApplicationShared  [UIApplication sharedApplication]
#import "ICN_LabelPickerView.h"

@interface  ICN_LabelPickerView()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSString *selectedStr;
    NSInteger row1;
    NSArray *dataDegreeArr;
}
@property (weak, nonatomic) IBOutlet UIView *pickView;
@property (weak, nonatomic) IBOutlet UIView *backGroundView;
@end
@implementation ICN_LabelPickerView
- (NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return  _dataSource;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setFrame:CGRectMake(0, 64, kSCREEN_WIDTH, kSCREEN_HEIGHT-64)];
    
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
}
- (void)layoutSubviews {
    
    [self.pickView setFrame:CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, 200)];
    CABasicAnimation *move = [CABasicAnimation animationWithKeyPath:@"position"];
    move.fromValue = [NSValue valueWithCGPoint:CGPointMake(self.center.x, kSCREEN_HEIGHT)];
    move.duration = 0.2;
    [self.pickView.layer addAnimation:move forKey:nil];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.dataSource.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 30;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    selectedStr = self.dataSource[0];
    return self.dataSource[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    selectedStr = self.dataSource[row];
    row1 = row;
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont systemFontOfSize:12]];
        pickerLabel.textColor = kUIColorFromRGB(0X333333);
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

#pragma mark 背景手势
- (IBAction)backTap:(UITapGestureRecognizer *)sender {
    [self animationHideShadowView];
    [self animationHideActionSheet];
}
#pragma mark 取消按钮
- (IBAction)cancelBtnClick:(UIButton *)sender {
     [self removeFromSuperview];
}
#pragma mark 确定按钮
- (IBAction)sureBtnClick:(UIButton *)sender {
    if(self.enterBlock){
        self.enterBlock(selectedStr,row1);
    }
    [self removeFromSuperview];
}

#pragma mark - 隐藏动画
- (void)animationHideShadowView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.backGroundView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}
- (void)animationHideActionSheet
{
    [UIView animateWithDuration:0.3 animations:^{
        self.pickView.alpha = 0;
        self.pickView.frame = CGRectMake(0, kSCREEN_HEIGHT, kSCREEN_WIDTH, self.frame.size.height);
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
    }];
    
}
- (void)showView:(ICN_LabelPickerView *)view
{
    [kApplicationShared.keyWindow addSubview:view];
    
    CABasicAnimation *opacity = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacity.fromValue = @(0);
    opacity.duration = 0.2;
    [self.backGroundView.layer addAnimation:opacity forKey:nil];
}
- (void)cancelBlock:(CancelBlock)cancelBlock {
    
    self.cancelBlock = cancelBlock;
}
- (void)enterBlock:(EnterBlock)enterBlock {
    
    self.enterBlock = enterBlock;
}
@end
