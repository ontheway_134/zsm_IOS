//
//  ICN_LabelPickerView.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CancelBlock) ();
typedef void (^EnterBlock) (NSString *str,NSInteger row);
@interface ICN_LabelPickerView : UIView

@property (weak, nonatomic) IBOutlet UIButton *cacalBtnClick;
@property (weak, nonatomic) IBOutlet UIButton *sureBtnClick;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *dataSource;


@property (copy, nonatomic) CancelBlock cancelBlock;
@property (copy, nonatomic) EnterBlock enterBlock;
- (void)cancelBlock:(CancelBlock)cancelBlock;
- (void)enterBlock:(EnterBlock)enterBlock;
/** 显示选项卡 */
- (void)showView:(ICN_LabelPickerView *)view;
@end
