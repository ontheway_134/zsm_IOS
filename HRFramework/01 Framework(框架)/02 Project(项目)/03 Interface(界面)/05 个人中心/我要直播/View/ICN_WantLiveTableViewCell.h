//
//  ICN_WantLiveTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ReturnLabelBlock)();
typedef void (^ReturnStartBlock)();
typedef void (^ReturnFinishBlock)();
@interface ICN_WantLiveTableViewCell : UITableViewCell<UITextViewDelegate>
/***********下面是需要判断时候输入的属性***********/
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UITextView *titleLiveLabel;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelLabel;
@property (weak, nonatomic) IBOutlet UITextField *moneyLabel;
@property (weak, nonatomic) IBOutlet UITextField *peopleCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *placeHoderLabel;/*提示文字*/
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;/*放置背景图片*/

-(void)ReturnLabelBlock:(ReturnLabelBlock)block;
@property (strong, nonatomic) ReturnLabelBlock returnLabelBlock;

-(void)returnStartBlock:(ReturnStartBlock)block;
@property (strong, nonatomic) ReturnStartBlock returnStartBlock;

-(void)ReturnFinishBlock:(ReturnFinishBlock)block;
@property (strong, nonatomic) ReturnFinishBlock returnFinishBlock;
@end
