//
//  ICN_WantLiveViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "KMDatePicker.h"
@interface ICN_WantLiveViewController : BaseViewController<KMDatePickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *markView;/*蒙版*/
@property (strong,nonatomic)NSString * typeStr;/*判断是开始时间还是结束时间*/
@property (strong,nonatomic)NSString * startTime;
@property (strong,nonatomic)NSString * endTime;
@property (strong,nonatomic)NSMutableArray * labelSoure;/*标签数组*/
@property (strong,nonatomic)NSMutableArray * labelSoureID;
@property (strong,nonatomic)NSMutableArray * labelSoureName;
@property (strong,nonatomic)NSMutableArray * photoSource;
@property (strong,nonatomic)NSMutableArray * photoDeleSource;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (strong,nonatomic)NSMutableArray * deleSourse;
@end
