//
//  ICN_WantLiveViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//
#define kApplicationShared  [UIApplication sharedApplication]

#import "ICN_WantLiveViewController.h"
#import "ICN_WantLiveTableViewCell.h"/*发布直播*/
#import "DateHelper.h"
#import "NSDate+CalculateDay.h"
#import "NSDate+TimeStamp.h"
#import "NSString+BlankString.h"
#import "ICN_LabelModel.h"
#import "ICN_LabelPickerView.h"/*标签pickerView*/
#import "PhotosCollectionViewCell.h"/*图片选择器的cell*/
#import "DLPopPhotoView.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <Photos/Photos.h>
#import "TZImageManager.h"
#import "TZVideoPlayerController.h"

/*图片上传*/
#import "HRNetworkingManager+ICN_Publication.h"
#import "ICN_CommonImagesLoadModel.h"

@interface ICN_WantLiveViewController ()<TZImagePickerControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
@property (strong,nonatomic)DLPopPhotoView * photoPopView;
@property(strong,nonatomic)ICN_WantLiveTableViewCell * cell;
@property (strong,nonatomic) KMDatePicker *datePicker;
@property (strong,nonatomic) NSString * LabelID;/*标签ID*/
@property (strong,nonatomic) ICN_LabelPickerView *agePickView;/**/
@property(nonatomic, strong)UIImagePickerController *imagePickerController;
@property (nonatomic, assign)CGFloat itemWH;
@property (nonatomic, assign)CGFloat margin;
@end

@implementation ICN_WantLiveViewController
#pragma mark - ---------- 懒加载 ----------
#pragma mark 懒加载照相机弹出视图
- (DLPopPhotoView *)photoPopView{
    if (!_photoPopView) {
        _photoPopView = [[NSBundle mainBundle]loadNibNamed:@"DLPopPhotoView" owner:nil options:nil].firstObject ;
        _photoPopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    return  _photoPopView;
}
#pragma mark 删除的数组
- (NSMutableArray *)deleSourse{
    if (!_deleSourse) {
        _deleSourse = [NSMutableArray array];
    }
    return  _deleSourse;
}
- (NSMutableArray *)photoDeleSource{
    if (!_photoDeleSource) {
        _photoDeleSource = [NSMutableArray array];
    }
    return _photoDeleSource;
}
- (NSMutableArray *)photoSource{
    if (!_photoSource) {
        _photoSource = [NSMutableArray array];
    }
    return _photoSource;
}
- (NSMutableArray *)labelSoure{
    if (!_labelSoure) {
        _labelSoure = [NSMutableArray array];
    }
    return _labelSoure;
}
- (NSMutableArray *)labelSoureID{
    if (!_labelSoureID) {
        _labelSoureID = [NSMutableArray array];
    }
    return _labelSoureID;
}
- (NSMutableArray *)labelSoureName{
    if (!_labelSoureName) {
        _labelSoureName = [NSMutableArray array];
    }
    return _labelSoureName;
}
#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*初始化日历*/
    CGRect rect = [[UIScreen mainScreen] bounds];
    rect = CGRectMake(0, SCREEN_HEIGHT-240, SCREEN_WIDTH, 240);
    // 年月日时分
    self.datePicker= [[KMDatePicker alloc]
                      initWithFrame:rect
                      delegate:self
                      datePickerStyle:KMDatePickerStyleYearMonthDayHourMinute];
    self.datePicker.minLimitedDate = [[DateHelper localeDate] addMonthAndDay:0 days:0];
    self.datePicker.maxLimitedDate = [self.datePicker.minLimitedDate addMonthAndDay:240 days:0];
    [self.markView addSubview:self.datePicker];
    /*取消按钮*/
    [self.datePicker ReturnCacelBtnBlock:^{
        self.markView.hidden = YES;
    }];
    /*添加手势*/
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectedIcon)];
    [self.markView addGestureRecognizer:tap];
    /*标签的网络请求*/
    [self labelAF];
    /*布局collection*/
    [self collectionConfig];
}
#pragma mark - ---------- IBActions ----------
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.agePickView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 提交按钮
- (IBAction)submitBtnClick:(UIButton *)sender {
    
    // 第一步判断网络状态
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        // 当前无网络
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
        return ;
    }
    /*对所录入的信息进行判断*/
    if ([self.cell.labelLabel.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择标签"];
        return;
    }
    if ([self.cell.titleTF.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写标题"];
        return;
    }
    if ([self.cell.titleLiveLabel.text isBlankString]||[self.cell.titleLiveLabel.text isEqualToString:@"输入直播内容"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写直播内容"];
        return;
    }
    if ([self.cell.startTimeLabel.text isBlankString]||[self.cell.startTimeLabel.text isEqualToString:@"开始时间"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择开始时间"];
        return;
    }
    if ([self.cell.finishTimeLabel.text isBlankString]||[self.cell.finishTimeLabel.text isEqualToString:@"结束时间"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择结束时间"];
        return;
    }
    if ([self.cell.moneyLabel.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写报名费用"];
        return;
    }
    if ([self.cell.peopleCountLabel.text isBlankString]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写报名人数"];
        return;
    }
    if (self.photoSource.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择背景图片"];
        return;
    }
    NSLog(@"%@",self.photoSource);
    
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"图片上传中"];
    [HRNetworkingManager uploadImagesWithImageArr:self.photoSource Success:^(ICN_CommonImagesLoadModel *model) {
        if (model.code == 0) {
            // 图片上传成功
            
            [MBProgressHUD hiddenHUDWithSuperView:self.view];

            // 成功后将路径信息传递过去
            NSString *photeStr = model.result[@"fileName"];
            
            /*返回地址成功之后进行下一步的网络请求*/
            
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            /*下面是网络请求部分*/
            WEAK(weakSelf);
            NSDictionary *paramDic = @{@"token":token,
                                       @"title":weakSelf.cell.titleTF.text,
                                       @"content":weakSelf.cell.titleLiveLabel.text,
                                       @"price":weakSelf.cell.moneyLabel.text,
                                       @"beginDate": weakSelf.startTime,
                                       @"endDate":weakSelf.endTime,
                                       @"limitPerson":weakSelf.cell.peopleCountLabel.text,
                                       @"contantType":self.LabelID,
                                       @"pic":photeStr,
                                       };
            NSLog(@"%@",paramDic);
            [[[HRNetworkingManager alloc] init ] POST_PATH:PATH_ReleaseLive params:paramDic success:^(id result) {
                BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (baseModel.code == 0) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"提交成功"];
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"提交失败"];
                }
            } failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"提交失败"];
            }];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"提交失败"];
        }
    } Failure:^(NSDictionary *errorInfo) {
        
    }];
    
}
#pragma mark 蒙版上面的手势
-(void)selectedIcon{
    self.markView.hidden = YES;
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark 布局collectionView
-(void)collectionConfig{
    
    self.cell= [[NSBundle mainBundle]loadNibNamed:@"ICN_WantLiveTableViewCell" owner:nil options:nil].firstObject;
    [self.cell.collectionView registerNib:[UINib nibWithNibName:@"PhotosCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PhotosCollectionViewCell"];
    
    /*特色商家的布局*/
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 10;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(SCREEN_WIDTH/4-10,60.0);
    [self.cell.collectionView setCollectionViewLayout:layout];
    /*初始化商铺的collectionView*/
    [self.cell.collectionView registerNib:[UINib nibWithNibName:@"DLSearchCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"DLSearchCollectionViewCell"];
    /*设置代理*/
    self.cell.collectionView.delegate = self ;
    self.cell.collectionView.dataSource = self;
}
#pragma mark 照相机拍照
-(void)photoByCamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if ((authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied) && iOS7Later) {
        
    } else { // 调用相机
        UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            /*隐藏tabar*/
            self.tabBarController.tabBar.hidden = YES;
            self.imagePickerController.sourceType = sourceType;
            if(iOS8Later) {
                self.imagePickerController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            }
            [self presentViewController:self.imagePickerController animated:YES completion:nil];
        } else {
            NSLog(@"模拟器中无法打开照相机,请在真机中使用");
        }
    }
}
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
-(void)labelAF{
    
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    [[HRRequest manager]POST:PATH_MyactivityNav para:nil success:^(id data) {
        NSLog(@"%@",data);
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_LabelModel * model = [[ICN_LabelModel alloc]initWithDictionary:obj error:nil];
            [self.labelSoure addObject:model];
        }];
        [self.labelSoure enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_LabelModel * model = obj;
            [self.labelSoureID addObject:model.LabelID];
            [self.labelSoureName addObject:model.label];
        }];
        NSLog(@"%@",self.labelSoureID);
        NSLog(@"%@",self.labelSoureName);
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"标签获取失败"];
    }];
}
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  516;
}
#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // self.cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_WantLiveTableViewCell" owner:nil options:nil].firstObject;
    self.cell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    self.cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*选择标签*/
    [self.cell ReturnLabelBlock:^{
        /**/
        self.agePickView = [[NSBundle mainBundle]loadNibNamed:@"ICN_LabelPickerView" owner:nil options:nil].firstObject;
        [ self.agePickView showView: self.agePickView];
        
        
        self.agePickView.dataSource = self.labelSoureName;
        [ self.agePickView.pickerView reloadAllComponents];
        
        
        [ self.agePickView enterBlock:^(NSString *str,NSInteger row1) {
            NSLog(@"%@",str);
            NSLog(@"%ld",row1);
            self.cell.labelLabel.text = str;
            self.LabelID = self.labelSoureID[row1];
            NSLog(@"%@",self.LabelID);
        }];
    }];
    /*选择开始时间*/
    [self.cell returnStartBlock:^{
        self.typeStr = @"1";
        self.markView.hidden = !self.markView.hidden;
        [self.view endEditing:YES];
    }];
    /*选择结束时间*/
    [self.cell ReturnFinishBlock:^{
        self.typeStr = @"2";
        self.markView.hidden = !self.markView.hidden;
        [self.view endEditing:YES];
    }];
    return  self.cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
#pragma mark - ---------- KMDatePickerDelegate ----------
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
#pragma mark - ---------- KMDatePickerDelegate ----------
- (void)datePicker:(KMDatePicker *)datePicker didSelectDate:(KMDatePickerDateModel *)datePickerDate {
    NSString *dateStr = [NSString stringWithFormat:@"%@-%@-%@ %@:%@",
                         datePickerDate.year,
                         datePickerDate.month,
                         datePickerDate.day,
                         datePickerDate.hour,
                         datePickerDate.minute
                         ];
    /*完成赋值 首先需要判断 1 开始时间 2结束时间*/
    if ([self.typeStr isEqualToString:@"1"]) {
        self.cell.startTimeLabel.text = dateStr;
        // 日期格式化类
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        
        // 设置日期格式 为了转换成功
        
        format.dateFormat = @"yyyy-MM-dd HH:mm";
        
        // NSString * -> NSDate *
        
        NSDate *data = [format dateFromString:dateStr];
        
        self.startTime = [data timeStamp10];
        NSLog(@"%@",self.startTime);
        
        self.markView.hidden = YES;
    }else if ([self.typeStr isEqualToString:@"2"]){
        self.cell.finishTimeLabel.text = dateStr;
        
        
        // 日期格式化类
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        
        // 设置日期格式 为了转换成功
        
        format.dateFormat = @"yyyy-MM-dd HH:mm";
        
        // NSString * -> NSDate *
        
        NSDate *data = [format dateFromString:dateStr];
        
        self.endTime = [data timeStamp10];
        NSLog(@"%@",self.endTime);
        
        self.markView.hidden = YES;
    }else{
        
    }
}
#pragma mark - --- UICollectionViewDataSource ---
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;{
    NSLog(@"%@",self.photoSource);
    return self.photoSource.count + 1;
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotosCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PhotosCollectionViewCell" forIndexPath:indexPath];
    if(self.photoDeleSource.count<=1){
        if (indexPath.row == self.photoSource.count) {
            if (self.photoDeleSource.count == 1) {
                cell.contentView.hidden = YES;
            }else{
                cell.imageView.image = [UIImage imageNamed:@"+"];
                cell.deleteBtn.hidden = YES;
                cell.contentView.hidden = NO;
            }
        } else {
            NSLog(@"======%ld",indexPath.row);
            //            if ([self.photoSource[indexPath.row] isKindOfClass:[NSString class]]) {
            //                [cell.imageView sd_setImageWithURL:[NSURL URLWithString:CZString(@"%@%@",Image_DomainName,self.photoSource[indexPath.row])]] ;
            //            }else{
            cell.imageView.image = self.photoSource[indexPath.row];
            //            }
            cell.deleteBtn.hidden = NO;
            cell.contentView.hidden = NO;
        }
        cell.deleteBtn.tag = indexPath.row;
        [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        cell.contentView.hidden = YES;
    }
    return cell;
}
#pragma mark - --- UICollectionViewDelegate ---
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == self.photoSource.count) {
        if (self.photoDeleSource.count<1) {
            NSLog(@"%ld",indexPath.row);
            NSLog(@"%ld",self.photoSource.count);
            [self choosePhoto:indexPath];
            //[self.photoPopView removeFromSuperview];
            //            self.photoPopView.headLabel.text = @"上传图片";
           // [kApplicationShared.keyWindow addSubview:self.photoPopView];
            //[self.photoPopView mas_makeConstraints:^(MASConstraintMaker *make) {
               // make.bottom.mas_equalTo(0);
               // make.left.mas_equalTo(0);
               // make.right.mas_equalTo(0);
               // make.top.mas_equalTo(0);
           // }];
            
            //[self.photoPopView returnLocationPhoto:^{
               // [self choosePhoto:indexPath];
                //[self.photoPopView removeFromSuperview];
           // }];
            
           // [self.photoPopView returnTakingPhoto:^{
            //    [self photoByCamera];
            //    [self.photoPopView removeFromSuperview];
          //  }];
        }
    }
}
//调用相机
- (void)choosePhoto:(NSIndexPath *)indexPath{
    [self pushImagePickerController];
}
#pragma mark - ---------- UICollectionViewDelegate ----------
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.cell.collectionView) {
        return CGSizeMake(SCREEN_WIDTH/4-40, 60.0);
    }
    else{
        return  CGSizeMake(0, 0);
    }
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.cell.collectionView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    else{
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.cell.collectionView) {
        return 5;
    }
    else{
        return  0;
    }
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.cell.collectionView) {
        return 0;
    }
    else{
        return  0;
    }
}
#pragma mark - ---------- UICollectionViewDataSource ----------
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == self.cell.collectionView) {
        return 1;
    }
    
    else{
        return  0;
    }
}
#pragma mark --- TZImagePickerController ---

- (void)pushImagePickerController {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc]initWithMaxImagesCount:(1-self.photoDeleSource.count) delegate:self];
    imagePickerVc.navigationBar.barTintColor = RGB0X(0X098d7);
    /*这句话的意思就是将已经选择照片可以在被选择*/
    //    imagePickerVc.selectedAssets = self.photoDeleSource;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowTakePicture = NO;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark --- UIImagePickerController ---
- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.tabBar.hidden = NO;
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:@"public.image"]) {
        TZImagePickerController *tzImagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:(1-self.photoDeleSource.count) delegate:self];
        [tzImagePickerVc showProgressHUD];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        [[TZImageManager manager] savePhotoWithImage:image completion:^(NSError *error){
            if (error) {
                [tzImagePickerVc hideProgressHUD];
            } else {
                [[TZImageManager manager] getCameraRollAlbum:NO allowPickingImage:YES completion:^(TZAlbumModel *model) {
                    [[TZImageManager manager] getAssetsFromFetchResult:model.result allowPickingVideo:NO allowPickingImage:YES completion:^(NSArray<TZAssetModel *> *models) {
                        [tzImagePickerVc hideProgressHUD];
                        TZAssetModel *assetModel = [models firstObject];
                        if (tzImagePickerVc.sortAscendingByModificationDate) {
                            assetModel = [models lastObject];
                        }
                        [self.photoDeleSource addObject:assetModel.asset];
                        [self.photoSource addObject:image];
                        [self.cell.collectionView reloadData];
                    }];
                }];
            }
        }];
    }
}
#pragma mark --- TZImagePickerControllerDelegate ---

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if ([picker isKindOfClass:[UIImagePickerController class]]) {
        [picker dismissViewControllerAnimated:YES completion:nil];
        self.tabBarController.tabBar.hidden = NO;
    }
}
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    [self.photoSource addObjectsFromArray:photos];
    [self.photoDeleSource addObjectsFromArray:assets];
    /*如果按照下面这样写 就会让之前选中的图片不会在显示*/
    //    self.photoSource = [NSMutableArray arrayWithArray:photos];
    //    self.photoDeleSource = [NSMutableArray arrayWithArray:assets];
    [self.cell.collectionView reloadData];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingVideo:(UIImage *)coverImage sourceAssets:(id)asset {
    self.photoSource = [NSMutableArray arrayWithArray:@[coverImage]];
    self.photoDeleSource = [NSMutableArray arrayWithArray:@[asset]];
    [self.cell.collectionView reloadData];
    self.cell.collectionView.contentSize = CGSizeMake(0, ((self.photoSource.count + 2) / 3 ) * (_margin + _itemWH));
}

- (void)deleteBtnClik:(UIButton *)sender {
    
    NSLog(@"%@",self.photoSource);
    [self.photoSource enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSString class]]) {
            [self.deleSourse addObject:[self.photoSource objectAtIndex:sender.tag]];
        }
        else{
            
        }
    }];
    
    [self.photoSource removeObjectAtIndex:sender.tag];
    [self.photoDeleSource removeObjectAtIndex:sender.tag];
    
    //    [self.deleSourse addObject:sender.tag];
    /*做判断 如果是url类型的 也就是修改订单之前传过来的url就存起来*/
    
    NSLog(@"%@",self.deleSourse);
    [self.cell.collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self.cell.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self.cell.collectionView reloadData];
    }];
}

- (UIImagePickerController *)imagePickerController{
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.delegate = self;
        UIBarButtonItem *tzBarItem, *BarItem;
        if (iOS9Later) {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[TZImagePickerController class]]];
            BarItem = [UIBarButtonItem appearanceWhenContainedInInstancesOfClasses:@[[UIImagePickerController class]]];
        } else {
            tzBarItem = [UIBarButtonItem appearanceWhenContainedIn:[TZImagePickerController class], nil];
            BarItem = [UIBarButtonItem appearanceWhenContainedIn:[UIImagePickerController class], nil];
        }
        NSDictionary *titleTextAttributes = [tzBarItem titleTextAttributesForState:UIControlStateNormal];
        [BarItem setTitleTextAttributes:titleTextAttributes forState:UIControlStateNormal];
    }
    return _imagePickerController;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    /*需要根据手机尺寸来判断tableView的尺寸*/
    if (kIs_phone5) {
        self.tableView.contentSize=CGSizeMake(SCREEN_WIDTH, 650);
    }
    if (kIs_phone6) {
        self.tableViewHeight.constant = 800;
    }
    if (kIs_phone6plus) {
        self.tableViewHeight.constant = 800;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
