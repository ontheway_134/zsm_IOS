//
//  ICN_SetUserInfoViewController.h
//  ICan
//
//  Created by Lym on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_SetUserInfoViewController : BaseViewController

@property (assign, nonatomic) BOOL isFirst;

@property (copy, nonatomic) NSString *token;

@end
