//
//  ICN_SetUserInfoViewController.m
//  ICan
//
//  Created by Lym on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//
#define kApplicationShared  [UIApplication sharedApplication]
#define arcWitch [UIScreen mainScreen].bounds.size.width/2
#define arcHeight [UIScreen mainScreen].bounds.size.height/2
#define HRWitch [UIScreen mainScreen].bounds.size.width
#define HRHeight [UIScreen mainScreen].bounds.size.height
#import "ICN_SetUserInfoViewController.h"
#import "ICN_HeadImageTableViewCell.h"
#import "ICN_NickNameTableViewCell.h"
#import "ICN_GanderTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_UserSignTableViewCell.h"
#import "STPhotoKitController.h"
#import "UIImagePickerController+ST.h"
#import "NSObject+Unicode.h"
#import "ICN_HeadImageSelView.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_EducationView.h"
#import "ICN_JobConditionView.h"
#import "ICN_JobExperienceView.h"
#import "ICN_PositionDesiredView.h"
#import "ICN_LocalityView.h"
#import "ICN_DynLocationPickerView.h"
#import "HRNetworkingManager+ICN_Publication.h"
#import "HRNetworkingManager+DynFirstPager.h"
#import "ICN_Regular.h"
#import "NSString+Regular.h"

#import "ICN_HangYeView.h"

#import "ICN_SetUserInfoModel.h"
#import "ICN_LocationModel.h"
#import "ICN_MyWorkPositionModel.h"
#import "NSString+BlankString.h"
#import "SQJudgeInfomation.h"



#import "ViewController.h"

#import <Photos/Photos.h>
#import "UIImage+cutImage.h"
#import <objc/runtime.h>
#import <objc/message.h>
#ifdef DEBUG
#   define WKLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define WKLog(...)
#endif
@interface ICN_SetUserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate, STPhotoKitDelegate,birthdayDelegate,EducationDelegate,JobExperienceDelegate, PositionDesiredDelegate, UITextFieldDelegate,DynLocationPickerViewDelegate,HangYeViewDelegate>
{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *educationHighest;
    
    
    NSString *hopehangyeID;
    
    NSString *jobCondition;
    NSString *experience;
    NSString *PositionDesired;
    NSString *LocalityStr;
    ICN_NickNameTableViewCell *nameCell;
    
    NSString * typeStr;
    UIImage *tempImage;
    
    
    ICN_NickNameTableViewCell *emailCell;
    ICN_NickNameTableViewCell *phoneCell;
    ICN_UserSignTableViewCell *personalizeSignatureCell;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) ICN_HeadImageTableViewCell *headImageCell;
@property (strong, nonatomic) ICN_HeadImageSelView *changeHeadImageView;

@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (strong, nonatomic) ICN_EducationView *educationView;
@property (strong, nonatomic) ICN_HangYeView *hangyeView;
@property (strong, nonatomic) ICN_JobConditionView *jobView;
@property (strong, nonatomic) ICN_JobExperienceView *experienceView;
@property (strong, nonatomic) ICN_PositionDesiredView *PositionDesiredView;
@property (strong, nonatomic) ICN_LocalityView *LocalityView;
@property (strong,nonatomic) NSString  * nameID;/*最高学历的id*/
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSMutableArray *industryIdArrary;
@property (strong,nonatomic)NSString *hopehangye;;
@property (strong,nonatomic)NSString * studyStr;
@property(nonatomic,strong)UIImageView *imageview;

@property (nonatomic, strong) ICN_DynLocationPickerView *citySelectedView;

@property (nonatomic , strong)NSMutableArray <ICN_LocationModel *>* locationModelsArr;
@property (strong,nonatomic) NSString * againStr;/*当再次更改工作情况之后判断赋值的字符串*/

@end

@implementation ICN_SetUserInfoViewController
#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<ICN_LocationModel *> *)locationModelsArr{
    if (_locationModelsArr == nil) {
        _locationModelsArr = [NSMutableArray array];
    }
    return _locationModelsArr;
}

- (ICN_HeadImageSelView *)changeHeadImageView {
    if (!_changeHeadImageView)
    {
        _changeHeadImageView = [[NSBundle mainBundle] loadNibNamed:@"ICN_HeadImageSelView" owner:self options:nil].lastObject;
    }
    return _changeHeadImageView;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isFirst) {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configData];
    [self configUI];
    [self regeditCell];
    [self configDates];
    
    
   
    NSLog(@"%d",__LINE__);
    
    self.dict = [[NSMutableDictionary alloc] init];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void) configDates{
    //所在地
    [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            [self.locationModelsArr addObjectsFromArray:modelsArr];
        }
        
    } Failure:^(NSDictionary *errorInfo) {
    }];
    
    //期望职位
    //最高学历
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 5) {
        return 10;
    }
    else {
        return 0;
    }
}
#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3) {
        return 7;
    }
    else if (section == 4) {
        return 2;
    }
    else {
        return 1;
    }
}
#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 70;
    }
    else if (indexPath.section == 5) {
        return 110;
    }
    else {
        return 44;
    }
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    NSArray *sectionThreeArr = @[@"出生年月",@"最高学历",@"所属行业",@"工作情况",@"工作经验",@"期望职位",@"所在地"];
    if (indexPath.section == 0) {
        self.headImageCell = [self.tableView
                              dequeueReusableCellWithIdentifier:@"ICN_HeadImageTableViewCell"
                              forIndexPath:indexPath];
        self.headImageCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        self.headImageCell.model = model;
        
        if (tempImage) {
            self.headImageCell.headImageView.image = tempImage;
        } else {
            [self.headImageCell.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }
        self.headImageCell.headImageView.layer.masksToBounds = YES;
        self.headImageCell.headImageView.layer.cornerRadius = self.headImageCell.headImageView.bounds.size.width*0.5;
        return self.headImageCell;
    }
    else if (indexPath.section == 1) {
        nameCell = [self.tableView
                    dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                    forIndexPath:indexPath];
        if (nameCell.rightTextField.text.length == 0) {
            ICN_SetUserInfoModel *model = self.dataArr.firstObject;
            nameCell.rightTextField.text = model.memberNick;
        }
        nameCell.leftLabel.text = @"姓名";
        return nameCell;
    }
    else if (indexPath.section == 2) {
        ICN_GanderTableViewCell *cell = [self.tableView
                                         dequeueReusableCellWithIdentifier:@"ICN_GanderTableViewCell"
                                         forIndexPath:indexPath];
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        
        if ([model.memberGender isEqualToString:@"1"]) {
            cell.manButton.selected = YES;
            cell.womenButton.selected = NO;
        } else {
            cell.womenButton.selected = YES;
            cell.manButton.selected = NO;
        }
        
        
        if ([[USERDEFAULT valueForKey:@"memberGender"] isEqualToString:@"1"]) {
            
            cell.manButton.selected = YES;
            cell.womenButton.selected = NO;
        } else if ([[USERDEFAULT valueForKey:@"memberGender"]  isEqualToString:@"2"]) {
            
            cell.womenButton.selected = YES;
            cell.manButton.selected = NO;
        }
        
        
        return cell;
        
    }
    else  if (indexPath.section == 3) {
        ICN_NoImageTableCell *cell = [self.tableView
                                      dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell"
                                      forIndexPath:indexPath];
        cell.leftLabel.text = sectionThreeArr[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        cell.model = model;
        if (indexPath.row == 0) {
            
            cell.rightLabel.text = tempBirthdayStr.length == 0 ? model.memberBirthday : tempBirthdayStr;
        }
        if (indexPath.row == 1) {
            if (self.studyStr == nil) {
                cell.rightLabel.text = model.memberQualificationName;
            }else{
            cell.rightLabel.text =  self.studyStr;
                
            
                
            }
        }
        if (indexPath.row == 2) {
            NSLog(@"%@",self.hopehangye);
            
            cell.rightLabel.text = self.hopehangye;

        }
        if (indexPath.row == 3) {
            
            if ([model.workStatus isEqualToString:@"1"]) {
                if (model.companyName == NULL || model.memberPosition == NULL) {
                    cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                } else {
//                    cell.rightLabel.text = [NSString stringWithFormat:@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text];
                    cell.rightLabel.text = [NSString stringWithFormat:@"%@|%@",model.companyName,model.memberPosition];
//                    NSLog(@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text);
                    if ( [self.againStr integerValue] == 1){
                        NSLog(@"%@",jobCondition);
                        cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                    }
                    if ([self.againStr integerValue] == 2){
                        cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                    }
                }
            }else if ([model.workStatus isEqualToString:@"0"]) {
                if (model.memberSchool == NULL || model.memberMajor == NULL) {
                    cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                } else {
                    cell.rightLabel.text = [NSString stringWithFormat:@"%@||%@",model.memberSchool,model.memberMajor];
                    NSLog(@"%@||%@",model.memberSchool,model.memberMajor);
                    if ( [self.againStr integerValue] == 1){
                        NSLog(@"%@",jobCondition);
                        cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                    }
                    if ([self.againStr integerValue] == 2){
                        cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
                    }
                }
            }
            else{
                cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
            }
            
        }
        if (indexPath.row == 4) {

            cell.rightLabel.text = experience == 0 ? model.workExperienceName : experience;
        }
        if (indexPath.row == 5) {
            cell.rightLabel.text = PositionDesired == 0 ? model.hopePositionName : PositionDesired;
        }
        if (indexPath.row == 6) {
            cell.rightLabel.text = LocalityStr == 0 ? SF(@"%@ %@", model.provinceName, model.cityName) : LocalityStr;
            if (model == nil && LocalityStr.length == 0) {
                cell.rightLabel.text = @"";
            }
        }
        return cell;
    }
    
    else  if (indexPath.section == 4) {

        if (indexPath.row == 0) {
            emailCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                                               forIndexPath:indexPath];
            emailCell.leftLabel.text = @"邮箱";
            if (emailCell.rightTextField.text.length == 0) {
                ICN_SetUserInfoModel *model = self.dataArr.firstObject;
                emailCell.rightTextField.text = model.memberEmail;
            }
            return emailCell;
        }else {
            phoneCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                                               forIndexPath:indexPath];
            phoneCell.leftLabel.text = @"手机";
            if (phoneCell.rightTextField.text.length == 0) {
                ICN_SetUserInfoModel *model = self.dataArr.firstObject;
                phoneCell.rightTextField.text = model.memberMobile;
                NSLog(@"%@",model);
                NSLog(@"%@",model.memberMobile);
            }
            
            return phoneCell;
            
        }
        
        
    }
    
    else {
        personalizeSignatureCell = [self.tableView
                                           dequeueReusableCellWithIdentifier:@"ICN_UserSignTableViewCell"
                                           forIndexPath:indexPath];
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        personalizeSignatureCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (personalizeSignatureCell.signTextView.text.length == 0 || [personalizeSignatureCell.signTextView.text isEqualToString:@"写个让自己个性的签名吧！"]) {
            personalizeSignatureCell.signTextView.text = model.personalizeSignature;
        }
        personalizeSignatureCell.numberLabel.text = SF(@"%lu/50",(unsigned long)personalizeSignatureCell.signTextView.text.length);
        return personalizeSignatureCell;
    }
    
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        [self editImageSelected];
    }
    else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            [self setUserBirthday];
            [self.view endEditing:YES];
        }else if (indexPath.row == 1) {
            //最高学历  加点注释啊 －－－－－－－－－－－－－－－－－－－－－－－－－－－－
            [self.view endEditing:YES];
            [self setUserEducation];
            
            [self.educationView RenturnEducationBtnClick:^(NSString * nameStr,NSString * nameID) {
                if (nameStr == nil) {
                    nameStr = @"小学";
                    [self.dict setObject:@"1" forKey:@"memberQualification"];
                }
                self.studyStr = nameStr;
                self.nameID = nameID;
                [self.dict setObject:self.nameID forKey:@"memberQualification"];
                
                [self.tableView reloadData];
            }];
        }else if (indexPath.row == 2){
            [self.view endEditing:YES];
            [self setUserHangYe];
            
        }else if (indexPath.row == 3){
            [self.view endEditing:YES];
            [self setUserJob];
            
        }else if (indexPath.row == 4) {
            [self.view endEditing:YES];
            //工作经验   －－－－－牛掰
            [self setUserJobExperience];
        }else if (indexPath.row == 5) {
            [self.view endEditing:YES];
            [self setUserPositionDesired];
        }else if (indexPath.row == 6) {
            [self.view endEditing:YES];
            [self setUserLocality];
        }
    }
}
#pragma mark - --- delegate 视图委托 ---
#pragma mark - 1.STPhotoKitDelegate的委托
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    NSLog(@"%s",__func__);
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSLog(@"%@",NSStringFromCGSize(image.size));
    /*当返回时候进行赋值 赋值可以经过剪切之后再赋值 还可以不剪切就赋值 剪切用上面这句 不剪切用下面那句*/
    UIImage *image2 = [UIImage imagewithImage:image];
    /*不剪切直接赋值*/
    //    UIImage *image2 = image;
    self.headImageCell.headImageView.image = image2;
}
#pragma mark 即将进入到第二个nav时进行调用
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    viewController.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];
    viewController.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    /*这一步需要判断 只要当照片选择器到了图片剪切的时候就调用 */
    if (navigationController.viewControllers.count == 3)
    {
        Method method = class_getInstanceMethod([self class], @selector(drawRect:));
        
        class_replaceMethod([[[[navigationController viewControllers][2].view subviews][1] subviews][0] class],@selector(drawRect:),method_getImplementation(method),method_getTypeEncoding(method));
    }
}


-(void)drawRect:(CGRect)rect
{
    CGContextRef ref = UIGraphicsGetCurrentContext();
    CGContextAddRect(ref, rect);
    CGContextAddArc(ref, [UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, arcWitch, 0, M_PI*2, NO);
    [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]setFill];
    /*设置挡板的颜色*/
    CGContextDrawPath(ref, kCGPathEOFill);
    
    CGContextAddArc(ref, [UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2, arcWitch, 0, M_PI*2, NO);
    /*圆外圈线的颜色*/
    [[UIColor blueColor]setStroke];
    CGContextStrokePath(ref);
}
#pragma mark - --- event response 事件相应 ---
- (void)editImageSelected
{
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
    
    [grayView addSubview:self.changeHeadImageView];
    [self.changeHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 160));
    }];
    self.changeHeadImageView.layer.cornerRadius = 8;
    
    [self.changeHeadImageView.takePhotoButton addAction:^(NSInteger tag) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                NSLog(@"Authorized");/*授权*/
                if (TARGET_IPHONE_SIMULATOR) {
                    NSLog(@"模拟器");
                    return ;
                }
                if (TARGET_OS_IPHONE) {
                    NSLog(@"真机");
                    /*移除视图*/
                    [grayView removeFromSuperview];
                    UIImagePickerController *pc = [[UIImagePickerController alloc]init];
                    pc.delegate = self;
                    [pc setSourceType:UIImagePickerControllerSourceTypeCamera];
                    [pc setModalPresentationStyle:UIModalPresentationFullScreen];
                    [pc setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
                    [pc setAllowsEditing:YES];
                    [self presentViewController:pc animated:YES completion:nil];
                    return ;
                }
            }else{
                NSLog(@"Denied or Restricted");/*限制*/
            }
        }];
    }];
    [self.changeHeadImageView.localAlbumButton addAction:^(NSInteger tag) {
        /*移除视图*/
        [grayView removeFromSuperview];
        UIImagePickerController *pc = [[UIImagePickerController alloc]init];
        pc.delegate = self;
        [pc setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [pc setModalPresentationStyle:UIModalPresentationFullScreen];
        [pc setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [pc setAllowsEditing:YES];
        [self presentViewController:pc animated:YES completion:nil];
    }];
    [self.changeHeadImageView.closeViewButton addAction:^(NSInteger tag) {
        [self dismissContactView];
    }];
}

- (void)dismissContactView {
    [grayView removeFromSuperview];
}
- (void)configUI {
    self.navigationItem.title = @"基本信息";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    _tableView=[[UITableView alloc]init];
    _tableView.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [self.view addSubview:_tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = RGB(235, 236, 237);
}
- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}
#pragma mark - ---------- 生日选择 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}
- (void)birthdayChoose {
    //    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:0 inSection:3];//获取cell的位置
    //    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    //    if (tempBirthdayStr == NULL) {
    //        tempBirthdayStr = @"1990年1月";
    //    }
    //    cell.rightLabel.text = tempBirthdayStr;
    if (tempBirthdayStr == nil) {
       // [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择生日"];
        tempBirthdayStr = @"1960年1月";
    }else {
        [self.dict setObject:tempBirthdayStr forKey:@"memberBirthday"];
    }
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1990年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
}
#pragma mark - ---------- 学历选择 ----------
- (void)setUserEducation{
    
    [self setGrayView];
    self.educationView = XIB(ICN_EducationView);
    self.educationView.delegate = self;
    self.educationView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.educationView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.educationView];
    [self.educationView.confirmButton addTarget:self action:@selector(educationChoose) forControlEvents:UIControlEventTouchUpInside];
}
- (void)educationChoose {
    if (educationHighest == NULL) {
        educationHighest = @"小学";
        
    }
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getEducation:(NSString *)education andID:(NSString *)ID{
    educationHighest = [NSString stringWithFormat:@"%@",education];
    if (educationHighest == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择学历"];
    }else {
        [self.dict setObject:ID forKey:@"memberQualification"];
    }
}
#pragma mark - ---------- 所属行业 ----------
- (void)setUserHangYe{
    
    [self setGrayView];
    self.hangyeView = XIB(ICN_HangYeView);
    self.hangyeView.delegate = self;
    self.hangyeView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.hangyeView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.hangyeView];
    [self.hangyeView.confirmButton addTarget:self action:@selector(hangYeChoose) forControlEvents:UIControlEventTouchUpInside];
  
}
- (void)hangYeChoose {
    if (self.hopehangye == NULL) {
        self.hopehangye = @"互联网/软件";
    }
    if (hopehangyeID == NULL) {
        hopehangyeID = @"59";
    }
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}

- (void)getHangYeView:(NSString *)HangYeView andID:(NSString *)ID {
    self.hopehangye = HangYeView;
    hopehangyeID = ID;
}

#pragma mark - ---------- 工作情况 ----------
- (void)setUserJob{
    
    [self setGrayView];
    self.jobView = XIB(ICN_JobConditionView);
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.jobView.cacelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    
    
    NSLog(@"%@",self.dataArr);
    ICN_SetUserInfoModel *model = self.dataArr.firstObject;
    if ([model.workStatus isEqualToString:@"0"]) {
        NSLog(@"未工作");
        self.jobView.outOfWorkButton.selected = YES;
        self.jobView.inWorkButton.selected = NO;
        
        self.jobView.companyTF.text = model.memberSchool;
        self.jobView.positionTF.text = model.memberMajor;
    }
    else if  ([model.workStatus isEqualToString:@"1"]) {
        NSLog(@"已工作");
        self.jobView.inWorkButton.selected = YES;
        self.jobView.outOfWorkButton.selected = NO;
        self.jobView.companyTF.text = model.companyName;
        self.jobView.positionTF.text = model.memberPosition;
    }
    [grayView addSubview:self.jobView];
    [self.jobView.confirmButton addTarget:self action:@selector(jobChoose) forControlEvents:UIControlEventTouchUpInside];
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


//当键盘出现或改变时调用
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163 - height, SCREEN_WIDTH, 163);
}

//当键退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification{
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
}

- (void)jobChoose{
    

    
    if (self.jobView.inWorkButton.selected == YES) {
        /*如果是已工作的情况下，需要判断当前填写的东西是不是空*/
        if ([self.jobView.companyTF.text isEqualToString:@"请输入公司名称"]||[self.jobView.companyTF.text isEqualToString:@"请输入所处职位"]||[self.jobView.companyTF.text isEqualToString:@""]||[self.jobView.companyTF.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入公司名和所处职位"];
            return;
        }
        
        jobCondition = [NSString stringWithFormat:@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text];
        NSString * str = @"1";
 //       memberSchool ;memberMajor
        [self.dict setObject:str forKey:@"workStatus"];
       
        [self.dict setObject:self.jobView.companyTF.text forKey:@"companyName"];
        [self.dict setObject:self.jobView.positionTF.text forKey:@"memberPosition"];
        /*1代表工作了*/
        self.againStr = @"1";
         NSLog(@"%@",self.dict);
        [grayView removeFromSuperview];
    }else if  (self.jobView.outOfWorkButton.selected == YES) {
        
        /*如果是已工作的情况下，需要判断当前填写的东西是不是空*/
        if ([self.jobView.companyTF.text isEqualToString:@"请输入学校名称"]||[self.jobView.companyTF.text isEqualToString:@"请输入所学专业"]||[self.jobView.companyTF.text isEqualToString:@""]||[self.jobView.companyTF.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入学校名称和所学专业"];
            return;
        }
        
        jobCondition = [NSString stringWithFormat:@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text];
        NSString *str = @"0";
        [self.dict setObject:self.jobView.companyTF.text forKey:@"memberSchool"];
        [self.dict setObject:self.jobView.positionTF.text forKey:@"memberMajor"];
        /*2代表未工作*/
        self.againStr = @"2";
        [self.dict setObject:str forKey:@"workStatus"];
        NSLog(@"%@",self.dict);
        [grayView removeFromSuperview];
    }
    
    [self.tableView reloadData];
    
    
}

#pragma mark - ---------- 工作经验 ----------
- (void)setUserJobExperience{
    [self setGrayView];
    self.experienceView = XIB(ICN_JobExperienceView);
    self.experienceView.delegate = self;
    self.experienceView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.experienceView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.experienceView];
    [self.experienceView.confirmButton addTarget:self action:@selector(JobExperienceChoose) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
- (void)JobExperienceChoose {
    //    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:3 inSection:3];//获取cell的位置
    //    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (experience == NULL) {
        experience = @"在读";
    }
    
    //[self.dict setObject:cell.rightLabel.text forKey:@"workExperience"];
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getJobExperiencen:(NSString *)JobExperience andID:(NSString *)ID{
    
    if (experience == NULL) {
        experience = @"在读";
    }
    
    experience = [NSString stringWithFormat:@"%@",JobExperience];
    WKLog(@"%@",ID);
    [self.dict setObject:ID forKey:@"workExperience"];
    
}
#pragma mark - ---------- 期望职位 ----------
- (void)setUserPositionDesired{
    [self setGrayView];
    self.PositionDesiredView = XIB(ICN_PositionDesiredView);
    self.PositionDesiredView.delegate = self;
    self.PositionDesiredView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.PositionDesiredView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.PositionDesiredView];
    [self.PositionDesiredView.savePositionDesiredButton addTarget:self action:@selector(PositionDesiredChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)PositionDesiredChoose {
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}

- (void)getPositionDesired:(NSString *)industryId andpositioId:(NSString *)positioId andworkId:(NSString *)workId andID:(NSString *)ID{
    
    NSLog(@"6666666666666666666666666%@", workId);
    PositionDesired = SF(@"%@", workId);
    [self.dict setObject:ID forKey:@"hopePosition"];
    
}
#pragma mark - ---------- 所在地 ----------
- (void)setUserLocality{
    [self setGrayView];
    CGRect frame = SCREEN_BOUNDS;
    frame.size.height = 161.5;
    _citySelectedView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
    frame.origin.y = SCREEN_HEIGHT - frame.size.height;
    _citySelectedView.locationsArr = self.locationModelsArr;
    _citySelectedView.frame = frame;
    _citySelectedView.delegate = self;
    [grayView addSubview:_citySelectedView];
    
    if (_locationModelsArr == nil || self.locationModelsArr.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未获取到地理信息列表"];
        [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
            if (modelsArr.firstObject.code == 0) {
                [self.locationModelsArr addObjectsFromArray:modelsArr];
            }
        } Failure:nil];
    }

    
}


//代理方法
#pragma mark --- DynLocationPickerViewDelegate ---

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (success) {

        // 修改当前显示的城市
        for (ICN_LocationModel *localModel in self.locationModelsArr) {
            for (ICN_CityDetialModel *detialModel in localModel.list) {
                if (detialModel.cityId.integerValue == code) {
                    LocalityStr = SF(@"%@ %@",localModel.className , detialModel.className);
                    
                    [self.dict setObject:localModel.provinceId forKey:@"province"];
                    [self.dict setObject:detialModel.cityId forKey:@"city"];
                    
                    [self.tableView reloadData];
                }
            }
        }
        
    }else{
        LocalityStr = SF(@"%@ %@",@"北京市" , @"北京市");
        
        [self.dict setObject:@"110000" forKey:@"province"];
        [self.dict setObject:@"110100" forKey:@"city"];
        [self.tableView reloadData];
    }
    
    [grayView removeFromSuperview];
}


//处理数据
- (void)configData{
    self.hopehangye = @"";
    if (!_isFirst) {
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
            _dataArr = [[NSMutableArray alloc]init];
            ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:result[@"result"] error:nil];
            [_dataArr addObject:model];
            NSLog(@"%@",_dataArr);
            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"刷新中"];
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/Api/Company/getIndustry" params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (![result[@"result"] isEqual:[NSNull null]]) {
                    if ((self.hopehangye = NULL)) {
                        
                        self.hopehangye = @"";
                    }
                    
                    self.hopehangye = SF(@"%@",result[@"result"][@"classname"]);
                    
                    NSLog(@"%@",self.hopehangye);
                    
                }
                [_tableView reloadData];
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
            }];
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }

}
- (void)regeditCell {
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_HeadImageTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_HeadImageTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_NickNameTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_NickNameTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_GanderTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_GanderTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_NoImageTableCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_NoImageTableCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_UserSignTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_UserSignTableViewCell"];
}
//保存
- (void)saveAction{
    
    // 1. 校验各种基本信息填写是否完善
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        [self.dict setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    } else {
        // 没有用户信息直接退出
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"用户不合法"];
        [self.navigationController popViewControllerAnimated:YES];
    }
    // 校验用户姓名是否填完
    if (nameCell.rightTextField.text.length != 0) {
        [self.dict setValue:nameCell.rightTextField.text forKey:@"memberNick"];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写用户名"];
        return ;
    }
    if ([USERDEFAULT objectForKey:@"memberGender"] != nil) {
        [self.dict setObject:[USERDEFAULT objectForKey:@"memberGender"] forKey:@"memberGender"];
    }
    if (personalizeSignatureCell.signTextView.text.length != 0) {
        [self.dict setValue:personalizeSignatureCell.signTextView.text forKey:@"personalizeSignature"];
    }
    //判断邮箱
    if (emailCell.rightTextField.text.length != 0) {
        if ([emailCell.rightTextField.text checkEmail]) {
             [self.dict setObject:emailCell.rightTextField.text forKey:@"memberEmail"];
        } else {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的邮箱"];
            return;
        }
    }
    // 校验用户手机号
//    if ([phoneCell.rightTextField.text isBlankString]){
//        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"手机号码不能为空"];
//        return;
//    }
    if (![SQJudgeInfomation isMobileNumber:phoneCell.rightTextField.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的手机号"];
        return;
    }
    // 将用户手机号存到数据字典中
    [self.dict setObject:phoneCell.rightTextField.text forKey:@"memberMobile"];
    NSLog(@"%@",self.dict);
    // 存在图片的话走图片的网络请求
#warning mark -- I have no idea why there is two solution
    NSLog(@"%@",self.headImageCell.headImageView.image);
    if (self.headImageCell.headImageView.image)
    {   [MBProgressHUD ShowProgressToSuperView:self.view Message:@"数据上传中"];
//        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"图片上传中"];
        [HRNetworkingManager uploadImagesWithImage:self.headImageCell.headImageView.image Success:^(ICN_CommonImagesLoadModel *model) {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            NSString *photoStr = model.result[@"fileName"];
            // 在数据字典中填充图片数组的地址
            [self.dict setObject:photoStr forKey:@"memberLogo"];
            // 获取到图片地址之后上传数据资料 -- 成功后修正 信息补全状态
            [self requestToSaveUserBaseMessage];
        } Failure:^(NSDictionary *errorInfo) {
            HRLog(@"%@",errorInfo);
        }];
    }
    else {
        // 没有图片相关内容直接上传数据资料
        [self requestToSaveUserBaseMessage];
    }
    
}

// 根据数据字典保存基本信息的网络请求
- (void)requestToSaveUserBaseMessage{
    NSLog(@"%@",self.dict);
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySaveTheMessage params:self.dict success:^(id result) {
        
        
        // 数据保存成功，修正用户信息校验的值
        [USERDEFAULT setObject:@"0" forKey:@"loginStatus"];
        
        //如果填写了所属行业 则走一下保存行业数据的接口
        if (hopehangyeID.length != 0) {
            NSMutableDictionary *hangyeDic = [NSMutableDictionary dictionary];
            [hangyeDic setObject:hopehangyeID forKey:@"id"];
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                [hangyeDic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
            } else {
                [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"用户不合法"];
                [self.navigationController popViewControllerAnimated:YES];
                return ;
            }
            NSLog(@"%@",hangyeDic);
            // 保存行业数据PATH_MySaveIndustry
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySaveIndustry params:hangyeDic success:^(id result) {
                // 成功之后返回上级页面
                [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"保存成功"];
                [self.navigationController popViewControllerAnimated:YES];
                
            } failure:^(NSDictionary *errorInfo) {
                HRLog(@"%@",errorInfo);
            }];
            
        } else {
            
            [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:@"保存成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"%@",errorInfo);
        
    }];
}


//手机号码的正则表达式
- (BOOL)isValidateMobile:(NSString *)mobile{
    //手机号以13、15、18开头，八个\d数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

//邮箱地址的正则表达式
- (BOOL)isValidateEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


@end
