//
//  ICN_SetUserInfoModel.h
//  ICan
//
//  Created by apple on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_SetUserInfoModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberLogo;//用户头像
@property (nonatomic, copy) NSString *memberNick;//用户名称
@property (nonatomic, copy) NSString *memberGender;//用户性别
@property (nonatomic, copy) NSString *memberBirthday;//出生年月
@property (nonatomic, copy) NSString *memberQualification;//最高学历
@property (nonatomic, copy) NSString *memberQualificationName;//学历
@property (nonatomic, copy) NSString *memberSchool;

@property (nonatomic, copy) NSString *workExperience;//工作经验
@property (nonatomic, copy) NSString *workExperienceName;

@property (nonatomic, copy) NSString *workStatus;//是否工作
@property (nonatomic, copy) NSString *companyName;//公司名称
@property (nonatomic, copy) NSString *memberPosition;//用户职位
@property (nonatomic, copy) NSString *hopePosition;//期望职位
@property (nonatomic, copy) NSString *province;//省份
@property (nonatomic, copy) NSString *provinceName;

@property (nonatomic, copy) NSString *city;//城市
@property (nonatomic, copy) NSString *cityName;//城市
@property (nonatomic, copy) NSString *hopePositionName;
@property (nonatomic, copy) NSString *memberEmail;//用户邮箱
@property (nonatomic, copy) NSString *memberMobile;//用户电话
@property (nonatomic, copy) NSString *personalizeSignature;//个性签名
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *memberMajor;//专业





@end
