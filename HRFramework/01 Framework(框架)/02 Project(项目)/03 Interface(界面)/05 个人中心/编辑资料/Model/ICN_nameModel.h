//
//  ICN_nameModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/23.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_nameModel : BaseOptionalModel
@property (nonatomic, copy) NSString *nameID;//用户邮箱
@property (nonatomic, copy) NSString *name;//用户电话
@end
