//
//  ICN_MyWorkPositionModel.h
//  ICan
//
//  Created by apple on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyWorkPositionModel : BaseOptionalModel
@property (nonatomic, copy) NSString *industryId;//行业
@property (nonatomic, copy) NSString *positioId;//职位
@property (nonatomic, copy) NSString *workId;


@end
