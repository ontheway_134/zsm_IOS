//
//  ICN_nameModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/23.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_nameModel.h"

@implementation ICN_nameModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"nameID" : @"id"}];
}
@end
