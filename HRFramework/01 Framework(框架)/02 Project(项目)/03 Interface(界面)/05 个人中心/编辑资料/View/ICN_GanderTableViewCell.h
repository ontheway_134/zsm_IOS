//
//  ICN_GanderTableViewCell.h
//  ICan
//
//  Created by Lym on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_SetUserInfoModel;
@interface ICN_GanderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *womenButton;
@property (weak, nonatomic) IBOutlet UIButton *manButton;
@property (weak, nonatomic) IBOutlet UILabel *manLabel;
@property (weak, nonatomic) IBOutlet UILabel *womenLabel;
@property (nonatomic, strong)ICN_SetUserInfoModel *modelOfGander;

@end
