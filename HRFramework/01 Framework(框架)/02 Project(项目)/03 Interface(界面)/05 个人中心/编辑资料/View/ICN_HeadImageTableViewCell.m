//
//  UBI_HeadImageTableViewCell.m
//  UbiTalk
//
//  Created by Lym on 2016/11/30.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import "ICN_HeadImageTableViewCell.h"
#import "ICN_SetUserInfoModel.h"
@implementation ICN_HeadImageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImageView.layer.cornerRadius = self.headImageView.frame.size.width * 0.5;
    self.headImageView.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setModel:(ICN_SetUserInfoModel *)model{

    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
}
@end
