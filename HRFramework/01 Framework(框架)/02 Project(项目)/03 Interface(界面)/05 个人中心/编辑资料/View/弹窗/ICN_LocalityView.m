//
//  ICN_LocalityView.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_LocalityView.h"
#import "ICN_LocationModel.h"

@interface ICN_LocalityView()
@property (nonatomic , strong) ICN_LocationModel *selectedModel;

@property (nonatomic , assign)NSInteger selectCityCode; // 选中城市的代码



@end





@implementation ICN_LocalityView





- (void)awakeFromNib {
    [super awakeFromNib];
    self.LocalityPickerView.delegate = self;
    self.LocalityPickerView.dataSource = self;
    self.provinceArr = [NSMutableArray array];
    self.provinceArr = @[@"北京", @"天津", @"河北"];
    
    
    self.cityArr = [NSMutableArray array];
    self.cityArr = @[@"北京", @"天津", @"河北"];
    
//        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"];
//    
//    
//      NSDictionary *areaDic = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
//    
//        NSArray *components = [areaDic allKeys];
//        NSArray *sortedArray = [components sortedArrayUsingComparator: ^(id obj1, id obj2) {
//    
//            if ([obj1 integerValue] > [obj2 integerValue]) {
//                return (NSComparisonResult)NSOrderedDescending;
//            }
//    
//            if ([obj1 integerValue] < [obj2 integerValue]) {
//                return (NSComparisonResult)NSOrderedAscending;
//            }
//            return (NSComparisonResult)NSOrderedSame;
//        }];
//    
//        NSMutableArray *provinceTmp = [[NSMutableArray alloc] init];
//        for (int i=0; i<[sortedArray count]; i++) {
//            NSString *index = [sortedArray objectAtIndex:i];
//            NSArray *tmp = [[areaDic objectForKey: index] allKeys];
//            [provinceTmp addObject: [tmp objectAtIndex:0]];
//        }
//    
//        self.provinceArr = [[NSArray alloc] initWithArray: provinceTmp].mutableCopy;
//    
//    
//        NSString *index = [sortedArray objectAtIndex:0];
//        NSString *selected = [self.provinceArr objectAtIndex:0];
//        NSDictionary *dic = [NSDictionary dictionaryWithDictionary: [[areaDic objectForKey:index]objectForKey:selected]];
//    
//        NSArray *cityArray = [dic allKeys];
//        NSDictionary *cityDic = [NSDictionary dictionaryWithDictionary: [dic objectForKey: [cityArray objectAtIndex:0]]];
//        self.cityArr = [[NSArray alloc] initWithArray: [cityDic allKeys]].mutableCopy;
//
//
    
}
- (void)setLocationsArr:(NSArray *)locationsArr{
    _provinceArr = locationsArr;
    self.selectedModel = self.provinceArr.firstObject;
}


#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        return self.provinceArr.count;
    }
    else {
        return self.selectedModel.list.count;
    }
}
#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (component == 0) {
        ICN_LocationModel *model = self.provinceArr[row];
        return model.className;
    }else{
        return [self.selectedModel.list[row] className];
    }
    
    return nil;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        self.selectedModel = self.provinceArr[row];
        [self. LocalityPickerView reloadComponent:1];
        self.provinceStr = self.provinceArr[row];
    }
    else {
        ICN_CityDetialModel *model = self.selectedModel.list[row];
        self.selectCityCode = model.cityId.integerValue;
    }

    [self.delegate getLocality:self.provinceStr andcity:self.cityStr];
}

@end
