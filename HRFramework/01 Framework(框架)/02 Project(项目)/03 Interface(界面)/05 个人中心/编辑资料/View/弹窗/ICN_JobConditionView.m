//
//  ICN_JobConditionView.m
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_JobConditionView.h"

@implementation ICN_JobConditionView
//- (id)init{
//    /*最开始公司是默认选项 */
//
//}
- (void)awakeFromNib{
    [super awakeFromNib];
//    self.inWorkButton.selected = YES;
//    self.outOfWorkButton.selected = NO;
}
- (IBAction)userWork:(UIButton *)sender {
//    sender.selected = !sender.selected;
    if (sender.tag == 1) {
        self.inWorkButton.selected = YES;
        self.outOfWorkButton.selected = NO;
        self.companyTF.placeholder = @"请输入公司名";
        self.positionTF.placeholder = @"请输入所处职位";
    } else {
        self.outOfWorkButton.selected = YES;
        self.inWorkButton.selected = NO;
        self.companyTF.placeholder = @"请输入学校名称";
        self.positionTF.placeholder = @"请输入所学专业";
        
    }
}
@end
