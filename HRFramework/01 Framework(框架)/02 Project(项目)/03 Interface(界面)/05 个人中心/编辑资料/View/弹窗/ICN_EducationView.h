//
//  ICN_EducationView.h
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^RenturnEducationBtnClick)(NSString * nameStr,NSString * nameID);
@class ICN_MyEducationChoiceModel;


@protocol EducationDelegate <NSObject>

- (void)getEducation:(NSString *)education andID:(NSString *)ID temp:(NSString*)strID;

@end
@interface ICN_EducationView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>

@property (nonatomic , copy)NSString *qualificationStr; // 学历字段

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;

@property (nonatomic, strong) NSMutableArray *arrayOfEducation;
@property (copy, nonatomic) NSString *educationArr;
@property (strong,nonatomic)NSString * tempID;
@property (weak, nonatomic) id<EducationDelegate> delegate;
@property (nonatomic, strong)ICN_MyEducationChoiceModel *model;

-(void)RenturnEducationBtnClick:(RenturnEducationBtnClick)block;
@property (strong,nonatomic)RenturnEducationBtnClick renturnEducationBtnClick;

@end
