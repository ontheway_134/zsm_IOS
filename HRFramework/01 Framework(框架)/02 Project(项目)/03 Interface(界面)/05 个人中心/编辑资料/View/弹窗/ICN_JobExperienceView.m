//
//  ICN_JobExperienceView.m
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_JobExperienceView.h"
#import "ICN_ educationModel.h"
static NSString *const PATH_getWorkExperienceList = @"Member/Common/getWorkExperienceList";
@implementation ICN_JobExperienceView

- (void)awakeFromNib{
    
    [super awakeFromNib];
    self.pickView.delegate = self;
    self.pickView.dataSource = self;
    self.arrayOfJobExperience = [NSMutableArray array];
//    self.arrayOfJobExperience = @[@"在读", @"应届生", @"1-3年", @"3-5年", @"5-10年" , @"10年以上"].mutableCopy;
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Common/getWorkExperienceList" params:@{} success:^(id result) {
//        
//        NSLog(@"Wwwwwwwwww%@", result);
//        
//        NSArray *arr = result[@"result"];
//        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//            ICN__educationModel *model =  [[ICN__educationModel alloc]initWithDictionary:obj error:nil];
//            [self.arrayOfJobExperience addObject:model];
//            [self.pickView reloadAllComponents];
//        }];
//        
//        
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [HTTPManager POST_PATH:PATH_getWorkExperienceList params:nil success:^(id responseObject) {
        NSLog(@"Wwwwwwwwww%@", responseObject);
        
        NSArray *arr = responseObject;
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN__educationModel *model =  [[ICN__educationModel alloc]initWithDictionary:obj error:nil];
            [self.arrayOfJobExperience addObject:model];
            [self.pickView reloadAllComponents];
        }];
        
        ICN__educationModel *model = self.arrayOfJobExperience[0];
        
        
        
        self.JobExperienceArr = model.name;
        self.strID = model.ID;
        NSLog(@"%@ %@",model.name,model.ID);
        [self.pickView reloadAllComponents];
        [self.delegate getJobExperiencen:self.JobExperienceArr andID:self.strID];

       // [self.delegate getJobExperiencen:self.JobExperienceArr andID:model.ID];
    } failure:^(NSError *error) {
        
    }];
}
#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.arrayOfJobExperience.count;
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ICN__educationModel *model = self.arrayOfJobExperience[row];
    
    return model.name;

    
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    ICN__educationModel *model = self.arrayOfJobExperience[row];
    self.JobExperienceArr = model.name;
    self.strID = model.ID;
    [self.delegate getJobExperiencen:self.JobExperienceArr andID:self.strID];
}

@end
