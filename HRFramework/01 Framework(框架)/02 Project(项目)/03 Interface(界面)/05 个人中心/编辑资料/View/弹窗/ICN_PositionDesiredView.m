//
//  ICN_PositionDesiredView.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionDesiredView.h"
#import "ICN_workExpectationModel.h"
@interface ICN_PositionDesiredView()



@property (nonatomic,strong)NSMutableArray *arr1;

@property (nonatomic,assign)NSInteger num1;
@property (nonatomic,assign)NSInteger num2;
@end
@implementation ICN_PositionDesiredView
- (void)awakeFromNib {
    [super awakeFromNib];
    self.PositionDesiredPickerView.delegate = self;
    self.PositionDesiredPickerView.dataSource = self;
    
    self.num1 = 0;
    self.num2 = 0;
    
   
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Enterprise/EnterprisePosition/doPositionLinkage" params:@{} success:^(id result) {
        self.arr1 = [NSMutableArray array];
        NSArray *arr = result[@"result"];
        NSLog(@"Wwwwwwwwww%@", result);
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_workExpectationModel *model = [[ICN_workExpectationModel alloc]initWithDictionary:obj error:nil];
            [self.arr1 addObject:model];

        }];
        
        ICN_workExpectationModel *model = self.arr1[0];
        
        workExpectationModel *model1 = model.list[0];
        
        self.industryId = model.className;
        self.positioId = model1.className;
        

        if (model1.list.count > 0) {
            expectationModel *model2 = model1.list[0];
            self.workId = model2.className;
            self.ymId = model2.ID;
            [self.delegate getPositionDesired:self.industryId andpositioId:self.positioId andworkId:self.workId andID:model2.ID];
        }

        NSLog(@"%ld",self.arr1.count);
        [self.PositionDesiredPickerView reloadAllComponents];
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    

}


#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        NSLog(@"%ld",self.arr1.count);
        return self.arr1.count;
    }
    else if (component == 1) {
        ICN_workExpectationModel *model = self.arr1[_num1];
        return model.list.count;
    } else {
        
        ICN_workExpectationModel *model = self.arr1[_num1];
        workExpectationModel *model1 = model.list[_num2];
        if (model1.list > 0) {
            return model1.list.count;
        }else
        {
            return 0;
        }
        
       
    }
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    if (component == 0) {
        ICN_workExpectationModel *model = self.arr1[row];
        
        return model.className;

        //return self.industryIdArr[row];
    }
    else if (component == 1) {
        ICN_workExpectationModel *model = self.arr1[_num1];
        workExpectationModel * model1 = model.list[row];
        
        

        return model1.className;
    }else {
        ICN_workExpectationModel *model = self.arr1[_num1];
        workExpectationModel * model1 = model.list[_num2];
        expectationModel *model2 = model1.list[row];
        
        return model2.className;
        //return self.workIdArr[row];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        ICN_workExpectationModel *model = self.arr1[row];
        self.industryId = model.className;
        workExpectationModel * model1 = model.list[0];
        self.positioId = model1.className;
        expectationModel *model2 = model1.list[0];
        self.workId = model2.className;
        self.ymId = model2.ID;
        self.num1 = row;
        [self.PositionDesiredPickerView reloadComponent:1];
        [self.PositionDesiredPickerView reloadComponent:2];
    }
    else if (component == 1){
        ICN_workExpectationModel *model = self.arr1[_num1];
        workExpectationModel * model1 = model.list[row];
        
        expectationModel *model2 = model1.list[0];
        self.workId = model2.className;
        self.positioId = model1.className;
        self.ymId = model2.ID;
         self.num2 = row;
        [self.PositionDesiredPickerView reloadComponent:2];
    } else {
        ICN_workExpectationModel *model = self.arr1[_num1];
        workExpectationModel * model1 = model.list[_num2];
        expectationModel *model2 = model1.list[row];
        self.workId = model2.className;
        self.ymId = model2.ID;
    }
    
    [self.delegate getPositionDesired:self.industryId andpositioId:self.positioId andworkId:self.workId andID:self.ymId];
    
    
}

@end

