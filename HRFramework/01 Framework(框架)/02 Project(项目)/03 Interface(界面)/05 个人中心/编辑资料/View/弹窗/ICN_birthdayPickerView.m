//
//  ICN_birthdayPickerView.m
//  ICan
//
//  Created by Lym on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_birthdayPickerView.h"

@implementation ICN_birthdayPickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.birthdayPickerView.delegate = self;
    self.birthdayPickerView.dataSource = self;
    
    self.yearArr = [NSMutableArray array];
    for (int i=1960; i<2018; i++) {
        [self.yearArr addObject:[NSString stringWithFormat:@"%d年",i]];
    }
    self.monthArr = [NSMutableArray array];
    for (int i=1; i<13; i++) {
        [self.monthArr addObject:[NSString stringWithFormat:@"%d月",i]];
    }
    NSLog(@"%@",self.yearArr);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (_controlYearArr) {
        self.yearArr = [NSMutableArray arrayWithArray:_controlYearArr];
    }
    NSLog(@"%@",self.controlYearArr);
}

#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0) {
        if (_lineNumber > 0) {
            return _lineNumber;
        }
        return 58;
    }
    else {
        return 12;
    }
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSLog(@"%@",self.yearArr);
    if (component == 0) {
        return self.yearArr[row];
    }
    else {
        return self.monthArr[row];
    }
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        self.yaerStr = self.yearArr[row];
    }
    else {
        self.monthStr = self.monthArr[row];
    }
    [self.delegate getBirthdayYear:self.yaerStr andMonth:self.monthStr];
}

@end
