//
//  ICN_birthdayPickerView.h
//  ICan
//
//  Created by Lym on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol birthdayDelegate <NSObject>

- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month;

@end

@interface ICN_birthdayPickerView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *saveBirthdayButton;
@property (weak, nonatomic) IBOutlet UIPickerView *birthdayPickerView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (strong, nonatomic) NSMutableArray *yearArr;
@property (strong, nonatomic) NSMutableArray *monthArr;

@property (copy, nonatomic) NSString *yaerStr;
@property (copy, nonatomic) NSString *monthStr;

@property (weak, nonatomic) id<birthdayDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *controlYearArr;
@property (assign, nonatomic) NSInteger lineNumber;

@end
