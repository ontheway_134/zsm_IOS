//
//  ICN_JobConditionView.h
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol JobConditionDelegate <NSObject>

- (void)getJobCondition:(NSString *)company andposition:(NSString *)position;

@end

@interface ICN_JobConditionView : UIView
@property (weak, nonatomic) IBOutlet UIButton *inWorkButton;
@property (weak, nonatomic) IBOutlet UIButton *outOfWorkButton;
@property (weak, nonatomic) IBOutlet UIButton *cacelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *positionTF;








@end
