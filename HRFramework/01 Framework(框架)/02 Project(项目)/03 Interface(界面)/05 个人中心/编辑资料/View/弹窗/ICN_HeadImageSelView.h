//
//  UBI_HeadImageSelView.h
//  UbiTalk
//
//  Created by Lym on 2016/12/5.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_HeadImageSelView : UIView
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *localAlbumButton;
@property (weak, nonatomic) IBOutlet UIButton *closeViewButton;

@end
