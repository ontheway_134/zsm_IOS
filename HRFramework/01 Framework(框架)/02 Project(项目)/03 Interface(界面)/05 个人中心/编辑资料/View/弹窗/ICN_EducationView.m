//
//  ICN_EducationView.m
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EducationView.h"
#import "ICN_MyEducationChoiceModel.h"
#import "ICN_ educationModel.h"
@implementation ICN_EducationView

- (void)awakeFromNib{

    [super awakeFromNib];
    self.pickView.delegate = self;
    self.pickView.dataSource = self;
    self.arrayOfEducation = [NSMutableArray array];
//    self.arrayOfEducation = @[@"小学", @"初中", @"职高",@"高中", @"中专", @"专科", @"本科", @"硕士", @"博士"].mutableCopy;
    
    //http://1ican.com/index.php/Member/Common/getQualificationList
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Common/getQualificationList" params:@{} success:^(id result) {
        
        NSLog(@"Wwwwwwwwww%@", result);
        
        NSArray *arr = result[@"result"];

        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
          ICN__educationModel *model =  [[ICN__educationModel alloc]initWithDictionary:obj error:nil];
            [self.arrayOfEducation addObject:model];
        }];
        [self.pickView reloadAllComponents];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];



}
#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return self.arrayOfEducation.count;
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ICN__educationModel *model = self.arrayOfEducation[row];
    
    return model.name;

}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    ICN__educationModel *model = self.arrayOfEducation[row];
    self.educationArr = model.name;
    self.tempID = model.ID;
    NSLog(@"%@",self.tempID);
    if ([self.delegate respondsToSelector:@selector(getEducation:andID:temp:)]) {
        [self.delegate getEducation:self.educationArr andID:model.ID temp:self.tempID];
    }
}
- (void)setModel:(ICN_MyEducationChoiceModel *)model{
    _model = model;
}
- (IBAction)sureBtnClick:(UIButton *)sender {
    if (self.renturnEducationBtnClick) {
        
        self.renturnEducationBtnClick(self.educationArr,self.tempID);
    }
}

-(void)RenturnEducationBtnClick:(RenturnEducationBtnClick)block{
    self.renturnEducationBtnClick = block;
}
@end
