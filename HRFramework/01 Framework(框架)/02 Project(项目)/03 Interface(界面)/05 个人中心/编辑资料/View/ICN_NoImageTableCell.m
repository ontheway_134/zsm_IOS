//
//  UBI_NoImageTableCell.m
//  UbiTalk
//
//  Created by Lym on 2016/11/30.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import "ICN_NoImageTableCell.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_EditTrainAndExModel.h"
#import "ICN_TrainAndExperienceModel.h"
#import "ICN_EducationAndExperienceModel.h"

@implementation ICN_NoImageTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (void)setModel:(ICN_SetUserInfoModel *)model{
    _model = model;
    if ([self.leftLabel.text isEqualToString:@"出生年月"]) {
        self.rightLabel.text = model.memberBirthday;
    }else if ([self.leftLabel.text isEqualToString:@"最高学历"]) {
        
        self.rightLabel.text = model.memberQualificationName;
    } else if ([self.leftLabel.text isEqualToString:@"工作情况"]) {
        
        if (model.workStatus) {
            if (model.workStatus.integerValue) {
                if (model.workStatus.integerValue == 2) {
                    self.rightLabel.text = @" ";
                }
                if (model.workStatus.integerValue == 1) {
                    self.rightLabel.text = @"未工作";
                }
                if (model.workStatus.integerValue == 0) {
                    self.rightLabel.text = @"已工作";
                }
            }
        }
    }else if ([self.leftLabel.text isEqualToString:@"工作经验"]) {
        
        self.rightLabel.text = model.workExperienceName;
    }else if ([self.leftLabel.text isEqualToString:@"期望职位"]){
        
        self.rightLabel.text = model.hopePositionName;
    }else if ([self.leftLabel.text isEqualToString:@"所在地"]){
        
        self.rightLabel.text = [model.provinceName.description stringByAppendingString:model.cityName];
        
    }
    
}
-(void)setEditTrainModel:(ICN_EditTrainAndExModel *)editTrainModel
{     _editTrainModel = editTrainModel;
    //    self.rightLabel.text = editTrainModel.startDate;
    //    self.rightLabel.text = editTrainModel.endDate;
    
}
- (void)setTrainModel:(ICN_TrainAndExperienceModel *)trainModel{
    _trainModel = trainModel;
    if ([self.leftLabel.text isEqualToString:@"开始时间"]) {
        self.rightLabel.text = trainModel.startDate;
        
    } else if ([self.leftLabel.text isEqualToString:@"结束时间"])
        
        self.rightLabel.text = trainModel.endDate;
}

- (void)setModelAndEx:(ICN_EducationAndExperienceModel *)modelAndEx{
    
    
    _modelAndEx = modelAndEx;
    
    
    
}

@end
