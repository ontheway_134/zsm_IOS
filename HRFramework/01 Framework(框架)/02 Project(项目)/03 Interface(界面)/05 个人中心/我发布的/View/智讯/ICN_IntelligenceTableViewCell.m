//
//  ICN_IntelligenceTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_IntelligenceTableViewCell.h"

@implementation ICN_IntelligenceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_intelligenceModel *)model{
    _model = model;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
