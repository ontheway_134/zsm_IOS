//
//  ICN_IntelligenceTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_intelligenceModel.h"
@interface ICN_IntelligenceTableViewCell : UITableViewCell
@property (strong,nonatomic)ICN_intelligenceModel * model;
@end
