//
//  ICN_RelteasingTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_RelteasingTableViewCell.h"

@implementation ICN_RelteasingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_RelteasingModel *)model{
    _model = model;
    self.name.text = model.RandomName;
    [self.url sd_setImageWithURL:[NSURL URLWithString:model.RandomPicture]];
    self.time.text = model.createTime;
    self.contant.text = model.contant;
    [self.commentNumBtn setTitle:model.commentNum forState:UIControlStateNormal];
    [self.ThumbsupNumBtn setTitle:model.ThumbsupNum forState:UIControlStateNormal];
    self.toID = model.RelteasingID;
    /*判断是否点赞了 0 点赞了 1 没有点赞*/
    if ([self.ismakeComplaints isEqualToString:@"0"]) {
        /*更改样式*/
        [self.ThumbsupNumBtn setImage:[UIImage imageNamed:@"点赞"] forState:UIControlStateNormal];
    }else{
        /*更改样式*/
        [self.ThumbsupNumBtn setImage:[UIImage imageNamed:@"点赞未选"] forState:UIControlStateNormal];
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
