//
//  ICN_RelteasingTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_RelteasingModel.h"
@interface ICN_RelteasingTableViewCell : UITableViewCell
@property (strong,nonatomic)ICN_RelteasingModel * model;
@property (weak, nonatomic) IBOutlet UIImageView *url;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *contant;
@property (weak, nonatomic) IBOutlet UIButton *commentNumBtn;
@property (weak, nonatomic) IBOutlet UIButton *ThumbsupNumBtn;
@property (strong,nonatomic)NSString * ismakeComplaints;/*判断是否点赞0为已经点赞 1为未点赞*/
@property (strong,nonatomic)NSString * toID;/*吐槽ID用来传值的*/
@end
