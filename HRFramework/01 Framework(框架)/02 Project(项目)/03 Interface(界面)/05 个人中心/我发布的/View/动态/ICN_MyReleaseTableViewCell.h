//
//  ICN_MyReleaseTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyReleaseXModel.h"
@interface ICN_MyReleaseTableViewCell : UITableViewCell
/*下面是网络请求赋值的属性*/
@property (weak, nonatomic) IBOutlet UIImageView *imageUrl;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *praiseBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UIView *typeView;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeight;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *forwardingView;
@property (weak, nonatomic) IBOutlet UILabel *forwardingLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forwardingImageView;

@property (assign,nonatomic)CGRect scrollViewFrame;;
@property (strong,nonatomic)NSMutableArray * imageViews;
@property (strong,nonatomic)ICN_MyReleaseXModel * model;
@end
