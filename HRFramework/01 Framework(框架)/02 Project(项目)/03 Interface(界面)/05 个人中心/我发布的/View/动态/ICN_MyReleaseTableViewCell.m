//
//  ICN_MyReleaseTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyReleaseTableViewCell.h"
/*图片放大*/
//#import "YYPhotoBrowseView.h"
@implementation ICN_MyReleaseTableViewCell

- (NSMutableArray *)imageViews{
    if (!_imageViews) {
        _imageViews = [NSMutableArray array];
    }
    return _imageViews;
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.typeView.layer.cornerRadius= 6;
    self.typeView.layer.masksToBounds = YES;
    self.typeView.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
    self.typeView.layer.borderWidth = 0.5;
}
#pragma mark - ---------- 懒加载 ----------
#pragma mark - ---------- 生命周期 ----------
#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark 评论按钮
- (IBAction)commentBtnClick:(UIButton *)sender {
}
#pragma mark 转发按钮
- (IBAction)forwardingBtnClick:(UIButton *)sender {
}
#pragma mark 点赞按钮
- (IBAction)praiseBtnClick:(UIButton *)sender {
}
#pragma mark 下拉按钮
- (IBAction)downBtnClick:(id)sender {
    
}
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
- (void)setModel:(ICN_MyReleaseXModel *)model{
    _model = model;
    /*赋值*/
    [self.imageUrl sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.nameLabel.text = model.title;
    self.contentLabel.text = model.content;
    [self.praiseBtn setTitle:model.praiseCount forState:UIControlStateNormal] ;
    [self.commentBtn setTitle:model.commentCount forState:UIControlStateNormal] ;
    self.typeLabel.text = model.memberProfession;
    
    /*需要判断他就没就业 1就业的话就显示单位 0不就业就显示学校 */
    NSString * str = model.workStatus;
    if ([str isEqualToString:@"0"]) {
        self.companyLabel.text = model.memberSchool;
        self.positionLabel.text = model.memberMajor;
        
    }else if ([str isEqualToString:@"1"])
    {
        self.companyLabel.text = model.companyName;
        self.positionLabel.text = model.memberPosition;
        
    }
    else{
    
    }
    NSString * picStr = model.pic;
    /*判断有没有pic这个字段*/
    if ([picStr isEqualToString:@""]) {
        self.imageViewHeight.constant = 0;
        NSLog(@"没图");
    }else{
        /*需要将*/
        NSArray  *array = [picStr componentsSeparatedByString:@","];
        /*开始安装图片*/
        for (int i=0;i<array.count;i++) {
            
            UIImageView *imageview = [[UIImageView alloc] init];
            [imageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(array[i])]];
            /*充满*/
            imageview.userInteractionEnabled = YES;
            /* 图片添加手势 **/
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGes:)];
            [imageview setContentMode:UIViewContentModeScaleToFill];
            imageview.frame = CGRectMake(self.scrollViewFrame.origin.x, 0, 100, 100);
            [imageview addGestureRecognizer:tap];
            imageview.tag = i;
            
            [self.imageViews addObject:imageview];
            [self.scrollView addSubview:imageview];
            _scrollViewFrame.origin.x = _scrollViewFrame.origin.x + 100+10;
        }
        self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*3,self.scrollView.frame.size.height);
        self.scrollView.bounces = NO;
        self.scrollView.showsVerticalScrollIndicator = FALSE;
    }
    
    /*这个有没有isTransmit这个属性 如果是0的话 代表不是转发过来的 如果是1代表是转发过来的*/
    if ([model.isTransmit isEqualToString:@"0"]) {
        /*如果是不是转发过来的话 那么这个高度就设置为0 */
//        self.imageViewHeight.constant = 0;
        
    }else if ([model.isTransmit isEqualToString:@"1"]){
        self.forwardingView.hidden = NO;
        /*对转发的东西进行文字赋值*/
        self.forwardingLabel.text = model.summary;
        /*取pic的第一张图片*/
        if (![model.pic isEqualToString:@""]) {
            /*需要将*/
            NSArray  *array = [model.pic componentsSeparatedByString:@","];
            
            [self.forwardingImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(array[0])]];
        }
    }
}

#pragma  mark 图片点击放大
- (void)handleTapGes:(UITapGestureRecognizer *)tap{
    
//    NSInteger selectIndex = [(UIImageView *)tap.view tag];
//    
//    NSLog(@"点击了%ld",selectIndex);
//    
//    NSMutableArray *items = [NSMutableArray array];
//    
//    UIView *fromView = nil;
//    
//    for (int i =0; i < self.allArr.count; i++) {
//        
//        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
//        
//        item.thumbView = self.imageViews[i];
//        NSURL *url = [NSURL URLWithString:CZString(@"%@%@",Image_DomainName,self.allArr[i])];
//        
//        item.largeImageURL = url;
//        [items addObject:item];
//        if (i == selectIndex) {
//            fromView =  self.imageViews[i];
//        }
//    }
//    
//    YYPhotoBrowseView *groupView = [[YYPhotoBrowseView alloc]initWithGroupItems:items];
//    [groupView presentFromImageView:fromView toContainer:self.navigationController.view animated:YES completion:nil];
    
}
- (void)feedBackHandleTapGes:(UITapGestureRecognizer *)tap{
    
//    NSInteger selectIndex = [(UIImageView *)tap.view tag];
//    
//    NSLog(@"点击了%ld",selectIndex);
//    
//    NSMutableArray *items = [NSMutableArray array];
//    
//    UIView *fromView = nil;
//    
//    for (int i =0; i < self.footImageArr.count; i++) {
//        
//        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
//        
//        item.thumbView = self.feedbackArr[i];
//        NSURL *url = [NSURL URLWithString:CZString(@"%@%@",Image_DomainName,self.footImageArr[i])];
//        
//        item.largeImageURL = url;
//        [items addObject:item];
//        if (i == selectIndex) {
//            fromView =  self.feedbackArr[i];
//        }
//    }
//    
//    YYPhotoBrowseView *groupView = [[YYPhotoBrowseView alloc]initWithGroupItems:items];
//    [groupView presentFromImageView:fromView toContainer:self.navigationController.view animated:YES completion:nil];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
