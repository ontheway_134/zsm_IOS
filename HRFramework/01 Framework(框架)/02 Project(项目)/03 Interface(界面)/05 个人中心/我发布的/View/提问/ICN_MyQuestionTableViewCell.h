//
//  ICN_MyQuestionTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyReQuestionModel.h"
@interface ICN_MyQuestionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *url;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *IndustryLabel;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *position;

@property (weak, nonatomic) IBOutlet UILabel *score;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contant;

@property (weak, nonatomic) IBOutlet UILabel *createTime;

@property (strong,nonatomic)ICN_MyReQuestionModel * model;
@end
