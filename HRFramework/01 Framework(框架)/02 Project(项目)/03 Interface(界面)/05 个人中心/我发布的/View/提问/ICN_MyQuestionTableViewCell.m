//
//  ICN_MyQuestionTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyQuestionTableViewCell.h"

@implementation ICN_MyQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_MyReQuestionModel *)model{
    _model = model;
    [self.url sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    self.title.text = model.memberNick;
    self.IndustryLabel.text = model.IndustryLabel;
    self.company.text = model.company;
    self.position.text = model.position;
    self.score.text = model.score;
    self.titleLabel.text = model.title;
    self.contant.text = model.contant;
    self.createTime.text = model.createTime;
}
- (IBAction)forwardingBtnClick:(UIButton *)sender {
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
