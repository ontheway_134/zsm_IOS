//
//  ICN_UserPostMsgListPagerViewController.m
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyReleasedViewController.h"

#pragma mark --- 业务处理ViewModel ---
#import "ICN_DynamicStateListViewModel.h" // 动态的ViewModel
#import "ICN_ComplainListViewModel.h" // 吐槽详情的ViewModel
#import "ICN_QuestionViewModel.h"     // 提问的ViewModel

#pragma mark --- 跳转的详情页面 ---
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_TransmitToDynamicPager.h"  // 转发到动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 动态详情页面
#import "ICN_ComplainDetialPager.h"    // 吐槽详情页面
#import "ICN_OthersQuestionDetialPager.h" // 别人的回答详情页面
#import "ICN_MyQuestionDetialPager.h" // 自己的回答详情页面
#import "ICN_UserReportDetialVC.h"    // 举报详情页
#import "ICN_DynamicStatePublicationVC.h"
#import "ICN_TopicPublicationVC.h"  // 点击发布智讯
#import "ICN_PublishComplainPager.h" // 发布 吐槽
#import "ICN_PublishAskQuestionPager.h"    // 发布提问
#import "ICN_MyfridentViewController.h"   //实现分享I行动态
#import "MJDIYAutoFooter.h"

#pragma mark --- Cell的样式 ---
#import "ICN_ComplainContentCell.h" // 吐槽列表页Cell 180（默认高度）
#import "ICN_UserListQuestionContentCell.h" // 提问列表页Cell 210（默认高度）
#import "ICN_NewReplyToStateCell.h" // 新的转发类型的Cell
#import "ICN_CommonPsersonDynamicCell.h" // 动态的显示样式Cell

#import "ICN_OwnerViewController.h"/*谁不可看*/

#pragma mark --- Model的样式 ---
#import "ICN_DynStateContentModel.h" // 动态的Model
#import "ICN_ComplainListModel.h" // 吐槽列表的Model
#import "ICN_QuestionListModel.h" // 提问列表Model

#pragma mark --- 友盟分享相关 ---
#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model


@interface ICN_MyReleasedViewController ()<UITableViewDelegate , UITableViewDataSource , ComplainListDelegate , QuestionConfigurationDelegate , ICN_DynamicStateListDelegate , ICN_DynWarnViewDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; // 标题
@property (weak, nonatomic) IBOutlet UILabel *noContentMsgLabel; // 没有内容信息的提示标签
@property (weak, nonatomic) IBOutlet UIButton *dynamicStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *wisdonStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *complainStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *askActionStateBtn;
@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗页面
@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面


// 用户计算位置的背景
@property (weak, nonatomic) IBOutlet UIView *delertSegmentView; // 菜单分隔栏 -- 下面的部分就是tableView
@property (weak, nonatomic) IBOutlet UIView *selectedDelertBackGround; // 选项卡背景板


#pragma mark - ---------- 其他属性 ----------

@property (nonatomic , strong)UIButton *currentSelectedBtn; // 当前选中的按钮
@property (nonatomic , strong)UITableView *tableView; // 当前页面的tableView
@property (nonatomic , strong)UIView *buttonSignView; // 设置按钮下方的切换栏
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 将动态转发为动态用Model
@property (nonatomic , assign , getter=isFooterRefresh)BOOL footerRefresh; // 底部是否刷新方法 默认是否

#pragma mark --- 网络代理 ---
@property (nonatomic , strong) ICN_DynamicStateListViewModel *dynamicViewModel; // 动态 / 智讯网络代理
@property (nonatomic , strong)ICN_ComplainListViewModel *complainViewModel; // 吐槽列表的ViewModel
@property (nonatomic , strong)ICN_QuestionViewModel *questionViewModel; // 提问列表的ViewModel
@property (nonatomic , assign , getter=isEnterpriseLogin)BOOL enterpriseLogin; // 企业用户登录



@end

@implementation ICN_MyReleasedViewController

#pragma mark - ---------- 懒加载 ----------

// 判定是企业用户的时候屏蔽掉发布提问的功能
- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:@[[UIImage imageNamed:@"发布动态"] , [UIImage imageNamed:@"发布智讯"]] TitleLabels:@[@"发布动态" , @"发布智讯"] TabbarHidden:YES];
        _warnView.delegate = self;
        if (self.isEnterpriseLogin) {
            // 隐藏提问按钮
            _warnView.forbidQestionClick = YES;
        }else{
            _warnView.forbidQestionClick = NO;
        }
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    _warnView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    return _warnView;
}

- (ICN_DynWarnView *)replayView{
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        
        _replayView.frame = frame ;
        _replayView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}


- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])]; // 注册吐槽Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserListQuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])]; // 注册提问Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NewReplyToStateCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])]; // 注册新动态转发Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])]; // 注册动态Cell
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
        // 设置在tableview的数据显示不全的时候不显示多余的行分割的方法
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        // 使用约束设置tableView
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.delertSegmentView.mas_bottom);
            make.bottom.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
        }];
        // 配置刷新加载
        [self configTableViewRefreshHeaderFooterView];
    }
    return _tableView;
}

- (UIView *)buttonSignView{
    if (_buttonSignView == nil) {
        _buttonSignView = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonSignView.backgroundColor = RGB0X(0x009dff);
        [self.selectedDelertBackGround addSubview:_buttonSignView];
    }
    return _buttonSignView;
}



#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.footerRefresh = NO;
    // 判断是不是企业登陆
    if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue]) {
        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
            self.enterpriseLogin = YES;
        }else{
            self.enterpriseLogin = NO;
        }
    }
    // 设置隐藏默认的导航栏
    [self setHiddenDefaultNavBar:YES];
    // 设置ViewModel
    self.complainViewModel = [[ICN_ComplainListViewModel alloc] init];
    self.complainViewModel.delegate = self;
    self.questionViewModel = [[ICN_QuestionViewModel alloc] init];
    self.questionViewModel.delegate = self;
    self.dynamicViewModel = [[ICN_DynamicStateListViewModel alloc] init];
    self.dynamicViewModel.delegate = self;
    // 配置tableView
    [self tableView];
    // 配置默认的选中按钮
    UIButton *button = [self.selectedDelertBackGround viewWithTag:self.currentSelectedTag];
    // 获取到正确的button的时候将其设置为选中状态
    if (button) {
        button.selected = YES;
        self.currentSelectedBtn = button;
    }else{
        HRLog(@"error===未获取到正确的切换按钮");
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置默认的选中的按钮下面的切换选项卡
    [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
        make.bottom.equalTo(self.selectedDelertBackGround);
        make.centerX.equalTo(self.currentSelectedBtn);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}

// 根据选中的按钮的tag来切换选项卡的方法 -- 忽略页面刚载入时的处理
- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    // 选中按钮的时候先显示tableview
    self.tableView.hidden = NO;
    // 根据tag计算选中的是哪个按钮 - 并将其他按钮的选中状态改变
    if (tag != self.currentSelectedBtn.tag) {
        // 两次选中的按钮的tag不一样
        
        // 1. 设置之前选中的按钮取消选择
        self.currentSelectedBtn.selected = NO;
        // 2. 判断选中的按钮并设置currentSelectedTag
        self.currentSelectedTag = tag;
        // 3. 设置选中的按钮的状态
        switch (tag) {
            case PRIVATE_DynamicBtn:{
                self.dynamicStateBtn.selected = YES;
                self.currentSelectedBtn = self.dynamicStateBtn;
                break;
            }
            case PRIVATE_WisdomBtn:{
                self.wisdonStateBtn.selected = YES;
                self.currentSelectedBtn = self.wisdonStateBtn;
                break;
            }
            case PRIVATE_TopicBtn:{
                self.complainStateBtn.selected = YES;
                self.currentSelectedBtn = self.complainStateBtn;
                [self.tableView reloadData];
                break;
            }
            case PRIVATE_AskActionBtn:{
                self.askActionStateBtn.selected = YES;
                self.currentSelectedBtn = self.askActionStateBtn;
                [self.tableView reloadData];
                break;
            }
            default:
                break;
        }
        
        // 4. 设置切换选中状态的标签的跟随移动
        [UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.buttonSignView.center;
            center.x = self.currentSelectedBtn.centerX;
            self.buttonSignView.center = center;
        } completion:^(BOOL finished) {
            [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.selectedDelertBackGround);
                make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
                make.centerX.equalTo(self.currentSelectedBtn);
            }];
        }];
    }
    
    // 6. 根据选中的内容刷新页面
    [self.tableView.mj_header beginRefreshing];
    
}
#pragma mark --- ICN_DynWarnViewDelegate ---
// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

/** 配置分享给联系人的拓展字典 */
- (NSDictionary *)configSharePramsDicWithType:(NSInteger)shareType Pic:(NSString *)contentPic Content:(NSString *)content EXT:(NSString *)contentEXT ShareId:(NSString *)shareId{
    
    NSMutableDictionary *mutableParams = [@{
                                            @"shareType" : SF(@"%ld",shareType),
                                            @"content" : SF(@"%@",content),
                                            @"shareId" : SF(@"%@",shareId),
                                            } mutableCopy];
    if (contentPic) {
        [mutableParams setValue:contentPic forKey:@"contentPic"];
    }
    if (contentEXT) {
        [mutableParams setValue:contentEXT forKey:@"contentEXT"];
    }
    return [NSDictionary dictionaryWithDictionary:mutableParams];
}

// 实现分享到i行好友的方法
- (void)shareToMyFriendPager{
    // 分享到 I行好友
    ICN_MyfridentViewController *myfrident=[[ICN_MyfridentViewController alloc]init];
    myfrident.model=self.replayModel;
    // 第一步判断类型
    switch (self.transmitModel.paramsType) {
        case REPLAY_DynamicReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:2 Pic:picUrl Content:self.replayModel.content EXT:nil ShareId:self.replayModel.DynID];
            break ;
        }
        case REPLAY_WisdomReplay:{
            NSString *picUrl ;
            if (self.replayModel) {
                if (self.replayModel.pic && [self.replayModel.pic containsString:@"."]) {
                    // 获取到图片
                    picUrl = ICN_IMG([[self.replayModel.pic componentsSeparatedByString:@","] firstObject]);
                }
            }
            myfrident.ext = [self configSharePramsDicWithType:3 Pic:picUrl Content:self.replayModel.content EXT:self.replayModel.title ShareId:self.replayModel.DynID];
            break ;
        }
        case REPLAY_ComplainReplay:{
            myfrident.ext = [self configSharePramsDicWithType:5 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        case REPLAY_QuestionReplay:{
            myfrident.ext = [self configSharePramsDicWithType:4 Pic:self.transmitModel.iconUrl Content:self.transmitModel.content EXT:nil ShareId:self.transmitModel.modelId];
            break ;
        }
        default:
            break;
    }
    self.replayModel = nil;
    [self currentPagerJumpToPager:myfrident];
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [HSGifHeader headerWithRefreshingBlock:^{
        
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            switch (self.currentSelectedTag) {
                case PRIVATE_DynamicBtn:{
////                    // 先设置不是 ta的智讯
                    self.dynamicViewModel.mainWisdom = NO;
                    //此时刷新的是 我的动态
                    self.dynamicViewModel.mainDynamic = YES;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel refreshDynamicIntelligenceAF];
                    
                    /*网络请求动态*/
                    
                    
                    break;
                }
                case  PRIVATE_WisdomBtn:{
                    //此时刷新的是智讯
                    self.dynamicViewModel.mainWisdom = YES;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel XDDAF];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 刷新吐槽操作
                    [self.complainViewModel requestComplainListWithType:Complain_ResponseMineList isLoad:NO MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                case PRIVATE_AskActionBtn:{
                    // 刷新提问列表操作
                    [self.questionViewModel requestQuestionListWithType:Question_ResponseMainList isLoad:NO MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                default:
                    break;
            }
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
   // [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
   // [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
   // [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
   // [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
        
        // 判断调用的ViewModel以及对应的逻辑处理
        if (!self.isFooterRefresh) {
            self.footerRefresh = NO;
            switch (self.currentSelectedTag) {
                case PRIVATE_DynamicBtn:{
                    // 首相设置不是 ta的智讯
                    self.dynamicViewModel.mainDynamic = YES;
                    //此时加载的是 ta的动态
                    self.dynamicViewModel.mainWisdom = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel loadNextPageContentCells];
                    break;
                }
                case  PRIVATE_WisdomBtn:{
                    //此时加载的是智讯
                    self.dynamicViewModel.mainWisdom = NO;
                    self.dynamicViewModel.memberId = self.memberId;
                    [self.dynamicViewModel loadNextPageContentCells];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 加载吐槽操作
                    [self.complainViewModel requestComplainListWithType:Complain_ResponseUserIDList isLoad:YES MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                case PRIVATE_AskActionBtn:{
                    // 加载提问列表操作
                    [self.questionViewModel requestQuestionListWithType:Question_ResponseUserIdList isLoad:YES MemberId:self.memberId SearchContent:nil];
                    break ;
                }
                default:
                    break;
            }
        }
        
    }];
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


- (UITableViewCell *)requestCurrentDynamicCellWithTableView:(UITableView *)tableview Model:(ICN_DynStateContentModel *)model IndexPath:(NSIndexPath *)indexPath{
    
    // 确认显示动态的样式是转发样式的类型
    if ([model.fromId integerValue] != 0) {
        // 存在则证明是最新类型的Model
        ICN_NewReplyToStateCell *cell = [tableview dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_NewReplyToStateCell class])];
        cell.model = model;
        
        if ([model.type isEqualToString:@"0"]) {
            [cell.reportBtn setImage:[UIImage imageNamed:@"小人图标"] forState:UIControlStateNormal];
        }else if ([model.type isEqualToString:@"1"]){
            /*如果是转发的智讯 那么隐藏这个按钮*/
            cell.reportBtn.hidden = YES;
            //  [cell.peopleBtn setImage:[UIImage imageNamed:@"举报"] forState:UIControlStateNormal];
        }
        
        /*判断是不是智讯 有title才是智讯*/
        if ([model.title isEqualToString:@""]||model.title == nil) {
            cell.reportBtn.hidden = YES;
        }else{
            [cell.reportBtn setImage:[UIImage imageNamed:@"举报"] forState:UIControlStateNormal];
        }
        
        if ([model.isTransmit isEqualToString:@"1"]) {
             cell.reportBtn.hidden = YES;
        }
        else if ([model.isTransmit isEqualToString:@"0"]){
            cell.reportBtn.hidden = NO;
            [cell.reportBtn setImage:[UIImage imageNamed:@"小人图标"] forState:UIControlStateNormal];
            cell.reportBtn.tag = 10023;
        }
        
        [cell callBackWithNewDynReplyCellBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag, BOOL isLikeUp) {
            // 根据tag判断点击的按钮
            switch (btnTag) {
                case ICN_CellLikeActionBtnType:{
                    // 点赞操作 1：点赞，2：取消点赞
                    if (isLikeUp) {
                        // 进行点赞操作
                        [self.dynamicViewModel likeUpWithType:1 Model:model];
                    }else{
                        // 进行取消点赞操作
                        [self.dynamicViewModel likeUpWithType:2 Model:model];
                    }
                    break ;
                }
                case ICN_CellReportBtnType:{
                    // 举报操作
                    ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                    pager.matterId = model.matterId;
                    [self currentPagerJumpToPager:pager];
                    break ;
                }
                case ICN_CellCommentBtnType:{
                    // 评论操作
                    //相关逻辑 - 跳转到评论页面
                    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                    pager.model = model;
                    [self currentPagerJumpToPager:pager];
                    break ;
                }
                case ICN_CellReplayType:{
                    // 转发操作
                    // 1. 获取转发需要的两种Model
                    self.replayModel = model;
                    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                    // 2. 对于智讯的title进行处理
                    if (model.title == nil) {
                        self.transmitModel.title = @"i行动态";
                    }
                    // 3. 展开转发页面
                    [self.view addSubview:self.replayView];
                    break ;
                }
                default:
                    break;
            }
        }];
        return cell;
    }else{
        
        ICN_CommonPsersonDynamicCell *cell = [tableview dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
        //更改图片样式 变成小人
       //
        cell.model = model;
        /*需要判断当前状态是动态还是智讯0：动态，1：智讯 */
        if ([model.type isEqualToString:@"0"]) {
            [cell.peopleBtn setImage:[UIImage imageNamed:@"小人图标"] forState:UIControlStateNormal];
            cell.peopleBtn.tag = 10023;
        }else if ([model.type isEqualToString:@"1"]){
            /*如果是转发的智讯 那么隐藏这个按钮*/
            cell.peopleBtn.hidden = YES;
            //[cell.peopleBtn setImage:[UIImage imageNamed:@"举报"] forState:UIControlStateNormal];
       }
        /*判断是不是智讯 有title才是智讯*/
        if ([model.title isEqualToString:@""]||model.title == nil) {
        }else{
            [cell.peopleBtn setImage:[UIImage imageNamed:@"举报"] forState:UIControlStateNormal];
        }
        if ([model.isTransmit isEqualToString:@"1"]) {
            cell.peopleBtn.hidden = YES;
        }
        else if ([model.isTransmit isEqualToString:@"0"]){
            cell.peopleBtn.hidden = NO;
            [cell.peopleBtn setImage:[UIImage imageNamed:@"小人图标"] forState:UIControlStateNormal];
            cell.peopleBtn.tag = 10023;
        }
        // 添加对于Cell上点击事件的处理
        [cell callWhileCellBtnClick:^(NSInteger SenderTag, ICN_DynStateContentModel *model) {
            NSLog(@"%ld",SenderTag);
            switch (SenderTag) {
                case ICN_CellLikeActionBtnType:{
                    model.likeUp = !model.likeUp;
                    NSInteger likeType = 0;
                    if (model.isLikeUp) {
                        likeType = 1;
                    }else{
                        likeType = 2;
                    }
                    [self.dynamicViewModel likeUpWithType:likeType Model:model];
                    break;
                }
                case ICN_CellCommentBtnType:{
                    //相关逻辑 - 跳转到动态详情页面
                    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                    pager.model = self.dynamicViewModel.modelsArr[indexPath.row];
                    [self currentPagerJumpToPager:pager];
                    break;
                }
                case ICN_CellReviewBtnType:{
                    // 转发
                    // 1. 获取转发需要的两种Model
                    self.replayModel = model;
                    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_DynamicReplay ParamsKey:@"matterId" ModelId:model.matterId Title:model.title IconUrl:ICN_IMG(model.memberLogo) Content:model.content];
                    // 2. 对于智讯的title进行处理
                    if (model.title == nil) {
                        self.transmitModel.title = @"i行动态";
                    }
                    // 3. 展开转发页面
                    [self.view addSubview:self.replayView];
                    break;
                }
                case 10023:{
                    /*需要判断当前状态是动态还是智讯0：动态，1：智讯 */
                   // if (model.title == nil) {
                    NSLog(@"%ld",cell.peopleBtn.tag);
                        if ((cell.peopleBtn.tag = 10023)) {
                            //跳转到谁不可看页面
                            ICN_OwnerViewController * vc =[[ICN_OwnerViewController alloc]init];
                            vc.generalID = model.matterId;
                            [self.navigationController pushViewController:vc animated:YES];
                            break ;
                        }else if ([model.type isEqualToString:@"1"]){
                            //跳转到举报页面
                            ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                            pager.matterId = model.matterId;
                            [self currentPagerJumpToPager:pager];
                            break ;
                        }
//                    }
                    else {
                        //跳转到举报页面
                        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                        pager.matterId = model.matterId;
                        [self currentPagerJumpToPager:pager];
                        break ;
                    }
                }
                case ICN_CellPullBtnType:{
                    // 文本框下拉操作
                    [self.tableView reloadData];
                    break;
                }
                default:
                    break;
            }
        }];
        [cell setListStyle:YES];
        return cell;
    }
    return nil;
}
#pragma mark 智讯删除
- (void)applyDeleteIntelligence:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    
    [[HRRequest manager]POST:PATH_MyReleasedelTopic para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
        //        [self.tableView reloadData];
    }];
    
}

#pragma mark 我发布的提问删除
- (void)applyDeleteQuestion:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    
    [[HRRequest manager]POST:PATH_MyReleasedelQuestion para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
        //        [self.tableView reloadData];
    }];
}

#pragma mark 吐槽删除
- (void)applyDeleteComplaints:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    
    [[HRRequest manager]POST:PATH_MyReleasedelComplaint para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
        //        [self.tableView reloadData];
    }];
}
#pragma mark 动态删除
- (void)applyDelete:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    
    [[HRRequest manager]POST:PATH_MyReleasedelDynamic para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"删除失败"];
        [self.tableView reloadData];
    }];
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnPublishBtnAction:(UIButton *)sender {
    self.warnView.frame = [UIScreen mainScreen].bounds;
    [self.view addSubview:self.warnView];
}


// 点击返回按钮
- (IBAction)clickOnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 选中切换状态按钮的方法
- (IBAction)clickOnStateSelectedBtnAction:(UIButton *)sender {
    // 根据按钮的tag进行对应的切换操作
    [self changeSelectedContentLabelStatusWithSenderTage:sender.tag];
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    //    1002331  动态  1002332 智讯
    switch (type) {
            // 点击发布动态
        case Publich_DynamicState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_DynamicBtn];
            ICN_DynamicStatePublicationVC *pager = [[ICN_DynamicStatePublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 点击发布智讯
        case Publich_WisdomState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_WisdomBtn];
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布吐槽
        case Publich_ComplainState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_TopicBtn];
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布提问
        case Publich_AskQuestion:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_AskActionBtn];
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态 == 需要判断是何种形式的转发到动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到i行好友功能
            [self shareToMyFriendPager];
            //            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到微信朋友圈
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            NSLog(@"分享到QQ空间");
            [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            break;
        }
        default:
            break;
    }
    // 进行完操作后默认将视图移除出主页面
    [self.warnView removeFromSuperview];
    [self.replayView removeFromSuperview];
}
#pragma mark --- 动态与智讯相关代理 ---

- (void)responseWithDynamicStateListRequestSuccess:(BOOL)success Code:(NSInteger)code{
    // 第一步 结束tableview的刷新
    [self endRefreshWithTableView:self.tableView];
    // 判断是加载的状态则设置是否加载属性为未加载
    if (code == 5) {
        self.footerRefresh = NO;
    }
    if (success) {
        if (code == 0) {
            [self.tableView reloadData];
        }
        if (code == 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该用户未登录"];
        }
    }else{
        // 网络请求失败
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败 - 请稍后重试"];
    }
    // 如果刷新失败 隐藏tableview
    switch (self.currentSelectedTag) {
        case PRIVATE_DynamicBtn:{
            if (self.dynamicViewModel.modelsArr.count == 0) {
                self.tableView.hidden = YES;
            }
            break ;
        }
        case PRIVATE_WisdomBtn:{
            if (self.dynamicViewModel.wisdomModelsArr.count == 0) {
                self.tableView.hidden = YES;
            }
            break ;
        }
        default:
            break;
    }
    
    
}

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (!success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"点赞失败"];
    }else{
        [self.tableView reloadData];
    }
}

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您无法为该用户点赞"];
}
/** 已经点赞后的回调 */
- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经为该用户点过赞"];
    [self.tableView reloadData];
}
#pragma mark --- 提问代理 ---
- (void)responseQuestionWithEnumType:(QuestionResponseType)enumType Success:(BOOL)success Info:(NSString *)info{
    [self endRefreshWithTableView:self.tableView];
    switch (enumType) {
        case Question_ResponseMainList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 如果刷新失败 隐藏tableview
                if (self.questionViewModel.modelsArr.count == 0) {
                    self.tableView.hidden = YES;
                }
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
            
        default:
            break;
    }
}

#pragma mark --- 吐槽代理 ---
- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    [self endRefreshWithTableView:self.tableView];
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (!success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseDisLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            if (!success) {
                [self.tableView reloadData];
            }
            break ;
        }
        case Complain_ResponseMineList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                // 如果刷新失败 隐藏tableview
                if (self.complainViewModel.modelsArr.count == 0) {
                    self.tableView.hidden = YES;
                }
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    switch (self.currentSelectedTag) {
        case PRIVATE_DynamicBtn:{
            //相关逻辑 - 跳转到动态详情页面
            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
            pager.model = self.dynamicViewModel.modelsArr[indexPath.row];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_WisdomBtn:{
            //相关逻辑 - 跳转到智讯详情页面
            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
            pager.model = self.dynamicViewModel.wisdomModelsArr[indexPath.row];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_TopicBtn:{
            // 吐槽详情
            ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
            pager.complainId = [self.complainViewModel.modelsArr[indexPath.row] complainId];
            [self currentPagerJumpToPager:pager];
            break ;
        }
        case PRIVATE_AskActionBtn:{
            // 提问详情
            // 获取到需要的Model
            ICN_QuestionListModel *model = self.questionViewModel.modelsArr[indexPath.row];
            if ([model.isMine integerValue] == 0) {
                // 是我发布的
                ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
                pager.constentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }else{
                // 不是我发布的
                ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
                pager.contentId = model.questionId;
                [self currentPagerJumpToPager:pager];
            }
            break ;
        }
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 默认返回十条用于测试假数据
    switch (self.currentSelectedTag) {
        case PRIVATE_TopicBtn:{
            // 吐槽
            return self.complainViewModel.modelsArr.count;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 提问
            return self.questionViewModel.modelsArr.count;
            break;
        }case PRIVATE_WisdomBtn:{
            return self.dynamicViewModel.wisdomModelsArr.count;
            break;
        }
        case PRIVATE_DynamicBtn:{
            return self.dynamicViewModel.modelsArr.count;
            break;
        }
        default:
            break;
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据选中按钮的tag来判断返回的是什么内容
    switch (self.currentSelectedBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 吐槽
            ICN_ComplainContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
            cell.model = self.complainViewModel.modelsArr[indexPath.row];
            [cell callBackWithCellBlock:^(ICN_ComplainListModel *model, NSInteger selectedType, BOOL isSelected) {
                switch (selectedType) {
                        // 点赞 / 取消点赞操作
                    case ICN_CellLikeActionBtnType:{
                        if (isSelected) {
                            [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }else{
                            [self.complainViewModel requestComplainFunctionWithType:Complain_ResponseDisLikeUp ComplainId:model.complainId ReplayerId:nil Content:nil CommentId:nil];
                        }
                        break ;
                    }
                        // 评论操作
                    case ICN_CellCommentBtnType:{
                        ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
                        pager.complainId = model.complainId;
                        [self currentPagerJumpToPager:pager];
                        break ;
                    }
                        // 转发操作
                    case ICN_CellReplayType:{
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_ComplainReplay ParamsKey:@"complaintsId" ModelId:model.complainId Title:model.RandomName IconUrl:model.RandomPicture Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.replayView];
                        break ;
                        break ;
                    }
                        
                    default:
                        break;
                }
            }];
            return cell;
            break ;
        }
        case PRIVATE_AskActionBtn:{
            // 提问
            ICN_UserListQuestionContentCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])];
            cell.model = self.questionViewModel.modelsArr[indexPath.row];
            [cell callBackQuestionCellBlock:^(ICN_QuestionListModel *model, NSInteger buttonType, BOOL isSelected) {
                switch (buttonType) {
                    case ICN_CellReplayType:{
                        // 转发类型
                        // 1. 生成转发Model
                        self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_QuestionReplay ParamsKey:@"questionId" ModelId:model.questionId Title:@"i行提问" IconUrl:model.memberLogo Content:model.contant];
                        // 2. 弹出转发窗口
                        [self.view addSubview:self.replayView];
                    }
                        
                    default:
                        break;
                }
            }];
            return cell;
            break ;
        }
        case PRIVATE_DynamicBtn:
        case PRIVATE_WisdomBtn:{
            // 1. 获取到合理的数据源
            NSMutableArray *modelsArr ;
            if (self.currentSelectedBtn.tag == PRIVATE_DynamicBtn) {
                // 设置当前获取到动态的数据源
                modelsArr = self.dynamicViewModel.modelsArr;
            }else{
                // 设置当前获取到的是智讯的数据源
                modelsArr = self.dynamicViewModel.wisdomModelsArr;
            }
            // 根据数据源去部署Cell
            return [self requestCurrentDynamicCellWithTableView:tableView Model:modelsArr[indexPath.row] IndexPath:indexPath];
            break;
        }
        default:
            break;
    }
    return nil;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return tableView.rowHeight;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    /*枚举判断当前选择按钮是什么*/
    switch (self.currentSelectedBtn.tag){
        case PRIVATE_TopicBtn:{
            ICN_ComplainListModel *model =self.complainViewModel.modelsArr[indexPath.row];
            
            [self.complainViewModel.modelsArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteComplaints:model.complainId];
            
            break;
        }
        case PRIVATE_DynamicBtn:{
            
            
            ICN_DynStateContentModel  * model = self.dynamicViewModel.modelsArr[indexPath.row];
            [self.dynamicViewModel.modelsArr removeObjectAtIndex:indexPath.row];
            [self applyDelete:model.DynID];
            
            
            break;
        }
        case PRIVATE_WisdomBtn:{
            
            
            ICN_DynStateContentModel * model = self.dynamicViewModel.wisdomModelsArr[indexPath.row];
            
            [self.dynamicViewModel.wisdomModelsArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteIntelligence:model.roleId];
            
            
            break;
        }
        case PRIVATE_AskActionBtn:{
            
            ICN_QuestionListModel *model = self.questionViewModel.modelsArr[indexPath.row];
            
            [self.questionViewModel.modelsArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteQuestion:model.questionId];
            
            break;
        }
        default:
            break;
    }
//    /*进行枚举判断 0 动态 1 智讯 2 吐槽 3 提问  */
//    switch (self.selectIndex) {
//        case 0:
//        {
//            ICN_MyReleaseXModel *model = self.baseSourseArr[indexPath.row];
//            [self.viewModel.modelsArr removeObjectAtIndex:indexPath.row];
//            [self applyDelete:model.ReleaseID];
//        }
//            break;
//        case 1:
//        {
//            ICN_MyReleaseXModel *model = self.todicSourseArr[indexPath.row];
//            [self.viewModel.modelsArr removeObjectAtIndex:indexPath.row];
//            [self applyDeleteIntelligence:model.ReleaseID];
//        }
//            break;
//        case 2:
//        {
//            ICN_RelteasingModel *model = self.complaintsSourseArr[indexPath.row];
//            [self.complaintsSourseArr removeObjectAtIndex:indexPath.row];
//            [self applyDeleteComplaints:model.RelteasingID];
//        }
//            break;
//        case 3:
//        {
//            
//            ICN_MyReQuestionModel *model = self.questionSourseArr[indexPath.row];
//            [self.questionSourseArr removeObjectAtIndex:indexPath.row];
//            [self applyDeleteQuestion:model.questionID];
//        }
//            break;
//        default:
//            break;
//    }
}
-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath {
    return @"删除";
}
/** 逐条调节tableView的Cell高度 */
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    switch (self.currentSelectedBtn.tag) {
//        case PRIVATE_DynamicBtn:{
//            return 120;
//            break;
//        }
//        case PRIVATE_WisdomBtn:{
//            return 120;
//            break;
//        }
//        case PRIVATE_TopicBtn:{
//            // 返回吐槽的高度
//            return 180;
//            break;
//        }
//        case PRIVATE_AskActionBtn:{
//            // 返回提问的高度
//            return 210;
//            break;
//        }
//        default:
//            return 120;
//            break;
//    }
//
//    return 120;
//}





@end
