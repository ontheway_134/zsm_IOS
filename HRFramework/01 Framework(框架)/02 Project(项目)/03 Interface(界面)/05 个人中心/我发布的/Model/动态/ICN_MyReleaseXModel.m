//
//  ICN_MyReleaseXModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyReleaseXModel.h"

@implementation ICN_MyReleaseXModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ReleaseID" : @"id"}];
}
@end
