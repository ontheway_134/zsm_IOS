//
//  ICN_MyReQuestionModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyReQuestionModel.h"

@implementation ICN_MyReQuestionModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"questionID" : @"id"}];
}
@end
