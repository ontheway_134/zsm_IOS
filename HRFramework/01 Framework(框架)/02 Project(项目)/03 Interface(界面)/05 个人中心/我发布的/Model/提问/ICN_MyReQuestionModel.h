//
//  ICN_MyReQuestionModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyReQuestionModel : BaseOptionalModel
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * title;
@property (strong,nonatomic)NSString * contant;
@property (strong,nonatomic)NSString * createTime;
@property (strong,nonatomic)NSString * collectum;
@property (strong,nonatomic)NSString * score;
@property (strong,nonatomic)NSString * questionID;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * IndustryLabel;
@property (strong,nonatomic)NSString * plusv;
@property (strong,nonatomic)NSString * position;
@property (strong,nonatomic)NSString * company;
@property (strong,nonatomic)NSString * iscollect;
@property (strong,nonatomic)NSString * commentNum;
@end
