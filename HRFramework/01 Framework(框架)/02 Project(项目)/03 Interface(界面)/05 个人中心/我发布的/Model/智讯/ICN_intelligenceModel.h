//
//  ICN_intelligenceModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_intelligenceModel : BaseOptionalModel
@property (strong,nonatomic)NSString * acompanyName;
@property (strong,nonatomic)NSString * amemberPosition;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * title;
@property (strong,nonatomic)NSString * summary;
@property (strong,nonatomic)NSString * isOpened;
@property (strong,nonatomic)NSString * transmitCount;
@property (strong,nonatomic)NSString * plusv;
@property (strong,nonatomic)NSString * memberProfession;
@property (strong,nonatomic)NSString * memberSchool;
@property (strong,nonatomic)NSString * authorNick;
@property (strong,nonatomic)NSString * memberMajor;
@property (strong,nonatomic)NSString * isTransmit;
@property (strong,nonatomic)NSString * isRecommit;
@property (strong,nonatomic)NSString * roleId;
@property (strong,nonatomic)NSString * pic;
@property (strong,nonatomic)NSString * memberPosition;
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * companyName;
@property (strong,nonatomic)NSString * amemberSchool;
@property (strong,nonatomic)NSString * commentCount;
@property (strong,nonatomic)NSString * amemberMajor;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * createDate;
@property (strong,nonatomic)NSString * praiseCount;
@property (strong,nonatomic)NSString * matterId;
@property (strong,nonatomic)NSString * authorId;
@property (strong,nonatomic)NSString * content;

@end
