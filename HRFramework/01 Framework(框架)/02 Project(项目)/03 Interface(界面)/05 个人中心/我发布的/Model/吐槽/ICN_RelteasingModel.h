//
//  ICN_RelteasingModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_RelteasingModel : BaseOptionalModel
@property (strong,nonatomic)NSString * RelteasingID;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * RandomName;
@property (strong,nonatomic)NSString * RandomPicture;
@property (strong,nonatomic)NSString * contant;
@property (strong,nonatomic)NSString * commentNum;
@property (strong,nonatomic)NSString * ThumbsupNum;
@property (strong,nonatomic)NSString * createTime;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * ismakeComplaints;
@end
