//
//  ICN_RelteasingModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_RelteasingModel.h"

@implementation ICN_RelteasingModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"RelteasingID" : @"id"}];
}
@end
