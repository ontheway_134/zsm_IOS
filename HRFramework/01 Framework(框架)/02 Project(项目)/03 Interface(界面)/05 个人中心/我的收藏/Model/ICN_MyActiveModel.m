//
//  ICN_MyActiveModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyActiveModel.h"

@implementation MyActiveModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"baseId" : @"id"}];
}

@end
@implementation ICN_MyActiveModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"infoList" : @"info"}];
}

@end
