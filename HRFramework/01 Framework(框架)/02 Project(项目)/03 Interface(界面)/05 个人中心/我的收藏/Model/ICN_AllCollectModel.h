//
//  ICN_AllCollectModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@interface ICN_AllCollectModel : BaseOptionalModel
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *positionTitle;//收藏职位名称
@property (nonatomic, copy) NSString *IndustryId;//所属行业
@property (nonatomic, copy) NSString *workType;//工作类别
@property (nonatomic, copy) NSString *minSalary;//最低薪资
@property (nonatomic, copy) NSString *maxSalary;//最高薪资
@property (nonatomic, copy) NSString *companyName;//公司名称
@property (nonatomic, copy) NSString *companyLogo;//公司图标
@property (nonatomic, copy) NSString *cityName;//所在城市
@property (nonatomic, copy) NSString *positionId;
@property (nonatomic, copy) NSString *industryName;//行业名称

@property (nonatomic, copy) NSString *CollectID;
@property (strong,nonatomic)NSString * activityNum;
@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *pic;
@property(nonatomic, copy)NSString *createDate;
@property(nonatomic, copy)NSString *createdate;
@property(nonatomic, copy) NSString *pageViews;


@property(nonatomic, copy)NSString *infoid;

@property (strong,nonatomic)NSString * contant;
@property (strong,nonatomic)NSString * questID;
@property (strong,nonatomic)NSString * iscollect;
@property (strong,nonatomic)NSString * position;
@property (strong,nonatomic)NSString * plusv;
@property (strong,nonatomic)NSString * company;

@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * createTime;
@property (strong,nonatomic)NSString * questionId;
@property (strong,nonatomic)NSString * IndustryLabel;
@property (strong,nonatomic)NSString * collectNum;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * score;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * collectId;

@property (strong,nonatomic)NSString * activityId;
@property (strong,nonatomic)NSString * collectDate;

@property (strong,nonatomic)NSString * content;

@property (strong,nonatomic)NSString * canNum;
@property (strong,nonatomic)NSString * subNum;
@property (strong,nonatomic)NSString * price;
@property (strong,nonatomic)NSString * beginDate;
@property (strong,nonatomic)NSString * endDate;

@property (strong,nonatomic)NSString * id;
@property (strong,nonatomic)NSString * type;
@end
