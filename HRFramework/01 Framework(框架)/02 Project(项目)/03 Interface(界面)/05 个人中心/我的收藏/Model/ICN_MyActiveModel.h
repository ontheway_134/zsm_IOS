//
//  ICN_MyActiveModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@protocol MyActiveModel <NSObject>
@end
@interface  MyActiveModel: BaseOptionalModel
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * activityId;
@property (strong,nonatomic)NSString * collectDate;
@property (strong,nonatomic)NSString * title;
@property (strong,nonatomic)NSString * content;
@property (strong,nonatomic)NSString * pic;
@property (strong,nonatomic)NSString * canNum;
@property (strong,nonatomic)NSString * subNum;
@property (strong,nonatomic)NSString * price;
@property (strong,nonatomic)NSString * beginDate;
@property (strong,nonatomic)NSString * endDate;
@property (strong,nonatomic)NSString * createDate;
@property (strong,nonatomic)NSString * baseId;

@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * companyName;
@property (strong,nonatomic)NSString * memberPosition;
@property (strong,nonatomic)NSString * liveId;
@property (strong,nonatomic)NSString * hasBegin;
@property (strong,nonatomic)NSString * hasBeginId;
@property (strong,nonatomic)NSString * memberStatus;

@property (strong,nonatomic)NSString * liveUrl;
@property (strong,nonatomic)NSString * contantType;

@property (strong,nonatomic)NSString * relationStatus;
@property (strong,nonatomic)NSString * packageStatus;

@property (strong,nonatomic)NSString * memberLogo;


@end
@interface ICN_MyActiveModel : JSONModel
@property (nonatomic,strong) MyActiveModel<MyActiveModel> * infoList;
@property (nonatomic,assign) NSInteger type;
@end
