//
//  ICN_MyCollcetPositionModel.h
//  ICan
//
//  Created by apple on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface ICN_MyCollcetPositionModel : BaseOptionalModel
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *positionTitle;//收藏职位名称
@property (nonatomic, copy) NSString *IndustryId;//所属行业
@property (nonatomic, copy) NSString *workType;//工作类别
@property (nonatomic, copy) NSString *minSalary;//最低薪资
@property (nonatomic, copy) NSString *maxSalary;//最高薪资
@property (nonatomic, copy) NSString *companyName;//公司名称
@property (nonatomic, copy) NSString *companyLogo;//公司图标
@property (nonatomic, copy) NSString *cityName;//所在城市
@property (nonatomic, copy) NSString *positionId;
@property (nonatomic, copy) NSString *industryName;//行业名称

@property (nonatomic, copy) NSString *CollectID;


@end
