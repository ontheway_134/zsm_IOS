//
//  ICN_MyQuestionModel.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyQuestionModel.h"

@implementation ICN_MyQuestionModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"questID" : @"id"}];
}
@end
