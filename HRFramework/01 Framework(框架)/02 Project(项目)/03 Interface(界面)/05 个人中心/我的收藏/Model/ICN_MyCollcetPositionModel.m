//
//  ICN_MyCollcetPositionModel.m
//  ICan
//
//  Created by apple on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCollcetPositionModel.h"

@implementation ICN_MyCollcetPositionModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"CollectID" : @"id"}];
}


@end
