//
//  ICN_MyInformationCollModel.h
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyInformationCollModel : BaseOptionalModel
@property(nonatomic, copy)NSString *title;
@property(nonatomic, copy)NSString *pic;
@property(nonatomic, copy)NSString *createDate;
@property(nonatomic, copy) NSString *pageViews;
@property(nonatomic, copy) NSString *positionId;

@property(nonatomic, copy)NSString *infoid;
@end
