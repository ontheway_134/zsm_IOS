//
//  ICN_MyCollectingViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_MyCollectingViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *typeBtn;
@property (weak, nonatomic) IBOutlet UIView *redLine;


/******************数据源*******************/

@property (strong,nonatomic)NSMutableArray * allSourseArr;/*全部数据源*/
@property (strong,nonatomic)NSMutableArray * positionSourseArr;/*职位数据源*/
@property (strong,nonatomic)NSMutableArray * informationSourseArr;/*资讯数据源*/
@property (strong,nonatomic)NSMutableArray * activeSourseArr;/*活动数据源*/
@property (strong,nonatomic)NSMutableArray * questionSourseArr;/*提问数据源*/
@end
