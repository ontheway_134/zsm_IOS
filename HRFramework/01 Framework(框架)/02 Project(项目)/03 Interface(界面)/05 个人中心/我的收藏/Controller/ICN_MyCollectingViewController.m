//
//  ICN_MyCollectingViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyCollectingViewController.h"
#import "ICN_MyCollcetingTableViewCell.h"/*职位*/
#import "ICN_CollectInformationTableViewCell.h"/*资讯*/
#import "ICN_CollecActiveTableViewCell.h"/*活动*/
#import "ICN_CollectQuestionsTableViewCell.h"
#import "ICN_PositionNextOneViewController.h"/*职位详情*/
#import "ICN_IndustryNextViewController.h"/*资讯详情*/
#import "ICN_ActivityDetialViewController.h"/*活动详情*/
#import "ICN_MyQuestionDetialPager.h"/*提问详情*/
#import "ICN_OthersQuestionDetialPager.h" // 他的提问详情
#import "ICN_LiveDetialViewController.h"/*直播详情*/
#import "ICN_MYAnswerQuestionPager.h"
#import "ICN_ApplyModel.h"
#import "MJDIYAutoFooter.h"
#pragma mark --- 友盟分享相关 ---
#import "ICN_DynWarnView.h"                     // 通用提示窗口
#import "ICN_YouMengShareTool.h"                // 友盟分享工具类
#import "ICN_YouMengShareModel.h"               // 友盟分享用Model
#import "ICN_ShareManager.h"/*单例*/
#import "ICN_TransmitToDynamicPager.h"  // 转发到动态页面
/************model**************/
#import "ICN_MyCollcetPositionModel.h"
#import "ICN_MyInformationCollModel.h"
#import "ICN_MyQuestionModel.h"
#import "ICN_MyActiveModel.h"
#import "ICN_AllCollectModel.h"
#import "ICN_DynStateContentModel.h" // 动态的Model
#import "ICN_QuestionListModel.h" // 提问列表model
@interface ICN_MyCollectingViewController ()<ICN_DynWarnViewDelegate>
{
    /*需要定义四个常量来判断当前状态都包括哪些类型的cell 为全部收藏做准备*/
    NSString * Zstr;
    NSString * ZIstr;
    NSString * Hstr;
    NSString * Tstr;
    NSMutableArray * arr;
    NSString * strType;
}

@property (nonatomic,assign) NSInteger  selectIndex;/*辛*/
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/

/*全部收藏的逻辑*/
@property (strong,nonatomic)NSMutableArray * positionListArr;/*字典第一层*/
@property (strong,nonatomic)NSMutableArray * informationListArr;/*字典第二层*/
@property (strong,nonatomic)NSMutableArray * questionListArr;/*字典第三层*/
@property (strong,nonatomic)NSMutableArray * activityListArr;/*字典第四层*/
@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作
@property (nonatomic , strong)ICN_YouMengShareModel *transmitModel;  // 转发用Model
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 将动态转发为动态用Model
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;
@property (strong,nonatomic)NSString * typeActive;/*判断状态单直播 还是 多直播*/
@end

@implementation ICN_MyCollectingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectIndex = 0;
    
    
    
    
   
    
    
    
    [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
    //[self.tableView.mj_header beginRefreshing];
}
#pragma mark - ---------- 懒加载 ----------
- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    return _warnView;
}
- (NSMutableArray *)allSourseArr{
    if (!_allSourseArr) {
        _allSourseArr = [NSMutableArray array];
    }
    return _allSourseArr;
}
- (NSMutableArray *)positionSourseArr{
    if (!_positionSourseArr) {
        _positionSourseArr = [NSMutableArray array];
    }
    return _positionSourseArr;
}
- (NSMutableArray *)informationSourseArr{
    if (!_informationSourseArr) {
        _informationSourseArr = [NSMutableArray array];
    }
    return _informationSourseArr;
}
- (NSMutableArray *)questionSourseArr{
    if (!_questionSourseArr) {
        _questionSourseArr = [NSMutableArray array];
    }
    return _questionSourseArr;
}
-(NSMutableArray *)activeSourseArr{
    if (!_activeSourseArr) {
        _activeSourseArr = [NSMutableArray array];
    }
    return _activeSourseArr;
}
#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark 类型选择
- (IBAction)typeBtnClick:(UIButton *)sender {
    for (int i = 0; i<5 ; i++) {
        if (sender.tag == 100 + i) {
            sender.selected = YES;
            [UIView animateWithDuration:0.1 animations:^{
                self.redLine.frame = CGRectMake((SCREEN_WIDTH/5+40)/5.0+SCREEN_WIDTH/5.0*i, self.typeBtn.frame.origin.y+self.typeBtn.frame.size.height-2.0, 30, 1);
            }];
            continue;
        }
        UIButton *btn = (UIButton *)[self.view viewWithTag:100+i];
        btn.selected = NO;
    }
    
    self.selectIndex = sender.tag-100;
    //    [self.tableView reloadData];
    /*判断当前点击位置是什么  全部 职位 资讯 活动 提问*/
    self.tableView.hidden = NO;
    self.noDataImage.hidden = YES;
    self.noDataLabel.hidden = YES;
    switch (self.selectIndex) {
        case 0:
        {
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            [self AllnetWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
        case 1:
        {
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self CollectnetWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
                [self CollectnetWorkRequest:TYPE_RELOADDATA_UP];
            }];
            [self CollectnetWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
        case 2:
        {
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self informationWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
                [self informationWorkRequest:TYPE_RELOADDATA_UP];
            }];
            [self informationWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
        case 3:
        {
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self ActiveWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
                [self ActiveWorkRequest:TYPE_RELOADDATA_UP];
            }];
            [self ActiveWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
        case 4:
        {
            self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
                [self QuestionWorkRequest:TYPE_RELOADDATA_DOWM];
            }];
            self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
                [self QuestionWorkRequest:TYPE_RELOADDATA_UP];
            }];
            [self QuestionWorkRequest:TYPE_RELOADDATA_DOWM];
        }
            break;
            
        default:
            break;
    }
    /*网络请求*/
    //
}
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark 我收藏的全部接口
- (void)AllnetWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_AllCollection para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_AllCollectModel * model=[[ICN_AllCollectModel alloc]initWithDictionary:obj error:nil];
                [self.allSourseArr addObject:model];
            }];
            if (self.allSourseArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.allSourseArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_AllCollection para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            /*判断data是什么类型*/
            if ([data isKindOfClass:[NSMutableArray class]]) {
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSLog(@"%@",obj);
                    
                    ICN_AllCollectModel * model=[[ICN_AllCollectModel alloc]initWithDictionary:obj error:nil];
                    [self.allSourseArr addObject:model];
                }];
            }
            NSLog(@"%@",self.allSourseArr);
//            if (self.allSourseArr.count == 0) {
//                self.tableView.hidden =YES;
//                self.noDataImage.hidden = NO;
//                self.noDataLabel.hidden = NO;
//                self.noDataLabel.text = @"暂无全部收藏信息";
//            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
    
}
#pragma mark 我的收藏职位网络请求
- (void)CollectnetWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_CollectionList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyCollcetPositionModel * model=[[ICN_MyCollcetPositionModel alloc]initWithDictionary:obj error:nil];
                [self.positionSourseArr addObject:model];
            }];
            if (self.positionSourseArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.positionSourseArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_CollectionList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyCollcetPositionModel * model=[[ICN_MyCollcetPositionModel alloc]initWithDictionary:obj error:nil];
                [self.positionSourseArr addObject:model];
            }];
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            [self endRefresh];
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无职位收藏信息";
            [self.tableView reloadData];
        }];
    }
}
#pragma mark 我的资讯职位网络请求
- (void)informationWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        [[HRRequest manager]POST:PATH_PostionNewslist para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyInformationCollModel * model=[[ICN_MyInformationCollModel alloc]initWithDictionary:obj error:nil];
                [self.informationSourseArr addObject:model];
            }];
            if (self.informationSourseArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%d",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.informationSourseArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_PostionNewslist para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyInformationCollModel * model=[[ICN_MyInformationCollModel alloc]initWithDictionary:obj error:nil];
                [self.informationSourseArr addObject:model];
            }];
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无资讯收藏信息";
            [self endRefresh];
            
            [self.tableView reloadData];
        }];
    }
}
#pragma mark 我的活动网络请求
- (void)ActiveWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        [[HRRequest manager]POST:PATH_ActivityList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyActiveModel * model=[[ICN_MyActiveModel alloc]initWithDictionary:obj error:nil];
                [self.activeSourseArr addObject:model];
                
            }];
            if (self.activeSourseArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.activeSourseArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_ActivityList para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyActiveModel * model=[[ICN_MyActiveModel alloc]initWithDictionary:obj error:nil];
                [self.activeSourseArr addObject:model];
            }];
            
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无活动收藏信息";
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
}
#pragma mark 我的提问网络请求
- (void)QuestionWorkRequest:(BOOL)isMore{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        [[HRRequest manager]POST:PATH_Questionlist para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyQuestionModel * model=[[ICN_MyQuestionModel alloc]initWithDictionary:obj error:nil];
                [self.questionSourseArr addObject:model];
            }];
            if (self.informationSourseArr.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.questionSourseArr = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"刷新中"];
        [[HRRequest manager]POST:PATH_Questionlist para:paramDic success:^(id data) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_MyQuestionModel * model=[[ICN_MyQuestionModel alloc]initWithDictionary:obj error:nil];
                [self.questionSourseArr addObject:model];
            }];
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden =YES;
            self.noDataImage.hidden = NO;
            self.noDataLabel.hidden = NO;
            self.noDataLabel.text = @"暂无提问收藏信息";
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
}
#pragma mark 我的收藏职位删除
- (void)applyDeleteposition:(NSString *)applyId{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    [[HRRequest manager]POST:PATH_CancelPosition para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
    }];
}
#pragma mark 我的资讯职位删除
- (void)applyDeleteinformation:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"informationId":applyId,
                               };
    [[HRRequest manager]POST:PATH_positionNewsCollectDel para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
    }];
    
}
#pragma mark 我的活动职位删除
- (void)applyDeleteActive:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    [[HRRequest manager]POST:PATH_DeleteActivity para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
    }];
}
#pragma mark 我的提问职位删除
- (void)applyDeleteQuestion:(NSString *)applyId{
    //    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    [[HRRequest manager]POST:PATH_DelcollecQuestion para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
        [self.tableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除失败"];
    }];
}
#pragma mark 我的提问取消收藏
-(void)cancelPraiseAF:(NSString*)starID{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":starID,
                               };
    [[HRRequest manager]POST:PATH_QuestionListDisLikeUp para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏成功"];
        [self QuestionWorkRequest:TYPE_RELOADDATA_DOWM];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏失败"];
    }];
}
#pragma mark 我的提问收藏
-(void)PraiseAF:(NSString*)starID{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":starID,
                               };
    [[HRRequest manager]POST:PATH_QuestionListLikeUp para:paramDic success:^(id data) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
        [self QuestionWorkRequest:TYPE_RELOADDATA_DOWM];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏失败"];
    }];
}
// 转发具体内容到动态的实现方法
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:self.transmitModel.content IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
    }
}
#pragma mark 停止刷新
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    if (self.allSourseArr.count == 0 ||self.allSourseArr == nil) {
        ICN_AllCollectModel * model = self.allSourseArr[indexPath.row];
        strType = model.type;
        /*判断是不是单直播还是多直播*/
        NSLog(@"%@", model.activityNum);
        self.typeActive =  model.activityNum;
    }
   
    switch (self.selectIndex) {
        case 0:
        {
            switch ([strType integerValue]) {
                case 1:
                {
                    return 86;
                }
                    break;
                case 2:
                {
                    return 106;
                }
                    break;
                case 3:
                {/*单直播 1 多直播 2 */
                    
                    ICN_AllCollectModel * model = self.allSourseArr[indexPath.row];
                    if ([model.activityNum isEqualToString:@"1"]) {
                        return 130;
                    }else{
                        return 105;
                    }
                }
                    break;
                case 4:
                {
                    return 200;
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
        case 1:
        {
            return  86;
        }
            break;
        case 2:
        {
            return  106;
        }
            break;
        case 3:
        {
            ICN_MyActiveModel * model = self.activeSourseArr[indexPath.row];
            /*通过type判断当前状态是什么*/
            if (model.type == 1) {
                 return 130;
            }else{
                return 105;
            }
        }
            break;
        case 4:
        {
            return 200;
        }
            break;
            
        default:
            break;
    }
    return 0;
}
#pragma mark - ---------- UITableViewDataSource ----------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    switch (self.selectIndex) {
        case 0:
        {
            return self.allSourseArr.count;
        }
            break;
        case 1:
        {
            return self.positionSourseArr.count;
        }
            break;
        case 2:
        {
            return self.informationSourseArr.count;
        }
            break;
        case 3:
        {
            return self.activeSourseArr.count;
        }
            break;
        case 4:
        {
            return self.questionSourseArr.count;
        }
            break;
            
        default:
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    
    ICN_AllCollectModel * model = self.allSourseArr[indexPath.row];
    strType = model.type;
    switch (self.selectIndex) {
        case 0:
        {
            /*还需要对里面的数据进行判断 判断是什么类型的 1职位 2 资讯 3 活动 4 提问*/
            switch ([strType integerValue]) {
                case 1:
                {
                    ICN_MyCollcetingTableViewCell * cell =XIB(ICN_MyCollcetingTableViewCell);
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    /*去掉系统点击阴影*/
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    /*赋值*/
                    if (!(self.allSourseArr == nil)||!(self.allSourseArr.count == 0)) {
                        cell.allModel = self.allSourseArr[indexPath.row];
                    }
                    return  cell;
                }
                    break;
                case 2:
                {
                    ICN_CollectInformationTableViewCell * cell = XIB(ICN_CollectInformationTableViewCell);
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    /*去掉系统点击阴影*/
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    if (!(self.allSourseArr == nil)||!(self.allSourseArr.count == 0)) {
                        /*赋值*/
                        cell.modelall = self.allSourseArr[indexPath.row];
                    }
                   
                    return  cell;
                }
                    break;
                case 3:
                {
                    ICN_CollecActiveTableViewCell * cell = XIB(ICN_CollecActiveTableViewCell);
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    /*去掉系统点击阴影*/
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                    /*赋值*/
                    if (!(self.allSourseArr == nil)||!(self.allSourseArr.count == 0)) {
                        cell.allmodel = self.allSourseArr[indexPath.row];
                    }
                    return  cell;
                }
                    break;
                case 4:
                {
                    ICN_CollectQuestionsTableViewCell * cell = XIB(ICN_CollectQuestionsTableViewCell);
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    /*去掉系统点击阴影*/
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    /*赋值*/
                    if (!(self.allSourseArr == nil)||!(self.allSourseArr.count == 0)) {
                        cell.modelall = self.allSourseArr[indexPath.row];
                    }
                    return  cell;
                }
                    break;
                    
                default:
                    break;
            }
            
        }
            break;
        case 1:
        {
            ICN_MyCollcetingTableViewCell * cell =XIB(ICN_MyCollcetingTableViewCell);
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            /*赋值*/
            if (!(self.positionSourseArr == nil)||!(self.positionSourseArr.count == 0)) {
                /*赋值*/
                cell.model = self.positionSourseArr[indexPath.row];
            }
            
            return  cell;
        }
            break;
        case 2:
        {
            ICN_CollectInformationTableViewCell * cell = XIB(ICN_CollectInformationTableViewCell);
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            /*赋值*/
            if (!(self.informationSourseArr == nil)||!(self.informationSourseArr.count == 0)) {
                /*赋值*/
                cell.model = self.informationSourseArr[indexPath.row];
            }
            
            return  cell;
        }
            break;
        case 3:
        {
            ICN_CollecActiveTableViewCell * cell = XIB(ICN_CollecActiveTableViewCell);
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            /*赋值*/
            if (!(self.activeSourseArr == nil)||!(self.activeSourseArr.count == 0)) {
                cell.model = self.activeSourseArr[indexPath.row];
            }
            
            return  cell;
            
        }
            break;
        case 4:
        {
            ICN_CollectQuestionsTableViewCell * cell = XIB(ICN_CollectQuestionsTableViewCell);
            cell.accessoryType = UITableViewCellAccessoryNone;
            /*去掉系统点击阴影*/
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            /*赋值*/
            if (!(self.questionSourseArr == nil)||!(self.questionSourseArr.count == 0)) {
                /*赋值*/
                cell.model = self.questionSourseArr[indexPath.row];
            }
           
            
            /*点赞 type 0 点赞 1 未点赞 */
            [cell ReturnStarClick:^(NSString *type, UIButton *btn,NSString * starID) {
                switch ([type integerValue]) {
                    case 0:
                    {
                        /*取消点赞网络请求*/
                        [self cancelPraiseAF:starID];
                    }
                        break;
                    case 1:
                    {
                        /*点赞网络请求*/
                        [self PraiseAF:starID];
                    }
                        break;
                        
                    default:
                        break;
                }
            }];
            /*转发*/
            [cell ReturnForwardingClick:^{
                // 2. 弹出转发窗口
                [self.view addSubview:self.warnView];
            }];
            /*回答*/
            [cell ReturnAskClick:^{
                ICN_MYAnswerQuestionPager * vc = [[ICN_MYAnswerQuestionPager alloc]init];
                ICN_MyQuestionModel * model = self.questionSourseArr[indexPath.row];
                vc.questionId = model.questID;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            return  cell;
        }
            break;
        default:
            break;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*调入详情页面*/
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    switch (self.selectIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            ICN_PositionNextOneViewController * vc =[[ICN_PositionNextOneViewController alloc]init];
            ICN_MyCollcetPositionModel*model = self.positionSourseArr[indexPath.row];
            vc.url = model.positionId;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            ICN_IndustryNextViewController * vc =[[ICN_IndustryNextViewController alloc]init];
            ICN_MyInformationCollModel * model =self.informationSourseArr[indexPath.row];
            vc.url = model.infoid;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
        {
            ICN_MyActiveModel* model = self.activeSourseArr[indexPath.row];
            /*单直播*/
            if (model.type == 1) {
                ICN_LiveDetialViewController * vc = [[ICN_LiveDetialViewController alloc]init];
                vc.liveID = model.infoList.liveId;
                vc.actID = model.infoList.activityId;
                 [self.navigationController pushViewController:vc animated:YES];
                
            }else{
                ICN_ActivityDetialViewController * vc =[[ICN_ActivityDetialViewController alloc]init];
                vc.packID = model.infoList.activityId;
                [self.navigationController pushViewController:vc animated:YES];
            }
            
        }
            break;
        case 4:
        {
            
            // 提问详情
            // 获取到需要的Model
            ICN_MyQuestionModel *model = self.questionSourseArr[indexPath.row];
            ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
            pager.contentId = model.questID;
            [self currentPagerJumpToPager:pager];
            
        }
            break;
        default:
            break;
    }
    
}
#pragma mark --- UITableViewDelegate ---
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    /*进行枚举判断 0 全部 1 职位 2 资讯 3 活动 4 提问 */
    switch (self.selectIndex) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            ICN_MyCollcetPositionModel *model = self.positionSourseArr[indexPath.row];
            [self.positionSourseArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteposition:model.positionId];
        }
            break;
        case 2:
        {
            ICN_MyInformationCollModel *model = self.informationSourseArr[indexPath.row];
            [self.informationSourseArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteinformation:model.infoid];
        }
            break;
        case 3:
        {
            ICN_MyActiveModel *model = self.activeSourseArr[indexPath.row];
            [self.activeSourseArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteActive:model.infoList.baseId];
        }
            break;
        case 4:
        {
            ICN_MyQuestionModel *model = self.questionSourseArr[indexPath.row];
            [self.questionSourseArr removeObjectAtIndex:indexPath.row];
            [self applyDeleteQuestion:model.questID];
        }
            break;
        default:
            break;
    }
    // 刷新列表
    //    [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.row] withRowAnimation:UITableViewRowAnimationRight];
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.selectIndex) {
        case 0:
        {
            return UITableViewCellEditingStyleNone;
        }
            break;
        case 1:
        {
            return UITableViewCellEditingStyleDelete;
        }
            break;
        case 2:
        {
            return UITableViewCellEditingStyleDelete;
        }
            break;
        case 3:
        {
            return UITableViewCellEditingStyleDelete;
        }
            break;
        case 4:
        {
            return UITableViewCellEditingStyleDelete;
        }
            break;
            
        default:
            break;
    }
    return UITableViewCellEditingStyleNone;
}
-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath {
    return @"取消收藏";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma mark --- 转发窗代理 - ICN_DynWarnViewDelegate ---

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            // 点击底部取消视图按钮
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            // 分享到朋友圈
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            // 分享到QQ空间
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请您去动态首页提问进行转发"];
        }
        default:
            break;
    }
    
    [self.warnView removeFromSuperview];
}

@end
