//
//  ICN_CollectQuestionsTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CollectQuestionsTableViewCell.h"

@implementation ICN_CollectQuestionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark 点赞
- (IBAction)starBtnClick:(UIButton *)sender {
    if (self.returnStarClick) {
        self.returnStarClick(self.iscollect,sender,self.questID);
    }
}
#pragma mark 转发
- (IBAction)forwardBtnClick:(UIButton *)sender {
    if (self.returnForwardingClick) {
        self.returnForwardingClick(sender);
    }
}
#pragma mark 回答
- (IBAction)askBtnClick:(UIButton *)sender {
    if (self.returnAskClick) {
        self.returnAskClick(sender);
    }
}


-(void)ReturnAskClick:(ReturnAskClick)block{
    self.returnAskClick = block;
}
-(void)ReturnStarClick:(ReturnStarClick)block{
    self.returnStarClick = block;
}
-(void)ReturnForwardingClick:(ReturnForwardingClick)block{
    self.returnForwardingClick = block;
}
- (void)setModelall:(ICN_AllCollectModel *)modelall{
    _modelall = modelall;
    self.contant.text = modelall.contant;
    self.position.text = modelall.position;
    self.questID = modelall.questID;
    self.iscollect = modelall.iscollect;
    self.plusv = modelall.plusv;
    /*判断是否显示v字*/
    if ([modelall.plusv isEqualToString:@"1"]) {
        self.plusvImage.hidden = YES;
    }
    
    
    if ([self.iscollect isEqualToString:@"0"]) {
        [self.collectNum setImage:[UIImage imageNamed:@"收藏-选中"] forState:UIControlStateNormal];
    }else{
        [self.collectNum setImage:[UIImage imageNamed:@"收藏-职场"] forState:UIControlStateNormal];
    }
    
    self.company.text = modelall.company;
    self.title.text = modelall.title;
    self.memberNick.text = modelall.memberNick;
    self.createTime.text = modelall.createTime;
    self.IndustryLabel.text = modelall.IndustryLabel;
    [self.collectNum setTitle:modelall.collectNum forState:UIControlStateNormal];
    [self.memberLogo sd_setImageWithURL:[NSURL URLWithString:modelall.memberLogo]];/*头像需要拼接*/
    
    self.score.text = modelall.score;

}

-(void)setModel:(ICN_MyQuestionModel *)model{
    _model = model;
    self.contant.text = model.contant;
    self.position.text = model.position;
    self.questID = model.questID;
    self.iscollect = model.iscollect;
    if ([self.iscollect isEqualToString:@"0"]) {
        [self.collectNum setImage:[UIImage imageNamed:@"收藏-选中"] forState:UIControlStateNormal];
    }else{
        [self.collectNum setImage:[UIImage imageNamed:@"收藏-职场"] forState:UIControlStateNormal];
    }
    
    self.plusv = model.plusv;
    /*判断是否显示v字*/
    if ([model.plusv isEqualToString:@"1"]) {
        self.plusvImage.hidden = YES;
    }
    self.company.text = model.company;
    self.title.text = model.title;
    self.memberNick.text = model.memberNick;
    self.createTime.text = model.createTime;
    self.IndustryLabel.text = model.IndustryLabel;
    [self.collectNum setTitle:model.collectNum forState:UIControlStateNormal];
     [self.memberLogo sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    self.score.text = model.score;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
