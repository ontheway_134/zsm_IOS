//
//  ICN_CollectQuestionsTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyQuestionModel.h"
#import "ICN_AllCollectModel.h"

typedef void (^ReturnStarClick)(NSString * type,UIButton * btn,NSString * starID);/*星星*/
typedef void (^ReturnForwardingClick)();/*转发*/
typedef void (^ReturnAskClick)();/*回答*/
@interface ICN_CollectQuestionsTableViewCell : UITableViewCell
@property (strong,nonatomic)ICN_MyQuestionModel * model;
@property (strong,nonatomic)ICN_AllCollectModel * modelall;
@property (weak, nonatomic) IBOutlet UILabel *contant;
@property (weak, nonatomic) IBOutlet UILabel *position;
@property (strong,nonatomic)NSString * questID;
@property (strong,nonatomic)NSString * iscollect;
@property (weak, nonatomic) IBOutlet UIImageView *plusvImage;/*判断是否显示V*/
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *memberNick;
@property (strong,nonatomic)NSString * memberId;
@property (weak, nonatomic) IBOutlet UILabel *createTime;
@property (weak, nonatomic) IBOutlet UILabel *IndustryLabel;
@property (strong,nonatomic)NSString * questionId;
@property (weak, nonatomic) IBOutlet UIButton *collectNum;
@property (weak, nonatomic) IBOutlet UIImageView *memberLogo;
@property (weak, nonatomic) IBOutlet UILabel *score;
@property (strong,nonatomic)NSString * collectId;
@property (strong,nonatomic)NSString * plusv;


-(void)ReturnStarClick:(ReturnStarClick)block;
@property (strong,nonatomic)ReturnStarClick  returnStarClick;
-(void)ReturnForwardingClick:(ReturnForwardingClick)block;
@property (strong,nonatomic)ReturnForwardingClick returnForwardingClick;
-(void)ReturnAskClick:(ReturnAskClick)block;
@property (strong,nonatomic)ReturnAskClick returnAskClick;

@end
