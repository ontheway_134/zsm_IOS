//
//  ICN_CollectInformationTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CollectInformationTableViewCell.h"
#import "NSString+Date.h"
@implementation ICN_CollectInformationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(ICN_MyInformationCollModel *)model{
    _model = model;
    self.content.text = model.title;
    self.time.text = model.createDate;
    self.see.text = model.pageViews;
    [self.url sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    self.collectID = model.infoid;
//    self.url.text = model.title;
    
    
}

- (void)setModelall:(ICN_AllCollectModel *)modelall{
    _modelall = modelall;
    self.content.text = modelall.title;
    self.time.text = [NSString conversionTimeStamp:modelall.createDate];/*全部时候需要转换形式*/
    self.see.text = modelall.pageViews;
    [self.url sd_setImageWithURL:[NSURL URLWithString:modelall.pic]];
    
    self.collectID = modelall.infoid;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
