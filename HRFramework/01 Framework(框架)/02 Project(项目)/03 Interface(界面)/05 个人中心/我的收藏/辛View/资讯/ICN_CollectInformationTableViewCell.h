//
//  ICN_CollectInformationTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyInformationCollModel.h"
#import "ICN_AllCollectModel.h"
@interface ICN_CollectInformationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *see;
@property (weak, nonatomic) IBOutlet UIImageView *url;
@property (strong,nonatomic)ICN_MyInformationCollModel * model;
@property (strong,nonatomic)ICN_AllCollectModel * modelall;
@property (strong,nonatomic)NSString * collectID;
@end
