//
//  ICN_MyCollcetingTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_AllCollectModel.h"
@class ICN_MyCollcetPositionModel;

@interface ICN_MyCollcetingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
@property (weak, nonatomic) IBOutlet UILabel *positionTitle;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *workType;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;


/**
 教育 金融 等 文字放置的view
 */
@property (nonatomic, strong) ICN_MyCollcetPositionModel *model;
@property (strong,nonatomic)ICN_AllCollectModel * allModel;
@end
