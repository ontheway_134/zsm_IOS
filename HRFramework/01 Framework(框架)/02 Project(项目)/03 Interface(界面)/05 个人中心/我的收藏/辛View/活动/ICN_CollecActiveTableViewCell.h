//
//  ICN_CollecActiveTableViewCell.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_MyActiveModel.h"
#import "ICN_AllCollectModel.h"
@interface ICN_CollecActiveTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *url;
@property (weak, nonatomic) IBOutlet UILabel *peopleCount;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIImageView *renImage;
@property (weak, nonatomic) IBOutlet UIImageView *renTImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *renImageWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong,nonatomic)ICN_MyActiveModel * model;
@property (weak, nonatomic) IBOutlet UILabel *topPrice;
@property (weak, nonatomic) IBOutlet UILabel *topTitle;
@property (strong,nonatomic)ICN_AllCollectModel * allmodel;
@end
