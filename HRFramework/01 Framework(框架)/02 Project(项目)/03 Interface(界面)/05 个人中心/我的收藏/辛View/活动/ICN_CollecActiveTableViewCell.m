//
//  ICN_CollecActiveTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CollecActiveTableViewCell.h"
#import "NSString+Date.h"
@implementation ICN_CollecActiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setAllmodel:(ICN_AllCollectModel *)allmodel{
    
    _allmodel = allmodel;
    /*判断是单直播还是多直播 model.activityNum  1 单直播 2 多直播 */
    /*判断是否是多直播 还是单直播 type = 1单直播 type = 0 多直播*/
    if ([allmodel.activityNum isEqualToString:@"1"] ) {
        
        
        [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(allmodel.pic)]];
        self.imageWidth.constant = 70;
        self.url.layer.cornerRadius= 35;
        self.url.layer.masksToBounds = YES;
        self.url.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
        self.url.layer.borderWidth = 0.5;
        self.topTitle.text = allmodel.title;
        self.price.hidden = YES;
        self.title.text = allmodel.memberNick;
        /*判断钱是不是0*/
        if ([allmodel.price  doubleValue] == 0) {
            self.topPrice.text  = @"免费";
        }else{
            self.topPrice.text = allmodel.price;
        }
        self.peopleCount.text = [NSString stringWithFormat:@"%@到%@",[NSString conversionTimeStamp:allmodel.beginDate],[NSString conversionTimeStamp:allmodel.endDate]];
        //        self.time.text = [NSString conversionTimeStamp:model.infoList.beginDate];
        self.time.hidden = YES;
        self.renImage.hidden = NO;
        
        self.content.text = [NSString stringWithFormat:@"%@/%@",allmodel.subNum,allmodel.canNum];
        [self.renTImage setImage:[UIImage imageNamed:@"时间"]];
        
    }
    if ([allmodel.activityNum  isEqualToString:@"2"] ) {
        self.topView.hidden = YES;
        self.topViewHeight.constant = 0;
        self.renImageWidth.constant = 0;
        self.title.text = allmodel.title;
        [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(allmodel.pic)]];
        /*判断钱是不是0*/
        if ([allmodel.price  doubleValue] == 0) {
            self.price.text  = @"免费";
        }else{
            self.price.text = allmodel.price;
        }
        self.peopleCount.text = [NSString stringWithFormat:@"%@/%@",allmodel.subNum,allmodel.canNum];
        self.time.text = [NSString conversionTimeStamp:allmodel.beginDate];
        self.content.text = allmodel.content;
        [self.renTImage setImage:[UIImage imageNamed:@"报名人数"]];
        self.renImage.hidden = YES;
    }

//    self.topViewHeight.constant = 0;
//    self.topView.hidden = YES;
//    [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(allmodel.pic)]];
//    self.title.text = allmodel.title;
//    /*判断钱是不是0*/
//    if ([allmodel.price  doubleValue] == 0) {
//        self.price.text  = @"免费";
//    }else{
//        self.price.text = allmodel.price;
//    }
//    self.peopleCount.text = [NSString stringWithFormat:@"%@/%@",allmodel.subNum,allmodel.canNum];
//    self.time.text = [NSString conversionTimeStamp:allmodel.beginDate];
//    self.content.text = allmodel.content;
}
-(void)setModel:(ICN_MyActiveModel *)model{
    
    _model = model;
    
    
    
    /*判断是否是多直播 还是单直播 type = 1单直播 type = 0 多直播*/
    if (model.type  ==1 ) {
        
        
        [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
        self.imageWidth.constant = 70;
        self.url.layer.cornerRadius= 35;
        self.url.layer.masksToBounds = YES;
        self.url.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
        self.url.layer.borderWidth = 0.5;
        self.topTitle.text = model.infoList.title;
        self.price.hidden = YES;
        self.title.text = model.infoList.memberNick;
        /*判断钱是不是0*/
        if ([model.infoList.price  doubleValue] == 0) {
            self.topPrice.text  = @"免费";
        }else{
            self.topPrice.text = model.infoList.price;
        }
        self.peopleCount.text = [NSString stringWithFormat:@"%@到%@",[NSString conversionTimeStamp:model.infoList.beginDate],[NSString conversionTimeStamp:model.infoList.endDate]];
//        self.time.text = [NSString conversionTimeStamp:model.infoList.beginDate];
        self.time.hidden = YES;
        self.renImage.hidden = NO;
        self.content.text = [NSString stringWithFormat:@"%@/%@",model.infoList.subNum,model.infoList.canNum];
        [self.renTImage setImage:[UIImage imageNamed:@"时间"]];
        
    }
    if (model.type  ==0 ) {
        self.topView.hidden = YES;
        self.topViewHeight.constant = 0;
        self.renImageWidth.constant = 0;
        self.title.text = model.infoList.title;
        [self.url sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)]];
        /*判断钱是不是0*/
        if ([model.infoList.price  doubleValue] == 0) {
            self.price.text  = @"免费";
        }else{
            self.price.text = model.infoList.price;
        }
        self.peopleCount.text = [NSString stringWithFormat:@"%@/%@",model.infoList.subNum,model.infoList.canNum];
        self.time.text = [NSString conversionTimeStamp:model.infoList.beginDate];
        self.content.text = model.infoList.content;
        [self.renTImage setImage:[UIImage imageNamed:@"报名人数"]];
        self.renImage.hidden = YES;
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
