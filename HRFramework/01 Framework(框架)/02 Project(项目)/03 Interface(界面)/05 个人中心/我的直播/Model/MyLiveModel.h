//
//  MyLiveModel.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface MyLiveModel : BaseOptionalModel
@property (strong,nonatomic)NSString * address;
@property (strong,nonatomic)NSString * beginDate;
@property (strong,nonatomic)NSString * canNum;
@property (strong,nonatomic)NSString * companyName;
@property (strong,nonatomic)NSString * endDate;
@property (strong,nonatomic)NSString * liveID;
@property (strong,nonatomic)NSString * isAdopt;
@property (strong,nonatomic)NSString * memberId;
@property (strong,nonatomic)NSString * memberNick;
@property (strong,nonatomic)NSString * memberLogo;
@property (strong,nonatomic)NSString * memberPosition;
@property (strong,nonatomic)NSString * status;
@property (strong,nonatomic)NSString * subNum;
@property (strong,nonatomic)NSString * title;
@property (nonatomic,strong) NSString * streamName; //直播流
@end
