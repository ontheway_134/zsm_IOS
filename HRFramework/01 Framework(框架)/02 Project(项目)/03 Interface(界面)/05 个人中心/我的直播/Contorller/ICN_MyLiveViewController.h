//
//  ICN_MyLiveViewController.h
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_MyLiveViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
/*******************网络请求部分逻辑*********************/
@property (strong,nonatomic)NSMutableArray * baseSource;/*数据源*/
@property (strong,nonatomic)NSString * plusv;/*判断是不是牛人*/
@end
