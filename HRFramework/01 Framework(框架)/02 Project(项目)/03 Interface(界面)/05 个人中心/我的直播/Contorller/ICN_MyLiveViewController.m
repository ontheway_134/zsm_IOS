//
//  ICN_MyLiveViewController.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyLiveViewController.h"
#import "ICN_WantLiveViewController.h"/*我要直播*/
#import "ICN_MyLiveTableViewCell.h"/*我的直播*/
#import "AlivcLiveViewController.h"/*直播*/
#import "NSString+Date.h"
#import "NSDate+TimeStamp.h"
#import "MyLiveModel.h"
#import "MJDIYAutoFooter.h"
@interface ICN_MyLiveViewController ()
@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@property (weak, nonatomic) IBOutlet UILabel *noDataLabel;/*无数据展示文字*/
@property (weak, nonatomic) IBOutlet UIImageView *noDataImage;

@end

@implementation ICN_MyLiveViewController


#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)baseSource{
    if (!_baseSource) {
        _baseSource = [NSMutableArray array];
    }
    return _baseSource;
}
#pragma mark - ---------- 生命周期 ----------
-(void)viewWillAppear:(BOOL)animated{
    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
#pragma mark - ---------- 重写属性合成器 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        });
       
    }];
    
    self.tableView.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
        
        [self netWorkRequest:TYPE_RELOADDATA_UP];
    }];
//    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    // [self.tableView.mj_header beginRefreshing];
}

#pragma mark - ---------- IBActions ----------
#pragma mark 返回按钮
- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark 我要直播按钮
- (IBAction)doLiveBtnClick:(UIButton *)sender {
    
    if ([self.plusv isEqualToString:@"1"]) {
        ICN_WantLiveViewController * vc = [[ICN_WantLiveViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"没有认证的牛人不能发布直播"];
    }
    
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求--
- (void)netWorkRequest:(BOOL)isMore{
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (isMore) {
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   
                                   };
        
        [[HRRequest manager]POST:PATH_MyLiveList para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                MyLiveModel * model=[[MyLiveModel alloc]initWithDictionary:obj error:nil];
                if (model == nil) {
                    [self.baseSource addObject:model];
                }
                
            }];
            if (self.baseSource.count == 0) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
                self.currentPage = SF(@"%ld",[self.currentPage integerValue] - 1);
                [self endRefresh];
                return ;
            }else{
                [self endRefresh];
            }
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            [self endRefresh];
        }];
    }else{
        self.currentPage = @"1";
        self.baseSource = nil;
        NSDictionary *paramDic = @{
                                   @"token":token,
                                   @"page":self.currentPage,
                                   };
        NSLog(@"%@",paramDic);
        [[HRRequest manager]POST:PATH_MyLiveList para:paramDic success:^(id data) {
            NSLog(@"%@",data);
            self.noDataLabel.hidden = YES;
            self.noDataImage.hidden = YES;
            /*判断data是什么类型*/
            if ([data isKindOfClass:[NSMutableArray class]]) {
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSLog(@"%@",obj);
                    
                    MyLiveModel * model=[[MyLiveModel alloc]initWithDictionary:obj error:nil];
                    [self.baseSource addObject:model];
                    
                }];
            }
            NSLog(@"%@",self.baseSource);
            if (self.baseSource.count == 0) {
                self.tableView.hidden = YES;
                self.noDataLabel.hidden = NO;
                self.noDataImage.hidden = NO;
            }
            [self endRefresh];
            [self.tableView reloadData];
        } faiulre:^(NSString *errMsg) {
            NSLog(@"%@",errMsg);
            self.currentPage = @"1";
            self.tableView.hidden = YES;
            self.noDataLabel.hidden = NO;
            self.noDataImage.hidden = NO;
            [self endRefresh];
            [self.tableView reloadData];
        }];
    }
}
#pragma mark    删除数据网络请求
- (void)applyDelete:(NSString *)applyId{
    WEAK(weakSelf);
    /*初始化token*/
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSDictionary *paramDic = @{@"token":token,
                               @"id":applyId,
                               };
    
    [[HRRequest manager]POST:PATH_DeleLive para:paramDic success:^(id data) {
        NSLog(@"%@",data);
        [MBProgressHUD ShowProgressWithBaseView:weakSelf.view Message:@"删除成功"];
        [self netWorkRequest:TYPE_RELOADDATA_DOWM];
        [self.tableView reloadData];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [MBProgressHUD ShowProgressWithBaseView:weakSelf.view Message:@"删除失败"];
    }];
}
-(void)endRefresh{
    [self.tableView.mj_footer endRefreshing];
    [self.tableView.mj_header endRefreshing];
}
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark - ---------- UITableViewDelegate ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 112;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*去掉系统线*/
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    ICN_MyLiveTableViewCell * cell = [[NSBundle mainBundle]loadNibNamed:@"ICN_MyLiveTableViewCell" owner:nil options:nil].lastObject;
    cell.accessoryType = UITableViewCellAccessoryNone;
    /*去掉系统点击阴影*/
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (!(self.baseSource == nil)||!(self.baseSource.count == 0)) {
        cell.model = self.baseSource[indexPath.row];
    }
    /*赋值*/

    return  cell;
}
#pragma mark - ---------- UITableViewDataSource ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.baseSource.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1 ;
}
#pragma mark --- UITableViewDelegate ---
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    MyLiveModel *model = self.baseSource[indexPath.row];
    [self.baseSource removeObjectAtIndex:indexPath.row];
    [self applyDelete:model.liveID];
}
-(NSString*)tableView:(UITableView*)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath*)indexpath {
    return @"删除";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //@"rtmp://video-center.alivecdn.com/ixtest/liveTest?vhost=live.1ican.com"
    MyLiveModel *model = self.baseSource[indexPath.row];
    NSLog(@"%@",self.baseSource);
    NSLog(@"%@",model.isAdopt);
    
//    NSString *pushUrl = SF(@"rtmp://video-center.alivecdn.com/ixtest/%@?vhost=live.1ican.com",model.streamName) ;
//    AlivcLiveViewController *live = [[AlivcLiveViewController alloc] initWithNibName:@"AlivcLiveViewController" bundle:nil url:pushUrl];
//    live.isScreenHorizontal = NO;
//    live.hidesBottomBarWhenPushed = YES;
//    live.liveID = model.liveID;
//    [self.navigationController pushViewController:live animated:YES];
//    return;
    
    /*这里需要判断当前直播是否经过审核 经过审核才能进入详情页面 0审核中  1已通过  2已拒绝*/
    if ([model.isAdopt isEqualToString:@"0"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"直播处于审核中"];
        
    }else if ([model.isAdopt isEqualToString:@"1"]){
        NSString *pushUrl = SF(@"rtmp://video-center.alivecdn.com/ixtest/%@?vhost=live.1ican.com",model.streamName) ;
        // NSString *pushUrl = SF(@"rtmp://video-center.alivecdn.com/ixtest/liveTest?vhost=live.1ican.com");
        
        NSLog(@"%@",pushUrl);
        if (![pushUrl containsString:@"rtmp://"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"推流地址格式错误，无法直播" message:nil delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
        NSDate *date = [NSDate date];
        NSString *comp = [NSDate currentTimeStamp10];
        if ([comp integerValue] > [model.endDate integerValue]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"直播已结束"];
            return;
        }
        //NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:model.endDate];
        AlivcLiveViewController *live = [[AlivcLiveViewController alloc] initWithNibName:@"AlivcLiveViewController" bundle:nil url:pushUrl];
        live.isScreenHorizontal = NO;
        live.hidesBottomBarWhenPushed = YES;
        live.liveID = model.liveID;
        [self.navigationController pushViewController:live animated:YES];
        
    }else if ([model.isAdopt isEqualToString:@"2"]){
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"直播已被拒绝"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
