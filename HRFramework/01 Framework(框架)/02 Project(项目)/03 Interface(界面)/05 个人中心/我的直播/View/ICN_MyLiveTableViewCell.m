//
//  ICN_MyLiveTableViewCell.m
//  ICan
//
//  Created by 辛忠志 on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyLiveTableViewCell.h"
#import "NSString+Date.h"
@implementation ICN_MyLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(MyLiveModel *)model{
    model = model;
    self.titleLabel.text = model.title;
    self.imageUrl.layer.cornerRadius= 18;
    self.imageUrl.layer.masksToBounds = YES;
    self.imageUrl.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
    self.imageUrl.layer.borderWidth = 0.5;
    [self.imageUrl sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.nameLabel.text = model.memberNick;
    //memberPosition
    self.companyLabel.text = [NSString stringWithFormat:@"%@|%@",model.companyName,model.memberPosition];
    self.timeLabel.text = [NSString conversionTimeStamp:model.beginDate];
    self.timeLabel.text = [NSString stringWithFormat:@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]];
    
    /*通过状态来判断UI*/
    if ([model.isAdopt isEqualToString:@"0"]) {
        self.stateLabel.text = @"审核中";
    }else if ([model.isAdopt isEqualToString:@"1"]){
        self.stateLabel.text = @"已通过";
        self.stateLabel.textColor = RGB0X(0x009dff);
    }else if ([model.isAdopt isEqualToString:@"2"]){
        self.stateLabel.text = @"已拒绝";
        self.stateLabel.textColor = RGB0X(0x333333);
    }else{
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
