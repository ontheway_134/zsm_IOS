//
//  ICN_GuiderPager.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_GuiderPager.h"

#pragma mark - ---------- 视图头文件 ----------
#import "ICN_LoginBtn.h" // 登录按钮
#import "ICN_RegisterBtn.h" // 注册按钮
#import "ICN_VisitorLoginBtn.h" // 游客登录按钮
#import "ICN_ TrilateralLoginBtn.h" // 三方登录按钮
#import "ICN_GuiderPageControl.h" // 引导页page control
#import "ICN_GuiderScroller.h" // 引导页轮播图



#pragma mark - ---------- 资源头文件 ----------
#import "BaseOptionalModel.h" // 
#import "ICN_AdvertimistModel.h" // 轮播图网络请求Model

@interface ICN_GuiderPager ()

@property (weak, nonatomic) IBOutlet ICN_VisitorLoginBtn *visitorLoginBtn; // 游客登录按钮
@property (weak, nonatomic) IBOutlet ICN_RegisterBtn *registerBtn; // 注册按钮
@property (weak, nonatomic) IBOutlet ICN_LoginBtn *loginBtn; // 登录按钮

@property (weak, nonatomic) IBOutlet UILabel *trilateralLabel; // 标识三方登录标签
@property (weak, nonatomic) IBOutlet ICN__TrilateralLoginBtn *QQLoginBtn; // QQ登录按钮
@property (weak, nonatomic) IBOutlet ICN__TrilateralLoginBtn *WeiBoLoginBtn; // 微博登录按钮
@property (weak, nonatomic) IBOutlet ICN__TrilateralLoginBtn *WeiChatLoginBtn; // 微信登录按钮

@property (weak, nonatomic) IBOutlet ICN_GuiderPageControl *picturePageController; // 图片page control 控制器
@property (weak, nonatomic) IBOutlet ICN_GuiderScroller *pictureScroller; // 图片轮播视图

#pragma mark - ---------- 页面基本属性 ----------

@property (nonatomic , strong)NSMutableArray *guiderPictureUrlsArr; // 引导页图片网址数组
@property (strong, nonatomic) CAGradientLayer *gradientLayer;

#pragma mark - ---------- 约束属性 ----------
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTopContraints; // 三方登录label顶部约束

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginBtnTopContraints; // 登录注册页面距离顶部约束

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelBottomContraints; // 三方登录按钮底部约束



@end

@implementation ICN_GuiderPager

#pragma mark - ---------- 网络请求 ----------

// 获取引导页图片网址接口
- (void)requestWithGuiderPNGS{
    [[HRRequest alloc] POST_PATH:PATH_GUIDPICS params:nil success:^(id result) {
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (_guiderPictureUrlsArr != nil) {
            [self.guiderPictureUrlsArr removeAllObjects];
        }
        if (baseModel.code == 0) {
            // 如果数据获取成功则开始添加数据
            NSArray *array = [result valueForKey:@"result"];
            for (NSDictionary *resultTemp in array) {
                ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc] initWithDictionary:resultTemp error:nil];
                [self.guiderPictureUrlsArr addObject:ICN_IMG(model.pic)];
            }
            // 数据获取成功开始配置scroller
            if (self.guiderPictureUrlsArr.count > 0) {
                [self.pictureScroller configGuidersArrWithIconsUrlsArr:self.guiderPictureUrlsArr];
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败"];
    }];
}


#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)guiderPictureUrlsArr{
    if (_guiderPictureUrlsArr == nil) {
        _guiderPictureUrlsArr = [NSMutableArray array];
    }
    return _guiderPictureUrlsArr;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 配置页面视图属性
    [self configWithViewProperty];
    // 请求加载引导页图片网址的接口
    [self requestWithGuiderPNGS];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /*判断是不是退出登录跳转过来的*/
    if ([self.isBarHidden isEqualToString:@"1"] ) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    // 在视图将要出现的时候将scroller插在最下面
    [self.view insertSubview:self.pictureScroller belowSubview: self.visitorLoginBtn];
    // 设置渐变色
    [self configGradientLayer];
    // 根据屏幕尺寸修改约束
    if (SCREEN_WIDTH > 370) {
        if (SCREEN_WIDTH > 400) {
            // 6p的尺寸
            self.labelTopContraints.constant = 30;
            self.labelBottomContraints.constant = 30;
            self.loginBtnTopContraints.constant = 30;
        }else{
            // 6的尺寸
            self.labelTopContraints.constant = 20;
            self.labelBottomContraints.constant = 20;
            self.loginBtnTopContraints.constant = 20;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 设置渐变色
- (void)configGradientLayer{
    UIView *view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    //初始化渐变层
    self.gradientLayer = [CAGradientLayer layer];
    self.gradientLayer.frame = [UIScreen mainScreen].bounds;
    
    [view.layer addSublayer:self.gradientLayer];
    
    //设置渐变颜色方向
    self.gradientLayer.startPoint = CGPointMake(0, 0);
    self.gradientLayer.endPoint = CGPointMake(0, 1);
    
    //设定颜色组
    //    endColor="#397cd5"
    //android:centerColor="#1da6e9"
    //android:startColor="#02cefc"
    self.gradientLayer.colors = @[(__bridge id)RGB0X(0x02cefc).CGColor,
                                  (__bridge id)RGB0X(0x1da6e9).CGColor,
                                  (__bridge id)RGB0X(0x397cd5).CGColor];
    
    //设定颜色分割点
    self.gradientLayer.locations = @[@(0.01f) ,@(0.5f) ,@(1.0f)];
    [self.view insertSubview:view belowSubview:self.pictureScroller];
}



// 配置页面的视图属性 -- 让属性反向持有VC
- (void)configWithViewProperty{
    // 设置三方的目标controller按钮
    self.WeiBoLoginBtn.targetViewController = self;
    self.WeiChatLoginBtn.targetViewController = self;
    self.QQLoginBtn.targetViewController = self;
    // 设置登录注册的controller按钮
    self.visitorLoginBtn.targetViewController = self;
    self.loginBtn.targetViewController = self;
    self.registerBtn.targetViewController = self;
    // 设置图片轮播的controller按钮
    self.picturePageController.targetViewController = self;
    self.pictureScroller.targetViewController = self;
    
}

#pragma mark - ---------- 共有方法 ----------

// 设置pagecontrol的方法
- (void)setPageControlWithIndex:(NSInteger)index Total:(NSInteger)total{
    self.picturePageController.numberOfPages = total;
    self.picturePageController.currentPage = index;
}




@end
