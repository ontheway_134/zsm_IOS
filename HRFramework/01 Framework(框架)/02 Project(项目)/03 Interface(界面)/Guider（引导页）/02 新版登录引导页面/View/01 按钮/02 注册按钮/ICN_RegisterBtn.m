//
//  ICN_RegisterBtn.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_RegisterBtn.h"
#import "ICN_UserRegisterViewController.h" // 注册页面头视图

@implementation ICN_RegisterBtn

- (void)awakeFromNib{
    [super awakeFromNib];
    // 进行后续操作
    __weak typeof(self) weakSelf = self;
    [self addAction:^(NSInteger tag) {
        // 添加注册操作
        ICN_UserRegisterViewController *userRegister=[[ICN_UserRegisterViewController alloc]init];
        UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:userRegister];
        [weakSelf.targetViewController presentViewController:na animated:YES completion:nil];
    }];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = 5.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.masksToBounds = YES;
}


@end
