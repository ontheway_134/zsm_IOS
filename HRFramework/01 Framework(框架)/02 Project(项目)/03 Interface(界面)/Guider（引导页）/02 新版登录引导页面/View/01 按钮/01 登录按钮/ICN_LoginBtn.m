//
//  ICN_LoginBtn.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LoginBtn.h"

@implementation ICN_LoginBtn

- (void)awakeFromNib{
    [super awakeFromNib];
    // 进行后续操作
    __weak typeof(self) weakSelf = self;
    [self addAction:^(NSInteger tag) {
        // 添加登录操作
        ICN_SignViewController *sign=[[ICN_SignViewController alloc]init];
        UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:sign];
        [weakSelf.targetViewController presentViewController:na animated:YES completion:nil];

    }];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.cornerRadius = 5.0;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.0;
    self.layer.masksToBounds = YES;
}

@end
