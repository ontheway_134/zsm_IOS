//
//  ICN_ TrilateralLoginBtn.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ TrilateralLoginBtn.h"
#import "ICN_huanxinModel.h"
#import "ICN_ThridModel.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_MyAccountViewController.h" // 验证手机号页面
#import <UMSocialCore/UMSocialCore.h>
#import <CloudPushSDK/CloudPushSDK.h>


// 添加私有枚举

typedef enum : NSUInteger {
    Trilateral_QQLogin = 1001,
    Trilateral_WeiChatLogin,
    Trilateral_WeiBoLogin,
} private_TrilateralLoginType; // 设置QQ ， 微博 ， 微信登录的tag枚举

@implementation ICN__TrilateralLoginBtn

- (void)awakeFromNib{
    [super awakeFromNib];
    // 进行后续操作
    __weak typeof(self) weakSelf = self;
    [self addAction:^(NSInteger tag) {
        // 添加三方登录操作
        switch (weakSelf.tag) {
            case Trilateral_QQLogin:{
                [weakSelf getAuthWithUserInfoFromQQ];
                break ;
            }
            case Trilateral_WeiChatLogin:{
                [weakSelf getAuthWithUserInfoFromWechat];
                break ;
            }
            case Trilateral_WeiBoLogin:{
                [weakSelf getAuthWithUserInfoFromSina];
                break ;
            }
            default:
                break;
        }
    }];
}

- (BOOL)requestBaseUserMessageJudgeWithThirdUserToken:(NSString *)token{
    // 如果获取的token不存在直接返回错误
    if (token == nil || [token isEqualToString:@""]) {
        return NO;
    }
    // 将token作为从参数传入校验五项基本信息是否完整
    NSDictionary *userDic = @{@"token" : token};
    __weak typeof(self) weakSelf = self;
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_GetMineUserMSG params:userDic success:^(id result) {
        
        NSDictionary *dic1 =result[@"result"];
        
        ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        NSLog(@"%@",model);
        if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
            || model.hopePositionName == nil|| model.workStatus == nil) {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        } else {
            // 校验成功后将用户token传入userdefault中
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        }
        // 进入主页之前的操作
        // 如果当时存在token的话使用当时的token注册阿里云推送的别名
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
            [CloudPushSDK addAlias:[USERDEFAULT valueForKey:HR_CurrentUserToken] withCallback:^(CloudPushCallbackResult *res) {
                if (errno) {
                    HRLog(@"%@",errno);
                }
            }];
        }
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:HR_CurrentUserToken];
        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
        [weakSelf.targetViewController presentViewController:barController animated:YES completion:nil];
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"获取信息校验失败");
    }];
    
    return YES;
    
}


// 使用信息dic进行三方登录
- (void)loginWithThirdPartyMessageDic:(NSDictionary *)dic{
    // 授权信息
    [MBProgressHUD ShowProgressToSuperView:self.targetViewController.view Message:@"登录中"];
    __weak typeof(self) weakSelf = self;
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_THIRDSREGIST params:dic success:^(id result) {
        
        NSString *codeStr = [result valueForKey:@"code"];
        
        if (codeStr != nil && codeStr.integerValue == 0) {
            
            
            Modelss *model = [[Modelss alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
            //从后台获取用户名密码的接口
            NSDictionary *dictoken=@{@"token":model.token};
            // 进行身份信息校验
            [self requestPersonalUserMessageCheckWithDic:dic];
            // 请求环信id数据
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
                
                ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
                
                if (basemodel.code == 0) {
                    //如果获取到接口，进行环信的登录
                    [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                        
                        // 登录成功
                        [MBProgressHUD hiddenHUDWithSuperView:self.targetViewController.view];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"huanxin" forKey:@"phonehuanxin"];
                        
                        //  环信登录成功
                        [[NSUserDefaults standardUserDefaults] setValue:model.token forKey:HR_CurrentUserToken];
                        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                        if (model.authMobile == nil || [model.authMobile isEqual:@""]) {
                            // 跳转到验证手机号页面
                            ICN_MyAccountViewController *myAcount=[[ICN_MyAccountViewController alloc]init];
                            myAcount.token = model.token;
                            UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:myAcount];
                            [weakSelf.targetViewController presentViewController:na animated:YES completion:nil];
                            
                        }else{
                            
                            // 根据token校验如何跳转
                            if ([weakSelf requestBaseUserMessageJudgeWithThirdUserToken:model.token] == NO) {
                                [MBProgressHUD ShowProgressWithBaseView:weakSelf.targetViewController.view Message:@"信息校验异常"];
                            }
                            
                        }
                        
                    } failure:^(id data) {
                        [MBProgressHUD hiddenHUDWithSuperView:weakSelf.targetViewController.view];
                        HRLog(@"error -- 环信登录失败");
                    }];
                    
                    
                }
                
            } failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD hiddenHUDWithSuperView:weakSelf.targetViewController.view];
                HRLog(@"error -- 获取环信登录信息网络请求失败");
            }];
            
            
        }else{
            NSString *info = [result valueForKey:@"info"];
            if (info) {
                [MBProgressHUD hiddenHUDWithSuperView:weakSelf.targetViewController.view];
                [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:info];
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD hiddenHUDWithSuperView:weakSelf.targetViewController.view];
        HRLog(@"error -- 获取三方登录基本信息网络请求失败");
    }];
}

// 校验用户基本信息的网络请求
- (void)requestPersonalUserMessageCheckWithDic:(NSDictionary *)dic{
    // 同步进行获取信息是否完善的校验
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
        NSDictionary *dic1 =result[@"result"];
        // 获取基本信息Model
        ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
            || model.hopePositionName == nil|| model.workStatus == nil) {
            // 设置用户登录类型 - loginStatus（1 ： 基本信息未填完 ； 0：基本信息填完了）
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        } else {
            //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"error --- 个人用户基本信息网络请求失败");
        // 请求失败默认设置游客登录状态
        [USERDEFAULT setValue:SF(@"%ld",(long)HR_ICNVisitor) forKey:HR_UserTypeKEY];
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
    }];
}


//三方登录 -- 微信
- (void)getAuthWithUserInfoFromWechat
{
    // 设置登录方式为三方登录
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]){
        
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                NSLog(@"%@",error);
                
            } else {
                
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@2};
                [self loginWithThirdPartyMessageDic:dic];
                
            }
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.targetViewController.view Message:@"未安装微信，请选其他方式登录"];
    }
    
}
//三方登录 -- QQ
- (void)getAuthWithUserInfoFromQQ
{
    // 设置登录方式为三方登录
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]){
        
        
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_QQ currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                NSLog(@"%@",error);
                
            } else {
                
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@1};
                [self loginWithThirdPartyMessageDic:dic];
                
            }
        }];
    }
    else{
        [MBProgressHUD ShowProgressWithBaseView:self.targetViewController.view Message:@"未安装QQ，请选其他方式登录"];
    }
}
//三方登录 -- 新浪
- (void)getAuthWithUserInfoFromSina
{
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]){
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_Sina currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                
            } else {
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@3};
                [self loginWithThirdPartyMessageDic:dic];
            }
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.targetViewController.view Message:@"未安装微博，请选其他方式登录"];
    }
}



@end
