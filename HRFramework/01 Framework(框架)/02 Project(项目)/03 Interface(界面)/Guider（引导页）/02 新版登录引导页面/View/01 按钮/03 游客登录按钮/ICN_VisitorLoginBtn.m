//
//  ICN_VisitorLoginBtn.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_VisitorLoginBtn.h"
#import "BaseTabBarController.h"

@implementation ICN_VisitorLoginBtn

- (void)awakeFromNib{
    [super awakeFromNib];
    // 进行后续操作
    __weak typeof(self) weakSelf = self;
    [self addAction:^(NSInteger tag) {
        // 添加游客登录操作
        // 将第一次登陆的标识存储
        [USERDEFAULT setBool:YES forKey:@"isApplication"];
        // 将游客登录
        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
            //延时后想要执行的代码
            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
            [weakSelf.targetViewController presentViewController:barController animated:YES completion:nil];
        });
    }];
}


@end
