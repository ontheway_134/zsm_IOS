//
//  ICN_GuiderPageControl.h
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_GuiderPageControl : UIPageControl

@property (nonatomic , weak)UIViewController *targetViewController;

@end
