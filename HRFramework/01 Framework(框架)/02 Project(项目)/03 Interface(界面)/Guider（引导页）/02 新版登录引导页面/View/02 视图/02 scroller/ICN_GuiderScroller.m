//
//  ICN_GuiderScroller.m
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_GuiderScroller.h"
#import "ICN_GuiderPager.h"

@interface ICN_GuiderScroller ()<UIScrollViewDelegate>

@property (nonatomic , strong)NSMutableArray *guiderPicsMArr; // 引导图片数组
@property (strong, nonatomic) CAGradientLayer *gradientLayer;

@end

@implementation ICN_GuiderScroller

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)guiderPicsMArr{
    if (_guiderPicsMArr == nil) {
        _guiderPicsMArr = [NSMutableArray array];
        // 默认添加第一张图片 -- 设置默认背景色是浅灰色
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        imageView.backgroundColor = [UIColor lightGrayColor];
        [_guiderPicsMArr addObject:imageView];
    }
    return _guiderPicsMArr;
}

#pragma mark - ---------- 私有方法 ----------


- (void)awakeFromNib{
    [super awakeFromNib];
    // 设置默认的显示样式
    [self configWithDefaultUI];
    // 设置背景图
//    self.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"back.jpg"]];
}

#pragma mark - ---------- 共有方法 ----------

- (void)configGuidersArrWithIconsUrlsArr:(NSMutableArray *)urlsArr{
    // 第一步清空历史数据
    [self clearHisGuiderImageViews];
    // 添加引导页网络图片
    for (NSInteger i = 0; i < urlsArr.count; i++) {
        UIImageView *guiderView;
        if (i == 0) {
            guiderView = self.guiderPicsMArr.firstObject;
        }else{
            guiderView = [[UIImageView alloc] initWithFrame:CGRectZero];
            [self.guiderPicsMArr addObject:guiderView];
        }
        [guiderView sd_setImageWithURL:[NSURL URLWithString:SF(@"%@",urlsArr[i])] placeholderImage:[UIImage imageNamed:@"占位图"]];
    }
    // 设置scroll尺寸
    NSInteger total = self.guiderPicsMArr.count;
    self.contentSize = CGSizeMake(self.width * total, self.height);
    // 重新刷新布局
    [self setNeedsLayout];
}


#pragma mark - ---------- UI配置 ----------

- (void)configWithDefaultUI{
    // 设置基本属性
    self.userInteractionEnabled = YES;
    self.pagingEnabled = YES;
    self.delegate = self;
}

// 设置渐变色
- (void)drawRect:(CGRect)rect{
    //初始化渐变层
    self.gradientLayer = [CAGradientLayer layer];
    CGRect frame = self.bounds;
    if (self.contentSize.width > 0) {
        frame.size = self.contentSize;
    }
    self.gradientLayer.frame = frame;
    
    [self.layer addSublayer:self.gradientLayer];
    
    //设置渐变颜色方向
    self.gradientLayer.startPoint = CGPointMake(0, 0);
    self.gradientLayer.endPoint = CGPointMake(0, 1);
    
    //设定颜色组
    //    endColor="#397cd5"
    //android:centerColor="#1da6e9"
    //android:startColor="#02cefc"
    self.gradientLayer.colors = @[(__bridge id)RGB0X(0x02cefc).CGColor,
                                  (__bridge id)RGB0X(0x1da6e9).CGColor,
                                  (__bridge id)RGB0X(0x397cd5).CGColor];
    
    //设定颜色分割点
    self.gradientLayer.locations = @[@(0.01f) ,@(0.5f) ,@(1.0f)];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 设置刷新 drawRect
//    [self setNeedsDisplay];
    // 根据引导页数组设置对应图片的约束
    for (UIImageView *guiderView in self.guiderPicsMArr) {
        if (![guiderView.superview isEqual:self]) {
            // 数组中还没有设置父类的元素 -- 设置父类是scroll
            [self addSubview:guiderView];
        }
        // 添加约束布局
        if ([guiderView isEqual:self.guiderPicsMArr.firstObject]) {
            // 是数组中的第一个元素
            [guiderView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self);
                make.top.equalTo(self);
                make.width.equalTo(self);
                make.height.mas_equalTo(self);
            }];
        }else{
            // 数组中的其他元素
            NSInteger beforeIndex = [self.guiderPicsMArr indexOfObject:guiderView] - 1;
            UIImageView *beforeImageView = [self.guiderPicsMArr objectAtIndex:beforeIndex];
            [guiderView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(beforeImageView.mas_right);
                make.top.equalTo(beforeImageView);
                make.width.equalTo(self);
                make.height.mas_equalTo(self);
            }];
        }
    }
}

- (void)clearHisGuiderImageViews{
    for (UIImageView *guiderView in self.guiderPicsMArr) {
        if (guiderView.superview != nil) {
            [guiderView removeFromSuperview];
        }
    }
    [self.guiderPicsMArr removeAllObjects];
    // 默认添加第一张图片 -- 设置默认背景色是浅灰色
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    imageView.backgroundColor = [UIColor lightGrayColor];
    [_guiderPicsMArr addObject:imageView];
}

#pragma mark - ---------- 配置代理 ----------

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // scroller已经停止减速的代理
    NSInteger total = self.guiderPicsMArr.count;
    NSInteger index = self.contentOffset.x / [UIScreen mainScreen].bounds.size.width;
    [self.targetViewController setPageControlWithIndex:index Total:total];
}



@end
