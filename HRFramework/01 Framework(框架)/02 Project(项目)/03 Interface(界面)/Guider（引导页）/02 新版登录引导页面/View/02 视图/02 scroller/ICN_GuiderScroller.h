//
//  ICN_GuiderScroller.h
//  ICan
//
//  Created by albert on 2017/4/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ICN_GuiderPager;
@interface ICN_GuiderScroller : UIScrollView

@property (nonatomic , weak)ICN_GuiderPager *targetViewController;


- (void)configGuidersArrWithIconsUrlsArr:(NSMutableArray *)urlsArr;

@end
