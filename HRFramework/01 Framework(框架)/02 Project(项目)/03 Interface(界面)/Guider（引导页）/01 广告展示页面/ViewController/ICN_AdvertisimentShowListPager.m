//
//  ICN_AdvertisimentShowListPager.m
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#pragma mark - ---------- 私有常量 ----------

static NSInteger const PRIVATE_IconTag = 102355;
static NSInteger const PRIVATE_PersonalUserStatus = 12344; // 个人用户登录状态
static NSInteger const PRIVATE_EnterpriseUserStatus = 12345; // 企业用户登录状态


#import "ICN_AdvertisimentShowListPager.h"
#import "ICN_AdvertisimentViewModel.h" // 广告处理的ViewModel
#import "ICN_AdvertimistModel.h"
#import "ICN_MyPersonalHeaderModel.h"
#import "ICN_huanxinModel.h"
#import "UIButton+albWebCache.h" // 为按钮添加加载网络图片的类目
#import "ICN_GroupDetialViewController.h"    //职场规划
#import "ICN_PositionNextOneViewController.h"    //职场规划详情
#import "ICN_ActivityDetialViewController.h"    //活动详情
#import "ICN_GetEnterPriseModel.h"
#import "ICN_SetUserInfoModel.h"  // 用户基本信息Model
#import "ICN_GuiderPager.h" // 新版引导页
#import <CloudPushSDK/CloudPushSDK.h>

#pragma mark --- 跳转页面 ---
#import "ICN_StartViewController.h"

@interface ICN_AdvertisimentShowListPager ()<AdvertisimentDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *ADBackScroll; // 广告占位背景scrollView
@property (weak, nonatomic) IBOutlet UIButton *timerBtn; // 倒计时显示按钮

@property (nonatomic , strong)ICN_AdvertisimentViewModel *viewModel; // 广告处理ViewModel
@property (nonatomic , strong)NSMutableArray<UIButton *> * advertisimentIconArr;

@property(nonatomic,strong)UIPageControl *page;   //小圆点
@property (nonatomic , assign , getter=isJumped)BOOL currentJump; // 是否当时跳转

@end

@implementation ICN_AdvertisimentShowListPager

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<UIButton *> *)advertisimentIconArr{
    if (_advertisimentIconArr == nil) {
        _advertisimentIconArr = [NSMutableArray array];
    }
    return _advertisimentIconArr;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];

    HRLog(@"=========%d",[[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue]);
    self.ADBackScroll.bounces = NO;
    self.viewModel = [[ICN_AdvertisimentViewModel alloc] init];
    self.viewModel.delegate = self;
    // 在开始访问数据之后，没有刷新数据之前需要显示一个HUD
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"刷新数据"];
    // 请求广告数据
    [self.viewModel requestCurrentPagerAdvertisiment];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置默认 currentJump = NO 不是当时跳转（主动点击跳转之后为当时跳转）
    // 每次再次进入页面的时候重置
    self.currentJump = NO;
    [self.timerBtn removeFromSuperview];
    [self.view addSubview:self.timerBtn];
    // 添加约束
    [self.timerBtn mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).offset(-10);
        make.top.equalTo(self.view).offset(20);
        make.size.mas_equalTo(CGSizeMake(60, 20));
    }];
    [self.timerBtn addAction:^(NSInteger tag) {
        // 添加对应的跳转操作
        [self pushToDetialPagerAfterADLoaded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

- (void)configScrollerAdvertisimentWithModelsArr{
    if (!self.viewModel.modelsArr) {
        // 没有数据源的时候不执行后续操作 并进行跳转操作
        [self pushToDetialPagerAfterADLoaded];
        return ;
    }
    // 根据获取到的持续时间计算
    [self startWithHolderTime:self.viewModel.holderSeconds];
    
    // 根据modelarr的count计算scroll的偏移量
    NSInteger count = self.viewModel.modelsArr.count;
    
    
    self.page = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    self.page.center = CGPointMake(SCREEN_WIDTH /2+20, SCREEN_HEIGHT - 154/BASESCREENPX_HEIGHT);
    [self.view addSubview:self.page];
    self.page.numberOfPages = self.viewModel.modelsArr.count;
    
    
    self.ADBackScroll.contentSize = CGSizeMake(self.ADBackScroll.size.width * count, self.ADBackScroll.height);
    self.ADBackScroll.delegate =self;
    for (NSInteger i = 0; i < self.viewModel.modelsArr.count; i++) {
        // 将按钮加入按钮数组
        [self.advertisimentIconArr addObject:[self loadAdvertisimentIconWithTag:(i + PRIVATE_IconTag)]];
        // 对于按钮布局
        if (i == 0) {
            [self.advertisimentIconArr[i] mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(self.ADBackScroll.width, self.ADBackScroll.height));
                make.left.equalTo(self.ADBackScroll);
                make.top.equalTo(self.ADBackScroll);
            }];
        }
        if (i > 0) {
            [self.advertisimentIconArr[i] mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(self.ADBackScroll.width, self.ADBackScroll.height));
                make.left.equalTo(self.self.advertisimentIconArr[i - 1].mas_right);
                make.top.equalTo(self.ADBackScroll);
            }];
        }
    }
    
}

- (UIButton *)loadAdvertisimentIconWithTag:(NSInteger)tag{
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.ADBackScroll.width, self.ADBackScroll.height)];
    [self.ADBackScroll addSubview:button];
    button.tag = tag;
    [button addTarget:self action:@selector(clickOnAdvertisimentIconBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [button alb_setButtonBackGroundImageWithUlr:[self.viewModel.modelsArr[tag - PRIVATE_IconTag] pic]];
    return button;
}

// 计时器方法
- (void)startWithHolderTime:(NSInteger)holder{
    __block int32_t timeOutCount = (int)holder;
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 1ull * NSEC_PER_SEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        timeOutCount -- ;
        dispatch_async(dispatch_get_main_queue(), ^{
            // 如果没有立即跳转 -- 而是一直等到广告结束的话 回到主线程刷新UI
            [self.timerBtn setTitle:SF(@"(%d) 跳过",timeOutCount) forState:UIControlStateNormal];
        });
        if (timeOutCount == 0) {
            HRLog(@"计时器时间到了");
            dispatch_source_cancel(timer);
        }
    });
    
    dispatch_source_set_cancel_handler(timer, ^{
        // 调用了取消计时器方法，在这里调用正常的跳转方法
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.isJumped == NO) {
                [self pushToDetialPagerAfterADLoaded];
            }
        });
    });
    
    dispatch_resume(timer);
}

// 点击广告内容的跳转内链
- (void)clickOnAdvertisimentIconBtnAction:(UIButton *)sender{
    // 1. 获取选中的索引
    NSInteger index = sender.tag - PRIVATE_IconTag;
    // 2. 获取索引的Model
    ICN_AdvertimistModel *model = self.viewModel.modelsArr[index];
    // 3. 根据条件判断跳转
    // 4. 判断用户是否有登录状态 - 如果没有默认设置是游客登录
    BOOL istoken = YES;
    // 判断用户状态并根据状态校验基本信息
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        NSInteger userType = [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue];
        switch (userType) {
            case HR_ICNUser:{
                // 个人用户登录 - 校验基本信息
                [self checkOutUserBaseMessageWithStatus:PRIVATE_PersonalUserStatus];
                break ;
            }
            case HR_ICNEnterprise:{
                // 企业用户登录 - 校验基本信息
                [self checkOutUserBaseMessageWithStatus:PRIVATE_EnterpriseUserStatus];
                break ;
            }
            default:{
                // 默认按照游客登录处理
                // 清空token
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
                // 设置状态为游客登录
                [USERDEFAULT setObject:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
                // 设置当前状态为游客登录状态
                istoken = NO;
                break ;
            }
        }
    }
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == nil) {
        [USERDEFAULT setValue:SF(@"%d",HR_ICNVisitor) forKey:HR_UserTypeKEY];
        istoken = NO;
    }
    if ([model.adType integerValue] == 1) {
        // 跳转到内部链接
        // 1. 区分对应的跳转操作
        switch ([model.adClass integerValue]) {
            case 1:{
                // 跳转到首页列表
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    
                    [self presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 2:{
                //跳转到职场
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 2;
                    [self presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 3:{
                // 跳转到职场详情
                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                barController.selectedIndex = 2;
                ICN_PositionNextOneViewController *position = [[ICN_PositionNextOneViewController alloc]init];
                position.url = model.pageId;
                [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                [self presentViewController:barController animated:YES completion:^{
                    [APPLICATION keyWindow].rootViewController = barController;
                    
                }];
                
                
                break ;
            }
            case 4:{
                // 跳转到活动列表
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                    //延时后想要执行的代码
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 3;
                    [self presentViewController:barController animated:YES completion:nil];
                });
                
                break ;
            }
            case 5:{
                // 跳转到活动详情
                if (istoken) {
                    // 用户登录
                    BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                    barController.selectedIndex = 3;
                    ICN_ActivityDetialViewController *position = [[ICN_ActivityDetialViewController alloc]init];
                    position.packID = model.pageId;
                    [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                    
                    [self presentViewController:barController animated:YES completion:^{
                        [APPLICATION keyWindow].rootViewController = barController;
                    }];
                }else{
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                        //延时后想要执行的代码
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        barController.selectedIndex = 3;
                        [self presentViewController:barController animated:YES completion:nil];
                    });
                }
                
                break ;
            }
            default:
                break;
        }
    }else{
        // 跳转到广告
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.adUrl]];
    }
    
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- 广告代理 - AdvertisimentDelegate ---

- (void)responseWithAdvertisimentRequestSuccess:(BOOL)isSuccess Info:(NSString *)info{
    // 1. 先隐藏掉之前的刷新数据HUD
    [MBProgressHUD hiddenHUDWithSuperView:self.view];
    // 2. 无论什么情况只要失败就自动跳转到正文内容页面
    if (!isSuccess) {
        [MBProgressHUD ShowProgressWithBaseView:[APPLICATION keyWindow] Message:info];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self pushToDetialPagerAfterADLoaded];
        });
    }else{
        // 3. 在广告数据获取成功之后调用加载
        [self configScrollerAdvertisimentWithModelsArr];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.page.currentPage=self.ADBackScroll.contentOffset.x/SCREEN_WIDTH;
}

// 在广告加载完成 / 广告加载失败之后的页面跳转
- (void)pushToDetialPagerAfterADLoaded{
    [self SwitchRootViewController];
    // 设置为当时跳转
    self.currentJump = YES;
}

-(void)SwitchRootViewController{
    /*
     *  loginStatus
     *  0：已填写完整信息，可以登录
     *  1：注册完成，但是没填写完整的信息，则跳到登录页    如果再次登录信息不完整的账号会跳转到信息设置界面
     *  2：
     *
     */
    BaseTabBarController *tabbarVC = [[BaseTabBarController alloc] init];
    // 判断如果没有token设置为游客登录类型
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        [USERDEFAULT setValue:SF(@"%d",HR_ICNVisitor) forKey:HR_UserTypeKEY];
    }
    // 企业登录
    if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
        // 设置状态是企业登录
        tabbarVC.loginUserType = HR_ICNEnterprise;
        // 校验企业基本信息
        [self checkOutUserBaseMessageWithStatus:PRIVATE_EnterpriseUserStatus];
        // 跳转到首页
        [self configWithUserBaseTabbar:tabbarVC];
        //        NSDictionary * dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
        //        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:dic success:^(id result) {
        //            ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
        //            // 根据企业的audit判断是否审核通过
        //            if ([model.result.audit integerValue] == 0) {
        //                // 审核未通过
        //                [USERDEFAULT setValue:@"0" forKey:HR_BusinessAuditKEY];
        //                [self configWithUserBaseTabbar:tabbarVC];
        //            }
        //            if ([model.result.audit integerValue] == 1) {
        //                // 审核通过了
        //                [USERDEFAULT setValue:@"1" forKey:HR_BusinessAuditKEY];
        //                [self configWithUserBaseTabbar:tabbarVC];
        //            }
        //            // 进行基本信息校验 -- 企业
        //            if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
        //                model.result.provinceName == nil || model.result.companyDetail == nil ) {
        //                [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        //            } else {
        //                [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        //            }
        //        } failure:^(NSDictionary *errorInfo) {
        //            NSLog(@"请求数据失败");
        //            // 默认设置审核未通过
        //            [USERDEFAULT setObject:@"0" forKey:HR_BusinessAuditKEY];
        //            [self configWithUserBaseTabbar:tabbarVC];
        //        }];
    }else{
        // 用户登录 - 需要走网络判断是否有信息补全
        // 设置状态是用户登录
        tabbarVC.loginUserType = HR_ICNUser;
        [self configWithUserBaseTabbar:tabbarVC];
    }
}
// 校验tabbarVC的用户检测状态 并跳转
- (void)configWithUserBaseTabbar:(BaseTabBarController *)tabbarVC{
    if ([USERDEFAULT objectForKey:HR_CurrentUserToken] && [USERDEFAULT valueForKey:@"phonehuanxin"]) {
        // 判断使用用户登录才进行如下操作
        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNUser) {
            // 判断用户是否通过身份认证完全
            // 进行基本信息校验 --
            NSDictionary * dic = @{@"token":[USERDEFAULT objectForKey:HR_CurrentUserToken]};
            [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
                NSDictionary *dic1 =result[@"result"];
                // 获取基本信息Model
                ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                if (model.memberNick == nil || model.memberLogo == nil
                    || [model.workStatus integerValue] == 2) {
                    // 设置用户登录类型 - loginStatus（1 ： 基本信息未填完 ； 0：基本信息填完了）
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                } else {
                    //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
                    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                }
                
            } failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                HRLog(@"error --- 个人用户基本信息网络请求失败");
                // 设置为游客登录状态
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
            }];
            [self huanxin];
        }
        [self presentViewController:tabbarVC animated:YES completion:^{
            [APPLICATION keyWindow].rootViewController = tabbarVC;
        }];
    } else {
        ICN_GuiderPager *stare = [[ICN_GuiderPager alloc] init];
        [self presentViewController:stare animated:YES completion:^{
            [APPLICATION keyWindow].rootViewController = stare;
        }];
    }
}

-(void)huanxin{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    //     http://1ican.com/index.php/Member/Member/getIndexInfo 原网址
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
        NSDictionary *dict = result[@"result"]
        ;
        ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:dict];
        
        if (model.memberLogo) {
            [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
            [USERDEFAULT synchronize];
        }
        if (model.memberNick) {
            [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
            [USERDEFAULT synchronize];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    NSString *str=[[NSUserDefaults standardUserDefaults] valueForKey:HR_CurrentUserToken];
    
    NSDictionary *dictoken=@{@"token":str};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
        
        ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
        
        if (basemodel.code == 0) {
            //如果获取到接口，进行环信的登录
            
            
            [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                
                [[NSUserDefaults standardUserDefaults] setValue:str forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                
                
                
            } failure:^(id data) {
                NSLog(@"环信登录失败");
            }];
            
            
        }
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求环信的用户名秘密失败");
    }];
    
}

- (void)checkOutUserBaseMessageWithStatus:(NSInteger)userStatus{
    switch (userStatus) {
        case PRIVATE_PersonalUserStatus:{
            // 个人用户信息校验
            NSDictionary * dic = @{@"token":[USERDEFAULT objectForKey:HR_CurrentUserToken]};
            [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
                NSDictionary *dic1 =result[@"result"];
                // 获取基本信息Model
                ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                    || model.hopePositionName == nil|| model.workStatus == nil) {
                    // 设置用户登录类型 - loginStatus（1 ： 基本信息为填完 ； 0：基本信息填完了）
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                } else {
                    //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
                    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                }
                
            } failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                HRLog(@"error --- 个人用户基本信息网络请求失败");
                // 设置为游客登录状态
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
            }];
            break ;
        }
        case PRIVATE_EnterpriseUserStatus:{
            // 企业用户基本信息校验 -- 校验基本信息和是否通过审核
            NSDictionary * dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
            [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:dic success:^(id result) {
                ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
                // 根据企业的audit判断是否审核通过
                if ([model.result.audit integerValue] == 0) {
                    // 审核未通过
                    [USERDEFAULT setValue:@"0" forKey:HR_BusinessAuditKEY];
                }
                if ([model.result.audit integerValue] == 1) {
                    // 审核通过了
                    [USERDEFAULT setValue:@"1" forKey:HR_BusinessAuditKEY];
                }
                // 进行基本信息校验 --
                if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
                    model.result.provinceName == nil || model.result.companyDetail == nil ) {
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                } else {
                    [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                }
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"请求数据失败");
                // 默认设置审核未通过
                [USERDEFAULT setObject:@"0" forKey:HR_BusinessAuditKEY];
            }];
            break;
        }
        default:
            break;
    }
}



@end
