//
//  ICN_AdvertisimentViewModel.m
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_AdvertisimentViewModel.h"
#import "ICN_AdvertimistModel.h" // 广告Model

@implementation ICN_AdvertisimentViewModel


#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<ICN_AdvertimistModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}


#pragma mark - ---------- 公开方法 ----------

- (void)requestCurrentPagerAdvertisiment{
    
    // 1. 判断是否持有代理否则不进行后期操作
    if (!self.delegate) {
        HRLog(@"warn===   当前广告页面delegate缺失");
        return ;
    }else{
        if (![self.delegate respondsToSelector:@selector(responseWithAdvertisimentRequestSuccess:Info:)]) {
            // 代理没有响应必要方法
            HRLog(@"warn===   当前广告页面delegate没有响应必要方法");
            return ;
        }
    }
    
    [[HRRequest alloc] POST_PATH:PATH_AD params:nil success:^(id result) {
        
        // 1.对于获取到的内容处理 == 获取是否成功数据以及持续时间
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        
        if (baseModel.code == 0) {
            // 数据获取成功
            NSDictionary *resultDic = [result valueForKey:@"result"];
            // 根据是否存在数据数组判断是否进行下一步操作
            NSArray *dicArr = [resultDic valueForKey:@"list"];
            if (dicArr == nil || dicArr.count == 0) {
                // 没有必要的广告数据 返回错误
                [self.delegate responseWithAdvertisimentRequestSuccess:NO Info:@"没有广告数据"];
            }else{
                // 存在广告数据
                // 1. 获取整个广告的持续时间
                self.holderSeconds = [[resultDic valueForKey:@"adTime"] integerValue];
                // 2. 获取实际的广告Model数组
                for (NSDictionary *temp in dicArr) {
                    ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc] initWithDictionary:temp error:nil];
                    [self.modelsArr addObject:model];
                }
                // 3. 返回数据获取成功
                [self.delegate responseWithAdvertisimentRequestSuccess:YES Info:@"广告数据获取成功"];
            }
        }else{
            // 数据获取失败
            [self.delegate responseWithAdvertisimentRequestSuccess:NO Info:baseModel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        [self.delegate responseWithAdvertisimentRequestSuccess:NO Info:@"网络请求失败"];
    }];
    
}

@end
