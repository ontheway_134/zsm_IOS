//
//  ICN_AdvertisimentViewModel.h
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_AdvertimistModel;

@protocol AdvertisimentDelegate <NSObject>

- (void)responseWithAdvertisimentRequestSuccess:(BOOL)isSuccess
                                           Info:(NSString *)info;

@end

@interface ICN_AdvertisimentViewModel : NSObject

@property (nonatomic , strong)NSMutableArray <ICN_AdvertimistModel *> *modelsArr; // Model数组
@property (nonatomic , assign)NSInteger holderSeconds; // 广告页面持续的秒数


@property (nonatomic , weak)id<AdvertisimentDelegate> delegate; // 当前页面的代理


/** 请求当前页面的广告数据Model */
- (void)requestCurrentPagerAdvertisiment;

@end
