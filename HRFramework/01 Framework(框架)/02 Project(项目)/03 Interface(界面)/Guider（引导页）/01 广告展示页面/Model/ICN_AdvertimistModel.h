//
//  ICN_AdvertimistModel.h
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//  返回的广告持续时间在上层数组  key = adTime

#import "BaseOptionalModel.h"

@interface ICN_AdvertimistModel : BaseOptionalModel

@property (nonatomic , copy)NSString *ADId; // 广告ID,
@property (nonatomic , copy)NSString *beginTime; // 开始时间
@property (nonatomic , copy)NSString *endTime; // 结束时间
@property (nonatomic , copy)NSString *pic; // 广告图片
@property (nonatomic , copy)NSString *timeStatus; // 广告状态：无时间期限1  有时间期限2
@property (nonatomic , copy)NSString *adType; //链接类型：1内链，2外链
@property (nonatomic , copy)NSString *adUrl; // 外链链接地址,
@property (nonatomic , copy)NSString *adClass; // 内链地址，1首页，2职场列表，3职场详情，4 活动列表，5活动详情
@property (nonatomic , copy)NSString *pageId; // 详情ID



@end
