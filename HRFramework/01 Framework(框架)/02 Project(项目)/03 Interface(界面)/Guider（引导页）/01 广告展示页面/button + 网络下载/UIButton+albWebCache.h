//
//  UIButton+webCache.h
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (albWebCache)

- (void)alb_setButtonBackGroundImageWithUlr:(NSString *)urlStr;

@end
