//
//  UIButton+webCache.m
//  ICan
//
//  Created by albert on 2017/3/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "UIButton+albWebCache.h"

@implementation UIButton (albWebCache)

- (void)alb_setButtonBackGroundImageWithUlr:(NSString *)urlStr{
    NSURL * url = [NSURL URLWithString:urlStr];
    dispatch_queue_t albQueue = dispatch_queue_create("loadImage", NULL); // 创建GCD线程队列
    dispatch_async(albQueue, ^{
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        // 刷新主线程UI
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setBackgroundImage:image forState:UIControlStateNormal];
        });
    });
}



@end
