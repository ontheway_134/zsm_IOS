//
//  ICN_ThridModel.h
//  ICan
//
//  Created by shilei on 17/1/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@protocol Modelss <NSObject>
@end

@interface Modelss : BaseOptionalModel

@property(nonatomic,copy)NSString *authMobile;
@property(nonatomic,copy)NSString *roleId;
@property(nonatomic,copy)NSString *token;

@end



@interface ICN_ThridModel : BaseOptionalModel

@property(nonatomic,strong)Modelss *result;


@end
