//
//  ICN_StartViewController.m
//  ICan
//
//  Created by shilei on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_StartViewController.h"
#import "ICN_DynamicStateVC.h"
#import "ICN_UserRegisterViewController.h"
#import "ICN_SignViewController.h"
#import <UMSocialCore/UMSocialCore.h>
#import "HRNetworkingManager.h"
#import "BaseOptionalModel.h"
#import "ICN_MyAccountViewController.h"
#import "ICN_ThridModel.h"
#import "BaseOptionalModel.h"
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
#import "ChatDemoHelper.h"
#import "ICN_huanxinModel.h"
#import "ICN_AdvertimistModel.h"



@interface ICN_StartViewController ()<UIScrollViewDelegate>

@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)NSArray *dataArr;   //数据
@property(nonatomic,strong)UIPageControl *page;

@property (nonatomic , strong)NSMutableArray <UIButton *>*thirdPartBtnMutableArr; // 三方按钮数组
@property (nonatomic , strong)NSMutableArray *guiderPictureUrlsArr; // 引导页图片网址数组
@property (nonatomic , assign)BOOL hiddenBtn; // 是否隐藏按钮的属性

@end

@implementation ICN_StartViewController

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<UIButton *> *)thirdPartBtnMutableArr{
    if (_thirdPartBtnMutableArr == nil) {
        _thirdPartBtnMutableArr = [NSMutableArray array];
    }
    return _thirdPartBtnMutableArr;
}

- (NSMutableArray *)guiderPictureUrlsArr{
    if (_guiderPictureUrlsArr == nil) {
        _guiderPictureUrlsArr = [NSMutableArray array];
    }
    return _guiderPictureUrlsArr;
}


#pragma mark - --- 声明周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenBtn = NO;
    // 默认设置用户的登录状态是正常登陆
    [self hiddenUserThirdBtnWithNetRequestWhileViewDidLoad];
    [self createGuide];
    [self.navigationController setNavigationBarHidden:YES];
    // 请求加载引导页图片网址的接口
    [self requestWithGuiderPNGS];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    self.automaticallyAdjustsScrollViewInsets=NO;
    
}

#pragma mark - ---------- NetWorkRequest ----------

// 获取引导页图片网址接口
- (void)requestWithGuiderPNGS{
    [[HRRequest alloc] POST_PATH:PATH_appPay params:nil success:^(id result) {
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (_guiderPictureUrlsArr != nil) {
            [self.guiderPictureUrlsArr removeAllObjects];
        }
        if (baseModel.code == 0) {
            // 如果数据获取成功则开始添加数据
            NSArray *array = [result valueForKey:@"result"];
            for (NSDictionary *resultTemp in array) {
                ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc] initWithDictionary:resultTemp error:nil];
                [self.guiderPictureUrlsArr addObject:ICN_IMG(model.pic)];
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

#pragma mark - --- Private Method ---

- (void)createGuide {
    
    [self.scrollView setContentSize:CGSizeMake(self.dataArr.count * SCREEN_WIDTH, SCREEN_HEIGHT)];
    for (int i = 0; i < self.dataArr.count; i++) {
        UIImageView *imageVieww = [[UIImageView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH * i, 0, SCREEN_WIDTH,SCREEN_HEIGHT)];
        [imageVieww setUserInteractionEnabled:YES];
        [self.scrollView addSubview:imageVieww];
        
        [imageVieww setImage:[UIImage imageNamed:self.dataArr[i]]];
        
    }
    
    for (int i = 0; i < 2; i++) {
        UIButton *signBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.view addSubview:signBtn];
        
        [signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(@((SCREEN_WIDTH*121/320*i)+SCREEN_WIDTH*25/320*(i+1)));
            make.bottom.mas_equalTo(SCREEN_HEIGHT * -115/568);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH*120/320, SCREEN_HEIGHT * 39/568));
        }];
        
        if (i==0) {
            [signBtn setBackgroundImage:[UIImage imageNamed:@"注册按钮"] forState:UIControlStateNormal];
            [signBtn setTitle:@"注册" forState:UIControlStateNormal];
            [signBtn setTitleColor:RGB0X(0x009dff) forState:UIControlStateNormal];
            [[signBtn titleLabel] setFont:FONT(15)];
        }
        if (i==1) {
            [signBtn setBackgroundImage:[UIImage imageNamed:@"下载App按钮"] forState:UIControlStateNormal];
            [signBtn setTitle:@"登录" forState:UIControlStateNormal];
            [signBtn setTitleColor:RGB0X(0xffffff) forState:UIControlStateNormal];
            [[signBtn titleLabel] setFont:FONT(15)];
        }
        signBtn.tag=i;
        [signBtn addTarget:self action:@selector(signBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession] || [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ] || [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]){
        
        [self thirdBtnClick];
    }
    
    //登录注册按钮的创建
    UIButton *touristBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [touristBtn setTitle:@"游客登录" forState:UIControlStateNormal];
    touristBtn.titleLabel.font=[UIFont systemFontOfSize:12];
    [touristBtn addTarget:self action:@selector(startTourist) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:touristBtn];
    
    [touristBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(@(-10));
        make.top.mas_equalTo(@(36));
        make.size.mas_equalTo(CGSizeMake(80*AUTO_320, 30*AUTO_320));
    }];
    
    self.page = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    self.page.center = CGPointMake(SCREEN_WIDTH /2+25, SCREEN_HEIGHT - 184/BASESCREENPX_HEIGHT);
    [self.view addSubview:self.page];
    self.page.numberOfPages = self.dataArr.count;
    
    
}

//三方按钮是否创建的判断
-(void)thirdBtnClick{
    
    UILabel *LoginLable=[[UILabel alloc]init];
    LoginLable.text=@"第三方登录";
    [LoginLable setTextColor:[UIColor whiteColor]];
    LoginLable.font=[UIFont systemFontOfSize:12];
    [self.view addSubview:LoginLable];
    [LoginLable mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(SCREEN_HEIGHT * -87/568);
        make.centerX.mas_equalTo(self.view);
    }];
    for (int i = 0; i < 3; i++) {
        UIButton *ThirdBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        // 将全部的三方按钮添加在三方按钮数组中
        [self.thirdPartBtnMutableArr addObject:ThirdBtn];
        if (self.hiddenBtn) {
            ThirdBtn.hidden = YES;
        }
        ThirdBtn.tag=i;
        [self.view addSubview:ThirdBtn];
        if (i==0) {
            [ThirdBtn setImage:[UIImage imageNamed:@"QQ"] forState:UIControlStateNormal];
        }
        if (i==1) {
            [ThirdBtn setImage:[UIImage imageNamed:@"微信"] forState:UIControlStateNormal];
        }
        if (i==2) {
            [ThirdBtn setImage:[UIImage imageNamed:@"微博"] forState:UIControlStateNormal];
        }
        [ThirdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(SCREEN_WIDTH *54/320*(i+1)+(SCREEN_WIDTH*33/320*i));
            make.bottom.mas_equalTo(SCREEN_HEIGHT * -35/568);
            make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH*40/320, SCREEN_HEIGHT * 40/568));
        }];
        ThirdBtn.layer.cornerRadius=20*AUTO_320;
        ThirdBtn.layer.masksToBounds=YES;
        [ThirdBtn addTarget:self action:@selector(ThirdBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
}

//游客身份登录
- (void)startTourist {
    // 将第一次登陆的标识存储
    [USERDEFAULT setBool:YES forKey:@"isApplication"];
    
    // 将游客登录
    [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
        //延时后想要执行的代码
        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
        [self presentViewController:barController animated:YES completion:nil];
    });
}

#pragma mark - ----- IBActions -----

-(void)signBtnClick:(UIButton *)sender{
    // 在点击登录注册按钮的时候添加
    [USERDEFAULT setValue:@"1" forKey: HR_LoginTypeKEY];
    switch ([sender tag]) {
        case 0:{
            ICN_UserRegisterViewController *userRegister=[[ICN_UserRegisterViewController alloc]init];
            UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:userRegister];
            
            [self presentViewController:na animated:YES completion:nil];
        }
            break;
            
        case 1:{
            ICN_SignViewController *sign=[[ICN_SignViewController alloc]init];
            UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:sign];
            [self presentViewController:na animated:YES completion:nil];
            
        }
            break;
    }
    
}

-(void)ThirdBtnClick:(UIButton *)sender{
    switch ([sender tag]) {
            //此处的所有跳转都需要用模态实现
        case 0:{
            NSLog(@"qq");
            [self getAuthWithUserInfoFromQQ];
        }
            break;
            
        case 1:{
            NSLog(@"微信");
            [self getAuthWithUserInfoFromWechat];
        }
            break;
            
        case 2:{
            NSLog(@"微博");
            [self getAuthWithUserInfoFromSina];
            
        }
            break;
    }
}


#pragma mark - ------- UIScrollViewDelegate -------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.page.currentPage=self.scrollView.contentOffset.x/SCREEN_WIDTH;
}

#pragma mark - -------- 懒加载 -------

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView=[[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _scrollView.delegate=self;
        _scrollView.pagingEnabled=YES;
        _scrollView.showsVerticalScrollIndicator=NO;
        _scrollView.showsHorizontalScrollIndicator=NO;
        [self.view addSubview:_scrollView];
        [_scrollView setBounces:NO];
    }
    return _scrollView;
}

-(NSArray *)dataArr{
    if (!_dataArr) {
        _dataArr=[NSArray arrayWithObjects:@"引导页0",@"引导页1",@"引导页2",@"引导页3", nil];
    }
    return _dataArr;
}

// 校验用户基本信息的网络请求
- (void)requestPersonalUserMessageCheckWithDic:(NSDictionary *)dic{
    // 同步进行获取信息是否完善的校验
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
        NSDictionary *dic1 =result[@"result"];
        // 获取基本信息Model
        ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
            || model.hopePositionName == nil|| model.workStatus == nil) {
            // 设置用户登录类型 - loginStatus（1 ： 基本信息未填完 ； 0：基本信息填完了）
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        } else {
            //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"error --- 个人用户基本信息网络请求失败");
        // 请求失败默认设置游客登录状态
        [USERDEFAULT setValue:SF(@"%d",HR_ICNVisitor) forKey:HR_UserTypeKEY];
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
    }];
}

// 使用信息dic进行三方登录
- (void)loginWithThirdPartyMessageDic:(NSDictionary *)dic{
    // 授权信息
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"登录中"];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_THIRDSREGIST params:dic success:^(id result) {
        
        NSString *codeStr = [result valueForKey:@"code"];
        
        if (codeStr != nil && codeStr.integerValue == 0) {
            
            
            Modelss *model = [[Modelss alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
            //从后台获取用户名密码的接口
            NSDictionary *dictoken=@{@"token":model.token};
            // 进行身份信息校验
            [self requestPersonalUserMessageCheckWithDic:dic];
            // 请求环信id数据
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
                
                ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
                
                if (basemodel.code == 0) {
                    //如果获取到接口，进行环信的登录
                    [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                        
                        // 登录成功
                        [MBProgressHUD hiddenHUDWithSuperView:self.view];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"huanxin" forKey:@"phonehuanxin"];
                        
                        //  环信登录成功
                        [[NSUserDefaults standardUserDefaults] setValue:model.token forKey:HR_CurrentUserToken];
                        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                        if (model.authMobile == nil || [model.authMobile isEqual:@""]) {
                            // 跳转到验证手机号页面 
                            ICN_MyAccountViewController *myAcount=[[ICN_MyAccountViewController alloc]init];
                            myAcount.token = model.token;
                            UINavigationController *na=[[UINavigationController alloc]initWithRootViewController:myAcount];
                            [self presentViewController:na animated:YES completion:nil];
                            
                        }else{
                            
                            // 根据token校验如何跳转
                            if ([self requestBaseUserMessageJudgeWithThirdUserToken:model.token] == NO) {
                                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"信息校验异常"];
                            }
                            
                        }
                        
                    } failure:^(id data) {
                        [MBProgressHUD hiddenHUDWithSuperView:self.view];
                        HRLog(@"error -- 环信登录失败");
                    }];
                    
                    
                }
                
            } failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                HRLog(@"error -- 获取环信登录信息网络请求失败");
            }];
            
            
        }else{
            NSString *info = [result valueForKey:@"info"];
            if (info) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:info];
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        HRLog(@"error -- 获取三方登录基本信息网络请求失败");
    }];
}


//三方登录 -- 微信
- (void)getAuthWithUserInfoFromWechat
{
    // 设置登录方式为三方登录
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_WechatSession]){
        
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                NSLog(@"%@",error);
                
            } else {
                
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@2};
                [self loginWithThirdPartyMessageDic:dic];
                
            }
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未安装微信，请选其他方式登录"];
    }
    
}
//三方登录 -- QQ
- (void)getAuthWithUserInfoFromQQ
{
    // 设置登录方式为三方登录
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_QQ]){
        
        
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_QQ currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                NSLog(@"%@",error);
                
            } else {
                
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@1};
                [self loginWithThirdPartyMessageDic:dic];
                
            }
        }];
    }
    else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未安装QQ，请选其他方式登录"];
    }
}
//三方登录 -- 新浪
- (void)getAuthWithUserInfoFromSina
{
    [USERDEFAULT setValue:@"0" forKey: HR_LoginTypeKEY];
    if(  [[UMSocialManager defaultManager] isInstall:UMSocialPlatformType_Sina]){
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_Sina currentViewController:self completion:^(id result, NSError *error) {
            if (error) {
                
            } else {
                UMSocialUserInfoResponse *resp = result;
                NSDictionary *dic=@{@"openid":resp.uid,@"type":@3};
                [self loginWithThirdPartyMessageDic:dic];
            }
        }];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未安装微博，请选其他方式登录"];
    }
}

#pragma mark - ---------- 网络请求 ----------

- (void)hiddenUserThirdBtnWithNetRequestWhileViewDidLoad{
    //    http://1ican.com/index.php/Login/Login/isHidden
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HiddenThird params:nil success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc]  initWithDictionary:result error:nil];
        if (model.code == 0) {
            // 数据获取成功
            NSString *hiddenStr = [[result valueForKey:@"result"] valueForKey:@"isHidden"];
            if (hiddenStr != nil && hiddenStr.integerValue == 0) {
                // 获取到隐藏指令
                self.hiddenBtn = YES;
                if (self.thirdPartBtnMutableArr.count > 0) {
                    for (UIButton *thirdBtn in self.thirdPartBtnMutableArr) {
                        thirdBtn.hidden = YES;
                    }
                }
            }else{
                self.hiddenBtn = NO;
            }
        }
    } failure:nil];
}

- (BOOL)requestBaseUserMessageJudgeWithThirdUserToken:(NSString *)token{
    // 如果获取的token不存在直接返回错误
    if (token == nil || [token isEqualToString:@""]) {
        return NO;
    }
    // 将token作为从参数传入校验五项基本信息是否完整
    NSDictionary *userDic = @{@"token" : token};
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:userDic success:^(id result) {
        
        NSDictionary *dic1 =result[@"result"];
        
        ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
        NSLog(@"%@",model);
        if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
            || model.hopePositionName == nil|| model.workStatus == nil) {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        } else {
            // 校验成功后将用户token传入userdefault中
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        }
        // 进入主页之前的操作
        [[NSUserDefaults standardUserDefaults] setValue:token forKey:HR_CurrentUserToken];
        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
        [self presentViewController:barController animated:YES completion:nil];
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"获取信息校验失败");
    }];
    
    return YES;
    
}

@end
