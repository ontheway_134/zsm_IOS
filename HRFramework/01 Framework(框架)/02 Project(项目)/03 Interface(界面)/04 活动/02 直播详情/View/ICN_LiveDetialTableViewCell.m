//
//  ICN_LiveDetialTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveDetialTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_LiveDetialTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *timeImg;

@end
@implementation ICN_LiveDetialTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.headImg.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:self.headImg.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.headImg.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.headImg.layer.mask = maskLayer;
    
}
- (void)setModel:(activityesListModel *)model
{
    _model = model;
    self.titleLabel.text = model.title;
    self.nameLabel.text = model.memberNick;
    self.timeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.companyNameLabel.text = model.companyName;
    self.memberPositionLabel.text = model.memberPosition;
    if ([model.beginDate integerValue] == 0) {
        self.timeLabel.text = @"";
        self.timeImg.hidden = YES;
    }

    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
