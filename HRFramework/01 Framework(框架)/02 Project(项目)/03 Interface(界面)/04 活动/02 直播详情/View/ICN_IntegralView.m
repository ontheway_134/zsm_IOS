//
//  ICN_IntegralView.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_IntegralView.h"

@implementation ICN_IntegralView

- (IBAction)backTap:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}
/**
 支付选择
 
 @param sender 微信支付按钮 - 支付宝支付按钮
 */
- (IBAction)choosePayTypeAction:(UIButton *)sender {
    for (int i = 200; i<202; i++) {
        UIButton *btn = [self viewWithTag:i];
        btn.selected = NO;
    }
        sender.selected = YES;
    if (self.choosePayBlock) {
        self.choosePayBlock(sender.tag);
    }
    
}
- (IBAction)choosePayAction:(UIButton *)sender {
        
    if (sender.tag == 200) {
        self.alipayBtn.selected = NO;
        self.weixinPayBtn.selected = YES;
    }else{
        self.alipayBtn.selected = YES;
        self.weixinPayBtn.selected = NO;
    }
    if (self.choosePayBlock) {
        self.choosePayBlock(sender.tag);
    }

    
}
- (IBAction)payAction:(UIButton *)sender {
    if (self.makeSureBlock) {
        self.makeSureBlock();
    }
}
//移除view
- (IBAction)removeAction:(UIButton *)sender {
     [self removeFromSuperview];
}
- (void)ReturnChoosePay:(ReturnChoosePay)block
{
    self.choosePayBlock = block;
}
- (void)ReturnMakeSure:(ReturnMakeSure)block
{
    self.makeSureBlock = block;
}
@end
