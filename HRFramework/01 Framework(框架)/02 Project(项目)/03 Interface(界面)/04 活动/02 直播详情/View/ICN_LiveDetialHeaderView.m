//
//  ICN_LiveDetialHeaderView.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveDetialHeaderView.h"
#import "NSString+Date.h"
@interface ICN_LiveDetialHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *memberNickLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *beginDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picImg;
@property (weak, nonatomic) IBOutlet UIImageView *timeImg;
@end
@implementation ICN_LiveDetialHeaderView

- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.headImg.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:self.headImg.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.headImg.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.headImg.layer.mask = maskLayer;
    
}
#pragma mark ----------------属性重写---------------------
- (void)setModel:(ICN_liveModel *)model
{
    _model = model;
    
    self.titleLabel.text = model.title;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    [self.picImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.pic)]];
    self.numLabel.text = SF(@"%@/%@人",model.subNum,model.canNum);
    if ([model.price doubleValue] == 0) {
        self.priceLabel.text = @"免费";
    }else{
        self.priceLabel.text = SF(@"￥%@",model.price);
    }
    
    self.contentLabel.text = model.content;
    //如果没有结束时间
    if ([model.endDate integerValue] == 0) {
         self.beginDateLabel.text = SF(@"%@-",[NSString conversionTimeStamp:model.beginDate]) ;
    }else{
    self.beginDateLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    }
    self.memberNickLabel.text = model.memberNick;
    self.companyNameLabel.text = model.companyName;
    self.memberPositionLabel.text = model.memberPosition;
    
    //根据文字算高
    float height = [self heightForString:model.content fontSize:12 andWidth:SCREEN_WIDTH-20];
    if ([self.delegate respondsToSelector:@selector(reloadIndexPathWithHeight:)]) {
        [self.delegate reloadIndexPathWithHeight:height];
    }
    
    if ([model.beginDate integerValue] == 0) {
        self.beginDateLabel.text = @"";
        self.timeImg.hidden = YES;
    }

}


- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width{
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 0)];
    detailLabel.font = [UIFont systemFontOfSize:fontSize];
    detailLabel.text = value;
    detailLabel.numberOfLines = 0;
    CGSize deSize = [detailLabel sizeThatFits:CGSizeMake(width,1)];
    return deSize.height;
}

@end
