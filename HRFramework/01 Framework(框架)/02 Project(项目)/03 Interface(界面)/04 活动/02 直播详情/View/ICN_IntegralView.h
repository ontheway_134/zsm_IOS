//
//  ICN_IntegralView.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ReturnChoosePay)(NSInteger type);
typedef void (^ReturnMakeSure)();
@interface ICN_IntegralView : UIView
@property (nonatomic,strong) NSString * price;
@property (copy,nonatomic) ReturnChoosePay choosePayBlock;
@property (copy,nonatomic) ReturnMakeSure makeSureBlock;
- (void)ReturnChoosePay:(ReturnChoosePay)block;
- (void)ReturnMakeSure:(ReturnMakeSure)block;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *alipayBtn;
@property (weak, nonatomic) IBOutlet UIButton *weixinPayBtn;
@end
