//
//  ICN_LiveDetialHeaderView.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_liveModel.h"
@protocol reloadDelegate<NSObject>
-(void)reloadIndexPathWithHeight:(float)height;
@end

@interface ICN_LiveDetialHeaderView : UIView

@property (nonatomic,strong) ICN_liveModel * model;

@property (nonatomic,weak) id<reloadDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end
