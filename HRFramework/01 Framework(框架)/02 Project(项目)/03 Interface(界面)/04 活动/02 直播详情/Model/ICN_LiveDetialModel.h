//
//  ICN_LiveDetialModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_LiveDetialModel : BaseOptionalModel

/**
 计算头视图的高度

 @return 头视图高度
 */
-(CGFloat)heightForHeader;
@end
