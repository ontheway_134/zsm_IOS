//
//  ICN_liveModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@protocol correlationListModel <NSObject>
@end
@interface correlationListModel :BaseOptionalModel
@property (nonatomic,strong) NSString * liveID;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * createDate;
@property (nonatomic,strong) NSString * beginDate;
@property (nonatomic,strong) NSString * endDate;
@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString * memberPosition;
@property (nonatomic,strong) NSString * liveUrl;
@property (nonatomic,strong) NSString * activityId;
@property (nonatomic,strong) NSString * streamName;
@property (nonatomic,strong) NSString * memberStatus;
@end


@protocol activityesListModel <NSObject>
@end
@interface activityesListModel :BaseOptionalModel
@property (nonatomic,strong) NSString * liveID;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * createDate;
@property (nonatomic,strong) NSString * beginDate;
@property (nonatomic,strong) NSString * endDate;
@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString* memberPosition;
@end


@interface ICN_liveModel : BaseOptionalModel
@property (nonatomic,strong) NSString * liveID;
@property (nonatomic,strong) NSString * subNum;
@property (nonatomic,strong) NSString* memberPosition;
@property (nonatomic,strong) NSString * isAdopt;
@property (nonatomic,strong) NSString * canNum;
@property (nonatomic,strong) NSString * endDate;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * memberId;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * beginDate;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * price;
@property (nonatomic,strong) NSString * memberStatus; //用户购买状态
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString * pic;
@property (nonatomic,strong) NSString * memberScore;
@property (nonatomic,strong) NSString * isCollected; //是否收藏
@property (nonatomic,strong) NSString * deductionScore; //可用积分
@property (nonatomic,strong) NSString * isAuth;  //是不是自己的直播
@property (nonatomic,strong) NSString * activityId;
@property (nonatomic,strong) NSArray<activityesListModel> * activityesList;
@property (nonatomic,strong) NSArray<correlationListModel> * correlationList;

@end
