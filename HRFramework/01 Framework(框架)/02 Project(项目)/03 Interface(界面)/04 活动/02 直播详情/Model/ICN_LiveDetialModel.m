//
//  ICN_LiveDetialModel.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveDetialModel.h"

@implementation ICN_LiveDetialModel

- (CGFloat)heightForHeader
{
    return [self heightForString:@"jajajjaajjajajfasjfasjfasjajsjasjafajfkafkafksajfkasfnkasnfksanfkasnfkasfnksafnaksfnakslnfklasnfklsanfkslanfklsanfklsanfksalnfsaklfnskalfnasklfnsaklfnasklfnasklfnalksfnklasnfakls" fontSize:15 andWidth:SCREEN_WIDTH - 45] + 130;
}
#pragma mark ----------------PRIVITE---------------------
- (float) heightForString:(NSString *)value fontSize:(float)fontSize andWidth:(float)width{
    UILabel *detailLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 0)];
    detailLabel.font = [UIFont systemFontOfSize:fontSize];
    detailLabel.text = value;
    detailLabel.numberOfLines = 0;
    CGSize deSize = [detailLabel sizeThatFits:CGSizeMake(width,1)];
    return deSize.height;
}

@end
