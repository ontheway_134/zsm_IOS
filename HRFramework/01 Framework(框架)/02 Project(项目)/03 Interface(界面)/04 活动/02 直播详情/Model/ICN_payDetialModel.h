//
//  ICN_payDetialModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_payDetialModel : BaseOptionalModel
@property (nonatomic,strong) NSString * orderId;
@property (nonatomic,strong) NSString * receiptAmount;
@property (nonatomic,strong) NSString * score;
@end
