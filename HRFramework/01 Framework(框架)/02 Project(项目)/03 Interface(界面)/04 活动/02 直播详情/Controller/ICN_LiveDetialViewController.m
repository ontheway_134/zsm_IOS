//
//  ICN_LiveDetialViewController.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveDetialViewController.h"
#import "ICN_LiveDetialTableViewCell.h"
#import "ICN_LiveDetialHeaderView.h"

#pragma mark --- Model相关头文件 ---
#import "ICN_DynStateADModel.h" // 广告专用Model
#import "ICN_DynStateContentModel.h" // 动态正文Model
#import "ICN_LocationModel.h" // 城市选择列表Model
#import "ICN_ApplyModel.h" // 友盟分享用Model
#import "ICN_liveModel.h"
#import "ICN_YouMengShareModel.h"    // 转发专用类型转化Model
#import "ICN_payDetialModel.h"

#import "ICN_DynamicStateHeader.h" // 动态的头文件
#import "ICN_DynamicStateVC+HXLogin.h" // 环信登录类目
#import "HRNetworkingManager+DynFirstPager.h" // 网络请求类目
#import "ICN_DynamicStateFirstPagerViewModel.h" // 首页ViewModel
#import "ICN_NearestPeopleViewModel.h" // 周边雷达管理器
#import "ICN_ShareManager.h" // 分享管理器
#import "ICN_ComplainListViewModel.h" // 吐槽的ViewModel
#import "ICN_YouMengShareTool.h"                // 分享工具类

//活动详情头视图
#import "ICN_ActivityDetialHeaderView.h"
#import "ICN_ApplySucessView.h"
//活动详情cell
#import "ICN_ActivityDetialTableViewCell.h"
#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_IntegralView.h"
#import "ICN_DynWarnView.h"
#import "JXTAlertTools.h"
#pragma mark - ---------- 跳转页面/视图 头文件 ----------
#import "ICN_TransmitToDynamicPager.h"
#import "ICN_StartViewController.h" // 引导页
#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_DynSearchPagerVC.h" // 搜索页面
#import "ICN_UserHomePagerVC.h" // 用户主页界面
#import "ICN_TopicPublicationVC.h" // 跳转到发布话题页面
#import "ICN_DynamicStatePublicationVC.h" // 跳转到发布动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 跳转到动态评论页面
#import "ICN_ReviewPublication.h" // 转发到I行动态页面
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_CompanyDetialVC.h" // 公司详情
#import "ICN_PicBroswerView.h" // 图片浏览器简化版
#import "ICN_PublishComplainPager.h" // 跳转到发布吐槽页面
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_PublishAskQuestionPager.h" // 跳转到发布提问页面
#import "ICN_MyQuestionDetialPager.h" // 跳转到我的提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 跳转到用户的提问详情页面
#import "ALB_WeChatPay.h"       //微信支付
#import "ICN_AliPayManager.h"   //支付宝支付
#import "ICN_MyfridentViewController.h" // 分享给好友
#import "AliVcMoiveViewController.h"
#import "NSDate+TimeStamp.h"
@interface ICN_LiveDetialViewController ()<ICN_DynWarnViewDelegate,reloadDelegate>
#pragma mark ----------------UIVIEW---------------------
@property (nonatomic,strong) ICN_LiveDetialHeaderView * headerView;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
#pragma mark ----------------UIBUTON---------------------
@property (weak, nonatomic) IBOutlet UIButton *ApplyBtn;
@property (nonatomic,strong) ICN_IntegralView * popView;
@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗
@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
#pragma mark ----------------属性---------------------
@property (nonatomic,strong) NSMutableArray * dataSourceArr;
@property (nonatomic,strong) ICN_YouMengShareModel * transmitModel;
@property (nonatomic,strong) ICN_liveModel *model ; //详情model
@property (nonatomic,strong) NSString * token;
@property (nonatomic,assign) NSInteger type; //支付方式
@property (nonatomic,strong) ICN_ApplySucessView * applySucessView;//报名成功view
@property (weak, nonatomic) IBOutlet UIButton *collectBtn;
@end

@implementation ICN_LiveDetialViewController

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

#pragma mark - ---------- 懒加载 ----------
- (ICN_ApplySucessView *)applySucessView
{
    if (!_applySucessView) {
        _applySucessView = XIB(ICN_ApplySucessView);
        [_applySucessView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    }
    return _applySucessView;
}

- (NSString *)token
{
    if (!_token) {
        _token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    return _token;
}
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (ICN_IntegralView *)popView
{
    if (!_popView) {
        _popView = XIB(ICN_IntegralView);
        _popView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    return _popView;
}
- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}
/**
 直播收藏
 
 @param sender 直播收藏按钮
 */

- (IBAction)collectAction:(UIButton *)sender {
    BOOL isLogin = [self getCurrentUserLoginStatus];
    if (!isLogin) {
        ICN_StartViewController *starPager = [[ICN_StartViewController alloc] init]; // 欢迎页面
        ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
        // 先静默从欢迎页跳转到登录页
        [nav pushViewController:pager animated:NO];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        if ([self.model.isCollected integerValue] == 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"已经收藏过了"];
        }else{
            
            NSString *token = [USERDEFAULT objectForKey:HR_CurrentUserToken];
            [HTTPManager POST_PATH:PATH_doAddActivityCollection params:@{@"token":token,@"id":self.model.activityId} success:^(id responseObject) {
                sender.selected = YES;
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
            } failure:^(NSError *error) {
                
            }];
        }
    }

}

- (ICN_LiveDetialHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_LiveDetialHeaderView);
        _headerView.delegate = self;
    }
    return _headerView;
}
#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.correlation) {
        self.ApplyBtn.hidden = YES;
        self.headerView.priceLabel.hidden = YES;
    }
    
    
    //默认使用微信支付
    self.type = 200;
    // Do any additional setup after loading the view from its nib.
    //初始化设置界面
    
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    
    self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
         [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    }];
    
    [self netWorkRequest:TYPE_RELOADDATA_DOWM];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)shareAction:(UIButton *)sender {
    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_LiveReplay ParamsKey:@"id" ModelId:SF(@"%@,%@", self.liveID ,self.actID)Title:self.model.title IconUrl:ICN_IMG(self.model.pic)   Content:self.model.content];
    // 执行转发相关操作
    [APPLICATION.keyWindow addSubview:self.replayView];

}

#pragma mark - ---------- 重写父类方法 ----------
- (void)configUI:(ICN_liveModel *)model{
    if ([model.isCollected integerValue] == 1) {
        self.collectBtn.selected = YES;
    }else{
        self.collectBtn.selected = NO;
    }

    NSInteger memberStatus = [model.memberStatus integerValue];
    switch (memberStatus) {
        case 1:
        {
            if ([model.price doubleValue]>0) {
                [self.ApplyBtn setTitle:@"立即购买" forState:UIControlStateNormal];
                [self.ApplyBtn setImage:[UIImage imageNamed:@"立即购买"] forState:UIControlStateNormal];
            }else{
                [self.ApplyBtn setTitle:@"立即报名" forState:UIControlStateNormal];
            }
        }
            break;
        case 2:
        {
            [self.ApplyBtn setTitle:@"报名截止" forState:UIControlStateNormal];
            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
            [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
        }
            break;
        case 3:
        {
            [self.ApplyBtn setTitle:@"报名截止" forState:UIControlStateNormal];
            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
             [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
        }
            break;
        case 4:
        {
            [self.ApplyBtn setTitle:@"报名截止" forState:UIControlStateNormal];
            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
            [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
        }
            break;
        case 5:
        {
            [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
            [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
        }
            break;
        case 6:
        {
            [self.ApplyBtn setTitle:@"等待付款" forState:UIControlStateNormal];
            [self.ApplyBtn setImage:[UIImage imageNamed:@"立即购买"] forState:UIControlStateNormal];
            
        }
            break;
            
        default:
            break;
    }
    
    //如果isAuth == 0 是自己的直播不能报名
    if ([model.isAuth isEqualToString:@"1"]) {
        self.ApplyBtn.hidden = YES;
    }
   
    
}

#pragma mark 列表数据网络请求
/**
 列表页数据请求
 
 @param isMore 是否加载更多
 */
- (void)netWorkRequest:(BOOL)isMore{
    NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    [[HRRequest manager]POST:PATH_activityDetail para:@{@"token":token,@"id":self.liveID,@"activityId":self.actID} success:^(id data) {
        
        [self.mainTableView.mj_header endRefreshing];
        self.dataSourceArr = nil;
        ICN_liveModel *model = [[ICN_liveModel alloc]initWithDictionary:data error:nil];
        self.model = model;
        self.headerView.model = model;
        [self configUI:model];
        NSLog(@"%@",model.memberPosition);
        [model.correlationList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.dataSourceArr addObject:obj];
            
        }];
        [self.mainTableView reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
        [self.mainTableView.mj_header endRefreshing];
    }];
    
}

/**
 立即报名按钮
 
 @param sender 立即报名
 */
- (IBAction)ApplyAction:(UIButton *)sender {
     WEAK(weakSelf);
    BOOL isLogin = [self getCurrentUserLoginStatus];
    if (!isLogin) {
        ICN_StartViewController *starPager = [[ICN_StartViewController alloc] init]; // 欢迎页面
        ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
        // 先静默从欢迎页跳转到登录页
        [nav pushViewController:pager animated:NO];
        [self presentViewController:nav animated:YES completion:nil];
    }else{

    if ([sender.titleLabel.text isEqualToString:@"报名截止"]||[sender.titleLabel.text isEqualToString:@"已报名"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"报名已截至"];
    }else{
        NSString *token = [USERDEFAULT objectForKey:HR_CurrentUserToken];
        //如果积分不够 --- 或者没有积分
       
        if ([self.model.price doubleValue] == 0) {
            [HTTPManager POST_PATH:PATH_createOrderAPP params:@{@"token":token,@"payScore":@"2",@"activityId":self.actID,@"status":@"2"} success:^(id responseObject) {
                [APPLICATION.keyWindow addSubview:self.applySucessView];
                [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                
                [weakSelf netWorkRequest:NO];
            } failure:^(NSError *error) {
                
            }];
            return;
        }
        if ([self.model.deductionScore doubleValue] > 0) {
            if ([self.model.deductionScore integerValue] < 100) {
                
                [HTTPManager POST_PATH:PATH_createOrderAPP params:@{@"token":token,@"payScore":@"2",@"activityId":self.actID,@"status":@"2"} success:^(id responseObject) {
                    [APPLICATION.keyWindow addSubview:self.popView];
                    
                    [weakSelf netWorkRequest:NO];
                    ICN_payDetialModel *model = [[ICN_payDetialModel alloc]initWithDictionary:responseObject error:nil];
                    WEAK(weakSelf);
                    [self.popView ReturnMakeSure:^{
                        //微信支付
                        if (self.type == 200) {
                            [ALB_WeChatPay shareWeiChatPayWithActivityId:model.orderId];
                            [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                            [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                        }else{  //支付宝支付
                            BOOL aliPay  = [ICN_AliPayManager AliPayWithActivityId:model.orderId];
                            if (aliPay) {
                                [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                                [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                                [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                                
                            }
                        }
                        
                        [weakSelf.popView removeFromSuperview];
                    }];
                    //点击的按钮传type
                    [self.popView ReturnChoosePay:^(NSInteger type) {
                        self.type = type;
                    }];
                    
                } failure:^(NSError *error) {
                    
                }];
            }else{
                //有积分有钱
                NSArray * styles = @[
                                     [NSNumber numberWithInteger:JXTAlertActionStyleDefault],
                                     [NSNumber numberWithInteger:JXTAlertActionStyleDefault],
                                     ];
                
                [JXTAlertTools showArrayAlertWith:self title:nil message:SF(@"您当前有%@积分，是否使用抵扣？",self.model.deductionScore)  callbackBlock:^(NSInteger btnIndex) {
                    if (btnIndex == 0) {
                        //生成订单接口
                        [HTTPManager POST_PATH:PATH_createOrderAPP params:@{@"token":token,@"payScore":@"1",@"activityId":self.actID,@"status":@"2"} success:^(id responseObject) {
                             [weakSelf netWorkRequest:NO];
                            ICN_payDetialModel *model = [[ICN_payDetialModel alloc]initWithDictionary:responseObject error:nil];
                            [APPLICATION.keyWindow addSubview:self.popView];
                            self.popView.titleLabel.text = SF(@"您使用了%@积分抵扣了%.2f元还需支付%@元",model.score ,[self.model.price floatValue] - [model.receiptAmount floatValue],model.receiptAmount);
                            WEAK(weakSelf);
                            [self.popView ReturnMakeSure:^{
                                //微信支付
                                if (self.type == 200) {
                                    [ALB_WeChatPay shareWeiChatPayWithActivityId:model.orderId];
                                    [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                                    [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                                    [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                                }else{  //支付宝支付
                                    BOOL aliPay  = [ICN_AliPayManager AliPayWithActivityId:model.orderId];
                                    if (aliPay) {
                                        [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                                        [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                                        [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                                    }
                                }
                                
                                [weakSelf.popView removeFromSuperview];
                            }];
                            //
                            [self.popView ReturnChoosePay:^(NSInteger type) {
                                self.type = type;
                            }];
                        } failure:^(NSError *error) {
                            
                        }];
                        
                    }
                    //不使用积分
                    if (btnIndex == 1) {
                        //生成订单接口
                        [HTTPManager POST_PATH:PATH_createOrderAPP params:@{@"token":token,@"payScore":@"2",@"activityId":self.actID,@"status":@"2"} success:^(id responseObject) {
                            [weakSelf netWorkRequest:NO];
                            ICN_payDetialModel *model = [[ICN_payDetialModel alloc]initWithDictionary:responseObject error:nil];
                            [APPLICATION.keyWindow addSubview:self.popView];
                            self.popView.titleLabel.text = SF(@"您需要支付%@元报名费",model.receiptAmount);
                            WEAK(weakSelf);
                            [self.popView ReturnMakeSure:^{
                                //微信支付
                                if (self.type == 200) {
                                    [ALB_WeChatPay shareWeiChatPayWithActivityId:model.orderId];
                                    [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                                    [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                                    [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];}
                                else{  //支付宝支付
                                    
                                    BOOL aliPay  = [ICN_AliPayManager AliPayWithActivityId:model.orderId];
                                    if (aliPay) {
                                        [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                                        [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                                        [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                                    }
                                    
                                }
                                
                                [weakSelf.popView removeFromSuperview];
                            }];
                            //
                            [self.popView ReturnChoosePay:^(NSInteger type) {
                                self.type = type;
                            }];
                        } failure:^(NSError *error) {
                            
                        }];
                        
                    }
                } cancelButtonTitle:nil otherButtonTitleArray:@[@"使用",@"不使用"] otherButtonStyleArray:styles] ;
            }
            
        }else{
            [HTTPManager POST_PATH:PATH_createOrderAPP params:@{@"token":token,@"payScore":@"2",@"activityId":self.actID,@"status":@"2"} success:^(id responseObject) {
                ICN_payDetialModel *model = [[ICN_payDetialModel alloc]initWithDictionary:responseObject error:nil];
                [APPLICATION.keyWindow addSubview:self.popView];
                WEAK(weakSelf);
                [weakSelf netWorkRequest:NO];
                self.popView.titleLabel.text = SF(@"您需要支付%@元报名费",model.receiptAmount);
                [self.popView ReturnMakeSure:^{
                    //微信支付
                    if (self.type == 200) {
                        [ALB_WeChatPay shareWeiChatPayWithActivityId:model.orderId];
                        [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                        [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                        [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];}
                    else{  //支付宝支付
                        
                        BOOL aliPay  = [ICN_AliPayManager AliPayWithActivityId:model.orderId];
                        if (aliPay) {
                            [self.ApplyBtn setTitle:@"已报名" forState:UIControlStateNormal];
                            [self.ApplyBtn setImage:nil forState:UIControlStateNormal];
                            [self.ApplyBtn setBackgroundColor:RGB0X(0X7e94a2)];
                        }
                        
                    }
                    
                    [weakSelf.popView removeFromSuperview];
                }];
                //
                [self.popView ReturnChoosePay:^(NSInteger type) {
                    self.type = type;
                }];
                
            } failure:^(NSError *error) {
                
            }];
        }
        }
    }
    
}

#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                
                break;
            }
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:SF(@"%@,%@",self.transmitModel.title,self.transmitModel.content) IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
        // 在转发之后无论成功失败清空相关Model
        
        self.transmitModel = nil;
    }
}

#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSourceArr.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICN_LiveDetialTableViewCell *cell = XIB(ICN_LiveDetialTableViewCell);
    cell.model = self.dataSourceArr[indexPath.row];
    return cell;
}
#pragma mark ----------------reloadDelegate---------------------
- (void)reloadIndexPathWithHeight:(float)height
{
    
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,490+height)];
    [headView addSubview:self.headerView];
    [self.headerView setFrame:CGRectMake(0, 0,SCREEN_WIDTH, 490+height)];
    [self.mainTableView setTableHeaderView:headView];
}
#pragma mark --- UITableViewDelegate ---
/**
 
 
 @param tableView
 @param indexPath
 @return
 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.rowHeight = self.mainTableView.rowHeight;
    return self.mainTableView.rowHeight;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    correlationListModel *model = self.dataSourceArr[indexPath.row];
//    if (model.liveUrl == nil ||[model.liveUrl isEqualToString:@""]) {
//        ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//        MVC.liveID = model.liveID;
//        MVC.actID = model.activityId;
//        [self.navigationController pushViewController:MVC animated:YES];
//    }else{
    
        
//        NSDate *date = [NSDate date];
//        NSString *comp = [NSDate currentTimeStamp10];
//        if ([comp integerValue] < [model.beginDate integerValue] ||[comp integerValue] > [model.endDate integerValue]) {
//            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
//            MVC.liveID = model.liveID;
//            MVC.actID = model.activityId;
//            [self.navigationController pushViewController:MVC animated:YES];
//        }else{
//            if (model.liveUrl == nil||[model.liveUrl isEqualToString:@""]) {
//                //测试
//                //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
//                NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.streamName)];
//                AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//                MVC.liveID = model.liveID;
//                MVC.actID = model.activityId;
//                //MVC.model = model.infoList;
//                [MVC SetMoiveSource:url];
//                [MVC setHidesBottomBarWhenPushed:YES];
//                [self.navigationController pushViewController:MVC animated:YES];
//            }else{
//                //测试
//                //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
//                NSURL* url = [NSURL URLWithString:model.liveUrl];
//                AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
//                MVC.liveID = model.liveID;
//                MVC.actID = model.activityId;
//                [MVC SetMoiveSource:url];
//                [MVC setHidesBottomBarWhenPushed:YES];
//                [self.navigationController pushViewController:MVC animated:YES];
//            }
//
////            NSURL* url = [NSURL URLWithString:model.liveUrl];
////            AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
////            MVC.liveID = model.liveID;
////            [MVC SetMoiveSource:url];
////            [MVC setHidesBottomBarWhenPushed:YES];
////            [self.navigationController pushViewController:MVC animated:YES];
//        }
    BOOL isLogin = [self getCurrentUserLoginStatus];
    if (!isLogin) {
        ICN_StartViewController *starPager = [[ICN_StartViewController alloc] init]; // 欢迎页面
        ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
        // 先静默从欢迎页跳转到登录页
        [nav pushViewController:pager animated:NO];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
    
    NSInteger memberStatus = [model.memberStatus integerValue];
    switch (memberStatus) {
        case 1:
        {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.liveID;
            MVC.status = model.memberStatus;
            MVC.actID = model.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
        case 2:
        {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.liveID;
            MVC.status = model.memberStatus;
            MVC.actID = model.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
            
            //                    NSURL* url = [NSURL URLWithString:model.infoList.liveUrl];
            //                    AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
            //                    MVC.liveID = model.infoList.packID;
            //                    MVC.model = model.infoList;
            //                    [MVC SetMoiveSource:url];
            //                    [MVC setHidesBottomBarWhenPushed:YES];
            //                    [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
        case 3:
        {
            
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.liveID;
            MVC.status = model.memberStatus;
            MVC.actID = model.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
        case 4:
        {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.liveID;
            MVC.status = model.memberStatus;
            MVC.actID = model.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
        case 5:
        {
            if (model.liveUrl == nil||[model.liveUrl isEqualToString:@""]) {
                //测试
                //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/%@",model.streamName)];
                AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                MVC.liveID = model.liveID;
                MVC.actID = model.activityId;

                [MVC SetMoiveSource:url];
                [MVC setHidesBottomBarWhenPushed:YES];
                [self.navigationController pushViewController:MVC animated:YES];
            }else{
                //测试
                //NSURL* url = [NSURL URLWithString:SF(@"rtmp://live.1ican.com/ixtest/1FB4C74F-FB9B-2EF9-FD0C-AA1B067A059D")];
                NSLog(@"%@",model.liveUrl);
                NSString* encodedString = [model.liveUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSString *str = model.liveUrl;
               NSURL *URL = [NSURL URLWithString:encodedString];
                AliVcMoiveViewController * MVC = [[AliVcMoiveViewController alloc] init];
                MVC.liveID = model.liveID;
                MVC.actID = model.activityId;
                if (URL == nil) {
                    MVC.payUrl = str;
                }else{
                    [MVC SetMoiveSource:URL];
                }
                [MVC setHidesBottomBarWhenPushed:YES];
                [self.navigationController pushViewController:MVC animated:YES];
            }
            
            
        }
            break;
        case 6:
        {
            ICN_LiveDetialViewController *MVC = [[ICN_LiveDetialViewController alloc]init];
            MVC.liveID = model.liveID;
            MVC.status = model.memberStatus;
            MVC.actID = model.activityId;
            [self.navigationController pushViewController:MVC animated:YES];
            
        }
            break;
            
            
        default:
            
            break;
    }
    
}






//}

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    
    //    1002331  动态  1002332 智讯
    
    switch (type) {
            // 点击发布动态
        case Publich_DynamicState:{
            [self transmitSourceDataToDynamicReview];
        }
            break;
            // 点击发布智讯
        case Publich_WisdomState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_WisdomBtn];
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布吐槽
        case Publich_ComplainState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_TopicBtn];
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布提问
        case Publich_AskQuestion:{
            HRLog(@"点击的是右边的按钮");
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_AskActionBtn];
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
           
            // 分享到 I行好友
            ICN_MyfridentViewController *MVC = [[ICN_MyfridentViewController alloc]init];
            NSDictionary *dic = @{@"shareType":@"10",@"shareId":self.transmitModel.modelId,@"contentPic":self.transmitModel.iconUrl,@"content":self.transmitModel.content};
            MVC.ext = dic;
            [self.navigationController pushViewController:MVC animated:YES];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareLiveWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
           // [ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareLiveWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            
            break;
        }
        case ICN_ShareToQQBtnType:{
            
            // 分享到 QQ
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            
             [ICN_YouMengShareTool shareLiveWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            
            // 分享到微信朋友圈
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareLiveWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            NSLog(@"分享到QQ空间");
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareLiveWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            break;
        }
        default:
            break;
    }
    [self.warnView removeFromSuperview];
    [self.replayView removeFromSuperview];
    
}

#pragma mark --- NSCopying ---

- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
