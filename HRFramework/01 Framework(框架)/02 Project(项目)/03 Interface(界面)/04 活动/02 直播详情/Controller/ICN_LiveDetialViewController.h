//
//  ICN_LiveDetialViewController.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_LiveDetialViewController : BaseViewController

@property (nonatomic,strong) NSString * liveID; /*直播id **/

@property (nonatomic,strong) NSString * actID;//活动id

@property (nonatomic,strong) NSString * status; //状态

@property (nonatomic,assign) BOOL  correlation; //是否是相关直播

@end
