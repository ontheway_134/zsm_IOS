//
//  ICN_ActivityDetialViewController.h
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_ActivityDetialViewController : BaseViewController

@property (nonatomic,strong) NSString * packID;
/*写一个标示 代表这个页面是从订单部分传递过来的*/
@property (strong,nonatomic)NSString * judgeStr;
//导航栏标题
@property (nonatomic,strong) NSString * navTitle;
@end
