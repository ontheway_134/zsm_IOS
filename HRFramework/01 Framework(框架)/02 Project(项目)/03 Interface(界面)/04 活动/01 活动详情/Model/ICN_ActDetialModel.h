//
//  ICN_ActDetialModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/13.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"
@protocol activityModel <NSObject>
@end
@interface activityModel :BaseOptionalModel
@property (nonatomic,strong) NSString * actID;
@property (nonatomic,strong) NSString * memberId;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * createDate;
@property (nonatomic,strong) NSString * beginDate;
@property (nonatomic,strong) NSString * endDate;
@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString* memberPosition;
@property (nonatomic,strong) NSString * activityId;
@property (nonatomic,strong) NSString * liveUrl;
@property (nonatomic,strong) NSString * hasBegin;
@property (nonatomic,strong) NSString * streamName;
@end

@interface ICN_ActDetialModel : BaseOptionalModel
@property (nonatomic,strong) NSString * liveID;
@property (nonatomic,strong) NSString * subNum;
@property (nonatomic,strong) NSString* memberPosition;
@property (nonatomic,strong) NSString * isAdopt;
@property (nonatomic,strong) NSString * canNum;
@property (nonatomic,strong) NSString * endDate;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * address;
@property (nonatomic,strong) NSString * memberId;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * beginDate;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * price;
@property (nonatomic,strong) NSString * memberStatus; //用户购买状态
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString * pic;
@property (nonatomic,strong) NSString * memberScore; //用户积分
@property (nonatomic,strong) NSString * deductionScore; //可以抵扣的积分
@property (nonatomic,strong) NSString * isCollected;
@property (nonatomic,strong) NSString * streamName;  //直播流
@property (nonatomic,strong) NSArray<activityModel> * activityList;
@end
