//
//  ICN_ActivityDetialHeaderView.h
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_liveModel.h"
#import "ICN_ActDetialModel.h"
@interface ICN_ActivityDetialHeaderView : UIView

@property (nonatomic,strong) ICN_ActDetialModel * model;
@end
