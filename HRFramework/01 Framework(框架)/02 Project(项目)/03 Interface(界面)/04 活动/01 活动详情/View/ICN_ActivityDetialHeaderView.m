//
//  ICN_ActivityDetialHeaderView.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActivityDetialHeaderView.h"
#import "NSString+Date.h"
@interface ICN_ActivityDetialHeaderView()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *memberNickLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *beginDateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picImg;
@end
@implementation ICN_ActivityDetialHeaderView

#pragma mark ----------------属性重写---------------------
- (void)setModel:(ICN_ActDetialModel *)model
{
    _model = model;
    [self.picImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.pic)]];
    self.titleLabel.text = model.title;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.numLabel.text = SF(@"%@/%@人",model.subNum,model.canNum);
    if ([model.price doubleValue] == 0) {
        self.priceLabel.text = @"免费";
    }else{
        self.priceLabel.text = SF(@"￥%@",model.price);
    }
    self.beginDateLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    self.timeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    self.memberNickLabel.text = model.memberNick;
    self.companyNameLabel.text = model.companyName;
    self.memberPositionLabel.text = model.memberPosition;
}

@end
