//
//  ICN_ActiveityDetialContentTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_ActDetialModel.h"
@interface ICN_ActiveityDetialContentTableViewCell : UITableViewCell
@property (nonatomic,strong) activityModel * model;
@end
