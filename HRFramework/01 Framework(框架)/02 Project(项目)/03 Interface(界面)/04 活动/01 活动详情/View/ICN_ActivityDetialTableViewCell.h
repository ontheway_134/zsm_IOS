//
//  ICN_ActivityDetialTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_liveModel.h"
@interface ICN_ActivityDetialTableViewCell : UITableViewCell

@property (nonatomic,strong) activityesListModel * model;
@end
