//
//  ICN_ActiveityDetialContentTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActiveityDetialContentTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_ActiveityDetialContentTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberPositionLabel;
@end
@implementation ICN_ActiveityDetialContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setModel:(activityModel *)model
{
    _model = model;
    self.titleLabel.text = model.title;
    self.nameLabel.text = model.memberNick;
    self.timeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.companyNameLabel.text = model.companyName;
    self.memberPositionLabel.text = model.memberPosition;
    self.adressLabel.text = model.address;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
