//
//  ICN_ActivityMultSearchViewController.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActivityMultSearchViewController.h"
#import "ICN_SearchDynamicListViewModel.h"
#import "ICN_UserDynamicStateDetialVC.h" // 用户动态详情Cell
#import "ICN_CommonPsersonDynamicCell.h" // 动态Cell
#import "ICN_DynStateContentModel.h" // 动态Model
#import "ICN_DynWarnView.h" // 通用提示窗口
#import "ICN_ReviewPublication.h" // 转发页面
#import "ICN_ApplyModel.h"
#import "ICN_ShareManager.h"
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_ActivitySearchReustTableViewCell.h" //活动cell
#import "ICN_professionSearchResultTableViewCell.h" //职场cell
#import "ICN_industryResultTableViewCell.h"   //行业资讯
#import "ICN_PositionNextTableViewCell.h"
#import "BaseOptionalModel.h"
#import "ICN_activityListModel.h"   //搜索活动的model
#import "ICN_IndustryandModel.h"   //行业动态和职业规划的model;
#import "ICN_ActivityDetialViewController.h"    //活动详情
#import "ICN_IndustryNextViewController.h"   //资讯详情
#import "ICN_ActivityMultSearchHeaderView.h"   //头视图
@interface ICN_ActivityMultSearchViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *searchTextF;

@property (weak, nonatomic) IBOutlet UITableView *mainTableview;

@property (nonatomic , strong)ICN_SearchDynamicListViewModel *viewModel;

@property (nonatomic , strong)ICN_DynStateContentModel *replyModel; // 用于记录转发的Model

@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作

@property (nonatomic , strong)NSMutableArray *searcherActivityArr;   //搜索活动的数组
@property(nonatomic , strong)NSMutableArray *searcherIndustryArr;     //搜素行业资讯和职业动态的接口
@property(nonatomic , strong)NSMutableArray *searcherJobArr;     //搜素职场接口


@property (nonatomic , assign) NSInteger Activitypage;   //活动的页数
@property(nonatomic , assign)NSInteger Industrypage;           //行业资讯和职业动态的页数

@property(nonatomic, strong)UIImageView *ima;
@property(nonatomic, strong)UILabel *la;

@property (nonatomic,strong) ICN_ActivityMultSearchHeaderView * headerView;

@end

@implementation ICN_ActivityMultSearchViewController
#pragma mark - ---------- 懒加载 ----------
- (ICN_ActivityMultSearchHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_ActivityMultSearchHeaderView);
    }
    return _headerView;
}
- (NSMutableArray *)searcherJobArr
{
    if (!_searcherJobArr) {
        _searcherJobArr = [NSMutableArray array];
    }
    return _searcherJobArr;
}
-(NSMutableArray *)searcherActivityArr{
    if (!_searcherActivityArr) {
        _searcherActivityArr = [NSMutableArray array];
    }
    return _searcherActivityArr;
}

-(NSMutableArray *)searcherIndustryArr{
    if (!_searcherIndustryArr) {
        _searcherIndustryArr=[NSMutableArray array];
    }
    return _searcherIndustryArr;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setHiddenDefaultNavBar:YES];
    self.searchTextF.delegate = self;
    if (self.searchContent) {
        self.searchTextF.text = self.searchContent;
    }
    [self refresh];
    NSLog(@"%ld",(long)self.type);
    //注册
    [self.mainTableview registerClass:[ICN_PositionNextTableViewCell class] forCellReuseIdentifier:@"ICN_PositionNextTableViewCell"];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    [self.headerView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    [view addSubview:self.headerView];
    [self.mainTableview setTableHeaderView:view];
    
    switch (self.type) {
        case 0:
        {
            self.headerView.categryLabel.text = @"活动";
        }
            break;
        case 1:
        {
            self.headerView.categryLabel.text = @"职场规划";
        }
            break;
        case 2:
        {
            self.headerView.categryLabel.text = @"行业资讯";
        }
            break;


            
        default:
            break;
    }
}


#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
- (IBAction)clickOnBackAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clockOnSearchBtnAction:(UIButton *)sender {
    // 调用显示加的搜索内容数据
    [self.view endEditing:YES];
    [self.searcherActivityArr removeAllObjects];
    [self.searcherIndustryArr removeAllObjects];
    [self.mainTableview reloadData];
    
    [self.searchTextF resignFirstResponder];
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""] && ![self.searchTextF.text isEqualToString:@"搜索关键字"]) {
//        [self.mainTableview.mj_header beginRefreshing];
        //判断是否正在刷新
        if (!self.mainTableview.mj_header.isRefreshing) {
            //调用刷新的方法
            [self refresh];
        }
        
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
        //[self customMethod]; //添加未搜索结果
    }
    
    NSLog(@"%ld",self.type);
    
}

#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- ICN_DynWarnViewDelegate ---

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
            pager.model = self.replyModel;
            self.replyModel = nil;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            NSDictionary *params = @{@"matterId":self.replyModel.DynID};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
            
        case ICN_ShareToWeiFridBtnType:{
            
            NSLog(@"分享到微信朋友圈");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            
            NSLog(@"分享到QQ空间");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
            
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        default:
            [self.warnView removeFromSuperview];
            break;
    }
}

#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark --- UITextFieldDelegate ---
/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    if (self.type == 0) {
        ICN_activityListModel *model = self.searcherActivityArr[indexPath.row];
        ICN_ActivityDetialViewController *MVC = [[ICN_ActivityDetialViewController alloc]init];
        MVC.packID = model.infoList.packID;
        [self.navigationController pushViewController:MVC animated:YES];
    }else if(self.type == 2){
        ICN_IndustryandModel *model = self.searcherIndustryArr[indexPath.row];
        ICN_IndustryNextViewController *MVC = [[ICN_IndustryNextViewController alloc]init];
        MVC.url = model.infoid;
         [self.navigationController pushViewController:MVC animated:YES];
        
    }else if(self.type == 1){
        ICN_IndustryandModel *model = self.searcherIndustryArr[indexPath.row];
        ICN_IndustryNextViewController *MVC = [[ICN_IndustryNextViewController alloc]init];
        MVC.url = model.infoid;
        [self.navigationController pushViewController:MVC animated:YES];
        
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (self.type) {
        case 0:{
            return self.searcherActivityArr.count;
        }
            break;
        case 1:{
            return self.searcherIndustryArr.count;
        }
            break;
        case 2:{
            return self.searcherIndustryArr.count;
        }
            break;
            
        default:
            break;
    }
    return 0;
}
/**
 
 自适应高度
 @param tableView
 @param indexPath
 @return
 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.rowHeight = self.mainTableview.rowHeight;
    return self.mainTableview.rowHeight;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.type) {
        case 0:
        {
            ICN_ActivitySearchReustTableViewCell *cell = XIB(ICN_ActivitySearchReustTableViewCell);
            cell.model = self.searcherActivityArr[indexPath.row];
            return cell;

        }
            break;
        case 1:
        {
            ICN_professionSearchResultTableViewCell *cell = XIB(ICN_professionSearchResultTableViewCell);
            cell.model = self.searcherIndustryArr[indexPath.row];
            return cell;
        }
            break;
        case 2:
        {
            ICN_industryResultTableViewCell *cell = XIB(ICN_industryResultTableViewCell);
            cell.model = self.searcherIndustryArr[indexPath.row];
            return cell;
        }
            break;
            
        default:
            break;
    }
    return nil;
}

#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text != nil && ![textField.text isEqualToString:@""] && ![textField.text isEqualToString:@"搜索关键字"]) {
        self.mainTableview.hidden = NO;
        self.searchContent = textField.text;
        [textField resignFirstResponder];
        [self.mainTableview.mj_header beginRefreshing];
    }
    return YES;
}
- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];;
    view.backgroundColor = RGB0X(0Xf0f0f0);
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
#pragma mark --- NSCopying ---
- (void)cofigShareContent{
    
}

#pragma mark - --- 网络请求 ---
////职场搜索
//-(void)httpNetworingSearcherJob:(NSInteger)ActivityPage
//{
//    
//}
//活动的网络请求
-(void)httpNetworingSearcherActivity:(NSInteger)ActivityPage{
    NSDictionary *dic=@{@"keyword":self.searchTextF.text,@"page":[NSString stringWithFormat:@"%ld",(long)ActivityPage]};

    [[HRRequest manager]POST:PATH_SECRCHERACTIVITY para:dic success:^(id data) {
       
        
        if (self.Activitypage == 1) {
            [self.searcherActivityArr removeAllObjects];
        }
    
        
        [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            
            ICN_activityListModel *searcher=[[ICN_activityListModel alloc]initWithDictionary:obj error:nil];
            [self.searcherActivityArr addObject:searcher];
            
            
        }];
        NSArray *temp = data;
        if (temp.count == 0) {
            [self customMethod];
        }else{
            [self.ima removeFromSuperview];
            [self.la removeFromSuperview];
        }
        
        [self.mainTableview reloadData];
        
    } faiulre:^(NSString *errMsg) {
         [self customMethod];
    }];
    if (self.searcherActivityArr.count != 0) {
        [self.ima removeFromSuperview];
        [self.la removeFromSuperview];
    }
    [self endRefresh];

}

/**
 <#Description#>

 @param informationType 1为行业资讯 2职场动态
 @param industrypage 页数
 */
-(void)httpIndustryInformationAndCareerplanning:(NSString *)informationType AndPage:(NSInteger)industrypage{
    NSDictionary *dic;
    if ([informationType integerValue] == 1) {
        dic=@{@"positionTitle":self.searchTextF.text,@"page":[NSString stringWithFormat:@"%ld",(long)industrypage],@"informationType":@"2"};
    }else{
        dic=@{@"positionTitle":self.searchTextF.text,@"page":[NSString stringWithFormat:@"%ld",(long)industrypage],@"informationType":@"1"};

    }
    
    
    
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_SEARCHERINDUSTRY params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        
        if (self.Industrypage == 1) {
            [self.searcherIndustryArr removeAllObjects];
        }
        
        if (basemodel.code == 0) {
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_IndustryandModel *model=[[ICN_IndustryandModel alloc]initWithDictionary:dic error:nil];
                [self.searcherIndustryArr addObject:model];
            }
            
                [self.ima removeFromSuperview];
                [self.la removeFromSuperview];
            

           
        }else{
            if (industrypage == 1) {
                 [self customMethod];
            }
        }
        [self.mainTableview reloadData];
        [self endRefresh];
       
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];

    [self.ima removeFromSuperview];
    [self.la removeFromSuperview];

}

#pragma mark - --- PriVate Method ---

-(void)refresh{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        switch (self.type) {
            case 0:{
                self.Activitypage = 1;
                [self httpNetworingSearcherActivity:self.Activitypage];
            }
                
                break;
            case 1:{
                self.Industrypage = 1;
                [self httpIndustryInformationAndCareerplanning:@"1" AndPage:self.Industrypage];
            }
                break;
            case 2:{
                self.Industrypage = 1;
                 [self httpIndustryInformationAndCareerplanning:@"2" AndPage:self.Industrypage];
            }

                break;
        }
        
       
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.mainTableview.mj_header = header;
    
    self.mainTableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        switch (self.type) {
            case 0:{
                self.Activitypage += 1;
                [self httpNetworingSearcherActivity:self.Activitypage];
            }
                
                break;
            case 1:{
                self.Industrypage += 1;
                [self httpIndustryInformationAndCareerplanning:@"1" AndPage:self.Industrypage];
            }
                break;
            case 2:{
                self.Industrypage += 1;
                [self httpIndustryInformationAndCareerplanning:@"2" AndPage:self.Industrypage];
            }
                
                break;
        }
        
    }];
    [self.mainTableview.mj_header beginRefreshing];
    
}

-(void)endRefresh{
    [self.mainTableview.mj_header endRefreshing];
    [self.mainTableview.mj_footer endRefreshing];
}

//无数据时背景图片的显示
-(void)customMethod{
    
    if (self.searcherActivityArr.count == 0 || self.searcherIndustryArr.count==0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无数据";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
}


@end
