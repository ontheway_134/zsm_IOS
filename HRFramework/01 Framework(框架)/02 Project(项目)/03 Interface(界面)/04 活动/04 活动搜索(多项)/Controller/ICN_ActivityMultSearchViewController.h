//
//  ICN_ActivityMultSearchViewController.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_ActivityMultSearchViewController : BaseViewController
//type = 0 活动
//type = 1 职业规划
//type = 2 行业资讯

@property (nonatomic,assign) NSInteger type;

@property (nonatomic , copy)NSString *searchContent; // 搜索内容

@end
