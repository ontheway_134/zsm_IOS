//
//  ICN_professionSearchResultTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_IndustryandModel.h"

@interface ICN_professionSearchResultTableViewCell : UITableViewCell

@property(nonatomic,strong)ICN_IndustryandModel *model;

@end
