

#import "ICN_ActivitySearchReustTableViewCell.h"
#import "NSString+Date.h"

@interface ICN_ActivitySearchReustTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titmeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contentIcon;

@end
@implementation ICN_ActivitySearchReustTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


-(void)setModel:(ICN_activityListModel *)model{
    _model=model;
    [self.contentIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.infoList.pic)] placeholderImage:[UIImage imageNamed:@"头像"]];
    self.titleLabel.text=model.infoList.title;
    self.priceLabel.text = model.infoList.price;
    self.titmeLabel.text = [NSString ConvertStrToDetialTime:model.infoList.beginDate];
    self.contentLabel.text = model.infoList.content;
}

@end
