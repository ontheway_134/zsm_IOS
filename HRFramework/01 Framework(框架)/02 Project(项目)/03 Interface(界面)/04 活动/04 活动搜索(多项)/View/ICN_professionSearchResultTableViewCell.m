//
//  ICN_professionSearchResultTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_professionSearchResultTableViewCell.h"

@interface ICN_professionSearchResultTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *readCountLabel; //阅读量

@end

@implementation ICN_professionSearchResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(ICN_IndustryandModel *)model{
    _model = model;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:nil];
    self.contentLabel.text = model.title;
    self.timeLabel.text = model.createDate;
    self.readCountLabel.text = model.pageViews;

}

@end
