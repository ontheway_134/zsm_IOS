//
//  ICN_ActivitySearchReustTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_activityListModel.h"   //搜索活动的model

@interface ICN_ActivitySearchReustTableViewCell : UITableViewCell

@property(nonatomic,strong)ICN_activityListModel *model;

@end
