//
//  ICN_ industryResultTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_industryResultTableViewCell.h"

@interface ICN_industryResultTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageVIew;

@property (weak, nonatomic) IBOutlet UILabel *contenLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end


@implementation ICN_industryResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setModel:(ICN_IndustryandModel *)model{
    _model = model;
    [self.iconImageVIew sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:nil];
    self.contenLabel.text = model.title;
    self.timeLabel.text = model.createDate;
    self.countLabel.text = model.pageViews;
}

@end
