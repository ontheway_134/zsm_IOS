//
//  ICN_LiveDetialBeginTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveDetialBeginTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_LiveDetialBeginTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *memberNickLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberPositionLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *beginDateLabel;
@end
@implementation ICN_LiveDetialBeginTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
#pragma mark ----------------属性重写---------------------
- (void)setModel:(ICN_liveModel *)model
{
    _model = model;
    
    self.titleLabel.text = model.title;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.numLabel.text = SF(@"%@/%@人",model.subNum,model.canNum);
//    if (model.content.length < 300) {
//        self.contentLabel.text = model.content;
//    }else{
//        //截取300字符串
//        NSString *tempStr = [[model.content substringToIndex:300]mutableCopy];
//       
//        self.contentLabel.text = tempStr;
//    }
     self.contentLabel.text = model.content;
    
    self.beginDateLabel.text = [NSString conversionTimeStamp:model.beginDate];
     self.timeLabel.text = SF(@"%@到%@",[NSString conversionTimeStamp:model.beginDate],[NSString conversionTimeStamp:model.endDate]) ;
    self.memberNickLabel.text = model.memberNick;
    self.companyNameLabel.text = model.companyName;
    self.memberPositionLabel.text = model.memberPosition;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
