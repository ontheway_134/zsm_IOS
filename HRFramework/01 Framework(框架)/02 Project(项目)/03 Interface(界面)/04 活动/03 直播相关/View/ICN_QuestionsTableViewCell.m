//
//  ICN_ QuestionsTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_QuestionsTableViewCell.h"
#import "NSString+Date.h"
@interface ICN_QuestionsTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *userNickLabel; // 用户昵称
@property (weak, nonatomic) IBOutlet UILabel *professionLabel; // 行业标签
@property (weak, nonatomic) IBOutlet UILabel *MajorOrWorkLabel; // 专业/职位标签
@property (weak, nonatomic) IBOutlet UILabel *schoolOrCompanyLabel; // 学校/公司标签

#pragma mark --- 正文部分 ---


@property (weak, nonatomic) IBOutlet UILabel *questionContentLabel; // 问题正文标签
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏标签



#pragma mark --- 功能按钮 ---

@property (weak, nonatomic) IBOutlet UIButton *dotBtn; // 用户点赞
@property (weak, nonatomic) IBOutlet UIButton *CommentBtn; // 用户评论


@end

@implementation ICN_QuestionsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

/**
 点赞

 @param sender 点赞按钮
 */
- (IBAction)dotAction:(UIButton *)sender {
    
    if (self.block) {
        self.block();
    }
    
}
- (void)setModel:(ICN_LiveQuestionsModel *)model
{
    _model = model;
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.questionContentLabel.text = model.content;
    self.userNickLabel.text = model.memberNick;
    self.professionLabel.text = model.memberProfessionName;
    [self.dotBtn setTitle:model.praiseNum forState:UIControlStateNormal] ;
    [self.dotBtn setTitle:model.praiseNum forState:UIControlStateSelected] ;
    [self.CommentBtn setTitle:model.commentNum forState:UIControlStateNormal];
    [self.CommentBtn setTitle:model.commentNum forState:UIControlStateSelected];
    self.MajorOrWorkLabel.text = model.memberPosition;
    self.schoolOrCompanyLabel.text = model.companyName;
   //这里少时间戳
    self.timeLabel.text = [NSString conversionTimeStamp:model.createTime];
    
    if ([model.isPraise integerValue] == 0) {
        self.dotBtn.selected = NO;
    }else{
        self.dotBtn.selected = YES;
    }
    
}

/**
 评论

 @param sender 评论按钮
 */
- (IBAction)CommentAction:(UIButton *)sender {
    if (self.Commentblock) {
        self.Commentblock();
    }
    
}
- (void)ReturndotAction:(ReturndotAction)block
{
    self.block = block;
}
- (void)ReturnCommentAction:(ReturnCommentAction)block
{
    self.Commentblock = block;
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.userIcon.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:self.userIcon.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.userIcon.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.userIcon.layer.mask = maskLayer;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
