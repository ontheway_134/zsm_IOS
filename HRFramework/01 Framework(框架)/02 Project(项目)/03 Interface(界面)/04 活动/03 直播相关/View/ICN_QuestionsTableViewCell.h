//
//  ICN_ QuestionsTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_LiveQuestionsModel.h"
typedef void (^ReturndotAction)();
typedef void (^ReturnCommentAction)();

@interface ICN_QuestionsTableViewCell : UITableViewCell

@property (nonatomic,strong) ICN_LiveQuestionsModel * model;
@property (nonatomic,copy) ReturndotAction block;
@property (nonatomic,copy) ReturnCommentAction Commentblock;
- (void)ReturndotAction:(ReturndotAction)block;
- (void)ReturnCommentAction:(ReturnCommentAction)block;
@end
