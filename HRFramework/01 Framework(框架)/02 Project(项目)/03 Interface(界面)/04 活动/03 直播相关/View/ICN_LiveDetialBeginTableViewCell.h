//
//  ICN_LiveDetialBeginTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_liveModel.h"
@interface ICN_LiveDetialBeginTableViewCell : UITableViewCell
@property (nonatomic,strong) ICN_liveModel * model;
@end
