//
//  ICN_CommentDetialTableViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_LiveQuestionsModel.h"
@interface ICN_CommentDetialTableViewCell : UITableViewCell
@property (nonatomic,strong) QuestionsModel * model;
@end
