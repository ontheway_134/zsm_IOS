//
//  ICN_CommentDetialTableViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CommentDetialTableViewCell.h"
@interface ICN_CommentDetialTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *headImg;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
@implementation ICN_CommentDetialTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(QuestionsModel *)model
{
    _model = model;
    [self.headImg sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)]];
    self.nameLabel.text = model.memberNick;
    self.contentLabel.text = model.content;
    
}
- (void)layoutSubviews{
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.headImg.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:self.headImg.bounds.size];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.headImg.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    self.headImg.layer.mask = maskLayer;
    
}

@end
