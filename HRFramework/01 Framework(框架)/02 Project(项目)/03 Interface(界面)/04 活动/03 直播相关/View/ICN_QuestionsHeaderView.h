//
//  ICN_QuestionsHeaderView.h
//  ICan
//
//  Created by 何壮壮 on 17/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_QuestionsHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
