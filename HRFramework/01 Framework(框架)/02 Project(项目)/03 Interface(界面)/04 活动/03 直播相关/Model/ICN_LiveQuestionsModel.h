//
//  ICN_LiveQuestionsModel.h
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@protocol  QuestionsModel <NSObject>
@end
@interface QuestionsModel : BaseOptionalModel
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSString* memberLogo;
@property (nonatomic,strong) NSString * memberNick;
@end
@interface ICN_LiveQuestionsModel : BaseOptionalModel
@property (nonatomic,strong) NSString * quesID;
@property (nonatomic,strong) NSString * quizId;
@property (nonatomic,strong) NSString * memberProfessionName;
@property (nonatomic,strong) NSString * memberPosition;
@property (nonatomic,strong) NSString * praiseNum;
@property (nonatomic,strong) NSString * commentNum;
@property (nonatomic,strong) NSString * isPraise;
@property (nonatomic,strong) NSString * activityId;
@property (nonatomic,strong) NSString * memberId;
@property (nonatomic,strong) NSString * createTime;
@property (nonatomic,strong) NSString * memberNick;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * memberProfession;
@property (nonatomic,strong) NSString * memberLogo;
@property (nonatomic,strong) NSString * status;
@property (nonatomic,strong) NSString * content;
@property (nonatomic,strong) NSArray<QuestionsModel> * commentList;
@end
