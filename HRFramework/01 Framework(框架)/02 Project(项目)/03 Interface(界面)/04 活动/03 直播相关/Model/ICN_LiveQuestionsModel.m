//
//  ICN_LiveQuestionsModel.m
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveQuestionsModel.h"
@implementation QuestionsModel
@end
@implementation ICN_LiveQuestionsModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"quesID" : @"id"}];
}
@end
