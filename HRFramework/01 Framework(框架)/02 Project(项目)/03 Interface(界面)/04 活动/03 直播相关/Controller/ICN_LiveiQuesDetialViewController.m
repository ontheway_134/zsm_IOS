//
//  ICN_LiveiQuesDetialViewController.m
//  ICan
//
//  Created by 何壮壮 on 17/3/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_LiveiQuesDetialViewController.h"
#import "ICN_QuestionsTableViewCell.h"
#import "ICN_LiveDetialTableViewCell.h"
#import "ICN_LiveDetialBeginTableViewCell.h"
#import "ICN_CommentDetialTableViewCell.h"
#pragma mark ----------------UIVIEW---------------------
#import "ICN_QuestionsHeaderView.h"
#import "ICN_liveQuesDetialHeaderView.h"
#pragma mark ----------------MODEL---------------------
#import "ICN_LiveQuestionsModel.h"
#import "ICN_liveModel.h"

@interface ICN_LiveiQuesDetialViewController ()
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic,strong) ICN_liveQuesDetialHeaderView * header;
#pragma mark ----------------属性---------------------
@property (nonatomic,strong) NSMutableArray * dataSourceArr;
@property (nonatomic,strong) ICN_LiveQuestionsModel * model;
@property (weak, nonatomic) IBOutlet UIView *textBackGround;

@property (weak, nonatomic) IBOutlet UITextField *inputTF;
@end

@implementation ICN_LiveiQuesDetialViewController
#pragma mark - ---------- 懒加载 ----------
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}

- (ICN_liveQuesDetialHeaderView *)header
{
    if (!_header) {
        _header = XIB(ICN_liveQuesDetialHeaderView);
    }
    return _header;
}
#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    self.mainTableView.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
        [self netWorkRequest:NO];
    }];
    [self netWorkRequest:NO];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
- (IBAction)backAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 评论

 @param sender 评论按钮
 */
- (IBAction)CommentAction:(UIButton *)sender {
    if (self.inputTF.text.length!=0) {
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        [[HRRequest manager]POST:PATH_addQuiz para:@{@"tolen":token, @"activityId":self.model.activityId,@"quizId":self.model.quesID,@"content":self.inputTF.text} success:^(id data) {
             self.inputTF.text = nil;
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"评论成功"];
             [self netWorkRequest:NO];
        } faiulre:^(NSString *errMsg) {
            self.inputTF.text = nil;
        }];
        [self.view endEditing:YES];

    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"评论不能为空"];
    }
}
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
- (void)netWorkRequest:(BOOL)isMore
{
    
    NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    [[HRRequest manager]POST:PATH_activityQuizDetail para:@{@"token":token,@"id":self.quesID} success:^(id data) {
        ICN_LiveQuestionsModel *model = [[ICN_LiveQuestionsModel alloc]initWithDictionary:data error:nil];
        self.model = model;
        self.dataSourceArr = nil;
        [model.commentList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [self.dataSourceArr addObject:obj];
        }];
        [self.mainTableView reloadData];
         [NOTIFICATION postNotificationName:@"点赞" object:nil];
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
    }];

}
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark --- UITextFieldDelegate ---
#pragma mark - ---------- 协议方法 ----------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return self.dataSourceArr.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ICN_QuestionsTableViewCell *cell = XIB(ICN_QuestionsTableViewCell);
        cell.model = self.model;
        [cell ReturndotAction:^{
            //PATH_doPraise
            NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            [[HRRequest manager]POST_PATH:PATH_doPraise params:@{@"token":token,@"id":self.model.quesID} success:^(id result) {
               
                [self netWorkRequest:NO];
                [self.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
        }];

        
        return cell;
    }else
    {
        ICN_CommentDetialTableViewCell *cell = XIB(ICN_CommentDetialTableViewCell);
        cell.model = self.dataSourceArr[indexPath.row];
        return cell;
    }
    

}

#pragma mark --- UITableViewDelegate ---
/**
 
 
 @param tableView <#tableView description#>
 @param indexPath <#indexPath description#>
 @return <#return value description#>
 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.rowHeight = self.mainTableView.rowHeight;
    return self.mainTableView.rowHeight;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark --- NSCopying ---
#pragma mark - 回收键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
