//
//  ICN_IndustryNextViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndustryNextViewController.h"
#import "ICN_ICN_IndustryNextModel.h"
#import "ICN_reportReportViewController.h"
#import "ICN_DynWarnView.h"
#import "ICN_ShareManager.h"
#import "ICN_ApplyModel.h"
#import "ICN_StartViewController.h"
#import "ToolAboutTime.h"
#import "ICN_CommonShareModel.h"
#pragma mark - ---------- 跳转页面/视图 头文件 ----------------------
#import "ICN_LiveDetialViewController.h"
#import "ICN_TransmitToDynamicPager.h"
#import "ICN_StartViewController.h" // 引导页
#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_DynSearchPagerVC.h" // 搜索页面
#import "ICN_UserHomePagerVC.h" // 用户主页界面
#import "ICN_TopicPublicationVC.h" // 跳转到发布话题页面
#import "ICN_DynamicStatePublicationVC.h" // 跳转到发布动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 跳转到动态评论页面
#import "ICN_ReviewPublication.h" // 转发到I行动态页面
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_CompanyDetialVC.h" // 公司详情
#import "ICN_PicBroswerView.h" // 图片浏览器简化版
#import "ICN_PublishComplainPager.h" // 跳转到发布吐槽页面
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_PublishAskQuestionPager.h" // 跳转到发布提问页面
#import "ICN_MyQuestionDetialPager.h" // 跳转到我的提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 跳转到用户的提问详情页面
#import "ALB_WeChatPay.h"       //微信支付
#import "ICN_AliPayManager.h"   //支付宝支付
#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_IntegralView.h"
#import "ICN_DynWarnView.h"
#import "JXTAlertTools.h"
#import "ICN_DynamicStateHeader.h" // 动态的头文件
#import "ICN_DynamicStateVC+HXLogin.h" // 环信登录类目
#import "HRNetworkingManager+DynFirstPager.h" // 网络请求类目
#import "ICN_DynamicStateFirstPagerViewModel.h" // 首页ViewModel
#import "ICN_NearestPeopleViewModel.h" // 周边雷达管理器
#import "ICN_YouMengShareTool.h"                // 分享工具类
#import "ICN_ShareManager.h" // 分享管理器
#import "ICN_ComplainListViewModel.h" // 吐槽的ViewModel
#import "ICN_MyfridentViewController.h"// 分享到动态
#import "ICN_GuiderPager.h"
static NSString *const PATH_positionNewsCollect = @"MyPosition/PositionNews/positionNewsCollect";
static NSString *const PATH_postionNewsdetail = @"MyPosition/PositionNews/postionNewsdetail";
//static NSString *const PATH_ShareLfx_news = @"MyPosition/ShareLfx/news";
//static NSString *const PATH_positionNewsCollectDel = @"MyPosition/PositionNews/positionNewsCollectDel";
@interface ICN_IndustryNextViewController ()<UIGestureRecognizerDelegate,ICN_DynWarnViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *wenziLabel;
@property (nonatomic , strong)UIView *alongView;
@property (nonatomic , strong)UIImageView *downImgView;
@property (nonatomic , strong)UITapGestureRecognizer *tap;

@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *dataLabel;
@property(nonatomic)UIImageView *picImageView;
@property(nonatomic)UILabel *summaryLabel;
@property (nonatomic,strong) UILabel * articleFromLabel; //作者
@property (nonatomic,strong) UILabel * articleAuthorLabel; //来源
@property(nonatomic)NSString *collect;
@property(nonatomic)UIView *contentView;
@property(nonatomic)UIView *winView;
@property(nonatomic)UIView *shareView;
@property (nonatomic,strong) UIWebView * webView;
@property (nonatomic , strong)ICN_ICN_IndustryNextModel *shareModel; // 分享需要的Model

#pragma mark ----------------UIVIEW---------------------
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@property (nonatomic,strong) ICN_IntegralView * popView;
@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗
@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
//分享
@property (nonatomic,strong) ICN_YouMengShareModel * transmitModel;

@end

@implementation ICN_IndustryNextViewController
#pragma mark - ---------- 懒加载 ----------
- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.tap.delegate = self;
   
        [self createUI];
        [self datas];
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [APPLICATION.keyWindow removeGestureRecognizer:_tap];
    if (_replayView) {
        [_replayView removeFromSuperview];
        _replayView = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self.scrollView.mj_header beginRefreshing];
}

#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
-(void)btnAction:(UIButton *)btn{
    
    
    ICN_reportReportViewController  * report = [[ICN_reportReportViewController alloc]init];
    _alongView.hidden = YES;
    _downImgView.hidden = YES;
    report.tempStr = @"1";
    report.urlStr = _url;
    [self.navigationController pushViewController:report animated:YES];
    
    
}
//收藏
-(void)collectionAction:(UIButton *)btn{
    ///btn.selected = !btn.selected;
    if ([_collect isEqualToString:@"0"]) {
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        
        NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url],@"token":token};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollect" params:dic success:^(id result) {
//            
//            if ([_collect isEqualToString:@"0"]) {
//                [btn setTitle:@"取消收藏" forState:UIControlStateNormal];
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
//            }
//            
//            [self datas];
//            
//            
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        [HTTPManager POST_PATH:PATH_positionNewsCollect params:dic success:^(id responseObject) {
            if ([_collect isEqualToString:@"0"]) {
                [btn setTitle:@"取消收藏" forState:UIControlStateNormal];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
            }
            
            [self datas];
        } failure:^(NSError *error) {
            
        }];
    }else{
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        
        NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url],@"token":token};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollectDel" params:dic success:^(id result) {
//            
//            
//            if ([_collect isEqualToString:@"1"]) {
//                [btn setTitle:@"收藏" forState:UIControlStateNormal];
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏"];
//            }
//            
//            
//            
//            
//            [self datas];
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        [HTTPManager POST_PATH:PATH_positionNewsCollectDel params:dic success:^(id responseObject) {
            if ([_collect isEqualToString:@"1"]) {
                [btn setTitle:@"收藏" forState:UIControlStateNormal];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏"];
            }
             [self datas];

        } failure:^(NSError *error) {
            
        }];
        
    }
}
- (void)onClick:(UIButton *)button{
    
    //    if (_alongView) {
    //        _alongView.hidden = YES;
    //        _downImgView.hidden = YES;
    //        return ;
    //    }
    button.selected = !button.selected;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    view.tag = 111;
    _alongView = view;
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];
    
    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.userInteractionEnabled = YES;
    imageV.image = [UIImage imageNamed:@"下拉.png"];
    imageV.tag = 112;
    [currentWindow addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(60);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(118);
    }];
    UIButton *btn = [[UIButton alloc]init];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"举报" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [imageV addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(11);
        make.height.mas_equalTo(35);
        
    }];
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
     UIButton *btn2 = [[UIButton alloc]init];
    if ([_collect isEqualToString:@"0"]) {
        [btn2 setTitle:@"收藏" forState:UIControlStateNormal];
    }else{
        
        [btn2 setTitle:@"取消收藏" forState:UIControlStateNormal];
    }
    btn2.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn2 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(collectionAction:) forControlEvents:UIControlEventTouchUpInside];
    [imageV addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];

    
//    UIButton *btn2 = [[UIButton alloc]init];
//    if ([_collect isEqualToString:@"0"]) {
//        [btn2 setTitle:@"收藏" forState:UIControlStateNormal];
//    }else{
//        
//        [btn2 setTitle:@"取消收藏" forState:UIControlStateNormal];
//    }
//    
//    btn2.titleLabel.font = [UIFont systemFontOfSize:11];
//    [btn2 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
//    [btn2 addTarget:self action:@selector(collectionAction:) forControlEvents:UIControlEventTouchUpInside];
//    [imageV addSubview:btn2];
//    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(label.mas_bottom).offset(0);
//        make.right.mas_equalTo(0);
//        make.left.mas_equalTo(0);
//        make.height.mas_equalTo(35);
//    }];
    
    _downImgView = imageV;
    UILabel *label2 = [[UILabel alloc]init];
    label2.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn2.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *btn3 = [[UIButton alloc]init];
    [btn3 setTitle:@"分享" forState:UIControlStateNormal];
    btn3.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn3 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [imageV addSubview:btn3];
    [btn3 addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label2.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    // 单击的 Recognizer
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap:)];
    //点击的次数
    //    singleRecognizer.numberOfTapsRequired = 1; // 单击
    _tap = singleRecognizer;
    singleRecognizer.delegate = self;
    //给self.view添加一个手势监测；
    
    [_alongView addGestureRecognizer:singleRecognizer];
    
    if ([self isCurrentEnterpriseUser] == YES) {
        
        btn2.hidden = YES;
        
    }
    
    
}

/**
 分享
 
 @param btn 分享按钮
 */
-(void)shareBtnAction:(UIButton *)btn{
    
    self.transmitModel = [ICN_YouMengShareModel modelWithParamsType:REPLAY_IndustryInfoReplay ParamsKey:@"informationid" ModelId:self.url Title:self.shareModel.title IconUrl:self.shareModel.pic Content:self.shareModel.content];
    // 执行转发相关操作
    [APPLICATION.keyWindow addSubview:self.replayView];
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    _alongView = nil;

    
}

#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
- (void)transmitSourceDataToDynamicReview{
    
    // 1. 如果transmitModel不存在则直接退出并提示该数据不支持转发
    if (self.transmitModel == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该数据不支持转发"];
    }else{
        // 1. 根据源数据的类型判断下一步进行的操作
        switch (self.transmitModel.paramsType) {
            case REPLAY_DynamicReplay:
            case REPLAY_WisdomReplay:{
                
                break;
            }
            case REPLAY_ComplainReplay:
            case REPLAY_QuestionReplay:
            case REPLAY_ActivityReplay:
            case REPLAY_LiveReplay:
            case REPLAY_PositionReplay:
            case REPLAY_IndustryInfoReplay:{
                // 其他类型需要跳转到新的转发详情页面进行下一步操作
                ICN_TransmitToDynamicPager *pager = [[ICN_TransmitToDynamicPager alloc] initWithReplyType:self.transmitModel.paramsType ContentId:self.transmitModel.modelId Content:SF(@"%@,%@",self.transmitModel.title,self.transmitModel.content) IconStr:self.transmitModel.iconUrl];
                [self currentPagerJumpToPager:pager];
            }
            default:
                break;
        }
        // 在转发之后无论成功失败清空相关Model
        
        self.transmitModel = nil;
    }
}

#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
-(void)datas{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    if (token == nil) {
        return;
    }else{
        NSDictionary *dic = @{@"infoid":[NSString stringWithFormat:@"%@",_url],@"token":token};
        [HTTPManager POST_PATH:PATH_postionNewsdetail params:dic success:^(id responseObject) {
           
           
                
                NSDictionary *dict = responseObject[0];
                //NSDictionary *dict0 = dict[@"0"];
                ICN_ICN_IndustryNextModel *model = [[ICN_ICN_IndustryNextModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                self.shareModel = model;
                NSAttributedString *attrStr = [[NSAttributedString alloc]initWithData:[model.title dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                
                _titleLabel.attributedText = attrStr ;
                
                if (model.createDate == nil) {
                    model.createDate = @"";
                }
            _dataLabel.text = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
            
            [_picImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]]];
            
            
            
                            NSAttributedString *attContent = [[NSAttributedString alloc]initWithData:[model.content dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                           _summaryLabel.attributedText = attContent ;
           // _summaryLabel.text = model.content;
            
                
                _collect  = [NSString stringWithFormat:@"%@",model.iscollect];
                NSLog(@"%@",model.iscollect);
                _articleFromLabel.text = SF(@"原创：%@",model.articleAuthor);
                _articleAuthorLabel.text = SF(@"来源：%@",model.articleFrom);
                [_scrollView.mj_header endRefreshing];
            

        } failure:^(NSError *error) {
             [_scrollView.mj_header endRefreshing];
        }];
        
    }
    
    
    
}

#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------//活动分享
- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    
    //    1002331  动态  1002332 智讯
    
    switch (type) {
            // 点击发布动态
        case Publich_DynamicState:{
            [self transmitSourceDataToDynamicReview];
        }
            break;
            // 点击发布智讯
        case Publich_WisdomState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_WisdomBtn];
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布吐槽
        case Publich_ComplainState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_TopicBtn];
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
            // 发布提问
        case Publich_AskQuestion:{
            HRLog(@"点击的是右边的按钮");
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_AskActionBtn];
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            [self transmitSourceDataToDynamicReview];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            ICN_MyfridentViewController *MVC = [[ICN_MyfridentViewController alloc]init];
            NSDictionary *dic = @{@"shareType":@"8",@"shareId":self.transmitModel.modelId,@"contentPic":self.transmitModel.iconUrl,@"content":self.transmitModel.content};
            MVC.ext = dic;
            [self.navigationController pushViewController:MVC animated:YES];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            
            //[ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            [ICN_YouMengShareTool shareNewsWithYouMengPlatFormType:UMSocialPlatformType_WechatSession Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            
            //[ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
             [ICN_YouMengShareTool shareNewsWithYouMengPlatFormType:UMSocialPlatformType_Sina Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQBtnType:{
            
            // 分享到 QQ
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            
           // [ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareNewsWithYouMengPlatFormType:UMSocialPlatformType_QQ Model:self.transmitModel];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            
            // 分享到微信朋友圈
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            //[ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            
            [ICN_YouMengShareTool shareNewsWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            NSLog(@"分享到QQ空间");
            //[ICN_YouMengShareTool shareWithYouMengPlatFormType:UMSocialPlatformType_WechatTimeLine Model:self.transmitModel];
            
            //[ICN_YouMengShareTool shareActivityWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            
              [ICN_YouMengShareTool shareNewsWithYouMengPlatFormType:UMSocialPlatformType_Qzone Model:self.transmitModel];
            
            
            break;
        }
        default:
            break;
    }
    [self.warnView removeFromSuperview];
    [self.replayView removeFromSuperview];
    
}

- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    
}

#pragma mark --- UITextFieldDelegate ---
#pragma mark --- UITableViewDataSource ---
#pragma mark --- UITableViewDelegate ---
#pragma mark --- NSCopying ---






//=== 分享  分享行业资讯和职场规划：图片、标题（超10字部分“…”）、内容（超20字部分“…”
-(void)wxBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
//        NSLog(@"%@",result);
//        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//        if (basemodel.code == 0) {
//            
//            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
//        }else{
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//            
//        }
//        
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
//    [HTTPManager POST_PATH:PATH_ShareLfx_news params:dic success:^(id responseObject) {
//        NSLog(@"%@",result);
//        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//        if (basemodel.code == 0) {
//            
//            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
//        }else{
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//            
//        }
//
//    } failure:^(NSError *error) {
//        
//    }];
    
    [[HRRequest manager]POST_PATH:PATH_ShareLfx_news params:dic success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }

    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}

// 职场相关分享 --微博
-(void)wbBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
//        
//        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//        if (basemodel.code == 0) {
//            
//            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
//        }else{
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//            
//        }
//        
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [[HRRequest manager]POST_PATH:PATH_ShareLfx_news params:dic success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    
}


// 职场相关分享 --QQ
-(void)qqBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
//        
//        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//        if (basemodel.code == 0) {
//            
//            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
//        }else{
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//            
//        }
//        
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [[HRRequest manager]POST_PATH:PATH_ShareLfx_news params:dic success:^(id result) {
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    
    
    
}
-(void)gbBtnAction:(UIButton *)btn{
    
    _winView.hidden = YES;
    _shareView.hidden = YES;
}
-(void)tapAction:(UITapGestureRecognizer*)recognizer  {
    
    
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    
    
}



-(void)SingleTap:(UITapGestureRecognizer*)recognizer  {

   
        _downImgView.hidden = YES;
        _alongView.hidden = YES;
    _alongView = nil;
 
    

}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)createUI{

    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"资讯详情";
    [self.navigationController.navigationBar setTitleTextAttributes:
  @{NSFontAttributeName:[UIFont systemFontOfSize:16],
    NSForegroundColorAttributeName:RGB0X(0Xffffff)}];
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
   
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
   
    self.navigationItem.leftBarButtonItem = leftItem3;
    
    
    
    UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(0,0, 20, 5)];
    
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setBackgroundImage:[[UIImage imageNamed:@"更多-点.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:btn1];
    self.navigationItem.rightBarButtonItem  = item;

   
     
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    
     [self.view addSubview:_scrollView];
    
    _scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self datas];
    }];
    
    _contentView = [[UIView alloc]init];
    
    [_scrollView addSubview:_contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView);
    }];

    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont systemFontOfSize:15];
    _titleLabel.text = @"";
    _titleLabel.numberOfLines = 0;
    _titleLabel.textColor = RGB0X(0X000000);
    [_contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        //make.width.mas_equalTo(100);
        
        make.top.mas_equalTo(15);
        
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"时间.png"];

    [_contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        
    }];
    
    _dataLabel =[[UILabel alloc]init];
    _dataLabel.text = @"";
    _dataLabel.textColor = RGB0X(0X666666);
    _dataLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_dataLabel];
    [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(5);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];
    
    //来源label
    _articleAuthorLabel = [[UILabel alloc]init];
    _articleAuthorLabel.text = @"来源";
    _articleAuthorLabel.textColor = RGB0X(0X666666);
    _articleAuthorLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_articleAuthorLabel];
    
    [_articleAuthorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_contentView.mas_right).offset(-10);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];
    //原创label
    _articleFromLabel = [[UILabel alloc]init];
    _articleFromLabel.text = @"原创";
    _articleFromLabel.textColor = RGB0X(0X666666);
    _articleFromLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_articleFromLabel];
    
    [_articleFromLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_articleAuthorLabel.mas_left).offset(-12);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];

    
    UILabel *xiantiaoLabel = [[UILabel alloc]init];
    xiantiaoLabel.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:xiantiaoLabel];
    
    [xiantiaoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_dataLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(1);
        
    }];
    
    _picImageView = [[UIImageView alloc]init];
    _picImageView.image = [UIImage imageNamed:@"图片.png"];
    [_contentView addSubview:_picImageView];
    [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(xiantiaoLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(220);
    }];
    
    _summaryLabel = [[UILabel alloc]init];
    _summaryLabel.text = @"";
    _summaryLabel.textColor = RGB0X(0X333333);
    //wenziLabel.font = [UIFont systemFontOfSize:12];
    _summaryLabel.numberOfLines = 0;
    _summaryLabel.font = [UIFont systemFontOfSize:13];
    [_contentView addSubview:_summaryLabel];
    [_summaryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_picImageView.mas_bottom).offset(10);
        make.bottom.mas_equalTo(_contentView.mas_bottom).offset(0);

    }];
    
//    _webView = [[UIWebView alloc]init];
//     [_contentView addSubview:_webView];
//    [_webView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(self.view.mas_left).offset(10);
//        make.right.mas_equalTo(self.view.mas_right).offset(-10);
//        make.top.mas_equalTo(_picImageView.mas_bottom).offset(10);
//        make.bottom.mas_equalTo(_contentView.mas_bottom).offset(0);
//        
//    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    
//    if ([touch.view isKindOfClass:[UIButton class]]){
//        
//        return NO;
//        
//    }
//        return YES;
//    
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
