//
//  ICN_ICN_IndustryNextModel.h
//  ICan
//
//  Created by 那风__ on 16/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ICN_IndustryNextModel : NSObject
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *createDate;
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *content;
//举报
@property(nonatomic)NSString *isreport;
//收藏
@property(nonatomic)NSString *iscollect;
@property (nonatomic,strong) NSString * articleFrom;
@property (nonatomic,strong) NSString * articleAuthor;
@end
