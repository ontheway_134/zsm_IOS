//
//  ICN_IndustryViewController.h
//  ICan
//
//  Created by 那风__ on 16/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJScrollPageViewDelegate.h"
@interface ICN_IndustryViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>
@property (nonatomic,strong) NSArray * dataSource;
@end
