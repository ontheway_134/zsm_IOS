//
//  ICN_IndustryViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndustryViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_IndustryTableViewCell.h"
#import "LKScrollView.h"
#import "ICN_IndustryGDModel.h"
#import "ICN_IndustryNewsModel.h"
#import "ICN_posNavModel.h"
#import "ICN_AdvertimistModel.h"
#import "ICN_IndustryNextViewController.h"
#import "ICN_GroupDetialViewController.h"    //职场规划
#import "ICN_PositionNextOneViewController.h"    //职场规划详情
#import "ICN_ActivityDetialViewController.h"    //活动详情
#import "ICN_ProjectTableViewCell.h"
static NSString *const PATH_postionNewsBanPic1 = @"MyPosition/PositionNewses/positionBannerList";
static NSString *const PATH_postionNewslist1 = @"MyPosition/PositionNewses/positionNewsList";
@interface ICN_IndustryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)LKScrollView *lksview;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)NSMutableArray *topGDArr;
@property(nonatomic,assign)NSInteger page;
@property (nonatomic,strong) NSString * showType;  //显示哪种类型
@property (nonatomic,strong) NSString * typeID; // 资讯ID
@property (nonatomic,strong) NSMutableArray * dataSourceArr;


@end

@implementation ICN_IndustryViewController
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}
- (void)zj_viewDidLoadForIndex:(NSInteger)index
{
   
}
- (void)zj_viewWillAppearForIndex:(NSInteger)index
{
    ICN_posNavModel *model = self.dataSource[index];
    self.typeID = model.ID;
    self.showType = model.showType;
    [self createUI];
    [self datas];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}
-(void)loadMoreData{
    
    _page ++;
    
    [self datas];
    
    
}
-(void)refreshingData{
    
    _page = 1;
    [self datas];
    
    
}
-(void)datas{
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewsBanPic1" params:nil success:^(id result) {
//        
//        NSDictionary *dict =result[@"result"];
//        _topGDArr = [[NSMutableArray alloc]init];
//        
//        for (NSDictionary *dic in dict) {
//            ICN_IndustryGDModel *model = [[ICN_IndustryGDModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_topGDArr addObject:model.lunbo_pic];
//            
//        }
//        
//        [_lksview loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
//        [_tableView reloadData];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    
    [HTTPManager POST_PATH:PATH_postionNewsBanPic1 params:@{@"type":self.typeID} success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        _topGDArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dict) {
//            ICN_IndustryGDModel *model = [[ICN_IndustryGDModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_topGDArr addObject:model.lunbo_pic];
            ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc]initWithDictionary:dic error:nil];
            [_topGDArr addObject:model.pic];
            [self.dataSourceArr addObject:model];
            
        }
        WEAK(weakSelf);
        [_lksview loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
        //=== 职场--招聘跳转对应页面的位置
        [_lksview addActionWithBlock:^(UIButton *button, NSInteger FirstButtonTag) {
            HRLog(@"%ld", (long)FirstButtonTag);
            ICN_AdvertimistModel *model = weakSelf.dataSourceArr[button.tag-21];
            BOOL istoken = YES;
            if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == nil) {
                [USERDEFAULT setValue:SF(@"%d",HR_ICNVisitor) forKey:HR_UserTypeKEY];
                istoken = NO;
            }
            
            if ([model.adType integerValue] == 1) {
                // 跳转到内部链接
                // 1. 区分对应的跳转操作
                switch ([model.adClass integerValue]) {
                    case 1:{
                        // 跳转到首页列表
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 2:{
                        //跳转到职场
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 2;
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 3:{
                        // 跳转到职场详情
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        barController.selectedIndex = 2;
                        ICN_PositionNextOneViewController *position = [[ICN_PositionNextOneViewController alloc]init];
                        position.url = model.pageId;
                        [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                        [self presentViewController:barController animated:YES completion:^{
                            [APPLICATION keyWindow].rootViewController = barController;
                            
                        }];
                        
                        
                        break ;
                    }
                    case 4:{
                        // 跳转到活动列表
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 3;
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 5:{
                        // 跳转到活动详情
                        if (istoken) {
                            // 用户登录
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 3;
                            ICN_ActivityDetialViewController *position = [[ICN_ActivityDetialViewController alloc]init];
                            position.packID = model.pageId;
                            [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                            
                            [self presentViewController:barController animated:YES completion:^{
                                [APPLICATION keyWindow].rootViewController = barController;
                            }];
                        }else{
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                                //延时后想要执行的代码
                                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                barController.selectedIndex = 3;
                                [self presentViewController:barController animated:YES completion:nil];
                            });
                        }
                        
                        break ;
                    }
                    default:
                        break;
                }
            }else{
                // 跳转到广告
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.adUrl]];
            }
            }];

         [_tableView.mj_footer endRefreshing];
        [_tableView reloadData];

    } failure:^(NSError *error) {
         [_tableView.mj_footer endRefreshing];
         [_tableView.mj_header endRefreshing];
    }];
    
    
    
    
    NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",_page],@"type":self.typeID};
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewslist1" params:dic success:^(id result) {
//        
//        if (_page == 1) {
//            [_dataArr removeAllObjects];
//        }
//        
//        NSArray *arr = result[@"result"];
//                for (NSDictionary *dic in arr) {
//            ICN_IndustryModel *model = [[ICN_IndustryModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_dataArr addObject:model];
//        }
//        
//        if (_page == 1) {
//            [_tableView.mj_header endRefreshing];
//        }
//        [_tableView.mj_footer endRefreshing];
//        [_tableView reloadData];
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [HTTPManager POST_PATH:PATH_postionNewslist1 params:dic success:^(id responseObject) {
        if (_page == 1) {
            [_dataArr removeAllObjects];
        }
        
        NSArray *arr = responseObject;
        for (NSDictionary *dic in arr) {
            ICN_IndustryModel *model = [[ICN_IndustryModel alloc]initWithDictionary:dic error:nil];
           // [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        if (_page == 1) {
            [_tableView.mj_header endRefreshing];
        }
        [_tableView.mj_footer endRefreshing];
        [_tableView reloadData];

    } failure:^(NSError *error) {
        [_tableView.mj_footer endRefreshing];
        [_tableView.mj_header endRefreshing];
    }];
    
    


}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
        return;
    }
    if ([self jumpToUserMessageSetUpPager]) {
        //return;
    }else{
        ICN_IndustryNextViewController *icn = [[ICN_IndustryNextViewController alloc]init];
        ICN_IndustryModel *model = _dataArr[indexPath.row];
        
        icn.url = model.infoid;
        
        icn.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:icn animated:YES];
        [self datas];
    }
    
    

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([self.showType isEqualToString:@"1"]) {
        return 103;
    }else{
        return 203;
    }
    return 0;
    

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
    

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.showType isEqualToString:@"1"]) {
        ICN_IndustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        ICN_IndustryModel *model = _dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        ICN_ProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_ProjectTableViewCell" forIndexPath:indexPath];
        ICN_IndustryModel *model = _dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;

    }
    

    return nil;



}
-(void)createUI{

    
//    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 130)];
//    imageV.image = [UIImage imageNamed:@"banner.png"];
    

    _page = 1;
    _dataArr = [[NSMutableArray alloc]init];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height  - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self refreshingData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
  
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    _tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;

    [_tableView registerClass:[ICN_IndustryTableViewCell class] forCellReuseIdentifier:@"cell"];
     [_tableView registerClass:[ICN_ProjectTableViewCell class] forCellReuseIdentifier:@"ICN_ProjectTableViewCell"];
    [self.view addSubview:_tableView];
    
    _lksview = [[LKScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200/BASESCREENPX_HEIGHT)];

    _tableView.tableHeaderView = _lksview;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
