//
//  ICN_IndustryNewsModel.m
//  ICan
//
//  Created by 何壮壮 on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_IndustryNewsModel.h"

@implementation ICN_IndustryNewsModel
//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"ID" : @"id",
             };
}

@end
