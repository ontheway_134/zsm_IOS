//
//  ICN_IndustryModel.h
//  ICan
//
//  Created by 那风__ on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_IndustryModel : BaseOptionalModel
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *createDate;
@property(nonatomic)NSString *pageViews;


@property(nonatomic)NSString *infoid;
@end
