//
//  ICN_IndustryNewsModel.h
//  ICan
//
//  Created by 何壮壮 on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_IndustryNewsModel : BaseOptionalModel
@property (nonatomic,strong) NSString * ID;
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *createDate;
@end
