//
//  ICN_posNavModel.m
//  ICan
//
//  Created by 何壮壮 on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_posNavModel.h"

@implementation ICN_posNavModel
//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ID" : @"id"}];
}

@end
