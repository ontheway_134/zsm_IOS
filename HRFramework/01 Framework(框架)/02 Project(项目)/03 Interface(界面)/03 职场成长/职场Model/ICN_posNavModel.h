//
//  ICN_posNavModel.h
//  ICan
//
//  Created by 何壮壮 on 2017/4/14.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_posNavModel : BaseOptionalModel
@property (nonatomic,strong) NSString * ID;
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * showType;
@end
