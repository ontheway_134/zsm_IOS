//
//  ICN_ProjectTableViewCell.h
//  ICan
//
//  Created by 那风__ on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "ICN_IndustryModel.h"
@interface ICN_ProjectTableViewCell : UITableViewCell
@property(nonatomic)UIImageView *picImage;
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *dataLabel;
@property(nonatomic)UILabel *pageLabel;
@property(nonatomic)ICN_IndustryModel *model;
@end
