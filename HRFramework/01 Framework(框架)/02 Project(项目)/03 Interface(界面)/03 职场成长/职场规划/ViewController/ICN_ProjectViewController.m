//
//  ICN_ProjectViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ProjectViewController.h"
#import "ICN_ProjectModel.h"
#import "ICN_ProjectTableViewCell.h"
#import "ICN_IndustryGDModel.h"
#import "LKScrollView.h"

#import "ICN_IndustryNextViewController.h"

static NSString *const PATH_postionNewsBanPic2 = @"MyPosition/PositionNews/postionNewsBanPic2";
static NSString *const PATH_postionNewslist2 = @"MyPosition/PositionNews/postionNewslist2";
@interface ICN_ProjectViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)NSMutableArray *topGDArr;
@property(nonatomic,strong)LKScrollView *lksView;
@property(nonatomic,assign)NSInteger page;
@end

@implementation ICN_ProjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self.tableView.mj_header beginRefreshing];
}

-(void)loadMoreData{
    
    _page ++;
    
    [self datas];
    
    
}
-(void)refreshingData{
    
    _page = 1;
    [self datas];
    
    
}
-(void)datas{
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewsBanPic1" params:nil success:^(id result) {
//        
//        NSDictionary *dict =result[@"result"];
//        _topGDArr = [[NSMutableArray alloc]init];
//        
//        for (NSDictionary *dic in dict) {
//            ICN_IndustryGDModel *model = [[ICN_IndustryGDModel alloc]init];#0	0x000000010024e8fc in -[ICN_ProjectViewController datas] at /Users/hezhuangzhuang/Desktop/zsm_IOS副本/HRFramework/01 Framework(框架)/02 Project(项目)/03 Interface(界面)/03 职场成长/职场规划/ViewController/ICN_ProjectViewController.m:78

//            [model setValuesForKeysWithDictionary:dic];
//            [_topGDArr addObject:model.lunbo_pic];
//            
//        }
//        
//        [_lksView loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
//        [_tableView reloadData];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [HTTPManager POST_PATH:PATH_postionNewsBanPic2 params:nil success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        _topGDArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dict) {
            ICN_IndustryGDModel *model = [[ICN_IndustryGDModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_topGDArr addObject:model.lunbo_pic];
            
        }
        
        [_lksView loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
        [self.tableView.mj_footer endRefreshing];
        [_tableView reloadData];

    } failure:^(NSError *error) {
         [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_header endRefreshing];
    }];
    
NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",_page]};
//[[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewslist2" params:dic success:^(id result) {
//    NSArray *arr = result[@"result"];
//    
//    if (_page == 1) {
//        [_dataArr removeAllObjects];
//    }
//    for (NSDictionary *dic in arr) {
//        ICN_ProjectModel *model = [[ICN_ProjectModel alloc]init];
//        [model setValuesForKeysWithDictionary:dic];
//        [_dataArr addObject:model];
//    }
//    if (_page == 1) {
//        [_tableView.mj_header endRefreshing];
//    }
//    [_tableView.mj_footer endRefreshing];
//    [_tableView reloadData];
//
//    
//} failure:^(NSDictionary *errorInfo) {
//    
//}];

    [HTTPManager POST_PATH:PATH_postionNewslist2 params:dic success:^(id responseObject) {
        NSArray *arr = responseObject;
        
        if (_page == 1) {
            [_dataArr removeAllObjects];
        }
        for (NSDictionary *dic in arr) {
            ICN_ProjectModel *model = [[ICN_ProjectModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        if (_page == 1) {
            [_tableView.mj_header endRefreshing];
        }
        [_tableView.mj_footer endRefreshing];
        [_tableView reloadData];

    } failure:^(NSError *error) {
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_header endRefreshing];
    }];


}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
        return;
    }
    if (![self jumpToUserMessageSetUpPager]) {
        ICN_IndustryNextViewController *icn = [[ICN_IndustryNextViewController alloc]init];
        ICN_ProjectModel *model = _dataArr[indexPath.row];
        
        icn.url = model.infoid;
        
        icn.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:icn animated:YES];
    }
    
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
    
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_ProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ICN_ProjectModel *model = _dataArr[indexPath.row];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 203;
    
}
-(void)createUI{
    
   
    
    _page = 1;
    _dataArr = [[NSMutableArray alloc]init];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self refreshingData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        //if (_tableView.mj_footer.isRefreshing) {
            [self loadMoreData];
        //}
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    //self.tableView.mj_footer.hidden = YES;
    
    [_tableView registerClass:[ICN_ProjectTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    _lksView = [[LKScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 170)];
    _tableView.tableHeaderView = _lksView;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
