//
//  LKScrollView.h
//  PoorTravel
//
//  Created by 黄金伟 on 16/7/13.
//  Copyright © 2016年 黄金伟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIButton+WebCache.h"
typedef enum {
    /** 靠左 */
    PageControlTypeLeft,
    /** 居中 */
    PageControlTypeMiddel,
    /** 靠右 */
    PageControlTypeRight
}PageControlType;

typedef void (^callBack) (UIButton * button, NSInteger FirstButtonTag);

@interface LKScrollView : UIScrollView <UIScrollViewDelegate>
/** 小圆点 */
@property (nonatomic, strong) UIPageControl * pageControl;

/**
 *  给滚动视图赋值
 *
 *  @param dataInfo 本地:图片名 网络:图片地址
 *  @param image    占位图(本地传nil)
 */
- (void)loadDataWithArray:(NSArray *)dataInfo placeholderImage:(UIImage *)image andPageControlType:(PageControlType)type;

/**
 *  添加点击方法
 *
 *  @param target 目标
 *  @param action 动作
 */
- (void)addTarget:(id)target withAction:(SEL)action;

/**
 *  添加点击方法
 *
 *  @param callBack 回调的block
 */
- (void)addActionWithBlock:(callBack)callBack;

@end
