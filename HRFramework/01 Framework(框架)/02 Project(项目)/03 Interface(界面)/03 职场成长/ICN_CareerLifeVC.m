//
//  ICN_CareerLifeVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CareerLifeVC.h"
#import "ICN_ProjectViewController.h"
#import "ICN_IndustryViewController.h"
#import "ICN_PositionViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_ProfessionSearchViewController.h"
#import "ICN_DynSearchPagerVC.h"
#import "ICN_posNavModel.h"
#import "ICN_GuiderPager.h"
@interface ICN_CareerLifeVC ()<ZJScrollPageViewDelegate>


@property(strong, nonatomic)NSMutableArray<NSString *> *titles;
@property (weak, nonatomic) ZJScrollSegmentView *segmentView;
@property(weak, nonatomic)ZJScrollPageView *scrollPageView;
@property (weak, nonatomic) ZJContentView *contentView;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;
@property (nonatomic,strong) ICN_PositionViewController * PositionViewController;
@property (nonatomic,strong) NSMutableArray * dataSoreceArr;
@end

@implementation ICN_CareerLifeVC
#pragma mark ----------------懒加载---------------------
- (ICN_PositionViewController *)PositionViewController
{
    if (!_PositionViewController) {
        _PositionViewController = [ICN_PositionViewController new];
    }
    return _PositionViewController;
}
- (NSMutableArray *)dataSoreceArr
{
    if (!_dataSoreceArr) {
        ICN_posNavModel *model = [ICN_posNavModel new];
        _dataSoreceArr = [NSMutableArray arrayWithObject:model];
    }
    return _dataSoreceArr;
}
- (NSMutableArray<NSString *> *)titles
{
    if (!_titles) {
        _titles = [NSMutableArray arrayWithObject:@"招聘"];
    }
    return _titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.naviTitle = @"";
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30*AUTO_320, 30)];
    //搜索框
    UIImageView *seacrchBackImgView = [[UIImageView alloc]init];
    seacrchBackImgView.image = [UIImage imageNamed:@"搜索框"];
    [view addSubview:seacrchBackImgView];
    [seacrchBackImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        
    }];
    
    //搜索按钮
    UIImageView *seacrchImgView = [[UIImageView alloc]init];
    seacrchImgView.image = [UIImage imageNamed:@"搜索"];
    [view addSubview:seacrchImgView];
    [seacrchImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.width.mas_equalTo(13);
        make.centerY.mas_equalTo(0);
        make.height.mas_equalTo(14);
        
    }];
    
    UILabel *searchLabel = [[UILabel alloc]init];
    searchLabel.text = @"搜索关键字";
    searchLabel.textColor = RGB0X(0X9BAAB6);
    searchLabel.font = [UIFont systemFontOfSize:12];
    [view addSubview:searchLabel];
    [searchLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(0);
        make.left.mas_equalTo(seacrchImgView.mas_right).offset(5);
        
    }];
   
    self.navigationItem.titleView = view;
    self.navigationItem.leftBarButtonItem = nil;
    UIButton *btn1 = [[UIButton alloc]init];
    [view addSubview:btn1];
    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
   
    //self.navigationItem.rightBarButtonItem  = item;
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    self.childVcs = [self setupChildVc];
    
    
//    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
//    style.showCover = YES;
//    // 不要滚动标题, 每个标题将平分宽度
//    style.scrollTitle = YES;
//    // 渐变
//    style.gradualChangeTitleColor = YES;
//    style.coverCornerRadius = 0;
//    // 遮盖背景颜色
//    style.coverBackgroundColor = [UIColor clearColor];
//    //标题一般状态颜色 --- 注意一定要使用RGB空间的颜色值
//    style.normalTitleColor = [UIColor colorWithRed:30/255.0 green:130/255.0 blue:210/255.0 alpha:1.0];
//    
//    //style.titleMargin = 50;
//    
//    //标题选中状态颜色 --- 注意一定要使用RGB空间的颜色值
//    style.selectedTitleColor = RGB0X(0Xffffff);
//    style.scrollLineColor = RGB0X(0Xffffff);
//    style.normalTitleColor = RGB0X(0Xc0e7ff);
//    style.showCover = YES;
//    style.showLine = YES;
//    
    
//    self.navigationController.navigationBar.barTintColor = RGB(16, 134, 255);
//    // 注意: 一定要避免循环引用!!
//    __weak typeof(self) weakSelf = self;
//    ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, 64.0, 250, 28.0) segmentStyle:style delegate:self titles:self.titles titleDidClick:^(ZJTitleView *titleView, NSInteger index) {
//        
//        [weakSelf.contentView setContentOffSet:CGPointMake(weakSelf.contentView.bounds.size.width * index, 0.0) animated:YES];
//        
//    }];
//    
//    self.segmentView = segment;
//    self.navigationItem.titleView = self.segmentView;
//    
//    
//    
//    
//    
//    ZJContentView *content = [[ZJContentView alloc] initWithFrame:CGRectMake(0.0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 44.0) segmentView:self.segmentView parentViewController:self delegate:self];
////    NSLog(@"=========%.2f , %.2f" , self.view.bounds.size.width , self.view.bounds.size.height);
//    self.contentView = content;
//    [self.view addSubview:self.contentView];
    [[HRRequest manager]POST:PATH_positionNewsTypeList para:@{} success:^(id data) {
        NSArray *dataArr = data;
        if (dataArr.count > 0) {
            [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                ICN_posNavModel *model = [[ICN_posNavModel alloc]initWithDictionary:obj error:nil];
                NSLog(@"%@",model.title);
                [self.titles addObject:model.title];
                [self.dataSoreceArr addObject:model];
                // 初始化
                [self setupSegmentView];
            }];
            
        }
        
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
    }];

   
    
}
#pragma mark --- 数据初始化 ---
- (void)setupSegmentView
{
    
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    
    style.titleFont = [UIFont systemFontOfSize:14];
    style.scrollTitle = YES;
    /** 是否滚动标题 默认为YES 设置为NO的时候所有的标题将不会滚动, 并且宽度会平分 和系统的segment效果相似 */
    style.autoAdjustTitlesWidth = YES;
    
    style.segmentViewBounces = NO;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    
    style.scrollLineColor = RGB0X(0X009DFF);
    
    style.scrollLineHeight = 2;
    
    //标题一般状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.normalTitleColor = RGB0X(0X333333);
    //标题选中状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.selectedTitleColor = RGB0X(0X009DFF);
    //展示滚动条
    style.showLine = YES;
    
    style.segmentHeight = 44;
    
    
    __weak typeof(self) weakSelf = self;
    
    // 初始化
    CGRect scrollPageViewFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.0);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        
        ZJScrollPageView *scrollPageView = [[ZJScrollPageView alloc] initWithFrame:scrollPageViewFrame segmentStyle:style titles:_titles parentViewController:strongSelf delegate:strongSelf];
        strongSelf.scrollPageView = scrollPageView;
        // 额外的按钮响应的block
        
        //        [strongSelf.scrollPageView setSelectedIndex:1 animated:true];
        
        strongSelf.scrollPageView.extraBtnOnClick = ^(UIButton *extraBtn){
            
            NSLog(@"点击了extraBtn");
            
        };
        [strongSelf.view addSubview:strongSelf.scrollPageView];
        
    });
    
    
}

- (NSArray *)setupChildVc {
    
    ICN_PositionViewController *vc1 = [ICN_PositionViewController new];
    
    ICN_IndustryViewController *vc2 = [ICN_IndustryViewController new];
    
    ICN_ProjectViewController *vc3 = [ICN_ProjectViewController new];
    
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2,vc3,nil];
    return childVcs;
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}




- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    NSLog(@"%ld",index);
    self.typeStr = [NSString stringWithFormat:@"%ld",index];
    NSLog(@"%@",self.typeStr);
    if (index == 0) {
        childVc = self.PositionViewController;
    }else{
        ICN_IndustryViewController *MVC =  [ICN_IndustryViewController new];
        MVC.dataSource = self.dataSoreceArr;
        if (!childVc) {
            childVc = MVC;
            
        }
    
    }
    return childVc;
}
-(CGRect)frameOfChildControllerForContainer:(UIView *)containerView {
    return  CGRectInset(containerView.bounds, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onClick:(UIButton *)btn{

//    ICN_ProfessionSearchViewController *search = [[ICN_ProfessionSearchViewController alloc]init];
//    /*属性传值*/
//    search.typeStr =self.typeStr;
//    
////    [self presentViewController:search animated:YES completion:nil];
//    search.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:search animated:YES];
////    [self presentViewController:search animated:YES completion:nil];
    
    if (![self getCurrentUserLoginStatus]) {
        ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        [self currentPagerJumpToPager:vc];
        return;
    }
    
    // 先判断信息补全
    if ([self jumpToUserMessageSetUpPager]) {
        // 判断若需要跳转到补全页面则结束当前所有操作
        return ;
    }
    switch ([self.typeStr integerValue]) {
            //招聘
        case 0:
        {
            ICN_DynSearchPagerVC *MVC = [[ICN_DynSearchPagerVC alloc]init];
            MVC.indexTag = Search_WorkJob;
            MVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
            //行业资讯
        case 1:
        {
            ICN_DynSearchPagerVC *MVC = [[ICN_DynSearchPagerVC alloc]init];
            MVC.indexTag = Search_CareerPlan;
            MVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
            //职场规划
        case 2:
        {
            ICN_DynSearchPagerVC *MVC = [[ICN_DynSearchPagerVC alloc]init];
            MVC.indexTag = Search_InsdurtyInformation;
            MVC.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:MVC animated:YES];
        }
            break;
            
        default:
            break;
    }
   

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
