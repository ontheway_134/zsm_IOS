//
//  ICN_centerLeftTableViewCell.m
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_centerLeftTableViewCell.h"

@implementation ICN_centerLeftTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _centerLeftLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_centerLeftLabel];
        [_centerLeftLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(14);
            make.center.equalTo(self.contentView);
        }];
        _centerLeftLabel.font = [UIFont systemFontOfSize:14];
    }
    
    return self;
}
-(void)setModel:(ICN_centerLaftModel *)model{

    _centerLeftLabel.text = model.classname;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
