//
//  ICN_PositionNextTableViewCell.m
//  ICan
//
//  Created by 那风__ on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionNextTableViewCell.h"
#import "ICN_PositionNextModel.h"
@implementation ICN_PositionNextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIView *view = [[UIView alloc]init];
        [self.contentView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.mas_top).offset(0);
            make.left.mas_equalTo(self.contentView.mas_left).offset(0);
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.height.mas_equalTo(81);
        }];
        
        _logoimageView = [[UIImageView alloc]init];
        [view addSubview:_logoimageView];
        [_logoimageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(13);
            make.width.mas_equalTo(54);
            make.height.mas_equalTo(54);
        }];
        
        
        
        _rmbLabel = [[UILabel alloc]init];
        [view addSubview:_rmbLabel];
        [_rmbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(13);
            make.right.mas_equalTo(-10);
            make.height.mas_equalTo(15);
            
        }];
        _rmbLabel.textColor = RGB0X(0Xff561b);
        _rmbLabel.font = [UIFont systemFontOfSize:15];

        _titleLabel = [[UILabel alloc]init];
        [view addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(13);
            make.left.mas_equalTo(_logoimageView.mas_right).offset(10);
            make.width.mas_equalTo(view.mas_width).multipliedBy(0.7);
            make.height.mas_equalTo(15);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _compLabel = [[UILabel alloc]init];
        [view addSubview:_compLabel];
        [_compLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_logoimageView.mas_right).offset(10);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(12);
            
        }];
        _compLabel.textColor = RGB0X(0X333333);
        _compLabel.font = [UIFont systemFontOfSize:12];

        UIImageView *image1 = [[UIImageView alloc]init];
        image1.image = [UIImage imageNamed:@"地点.png"];
        [view addSubview:image1];
        [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_compLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(_logoimageView.mas_right).offset(10);
        }];
        
        _cityLabel = [[UILabel alloc]init];
        [view addSubview:_cityLabel];
        [_cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_compLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(image1.mas_right).offset(5);
            make.height.mas_equalTo(12);
        }];
        _cityLabel.textColor = RGB0X(0X666666);
        _cityLabel.font = [UIFont systemFontOfSize:12];
        
        UIImageView *image2 = [[UIImageView alloc]init];
        image2.image = [UIImage imageNamed:@"学历.png"];
        [view addSubview:image2];
        [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_compLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(_cityLabel.mas_right).offset(15);
        }];
        
        _qualiLabel = [[UILabel alloc]init];
        [view addSubview:_qualiLabel];
        [_qualiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_compLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(image2.mas_right).offset(5);
            make.height.mas_equalTo(12);
        }];
        
        _qualiLabel.textColor = RGB0X(0X666666);
        _qualiLabel.font = [UIFont systemFontOfSize:12];
        
        UIView *view2 = [[UIView alloc]init];
        [self.contentView addSubview:view2];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.top.mas_equalTo(self.contentView.mas_bottom).offset(-5);
            make.left.mas_equalTo(self.contentView.mas_left).offset(0);
            make.height.mas_equalTo(5);
        }];
        view2.backgroundColor = RGB(236, 236, 236);
        
        
       _img = [[UIImageView alloc]init];
        _img.image = [UIImage imageNamed:@"推荐.png"];
        [view addSubview:_img];
        [_img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-10);
            make.bottom.mas_equalTo(-13);
            
        }];
        
       
    }

    return self;
}
-(void)setModel:(ICN_PositionNextModel *)model{
    [_logoimageView sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    _titleLabel.text = model.positionTitle;
    
//    NSString *minSalaryStr =  SF(@"%ld",model.salary.integerValue / 1000);
//    NSString *str = [minSalaryStr stringByAppendingString:@"-"];
//    NSString *maxSalaryStr = SF(@"%ld", model.salary.integerValue / 1000);
//    NSString *str2 = [model.salary stringByAppendingString:@"k"];
   
    
    NSLog(@"%@",model.salary);
    _rmbLabel.text = [NSString stringWithFormat:@"%@",model.salary];
    _compLabel.text = model.companyName;
    _cityLabel.text = model.city;
    _qualiLabel.text = model.qualification;

    _ishot1 = model.ishot;
    if ([_ishot1 isEqualToString:@"0"]) {
        _img.hidden = YES;
    }
    else if(_ishot1 == nil){
        _img.hidden = YES;
    }
    else{
        
        _img.hidden = NO;
        
    }

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
