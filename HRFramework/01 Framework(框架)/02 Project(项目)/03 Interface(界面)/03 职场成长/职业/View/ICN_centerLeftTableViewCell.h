//
//  ICN_centerLeftTableViewCell.h
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_centerLaftModel.h"
@interface ICN_centerLeftTableViewCell : UITableViewCell
@property(nonatomic)UILabel *centerLeftLabel;
@property(nonatomic)ICN_centerLaftModel *model;
@end
