//
//  ICN_rightTableViewCell.m
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_rightTableViewCell.h"

@implementation ICN_rightTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _shiLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_shiLabel];
        [_shiLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(15);
            make.center.equalTo(self.contentView);
        }];
        _shiLabel.font = [UIFont systemFontOfSize:15];
    }

    return self;
}
-(void)setModel:(ICN_rightModel *)model{

    _shiLabel.text = model.cityname;
    
    // 如果Model被选中了则改变其颜色
    if (model.selected != nil && model.selected.integerValue == 1) {
        _shiLabel.textColor = [UIColor blueColor];
    }else{
        _shiLabel.textColor = [UIColor blackColor];
    }

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
