//
//  ICN_centerRightTableViewCell.h
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_centerRightModel.h"
@interface ICN_centerRightTableViewCell : UITableViewCell
@property(nonatomic)UILabel *centerRightLabel;
@property(nonatomic)ICN_centerRightModel *model;
@end
