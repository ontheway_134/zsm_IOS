//
//  ICN_PositionViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_PositionNextViewController.h"
#import "LKScrollView.h"
#import "ICN_ industryModel.h"
#import "ICN_PositionPicModel.h"
#import "ICN_leftModel.h"
#import "ICN_leftTableViewCell.h"
#import "ICN_rightModel.h"
#import "ICN_rightTableViewCell.h"
#import "ICN_centerLaftModel.h"
#import "ICN_centerRightModel.h"
#import "ICN_centerLeftTableViewCell.h"
#import "ICN_centerRightTableViewCell.h"
#import "HFButton.h"

#import "ICN_GroupDetialViewController.h"    //职场规划
#import "ICN_PositionNextOneViewController.h"    //职场规划详情
#import "ICN_ActivityDetialViewController.h"    //活动详情

#import "ICN_PositionNextModel.h"
#import "ICN_PositionNextTableViewCell.h"
#import "ICN_AdvertimistModel.h"
#import "ICN_PositionNextOneViewController.h"

//职位搜索
static NSString *const PATH_SupplementTermlist = @"MyPosition/Supplement/SupplementTermlist";
static NSString *const PATH_postionpiclunbo = @"MyPosition/PositionNewses/positionBannerList";
static NSString *const PATH_SupplementProvince = @"MyPosition/Supplement/SupplementProvince";
static NSString *const PATH_SupplementIndustryOne = @"MyPosition/Supplement/SupplementIndustryOne";
static NSString *const PATH_SupplementIndustrytwo = @"MyPosition/Supplement/SupplementIndustrytwo";
static NSString *const PATH_Supplementcitylist = @"MyPosition/Supplement/Supplementcitylist";
static NSString *const PATH_postionclassSect = @"MyPosition/PositionClass/postionclassSect";
static NSString *const PATH_SupplementCity = @"MyPosition/Supplement/SupplementCity";
//static NSString *const PATH_SupplementIndustrytwo = @"MyPosition/Supplement/SupplementIndustrytwo";
//static NSString *const PATH_postionclassSect = @"MyPosition/PositionClass/postionclassSect";
//static NSString *const PATH_Supplementcitylist = @"MyPosition/Supplement/Supplementcitylist";
@interface ICN_PositionViewController ()
<ZJScrollPageViewDelegate,UITableViewDelegate,UITableViewDataSource>

#pragma mark - ---------- 搜索范围属性 ----------
@property (nonatomic , assign)BOOL citySearch; // 城市筛选 默认是否
@property (nonatomic , assign)BOOL posicationSearch; // 职位筛选 默认是否
@property (nonatomic , assign)BOOL needySearch; // 要求筛选 默认是否


@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController *> *childVcs;
@property(nonatomic,strong)UIScrollView *ZXScrollView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)ZJScrollPageView *scrollPageView;
@property(nonatomic,strong)NSArray *arr;
@property(nonatomic)UIImageView *picImage;
@property(nonatomic,strong)LKScrollView *lksView;
@property(nonatomic,strong)NSMutableArray *topGDArr;

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,assign)NSInteger page;

@property(nonatomic)UIButton *btn1;
@property(nonatomic)UIButton *btn2;
@property(nonatomic)UIButton *btn3;


@property(nonatomic)UIView *currentWindow;
@property(nonatomic)UIView *currentView;
@property(nonatomic,strong)UITableView *leftTableView;
@property(nonatomic,strong)UITableView *rightTableView;
@property(nonatomic,strong)UITableView *centerLeftTableView;
@property(nonatomic,strong)UITableView *centerRightTableView;
@property(nonatomic,strong)UIScrollView *downScrollView;
@property(nonatomic,strong)UIView *contentView;
/**  */
@property(nonatomic,strong)NSMutableArray *leftDataArray;
@property(nonatomic,strong)NSMutableArray *rightDataArray;
@property(nonatomic,strong)NSMutableArray *centerLeftDataArray;
@property(nonatomic,strong)NSMutableArray *centerRightDataArray;
@property (nonatomic,strong) NSMutableArray * dataSourceArr; // 内联外联
@property(nonatomic)NSString *provincecode;
@property(nonatomic)NSString *bigclassoneid;
@property(nonatomic)NSMutableArray *codeArr;
@property(nonatomic)NSString *citycode;
@property(nonatomic)NSString *bigclasstwoid;

@property(nonatomic)NSMutableArray *marksArr;
@property(nonatomic)NSMutableArray *marksArr2;
@property(nonatomic)NSMutableArray *marksArr3;

@property(nonatomic)UIButton *button;
@property(nonatomic)UIButton *button1;
@property(nonatomic)UIButton *button2;
@property(nonatomic)NSString *str1;
@property(nonatomic)NSString *str2;
@property(nonatomic)NSString *str3;
@property (strong,nonatomic)ICN_leftTableViewCell *cell;
@end

@implementation ICN_PositionViewController
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (UIView *)currentWindow{
    if (!_currentWindow) {
        _currentWindow = [[UIView alloc]initWithFrame:CGRectMake(0, 41 +64 + 40+200/BASESCREENPX_HEIGHT, self.view.frame.size.width, self.view.frame.size.height - 211)];
    }
    return _currentWindow;
}
- (NSMutableArray *)leftDataArray{
    if (!_leftDataArray) {
        _leftDataArray = [NSMutableArray array];
    }
    return  _leftDataArray;
}
- (NSMutableArray *)rightDataArray{
    if (!_rightDataArray) {
        _rightDataArray = [NSMutableArray array];
    }
    return  _rightDataArray;
}
- (void)zj_viewWillDisappearForIndex:(NSInteger)index
{
    self.currentWindow.hidden = YES;
}
- (void)zj_viewDidDisappearForIndex:(NSInteger)index
{
    self.currentWindow.hidden = YES;
}
- (void)zj_viewDidLoadForIndex:(NSInteger)index
{
    
}
- (UIButton *)btn1{
    if (!_btn1) {
        _btn1 = [[UIButton alloc]init];
    }
    return  _btn1;
}
-(void)loadMoreData{
    
    _page ++;
    
    [self datas];
    
    
}
- (void)zj_viewWillAppearForIndex:(NSInteger)index
{
    [HTTPManager POST_PATH:PATH_postionpiclunbo params:nil success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        _topGDArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dict) {
            //            ICN_PositionPicModel *model = [[ICN_PositionPicModel alloc]init];
            //            [model setValuesForKeysWithDictionary:dic];
            //            [_topGDArr addObject:model.pic];
            ICN_AdvertimistModel *model = [[ICN_AdvertimistModel alloc]initWithDictionary:dic error:nil];
            [_topGDArr addObject:model.pic];
            [self.dataSourceArr addObject:model];
            
        }
        WEAK(weakSelf);
        [_lksView loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
        //=== 职场--招聘跳转对应页面的位置
        [_lksView addActionWithBlock:^(UIButton *button, NSInteger FirstButtonTag) {
            HRLog(@"%ld", (long)FirstButtonTag);
            ICN_AdvertimistModel *model = weakSelf.dataSourceArr[button.tag-20];
            BOOL istoken = YES;
            if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == nil) {
                [USERDEFAULT setValue:SF(@"%ld",(long)HR_ICNVisitor) forKey:HR_UserTypeKEY];
                istoken = NO;
            }
            
            if ([model.adType integerValue] == 1) {
                // 跳转到内部链接
                // 1. 区分对应的跳转操作
                switch ([model.adClass integerValue]) {
                    case 1:{
                        // 跳转到首页列表
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 2:{
                        //跳转到职场
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 2;
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 3:{
                        // 跳转到职场详情
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        barController.selectedIndex = 2;
                        ICN_PositionNextOneViewController *position = [[ICN_PositionNextOneViewController alloc]init];
                        position.url = model.pageId;
                        [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                        [self presentViewController:barController animated:YES completion:^{
                            [APPLICATION keyWindow].rootViewController = barController;
                            
                        }];
                        
                        
                        break ;
                    }
                    case 4:{
                        // 跳转到活动列表
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                            //延时后想要执行的代码
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 3;
                            [self presentViewController:barController animated:YES completion:nil];
                        });
                        
                        break ;
                    }
                    case 5:{
                        // 跳转到活动详情
                        if (istoken) {
                            // 用户登录
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            barController.selectedIndex = 3;
                            ICN_ActivityDetialViewController *position = [[ICN_ActivityDetialViewController alloc]init];
                            position.packID = model.pageId;
                            [((UINavigationController *)barController.selectedViewController) pushViewController:position animated:NO];
                            
                            [self presentViewController:barController animated:YES completion:^{
                                [APPLICATION keyWindow].rootViewController = barController;
                            }];
                        }else{
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f*NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                                //延时后想要执行的代码
                                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                barController.selectedIndex = 3;
                                [self presentViewController:barController animated:YES completion:nil];
                            });
                        }
                        
                        break ;
                    }
                    default:
                        break;
                }
            }else{
                // 跳转到广告
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.adUrl]];
            }
            
        }];
        [_tableView reloadData];
        [self endRefresh];
        
    } failure:^(NSError *error) {
        [self endRefresh];
    }];
    
    [self datas];

}
-(void)refreshingData{
    
    _page = 1;
    
    [self datas];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.currentWindow setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}
- (void)endRefresh{
    [self.tableView.mj_header endRefreshing];
     [self.tableView.mj_footer endRefreshing];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _page = 1;
    self.citySearch = NO;
    _dataArr = [[NSMutableArray alloc]init];
    
    
    
    self.currentWindow.backgroundColor = [UIColor blackColor];
    self.currentWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
    self.currentWindow .hidden = YES;
    [[UIApplication sharedApplication].keyWindow addSubview:self.currentWindow];
    
    _currentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    [self.currentWindow addSubview:_currentView];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        [self refreshingData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [self loadMoreData];
    }];
    self.tableView.mj_footer = footer;
    [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
    [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
    self.tableView.mj_footer = footer;
    
    // 设置刚进来的时候mj_footer默认隐藏
    self.tableView.mj_footer.hidden = YES;

    self.tableView.mj_header = header;
    [_tableView registerClass:[ICN_PositionNextTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    UIView *heaView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 41+200/BASESCREENPX_HEIGHT)];
    _tableView.tableHeaderView = heaView;
    
    self.view.backgroundColor = [UIColor whiteColor];
    _lksView = [[LKScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200/BASESCREENPX_HEIGHT)];
    
    [heaView addSubview:_lksView];
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Position/postionpiclunbo" params:nil success:^(id result) {
//        
//        NSDictionary *dict =result[@"result"];
//        _topGDArr = [[NSMutableArray alloc]init];
//        
//        for (NSDictionary *dic in dict) {
//            ICN_PositionPicModel *model = [[ICN_PositionPicModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_topGDArr addObject:model.pic];
//            
//        }
//        
//        [_lksView loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
//        //=== 职场--招聘跳转对应页面的位置
//        [_lksView addActionWithBlock:^(UIButton *button, NSInteger FirstButtonTag) {
//            HRLog(@"%ld , %ld",(long)button.tag , (long)FirstButtonTag);
//        }];
//        [_tableView reloadData];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    
    self.btn1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 200/BASESCREENPX_HEIGHT, self.view.frame.size.width / 3, 40)];
    
    [self.btn1 setTitle:@"城市" forState:UIControlStateNormal];
    [self.btn1 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    self.btn1.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.btn1 setImage:[UIImage imageNamed:@"内容下拉.png"] forState:UIControlStateNormal];
    [self.btn1 addTarget:self action:@selector(btn1Action:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btn1.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
    // 交换button中title和image的位置
    CGFloat labelWidth = self.btn1.titleLabel.frame.size.width;
    CGFloat imageWith = self.btn1.imageView.frame.size.width;
    self.btn1.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth, 0, -labelWidth-60);
    self.btn1.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith, 0, imageWith);
    
    
    [heaView addSubview:_btn1];
    
    _btn2 = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3, 200/BASESCREENPX_HEIGHT, self.view.frame.size.width / 3, 40)];
    [_btn2 setTitle:@"职位" forState:UIControlStateNormal];
    [_btn2 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [_btn2 addTarget:self action:@selector(btn2Action:) forControlEvents:UIControlEventTouchUpInside];
    _btn2.titleLabel.font = [UIFont systemFontOfSize:14];
    [_btn2 setImage:[UIImage imageNamed:@"内容下拉.png"] forState:UIControlStateNormal];
    
    CGFloat labelWidth1 = self.btn2.titleLabel.frame.size.width;
    CGFloat imageWith1 = self.btn2.imageView.frame.size.width;
    self.btn2.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth1, 0, -labelWidth1-60);
    self.btn2.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith1, 0, imageWith1);
    [heaView addSubview:_btn2];
    
    _btn3 = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3 *2, 200/BASESCREENPX_HEIGHT, self.view.frame.size.width / 3, 40)];
    [_btn3 addTarget:self action:@selector(btn3Action:) forControlEvents:UIControlEventTouchUpInside];
    [_btn3 setTitle:@"要求" forState:UIControlStateNormal];
    [_btn3 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    _btn3.titleLabel.font = [UIFont systemFontOfSize:14];
    [_btn3 setImage:[UIImage imageNamed:@"内容下拉.png"] forState:UIControlStateNormal];
    _btn3.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
    CGFloat labelWidth2 = self.btn3.titleLabel.frame.size.width;
    CGFloat imageWith2 = self.btn3.imageView.frame.size.width;
    self.btn3.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth2, 0, -labelWidth2-60);
    self.btn3.titleEdgeInsets = UIEdgeInsetsMake(0, -imageWith2, 0, imageWith2);
    [heaView addSubview:_btn3];
    
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 40+200/BASESCREENPX_HEIGHT, self.view.frame.size.width, 1)];
    label.backgroundColor = RGB(236, 236, 236);
    [heaView addSubview:label];
    
    //[self datas];
    
    
    
}

// 配置两个tableView并将其添加上的方法
-(void)configWithDoubleTableView{
    
    if (_currentView == nil) {
        _currentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
        [self.currentWindow addSubview:_currentView];
    }
    
    if (_rightTableView == nil) {
        _rightTableView = [[UITableView alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2, 0, self.view.frame.size.width / 2, 300) style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _rightTableView.backgroundColor = RGB(208, 208, 208);
        [_rightTableView registerClass:[ICN_rightTableViewCell class] forCellReuseIdentifier:@"rightTableViewCell"];
    }
    
    if (_leftTableView == nil) {
        _leftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2, 300) style:UITableViewStylePlain];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        [_leftTableView registerClass:[ICN_leftTableViewCell class] forCellReuseIdentifier:@"leftTableViewCell"];
        _leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    [_currentView addSubview:_leftTableView];
    [_currentView addSubview:_rightTableView];
}

// 移除两个tableView的方法
- (void)removeDoubleTableViewFromSuperView{
    if (_leftTableView) {
        [_leftTableView removeFromSuperview];
        [_rightTableView removeFromSuperview];
        _leftTableView.delegate = nil;
        _leftTableView.dataSource = nil;
        _rightTableView.delegate = nil;
        _rightTableView.dataSource = nil;
        _leftTableView = nil;
        _rightTableView = nil;
    }
}

// 移除中间两个tableView的方法
- (void)removeCentersTableViewFromSuperView{
    if (_centerRightTableView) {
        [_centerLeftTableView removeFromSuperview];
        [_centerRightTableView removeFromSuperview];
        _centerLeftTableView.delegate = nil;
        _centerLeftTableView.dataSource = nil;
        _centerRightTableView.delegate = nil;
        _centerRightTableView.dataSource = nil;
        _centerLeftTableView = nil;
        _centerRightTableView = nil;
    }
}


-(void)btn1Action:(UIButton *)btn{
    
    // 点击button 1 的时候将其他两个button对应的内容清空
    [self removeCentersTableViewFromSuperView];
    if (_currentView) {
        [_currentView removeFromSuperview];
        _currentView = nil;
    }
    if (_downScrollView) {
        [_downScrollView removeFromSuperview];
        _downScrollView = nil;
    }
    
    btn.selected = !btn.selected;
    
    if (btn.selected) {
        
        _btn2.selected = NO;
        _btn3.selected = NO;
        
        [self configWithDoubleTableView];
        self.currentWindow.hidden = NO;
        [self.leftDataArray removeAllObjects];
        // 当城市选择器的做按钮为nil获取数据位空的时候才开始刷新数据
        if (self.leftDataArray == nil || self.leftDataArray.count == 0) {
            //=== 获取所选城市中省一级的接口
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementProvince" params:nil success:^(id result) {
//                self.leftDataArray = [[NSMutableArray alloc]init];
//                NSArray *dict = result[@"result"];
//                for (NSDictionary *dic in dict) {
//                    ICN_leftModel *model = [[ICN_leftModel alloc]init];
//                    [model setValuesForKeysWithDictionary:dic];
//                    [_leftDataArray addObject:model];
//                }
//                
//                // 在初始情况下市一级的Cell默认显示的是默认Cell
//                self.rightDataArray = [NSMutableArray array];
//                // 获取到全部的所选城市之后再最上方添加默认的选项
//                ICN_leftModel *firstLTModel = [[ICN_leftModel alloc] init];
//                firstLTModel.provincename = @"默认";
//                
//                ICN_rightModel *firsrRTModel = [[ICN_rightModel alloc] init];
//                firsrRTModel.cityname = @"默认";
//                firstLTModel.selected = @"1";
//                firsrRTModel.selected = @"1";
//                [self.leftDataArray insertObject:firstLTModel atIndex:0];
//                [self.rightDataArray addObject:firsrRTModel];
//                [_leftTableView reloadData];
//                [_rightTableView reloadData];
//            } failure:^(NSDictionary *errorInfo) {
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
//            }];
            
            [HTTPManager POST_PATH:PATH_SupplementProvince params:nil success:^(id responseObject) {
                self.leftDataArray = [[NSMutableArray alloc]init];
                NSArray *dict = responseObject;
                for (NSDictionary *dic in dict) {
                    ICN_leftModel *model = [[ICN_leftModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_leftDataArray addObject:model];
                }
                
                // 在初始情况下市一级的Cell默认显示的是默认Cell
                self.rightDataArray = [NSMutableArray array];
                // 获取到全部的所选城市之后再最上方添加默认的选项
                ICN_leftModel *firstLTModel = [[ICN_leftModel alloc] init];
                firstLTModel.provincename = @"默认";
                
                ICN_rightModel *firsrRTModel = [[ICN_rightModel alloc] init];
                firsrRTModel.cityname = @"默认";
                firstLTModel.selected = @"1";
                firsrRTModel.selected = @"1";
                [self.leftDataArray insertObject:firstLTModel atIndex:0];
                [self.rightDataArray addObject:firsrRTModel];
                [_leftTableView reloadData];
                [_rightTableView reloadData];

            } failure:^(NSError *error) {
                  [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
            }];
        }
        
    }else{
        // 第二下点击移除当前页面的内容
        self.currentWindow .hidden = YES;
        [self removeDoubleTableViewFromSuperView];
        _btn2.selected = NO;
        _btn3.selected = NO;
    }
}


-(void)btn2Action:(UIButton *)btn{
    // 点击button 2 的时候将其他两个button对应的内容清空
    [self removeDoubleTableViewFromSuperView];
    if (_currentView) {
        [_currentView removeFromSuperview];
        _currentView = nil;
    }
    if (_downScrollView) {
        [_downScrollView removeFromSuperview];
        _downScrollView = nil;
    }
    

    btn.selected = !btn.selected;
    if (btn.selected) {
        
        self.currentWindow.hidden = NO;
        self.btn1.selected = NO;
        _btn3.selected = NO;
        
        if (_currentView == nil) {
            _currentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
            [self.currentWindow addSubview:_currentView];
        }
        _centerLeftTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2,  300) style:UITableViewStylePlain];
        _centerLeftTableView.delegate = self;
        _centerLeftTableView.dataSource = self;
        [_centerLeftTableView registerClass:[ICN_centerLeftTableViewCell class] forCellReuseIdentifier:@"centerLeftTableViewCell"];
        _centerLeftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_currentView addSubview:_centerLeftTableView];
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementIndustryOne" params:nil success:^(id result) {
//            _centerLeftDataArray = [[NSMutableArray alloc]init];
//            NSArray *dict = result[@"result"];
//            for (NSDictionary *dic in dict) {
//                ICN_centerLaftModel *model = [[ICN_centerLaftModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [_centerLeftDataArray addObject:model];
//                //                _provincecode = model.provincecode;
//            }
//            [_centerLeftTableView reloadData];
//            
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        [HTTPManager POST_PATH:PATH_SupplementIndustryOne params:nil success:^(id responseObject) {
            _centerLeftDataArray = [[NSMutableArray alloc]init];
            NSArray *dict = responseObject;
            for (NSDictionary *dic in dict) {
                ICN_centerLaftModel *model = [[ICN_centerLaftModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [_centerLeftDataArray addObject:model];
                //                _provincecode = model.provincecode;
            }
            [_centerLeftTableView reloadData];
        } failure:^(NSError *error) {
            
        }];
        _centerRightTableView = [[UITableView alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2, 0, self.view.frame.size.width / 2, 300) style:UITableViewStylePlain];
        _centerRightTableView.delegate = self;
        _centerRightTableView.dataSource = self;
        _centerRightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _centerRightTableView.backgroundColor = RGB(208, 208, 208);
        [_centerRightTableView registerClass:[ICN_centerRightTableViewCell class] forCellReuseIdentifier:@"centerRightTableViewCell"];
        //_rightDataArray = @[@"ddd",@"ddd",@"ddd"];
        [_currentView addSubview:_centerRightTableView];
        _centerRightDataArray = [[NSMutableArray alloc]init];
        
        
        if (!(_centerLeftDataArray.count == 0)) {
            ICN_centerLaftModel *model= _centerLeftDataArray[0];
            _bigclassoneid = model.bigclassoneid;
        }
        
        
        NSDictionary *dic = @{@"bigclassoneid":[NSString stringWithFormat:@"%@",_bigclassoneid]};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementIndustrytwo" params:dic success:^(id result) {
//            //_rightDataArray = [[NSMutableArray alloc]init];
//            NSArray *dict = result[@"result"];
//            for (NSDictionary *dic in dict) {
//                ICN_centerRightModel *model = [[ICN_centerRightModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [_centerRightDataArray addObject:model];
//                
//            }
//            [_centerRightTableView reloadData];
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        [HTTPManager POST_PATH:PATH_SupplementIndustrytwo params:dic success:^(id responseObject) {
            NSArray *dict = responseObject;
            for (NSDictionary *dic in dict) {
                ICN_centerRightModel *model = [[ICN_centerRightModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [_centerRightDataArray addObject:model];
                
            }
            [_centerRightTableView reloadData];
        } failure:^(NSError *error) {
            
        }];
    }else{
        
        _currentWindow .hidden = YES;
        [self removeCentersTableViewFromSuperView];
        self.btn1.selected = NO;
        _btn3.hidden = NO;
    }
}
-(void)btn3Action:(UIButton *)btn{
    
    [self removeDoubleTableViewFromSuperView];
    [self removeCentersTableViewFromSuperView];
    btn.selected = !btn.selected;
    if (btn.selected) {
        
        self.currentWindow.hidden = NO;
        self.btn1.selected = NO;
        _btn2.selected = NO;
        //        self.currentWindow.backgroundColor = [UIColor blackColor];
        //        self.currentWindow.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        //
        //        [[UIApplication sharedApplication].keyWindow addSubview:self.currentWindow];
        //
        //        _currentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
        //        [self.currentWindow addSubview:_currentView];
        
        _downScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,  300)];
        _downScrollView.backgroundColor = [UIColor whiteColor];
        [_currentWindow addSubview:_downScrollView];
        
        _contentView = [[UIView alloc]init];
        _contentView.backgroundColor = [UIColor whiteColor];
        [_downScrollView addSubview:_contentView];
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.downScrollView);
            make.width.equalTo(self.downScrollView);
        }];
        
        UILabel *gongzuoLabel = [[UILabel alloc]init];
        gongzuoLabel.text = @"工作年限";
        gongzuoLabel.textColor = RGB0X(0x333333);
        gongzuoLabel.font = [UIFont systemFontOfSize:13];
        [_contentView addSubview:gongzuoLabel];
        [gongzuoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(15);
            make.height.mas_equalTo(14);
        }];
        
        self.marksArr = [NSMutableArray arrayWithObjects:@"默认",@"在读",@"应届生",@"1-3年",@"3-5年" ,@"5-10年",@"十年以上", nil];
        
        //        for (int i = 0; i<self.marksArr.count; i++) {
        
        CGFloat w = 0;//保存前一个button的宽以及前一个button距离屏幕边缘的距离
        CGFloat h = 40;//用来控制button距离父视图的高
        for (int i = 0; i < self.marksArr.count; i++) {
            UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.tag = 1001 + i;
            [button setTitleColor:RGB(16, 134, 255) forState:UIControlStateSelected];
            [button setTintColor:[UIColor clearColor]];
            [button setTitleColor:RGB0X(0x7e94a2) forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont systemFontOfSize:12];
            [button addTarget:self action:@selector(seleMarkClick:) forControlEvents:UIControlEventTouchUpInside];
            
            [button.layer setMasksToBounds:YES];//设置按钮的圆角半径不会被遮挡
            
            [button.layer setCornerRadius:4];
            button.layer.borderColor = RGB0X(0x7e94a2).CGColor;
            [button.layer setBorderWidth:1];
            
            
            //根据计算文字的大小
            
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
            CGFloat length = [self.marksArr[i] boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-40, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
            //为button赋值
            [button setTitle:self.marksArr[i] forState:UIControlStateNormal];
            button.titleLabel.adjustsFontSizeToFitWidth = YES;
            button.titleLabel.minimumScaleFactor = 0.5;
            //设置button的frame
            button.frame = CGRectMake(10 + w, h, (SCREEN_WIDTH-68)/4 , 30);
            if (kIs_phone5) {
                button.frame = CGRectMake(10 + w, h, (SCREEN_WIDTH+100)/7 , 30);
            }
            //当button的位置超出屏幕边缘时换行 320 只是button所在父视图的宽度
            if(10 + w + length + 20 > SCREEN_WIDTH-40){
                w = 0; //换行时将w置为0
                h = h + button.frame.size.height + 10;//距离父视图也变化
                button.frame = CGRectMake(10 + w, h, (SCREEN_WIDTH-68)/4 , 30);//重设button的frame
                
            }
            if (kIs_phone5) {
                if(10 + w + length + 20 > SCREEN_WIDTH-40){
                    w = 0; //换行时将w置为0
                    h = h + button.frame.size.height + 10;//距离父视图也变化
                    button.frame = CGRectMake(10 + w, h, (SCREEN_WIDTH-50)/4 , 30);//重设button的frame
                    
                }
            }
            w = button.frame.size.width + button.frame.origin.x;
            [_contentView addSubview:button];
        }
        //        }
        
        UILabel *xinziLabel = [[UILabel alloc]init];
        xinziLabel.text = @"薪资范围";
        xinziLabel.textColor = RGB0X(0x333333);
        xinziLabel.font = [UIFont systemFontOfSize:13];
        [_contentView addSubview:xinziLabel];
        
        [xinziLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(135);
            make.height.mas_equalTo(14);
        }];
        
        
        self.marksArr2 = [NSMutableArray arrayWithObjects:@"默认",@"3k-5k",@"5k-7k",@"7k-10k",@"10k-20k",@"20k-50k",@"50k以上",nil];
        
        //        for (int i = 0; i<self.marksArr2.count; i++) {
        
        CGFloat w1 = 0;//保存前一个button的宽以及前一个button距离屏幕边缘的距离
        CGFloat h1 = 160;//用来控制button距离父视图的高
        for (int i = 0; i < self.marksArr2.count; i++) {
            UIButton * button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button1.tag = 2000 + i;
            
            
            [button1 addTarget:self action:@selector(seleMarkClicktwo:) forControlEvents:UIControlEventTouchUpInside];
            [button1 setTintColor:[UIColor clearColor]];
            [button1 setTitleColor:RGB(16, 134, 255) forState:UIControlStateSelected];
            [button1.layer setMasksToBounds:YES];//设置按钮的圆角半径不会被遮挡
            button1.titleLabel.font = [UIFont systemFontOfSize:12];
            [button1.layer setCornerRadius:4];
            button1.layer.borderColor = RGB0X(0x7e94a2).CGColor;
            [button1.layer setBorderWidth:1];
            [button1 setTitleColor:RGB0X(0x7e94a2) forState:UIControlStateNormal];
            
            //根据计算文字的大小
            
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
            CGFloat length = [self.marksArr2[i] boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-40, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
            //为button赋值
            [button1 setTitle:self.marksArr2[i] forState:UIControlStateNormal];
            //设置button的frame
            button1.frame = CGRectMake(10 + w1, h1, (SCREEN_WIDTH-68)/4 , 30);
            if (kIs_phone5) {
                button1.frame = CGRectMake(10 + w1, h1, (SCREEN_WIDTH+100)/7 , 30);
            }
            //当button的位置超出屏幕边缘时换行 320 只是button所在父视图的宽度
            if(10 + w1 + length + 20 > SCREEN_WIDTH-40){
                w1 = 0; //换行时将w置为0
                h1 = h1 + button1.frame.size.height + 10;//距离父视图也变化
                button1.frame = CGRectMake(10 + w1, h1, (SCREEN_WIDTH-68)/4 , 30);//重设button的frame
            }
            w1 = button1.frame.size.width + button1.frame.origin.x;
            [_contentView addSubview:button1];
        }
        //        }
        UILabel *xueliLabel = [[UILabel alloc]init];
        xueliLabel.text = @"学历要求";
        xueliLabel.textColor = RGB0X(0x333333);
        xueliLabel.font = [UIFont systemFontOfSize:13];
        
        
        
        [_contentView addSubview:xueliLabel];
        
        
        if (kIs_phone6plus) {
            [xueliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.top.mas_equalTo(245);
                make.height.mas_equalTo(14);
            }];
        }
        else if (kIs_phone6){
            [xueliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.top.mas_equalTo(245);
                make.height.mas_equalTo(14);
            }];
        }
        else if (kIs_phone5){
            [xueliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.top.mas_equalTo(245);
                make.height.mas_equalTo(14);
            }];
        }
        
        
        self.marksArr3 = [NSMutableArray arrayWithObjects:@"默认",@"初中",@"职高", @"高中",@"中专",@"专科",@"本科" ,@"硕士",@"博士",nil];
        
        //for (int i = 0; i<self.marksArr3.count; i++) {
        
        CGFloat w2 = 0;//保存前一个button的宽以及前一个button距离屏幕边缘的距离
        CGFloat h2 = 265;//用来控制button距离父视图的高
        for (int i = 0; i < self.marksArr3.count; i++) {
            _button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            _button2.tag = 3000 + i;
            
            [_button2 setTitleColor:RGB(16, 134, 255) forState:UIControlStateSelected];
            [_button2 addTarget:self action:@selector(seleMarkClickthree:) forControlEvents:UIControlEventTouchUpInside];
            
            [_button2.layer setMasksToBounds:YES];//设置按钮的圆角半径不会被遮挡
            [_button2 setTintColor:[UIColor clearColor]];
            [_button2.layer setCornerRadius:4];
            _button2.layer.borderColor = RGB0X(0x7e94a2).CGColor;
            [_button2.layer setBorderWidth:1];
            [_button2 setTitleColor:RGB0X(0x7e94a2) forState:UIControlStateNormal];
            _button2.titleLabel.font = [UIFont systemFontOfSize:12];
            //根据计算文字的大小
            
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
            CGFloat length = [self.marksArr3[i] boundingRectWithSize:CGSizeMake(SCREEN_WIDTH-40, 2000) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
            //为button赋值
            [_button2 setTitle:self.marksArr3[i] forState:UIControlStateNormal];
            //设置button的frame
            _button2.frame = CGRectMake(10 + w2, h2, (SCREEN_WIDTH-68)/4 , 30);
            //当button的位置超出屏幕边缘时换行 320 只是button所在父视图的宽度
            if(10 + w2 + length + 20 > SCREEN_WIDTH-40){
                w2 = 0; //换行时将w置为0
                h2 = h2 + _button2.frame.size.height + 10;//距离父视图也变化
                _button2.frame = CGRectMake(10 + w2, h2, (SCREEN_WIDTH-68)/4 , 30);//重设button的frame
            }
            w2 = _button2.frame.size.width + _button2.frame.origin.x;
            [_contentView addSubview:_button2];
        }
        UIButton *querenBtn = [[UIButton alloc]init];
        [querenBtn setTitle:@"确认" forState:UIControlStateNormal];
        [querenBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        querenBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        querenBtn.backgroundColor = RGB(16, 134, 255);
        
        querenBtn.clipsToBounds=YES;
        
        querenBtn.layer.cornerRadius=4;
        [querenBtn addTarget:self action:@selector(querenBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [_contentView addSubview:querenBtn];
        
        if (kIs_phone5) {
            [querenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.top.mas_equalTo(400);
                make.height.mas_equalTo(50);
                make.bottom.mas_equalTo(-15);
            }];
        }else if (kIs_phone6){
            [querenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.top.mas_equalTo(400);
                make.height.mas_equalTo(50);
                make.bottom.mas_equalTo(-15);
            }];
        }else if (kIs_phone6plus){
            [querenBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.top.mas_equalTo(400);
                make.height.mas_equalTo(50);
                make.bottom.mas_equalTo(-15);
            }];
        }else{
            [xueliLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(10);
                make.right.mas_equalTo(-10);
                make.top.mas_equalTo(400);
                make.height.mas_equalTo(50);
                make.bottom.mas_equalTo(-15);
            }];
        }
    }else{
        // 在点击第三个栏目的时候将内容清空
        if (_currentView) {
            [_currentView removeFromSuperview];
            _currentView = nil;
        }
        self.currentWindow.hidden = YES;
        self.btn1.selected = NO;
        _btn2.selected = NO;
    }
}
-(void)seleMarkClick:(UIButton*)sender {
    
    for (int i = 0; i<7 ; i++) {
        if (sender.tag == 1000 + i + 1) {
            sender.selected = YES;
            _str1 = [NSString stringWithFormat:@"%d",i];
            NSLog(@"%@",_str1);
            continue;
        }
        UIButton *btn = (UIButton *)[self.contentView viewWithTag:1001+i];
        btn.selected = NO;
    }
}
-(void)seleMarkClicktwo:(UIButton*)sender{
    for (int i = 0; i<7 ; i++) {
        
        if (sender.tag == 2000 + i) {
            sender.selected = YES;
            _str2 = [NSString stringWithFormat:@"%d",i];
            continue;
        }
        UIButton *btn = (UIButton *)[self.contentView viewWithTag:2000+i];
        btn.selected = NO;
    }
}

-(void)seleMarkClickthree:(UIButton*)sender{
    for (int i = 0; i <9; i++) {
        if (sender.tag == 3000 + i ) {
            
            sender.selected = YES;
            _str3 = [NSString stringWithFormat:@"%d",i];
            continue;
        }
        UIButton *btn = (UIButton *)[self.contentView viewWithTag:3000+i];
        btn.selected = NO;
    }
}

/**
 确认按钮

 @param btn 确认按钮
 */
-(void)querenBtnAction:(UIButton *)btn{
    //
    NSLog(@"%@",_str1);
    if (!(_str1 == nil)&&!(_str2 == nil)&&!(_str3== nil)) {
        // 如果三个默认添加的信息完全则清空这个视图以及另外两个可能会有的视图的全部内容
        [self removeDoubleTableViewFromSuperView];
        [self removeCentersTableViewFromSuperView];
        if (_currentView) {
            [_currentView removeFromSuperview];
            _currentView = nil;
        }
        if (_downScrollView) {
            [_downScrollView removeFromSuperview];
            _downScrollView = nil;
        }
        NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",_page],@"qualification":_str1,@"work_experience":_str2,@"sellect_salary":_str3};
        
        [HTTPManager POST_PATH:PATH_SupplementTermlist params:dic success:^(id responseObject) {
            NSArray *dict = responseObject;
            if (dict == nil) {
                _dataArr = nil;
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据为空"];
                [self.tableView reloadData];
                self.currentWindow.hidden = YES;
                //                self.btn1.selected = NO;
                //                _btn2.selected = NO;
                //                _btn3.selected = NO;
                
            }else{
                if (_page == 1) {
                    [_dataArr removeAllObjects];
                }
                for (NSDictionary *dic in dict ) {
                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_dataArr addObject:model];
                }
                self.currentWindow.hidden = YES;
                
                [self.tableView reloadData];
            }

        } failure:^(NSError *error) {
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据为空"];
            _dataArr = [NSMutableArray array];
             self.currentWindow.hidden = YES;
             [self.tableView reloadData];
        }];
        
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementTermlist" params:dic success:^(id result) {
//            
//            NSDictionary *dict =result[@"result"];
//            if (dict == nil) {
//                _dataArr = nil;
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据为空"];
//                [self.tableView reloadData];
//                self.currentWindow.hidden = YES;
//                //                self.btn1.selected = NO;
//                //                _btn2.selected = NO;
//                //                _btn3.selected = NO;
//                
//            }else{
//                if (_page == 1) {
//                    [_dataArr removeAllObjects];
//                }
//                for (NSDictionary *dic in dict ) {
//                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                    [model setValuesForKeysWithDictionary:dic];
//                    [_dataArr addObject:model];
//                }
//                self.currentView.hidden = YES;
//                
//                [self.tableView reloadData];
//            }
//        } failure:^(NSDictionary *errorInfo) {
//        }];
       
    }
    else{
        
        [MBProgressHUD ShowProgressWithBaseView:self.contentView Message:@"请补全信息"];
    }
    
    
    
}
-(void)datas{
    
    //=== 如果现在是城市筛选则搜索城市数据
    if (self.citySearch) {
        
        NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",(long)_page],@"citycode":_citycode};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/Supplementcitylist" params:dic success:^(id result) {
//            [self.tableView.mj_header endRefreshing];
//            [self.tableView.mj_footer endRefreshing];
//            NSArray *dic1 =result[@"result"];
//            if (dic1 == nil) {
//                
//            }else{
//                
//                // 根据page判断是清空之前数据还是加载新的数据
//                if (self.page == 1) {
//                    _dataArr = [NSMutableArray array];
//                }
//                
//                self.currentWindow.hidden = YES;
//                for (NSDictionary *dic in dic1 ) {
//                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                    [model setValuesForKeysWithDictionary:dic];
//                    [_dataArr addObject:model];
//                    
//                }
//                [self.tableView reloadData];
//            }
//        } failure:^(NSDictionary *errorInfo) {
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
//            [self.tableView.mj_header endRefreshing];
//            [self.tableView.mj_footer endRefreshing];
//        }];
        [HTTPManager POST_PATH:PATH_Supplementcitylist params:dic success:^(id responseObject) {
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
            NSArray *dic1 = responseObject;
            if (dic1 == nil) {
                
            }else{
                
                // 根据page判断是清空之前数据还是加载新的数据
                if (self.page == 1) {
                    _dataArr = [NSMutableArray array];
                }
                
                self.currentWindow.hidden = YES;
                for (NSDictionary *dic in dic1 ) {
                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_dataArr addObject:model];
                    
                }
                [self.tableView reloadData];
                [self.tableView.mj_header endRefreshing];
                [self.tableView.mj_footer endRefreshing];
            }
        } failure:^(NSError *error) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }];
        
        return ;
        
    }
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSLog(@"%@",token);
    
    NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",(long)_page]};
    
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionClass/postionclassSect" params:dic success:^(id result) {
//        NSArray *dic1 =result[@"result"];
//        
//        if (_page == 1) {
//            [_dataArr removeAllObjects];
//            _dataArr = [NSMutableArray array];
//        }
//        for (NSDictionary *dic in dic1 ) {
//            ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_dataArr addObject:model];
//        }
//        
//        if (_page == 1) {
//            [_tableView.mj_header endRefreshing];
//        }
//        [_tableView.mj_footer endRefreshing];
//        
//        [self.tableView reloadData];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    
    [HTTPManager POST_PATH:PATH_postionclassSect params:dic success:^(id responseObject) {
        NSArray *dic1 = responseObject;
        
        if (_page == 1) {
            [_dataArr removeAllObjects];
            _dataArr = [NSMutableArray array];
        }
        for (NSDictionary *dic in dic1 ) {
            ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        if (_page == 1) {
            [_tableView.mj_header endRefreshing];
        }
        [_tableView.mj_footer endRefreshing];
        
        [self.tableView reloadData];

    } failure:^(NSError *error) {
        
    }];
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _leftTableView) {
        
        return self.leftDataArray.count;
        
    }else if (tableView == _rightTableView){
        
        return self.rightDataArray.count;
        
    }else if (tableView == _centerLeftTableView){
        
        return _centerLeftDataArray.count;
        
    }else if (tableView == _centerRightTableView){
        
        return _centerRightDataArray.count;
        
    }
    
    return _dataArr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //=== 城市选择器Cell -- 省
    if (tableView == _leftTableView) {
        _cell = [tableView dequeueReusableCellWithIdentifier:@"leftTableViewCell" forIndexPath:indexPath];
        ICN_leftModel *model = _leftDataArray[indexPath.row];
        _cell.model = model;
        
        return _cell;
        //=== 城市选择器Cell -- 市
    }else if (tableView == _rightTableView){
        
        ICN_rightTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rightTableViewCell" forIndexPath:indexPath];
        cell.backgroundColor = RGB(208, 208, 208);
        
        ICN_rightModel *model = self.rightDataArray[indexPath.row];
        cell.model = model;
        return cell;
    }else if (tableView == _centerLeftTableView){
        
        ICN_centerLeftTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"centerLeftTableViewCell" forIndexPath:indexPath];
        ICN_centerLaftModel *model = _centerLeftDataArray[indexPath.row];
        cell.model = model;
        return cell;
    }else if (tableView == _centerRightTableView){
        ICN_centerRightTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"centerRightTableViewCell" forIndexPath:indexPath];
        ICN_centerRightModel *model = _centerRightDataArray[indexPath.row];
        cell.model = model;
        cell.backgroundColor = RGB(208, 208, 208);
        return cell;
        
    }
    else{
        ICN_PositionNextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        ICN_PositionNextModel *model = _dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    return nil;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _leftTableView) {
        return 42.5;
    }else if (tableView == _rightTableView){
        return 42.5;
        
    }else if (tableView == _centerLeftTableView){
        return 42.5;
        
    }else if (tableView == _centerRightTableView){
        
        return 42.5;
    }
    else{
        return 86;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //=== 城市选择器Cell -- 省
    if (tableView == _leftTableView) {
        // 第一步 遍历左边数据将select转化为@"0"
        for (NSInteger i = 0; i < self.leftDataArray.count; i++) {
            ICN_leftModel *model = self.leftDataArray[i];
            if (i == indexPath.row) {
                model.selected = @"1";
                // 当第0行的时候显示的是默认 - 此时将显示的变迁转换为城市
                if (indexPath.row == 0) {
                    _provincecode = nil;
                }else{
                    _provincecode = model.provincecode;
                }
            }else{
                model.selected = @"0";
            }
        }
        [_leftTableView reloadData];
        // 判断 如果provincecode 没有值默认显示的是默认选项
        if (_provincecode == nil) {
            ICN_rightModel *model = [[ICN_rightModel alloc] init];
            model.cityname = @"默认";
            model.selected = @"1";
            self.rightDataArray = [NSMutableArray arrayWithObjects:model, nil];
            [self.rightTableView reloadData];
        }else{
            NSDictionary *dic = @{@"provincecode":[NSString stringWithFormat:@"%@",_provincecode]};
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementCity" params:dic success:^(id result) {
//                NSArray *dict = result[@"result"];
//                [_rightDataArray removeAllObjects];
//                _rightDataArray = [NSMutableArray array];
//                for (NSDictionary *dic in dict) {
//                    
//                    ICN_rightModel *model = [[ICN_rightModel alloc]init];
//                    [model setValuesForKeysWithDictionary:dic];
//                    [_rightDataArray addObject:model];
//                    
//                }
//                // 默认将第一个选项设置为选中状态
//                if (self.rightDataArray.count > 0) {
//                    ICN_rightModel *RTmodel = _rightDataArray.firstObject;
//                    RTmodel.selected = @"1";
//                }
//                [_rightTableView reloadData];
//            } failure:^(NSDictionary *errorInfo) {
//                
//            }];
            
            [HTTPManager POST_PATH:PATH_SupplementCity params:dic success:^(id responseObject) {
                NSArray *dict = responseObject;
                [_rightDataArray removeAllObjects];
                _rightDataArray = [NSMutableArray array];
                for (NSDictionary *dic in dict) {
                    
                    ICN_rightModel *model = [[ICN_rightModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_rightDataArray addObject:model];
                    
                }
                // 默认将第一个选项设置为选中状态
                if (self.rightDataArray.count > 0) {
                    ICN_rightModel *RTmodel = _rightDataArray.firstObject;
                    RTmodel.selected = @"1";
                }
                [_rightTableView reloadData];
            } failure:^(NSError *error) {
                
            }];
        }
        
        //=== 城市选择器Cell -- 城市
    }else if (tableView == _rightTableView){
        
        // 在点击城市的时候首先需要判定选择的是不是默认城市 如果是则切换城市标字段为城市
        // 否则 1. 记录城市code 2. 将城市标签设置为城市名 3. 根据城市Code进行筛选
        if (self.rightDataArray != nil && self.rightDataArray.count > 0) {
            
            ICN_rightModel *selectModel = [self.rightDataArray objectAtIndex:indexPath.row];
            if (selectModel.citycode != nil && ![selectModel.citycode isEqualToString:@""]) {
                // 不是默认Model
                self.citycode = selectModel.citycode;
            }else{
                self.citycode = nil;
            }
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"开始筛选"];
            if (self.citycode) {
                // 根据城市筛选 筛选之前更新page
                self.citySearch = YES;
                self.posicationSearch = NO;
                self.needySearch = NO;
                for (ICN_rightModel *model in self.rightDataArray) {
                    if (model.citycode != nil) {
                        if ([model.citycode isEqualToString:self.citycode]) {
                            [self.btn1 setTitle:model.cityname forState:UIControlStateNormal];
                        }
                    }
                }
                _page = 1;
                NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",(long)_page],@"citycode":_citycode};
                
//                [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/Supplementcitylist" params:dic success:^(id result) {
//                    NSArray *dic1 =result[@"result"];
//                    if (dic1 == nil) {
//                        _dataArr = nil;
//                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无相关职位"];
//                        self.currentWindow.hidden = YES;
//                        self.btn1.selected = NO;
//                        _btn2.selected = NO;
//                        _btn3.selected = NO;
//                        [_tableView reloadData];
//                    }else{
//                        _dataArr = [NSMutableArray array];
//                        self.currentWindow.hidden = YES;
//                        for (NSDictionary *dic in dic1 ) {
//                            ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                            [model setValuesForKeysWithDictionary:dic];
//                            [_dataArr addObject:model];
//                            
//                        }
//                        [self.tableView reloadData];
//                    }
//                } failure:^(NSDictionary *errorInfo) {
//                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
//                }];
                
                [HTTPManager POST_PATH:PATH_Supplementcitylist params:dic success:^(id responseObject) {
                    NSArray *dic1 = responseObject;
                    if (dic1 == nil) {
                        _dataArr = nil;
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无相关职位"];
                        self.currentWindow.hidden = YES;
                        self.btn1.selected = NO;
                        _btn2.selected = NO;
                        _btn3.selected = NO;
                        [_tableView reloadData];
                    }else{
                        _dataArr = [NSMutableArray array];
                        self.currentWindow.hidden = YES;
                        for (NSDictionary *dic in dic1 ) {
                            ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                            [model setValuesForKeysWithDictionary:dic];
                            [_dataArr addObject:model];
                            
                        }
                        [self.tableView reloadData];
                    }

                } failure:^(NSError *error) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
                }];
                
            }else{
                // 修改城市标签
                _page = 1;
                self.citySearch = NO;
                self.posicationSearch = NO;
                self.needySearch = NO;
                [self.btn1 setTitle:@"城市" forState:UIControlStateNormal];
                self.currentWindow.hidden = YES;
                NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",(long)_page]};
//                [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionClass/postionclassSect" params:dic success:^(id result) {
//                    NSArray *dic1 =result[@"result"];
//                    
//                    if (_page == 1) {
//                        [_dataArr removeAllObjects];
//                        _dataArr = [NSMutableArray array];
//                    }
//                    for (NSDictionary *dic in dic1 ) {
//                        ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                        [model setValuesForKeysWithDictionary:dic];
//                        [_dataArr addObject:model];
//                    }
//                    
//                    if (_page == 1) {
//                        [_tableView.mj_header endRefreshing];
//                    }
//                    [_tableView.mj_footer endRefreshing];
//                    
//                    [self.tableView reloadData];
//                    
//                } failure:^(NSDictionary *errorInfo) {
//                    
//                }];
                
                [HTTPManager POST_PATH:PATH_postionclassSect params:dic success:^(id responseObject) {
                    NSArray *dic1 = responseObject;
                    
                    if (_page == 1) {
                        [_dataArr removeAllObjects];
                        _dataArr = [NSMutableArray array];
                    }
                    for (NSDictionary *dic in dic1 ) {
                        ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                        [model setValuesForKeysWithDictionary:dic];
                        [_dataArr addObject:model];
                    }
                    
                    if (_page == 1) {
                        [_tableView.mj_header endRefreshing];
                    }
                    [_tableView.mj_footer endRefreshing];
                    
                    [self.tableView reloadData];

                } failure:^(NSError *error) {
                    
                }];
            }
            
        }else{
            self.rightTableView.hidden = YES;
        }
        
        self.currentWindow.hidden = YES;
        self.btn1.selected = NO;
        _btn2.selected = NO;
        _btn3.selected = NO;
    }else if (tableView == _centerLeftTableView){
        ICN_centerLaftModel *modl = _centerLeftDataArray[indexPath.row];
        _bigclassoneid = modl.bigclassoneid;
        NSDictionary *dic = @{@"bigclassoneid":[NSString stringWithFormat:@"%@",_bigclassoneid]};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementIndustrytwo" params:dic success:^(id result) {
//            //_rightDataArray = [[NSMutableArray alloc]init];
//            [_centerRightDataArray removeAllObjects];
//            NSArray *dict = result[@"result"];
//            for (NSDictionary *dic in dict) {
//                ICN_centerRightModel *model = [[ICN_centerRightModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [_centerRightDataArray addObject:model];
//                
//            }
//            [_centerRightTableView reloadData];
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        [HTTPManager POST_PATH:PATH_SupplementIndustrytwo params:dic success:^(id responseObject) {
            //_rightDataArray = [[NSMutableArray alloc]init];
            [_centerRightDataArray removeAllObjects];
            NSArray *dict = responseObject;
            for (NSDictionary *dic in dict) {
                ICN_centerRightModel *model = [[ICN_centerRightModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [_centerRightDataArray addObject:model];
                
            }
            [_centerRightTableView reloadData];
        } failure:^(NSError *error) {
            
        }];
    }else if (tableView == _centerRightTableView){
        
        ICN_centerRightModel * model= _centerRightDataArray[indexPath.row];
        
        _bigclasstwoid = model.bigclasstwoid;
        NSLog(@"%@",_bigclasstwoid);
        NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",_page],@"citycode":_bigclasstwoid};
        
        //self.dataArr = nil;
        
        
        [HTTPManager POST_PATH:PATH_supplementIndustrylist params:@{@"page":[NSString stringWithFormat:@"%ld",_page],@"bigclasstwoid":_bigclasstwoid} success:^(id responseObject) {
            NSArray *dic1 = responseObject;
            
            if (dic1 == nil) {
                _dataArr = nil;
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无相关职位"];
                self.currentWindow.hidden = YES;
                self.btn1.selected = NO;
                _btn3.selected = NO;
                _btn2.selected = NO;
                [_tableView reloadData];
            }else{
                _dataArr = [NSMutableArray array];
                self.currentView.hidden = YES;
                for (NSDictionary *dic in dic1 ) {
                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                    [model setValuesForKeysWithDictionary:dic];
                    [_dataArr addObject:model];
                    
                }
                NSLog(@"%@",_dataArr);
                [self.tableView reloadData];
            }

        } failure:^(NSError *error) {
             _dataArr = [NSMutableArray array];
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无相关职位"];
             [_tableView reloadData];
        }];
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Supplement/SupplementIndustrylist" params:dic success:^(id result) {
//            NSArray *dic1 =result[@"result"];
//            if (dic1 == nil) {
//                _dataArr = nil;
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"无相关职位"];
//                self.currentWindow.hidden = YES;
//                self.btn1.selected = NO;
//                _btn3.selected = NO;
//                _btn2.selected = NO;
//                [_tableView reloadData];
//            }else{
//                _dataArr = [NSMutableArray array];
//                self.currentView.hidden = YES;
//                for (NSDictionary *dic in dic1 ) {
//                    ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                    [model setValuesForKeysWithDictionary:dic];
//                    [_dataArr addObject:model];
//                    
//                }
//                NSLog(@"%@",_dataArr);
//                [self.tableView reloadData];
//            }
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
        
        self.currentWindow.hidden = YES;
        [self removeCentersTableViewFromSuperView];
        self.btn1.selected = NO;
        _btn3.selected = NO;
        _btn2.selected = NO;
        
    }
    else{
        
        if (![self getCurrentUserLoginStatus]) {
            ICN_SignViewController *vc = [[ICN_SignViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
            [self currentPagerJumpToPager:vc];
            return;
        }
        
        ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
        
        icn.hidesBottomBarWhenPushed = YES;//隐藏下面的分栏
        ICN_PositionNextModel *model = self.dataArr[indexPath.row];
        icn.url = model.positionid;
        [self.navigationController pushViewController:icn animated:YES];
        
    }
    
}

//- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
//    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
//
//    if (!childVc) {
//       ICN_PositionNextViewController *MVC = [[ICN_PositionNextViewController alloc] init];
//        MVC.index = index;
//        childVc = MVC;
//
//    }
//
//
//    //    if (index%2==0) {
//    //        childVc.view.backgroundColor = [UIColor blueColor];
//    //    } else {
//    //        childVc.view.backgroundColor = [UIColor greenColor];
//    //
//    //    }
//
//    NSLog(@"%ld-----%@",(long)index, childVc);
//
//    return childVc;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
