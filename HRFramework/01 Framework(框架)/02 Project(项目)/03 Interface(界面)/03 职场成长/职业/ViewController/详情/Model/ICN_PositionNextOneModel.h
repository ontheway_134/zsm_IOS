//
//  ICN_PositionNextOneModel.h
//  ICan
//
//  Created by 那风__ on 16/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_PositionNextOneModel : NSObject
@property(nonatomic)NSString *companyLogo;
@property(nonatomic)NSString *positionTitle;
@property(nonatomic)NSString *salary;
@property(nonatomic)NSString *qualification;
@property(nonatomic)NSString *companyName;
@property(nonatomic)NSString *city;
@property(nonatomic)NSString *workExperience;
@property(nonatomic)NSString *workType;
@property(nonatomic)NSString *personCount;
@property(nonatomic)NSString *summary;
@property (strong,nonatomic)NSString * code;

@property (strong,nonatomic)NSString * positionid;
@property(nonatomic)NSString *companyNature;
@property(nonatomic)NSString *companyScale;
@property(nonatomic)NSString *companyIndustry;
@property(nonatomic)NSString *companyDetail;

//举报
@property(nonatomic)NSString *isreport;
//收藏
@property(nonatomic)NSString *iscollect;
//关注
@property(nonatomic)NSString *isfollow;
//企业
@property(nonatomic)NSString *memberId;

@property(nonatomic)NSString *issend;   //是否投递  0可投递 1不可投递
@property(nonatomic)NSString *iscompleteness;  // 简历完成度 0大于70%  1没有

@end
