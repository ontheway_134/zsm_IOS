//
//  ICN_ProfessionSearchViewController.m
//  ICan
//
//  Created by 那风__ on 17/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ProfessionSearchViewController.h"
#import "ICN_PositionNextModel.h"
#import "ICN_PositionNextTableViewCell.h"
#import "ICN_IndustryTableViewCell.h"
#import "ICN_PositionNextOneViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_ProjectTableViewCell.h"
#import "ICN_ProjectModel.h"
#import "ICN_IndustryNextViewController.h"
#import "ICN_ActivityMultSearchHeaderView.h"   //头视图
//static NSString *const PATH_postionSecVague = @"MyPosition/PositionSelect/postionSecVague";

@interface ICN_ProfessionSearchViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic)UITextField *textField;
@property(nonatomic)NSString *textStr;
@property(nonatomic)NSInteger page;
@property (nonatomic,strong) ICN_ActivityMultSearchHeaderView * headerView;
@end

@implementation ICN_ProfessionSearchViewController
#pragma mark ----------------懒加载---------------------
- (ICN_ActivityMultSearchHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_ActivityMultSearchHeaderView);
    }
    return _headerView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
    _textField.text = _searchContent;
    [self.view addSubview:self.tableView];
    
    // Do any additional setup after loading the view.
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    [self.headerView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
    [view addSubview:self.headerView];
    self.headerView.categryLabel.text = @"职位";
    [self.tableView setTableHeaderView:view];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.hiddenDefaultNavBar = YES;
}

-(void)leftItemClicked:(UIButton *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)ssBtnAction:(UIButton *)btn{
    
    [self.textField resignFirstResponder];
    [self.tableView.mj_header beginRefreshing];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    /*这个地方需要判断当前网络请求是什么类型的    1行业资讯，2职场规划   */
    /*招聘  为0*/
    [self.tableView.mj_header beginRefreshing];
    
//    if ([self.typeStr isEqualToString:@"0"]) {
//        NSDictionary *dic = @{@"page":@"1",@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionSelect/postionSecVague" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            self.dataArr = nil;
//            for (NSDictionary *dic in dic1 ) {
//                ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            [self.view addSubview:self.tableView];
//            [self.tableView reloadData];
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
//
//    }
//    /*行业资讯 1*/
//    else if([self.typeStr isEqualToString:@"1"]){
//    
//        NSDictionary *dic = @{@"informationType":@"1",@"page":@"1",@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionSelectNewslist" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            self.dataArr = nil;
//            for (NSDictionary *dic in dic1 ) {
//                ICN_IndustryModel *model = [[ICN_IndustryModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            [self.view addSubview:self.tableView];
//            [self.tableView reloadData];
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
//    }
//    /*2职场规划*/
//    else if([self.typeStr isEqualToString:@"2"]){
//        NSDictionary *dic = @{@"informationType":@"2",@"page":@"1",@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionSelectNewslist" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            self.dataArr = nil;
//            for (NSDictionary *dic in dic1 ) {
//                ICN_ProjectModel *model = [[ICN_ProjectModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            [self.view addSubview:self.tableView];
//            [self.tableView reloadData];
//            
//        } failure:^(NSDictionary *errorInfo) {
//            
//        }];
//    }else{
//    
//    }
    
    
    return YES;
}
//=== 根据标识字段刷新data的方法
-(void)refreshingData{
    
    _page = 1;
    
    [self datas];

}
//=== 根据标识字段加载data的方法
-(void)loadMoreData{
    
    _page ++;
    
    [self datas];
    
    
}
- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor clearColor];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
            if (!header.isRefreshing) {
                [self refreshingData];
            }
        }];
        header.stateLabel.hidden = YES;
        header.lastUpdatedTimeLabel.hidden = YES;
        
        [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
        NSMutableArray *imagelist = [NSMutableArray array];
        for (NSInteger i = 1; i <= 35; i++) {
            [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
        }
        [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
        [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
        [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
        [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
        self.tableView.mj_header = header;
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            if (self.tableView.mj_footer.isRefreshing) {
                [self loadMoreData];
            }
        }];
        self.tableView.mj_footer = footer;
        [footer setTitle:@"下拉刷新列表" forState:MJRefreshStateIdle];
        [footer setTitle:@"加载列表中" forState:MJRefreshStatePulling];
        [footer setTitle:@"加载列表中" forState:MJRefreshStateRefreshing];
        [footer setTitle:@"列表数据加载完成" forState:MJRefreshStateNoMoreData];
        
        // 设置刚进来的时候mj_footer默认隐藏
        self.tableView.mj_footer.hidden = YES;
        [self.tableView.mj_header beginRefreshing];
        
        
        [_tableView registerClass:[ICN_PositionNextTableViewCell class] forCellReuseIdentifier:@"cell"];
         [_tableView registerClass:[ICN_IndustryTableViewCell class] forCellReuseIdentifier:@"ICN_IndustryTableViewCell"];
         [_tableView registerClass:[ICN_ProjectTableViewCell class] forCellReuseIdentifier:@"ICN_ProjectTableViewCell"];
    }
    return _tableView;
}

- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArr.count;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([scrollView isEqual:self.tableView]) {
        if (scrollView.contentOffset.y > 0) {
            // 在mj_header刷新的时候判断mj_footer是否隐藏若是隐藏则让其恢复显示
            if (self.tableView.mj_footer.hidden) {
                self.tableView.mj_footer.hidden = NO;
            }
        }
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*这里也需要判断当前类型  0 1 2 */
    if ([self.typeStr isEqualToString:@"0"]) {
        ICN_PositionNextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        ICN_PositionNextModel *model = self.dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if ([self.typeStr isEqualToString:@"1"]) {
        
        ICN_IndustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_IndustryTableViewCell" forIndexPath:indexPath];
        ICN_IndustryModel *model = self.dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if ([self.typeStr isEqualToString:@"2"]) {
        
        ICN_ProjectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_ProjectTableViewCell" forIndexPath:indexPath];
        ICN_ProjectModel *model = self.dataArr[indexPath.row];
        cell.model = model;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else{
        return  nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    /*这里也需要判断当前类型  0 1 2 */
    if ([self.typeStr isEqualToString:@"0"]) {
        return 86;
    }
    else if ([self.typeStr isEqualToString:@"1"]) {
        
        return 103;
    }
    else if ([self.typeStr isEqualToString:@"2"]) {
        
        return 203;
    }
    else{
        return  0;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.typeStr isEqualToString:@"0"]) {
        ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
        icn.hidesBottomBarWhenPushed = YES;
        ICN_PositionNextModel *model = self.dataArr[indexPath.row];
        icn.url = model.positionid;
        [self.navigationController pushViewController:icn animated:YES];
        /* 搜索返回闪屏 加下面这句 */
        [self.view endEditing:YES];
    } else if ([self.typeStr isEqualToString:@"1"]) {
        
        ICN_IndustryNextViewController *icn = [[ICN_IndustryNextViewController alloc]init];
        ICN_IndustryModel *model = self.dataArr[indexPath.row];
        
        icn.url = model.infoid;
        
        icn.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:icn animated:YES];
         [self.view endEditing:YES];
    }
    else if ([self.typeStr isEqualToString:@"2"]) {
        
        ICN_IndustryNextViewController *icn = [[ICN_IndustryNextViewController alloc]init];
        ICN_ProjectModel *model = self.dataArr[indexPath.row];
        
        icn.url = model.infoid;
        
        icn.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:icn animated:YES];
         [self.view endEditing:YES];
    }
    else{
     
    }


}

//=== 获取数据的基本方法
-(void)datas{
    
    // 如果搜索中没有内容直接提示重新添加搜索内容
    if (self.textField.text == nil || [self.textField.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加搜索内容"];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.hidden = YES;
        return ;
    }else{
         self.tableView.hidden = NO;
    }
   
    if ([self.typeStr isEqualToString:@"0"]) {
        NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",(long)_page],@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionSelect/postionSecVague" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            
//            if (_page == 1) {
//                [_dataArr removeAllObjects];
//            }
//            
//            for (NSDictionary *dic in dic1 ) {
//                ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            if (self.dataArr.count > 0) {
//                [self.tableView reloadData];
//            }else{
//                self.tableView.hidden = YES;
//            }
//            
//        } failure:^(NSDictionary *errorInfo) {
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            self.tableView.hidden = YES;
//        }];
        
        
        [HTTPManager POST_PATH:PATH_postionSecVague params:dic success:^(id responseObject) {
            NSArray *dic1 = responseObject;
            
            if (_page == 1) {
                [_dataArr removeAllObjects];
            }
            
            for (NSDictionary *dic in dic1 ) {
                ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            if (self.dataArr.count > 0) {
                [self.tableView reloadData];

            }
        } failure:^(NSError *error) {
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
           // self.tableView.hidden = YES;
        }];
        
    }
    /*行业资讯 1*/
    else if([self.typeStr isEqualToString:@"1"]){
        
        NSDictionary *dic = @{@"informationType":@"1",@"page":[NSString stringWithFormat:@"%ld",(long)_page],@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionSelectNewslist" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            //self.dataArr = nil;
//            if (_page == 1) {
//                [_dataArr removeAllObjects];
//            }
//            
//            for (NSDictionary *dic in dic1 ) {
//                ICN_IndustryModel *model = [[ICN_IndustryModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            if (self.dataArr.count > 0) {
//                [self.tableView reloadData];
//            }else{
//                self.tableView.hidden = YES;
//            }
//            
//        } failure:^(NSDictionary *errorInfo) {
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            self.tableView.hidden = YES;
//        }];
        
        [HTTPManager POST_PATH:PATH_postionSelectNewslist params:dic success:^(id responseObject) {
            NSArray *dic1 = responseObject;
            //self.dataArr = nil;
            if (_page == 1) {
                [_dataArr removeAllObjects];
            }
            
            for (NSDictionary *dic in dic1 ) {
                ICN_IndustryModel *model = [[ICN_IndustryModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            if (self.dataArr.count > 0) {
                [self.tableView reloadData];
            }else{
                self.tableView.hidden = YES;
            }

        } failure:^(NSError *error) {
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            self.tableView.hidden = YES;
        }];
    }
    /*2职场规划*/
    else if([self.typeStr isEqualToString:@"2"]){
        NSDictionary *dic = @{@"informationType":@"2",@"page":[NSString stringWithFormat:@"%ld",(long)_page],@"positionTitle":_textField.text};
//        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionSelectNewslist" params:dic success:^(id result) {
//            
//            NSArray *dic1 = result[@"result"];
//            //self.dataArr = nil;
//            if (_page == 1) {
//                [_dataArr removeAllObjects];
//            }
//            for (NSDictionary *dic in dic1 ) {
//                ICN_ProjectModel *model = [[ICN_ProjectModel alloc]init];
//                [model setValuesForKeysWithDictionary:dic];
//                [self.dataArr addObject:model];
//            }
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            if (self.dataArr.count > 0) {
//                [self.tableView reloadData];
//            }else{
//                self.tableView.hidden = YES;
//            }
//            
//        } failure:^(NSDictionary *errorInfo) {
//            [_tableView.mj_header endRefreshing];
//            [_tableView.mj_footer endRefreshing];
//            self.tableView.hidden = YES;
//        }];
        
        [HTTPManager POST_PATH:PATH_postionSelectNewslist params:dic success:^(id responseObject) {
            NSArray *dic1 = responseObject;
            //self.dataArr = nil;
            if (_page == 1) {
                [_dataArr removeAllObjects];
            }
            for (NSDictionary *dic in dic1 ) {
                ICN_ProjectModel *model = [[ICN_ProjectModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [self.dataArr addObject:model];
            }
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            if (self.dataArr.count > 0) {
                [self.tableView reloadData];
            }else{
                self.tableView.hidden = YES;
            }

        } failure:^(NSError *error) {
            [_tableView.mj_header endRefreshing];
            [_tableView.mj_footer endRefreshing];
            self.tableView.hidden = YES;

        }];
    }
}

// 对于整个页面的配置
-(void)createUI{
    _page = 1;
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *navView = [[UIView alloc]init];
    navView.backgroundColor = RGB0X(0x009dff);
    [self.view addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.top.mas_equalTo(self.view.mas_top).offset(0);
        make.height.mas_equalTo(64);
    }];
    
    UIButton *ssBtn = [[UIButton alloc]init];
    [navView addSubview:ssBtn];
    [ssBtn setTitle:@"搜索" forState:UIControlStateNormal];
    ssBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [ssBtn addTarget:self action:@selector(ssBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [ssBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(33);
        make.right.mas_equalTo(-5);
        make.height.mas_equalTo(20);
        make.width.mas_equalTo(30);
    }];
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 33, 10, 16)];
    
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [navView addSubview:btn];
    
    UIImageView *searchImage = [[UIImageView alloc]init];
    searchImage.image = [UIImage imageNamed:@"搜索框.png"];
    searchImage.userInteractionEnabled = YES;
    [navView addSubview:searchImage];
    [searchImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(btn.mas_right).offset(15);
        make.right.mas_equalTo(self.view.mas_right).offset(-35);
        make.top.mas_equalTo(navView.mas_top).offset(29);
        make.height.mas_equalTo(27);
    }];
    UIImageView *souImage = [[UIImageView alloc]init];
    souImage.image = [UIImage imageNamed:@"搜索.png"];
    [searchImage addSubview:souImage];
    
    [souImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(searchImage.mas_left).offset(10);
        make.top.mas_equalTo(navView.mas_top).offset(35);
        make.width.mas_equalTo(15);
    }];
    UITextField *textField = [[UITextField alloc]init];
    textField.placeholder = @"搜索关键字";
    textField.font = [UIFont systemFontOfSize:11];
    textField.textColor = RGB0X(0x7e94a2);
    textField.delegate = self;
    [searchImage addSubview:textField];
    _textField = textField;
    [_textStr isEqualToString:_textField.text];
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(souImage.mas_right).offset(6);
        make.top.mas_equalTo(navView.mas_top).offset(36);
        make.right.mas_equalTo(searchImage.mas_right).offset(0);
    }];
    
    UIImageView *jieImage = [[UIImageView alloc]init];
    jieImage.image = [UIImage imageNamed:@"无结果.png"];
    [self.view addSubview:jieImage];
    
    [jieImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(navView.mas_bottom).offset(160);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
    UILabel *textLabel = [[UILabel alloc]init];
    textLabel.text = @"没有搜索到您想要的结果!";
    textLabel.font = [UIFont systemFontOfSize:11];
    textLabel.textColor = RGB0X(0x666666);
    [self.view addSubview:textLabel];
    [textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(jieImage.mas_bottom).offset(35);
        make.centerX.mas_equalTo(self.view.mas_centerX);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
