//
//  phoneIstexit.h
//  ICan
//
//  Created by shilei on 17/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface phone : JSONModel

@property(nonatomic,strong)NSString *isExists;

@end

@interface phoneIstexit : JSONModel

@property(nonatomic,strong)NSString *info;
@property(nonatomic,assign)NSInteger code;

@property(nonatomic,strong)phone *result;

@end
