//
//  ICN_PritocolViewController.m
//  ICan
//
//  Created by shilei on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PritocolViewController.h"

@interface ICN_PritocolViewController ()

@end

@implementation ICN_PritocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (IBAction)backBtnActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
