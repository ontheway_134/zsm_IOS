//
//  ICN_FindPassWordViewController.m
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_FindPassWordViewController.h"
#import "ICN_SetNewPassWordViewController.h"
#import "BaseOptionalModel.h"
#import "YHCountDownButton.h"
#import "ICN_findPasswordModel.h"
#import "ICN_Regular.h"
#import "phoneIstexit.h"
#import "NSString+Regular.h"

@interface ICN_FindPassWordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emialOrPhoneText;
@property (weak, nonatomic) IBOutlet UIButton *CommitBtn;

@property (weak, nonatomic) IBOutlet UITextField *codeTextFiled;
@end

@implementation ICN_FindPassWordViewController

#pragma mark - --- 网络请求 ---

//手机号找回密码
-(void)httpVerifyMobile{
    NSDictionary *dic=@{@"verify":self.codeTextFiled.text,@"mobile":self.emialOrPhoneText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_PhoneCode params:dic success:^(id result) {
        ICN_findPasswordModel *basemodel=[[ICN_findPasswordModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [self.view endEditing:YES];
            ICN_SetNewPassWordViewController *setNewpasswoed=[[ICN_SetNewPassWordViewController alloc]init];
            setNewpasswoed.uesrID=basemodel.result.ttttt;
            [self.navigationController pushViewController:setNewpasswoed animated:YES];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"失败");
    }];

}

//邮箱的找回密码
-(void)httpVerityEmile{
    NSDictionary *dic=@{@"verify":self.codeTextFiled.text,@"email":self.emialOrPhoneText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CHECKEMAIL params:dic success:^(id result) {
        ICN_findPasswordModel *basemodel=[[ICN_findPasswordModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [self.view endEditing:YES];
            ICN_SetNewPassWordViewController *setNewpasswoed=[[ICN_SetNewPassWordViewController alloc]init];
            setNewpasswoed.uesrID=basemodel.result.ttttt;
            [self.navigationController pushViewController:setNewpasswoed animated:YES];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"失败");
    }];


}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.CommitBtn.layer.cornerRadius = 5.0;
    self.CommitBtn.layer.masksToBounds = YES;
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)CommitBtnAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if ([self.emialOrPhoneText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入手机号或邮箱"];
        return ;
    }
    
    if (![ICN_Regular checkEmail:self.emialOrPhoneText.text] && ![ICN_Regular checkTelephoneNumber:self.emialOrPhoneText.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的格式"];
        return ;
    }
//
    
    
    
    if ([ICN_Regular checkTelephoneNumber:self.emialOrPhoneText.text]) {
        [self httpVerifyMobile];
    }
    if ([ICN_Regular checkEmail:self.emialOrPhoneText.text]) {
        [self httpVerityEmile];
    }
    
   
    
}
//按钮的点击事件
- (IBAction)CodeBtnActions:(YHCountDownButton *)sender {
    
    [self.view endEditing:YES];
    if ([self.emialOrPhoneText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入手机号或邮箱"];
        return ;
    }
    
    if (![ICN_Regular checkEmail:self.emialOrPhoneText.text] && ![ICN_Regular checkTelephoneNumber:self.emialOrPhoneText.text] ) {
         [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的格式"];
        return ;
    }

    // 判断是邮箱走下面的流程
    if ([self.emialOrPhoneText.text checkEmail]) {
        YHCountDownButton *sender_btn = sender;
        NSDictionary *dic=@{@"email":self.emialOrPhoneText.text};
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_EMIALEXIT params:dic success:^(id result) {
            
            phoneIstexit *model=[[phoneIstexit alloc]initWithDictionary:result error:nil];
            if ([model.result.isExists isEqualToString:@"0"]) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该邮箱未注册"];
            }else{
                NSDictionary *dic = @{@"email":self.emialOrPhoneText.text};
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_EMIAL params:dic success:^(id result) {
                    BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
                    if (basemodel.code == 0) {
                        
                        
                        sender_btn.enabled = NO;
                        [sender_btn startWithSecond:CODE_S];
                        [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                            NSString *title = [NSString stringWithFormat:@"%.2d秒",second];
                            return title;
                        }];
                        [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                            countDownButton.enabled = YES;
                            return @"重新获取";
                        }];
                        
                        
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请到邮箱中验证"];
                    }else{
                        NSLog(@"这里走了code等于1");
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    NSLog(@"请求失败");
                }];
                
            }
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];

    }
    
    // 判断是手机号走下面的获取验证码
    
    if ([self.emialOrPhoneText.text checkTelephoneNumber]) {
       
        YHCountDownButton *sender_btn = sender;
        NSDictionary *dic=@{@"mobile":self.emialOrPhoneText.text};
        [[[HRNetworkingManager alloc]init] POST_PATH:PATH_PHONEISTEIT params:dic success:^(id result) {
            
            phoneIstexit *model=[[phoneIstexit alloc]initWithDictionary:result error:nil];
            if ([model.result.isExists isEqualToString:@"0"]) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该手机号未注册"];
                
            }else{
                
                [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CODE params:@{@"mobile":self.emialOrPhoneText.text,@"type":@1} success:^(id result) {
                    BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                    if (codemodel.code == 0) {
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"发送成功请注意查收"];
                        sender_btn.enabled = NO;
                        [sender_btn startWithSecond:CODE_S];
                        [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                            NSString *title = [NSString stringWithFormat:@"%.2d秒",second];                        return title;
                        }];
                        [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                            countDownButton.enabled = YES;
                            return @"重新获取验";
                            
                        }];
                    }
                    else{
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    
                }];
                
            }
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        

        
        
    
    }
    
  
}


@end
