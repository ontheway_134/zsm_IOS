//
//  ICN_RegisterFootView.h
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_RegisterFootView : UIView
@property (weak, nonatomic) IBOutlet UIButton *RegisterBtn;
@property (weak, nonatomic) IBOutlet UIButton *protocolBtn;

@end
