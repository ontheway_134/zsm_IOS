//
//  ICN_RegisterTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_RegisterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *falseimage;
@property (weak, nonatomic) IBOutlet UITextField *registTextFiled;

@end
