//
//  ICN_RegisterFootView.m
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_RegisterFootView.h"

@implementation ICN_RegisterFootView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.RegisterBtn.layer.cornerRadius=5.0;
    self.RegisterBtn.layer.masksToBounds = YES;

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
