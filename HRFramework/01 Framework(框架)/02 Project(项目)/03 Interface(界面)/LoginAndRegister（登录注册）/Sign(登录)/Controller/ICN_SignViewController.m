//
//  ICN_SignViewController.m
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SignViewController.h"
#import "ICN_FindPassWordViewController.h"
#import "ICN_SignModel.h"
#import "BaseOptionalModel.h"
#import "ChatDemoHelper.h"
#import "ICN_StartViewController.h"
#import "NSString+Regular.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import "ICN_GuiderPager.h" // 新版引导页面

//个人版判断
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
//企业版判断
#import "ICN_GetEnterPriseModel.h"
#import "ICN_BusinessMassageViewController.h"
#import "ICN_huanxinModel.h"
#import "ICN_MyPersonalHeaderModel.h"
@interface ICN_SignViewController ()
@property (weak, nonatomic) IBOutlet UITextField *registerText;
@property (weak, nonatomic) IBOutlet UITextField *repeaPasswordText;
@property (weak, nonatomic) IBOutlet UIButton *signBtn;


@end

@implementation ICN_SignViewController

#pragma mark - --- 网络请求 ---
-(void)HttpLogins{
    
    // 设置登录状态
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"登录中"];
    // 如果没有消失延时10秒之后取消登录状态
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
    });
    
    NSDictionary *dic=@{@"username":self.registerText.text,@"password":self.repeaPasswordText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERLOGIN params:dic success:^(id result) {
        
        // 设置登录按钮可用
        self.signBtn.enabled = YES;
        
        ICN_SignModel *sign = [[ICN_SignModel alloc] initWithDictionary:result error:nil];
        
        if (sign.code == 0) {
            
            if ([sign.result.roleId isEqualToString:@"1"]) {
                
                // 进行个人用户登录校验
                [self networkRequestWithPersonalLoginWithSingModel:sign];
                
                // 对于企业用户的登录进行判断
            }else if([sign.result.roleId isEqualToString:@"2"]){
                
                [self networkRequestWithEnterpriseLoginWithSingModel:sign];
                
            }
        }else{
            
            // 提示前先隐藏之前的HUD
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:sign.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        // 设置登录按钮可用
        self.signBtn.enabled = YES;
    }];
}

// 请求个人用户登录数据
- (void)networkRequestWithPersonalLoginWithSingModel:(ICN_SignModel *)sign{
     NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    token = sign.result.token;
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    //     http://1ican.com/index.php/Member/Member/getIndexInfo 原网址
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
        NSDictionary *dict = result[@"result"]
        ;
        ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:dict];
        
        if (model.memberLogo) {
            [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
            [USERDEFAULT synchronize];
        }
        if (model.memberNick) {
            [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
            [USERDEFAULT synchronize];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    // 获取环信id接口
    NSDictionary *dictoken=@{@"token":sign.result.token};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
        
        ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
        
        // 个人用户环信信息获取成功
        if (basemodel.code == 0) {
            //如果获取到接口，进行环信的登录
            [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                
                // 环信登录成功，隐藏MBP
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                
                //环信登录成功，把环信的值存起来
                [[NSUserDefaults standardUserDefaults] setValue:@"huanxin" forKey:@"phonehuanxin"];
                // 登陆成功后 存储用户token
                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                // 个人用户登录成功后设置用户类型
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                // 进行用户登录操作 -- 确定登录之前进行绑定阿里云别名操作
                // 如果当时存在token的话使用当时的token注册阿里云推送的别名
                if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                    [CloudPushSDK addAlias:[USERDEFAULT valueForKey:HR_CurrentUserToken] withCallback:^(CloudPushCallbackResult *res) {
                        
                    }];
                }
                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                [self presentViewController:barController animated:YES completion:nil];
#warning -- personal base mseeage check
                // 进行基本信息校验 --
                NSDictionary * dic = @{@"token":sign.result.token};
                [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
                    NSDictionary *dic1 =result[@"result"];
                    // 获取基本信息Model
                    ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                    if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                        || model.hopePositionName == nil|| model.workStatus == nil) {
                        // 设置用户登录类型 - loginStatus（1 ： 基本信息为填完 ； 0：基本信息填完了）
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    } else {
                        //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
                        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    [MBProgressHUD hiddenHUDWithSuperView:self.view];
                    HRLog(@"error --- 个人用户基本信息网络请求失败");
                    // 设置为游客登录状态
                    [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
                    [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
                }];
                
            } failure:^(id data) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                HRLog(@"error --- 环信服务器登录失败");
            }];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        NSLog(@"error --- 请求环信的用户名网络请求失败");
    }];

}

// 请求企业用户登录数据
- (void)networkRequestWithEnterpriseLoginWithSingModel:(ICN_SignModel *)sign{
    
    // 设置用户token 并设置用户基本类型（企业用户）
    [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
    [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNEnterprise] forKey:HR_UserTypeKEY];
    
    // 校验企业用户是否通过审核
    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        
        // 环信登录成功，隐藏MBP
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        
        ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
        // 根据企业的audit判断是否审核通过
        if ([model.result.audit integerValue] == 0) {
            // 审核未通过
            [USERDEFAULT setValue:@"0" forKey:HR_BusinessAuditKEY];
        }
        if ([model.result.audit integerValue] == 1) {
            // 审核通过了
            [USERDEFAULT setValue:@"1" forKey:HR_BusinessAuditKEY];
        }
        // 进行用户登录操作 -- 确定登录之前进行绑定阿里云别名操作
        // 如果当时存在token的话使用当时的token注册阿里云推送的别名
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
            [CloudPushSDK addAlias:[USERDEFAULT valueForKey:HR_CurrentUserToken] withCallback:^(CloudPushCallbackResult *res) {
                if (errno) {
                    HRLog(@"%@",errno);
                }
            }];
        }
        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
        [self presentViewController:barController animated:YES completion:nil];
#warning -- company base mseeage check
        // 进行基本信息校验 --
        if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
            model.result.provinceName == nil || model.result.companyDetail == nil ) {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"error --- 企业用户基本信息网络请求失败");
        // 环信登录成功，隐藏MBP
        [MBProgressHUD hiddenHUDWithSuperView:self.view];
        // 设置为游客登录状态
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
    }];

    
}

#pragma mark - --- 生命周期 ---

- (instancetype)init{
    self = [super init];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.signBtn.layer.cornerRadius = 5.0;
    self.signBtn.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

//跳转到欢迎页面
//- (void)popToVCc{
//    ICN_StartViewController *pager = [[ICN_StartViewController alloc] init];
//    [self presentViewController:pager animated:YES completion:nil];
//}


#pragma mark - --- IBActions ---

- (IBAction)ClickBtnActions:(id)sender {
    [self.view endEditing:YES];
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
            // 点击页面的返回按钮（返回上级页面）
        case 0:{
            /** 点击登录页面的返回按钮的逻辑操作
             
             1. 引导页跳转到的登录页面 (页面的导航栏中只有一个VC就是登录页面)
             2. 由游客登录等触发的登录页面（页面导航栏中不止有一个VC）
             由登录页面触发的返回操作的结果都是返回到欢迎页面
             */
            // 判断有navigationController 并且当前页面不是navigationController的跟视图
            if (self.navigationController != nil && self.navigationController.childViewControllers.count < 2) {
                // 选项1
                [self dismissViewControllerAnimated:YES completion:nil];
            }else{
                // 选项二 ： 模态跳转到引导页 - 设置rootVC为引导页
                ICN_GuiderPager * pager = [[ICN_GuiderPager alloc] init];
                [self presentViewController:pager animated:YES completion:^{
                    [APPLICATION keyWindow].rootViewController = pager;
                }];
            }
        }
            break;
            
            // 点击忘记密码按钮
        case 1:{
            // 点击获取用户密码需要想访问接口确认用户是否存在
            
            ICN_FindPassWordViewController *find=[[ICN_FindPassWordViewController alloc]init];
            [self.navigationController pushViewController:find animated:YES];
        }
            
            break;
            
            // 点击登录按钮
        case 2:{
            
            // 第一步判断网络状态
            if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
                // 当前无网络
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"当前无网络"];
                return ;
            }
            
            // 校验登录信息
            if (self.registerText.text == nil || [self.registerText.text isEqualToString:@""]) {
                // 先判断手机号 是否为空
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
                return ;
            }
            // 再判断手机号是否正确
//            if (![self.registerText.text checkTelephoneNumber]) {
//                [MBProgressHUD ShowProgressWithBaseView:self.view Message:TUREPHONE];
//                return ;
//            }
            // 最后判断密码
            if (self.repeaPasswordText.text == nil || [self.repeaPasswordText.text isEqualToString:@""]) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:PASSWORD];
                return ;
            }
            
            // 进行登录校验
            [self HttpLogins];
        }
            
            break;
    }
}

#pragma mark - --- Private Method ---


@end
