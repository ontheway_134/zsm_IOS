//
//  ICN_SignModel.h
//  ICan
//
//  Created by shilei on 16/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface Model : JSONModel

@property(nonatomic,strong)NSString *token;//用户唯一标识

@property(nonatomic,strong)NSString *roleId;   //1普通用户  2企业用户

@end



@interface ICN_SignModel : BaseOptionalModel

@property(nonatomic,strong)Model * result;






@end
