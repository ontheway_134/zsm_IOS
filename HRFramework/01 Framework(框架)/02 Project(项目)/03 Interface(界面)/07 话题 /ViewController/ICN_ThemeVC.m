//
//  ICN_ThemeVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ThemeVC.h"
#import "ICN_ActivityMainModel.h"
#import "ICN_ActivityFristTableViewCell.h"
#import "ICN_ActivityParticularsViewController.h"
#import "ICN_ActivityHeaderView.h"
#import "ICN_LiveTableViewCell.h"
#import "ICN_ActivityDetialViewController.h"
@interface ICN_ThemeVC ()
#pragma mark ----------------UIBUTON---------------------
// 选项卡按钮·（热门、付费、互联网、市场）
@property (weak, nonatomic) IBOutlet UIButton *hotSelectedBtn; // 选中热门按钮
@property (weak, nonatomic) IBOutlet UIButton *paySelectedBtn; // 选中付费按钮
@property (weak, nonatomic) IBOutlet UIButton *interNetSelectedBtn; // 选中互联网按钮
@property (weak, nonatomic) IBOutlet UIButton *marketSelectedBtn; // 选中市场按钮

@property (nonatomic , strong) UIButton *selectedStateBtn; // 选中的状态按钮 - 默认是动态的
#pragma mark ----------------UIVIEW---------------------

@property (nonatomic,strong) ICN_ActivityHeaderView * headerView;
@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@end

@implementation ICN_ThemeVC
#pragma mark - ---------- 懒加载 ----------
- (ICN_ActivityHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_ActivityHeaderView);
    }
    return _headerView;
}
#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
    //初始化设置界面
    [self configUI];
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
       
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark - ---------- 重写父类方法 ----------
- (void)configUI{
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, 160)];
    [headView addSubview:self.headerView];
    [self.headerView setFrame:CGRectMake(0, 0,SCREEN_WIDTH, 160)];
    [self.mainTableView setTableHeaderView:headView];
    self.headerView.dataSources = [@[@"banner",@"banner",@"banner"]mutableCopy];
}
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark --- 数据初始化 ---
#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark --- UITextFieldDelegate ---
#pragma mark --- UITableViewDataSource ---
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        ICN_LiveTableViewCell *cell = XIB(ICN_LiveTableViewCell);
        return cell;
    }else
    {
        ICN_ActivityFristTableViewCell *cell = XIB(ICN_ActivityFristTableViewCell);
        return cell;
    }
    return nil;
}

#pragma mark --- UITableViewDelegate ---
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
         return 134;
    }else
    {
        
        return 105;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        ICN_ActivityDetialViewController *MVC = [[ICN_ActivityDetialViewController alloc]init];
        [self.navigationController pushViewController:MVC animated:YES];
    }else
    {
        
    }

    
}


#pragma mark --- NSCopying ---






@end
