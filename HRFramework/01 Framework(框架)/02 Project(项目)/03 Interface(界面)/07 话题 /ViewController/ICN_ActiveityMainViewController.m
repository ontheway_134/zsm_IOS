//
//  ICN_ActiveityMainViewController.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActiveityMainViewController.h"
#import "ZJContentView.h"
#import "ZJScrollPageView.h"
#import "ICN_ThemeVC.h"
@interface ICN_ActiveityMainViewController ()<ZJScrollPageViewDelegate>
@property(weak, nonatomic)ZJScrollPageView *scrollPageView;
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;

@end

@implementation ICN_ActiveityMainViewController

#pragma mark - ---------- 懒加载 ----------

#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // 初始化
    [self setupSegmentView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---------- 重写属性合成器 ----------
#pragma mark - ---------- IBActions ----------
#pragma mark - ---------- 重写父类方法 ----------
#pragma mark - ---------- 公有方法 ----------
#pragma mark - ---------- 私有方法 ----------
#pragma mark 布局
-(void)config{
    
}
#pragma mark --- 数据初始化 ---
- (void)setupSegmentView
{
    
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    
    style.titleFont = [UIFont systemFontOfSize:14];
    style.scrollTitle = YES;
    /** 是否滚动标题 默认为YES 设置为NO的时候所有的标题将不会滚动, 并且宽度会平分 和系统的segment效果相似 */
    style.autoAdjustTitlesWidth = YES;
    
    style.segmentViewBounces = NO;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    
    style.scrollLineColor = RGB0X(0X009DFF);
    
    style.scrollLineHeight = 2;
    
    //标题一般状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.normalTitleColor = RGB0X(0X333333);
    //标题选中状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.selectedTitleColor = RGB0X(0X009DFF);
    //展示滚动条
    style.showLine = YES;
    
    style.segmentHeight = 44;
    
    
    __weak typeof(self) weakSelf = self;
    
    // 初始化
    CGRect scrollPageViewFrame = CGRectMake(0, 64.0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.0);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        __strong typeof(self) strongSelf = weakSelf;
        
        strongSelf.titles =  @[@"热门",
                               @"付费",
                               @"互联网",
                               @"市场营销",
                               
                               ];
        
        
        
        ZJScrollPageView *scrollPageView = [[ZJScrollPageView alloc] initWithFrame:scrollPageViewFrame segmentStyle:style titles:_titles parentViewController:strongSelf delegate:strongSelf];
        strongSelf.scrollPageView = scrollPageView;
        // 额外的按钮响应的block
        
        //        [strongSelf.scrollPageView setSelectedIndex:1 animated:true];
        
        strongSelf.scrollPageView.extraBtnOnClick = ^(UIButton *extraBtn){
            
            NSLog(@"点击了extraBtn");
            
        };
        [strongSelf.view addSubview:strongSelf.scrollPageView];
        
    });
    
    
}

#pragma mark --- UI布局 ---
#pragma mark --- 网络请求 ---
#pragma mark 列表数据网络请求
#pragma mark --- 设置计时器 ---
#pragma mark - ---------- 协议方法 ----------
#pragma mark ----------------ZJScrollPageViewDelegate---------------------
- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}
- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    ICN_ThemeVC *babyMVC = [ICN_ThemeVC new];
    if (!childVc) {
        childVc = babyMVC;
    }
    
    return childVc;
}
- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

@end
