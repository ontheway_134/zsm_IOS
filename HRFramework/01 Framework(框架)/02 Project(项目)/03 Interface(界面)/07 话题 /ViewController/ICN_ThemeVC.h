//
//  ICN_ThemeVC.h
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageView.h"
#import "ZJContentView.h"
#pragma mark - ---------- 动态枚举 ----------

/** 选中按钮类型枚举 */
typedef enum : NSUInteger {
    PRIVATE_HOT = 1002331, // 热门
    PRIVATE_PAY, // 付费精品 1002332
    PRIVATE_INTER, // 互联网 1002333
    PRIVATE_MARKET// 市场营销 1002334
} ActivityTYPE;


@interface ICN_ThemeVC : BaseViewController<ZJScrollPageViewChildVcDelegate>
@property(nonatomic)NSString *memberId;
@end
