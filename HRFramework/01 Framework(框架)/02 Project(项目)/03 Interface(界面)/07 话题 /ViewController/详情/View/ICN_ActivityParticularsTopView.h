//
//  ICN_ActivityParticularsTopView.h
//  ICan
//
//  Created by 那风__ on 16/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_ActivityParticularsTopModel;
@interface ICN_ActivityParticularsTopView : UIView
@property(nonatomic)UIImageView *picImageView;
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *rmbLabel;
@property(nonatomic)UILabel *numberLabel;
@property(nonatomic)UILabel *createdateLabel;

@property(nonatomic)ICN_ActivityParticularsTopModel *model;

@end
