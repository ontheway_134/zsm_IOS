//
//  ICN_ActivityParticularsLiveTableViewCell.m
//  ICan
//
//  Created by 那风__ on 16/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ActivityParticularsLiveTableViewCell.h"
#import "ICN_ActivityParticularsPlayCellModel.h"
@implementation ICN_ActivityParticularsLiveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
       
        _titleLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(13);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(13);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:13];
        
        UILabel *label = [[UILabel alloc]init];
        [self.contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(9);
            make.right.mas_equalTo(-9);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(14);
            make.height.mas_equalTo(1);
        }];
        label.backgroundColor = RGB(236, 236, 236);
        
        _memberlogoImage = [[UIImageView alloc]init];
        
        [self.contentView addSubview:_memberlogoImage];
        [_memberlogoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.width.mas_equalTo(35);
            make.height.mas_equalTo(35);
            
        }];
        _memberlogoImage.layer.cornerRadius =17.5;
        _memberlogoImage.layer.masksToBounds = YES;
        _nameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.height.mas_equalTo(13);
        }];
        _nameLabel.textColor = RGB0X(0X000000);
        _nameLabel.font = [UIFont systemFontOfSize:13];
        
        _companynameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_companynameLabel];
        [_companynameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_nameLabel.mas_right).offset(10);
            make.height.mas_equalTo(11);
        }];
        _companynameLabel.textColor = RGB0X(0X666666);
        _companynameLabel.font = [UIFont systemFontOfSize:11];
        
        UIImageView *image1 =[[UIImageView alloc]init];
        image1.image = [UIImage imageNamed:@"时间.png"];
        [self.contentView addSubview:image1];
        [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            
        }];
        
        _timeLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(image1.mas_right).offset(5);
            make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(11);
        }];
        
        
        _timeLabel.textColor = RGB0X(0X666666);
        _timeLabel.font = [UIFont systemFontOfSize:11];
        
        
        UILabel *label2 = [[UILabel alloc]init];
        [self.contentView addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_timeLabel.mas_bottom).offset(15);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(-0);
            make.height.mas_equalTo(4);
        }];
        label2.backgroundColor = RGB(236, 236, 236);

    }
    return self;
}


-(void)setModel:(ICN_ActivityParticularsPlayCellModel *)model{

    _titleLabel.text = model.title;
    [_memberlogoImage sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    _nameLabel.text = model.memberNick;
    _companynameLabel.text = [NSString stringWithFormat:@"%@ | %@",model.companyName,model.memberPosition];
    _timeLabel.text = model.time;

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
