//
//  ICN_ActivityParticularsViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//
//活动详情
#import "ICN_ActivityParticularsViewController.h"
#import "ICN_ActivityParticularsLiveViewController.h"
#import "ICN_ActivityParticularsPlayViewController.h"

#import "ICN_ActivityParticularsTopView.h"
#import "ICN_ActivityParticularsTopModel.h"
#import "BaseOptionalModel.h"

#import "ICN_ActivityParticularsDownModel.h"
#import "ICN_ActivityParticularsDownTableViewCell.h"
@class ICN_ICN_ActivityParticularsPlayModel;
#import "ICN_ActivityParticularsLiveTableViewCell.h"
#import "ICN_StartViewController.h"
#import "ALB_WeChatPay.h" // 微信支付单例
#import "ICN_AliPayManager.h" // 支付宝支付公开方法类



@interface ICN_ActivityParticularsViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *topDataArr;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic)NSString *activityClass;
@property (nonatomic,strong)NSMutableArray *activityArr;
@property(nonatomic)BOOL isok;
@property(nonatomic)NSString *free;
@property(nonatomic)UIButton *wxBtn;
@property(nonatomic)UIButton *apiBtn;
@property(nonatomic)UIView *view1;
@property(nonatomic)UIWindow *currentWindow;
@property(nonatomic)UIView *xuhuaView;
@property (strong,nonatomic)NSString * typeStr;
@property(nonnull)UIButton *btn;
@property(nonatomic)BaseOptionalModel *basemodel;
@end

@implementation ICN_ActivityParticularsViewController
- (NSMutableArray *)activityArr
{
    if (!_activityArr) {
        _activityArr = [NSMutableArray array];
    }
    return _activityArr;
}

#pragma mark - ---------- 生命周期 ----------


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    if (self.basemodel.code == 1) {
//         [self.btn setTitle:@"已报名" forState:UIControlStateNormal];
//    }else{
//       [self.btn setTitle:@"立即报名" forState:UIControlStateNormal];
//    }
    
    [self createUI];
//    [self datas];
}

-(void)viewWillAppear:(BOOL)animated{
    [self createUI];
}

-(void)createUI{

    
    self.naviTitle = @"精品活动";

    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 39) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

//    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        [self datas];
//    }];
    
    
//添加刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self datas];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    _tableView.mj_header = header;
    [_tableView.mj_header beginRefreshing];

    
    
    
    [self.view addSubview:_tableView];
    [_tableView registerClass:[ICN_ActivityParticularsDownTableViewCell class] forCellReuseIdentifier:@"cell"];

    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGB(16, 134, 254);
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
        make.height.mas_equalTo(39);
    }];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
   
    self.btn = [[UIButton alloc]init];
    [self.btn setImage:[UIImage imageNamed:@"立即报名.png"] forState:UIControlStateNormal];
    [self.btn setTitle:@"立即报名" forState:UIControlStateNormal];
    [self.btn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
    [self.btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.btn];
    [self.btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(39);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(0);
    }];
    self.btn.imageEdgeInsets = UIEdgeInsetsMake(0,-15, 0, 0);
    self.btn.titleLabel.font = [UIFont systemFontOfSize:14];
   
    
}

-(void)btnAction:(UIButton *)btn{
    //test=== 添加对于微信支付的预处理测试页面
//    [ALB_WeChatPay shareWeiChatPayWithActivityId:_url];
    //test=== 添加对于支付宝支付的测试预处理页面
    [ICN_AliPayManager AliPayWithActivityId:_url];
    if (![self getCurrentUserLoginStatus]) {
        ICN_StartViewController *vc = [[ICN_StartViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
    NSDictionary *dic = @{@"activityId":_url};
    if ([_free isEqualToString:@"0"]) {
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ActivityPay/ActivityPay" params:dic success:^(id result) {
            
            self.basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
            
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"报名成功"];
            [self.btn setTitle:@"已报名" forState:UIControlStateNormal];

            
            [self datas];
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }else{
       [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"功能暂未开通"];
//虚化view
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
//    view.backgroundColor = [UIColor blackColor];
//    view.alpha = 0.5;
//    view.tag = 111;
//    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
//
//    [currentWindow addSubview:view];
//
//    
//    //支付view
//    
//    UIView *view1 = [[UIView alloc]init];
//    view1.backgroundColor = RGB0X(0Xffffff);
//    view1.layer.cornerRadius = 5;
//    view1.tag = 112;
//    view1.layer.masksToBounds = YES;
//    [currentWindow addSubview:view1];
//    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.width.mas_equalTo(250);
//        make.height.mas_equalTo(193);
//
//        make.centerX.mas_equalTo(view);
//        make.top.mas_equalTo(150);
//    }];
//        _view1 = view1;
//        _currentWindow = currentWindow;
//    
//        _xuhuaView = view;
//    UILabel *label = [[UILabel alloc]init];
//    label.textColor = RGB0X(0X000000);
//    [view1 addSubview:label];
//    [label mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(20);
//        make.height.mas_equalTo(13);
//        make.centerX.mas_equalTo(view1).offset(10);
//    }];
//
//    label.text = [NSString stringWithFormat:@"¥%@",_free];
//        label.textColor = [UIColor redColor];
//        UILabel * labelfont = [[UILabel alloc]init];
//        labelfont.text = @"您需支付";
//        UILabel * labelback = [[UILabel alloc]init];
//        labelback.text = @"报名费";
//        [view1 addSubview:labelfont];
//        [view1 addSubview:labelback];
//        [labelfont mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(20);
//            make.height.mas_equalTo(13);
//        make.right.mas_equalTo(label.mas_left).offset(0);
//        }];
//        
//        [labelback mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.mas_equalTo(20);
//            make.height.mas_equalTo(13);
//            make.left.mas_equalTo(label.mas_right).offset(0);
//        }];
//        labelback.font = [UIFont systemFontOfSize:14];
//        labelfont.font = [UIFont systemFontOfSize:14];
//        
//        
//    label.font = [UIFont systemFontOfSize:13];
//    
//
//    UILabel *label2 = [[UILabel alloc]init];
//    label2.textColor = RGB0X(0X333333);
//    [view1 addSubview:label2];
//    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(20);
//        make.top.mas_equalTo(label.mas_bottom).offset(26);
//        make.height.mas_equalTo(12);
//        
//    }];
//    label2.text = @"支付方式";
//    label2.font = [UIFont systemFontOfSize:12];
//    
//    
//    UIButton *btn1 = [[UIButton alloc]init];
//    [btn1 setImage:[UIImage imageNamed:@"未选中.png"] forState:UIControlStateNormal];
//    [btn1 setImage:[UIImage imageNamed:@"退款理由选中.png"] forState:UIControlStateSelected];
//    [view1 addSubview:btn1];
//    [btn1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(label2.mas_bottom).offset(15);
//        make.left.mas_equalTo(20);
//        
//    }];
//        [btn1 addTarget:self action:@selector(btnzhifu:) forControlEvents:UIControlEventTouchUpInside];
//    
//        btn1.tag = 100;
//    UIImageView *imageV1 = [[UIImageView alloc]init];
//    imageV1.image = [UIImage imageNamed:@"微信支付.png"];
//    [view1 addSubview:imageV1];
//
//    [imageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(btn1.mas_right).offset(15);
//        make.top.mas_equalTo(label2.mas_bottom).offset(15);
//        
//    }];
//    UILabel *wxLabel = [[UILabel alloc]init];
//    wxLabel.textColor = RGB0X(0X333333);
//    [view1 addSubview:wxLabel];
//    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(imageV1.mas_right).offset(5);
//        make.top.mas_equalTo(label2.mas_bottom).offset(18);
//        make.height.mas_equalTo(12);
//    }];
//    wxLabel.text = @"微信";
//    wxLabel.font = [UIFont systemFontOfSize:12];
//
//    //支付宝
//    UILabel *zfbLabel =[[UILabel alloc]init];
//    zfbLabel.textColor = RGB0X(0X333333);
//    [view1 addSubview:zfbLabel];
//    [zfbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(-20);
//        make.top.mas_equalTo(label2.mas_bottom).offset(18);
//        make.height.mas_equalTo(12);
//        
//    }];
//    zfbLabel.text =@"支付宝";
//    zfbLabel.font = [UIFont systemFontOfSize:12];
//
//    
//    UIImageView *imageV2 = [[UIImageView alloc]init];
//    imageV2.image = [UIImage imageNamed:@"支付宝支付.png"];
//    [view1 addSubview:imageV2];
//    [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(zfbLabel.mas_left).offset(-5);
//        make.top.mas_equalTo(label2.mas_bottom).offset(15);
//    }];
//    
//    UIButton *btn2 = [[UIButton alloc]init];
//    [btn2 setImage:[UIImage imageNamed:@"未选中.png"] forState:UIControlStateNormal];
//        [btn2 setImage:[UIImage imageNamed:@"退款理由选中.png"] forState:UIControlStateSelected];
//    [view1 addSubview:btn2];
//    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(imageV2.mas_left).offset(-15);
//        make.top.mas_equalTo(label2.mas_bottom).offset(15);
//    }];
//    
//    [btn2 addTarget:self action:@selector(btnzhifu:) forControlEvents:UIControlEventTouchUpInside];
//        btn2.tag = 101;
//    UIButton *zfBtn = [[UIButton alloc]init];
//    [zfBtn setTitle:@"确认支付" forState:UIControlStateNormal];
//    [zfBtn setTintColor:RGB0X(0Xffffff)];
//    zfBtn.layer.cornerRadius = 5;
//    zfBtn.layer.masksToBounds = YES;
//    zfBtn.backgroundColor = RGB(16, 134, 254);
//    zfBtn.titleLabel.font = [UIFont systemFontOfSize:13];
//    [view1 addSubview:zfBtn];
//    [zfBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(20);
//        make.right.mas_equalTo(-20);
//        make.bottom.mas_equalTo(-23);
//        make.top.mas_equalTo(wxLabel.mas_bottom).offset(35);
//    }];
//        [zfBtn addTarget:self action:@selector(zfBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    
//        _wxBtn = btn1;
//        _apiBtn = btn2;
//    
//    UIButton *quxiaoBtn = [[UIButton alloc]init];
//    [quxiaoBtn setImage:[UIImage imageNamed:@"弹窗关闭.png"] forState:UIControlStateNormal];
//    [quxiaoBtn addTarget:self action:@selector(quxiaoBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [view addSubview:quxiaoBtn];
//    [quxiaoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(view1.mas_bottom).offset(20);
//        make.centerX.mas_equalTo(view1);
//        
//    }];
//    
    }
    /*默认情况使用微信进行支付*/
    _wxBtn.selected = NO;
    }
}
-(void)zfBtnAction:(UIButton *)btn{
    
    NSDictionary *dic = @{@"activityId":_url};
    
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ActivityPay/ActivityPay" params:dic success:^(id result) {
            
            [_view1 removeFromSuperview];
            [_currentWindow removeFromSuperview];
            [_xuhuaView removeFromSuperview];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"购买成功"];
            
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];


}
-(void)quxiaoBtn:(UIButton *)btn{
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    UIView *view = [currentWindow viewWithTag:111];
    [view removeFromSuperview];
    
    UIView *view2= [currentWindow viewWithTag:112];
    [view2 removeFromSuperview];



}
-(void)btnzhifu:(UIButton*)sender{
    NSMutableArray * tempBtnarr = [[NSMutableArray alloc]initWithObjects:_wxBtn,_apiBtn, nil];
    for (UIButton * btn1 in tempBtnarr) {
        if (btn1!= sender) {
            btn1.selected = NO;
        }
    }
    if (_wxBtn.selected == NO) {
        self.typeStr = @"1";
    }
    else if (_apiBtn.selected == NO){
        self.typeStr = @"2";
    }
    sender.selected = !sender.selected;
}
-(void)datas{
    
    NSDictionary *dic = @{@"id":self.url};
    

    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Activity/Activitydetail" params:dic success:^(id result) {
        NSDictionary *result1 = result[@"result"];
        NSDictionary *dic0 = result1[@"activitycontent"];
        _topDataArr = [[NSMutableArray alloc]init];
        //for (NSDictionary *dic in [dic0 allValues]) {
        ICN_ActivityParticularsTopModel *model = [[ICN_ActivityParticularsTopModel alloc]init];
        [model setValuesForKeysWithDictionary:dic0];
        //  [_topDataArr addObject:model];
        // }
        
        _free = model.price;
        
        ICN_ActivityParticularsTopView *view = [[ICN_ActivityParticularsTopView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 249)];
        
        _tableView.tableHeaderView = view;
        view.model = model;
        [_tableView.mj_header endRefreshing];
        [_tableView reloadData];
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
#warning 这里的网络请求与上面的相同，暂且先去掉。
//    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Activity/Activitydetail" params:dic success:^(id result) {
//        NSDictionary *result1 = result[@"result"];
//        NSArray *includecontent = result1[@"includecontent"];
//        _dataArr = [[NSMutableArray alloc]init];
//        
//        for (NSDictionary *dic in includecontent) {
//            ICN_ActivityParticularsDownModel *model = [[ICN_ActivityParticularsDownModel alloc]init];
//            [model setValuesForKeysWithDictionary:dic];
//            [_dataArr addObject:model];
//            [self.activityArr addObject:model.activityClass];
//            
//        }
//        [_tableView.mj_header endRefreshing];
//        [_tableView reloadData];
//        
//    } failure:^(NSDictionary *errorInfo) {
//        
//    }];
    

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _dataArr.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

       
        ICN_ActivityParticularsDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        ICN_ActivityParticularsDownModel *model = _dataArr[indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = model;
//        cell.model1 = model.activityClass;
        return cell;
   
    


}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%@",self.activityArr);
    NSString *str = self.activityArr[indexPath.row];
   
        if ([str isEqualToString:@"1"] ) {
            return 125;
        }else{
            
            return 104;
        }

    
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     NSString *str = self.activityArr[indexPath.row];
    if ([str isEqualToString:@"1"]) {
        //加上此句，返回时直接就是非选中状态。
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        ICN_ActivityParticularsPlayViewController *play = [[ICN_ActivityParticularsPlayViewController alloc]init];
        //ICN_ActivityParticularsLiveViewController *live = [[ICN_ActivityParticularsLiveViewController alloc]init];
        
        ICN_ActivityParticularsDownModel *model = _dataArr[indexPath.row];
        
        play.url = model.includeid;
        
        [self.navigationController pushViewController:play animated:YES];
    }else{
    
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        ICN_ActivityParticularsLiveViewController *live = [[ICN_ActivityParticularsLiveViewController alloc]init];
        //ICN_ActivityParticularsLiveViewController *live = [[ICN_ActivityParticularsLiveViewController alloc]init];
        
        ICN_ActivityParticularsDownModel *model = _dataArr[indexPath.row];
        
        live.url = model.includeid;
        
        [self.navigationController pushViewController:live animated:YES];
    
    
    
    }
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
