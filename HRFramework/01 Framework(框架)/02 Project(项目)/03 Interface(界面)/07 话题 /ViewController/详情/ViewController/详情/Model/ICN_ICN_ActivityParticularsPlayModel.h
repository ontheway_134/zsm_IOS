//
//  ICN_ICN_ActivityParticularsPlayModel.h
//  ICan
//
//  Created by 那风__ on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ICN_ActivityParticularsPlayModel : NSObject
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *memberLogo;
@property(nonatomic)NSString *memberNick;
@property(nonatomic)NSString *companyName;
@property(nonatomic)NSString *memberPosition;
@property(nonatomic)NSString *content;
@property(nonatomic)NSString *address;
@property(nonatomic)NSString *time;





@end
