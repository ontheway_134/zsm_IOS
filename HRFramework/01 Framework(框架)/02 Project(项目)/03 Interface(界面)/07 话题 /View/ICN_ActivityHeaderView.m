//
//  ICN_ActivityHeaderView.m
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ActivityHeaderView.h"
@interface ICN_ActivityHeaderView()<SDCycleScrollViewDelegate>
@end
@implementation ICN_ActivityHeaderView

-(void)setDataSources:(NSArray *)dataSources {
    
    _dataSources = dataSources;
    NSMutableArray *bannerArr = [NSMutableArray array];
    [_dataSources enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [bannerArr addObject:obj];
    }];
    [self layoutSubviewss:bannerArr];
    
    
}
-(void)layoutSubviewss:(NSArray *)arr {
    
    self.cycleScrollView.backgroundColor = [UIColor redColor];
    
    self.cycleScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 150) imageNamesGroup:arr];
    
    self.cycleScrollView.showPageControl = YES;
    
    self.cycleScrollView.placeholderImage = [UIImage imageNamed:@"default_Image"];
    
    self.cycleScrollView.delegate = self;
    
    self.cycleScrollView.autoScrollTimeInterval = 2.5;
    
    //设置轮播视图分也控件的位置
    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    self.cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    
    // 当前分页控件小圆标颜色
    UIImage *img = [UIImage imageNamed:@"banner选中"];
    self.cycleScrollView.currentPageDotImage = img;
    
    //其他分页控件小圆标颜色
    self.cycleScrollView.pageDotImage = [UIImage imageNamed:@"banner未选中"];
    
    
    
    [self addSubview:self.cycleScrollView];
    
}
#pragma mark - 轮播图代理方法
/** 点击图片回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    
    NSLog(@"%ld",index);
}

/** 图片滚动回调 */
- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didScrollToIndex:(NSInteger)index{
    
    
}


@end
