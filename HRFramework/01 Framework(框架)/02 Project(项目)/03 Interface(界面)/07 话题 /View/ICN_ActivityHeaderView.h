//
//  ICN_ActivityHeaderView.h
//  ICan
//
//  Created by 何壮壮 on 17/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDCycleScrollView/SDCycleScrollView.h>
@interface ICN_ActivityHeaderView : UIView
@property (strong, nonatomic) NSArray *dataSources;
@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;
@end
