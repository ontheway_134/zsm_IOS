//
//  KMDatePickerDateModel.h
//  KMDatePicker
//
//  Created by Lym on 16/8/11.
//  Copyright © 2015年 Kenmu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMDatePickerDateModel : NSObject
@property (nonatomic, copy) NSString *year;
@property (nonatomic, copy) NSString *month;
@property (nonatomic, copy) NSString *day;
@property (nonatomic, copy) NSString *hour;
@property (nonatomic, copy) NSString *minute;
@property (nonatomic, copy) NSString *weekdayName;

- (instancetype)initWithDate:(NSDate *)date;

@end
