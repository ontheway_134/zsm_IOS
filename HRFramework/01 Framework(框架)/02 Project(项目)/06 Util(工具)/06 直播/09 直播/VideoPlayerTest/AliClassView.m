//
//  AliClassView.m
//  ICan
//
//  Created by 王凯 on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AliClassView.h"
#import "ICN_LiveiQuesDetialViewController.h"
#pragma mark ----------------CELL---------------------
#import "AliMessageViewCell.h"
#import "ICN_QuestionsTableViewCell.h"
#import "ICN_LiveDetialTableViewCell.h"
#import "ICN_LiveDetialBeginTableViewCell.h"
#import "ICN_QuestionsHeaderView.h"
#include "UIView+Responder.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#pragma mark ----------------MODEL---------------------
#import "ICN_LiveQuestionsModel.h"
#import "MJDIYAutoFooter.h"
#import "ICN_liveModel.h"
#define NICK_NAME @"nickname"

enum {
    InputSourceForDetial,
    InputSourceForQuestion,
    InputSourceForChatRooms
};

typedef NSInteger InputSource;



@interface AliClassView()<UITableViewDelegate, UITableViewDataSource,EMChatroomManagerDelegate,EMChatManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView      *inputView;
@property (weak, nonatomic) IBOutlet UIButton    *detailBtn;
@property (weak, nonatomic) IBOutlet UIButton    *questionBtn;
@property (weak, nonatomic) IBOutlet UIButton    *liveBtn;
@property (weak, nonatomic) IBOutlet UITextField *inputTF;
@property (weak, nonatomic) IBOutlet UITableView *liveTextTableV;

@property (readwrite, nonatomic) InputSource inputSource;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic,strong) ICN_QuestionsHeaderView * headerView;
@property (nonatomic,strong) NSMutableArray* dataSourceArr;
@property (nonatomic,strong) NSMutableArray* dataSourceQuestionsArr;
@property (nonatomic,strong) NSString * quizCount;


@property (nonatomic , strong)NSString *currentPage; /* 当前页 **/
@end

@implementation AliClassView
/* 合成访问器方法 **/
@synthesize messageSendSource;
@synthesize inputSource;
@synthesize messagePool;
@synthesize roomsID;
@synthesize nickName;



static NSString *const MessageIdentifier = @"AliMessageViewCell";
static NSTimeInterval const reloadMessagePoolTime = 2; // 消息缓冲池刷新时间

#pragma mark ----------------懒加载---------------------
- (NSMutableArray *)dataSourceArr
{
    if (!_dataSourceArr) {
        _dataSourceArr = [NSMutableArray array];
    }
    return _dataSourceArr;
}
- (ICN_QuestionsHeaderView *)headerView
{
    if (!_headerView) {
        _headerView = XIB(ICN_QuestionsHeaderView);
    }
    return _headerView;
}
- (NSMutableArray *)dataSourceQuestionsArr
{
    if (!_dataSourceQuestionsArr) {
        _dataSourceQuestionsArr = [NSMutableArray array];
    }
    return _dataSourceQuestionsArr;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setInputSource:InputSourceForDetial];
    [self configProperty];
    [self configUI];
    
    [NOTIFICATION addObserver:self selector:@selector(dotNotfication) name:@"点赞" object:nil];
    
    [IQKeyboardManager sharedManager].enable = NO;
}

/**
 点赞通知
 */
- (void)dotNotfication
{
    self.currentPage = @"1";
    NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    [[HRRequest alloc] POST_PATH:PATH_activityQuizList params:@{@"token":token,@"activityId":self.liveID,@"page":self.currentPage} success:^(id result) {
        //如果成功结束刷新
        [self endRefesh];
        if ([result[@"code"]integerValue] == 0) {
            NSArray *data = result[@"result"];
            self.quizCount = result[@"count"];
            self.dataSourceQuestionsArr = nil;
            if (data.count > 0) {
                [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    ICN_LiveQuestionsModel *model = [[ICN_LiveQuestionsModel alloc] initWithDictionary:obj error:nil];
                    [self.dataSourceQuestionsArr addObject:model];
                }];
            }
            
        }
        [self.liveTextTableV reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"%@",errorInfo);
        //如果失败结束刷新
        [self endRefesh];
        
    }];

}
#pragma mark - 配置属性
- (void)configProperty {
    //    self.timer = [NSTimer scheduledTimerWithTimeInterval:reloadMessagePoolTime target:self selector:@selector(reloadMessage) userInfo:nil repeats:YES];
    [self.liveTextTableV setDelegate:self];
    [self.liveTextTableV setDataSource:self];
    [self.liveTextTableV registerNib:[UINib nibWithNibName:MessageIdentifier bundle:nil] forCellReuseIdentifier:MessageIdentifier];
    //创建消息缓冲池
    messagePool = [NSMutableArray array];
    //EMChatroomManagerDelegate
    //注册聊天室回调
    [[EMClient sharedClient].roomManager addDelegate:self delegateQueue:nil];
    //消息回调:EMChatManagerDelegate
    //注册消息回调
    [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    
}
- (void)setLiveID:(NSString *)liveID
{
    _liveID = liveID;
    [[HRRequest manager]POST:PATH_activityDetail para:@{@"id":_liveID} success:^(id data) {
        ICN_liveModel *model = [[ICN_liveModel alloc]initWithDictionary:data error:nil];
        NSLog(@"%@",model.memberPosition);
        self.dataSourceArr = nil;
        [self.dataSourceArr addObject:model];
        [self.liveTextTableV reloadData];
        
    } faiulre:^(NSString *errMsg) {
        NSLog(@"%@",errMsg);
    }];
    
    
    
    
}
#pragma mark - 配置UI
- (void)configUI {
    [self.inputView.layer addSublayer:[self addLine]];
    [self.questionBtn.layer addSublayer:[self addLeftLine]];
    [self.questionBtn.layer addSublayer:[self addRightLine]];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.001)];
    view.backgroundColor = [UIColor redColor];
    self.liveTextTableV.tableHeaderView = view;
    
    self.detailBtn.selected = YES;
    [self.questionBtn setSelected:NO];
    [self.liveBtn setSelected:NO];
    [self.inputView setHidden:YES];
    [self.liveTextTableV setHidden:NO];
    [self setInputSource:InputSourceForDetial];
    
    
}

#pragma mark - layer 添加分割线

- (CALayer *)addLine {
    CALayer *layer = [CALayer new];
    [layer setBackgroundColor:RGB0X(0xb6b6b6).CGColor];
    [layer setFrame:CGRectMake(0, 0, self.width, 0.5)];
    return layer;
}
- (CALayer *)addLeftLine {
    CALayer *layer = [CALayer new];
    [layer setBackgroundColor:RGB0X(0xe4e4e4).CGColor];
    [layer setFrame:CGRectMake(0, 10, 0.5, 20)];
    return layer;
}
- (CALayer *)addRightLine {
    CALayer *layer = [CALayer new];
    [layer setBackgroundColor:RGB0X(0xe4e4e4).CGColor];
    [layer setFrame:CGRectMake(self.questionBtn.width, 10, 0.5, 20)];
    return layer;
}

#pragma mark - 详情
- (IBAction)detailAction:(UIButton *)sender {
    [sender setSelected:YES];
    self.liveTextTableV.mj_header = nil;
    self.liveTextTableV.mj_footer = nil;
    [self.questionBtn setSelected:NO];
    [self.liveBtn setSelected:NO];
    [self.inputView setHidden:YES];
    [self.liveTextTableV setHidden:NO];
    [self setInputSource:InputSourceForDetial];
    [self.liveTextTableV reloadData];
}
#pragma mark - 提问区
- (IBAction)questionAction:(UIButton *)sender {
    //提问区列表接口
    
    [self netWorkRequest:TYPE_RELOADDATA_DOWM];
    //提问列表下拉刷新
//    self.liveTextTableV.mj_header = [HSGifHeader headerWithRefreshingBlock:^{
//        [self netWorkRequest:TYPE_RELOADDATA_DOWM];
//    }];
    //提问列表上拉刷新
    self.liveTextTableV.mj_footer = [MJDIYAutoFooter footerWithRefreshingBlock:^{
        [self netWorkRequest:TYPE_RELOADDATA_UP];
    }];
    
    [sender setSelected:YES];
    [self.detailBtn setSelected:NO];
    [self.liveBtn setSelected:NO];
    [self.inputView setHidden:NO];
    [self.liveTextTableV setHidden:NO];
    [self setInputSource:InputSourceForQuestion];
    [self.liveTextTableV reloadData];
}
- (void)endRefesh
{
    [self.liveTextTableV.mj_header endRefreshing];
    [self.liveTextTableV.mj_footer endRefreshing];
}

/**
 列表页数据请求
 
 @param isMore 是否加载更多
 */
- (void)netWorkRequest:(BOOL)isMore
{
    if (isMore) {
        self.currentPage = SF(@"%ld",[self.currentPage integerValue] + 1);
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        [[HRRequest alloc] POST_PATH:PATH_activityQuizList params:@{@"token":token,@"activityId":self.liveID,@"page":self.currentPage} success:^(id result) {
            //如果成功结束刷新
            
            [self endRefesh];
            if ([result[@"code"]integerValue] == 0) {
                NSArray *data = result[@"result"];
                if (data.count > 0) {
                    self.quizCount = result[@"count"];
                    self.dataSourceQuestionsArr = nil;
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_LiveQuestionsModel *model = [[ICN_LiveQuestionsModel alloc]initWithDictionary:obj error:nil];
                        [self.dataSourceQuestionsArr addObject:model];
                    }];

                }
                
                
                
            }
            [self.liveTextTableV reloadData];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"%@",errorInfo);
            //如果失败结束刷新
            [self endRefesh];
            //如果失败page-1
            self.currentPage = @"1";
        }];
        
    }else{
        self.currentPage = @"1";
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        [[HRRequest alloc] POST_PATH:PATH_activityQuizList params:@{@"token":token,@"activityId":self.liveID,@"page":self.currentPage} success:^(id result) {
            //如果成功结束刷新
            [self endRefesh];
            if ([result[@"code"]integerValue] == 0) {
                NSArray *data = result[@"result"];
                self.quizCount = result[@"count"];
                self.dataSourceQuestionsArr = nil;
                if (data.count >0) {
                    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        ICN_LiveQuestionsModel *model = [[ICN_LiveQuestionsModel alloc]initWithDictionary:obj error:nil];
                        [self.dataSourceQuestionsArr addObject:model];
                    }];
                }
                
            }
            [self.liveTextTableV reloadData];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"%@",errorInfo);
            //如果失败结束刷新
            [self endRefesh];
            
        }];
        
    }
    
}
#pragma mark - 直播间
- (IBAction)liveAction:(UIButton *)sender {
    self.liveTextTableV.mj_header = nil;
    self.liveTextTableV.mj_footer = nil;
    [sender setSelected:YES];
    [self.detailBtn setSelected:NO];
    [self.questionBtn setSelected:NO];
    [self.inputView setHidden:NO];
    [self.liveTextTableV setHidden:NO];
    [self setInputSource:InputSourceForChatRooms];
    [self.liveTextTableV reloadData];
}


- (void)joinRooms:(NSString *)roomID {
    [[EMClient sharedClient].roomManager joinChatroom:roomID completion:^(EMChatroom *aChatroom, EMError *aError) {
        if(!aError) {
            NSLog(@"成功进入聊天室");
        }else{
            NSLog(@"error:%@", aError.errorDescription);
        }
    }];
}

- (void)exitRooms:(NSString *)roomID {
    [[EMClient sharedClient].roomManager leaveChatroom:roomID completion:^(EMChatroom *aChatroom, EMError *aError) {
        if(!aError) {
            NSLog(@"成功退出聊天室");
        }else{
            NSLog(@"error:%@", aError.errorDescription);
        }
    }];
}

#pragma mark - 发送消息&提问
- (IBAction)sendMessageAction:(UIButton *)sender {
    
    if ([self.inputTF.text isEqualToString:@""] || self.inputTF.text == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self Message:@"请输入内容"];
        return;
    }

    if(messageSendSource == AliMessageSendSourceForLive) {
        if(inputSource == InputSourceForChatRooms) {
            NSLog(@"推流--聊天室");
            [self sendMessage];
            NSDictionary *body = @{@"nickname":nickName, @"message":self.inputTF.text};
            @synchronized (messagePool) {
                [messagePool addObject:body];
                [self reloadMessage];
            }
            
        }else{ // InputSourceForQuestion
            
        }
        
    }
    if(messageSendSource == AliMessageSendSourceForPlay) {
        if(inputSource == InputSourceForChatRooms) {
            NSLog(@"拉流--聊天室");
            [self sendMessage];
            NSDictionary *body = @{@"nickname":nickName, @"message":self.inputTF.text};
            @synchronized (messagePool) {
                [messagePool addObject:body];
                [self reloadMessage];
            }
            
        }else{ // InputSourceForQuestion
            
        }
    }
    
    if (inputSource == 1) {
        if ([self.inputTF.text isEqualToString:@""] || self.inputTF.text == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self Message:@"请输入内容"];
            return;
        }
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        [[HRRequest manager]POST:PATH_addQuiz para:@{@"tolen":token, @"activityId":self.liveID,@"quizId":@"0",@"content":self.inputTF.text} success:^(id data) {
             self.inputTF.text = nil;
            [MBProgressHUD ShowProgressWithBaseView:self Message:@"提问成功"];
            //提问成功再次刷新列表
            [self netWorkRequest:NO];
        } faiulre:^(NSString *errMsg) {
             self.inputTF.text = nil;
        }];
    }
    self.inputTF.text = nil;
}

#pragma mark - 刷新消息缓冲池数据
- (void)reloadMessage {
    //if(messagePool.count > 0) {
        
        [self.liveTextTableV reloadData];
//        if (messagePool.count >1) {
//            [self.liveTextTableV selectRowAtIndexPath:[NSIndexPath indexPathForRow:messagePool.count-1 inSection:0]
//                                             animated:YES
//                                       scrollPosition:UITableViewScrollPositionBottom];
//        }
        
        
      
  //  }
}

#pragma mark - UITableView ---- Delegate -- DataSource ----
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (inputSource == 1) {
        return self.dataSourceQuestionsArr.count;
    }else if(inputSource == 0){
        return self.dataSourceArr.count;
    }
    else{
        return messagePool.count;
    }
}

/**
 
 
 @param tableView <#tableView description#>
 @param indexPath <#indexPath description#>
 @return <#return value description#>
 */
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    tableView.rowHeight = self.liveTextTableV.rowHeight;
    return self.liveTextTableV.rowHeight;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (inputSource == 1) {
        UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH,40)];
        [headView addSubview:self.headerView];
        [self.headerView setFrame:CGRectMake(0, 0,SCREEN_WIDTH, 40)];
        if (self.quizCount == nil) {
            self.quizCount = @"0";
        }
        self.headerView.countLabel.text = SF(@"%@个提问",self.quizCount);
        
        return self.headerView;
    }else{
        return nil;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (inputSource == 1) {
        return 50;
    }else{
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (inputSource == 1) {
        ICN_LiveQuestionsModel *model = self.dataSourceQuestionsArr[indexPath.row];
        ICN_QuestionsTableViewCell *cell = XIB(ICN_QuestionsTableViewCell);
        //点赞block  牛逼了
        
        [cell ReturndotAction:^{
            //PATH_doPraise
            NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            [[HRRequest manager]POST_PATH:PATH_doPraise params:@{@"token":token,@"id":model.quesID} success:^(id result) {
                [self netWorkRequest:NO];
                [self.liveTextTableV reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            } failure:^(NSDictionary *errorInfo) {
                
            }];
        }];
        
        //评论block
        [cell ReturnCommentAction:^{
            
            ICN_LiveiQuesDetialViewController *MVC = [[ICN_LiveiQuesDetialViewController alloc]init];
            MVC.quesID = model.quesID;
            
            [self.viewController.navigationController pushViewController:MVC animated:YES];
            
        }];
        cell.model = model;
        return cell;
    }else if(inputSource == 0){
        
        ICN_LiveDetialBeginTableViewCell *cell = XIB(ICN_LiveDetialBeginTableViewCell);
        cell.model = self.dataSourceArr[indexPath.row];
        return cell;
    }
    else{
        AliMessageViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MessageIdentifier];
        if(!cell) {
            cell = [[AliMessageViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MessageIdentifier];
        }
        //direction
        NSDictionary *dic = messagePool[indexPath.row];
        NSString *direction = dic[@"direction"];
        [self setLabelSpace:cell.name withValue:[NSString stringWithFormat:@"%@:%@", messagePool[indexPath.row][@"nickname"],messagePool[indexPath.row][@"message"]] withFont:[UIFont systemFontOfSize:13] len:[messagePool[indexPath.row][@"nickname"] length]+1 direction:[direction integerValue]];
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (inputSource == 1) {
        ICN_LiveQuestionsModel *quesModel = self.dataSourceQuestionsArr[indexPath.row];
        ICN_LiveiQuesDetialViewController *MVC = [[ICN_LiveiQuesDetialViewController alloc]init];
        MVC.quesID = quesModel.quesID;
        
        [self.viewController.navigationController pushViewController:MVC animated:YES];
        
    }else if(inputSource == 0){
        
    }
    else{
        
    }
    
}
#pragma mark - 回收键盘
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}


#pragma mark - 工具区 - 设置文字行间距/字间距
-(void)setLabelSpace:(UILabel*)label withValue:(NSString*)str withFont:(UIFont*)font len:(NSInteger )len direction:(NSInteger)direction  {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = 3;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.5f
                          };
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:str attributes:dic];
    if (direction == 0) {
         [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0,len)];
    }else{
         [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(0,len)];
    }
   
    [attributeStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13.0f] range:NSMakeRange(0,len)];
    label.attributedText = attributeStr;
}

#pragma mark - 收到消息的回调
- (void)didReceiveMessages:(NSArray *)aMessages
{
    for (EMMessage *message in aMessages) {
        EMMessageBody *msgBody = message.body;
        switch (msgBody.type) {
            case EMMessageBodyTypeText:
            {
                EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
                NSString *txt = textBody.text;
                NSLog(@"收到的文字是 txt -- %@",txt);
                NSString *extMsg = message.ext[@"nickname"];
                NSDictionary *body;
                if (!(extMsg == nil)) {
                    body = @{@"nickname":extMsg, @"message":txt,@"direction":SF(@"%d",message.direction)};
                    [messagePool addObject:body];

                }else{
                    body = @{@"nickname":@"匿名", @"message":txt,@"direction":SF(@"%d",message.direction)};
                    [messagePool addObject:body];
                    }
                //@synchronized (messagePool) {
                   // [messagePool addObject:body];
                    [self reloadMessage];
               // }

                }
        
        }
                break;
                
                
        }
    
}

#pragma mark - 构建消息
- (EMMessage *)buildMessage:(NSString *)msg conversationID:(NSString *)ID to:(NSString *)to {
    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:msg];
    NSString *from = [[EMClient sharedClient] currentUsername];
    EMMessage *message = [[EMMessage alloc] initWithConversationID:ID from:from to:to body:body ext:@{NICK_NAME:nickName}];
    message.chatType = EMChatTypeChatRoom;
    return message;
}
#pragma mark - 发送消息
- (void)sendMessage {
    if ([self.inputTF.text isEqualToString:@""] || self.inputTF.text == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self Message:@"请输入内容"];
        return;
    }
    EMMessage *message = [self buildMessage:self.inputTF.text conversationID:roomsID to:roomsID];
    [[EMClient sharedClient].chatManager importMessages:@[message] completion:^(EMError *aError) {}];
    [[EMClient sharedClient].chatManager updateMessage:message completion:^(EMMessage *aMessage, EMError *aError) {}];
    [[EMClient sharedClient].chatManager sendMessage:message progress:nil completion:^(EMMessage *aMessage, EMError *aError) {
        
        if(!aError) {
            NSLog(@"发送成功");
        }else {
            NSLog(@"error:%@", aError.errorDescription);
        }
    }];
}

-(void)dealloc {
    //移除；聊天室回调
    [[EMClient sharedClient].roomManager removeDelegate:self];
    //移除消息回调
    [[EMClient sharedClient].chatManager removeDelegate:self];
    [messagePool removeAllObjects];
     [NOTIFICATION removeObserver:self name:@"点赞" object:nil];
}


@end
