//
//  AliClassView.h
//  ICan
//
//  Created by 王凯 on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

enum {
    AliMessageSendSourceForLive,   // 推流
    AliMessageSendSourceForPlay,   // 拉流
    
};
typedef NSInteger ICAliMessageSendSource;

#import <UIKit/UIKit.h>




@interface AliClassView : UIView
/* 直播ID **/
@property (nonatomic,strong) NSString * liveID;
/* 活动ID **/
@property (nonatomic,strong) NSString * actID;
/** Description:消息输入源 */
@property (readwrite, nonatomic) ICAliMessageSendSource messageSendSource;
/** Description:消息缓冲池 */
@property (strong, nonatomic) NSMutableArray *messagePool;
/** Description:聊天室ID */
@property (copy, nonatomic) NSString *roomsID;
/** Description:自身昵称 */
@property (copy, nonatomic) NSString *nickName;
/** 加入聊天室 */
- (void)joinRooms:(NSString *)roomID;
/** 退出聊天室 */
- (void)exitRooms:(NSString *)roomID;

@end
