//
//  IXLiveClent.m
//  IXLiveVideoTest
//
//  Created by 王凯 on 2017/2/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#define WKRootVC [UIApplication sharedApplication].keyWindow.rootViewController

#import "IXLiveClent.h"


@implementation IXLiveClent

+ (instancetype)sharedClient {
    static dispatch_once_t once;
    static id __singleton__;
    dispatch_once(&once, ^{__singleton__ = [[self alloc] init];});
    return __singleton__;
}



- (void)initializeLiveConfigWithUrl:(NSString *)url {
    
    AlivcLConfiguration *configuration = [[AlivcLConfiguration alloc] init];
    _configuration = configuration;
    configuration.url = url;
    configuration.videoMaxBitRate = 1500 * 1000;
    configuration.videoBitRate = 600 * 1000;
    configuration.videoMinBitRate = 400 * 1000;
    configuration.audioBitRate = 64 * 1000;
    configuration.videoSize = CGSizeMake(360, 640);// 横屏状态宽高不需要互换
    configuration.fps = 20;
    configuration.preset = AVCaptureSessionPresetiFrame1280x720;
    configuration.screenOrientation = NO;
    configuration.reconnectTimeout = 5;
    // 水印
    configuration.waterMaskImage = [UIImage imageNamed:@"watermask"];
    configuration.waterMaskLocation = 0;
    configuration.waterMaskMarginX = 10;
    configuration.waterMaskMarginY = 10;
    if (_currentPosition) {
        configuration.position = _currentPosition;
    } else {
        configuration.position = AVCaptureDevicePositionFront;
        _currentPosition = AVCaptureDevicePositionFront;
    }
    
    NSLog(@"版本号:%@", [AlivcLiveSession alivcLiveVideoVersion]);
    
    [self startliveSession:configuration];
}

- (void)startliveSession:(AlivcLConfiguration *)configuration {
    AlivcLiveSession *liveSession = [[AlivcLiveSession alloc] initWithConfiguration:configuration];
    _liveSession = liveSession;
    liveSession.delegate = self;
    liveSession.enableMute = NO;
    [liveSession alivcLiveVideoStartPreview];
    [liveSession alivcLiveVideoUpdateConfiguration:^(AlivcLConfiguration *configuration) {
        configuration.videoMaxBitRate = 1500 * 1000;
        configuration.videoBitRate = 600 * 1000;
        configuration.videoMinBitRate = 400 * 1000;
        configuration.audioBitRate = 64 * 1000;
        configuration.fps = 20;
    }];
    [liveSession alivcLiveVideoConnectServer];
    
}

- (void)stopLiveSession {
    
    [self.liveSession alivcLiveVideoDisconnectServer];
    [self.liveSession alivcLiveVideoStopPreview];
    [self.liveSession.previewView removeFromSuperview];
    self.liveSession = nil;
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session error:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSString *msg = [NSString stringWithFormat:@"%zd %@",error.code, error.localizedDescription];
//        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Live Error" message:msg preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction *action = [UIAlertAction actionWithTitle:@"重新连接" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self.liveSession alivcLiveVideoConnectServer];
//        }];
//        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            [self.liveSession alivcLiveVideoDisconnectServer];
//        }];
//        [alert addAction:action];
//        [alert addAction:actionCancel];
//        
//        [self.vc presentViewController:alert animated:YES completion:nil];
    });
    
    NSLog(@"!!!error : %@", error);
}

- (void)alivcLiveVideoReconnectTimeout:(AlivcLiveSession*)session {
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        [self alertTitle:@"提示" msg:@"重连超时（此处根据实际情况决定，默认重连时长5s，可更改，建议开发者在此处重连）"];
    });
    
}

- (void)alivcLiveVideoLiveSessionConnectSuccess:(AlivcLiveSession *)session {
    
    NSLog(@"connect success!");
}


- (void)alivcLiveVideoLiveSessionNetworkSlow:(AlivcLiveSession *)session{
    // 注意：一定要套 主线程 完成UI操作
    dispatch_async(dispatch_get_main_queue(), ^{
//        self.textView.text = @"网速太慢";
    });
}


- (void)alivcLiveVideoOpenAudioSuccess:(AlivcLiveSession *)session {
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self alertTitle:@"YES" msg:@"麦克风打开成功"];
//    });
}

- (void)alivcLiveVideoOpenVideoSuccess:(AlivcLiveSession *)session {
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self alertTitle:@"YES" msg:@"摄像头打开成功"];
//    });
}


- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session openAudioError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self alertTitle:@"Error" msg:@"麦克风获取失败"];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session openVideoError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self alertTitle:@"Error" msg:@"摄像头获取失败"];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session encodeAudioError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self alertTitle:@"Error" msg:@"音频编码初始化失败"];
    });
    
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session encodeVideoError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self alertTitle:@"Error" msg:@"视频编码初始化失败"];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session bitrateStatusChange:(ALIVC_LIVE_BITRATE_STATUS)bitrateStatus {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"升降码率:%ld", bitrateStatus);
    });
}


- (void)alertTitle:(NSString *)title msg:(NSString *)msg {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:action];
    
    [self.vc presentViewController:alert animated:YES completion:nil];
    
}


@end
