//
//  IXLiveClent.h
//  IXLiveVideoTest
//
//  Created by 王凯 on 2017/2/8.
//  Copyright © 2017年 王凯. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AlivcLiveVideo/AlivcLiveVideo.h>

@interface IXLiveClent : NSObject <AlivcLiveSessionDelegate>

/** Description:推流配置 */
@property (strong, nonatomic) AlivcLConfiguration *configuration;
/** Description:直播管理 */
@property (strong, nonatomic) AlivcLiveSession *liveSession;
/** Description:控制器 */
@property (strong, nonatomic) UIViewController *vc;

@property (nonatomic, assign) AVCaptureDevicePosition currentPosition;

+ (instancetype)sharedClient;

- (void)initializeLiveConfigWithUrl:(NSString *)url;

- (void)startliveSession:(AlivcLConfiguration *)configuration;

- (void)stopLiveSession;

- (void)alertTitle:(NSString *)title msg:(NSString *)msg;


@end
