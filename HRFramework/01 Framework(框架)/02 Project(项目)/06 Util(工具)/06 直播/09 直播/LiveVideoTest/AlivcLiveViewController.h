//
//  AlivcLiveViewController.h
//  DevAlivcLiveVideo
//
//  Created by lyz on 16/3/21.
//  Copyright © 2016年 Alivc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"

@interface AlivcLiveViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic, assign) BOOL isScreenHorizontal;
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil url:(NSString *)url;
//直播id
@property (nonatomic,strong) NSString * liveID;
@end
