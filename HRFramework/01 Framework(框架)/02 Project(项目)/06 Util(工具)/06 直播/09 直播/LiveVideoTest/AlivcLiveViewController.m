//
//  AlivcLiveViewController.m
//  DevAlivcLiveVideo
//
//  Created by yly on 16/3/21.
//  Copyright © 2016年 Alivc. All rights reserved.
//


/**
 *  杭州短趣传媒网络技术有限公司
 *  POWERED BY QUPAI
 */

#import "AlivcLiveViewController.h"
#import <AlivcLiveVideo/AlivcLiveVideo.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>
#import "IXLiveClent.h"
#import "AliClassView.h"
#import "ICN_MyPersonalHeaderModel.h"
//static NSString *const PATH_getIndexInfo = @"Member/Member/getIndexInfo";
@interface AlivcLiveViewController ()<AlivcLiveSessionDelegate>

@property (nonatomic, strong) CTCallCenter *callCenter;
@property (nonatomic, strong) AlivcLiveSession *liveSession;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) AVCaptureDevicePosition currentPosition;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSFileHandle *handle;
@property (nonatomic, strong) NSMutableArray *logArray;
@property (weak, nonatomic) IBOutlet UIButton *muteButton;
@property (nonatomic, strong) AliClassView *aliClassView; // 内容View
@property (nonatomic, strong) UIView *maskView; // 黑色遮罩


@property (nonatomic,strong) NSString * roomID; //房间id

@end

@implementation AlivcLiveViewController {
    
    NSUInteger _last;
    CGFloat _lastPinchDistance;
    BOOL _isCTCallStateDisconnected;
}

@synthesize maskView;
@synthesize aliClassView;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil url:(NSString *)url{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    _url = url;
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [aliClassView exitRooms:self.roomID];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.textView setHidden:YES];
    _logArray = [NSMutableArray array];
    
    self.title = @"我的直播";
    [self leftImageItem:@"返回" action:^{
        
        [_handle closeFile];
        [self destroySession];
        [_timer invalidate];
        _timer = nil;
        aliClassView = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];

    }];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appResignActive) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture:)];
    [self.view addGestureRecognizer:gesture];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGesture:)];
    [self.view addGestureRecognizer:pinch];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeUpdate) userInfo:nil repeats:YES];
    
    [self testPushCapture];
    
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"log.txt"];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil];
    _handle = [NSFileHandle fileHandleForWritingAtPath:path];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(test) name:@"kaishichonglian" object:nil];
}

- (void)test {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Hi" message:@"开始重连" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    });
}

- (void)timeUpdate{
    AlivcLDebugInfo *i = [self.liveSession dumpDebugInfo];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:i.connectStatusChangeTime];
    
    NSMutableString *msg = [[NSMutableString alloc] init];
    [msg appendFormat:@"周期性延迟：(%0.2fms)\n",i.cycleDelay];
    [msg appendFormat:@"当前码率：(%zd)\n当前buffer视频帧数：(%zd)\n",[self.liveSession alivcLiveVideoBitRate] ,self.liveSession.dumpDebugInfo.localBufferVideoCount];
    [msg appendFormat:@"所有编码帧数：(%zd)\n所有发送帧数：(%zd)\n",i.encodeFrameCount, i.pushFrameCount];
    [msg appendFormat:@"当前编码帧数：(%0.2ffps)\n编码速度：(%0.2fKB/s)\n当前上传速度：(%0.2fKB/s)\n", i.fps,i.encodeSpeed, i.speed/1024];
    [msg appendFormat:@"本地buffer大小：(%lluB)\n当前上传数据大小(%lluB)\n连接状态(%zd)\n %@\n",i.localBufferSize, i.pushSize, i.connectStatus, date];
    [msg appendFormat:@"编码耗时:(%0.2fms)\n",i.localDelay];
    [msg appendFormat:@"当前输出流video:(%zd)\n当前输出流audio:(%zd)\n", i.currentVideoPTS,i.currentAudioPTS];
    [msg appendFormat:@"当前编码帧数:(%f)\n", i.fps];
    
    _textView.text = msg;
    
}



- (void)tapGesture:(UITapGestureRecognizer *)gesture{
    CGPoint point = [gesture locationInView:self.view];
    CGPoint percentPoint = CGPointZero;
    percentPoint.x = point.x / CGRectGetWidth(self.view.bounds);
    percentPoint.y = point.y / CGRectGetHeight(self.view.bounds);
    [self.liveSession alivcLiveVideoFocusAtAdjustedPoint:percentPoint autoFocus:YES];
    
}

- (void)pinchGesture:(UIPinchGestureRecognizer *)gesture {
    
    if (_currentPosition == AVCaptureDevicePositionFront) {
        return;
    }
    
    if (gesture.numberOfTouches != 2) {
        return;
    }
    CGPoint p1 = [gesture locationOfTouch:0 inView:self.view];
    CGPoint p2 = [gesture locationOfTouch:1 inView:self.view];
    CGFloat dx = (p2.x - p1.x);
    CGFloat dy = (p2.y - p1.y);
    CGFloat dist = sqrt(dx*dx + dy*dy);
    if (gesture.state == UIGestureRecognizerStateBegan) {
        _lastPinchDistance = dist;
    }
    
    CGFloat change = dist - _lastPinchDistance;
        change = change / (CGRectGetWidth(self.view.bounds) * 0.5) * 2.0;
    //
    [self.liveSession alivcLiveVideoZoomCamera:(change / 100 )];
    
    NSLog(@"缩放：%f",change / 1000);
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)appResignActive{
    [self destroySession];
    
    // 监听电话
    _callCenter = [[CTCallCenter alloc] init];
    _isCTCallStateDisconnected = NO;
    _callCenter.callEventHandler = ^(CTCall* call) {
        if ([call.callState isEqualToString:CTCallStateDisconnected])
        {
            _isCTCallStateDisconnected = YES;
        }
        else if([call.callState isEqualToString:CTCallStateConnected])
            
        {
            _callCenter = nil;
        }
    };
    
}

- (void)appBecomeActive{
    
    if (_isCTCallStateDisconnected) {
        sleep(2);
    }
    
    [self testPushCapture];
}

- (void)testPushCapture{
    
    AlivcLConfiguration *configuration = [[AlivcLConfiguration alloc] init];
    configuration.url = _url;
    configuration.videoMaxBitRate = 600 * 1000;
    configuration.videoBitRate = 500 * 1000;
    configuration.videoMinBitRate = 400 * 1000;
    configuration.audioBitRate = 64 * 1000;
#ifdef kIs_phone6
    configuration.videoSize = CGSizeMake(self.view.width, 260);// 横屏状态宽高不需要互换
    
#elif kIs_phone6plus
    configuration.videoSize = CGSizeMake(self.view.width, 260);// 横屏状态宽高不需要互换
    
#elif kIs_phone5
    configuration.videoSize = CGSizeMake(self.view.width, 220);// 横屏状态宽高不需要互换
#endif
    configuration.fps = 20;
    configuration.preset = AVCaptureSessionPresetiFrame1280x720;
    configuration.screenOrientation = _isScreenHorizontal;
    
    configuration.reconnectTimeout = 25;
    
    // 水印
    configuration.waterMaskImage = [UIImage imageNamed:@"watermask"];
    configuration.waterMaskLocation = 0;
    configuration.waterMaskMarginX = 10;
    configuration.waterMaskMarginY = 10;
    
    
    if (_currentPosition) {
        configuration.position = _currentPosition;
    } else {
        configuration.position = AVCaptureDevicePositionFront;
        _currentPosition = AVCaptureDevicePositionFront;
    }
    NSLog(@"版本号:%@", [AlivcLiveSession alivcLiveVideoVersion]);
    
    self.liveSession = [[AlivcLiveSession alloc] initWithConfiguration:configuration];
    self.liveSession.delegate = self;
    
    self.liveSession.enableMute = self.muteButton.selected;
    
    [self.liveSession alivcLiveVideoStartPreview];
    
    
    
    [self.liveSession alivcLiveVideoUpdateConfiguration:^(AlivcLConfiguration *configuration) {
        configuration.videoMaxBitRate = 600 * 1000;
        configuration.videoBitRate = 500 * 1000;
        configuration.videoMinBitRate = 400 * 1000;
        configuration.audioBitRate = 64 * 1000;
        configuration.fps = 20;
    }];
    [self.liveSession alivcLiveVideoConnectServer];
#ifdef kIs_phone6
    self.liveSession.previewView.frame = CGRectMake(0, 0, self.view.width, 260);
    
#elif kIs_phone6plus
    
    self.liveSession.previewView.frame = CGRectMake(0, 0, self.view.width, 220);
    [self.liveSession.previewView setBackgroundColor:[UIColor redColor]];
#elif kIs_phone5
    self.liveSession.previewView.frame = CGRectMake(0, 0, self.view.width, 180);
#endif
    
    dispatch_async(dispatch_get_main_queue(), ^{


        
//        self.liveSession.previewView.frame = CGRectMake(0, 0, self.view.width, 160);
        
//        [self.view insertSubview:[self.liveSession previewView] atIndex:0];
        [self.view addSubview:[self.liveSession previewView]];
        // 黑色遮罩
        maskView = [[UIView alloc] init];
        maskView.backgroundColor = [UIColor blackColor];
        maskView.alpha = 0.3;
        maskView.frame = CGRectMake(0, self.liveSession.previewView.bottom-50, self.view.width, 50);
        [self.view addSubview:maskView];
        
        
        
        UIButton *devicePositionBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [devicePositionBtn setFrame:CGRectMake(0, maskView.bottom-50, self.view.width/3, 50)];
        [devicePositionBtn setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
        [devicePositionBtn setImage:[UIImage imageNamed:@"摄像头转换"] forState:UIControlStateNormal];
        [devicePositionBtn addTarget:self action:@selector(deviceChange:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *torchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [torchBtn setFrame:CGRectMake(self.view.width/3, maskView.bottom-50, self.view.width/3, 50)];
        [torchBtn setImage:[UIImage imageNamed:@"闪光灯"] forState:UIControlStateNormal];
        [torchBtn addTarget:self action:@selector(torch:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *connectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [connectBtn setFrame:CGRectMake(self.view.width/3*2, maskView.bottom-50, self.view.width/3, 50)];
        [connectBtn setImage:[UIImage imageNamed:@"暂停直播"] forState:UIControlStateNormal];
        [connectBtn setImage:[UIImage imageNamed:@"开始直播"] forState:UIControlStateSelected];
        [connectBtn addTarget:self action:@selector(connerAndDis:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:devicePositionBtn];
        [self.view addSubview:torchBtn];
        [self.view addSubview:connectBtn];
        aliClassView = XIB(AliClassView);
        aliClassView.frame = CGRectMake(0, maskView.bottom, self.view.width, self.view.height-self.liveSession.previewView.height);
        [aliClassView setMessageSendSource:AliMessageSendSourceForLive];
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        NSDictionary *dic = @{@"token":token,@"activityId":self.liveID};
        [HTTPManager POST_PATH:PATH_getRoomId params:dic success:^(id responseObject) {
            NSString *temp = responseObject;
            [HTTPManager POST_PATH:PATH_get_huanxinId params:@{@"token":token} success:^(id responseObject) {
               
            } failure:^(NSError *error) {
                
            }];
            
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getIndexInfo" params:dic success:^(id result) {
//                NSDictionary *dict = result[@"result"]
//                ;
//                ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
//                [model setValuesForKeysWithDictionary:dict];
//                
//                if (model.memberLogo) {
//                    [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
//                }
//                if (model.memberNick) {
//                    [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
//                }
//                aliClassView.nickName = model.memberNick;
//            } failure:^(NSDictionary *errorInfo) {
//                
//            }];
            
            [HTTPManager POST_PATH:PATH_getIndexInfo params:dic success:^(id responseObject) {
                NSDictionary *dict = responseObject
                ;
                ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                
                if (model.memberLogo) {
                    [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
                }
                if (model.memberNick) {
                    [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
                }
                aliClassView.nickName = model.memberNick;

            } failure:^(NSError *error) {
                
            }];

            
            
            
            [aliClassView setMessageSendSource:AliMessageSendSourceForLive];
            aliClassView.roomsID = temp;
            //直播id  测试使用
            aliClassView.liveID = self.liveID;
            self.roomID = temp;
            
            [aliClassView joinRooms:temp];
            
            
        } failure:^(NSError *error) {
            
        }];
        
        [self.view addSubview:aliClassView];

       
        
                
        
    });
}

- (void)deviceChange:(UIButton *)button {
    button.selected = !button.isSelected;
    self.liveSession.devicePosition = button.isSelected ? AVCaptureDevicePositionBack : AVCaptureDevicePositionFront;
    _currentPosition = self.liveSession.devicePosition;
}
- (void)torch:(UIButton *)button {
    button.selected = !button.isSelected;
    self.liveSession.torchMode = button.isSelected ? AVCaptureTorchModeOn : AVCaptureTorchModeOff;
}
- (void)connerAndDis:(UIButton *)btn {
    btn.selected = !btn.selected;
    if (self.liveSession.dumpDebugInfo.connectStatus == AlivcLConnectStatusNone) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
             [self.liveSession alivcLiveVideoConnectServer];
        });
       
    }else{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.liveSession alivcLiveVideoDisconnectServer];
        });

    }
}
- (void)destroySession{
    
    [self.liveSession alivcLiveVideoDisconnectServer];
    [self.liveSession alivcLiveVideoStopPreview];
    [self.liveSession.previewView removeFromSuperview];
    self.liveSession = nil;
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session error:(NSError *)error{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *msg = [NSString stringWithFormat:@"%zd %@",error.code, error.localizedDescription];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Live Error" message:msg delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"重新连接", nil];
        alertView.delegate = self;
        [alertView show];
    });
    
    NSLog(@"!!!error : %@", error);
}

- (void)alivcLiveVideoReconnectTimeout:(AlivcLiveSession*)session {
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"重连超时（此处根据实际情况决定，默认重连时长5s，可更改，建议开发者在此处重连）" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        
//        [alertView show];
//    });
    [self.liveSession alivcLiveVideoConnectServer];
    
}

- (void)alivcLiveVideoLiveSessionConnectSuccess:(AlivcLiveSession *)session {
    
    NSLog(@"connect success!");
}


- (void)alivcLiveVideoLiveSessionNetworkSlow:(AlivcLiveSession *)session{
    // 注意：一定要套 主线程 完成UI操作
    dispatch_async(dispatch_get_main_queue(), ^{
        self.textView.text = @"网速太慢";
    });
}


- (void)alivcLiveVideoOpenAudioSuccess:(AlivcLiveSession *)session {
    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"YES" message:@"麦克风打开成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    });
}

- (void)alivcLiveVideoOpenVideoSuccess:(AlivcLiveSession *)session {
    dispatch_async(dispatch_get_main_queue(), ^{
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"YES" message:@"摄像头打开成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    });
}


- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session openAudioError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"麦克风获取失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session openVideoError:(NSError *)error {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"摄像头获取失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session encodeAudioError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"音频编码初始化失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    });
    
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session encodeVideoError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"视频编码初始化失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [alertView show];
    });
}

- (void)alivcLiveVideoLiveSession:(AlivcLiveSession *)session bitrateStatusChange:(ALIVC_LIVE_BITRATE_STATUS)bitrateStatus {

    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"升降码率:%ld", bitrateStatus);
    });
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != alertView.cancelButtonIndex) {
        [self.liveSession alivcLiveVideoConnectServer];
    } else {
        [self.liveSession alivcLiveVideoDisconnectServer];
    }
}

- (IBAction)buttonCloseClick:(id)sender {
    [self destroySession];
    [_timer invalidate];
    _timer = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cameraButtonClick:(UIButton *)button {
    button.selected = !button.isSelected;
    self.liveSession.devicePosition = button.isSelected ? AVCaptureDevicePositionBack : AVCaptureDevicePositionFront;
    _currentPosition = self.liveSession.devicePosition;
}

- (IBAction)skinButtonClick:(UIButton *)button {
    button.selected = !button.isSelected;
    [self.liveSession setEnableSkin:button.isSelected];
}
- (IBAction)flashButtonClick:(UIButton *)button {
    button.selected = !button.isSelected;
    self.liveSession.torchMode = button.isSelected ? AVCaptureTorchModeOn : AVCaptureTorchModeOff;
}

- (IBAction)muteButton:(UIButton *)sender {
    [sender setSelected:!sender.selected];
    self.liveSession.enableMute = sender.selected;
}

- (IBAction)disconnectButtonClick:(id)sender {
    if (self.liveSession.dumpDebugInfo.connectStatus == AlivcLConnectStatusNone) {
        [self.liveSession alivcLiveVideoConnectServer];
    }else{
        [self.liveSession alivcLiveVideoDisconnectServer];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_handle closeFile];
    [self destroySession];
    [_timer invalidate];
    _timer = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
