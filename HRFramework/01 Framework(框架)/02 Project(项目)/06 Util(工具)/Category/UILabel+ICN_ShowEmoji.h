//
//  UILabel+ICN_ShowEmoji.h
//  ICan
//
//  Created by albert on 2017/4/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (ICN_ShowEmoji)

/** 上传的时候将emoji转化成chat code */
- (NSString *)replaceEmojiCodeWithUnicode;

/** 显示的时候将chat code 转化成emoji */
- (void)replaceUnicodeWithEmojiCode;

@end
