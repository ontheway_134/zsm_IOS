//
//  NSString+ICN_Emoji.m
//  emoji表情测试
//
//  Created by albert on 2017/4/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "NSString+ICN_Emoji.h"

@implementation NSString (ICN_Emoji)

+ (NSString *)returnUnicodeWithEmojiStr:(NSString *)str{
    NSString *uniStr = [NSString stringWithUTF8String:[str UTF8String]];
    NSData *uniData = [uniStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *goodStr = [[NSString alloc] initWithData:uniData encoding:NSUTF8StringEncoding] ;
    HRLog(@"---编码--->[%@]",goodStr);
    
    return goodStr;
    
}


+ (NSString *)getEmojiStrWithNetStr:(NSString *)netStr{
    const char *jsonString = [netStr UTF8String];   // goodStr 服务器返回的 json
    NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
    NSString *goodMsg1 = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];
    HRLog(@"---解码--->[%@]",goodMsg1);
    return goodMsg1;
}

@end
