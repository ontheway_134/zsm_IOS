//
//  NSString+ICN_Emoji.h
//  emoji表情测试
//
//  Created by albert on 2017/4/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ICN_Emoji)

+ (NSString *)getEmojiStrWithNetStr:(NSString *)netStr;
+ (NSString *)returnUnicodeWithEmojiStr:(NSString *)str;

@end
