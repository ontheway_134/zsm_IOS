//
//  UILabel+ICN_ShowEmoji.m
//  ICan
//
//  Created by albert on 2017/4/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "UILabel+ICN_ShowEmoji.h"
#import "NSString+ICN_Emoji.h"
#import "NSString+Emoji.h"



@implementation UILabel (ICN_ShowEmoji)

- (NSString *)replaceEmojiCodeWithUnicode{
    if (self.text) {
        NSString *contant;
        contant = [self.text stringByReplacingEmojiUnicodeWithCheatCodes];
        return contant;
    }
    return nil;
}

- (void)replaceUnicodeWithEmojiCode{
    
    if (self.text) {
        [self.text stringByReplacingEmojiUnicodeWithCheatCodes];
    }
}



@end
