//
//  interPopView.h
//  ICan
//
//  Created by 辛忠志 on 2017/4/11.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ReturnSureBtn)();
@interface interPopView : UIView
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UITextField *timeTF;
@property (weak, nonatomic) IBOutlet UITextField *adressTF;
@property (weak, nonatomic) IBOutlet UITextField *personTF;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;

@property (strong,nonatomic)ReturnSureBtn returnSureBtn;
-(void)returnSureBtn:(ReturnSureBtn)block;
@end
