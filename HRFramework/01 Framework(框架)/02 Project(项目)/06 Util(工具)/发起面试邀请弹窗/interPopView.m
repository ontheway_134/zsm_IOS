//
//  interPopView.m
//  ICan
//
//  Created by 辛忠志 on 2017/4/11.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "interPopView.h"

@implementation interPopView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backView.layer.cornerRadius= 6;
    self.backView.layer.masksToBounds = YES;
    self.backView.layer.borderColor = RGB0X(0Xdcdcdc).CGColor;
    self.backView.layer.borderWidth = 0.5;
}

- (IBAction)sureBtnClick:(UIButton *)sender {
    if (self.returnSureBtn) {
        self.returnSureBtn(sender);
    }
}
- (IBAction)backViewTap:(UITapGestureRecognizer *)sender {
//    [self removeFromSuperview];
}
- (IBAction)closeBtnClick:(UIButton *)sender {
    [self removeFromSuperview];
}

-(void)returnSureBtn:(ReturnSureBtn)block{
    self.returnSureBtn = block;
}
@end
