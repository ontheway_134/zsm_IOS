//
//  NSString+BlankString.h
//  领调云
//
//  Created by cxx on 16/4/21.
//  Copyright © 2016年 CAPF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BlankString)
- (BOOL) isBlankString;
- (NSString *)stringChangeLength:(NSString *)string;
@end
