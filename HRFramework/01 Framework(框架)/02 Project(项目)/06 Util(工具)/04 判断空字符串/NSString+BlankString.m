//
//  NSString+BlankString.m
//  领调云
//
//  Created by cxx on 16/4/21.
//  Copyright © 2016年 CAPF. All rights reserved.
//

#import "NSString+BlankString.h"
#import <UIKit/UIKit.h>

@implementation NSString (BlankString)
- (BOOL) isBlankString{
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

- (NSString *)stringChangeLength:(NSString *)string {
    
    if([string length] > 4) {
        
        return [[string substringToIndex:3] stringByAppendingString:@"..."];
    }else{
        return string;
    }
}




@end
