//
//  ICN_Regular.m
//  ICan
//
//  Created by shilei on 16/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_Regular.h"
#import "NSString+Regular.h"

@implementation ICN_Regular

+ (BOOL)checkTelephoneNumber:(NSString *)number {
    
    NSString *regex = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    
    NSPredicate *predicateRe = [NSPredicate predicateWithFormat:@"self matches %@", regex];
    
    return [predicateRe evaluateWithObject:number];
}

#pragma 正则匹配用户密码 6 - 16 位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password{
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat: @"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
    
}

+ (BOOL)checkEmail:(NSString *)email {
    // NSPredicate *predicateRe = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    // 添加测试用邮箱正则
    // NSString *patternEmail = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9])+\.)+([a-zA-Z0-9]{2,4})+$";
    // NSError *err = nil;
    // NSRegularExpression *EmailExp =[NSRegularExpression regularExpressionWithPattern:patternEmail options:0 error:&err];
    // NSTextCheckingResult *isMatchEmail = [EmailExp firstMatchInString:email options:0 range:NSMakeRange(0, email.length)];
    // if (isMatchEmail) {
        return YES;
    // }else{
    //    return NO;
    // }
    return [email checkEmail];
}


@end
