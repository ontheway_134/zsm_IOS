//
//  ICN_Regular.h
//  ICan
//
//  Created by shilei on 16/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_Regular : NSObject

/*~!
 *  @func Telephone Number
 *  @auth muyingbo
 *  @time 2016-05-17
 *  @brif
 */
+ (BOOL)checkTelephoneNumber:(NSString *)number;

#pragma 正则匹配用户密码 6 - 18 位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password;


/*~!
 *  @func Email
 *  @auth muyingbo
 *  @time 2016-05-17
 *  @brif
 */
+ (BOOL)checkEmail:(NSString *)email;
@end
