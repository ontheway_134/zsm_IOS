//
//  MJRefreshAutoNormalFooter+Chinese.m
//  ICan
//
//  Created by 何壮壮 on 17/3/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "MJRefreshAutoNormalFooter+Chinese.h"
@interface MJRefreshAutoNormalFooter()

@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIActivityIndicatorView *loading;

@end
@implementation MJRefreshAutoNormalFooter (Chinese)


#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    // 设置控件的高度
    self.mj_h = 50;
    
    // 添加label
    UILabel *label = [[UILabel alloc] init];
    label.textColor = RGB0X(0X666666);
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textAlignment = NSTextAlignmentCenter;
    [self addSubview:label];
    self.label = label;
    
    // loading
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self addSubview:loading];
    self.loading = loading;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews
{
    [super placeSubviews];
    self.label.frame = self.bounds;
    self.loading.center = CGPointMake(self.mj_w*0.5-50/BASESCREENPX_WIDTH, self.mj_h * 0.5);
}

#pragma mark 监听scrollView的contentOffset改变
- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
{
    [super scrollViewContentOffsetDidChange:change];
    
}

#pragma mark 监听scrollView的contentSize改变
- (void)scrollViewContentSizeDidChange:(NSDictionary *)change
{
    [super scrollViewContentSizeDidChange:change];
    
}

#pragma mark 监听scrollView的拖拽状态改变
- (void)scrollViewPanStateDidChange:(NSDictionary *)change
{
    [super scrollViewPanStateDidChange:change];
    
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case 1:
        {
            [self.loading stopAnimating];
            self.label.text = @"下拉刷新列表";
        }
            break;
        case 2:
            self.label.text = @"加载列表中";
            [self.loading startAnimating];
            break;
        case 3:
            self.label.text = @"加载列表中";
            //[self.loading startAnimating];
            break;
        case 5:
            self.label.text = @"列表数据加载完成";
            [self.loading stopAnimating];
            self.state = 1;
            break;
        default:
            break;
    }
}

@end
