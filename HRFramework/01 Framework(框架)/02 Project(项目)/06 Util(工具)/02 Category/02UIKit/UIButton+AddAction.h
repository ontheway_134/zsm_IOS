/*================================
||    @param name   buttonAction ||
||    @param author muyingbo     ||
||    @param date   2016-04-03   ||
=================================*/

#import <UIKit/UIKit.h>

@interface UIButton (AddAction)

/*~!
 *  @func UIButton add action
 *  @auth muyingbo
 *  @time 2016-06-13
 *  @brif
 */
- (void)addAction:(void(^)(NSInteger tag))block;

@end
