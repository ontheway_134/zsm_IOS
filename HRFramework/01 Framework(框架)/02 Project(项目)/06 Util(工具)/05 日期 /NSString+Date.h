//
//  NSString+Date.h
//  QiJingVR
//
//  Created by cxx on 16/6/25.
//  Copyright © 2016年 CAPF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)
/** 计算时间差 */
+ (NSString *)intervalFromLastDate:(NSString *)dateString1 toTheDate:(NSString *)dateString2;
/** 时间戳转时间 */
+ (NSString *)conversionTimeStamp:(NSString *)theTimeStamp;
/** 获取当前时间 */
+ (NSString *)getCurrentDate;
/** 秒转时间 */
+ (NSString *)timeFormatted:(int)totalSeconds;
/** 获取时间戳 */
+ (NSString *)getcurrentTimetamp;
/* 毫秒转时间 **/
+ (NSString *)ConvertStrToTime:(NSString *)timeStr;
/* 毫秒转时间(中文) **/
+ (NSString *)ChineseConvertStrToTime:(NSString *)timeStr;
/* 毫秒转时间(小时) **/
+ (NSString *)ConvertStrToDetialTime:(NSString *)timeStr;

/** 时间戳转时间年月日 */
+ (NSString *)conversionTimeStamp2:(NSString *)theTimeStamp;
/** 时间戳转时间小时分钟 */
+ (NSString *)conversionTimeStamp3:(NSString *)theTimeStamp;
@end
