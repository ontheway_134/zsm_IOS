//
//  SQJudgeInfomation.h
//  SQ365
//
//  Created by 辛忠志 on 16/7/27.
//  Copyright © 2016年 辛忠志. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQJudgeInfomation : NSObject

//判断字符串是否符合电话号码格式 YES：符合
+ (BOOL)isMobileNumber:(NSString *)phone;

//判断字符串是否符合邮箱格式  YES：符合
+ (BOOL)isEmailNumber:(NSString *)email;

//判断身份证号
+ (BOOL) validateIdentityCard:(NSString *)identityCard;

//正则匹配用户密码 6 - 16 位数字和字母组合
+ (BOOL)checkPassword:(NSString *) password;
@end
