//
//  ICN_AliPayManager.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_AliPayManager.h"
#import "BaseOptionalModel.h"

static NSString * const schemesStr = @"ICNAliPay1325Scheme";

@implementation ICN_AliPayManager


+ (BOOL)AliPayWithActivityId:(NSString *)activityId{
    
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil || activityId == nil) {
        return NO;
    }
    NSDictionary *params = @{
                             @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken],
                             @"orderId":activityId
                             };
    [[HRNetworkingManager alloc] POST_PATH:PATH_AliPayOrder params:params success:^(id result) {
        // 获取到参数列表
        NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingMutableLeaves error:nil];
        BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:resultDic error:nil];
        if (baseModel != nil && baseModel.code == 0) {
            // 获取到正确的数据
            NSString *resultStr = [[resultDic valueForKey:@"result"] valueForKey:@"orderInfo"];
            NSString *orderStr = [resultStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            if (orderStr != nil) {
                // 获取到订单信息 - 开始调用支付宝支付流程
                [[AlipaySDK defaultService] payOrder:orderStr fromScheme:schemesStr callback:^(NSDictionary *resultDic) {
                    // 回调参数 -- 处理支付结果
                    HRLog(@"%@",resultDic);
                }];
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"支付宝 -- 网络请求失败");
    }];
    
    return YES;
}

@end
