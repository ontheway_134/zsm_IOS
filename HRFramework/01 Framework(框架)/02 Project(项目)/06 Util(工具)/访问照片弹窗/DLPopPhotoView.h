//
//  DLPopPhotoView.h
//  DLCY
//
//  Created by 辛忠志 on 2017/1/24.
//  Copyright © 2017年 辛忠志. All rights reserved.
//

#import <UIKit/UIKit.h>
/* 本地拍照和直接拍照的两个block**/
typedef void (^ReturnTakingPhoto)();
typedef void (^ReturnLocationPhoto)();
@interface DLPopPhotoView : UIView
@property (strong,nonatomic)ReturnTakingPhoto returnTakingPhoto;
@property (strong,nonatomic)ReturnLocationPhoto locationPhoto;

-(void)returnTakingPhoto:(ReturnTakingPhoto)block;
-(void)returnLocationPhoto:(ReturnLocationPhoto)block;
@end
