//
//  DLPopPhotoView.m
//  DLCY
//
//  Created by 辛忠志 on 2017/1/24.
//  Copyright © 2017年 辛忠志. All rights reserved.
//

#import "DLPopPhotoView.h"

@implementation DLPopPhotoView

#pragma  mark  ----------本地 和 照相机拍照Click ---------
- (IBAction)takingPhotoBtn:(UIButton *)sender {
    if (self.returnTakingPhoto) {
        self.returnTakingPhoto(sender);
    }
}

- (IBAction)locationPhotoBtn:(UIButton *)sender {
    if (self.locationPhoto) {
        self.locationPhoto(sender);
    }
}
- (IBAction)backTap:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}
-(void)returnTakingPhoto:(ReturnTakingPhoto)block{
    self.returnTakingPhoto = block;
}
-(void)returnLocationPhoto:(ReturnLocationPhoto)block{
    self.locationPhoto = block;
}

@end
