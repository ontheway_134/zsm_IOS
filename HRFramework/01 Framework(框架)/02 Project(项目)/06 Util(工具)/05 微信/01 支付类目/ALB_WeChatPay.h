//
//  ALB_WeChatPay.h
//  ICan
//
//  Created by albert on 2017/2/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

// 创建一个用于微信支付的单例

static NSString * const WeiXinAppKey = @"wxcb356d39c9fc602a";

@interface ALB_WeChatPay : NSObject

// 获取单例并自动支付订单的方法
+ (instancetype)shareWeiChatPayWithActivityId:(NSString *)activityId;


@end
