//
//  ALB_WeChatPay.m
//  ICan
//
//  Created by albert on 2017/2/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ALB_WeChatPay.h"
#import <WXApi.h>
#import "BaseOptionalModel.h"
#import "ICN_WeiChatOrderModel.h"

//warn=== 微信支付


static ALB_WeChatPay *instance;


@interface ALB_WeChatPay ()

@property (nonatomic , strong)ICN_WeiChatOrderModel *payOrderModel; // 微信支付Model

@end

@implementation ALB_WeChatPay


+ (instancetype)shareWeiChatPayWithActivityId:(NSString *)activityId{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [[ALB_WeChatPay alloc] init];
        }
    });
    // 根据服务器网络请求获取订单
    NSDictionary *params ;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil && activityId != nil) {
        params = @{@"orderId" : activityId , @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken]};
    }
    if (params) {
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_WeiChatPAYOrder params:params success:^(id result) {
            // 获取成功之后获取其中的有用参数并调起微信支付
            [WXApi registerApp:WeiXinAppKey withDescription:@"调起微信支付"];
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                // 获取回调成功
                ICN_WeiChatOrderModel *model = [[ICN_WeiChatOrderModel alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
                // 将获取到的Model缓存在内容中
                if (model) {
                    instance.payOrderModel = model;
                }
                // 然后根据获取到的参数进行微信调起
                PayReq *request = [[PayReq alloc] init];
                request.partnerId = instance.payOrderModel.partnerid;
                request.prepayId= instance.payOrderModel.prepayid;
                request.package = instance.payOrderModel.package;
                request.nonceStr= instance.payOrderModel.noncestr;
                request.timeStamp = [instance.payOrderModel.timestamp integerValue];
                request.sign= instance.payOrderModel.sign;
                // 微信发起回调
                [WXApi sendReq:request];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }
    
    return instance;
}



@end
