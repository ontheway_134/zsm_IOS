//
//  PhotosCollectionViewCell.h
//  TXAD
//
//  Created by shilei on 16/11/1.
//  Copyright © 2016年 risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

@property (nonatomic, assign) NSInteger row;


@end
