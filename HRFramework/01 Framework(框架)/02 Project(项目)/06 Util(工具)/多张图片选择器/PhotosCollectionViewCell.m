//
//  PhotosCollectionViewCell.m
//  TXAD
//
//  Created by shilei on 16/11/1.
//  Copyright © 2016年 risenb. All rights reserved.
//

#import "PhotosCollectionViewCell.h"
#import "UIView+Layout.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "TZImagePickerController/TZImagePickerController.h"


@implementation PhotosCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _backView.frame = self.bounds;
    _imageView.frame = self.bounds;
}

- (void)setRow:(NSInteger)row {
    _row = row;
    _deleteBtn.tag = row;
}
@end
