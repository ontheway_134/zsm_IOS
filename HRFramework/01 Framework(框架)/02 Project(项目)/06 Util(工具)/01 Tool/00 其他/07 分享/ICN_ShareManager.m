//
//  ICN_ShareManager.m
//  ICan
//
//  Created by shilei on 17/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ShareManager.h"
#import <UMSocialCore/UMSocialCore.h>



@implementation ICN_ShareManager


+ (ICN_ShareManager *)defaultInstance {
    
    static ICN_ShareManager *instanceManager = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        instanceManager = [[self alloc] init];
    });
    return instanceManager;
}


- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType andVC:(UIViewController *)MVC andUrl:(NSString *)url andTitle:(NSString *)title andImage:(NSString *)image Detial:(NSString *)detial
{
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    NSString* thumbURL = url;
    NSString *contentDetial = SF(@"%@",detial);
    UIImage *imageIcon = [UIImage imageNamed:@"占位图"];
    NSString *iconUrl;
    __block UMShareWebpageObject *shareObject ;
    if (image != nil) {
        iconUrl = image;
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:iconUrl] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (image) {
                // 获取到需要的图片
                shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:contentDetial thumImage:image];
                //设置网页地址
                shareObject.webpageUrl = thumbURL;
                [self shareWithShareObject:shareObject MessageObject:messageObject PlatformType:platformType];
                
            }else{
                // 没获取到图片上传占位图
                shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:contentDetial thumImage:imageIcon];
                //设置网页地址
                shareObject.webpageUrl = thumbURL;
                [self shareWithShareObject:shareObject MessageObject:messageObject PlatformType:platformType];
            }
        }];
    }else{
        // 没传图片
        shareObject = [UMShareWebpageObject shareObjectWithTitle:title descr:contentDetial thumImage:imageIcon];
        //设置网页地址
        shareObject.webpageUrl = thumbURL;
        [self shareWithShareObject:shareObject MessageObject:messageObject PlatformType:platformType];
    }
    
}

- (void)shareWithShareObject:(UMShareWebpageObject *)shareObject MessageObject:(UMSocialMessageObject *)messageObject PlatformType:(UMSocialPlatformType)platformType{
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error){
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
                
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
        //[self alertWithError:error];
    }];

}

@end
