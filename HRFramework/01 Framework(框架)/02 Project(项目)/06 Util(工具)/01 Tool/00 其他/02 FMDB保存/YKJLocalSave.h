//
//  YKJLocalSave.h
//  YKJ.iOS
//
//  Created by 高阳 on 16/10/19.
//  Copyright © 2016年 高阳. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKJKeyValueStore.h"

@interface YKJLocalSave : NSObject

/** 使用提示：
 我使用的方式是将每个模块的所有缓存内容存储在一个以模块命名的表中
 
 每个path获取到的数据存储的key是这个path 获取的内容是data类型的
 
 暂时只能实现每个path存储最近的一条数据，其他内容以后考虑添加
 */

/** 根据模块名，路径名将数据存储在（模块表中的路径列） */
+ (BOOL)saveDataLocalWithModuleName:(NSString *)module
                           pahtName:(NSString *)path
                        ContentData:(id)data;

/** 根据模块名，路径名获取指定数据 */
+ (id)getLocalDataWithModuleName:(NSString *)module
                        pahtName:(NSString *)path;

/** 将本地表中存储的所有数据清空，表名也会不复存在 */
+ (BOOL)clearLocalDataCache;


@end
