

#import "HRRequest.h"

@implementation HRRequest

//初始化（单例）
+ (instancetype)manager {
  		static id instance = nil;
  		static dispatch_once_t onceToken;
  		dispatch_once(&onceToken, ^{
            instance = [[super allocWithZone:NULL] init];
        });
  		return instance;
}
+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [[self class] manager];
}
- (instancetype)copyWithZone:(struct _NSZone *)zone{
    return [[self class] manager];
}

#pragma mark - ---------- Public Methods ----------

#pragma mark POST请求
- (void)GET:(NSString *)path para:(NSDictionary *)para success:(Success)success faiulre:(Failure)failure {
    //网络异常处理
    if ([self handleNetNotWork:failure]) {
        return;
    }
    //
    [self GET_PATH:path params:para success:^(id result) {
        //处理请求结果返回值
        [self handleResult:result success:success failure:failure];
    } failure:^(NSDictionary *errorInfo) {
        //处理请求异常
        [self handleRequestError:errorInfo failure:failure];
    }];
}

- (void)POST:(NSString *)path para:(NSDictionary *)para success:(Success)success faiulre:(Failure)failure {
  
    //网络异常处理
    if ([self handleNetNotWork:failure]) {
        return;
    }
    //
    [self POST_PATH:path params:para success:^(id result) {
        //处理请求结果返回值
        [self handleResult:result success:success failure:failure];
    } failure:^(NSDictionary *errorInfo) {
        //处理请求异常
        [self handleRequestError:errorInfo failure:failure];
        
    }];
}

#pragma mark - ---------- Private Methods ----------
//处理网络异常
- (BOOL)handleNetNotWork:(Failure)failure {
    if (self.netStatus == HRNetStatusUnknown || self.netStatus == HRNetStatusNotReachable) {
        if (failure) {
            NSLog(@"错误提示信息可在 \"[02 project] -> [05 Common] -> [Contant.h] 中修改常量 \"NET_NOT_WORK\" 值");
            failure(NET_NOT_WORK);
        }
        
        return YES;
    }
    return NO;
}
//处理请求异常
- (void)handleRequestError:(NSDictionary *)errorInfo failure:(Failure)failure {
    if (errorInfo) {
        NSLog(@"错误提示信息可在 [02 project] -> [05 Common] -> [Contant.h] 中修改常量 \"ERROR_MESSAGE\" 值");
        if (failure) {
            failure(ERROR_MESSAGE);
        }
    }
}
//处理请求结果返回值
- (void)handleResult:(id)result success:(Success)success failure:(Failure)failure {
    //此处可修改 success -> status，或者其他参数名称， 值的判断同理
    if ([result[@"code"] integerValue] == 0) {
        //data 可修改，原理同上
        success(result[@"result"]);
    } else {
        //errorMsg 可修改，原理同上
        failure(result[@"errorMsg"]);
    }
}

@end
