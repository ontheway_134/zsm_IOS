//
//  MBProgressHUD+PrograssHUDHelper.m
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "MBProgressHUD+PrograssHUDHelper.h"

@implementation MBProgressHUD (PrograssHUDHelper)

+ (void)ShowProgressWithBaseView:(UIView *)view Message:(NSString *)message{
    
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUD.label.text = message;
    HUD.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
    [HUD hideAnimated:YES afterDelay:0.75];
    
}
+ (void)windowShowProgressWithBaseView:(UIWindow *)view Message:(NSString *)message{
    
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUD.label.text = message;
    HUD.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
    [HUD hideAnimated:YES afterDelay:0.75];
    
}


+ (void)ShowProgressToSuperView:(UIView *)view Message:(NSString *)message{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    HUD.label.text = message;
    HUD.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
}

+ (void)hiddenHUDWithSuperView:(UIView *)view{
    [MBProgressHUD hideHUDForView:view animated:YES];
}


@end
