//
//  NSURLRequest+IgnoreSSL.h
//  ICan
//
//  Created by shilei on 17/3/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURLRequest (IgnoreSSL)


+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host;

@end
