//
//  AppDelegate+OtherParty.m
//  ICan
//
//  Created by shilei on 17/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate+OtherParty.h"
#import <UMSocialCore/UMSocialCore.h>


@implementation AppDelegate (OtherParty)

-(void)OtherParty{
    [[UMSocialManager defaultManager] openLog:YES];
    [[UMSocialManager defaultManager] setUmSocialAppkey:@"587adbb51061d20f96000409"];
    
    //微信需要
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxcb356d39c9fc602a" appSecret:@"ff85f8638c191a8c6a02a0fe91753ce1" redirectURL:@"http://mobile.umeng.com/social"];
    
    //qq不需要
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:@"1105932124"  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    
     [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:@"4159040443"  appSecret:@"e65823ef4fad8490a123d41dc670197b" redirectURL:@"http://sns.whalecloud.com/sina2/callback"];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    // 对于获取到支付宝的跳转后的处理
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }
    if (!result) {
        NSLog(@"dddd");// 其他如支付等SDK的回调
         [WXApi handleOpenURL:url delegate:self];
    }
    return result;
}

#pragma mark - ---------- 支付回调类目 ----------

#pragma mark --- 微信支付回调类目 ---
- (void)onResp:(BaseResp *)resp
{
    if([resp isKindOfClass:[PayResp class]]){
        //支付返回结果，实际支付结果需要去微信服务器端查询
        NSString *strMsg,*strTitle = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
            case WXSuccess:
                strMsg = @"支付结果：成功！";
               
                break;
                
            default:
                strMsg = [NSString stringWithFormat:@"支付结果：失败"];
                
                break;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
}



@end
