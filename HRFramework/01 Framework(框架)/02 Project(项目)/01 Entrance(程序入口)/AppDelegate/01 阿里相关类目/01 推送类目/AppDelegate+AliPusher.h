//
//  AppDelegate+AliPusher.h
//  ICan
//
//  Created by albert on 2017/2/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate (AliPusher)<UNUserNotificationCenterDelegate>

// 初始化阿里云推送
- (void)initCloudPush;

//注册苹果推送，获取deviceToken用于推送
- (void)registerAPNS:(UIApplication *)application;

@end
