//
//  AppDelegate+AliPusher.m
//  ICan
//
//  Created by albert on 2017/2/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate+AliPusher.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import <UserNotifications/UserNotifications.h>
#import "ICN_IndustryNextViewController.h"   //行业资讯
#import "ICN_UserHomePagerVC.h"     //个人信息主页
#import "ICN_PositionNextOneViewController.h" // 职位详情页面
#import "ICN_ActivityDetialViewController.h" // 活动详情页面
#import "ICN_AdvertisimentShowListPager.h" // 广告页面
#import "ICN_SignViewController.h" // 用户登录页面
#import "ICN_BusinessMassageViewController.h" // 企业信息补全页面
#import "ICN_SetUserInfoViewController.h" // 个人信息补全页面
#import "ICN_DynamicStateVC.h" // 发现首页
#import "ICN_ActiveityMainViewController.h" // 活动列表页
#import "ICN_CareerLifeVC.h" // 职场首页


static NSString *const AliAppKey = @"23627801";
static NSString *const AliAppSecrect = @"950a4767609fd88d17a5f02fde0caf17";

@implementation AppDelegate (AliPusher)


- (void)initCloudPush {
    // SDK初始化
    [CloudPushSDK asyncInit:AliAppKey appSecret:AliAppSecrect callback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            NSLog(@"Push SDK init success, deviceId: %@.", [CloudPushSDK getDeviceId]);
            // 开启debug日志 ==
            [CloudPushSDK turnOnDebug];
            [self registerMessageReceive];
            
        } else {
            NSLog(@"Push SDK init failed, error: %@", res.error);
        }
    }];
}

// 注册推送消息到来监听
- (void)registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMessageReceived:)
                                                 name:@"CCPDidReceiveMessageNotification"
                                               object:nil];
}


// 注册苹果推送，获取deviceToken用于推送
- (void)registerAPNS:(UIApplication *)application {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        // iOS 8 Notifications
        [application registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:
          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
                                           categories:nil]];
        [application registerForRemoteNotifications];
    }
    else {
        // iOS < 8 Notifications
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
}

#pragma mark - ---------- 推送相关代理回调 ----------

//苹果推送注册成功回调，将苹果返回的deviceToken上传到CloudPush服务器
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [CloudPushSDK registerDevice:deviceToken withCallback:^(CloudPushCallbackResult *res) {
        HRLog(@"device - token %@",[CloudPushSDK getApnsDeviceToken]);
        if (res.success) {
            HRLog(@"Register deviceToken success.");
            if ([CloudPushSDK isChannelOpened]) {
                HRLog(@"测试通道打开");
            }
            
        } else {
            HRLog(@"Register deviceToken failed, error: %@", res.error);
        }
    }];
}

//苹果推送注册失败回调
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
}

//获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}


- (void)showMessageDic:(NSDictionary *)dic{
    if (dic == nil) {
        return;
    }
    NSString *title = [dic.allKeys componentsJoinedByString:@"++"];
    NSString *content = [dic.allValues componentsJoinedByString:@"--"];
    UIAlertController *actionController = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancer = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [actionController addAction:cancer];
    if ([self getCurrentVC]) {
        [[self getCurrentVC] presentViewController:actionController animated:YES completion:nil];
    }
}


// 收到消息之后对于页面数据的处理
- (void)dealWithReceiveNotificationWithUserInfo:(NSDictionary *)userInfo
                                    Application:(UIApplication*)application{
//    // userInfo是在推送中获取的全部字段 - 在这里能够看一下推送获取的全部内容
//    //获取具体的type值  //1牛人大咖 2 企业 3 企业广告 4咨询 5提问 6活动 7行业资讯 8职场规划 9职位 10其他
//    NSInteger type = [[userInfo valueForKey:@"type"] integerValue];
//    NSString *commentId ;
//    if ([userInfo valueForKey:@"fromId"]) {
//        commentId = [userInfo valueForKey:@"fromId"];
//    }
//    
//    // 在这里暂时只考虑能获取到推送信息的时候
//    // 1. 针对全局发的信息 - 判断token 如果不存在的话直接跳转到登录页面
//    // 2. 如果token存在 不管之前的页面是什么 - 直接跳转到指定的详情页面
//    // 3. 如果当前页面没有导航栏的话为当前页面添加一个导航栏然后在进行相应的跳转
//    
//    // 获取当前页面
//    UIViewController *currentViewController = [self getCurrentVC] ;
//    // 获取不到当前页面直接返回不进行下一步操作(弹窗并且两秒钟后消失)
//    if (currentViewController == nil) {
//        [MBProgressHUD showHUDAddedTo:[application keyWindow] animated:YES];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [MBProgressHUD hiddenHUDWithSuperView:[application keyWindow]];
//        });
//        return ;
//    }
//    BOOL isLogin = NO; // 默认设置登录状态是NO
//    BOOL isMsgCheck = NO; // 设置默认的信息补全状态是NO
//    // 从userdefault中获取用户类型信息 如果没有的话默认是游客类型
//    NSInteger userType = [USERDEFAULT valueForKey:HR_UserTypeKEY] ? [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] : HR_ICNVisitor;
//    // 根据用户token 判断当前的用户登录状态
//    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] && userType != HR_ICNVisitor) {
//        isLogin = YES;
//    }
//    if ([USERDEFAULT valueForKey:@"loginStatus"]) {
//        if ([[USERDEFAULT valueForKey:@"loginStatus"] integerValue] == 0) {
//            // 基本信息填完了
//            isMsgCheck = YES;
//        }
//    }
//#warning Deal With 企业认证（暂不处理）
//    
//    // 根据不符合要求的页面直接跳转到指定页面
//    
//    if (!isLogin) {
//        // 跳转到登录页面
//        ICN_SignViewController *pager = [[ICN_SignViewController alloc] init];
//        if (currentViewController.navigationController) {
//            [currentViewController.navigationController pushViewController:pager animated:YES];
//        }else{
//            [currentViewController presentViewController:pager animated:YES completion:^{
//                [APPLICATION keyWindow].rootViewController = pager;
//            }];
//        }
//        return ;
//    }
//    
//    if (!isMsgCheck) {
//        // 跳转到信息补全
//        if (userType == HR_ICNEnterprise) {
//            // 跳转到企业信息补全页面
//            ICN_BusinessMassageViewController *pager = [[ICN_BusinessMassageViewController alloc] init];
//            UINavigationController *nav ;
//            if (currentViewController.navigationController) {
//                nav = currentViewController.navigationController;
//            }else{
//                nav = [[UINavigationController alloc] initWithRootViewController:currentViewController];
//            }
//            [nav pushViewController:pager animated:YES];
//        }
//        if (userType == HR_ICNUser) {
//            // 跳转到用户信息补全页面
//            ICN_SetUserInfoViewController *pager = [[ICN_SetUserInfoViewController alloc] init];
//            UINavigationController *nav ;
//            if (currentViewController.navigationController) {
//                nav = currentViewController.navigationController;
//            }else{
//                nav = [[UINavigationController alloc] initWithRootViewController:currentViewController];
//            }
//            [nav pushViewController:pager animated:YES];
//        }
//        return ;
//    }
//    
//    // 然后进行默认的跳转处理
//    if (currentViewController.navigationController) {
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:currentViewController];
//    }
//    
//#warning mark -- 这部分可能会因为判断APP状态的失误导致跳转到错误的页面
//    switch (type) {
//        case 1:{
//            //牛人认证
//            //                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
//            //                barController.selectedIndex = 1;
//            //                                UIViewController  *currentController = self.window.rootViewController.presentedViewController;
//            //                [currentController presentViewController:barController animated:NO completion:^{
//            //
//            //                    [((UINavigationController *)barController.selectedViewController) pushViewController:userhome animated:YES];
//            //                }];
//            
//            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
//            [currentViewController.navigationController pushViewController:userhome animated:YES];
//            [currentViewController hidesBottomBarWhenPushed];
//            
//            
//        }
//            break;
//            //企业
//        case 2:
//            //企业广告
//        case 3:
//            // 智讯
//        case 4:
//            // 提问
//        case 5:
//        {
//            
//            // 全部跳转到首页
//            ICN_DynamicStateVC *pager = [[ICN_DynamicStateVC alloc] init];
//            if (userType == HR_ICNEnterprise) {
//                pager.enterpriseLogin = YES;
//            }
//            [currentViewController.navigationController pushViewController:pager animated:YES];
//            break;
//        }
//            // 跳转到活动页面
//        case 6:{
//            
//            if (commentId) {
//                ICN_ActivityDetialViewController *pager = [[ICN_ActivityDetialViewController alloc] init];
//                pager.packID = commentId;
//                [currentViewController.navigationController pushViewController:pager animated:YES];
//            }else{
//                ICN_ActiveityMainViewController *themePager = [[ICN_ActiveityMainViewController alloc] init];
//                [currentViewController.navigationController pushViewController:themePager animated:YES];
//            }
//        }
//            break;
//        case 7:
//        case 8:
//        {
//            if (commentId) {
//                ICN_IndustryNextViewController *industry=[[ICN_IndustryNextViewController alloc]init];
//                industry.url = commentId;
//                [currentViewController.navigationController pushViewController:industry animated:YES];
//            }else{
//                ICN_CareerLifeVC *pager = [[ICN_CareerLifeVC alloc] init];
//                [currentViewController.navigationController pushViewController:pager animated:YES];
//            }
//        }
//            break;
//            
//        case 9:{
//            //职位
//            if (commentId) {
//                ICN_PositionNextOneViewController *pager = [[ICN_PositionNextOneViewController alloc] init];
//                pager.url = commentId;
//                [currentViewController.navigationController pushViewController:pager animated:YES];
//            }else{
//                ICN_CareerLifeVC *pager = [[ICN_CareerLifeVC alloc] init];
//                [currentViewController.navigationController pushViewController:pager animated:YES];
//            }
//        }
//        case 10:{
//            //其他
//        }
//        default:
//            break;
//    }
//    // 设置确认点击的回调
//    // 通知打开回执上报
//    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
//    // iOS badge 清0
//    application.applicationIconBadgeNumber = 0;
    
    // userInfo是在推送中获取的全部字段 - 在这里能够看一下推送获取的全部内容
    //获取具体的type值  // 1牛人大咖 2 企业 3 企业广告 4咨询 5提问 6活动 7行业资讯 8职场规划 9职位 10其他
    NSInteger type = [[userInfo valueForKey:@"type"] integerValue];
    NSString *commentId;
    if ([userInfo valueForKey:@"fromId"]) {
        commentId = [userInfo valueForKey:@"fromId"];
    }
    
    // 在这里暂时只考虑能获取到推送信息的时候
    // 1. 针对全局发的信息 - 判断token 如果不存在的话直接跳转到登录页面
    // 2. 如果token存在 不管之前的页面是什么 - 直接跳转到指定的详情页面
    // 3. 如果当前页面没有导航栏的话为当前页面添加一个导航栏然后在进行相应的跳转
    
    BOOL isLogin = NO; // 默认设置登录状态是NO
    BOOL isMsgCheck = NO; // 设置默认的信息补全状态是NO
    // 从userdefault中获取用户类型信息 如果没有的话默认是游客类型
    NSInteger userType = [USERDEFAULT valueForKey:HR_UserTypeKEY] ? [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] : HR_ICNVisitor;
    // 根据用户token 判断当前的用户登录状态
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] && userType != HR_ICNVisitor) {
        isLogin = YES;
    }
    if ([USERDEFAULT valueForKey:@"loginStatus"]) {
        if ([[USERDEFAULT valueForKey:@"loginStatus"] integerValue] == 0) {
            // 基本信息填完了
            isMsgCheck = YES;
        }
    }
    
    // 根据不符合要求的页面直接跳转到指定页面
    if (!isLogin) {
        // 跳转到登录页面
        ICN_SignViewController *loginVC = [[ICN_SignViewController alloc] init];
        [APPLICATION keyWindow].rootViewController = loginVC;
        return;
    }
    
    if (!isMsgCheck) {
        // 跳转到信息补全
        if (userType == HR_ICNEnterprise) {
            // 跳转到企业信息补全页面
            ICN_BusinessMassageViewController *pager = [[ICN_BusinessMassageViewController alloc] init];
        }
        if (userType == HR_ICNUser) {
            // 跳转到用户信息补全页面
            ICN_SetUserInfoViewController *pager = [[ICN_SetUserInfoViewController alloc] init];
        }
        return;
    }
    
    //跳转特殊页面
    // 1牛人大咖 2 企业 3 企业广告 4咨询 5提问 6活动 7行业资讯 8职场规划 9职位 10其他
    //
    UITabBarController *tabBarVC = (UITabBarController *)self.window.rootViewController;
    switch (type) {
        case 1:{//牛人认证 -> (个人中心)
            tabBarVC.selectedIndex = 4;
        }
            break;
        case 2: { //企业 -> (职场.招聘)
            tabBarVC.selectedIndex = 2;
#warning TODO 切换到招聘标签
            
        }
            break;
        case 3: { //企业广告 -> (首页.动态.根据参数id,Push至对应广告详情)
            tabBarVC.selectedIndex = 0;
#warning TODO Push到广告详情
            
        }
            break;
        case 4: { // 智讯 -> (首页.智讯.根据参数id,Push至对应讯息详情)
            tabBarVC.selectedIndex = 0;
#warning TODO Push到智讯详情
            
        }
            break;
        case 5: { // 提问 -> (首页.智讯.根据参数id,Push至提问详情)
            tabBarVC.selectedIndex = 0;
#warning TODO Push到提问详情
            
            break;
        }
        case 6:{ // 活动 -> (活动.详情)
            if (commentId) {
                tabBarVC.selectedIndex = 3;
                ICN_ActivityDetialViewController *pager = [[ICN_ActivityDetialViewController alloc] init];
                pager.packID = commentId;
                [tabBarVC.childViewControllers[3] pushViewController:pager animated:YES];
            }
        }
            break;
        case 7:
        case 8:
        case 9:{ // 7行业资讯 8职场规划 9职位 -> (职场.详情)
            if (commentId) {
                ICN_PositionNextOneViewController *pager = [[ICN_PositionNextOneViewController alloc] init];
                pager.url = commentId;
                [tabBarVC.childViewControllers[3] pushViewController:pager animated:YES];
            }
        }
        case 10:{ //其他
        }
        default:
            break;
    }
    // 设置确认点击的回调
    
    // 测试到这里没有 -- 跳转至后进行上报操作了
    [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    [MBProgressHUD ShowProgressToSuperView:[APPLICATION keyWindow] Message:@"跳转至后进行上报操作了"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    });
    
    // 通知打开回执上报
    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
    // badge clear
    application.applicationIconBadgeNumber = 0;
}

// App处于启动状态时，通知打开回调
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    NSLog(@"Receive one notification.");
    
    // 测试到这里没有 -- 跳转至后进行上报操作了
    [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    [MBProgressHUD ShowProgressToSuperView:[APPLICATION keyWindow] Message:@"APP后台的通知"];
    [self showMessageDic:userInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    });
    // 进行远端推送数据逻辑处理
    [self dealWithReceiveNotificationWithUserInfo:userInfo Application:application];
    
    
    [CloudPushSDK sendNotificationAck:userInfo];
}




#pragma mark - ---------- 对于推送消息的处理 ----------

// 处理到来推送消息
- (void)onMessageReceived:(NSNotification *)notification {
    //    CCPSysMessage *message = [notification object];
    //    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
    //    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
    //    HRLog(@"Receive message title: %@, content: %@.", title, body);
}

/**
 *  处理iOS 10通知(iOS 10+)
 */
- (void)handleiOS10Notification:(UNNotification *)notification TouchAction:(BOOL)touchAction {
    UNNotificationRequest *request = notification.request;
    UNNotificationContent *content = request.content;
    NSDictionary *userInfo = content.userInfo;
    // 测试到这里没有 -- 跳转至后进行上报操作了
    [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    [MBProgressHUD ShowProgressToSuperView:[APPLICATION keyWindow] Message:@"看到iOS10 通知了"];
    [self showMessageDic:content.userInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
    });
//    // 通知时间
//    NSDate *noticeDate = notification.date;
//    // 标题
//    NSString *title = content.title;
//    // 副标题
//    NSString *subtitle = content.subtitle;
//    // 内容
//    NSString *body = content.body;
//    // 角标
//    int badge = [content.badge intValue];
    // 通知打开回执上报
    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
    // 判断是否进行了点击动作，如果进行了就触发后续页面的点击事件
    if (touchAction) {
        // 测试到这里没有 -- 跳转至后进行上报操作了
        [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
        [MBProgressHUD ShowProgressToSuperView:[APPLICATION keyWindow] Message:@"点击通知了"];
        [self showMessageDic:content.userInfo];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD hiddenHUDWithSuperView:[APPLICATION keyWindow]];
        });
        [self dealWithReceiveNotificationWithUserInfo:userInfo Application:APPLICATION];
    }
}
/**
 *  App处于前台时收到通知(iOS 10+) : 这个回调只是用于显示通知
 *  当App处于前台，收到通知会触发userNotificationCenter:willPresentNotification:withCompletionHandler:回调
 * 在回调中可以处理通知相关的字段信息，回调处理结束前需要调用completionHandler(UNNotificationPresentationOptions)，
 */
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSLog(@"Receive a notification in foregound.");
    // 处理iOS 10通知相关字段信息
    [self handleiOS10Notification:notification TouchAction:NO];
    // 通知不弹出
    //    completionHandler(UNNotificationPresentationOptionNone);
    // 通知弹出，且带有声音、内容和角标（App处于前台时不建议弹出通知）
    completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
}

/**
 * iOS 10 触发点击或者清除通知的回调
 * 点击/清除通知时， 可在userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:回调里捕获到这些动作
 * 点击通知打开App，对应UNNotificationDefaultActionIdentifier；
 * 左滑删除通知，对应UNNotificationDismissActionIdentifier
 */
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    NSString *userAction = response.actionIdentifier;
    // 点击通知打开
    if ([userAction isEqualToString:UNNotificationDefaultActionIdentifier]) {
        NSLog(@"User opened the notification.");
        // 处理iOS 10通知
        [self handleiOS10Notification:response.notification TouchAction:YES];
    }
    completionHandler();
}



@end
