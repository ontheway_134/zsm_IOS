//
//  AppDelegate+BMKManager.h
//  ICan
//
//  Created by albert on 2017/2/24.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (BMKManager)


// 初始化百度map管理器的方法
- (BOOL)createBaiduMapManager;



@end
