//
//  AppDelegate+Hyphenate.m
//  ICan
//
//  Created by zjk on 2017/2/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate+Hyphenate.h"

static NSString *HY_Appkey = @"1127161102115685#ixing";
static NSString *HY_apnsCertName = @"";


@implementation AppDelegate (Hyphenate)

- (void)initHyphenate {
    EMOptions *options = [EMOptions optionsWithAppkey:HY_Appkey];
    options.apnsCertName = HY_apnsCertName;
    [[EMClient sharedClient] initializeSDKWithOptions:options];
}

@end
