//
//  AppDelegate+Switch.m
//  ICan
//
//  Created by shilei on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "AppDelegate+Switch.h"
#import "ICN_StartViewController.h"
#import "ICN_SignViewController.h"
#import "JWLaunchAd.h"
#import "ICN_huanxinModel.h"
#import "ICN_MyPersonalHeaderModel.h"

#import "ICN_SignModel.h"
#import "BaseOptionalModel.h"


@implementation AppDelegate (Switch)

-(void)SwitchRootViewController{
    /*
     *  loginStatus 
     *  0：已填写完整信息，可以登录
     *  1：注册完成，但是没填写完整的信息，则跳到登录页    如果再次登录信息不完整的账号会跳转到信息设置界面
     *  2：
     *
     */
    BaseTabBarController *tabbarVC = [[BaseTabBarController alloc] init];

    if ([USERDEFAULT objectForKey:HR_CurrentUserToken] && [USERDEFAULT valueForKey:@"phonehuanxin"]) {
        
        [self huanxin];
        self.window.rootViewController = tabbarVC;
        [self setAdvViewController];
    } else {
        
        ICN_StartViewController *stare=[[ICN_StartViewController alloc]init];
        self.window.rootViewController=stare;
        [self setAdvViewController];
    }
    
}


- (void)setAdvViewController {

    //  1.设置启动页广告图片的URL
    NSString *imgUrlString =@"http://1ican.com/Public/Uploads/201701161333049253337.png";
    

    [JWLaunchAd initImageWithAttribute:3.0 showSkipType:SkipShowTypeDefault setLaunchAd:^(JWLaunchAd *launchAd) {
        __block JWLaunchAd *weakSelf = launchAd;
        
        [weakSelf setAnimationSkipWithAttribute:[UIColor clearColor] lineWidth:1.0 backgroundColor:nil textColor:nil];
        
        [launchAd setWebImageWithURL:imgUrlString options:JWWebImageDefault result:^(UIImage *image, NSURL *url) {
            
        
            //  异步缓冲图片完成后调整图片Frame
            weakSelf.launchAdViewFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        } adClickBlock:^{
            
            //  3.广告回调
            NSString *url = @"http://1ican.com/";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        }];
        
        UIView *backView=[[UIView alloc]init];
        backView.backgroundColor=[UIColor whiteColor];
        [launchAd addSubview:backView];
        [backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.mas_equalTo(launchAd).offset(-1);
            make.centerX.mas_equalTo(launchAd);
            make.width.mas_equalTo(SCREEN_WIDTH);
            make.height.mas_equalTo(SCREEN_HEIGHT*170/568);
        }];
        
        UIImageView *img=[[UIImageView alloc]init];
        img.image=[UIImage imageNamed:@"ic_start_page"];
        [backView addSubview:img];
        [img mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(backView);
            make.centerY.mas_equalTo(backView);
        }];

    }];
}

-(void)huanxin{
    
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_getIndexInfo params:dic success:^(id result) {
        NSDictionary *dict = result[@"result"]
        ;
        ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:dict];
        
        if (model.memberLogo) {
            [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
            [USERDEFAULT synchronize];
        }
        if (model.memberNick) {
            [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
             [USERDEFAULT synchronize];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    
    NSString *str=[[NSUserDefaults standardUserDefaults] valueForKey:HR_CurrentUserToken];

    NSDictionary *dictoken=@{@"token":str};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
        
        ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
        
        if (basemodel.code == 0) {
            //如果获取到接口，进行环信的登录
            
            
            [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                
                [[NSUserDefaults standardUserDefaults] setValue:str forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                

                
            } failure:^(id data) {
                NSLog(@"环信登录失败");
            }];
            
            
        }
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求环信的用户名秘密失败");
    }];
    
}


@end
