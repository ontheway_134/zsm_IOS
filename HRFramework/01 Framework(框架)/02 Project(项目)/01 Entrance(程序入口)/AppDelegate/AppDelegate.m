
#import "AppDelegate.h"
#import "HRAppdelegateProtocol.h"
#import "AppDelegate+Switch.h"
#import "AppDelegate+AliPusher.h"
#import "PPGetAddressBook.h"
#import <UMSocialCore/UMSocialCore.h>
#import "AppDelegate+OtherParty.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import "AppDelegate+Hyphenate.h"
#import "AppDelegate+BMKManager.h"
#import "ICN_AdvertisimentShowListPager.h" // 进入默认跳转的广告页面
#import <UserNotifications/UserNotifications.h> // 适配iOS 10以上设备的通知需要引入的头文件


@interface AppDelegate ()<HRAppdelegateProtocol>

@end

@implementation AppDelegate

#pragma mark - ---------- Lifecycle ----------
//程序开始
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // 设置默认网络类型网络类型为有网络状态  1 有网络
    [USERDEFAULT setValue:@"1" forKey:HR_ICNNETSTATUSKEY];
    
    [NSThread sleepForTimeInterval:1.5];//设置启动页面时间(进入之后先不进行任何操作睡1.5s)
    
    //推送消息的处理
    NSDictionary* remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotification) {
        NSLog(@"推送消息进入");
#warning mark -- 用于处理使用远端推送的方式进入APP
        // 使用消息推送的方式进入APP -- 发个通知
                [[NSNotificationCenter defaultCenter] postNotificationName:HR_REMOTESTART_NOTICATION object:self userInfo:remoteNotification];
        // 点击通知将App从关闭状态启动时，将通知打开回执上报
                [CloudPushSDK sendNotificationAck:launchOptions];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = RGB0X(0x009dff);
    [self createReachAblilityNetWorkManager];
    ICN_AdvertisimentShowListPager *pager = [[ICN_AdvertisimentShowListPager alloc] init];
    self.window.rootViewController = pager;
    [self OtherParty]; // 启动三方平台
    [self initCloudPush]; // 启动阿里云推送
    // iOS 10以上对于通知回调的处理
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [self initHyphenate]; // 初始化环信
    [self createBaiduMapManager]; // 初始化百度地图管理器
    [self registerAPNS:[UIApplication sharedApplication]]; //注册苹果推送，获取deviceToken用于推送
    
    
    
    
    
    
    return YES;
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if (self.isForceLandscape) {
        return UIInterfaceOrientationMaskLandscape;
    }else if (self.isForcePortrait){
        return UIInterfaceOrientationMaskPortrait;
    }
    return UIInterfaceOrientationMaskAll;
}
#pragma mark - ---------- 程序默认回调 ----------

//程序将要进入后台
- (void)applicationWillResignActive:(UIApplication *)application {
}
//程序已经进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
}
//程序将要进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
}
//程序已经进入前台
- (void)applicationDidBecomeActive:(UIApplication *)application {
}
//程序关闭
- (void)applicationWillTerminate:(UIApplication *)application {
}

#pragma mark - ---------- Private Methods ----------
//配置根视图
- (void)configRootViewController {
    
}
//配置网络监测环境
- (void)configNetStatusMoniterEnvironment {
    
    [[HRRequest manager] moniterNetStatus:^(HRNetStatus netStatus) {
        if (netStatus == HRNetStatusUnknown || netStatus == HRNetStatusNotReachable) {
            NSLog(@"%@", NET_NOT_WORK);
        }
    }];
}

#pragma mark - ---------- OpenUrl相关回调 ----------

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }else{
        //进行关于友盟的跳转处理
        [[UMSocialManager defaultManager] handleOpenURL:url];
    }
    return YES;
}


@end
@implementation UINavigationController (Rotation)


- (BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}


- (NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [[self.viewControllers lastObject] preferredInterfaceOrientationForPresentation];
}
@end
