

#import "BaseViewController.h"
#import "ICN_SignViewController.h" // 登录页面
#import "ICN_StartViewController.h" // 登录注册页面
#import "ICN_GuiderPager.h" // 新版引导页
#import "BaseTabBarController.h"
#import "ICN_SetUserInfoViewController.h" // 个人用户完善信息页面
#import "ICN_BusinessMassageViewController.h" // 企业用户完善信息页面
#import "ICN_GetEnterPriseModel.h" // 企业信息Model
#import "ICN_SetUserInfoModel.h" // 个人信息Model
#import "ICN_DynamicStateVC.h" // 发现首页VC
#import "ICN_CareerLifeVC.h" // 职场首页VC
#import "ICN_PersonHubChatVCs.h" // 人脉首页VC
#import "ICN_ActiveityMainViewController.h" // 活动首页VC
#import "ICN_AdvertisimentShowListPager.h"


@interface BaseViewController ()<UITabBarControllerDelegate>

@end

@implementation BaseViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置导航栏
    [self configWithNavigationBarWhileViewDidLoad];
    //监听通知中心
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseALLNoticifation:) name:nil object:nil];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // 添加tabbar代理
    [self configTabarDelegateWhileViewAppear];
    
    // 在视图将要出现的时候设置tabbar的显示状态
    [self configTabbarVisiableStatusWhileViewAppear];
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // 在视图将要消失的时候取消tabbar代理
    [self removeTabbarDelegateAfterViewWillDisappear];
    
    if (self.navigationController != nil) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---------- 重写属性构造器 ----------

- (void)setHiddenDefaultNavBar:(BOOL)hiddenDefaultNavBar{
    _hiddenDefaultNavBar = hiddenDefaultNavBar;
    if (_hiddenDefaultNavBar) {
        if (self.navigationController != nil) {
            if ([self isViewLoaded]) {
                [self.navigationController setNavigationBarHidden:YES animated:YES];
            }
        }
    }
}
#pragma mark - ---------- 私有方法 ----------

// 设置状态为游客登录
- (void)configUserStatusAsVistor{
    [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
    [USERDEFAULT setInteger:HR_ICNVisitor forKey:HR_UserTypeKEY];
}

// 配置导航栏
- (void)configWithNavigationBarWhileViewDidLoad{
    if (self.view.backgroundColor == nil) {
        self.view.backgroundColor = RGB0X(0xf2f2f2);
    }
    [self.navigationController.navigationBar setBarTintColor:RGB0X(0x009cff)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (self.navigationController) {
        self.navigationController.navigationBar.translucent = NO;
    }
}

// 配置tabbar的显示状态
- (void)configTabbarVisiableStatusWhileViewAppear{
    if ([self.navigationController.viewControllers.firstObject isEqual:self]) {
        self.tabBarController.tabBar.hidden = NO;
    }else
        self.tabBarController.tabBar.hidden = YES;
    
    if (self.navigationController != nil && self.hiddenDefaultNavBar == YES ) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
}

// 如果有tabbar的话配置代理
- (void)configTabarDelegateWhileViewAppear{
    if (self.tabBarController) {
        // 存在导航栏
        if (self.tabBarController.delegate) {
            self.tabBarController.delegate = nil;
        }
        // 将现有vc设置为tabbar的代理实现对象
        self.tabBarController.delegate = self;
    }
}

// 有的话取消tabbar的代理设置
- (void)removeTabbarDelegateAfterViewWillDisappear{
    if (self.tabBarController) {
        self.tabBarController.delegate = nil;
    }
}


// 对于用户的登出警告通知
- (void)warnUserWithLoginOut{
    UIAlertController *netWarn = [UIAlertController alertControllerWithTitle:@"异地登录" message:@"您的账号在另一台设备上登录。如非本人操作,则密码可能被泄露,建议修改密码。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *quitAction = [UIAlertAction actionWithTitle:@"退出" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        BaseTabBarController *tabbarVC = [[BaseTabBarController alloc] init];
        // 清空用户token
        [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        
        [UIApplication sharedApplication].keyWindow.rootViewController = tabbarVC;
    }];
    UIAlertAction *reLoginAction = [UIAlertAction actionWithTitle:@"重新登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 是否需要根据用户的状态跳转到不同的登录页面
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        if ([USERDEFAULT valueForKey:HR_LoginTypeKEY]) {
            if ([[USERDEFAULT valueForKey:HR_LoginTypeKEY] integerValue] == 0) {
                // 三方登录
                ICN_GuiderPager *pager = [[ICN_GuiderPager alloc] init];
                [self presentViewController:pager animated:YES completion:nil];
            }else{
                // 非三方登录
                ICN_GuiderPager *starPager = [[ICN_GuiderPager alloc] init]; // 欢迎页面
                ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
                // 先静默从欢迎页跳转到登录页
                [nav pushViewController:pager animated:NO];
                [self presentViewController:nav animated:YES completion:nil];
            }
        }else{
            // 默认跳转非三方登录
            ICN_GuiderPager *starPager = [[ICN_GuiderPager alloc] init]; // 欢迎页面
            ICN_SignViewController *pager = [[ICN_SignViewController alloc] init]; // 登录页
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:starPager];
            // 先静默从欢迎页跳转到登录页
            [nav pushViewController:pager animated:NO];
            [self presentViewController:nav animated:YES completion:nil];
            
        }
    }];
    [netWarn addAction:quitAction];
    [netWarn addAction:reLoginAction];
    [self presentViewController:netWarn animated:YES completion:^{
        
    }];
}

- (void)presentNoneNetWorkWarnAlert{
    // 没有网络的时候设置网络类型为无网络状态 0 无网络 ； 1 有网络
    [USERDEFAULT setValue:@"0" forKey:HR_ICNNETSTATUSKEY];
    // 如果是广告页面不弹出这个
    if ([self isKindOfClass:[ICN_AdvertisimentShowListPager class]]) {
        return ;
    }
    UIAlertController *netWarn = [UIAlertController alertControllerWithTitle:@"当前网络状况不佳" message:@"网络状态不佳,请检测网络" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [netWarn addAction:action];
    [self presentViewController:netWarn animated:YES completion:^{
        
    }];
}
#pragma mark - ---------- 公有方法 -- 供子类集成 ----------

// 跳转到信息补全页面
// 返回ture ： 需要信息不全并且已经跳转到对应的信息补全页面
// 返回false ：不需要信息不全 ： 存在是游客登录状态的可能性
- (BOOL)jumpToUserMessageSetUpPager{
    if ([self isCurrentEnterpriseUser]) {
        // 跳转到企业信息不全页面
        if ([self isCurrentUserMessageChecked] == NO) {
            // 需要信息不全
            // 跳转到企业信息补全页面的方法
            [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
                if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
                    model.result.provinceName == nil || model.result.companyDetail == nil ) {
                    ICN_BusinessMassageViewController *vc = [[ICN_BusinessMassageViewController alloc] init];
                    vc.model = model;
                    vc.notSetUserInfo = YES;
                    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"请求数据失败");
                // 信息校验失败则设置为游客登录
                [self configUserStatusAsVistor];
            }];
            return YES;
        }
    }
    if ([self getCurrentUserLoginStatus] == NO) {
        // 游客登录直接跳转到欢迎页面 -- 不用处理
    }else{
        // 用户登录跳转到用户信息不全页面
         if ([self isCurrentUserMessageChecked] == NO) {
        // 需要信息不全
        // 跳转到个人信息补全页面的方法
        
        [[[HRRequest alloc]init] POST_PATH:PATH_GetMineUserMSG params:nil success:^(id result) {
            
            NSDictionary *dic1 =result[@"result"];
            
            ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                            if (model.memberNick == nil || model.memberLogo == nil
                                || [model.workStatus integerValue] == 2) {
            
                                ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                                [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                                [self.navigationController pushViewController:vc animated:YES];
            
                            }
//            if (model.memberLogo != nil && model.memberNick != nil &&[model.workStatus integerValue] != 2) {
//                ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
//                [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
//                [self.navigationController pushViewController:vc animated:YES];
//            }
        } failure:^(NSDictionary *errorInfo) {
            // 信息校验失败则设置为游客登录
            [self configUserStatusAsVistor];
        }];
        return YES;
        }
    }
    return NO;
}

/** 用于实现rootVC页面跳转的通用方法 */
- (void)currentPagerJumpToPager:(UIViewController *)pager{
    
    if (pager) {
        if (self.navigationController) {
            [self.navigationController pushViewController:pager animated:YES];
        }else
            [self presentViewController:pager animated:YES completion:nil];
    }
    
}

/** 设置 -- 添加用户状态设置为企业用户未登录状态 */
- (void)setCurrentUserBusinessUnIdentificatedStatus{
    [USERDEFAULT setValue:SF(@"%ld",HR_ICNEnterprise) forKey:HR_UserTypeKEY];
    [USERDEFAULT setValue:@"0" forKey:HR_BIdentificationStatusKEY];
}

- (BOOL)isCurrentEnterpriseUser{
    if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
        return YES;
    }
    return NO;
}

- (BOOL)isCurrentUserMessageChecked{
    HRLog(@"--%@--",[USERDEFAULT valueForKey:@"loginStatus"]);
    if ([USERDEFAULT valueForKey:@"loginStatus"]) {
        if ([[USERDEFAULT valueForKey:@"loginStatus"] integerValue] == 0) {
            return YES;
        }
    }
    return NO;
}

/** 判断 -- 当前如果是否是未认证的企业用户状态 */
- (BOOL)isCurrentBusinessUnIdentificatedStatus{
    
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
            // 第一步判断当前是企业用户
            if ([USERDEFAULT valueForKey:HR_BIdentificationStatusKEY]) {
                // 判断用户状态标识存在 -- 不存在默认为已认证
                if ([[USERDEFAULT valueForKey:HR_BIdentificationStatusKEY] integerValue] == 0) {
                    // 当前企业用户状态为未认证
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

/** 判断当前企业用户是否审核通过 -- 如果是个人用户则清空之前的审核数据 */
- (BOOL)getCurrentUserBusinessAuditStatus{
    // 先判断用户类型
    NSInteger userLoginType = -100;
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        userLoginType = [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue];
    }
    switch (userLoginType) {
        case HR_ICNUser:
        case HR_ICNVisitor:{
            // 个人用户登录 、 游客登录
            if (userLoginType == HR_ICNVisitor) {
                // 清空token
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            }
            // 设置清空企业类型
            [USERDEFAULT setValue:nil forKey:HR_BusinessAuditKEY];
            break ;
        }
        case HR_ICNEnterprise:{
            // 企业用户登录
            if ([USERDEFAULT valueForKey:HR_BusinessAuditKEY]) {
                // 存在企业认证类型
                if ([[USERDEFAULT valueForKey:HR_BusinessAuditKEY] integerValue] == 0) {
                    // 未通过审核
                    return NO;
                }else{
                    // 已通过审核
                    return YES;
                }
            }
            break ;
        }
        default:{
            // 无用户状态 -- 设置为游客登录
            [USERDEFAULT setInteger:HR_ICNVisitor forKey:HR_UserTypeKEY];
            // 清空token
            [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        }
            break;
    }
    // 默认返回企业未验证 、 不是企业的状态
    return NO;
}

/** 判断 -- 当前是否是用户登录并在游客登录的时候删除掉本地的token */
- (BOOL)getCurrentUserLoginStatus{
    // 先判断用户的类型key的值存不存在
    NSInteger userLoginType = -100;
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        userLoginType = [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue];
    }
    // 根据userlogintype进行实际的枚举判断
    switch (userLoginType) {
        case HR_ICNUser:
            // 用户登录 如果存在token 确定是用户登录 ，若不存在则设置类型为游客登录
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return YES;
            }else{
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
                return NO;
            }
            break;
        case HR_ICNVisitor:{
            // 当游客登录存在token的时候，清除token
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            }
            return NO;
            break;
        }
        case HR_ICNEnterprise:{
            // 企业登录 如果存在token 确定是企业登录 ，若不存在则设置类型为游客登录
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return YES;
            }else{
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
                return NO;
            }
            break;
        }
            
        default:
            // 默认是不存在用户登录类型的情况，此时需要设置用户登录类型为游客登录
            [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
            [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            break;
    }
    return NO;
}


#pragma mark --- 响应通知方法 ---

- (void)responseALLNoticifation:(NSNotification *)notification{
    if ([notification.name isEqualToString:HR_NONENETWORK_NOTIFICATION]) {
        [self presentNoneNetWorkWarnAlert];
    }
    // 判断当用户被登出的通知发出的时候进行相应操作
    if ([notification.name isEqualToString:HR_USERLOGINOUT_NOTIFICATION]) {
        [self warnUserWithLoginOut];
    }
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


/**
 * 默认所有都不支持转屏,如需个别页面支持除竖屏外的其他方向，请在viewController重新下边这三个方法
 */

// 是否支持自动转屏
- (BOOL)shouldAutorotate
{
    return NO;
}

// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

// 默认的屏幕方向（当前ViewController必须是通过模态出来的UIViewController（模态带导航的无效）方式展现出来的，才会调用这个方法）
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

#pragma mark --- tabbar代理 - UITabBarControllerDelegate ---

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    //    HRLog(@"%ld",tabBarController.selectedIndex);
    UINavigationController *nav = (UINavigationController *)viewController;
    //    HRLog(@"%@",nav.childViewControllers);
    // VC是选中的tabbar对应的导航栏 - 需要判断是不是选中的还是第一个导航栏 - 也就是获取到的导航栏的第一个childVC是不是发现首页的VC
    if (![nav.childViewControllers.firstObject isKindOfClass:[ICN_PersonHubChatVCs class]]) {
        // 点击的不是人脉首页 - 不需要跳转判断
        return YES;
    }else{
        if ([self jumpToUserMessageSetUpPager]) {
            // 跳转到信息补全页面
            return NO;
        }
        return YES;
    }
    return NO;
}


@end

