
/*~!
 | @FUNC  项目基类视图控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseViewController.h"

@interface BaseViewController : HRBaseViewController

/** 设置该属性会在vc存在导航栏自动隐藏默认的导航栏 */
@property (nonatomic , assign)BOOL hiddenDefaultNavBar;

/** 用于跳转的公有方法 */
- (void)currentPagerJumpToPager:(UIViewController *)pager;

/** 判断 -- 当前是否是用户登录并在游客登录的时候删除掉本地的token */
- (BOOL)getCurrentUserLoginStatus;

/** 判断当前用户信息是否校验完全 */
- (BOOL)isCurrentUserMessageChecked;

/** 判断当前企业用户是否审核通过的状态 */
- (BOOL)getCurrentUserBusinessAuditStatus;

/** 判断当前用户是否是企业用户 */
- (BOOL)isCurrentEnterpriseUser;

/** 判断 -- 当前如果是否是未认证的企业用户状态 */
- (BOOL)isCurrentBusinessUnIdentificatedStatus;

/** 设置 -- 添加用户状态设置为企业用户未登录状态 */
- (void)setCurrentUserBusinessUnIdentificatedStatus;

/** 条跳转到信息补全页面 */
- (BOOL)jumpToUserMessageSetUpPager;


@end
