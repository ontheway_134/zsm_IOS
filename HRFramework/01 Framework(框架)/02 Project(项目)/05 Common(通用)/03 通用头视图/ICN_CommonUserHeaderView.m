//
//  ICN_CommonUserHeaderView.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonUserHeaderView.h"
#import "ICN_DynUserBaseMSGModel.h"

@interface ICN_CommonUserHeaderView ()
@property (nonatomic , strong)UIImageView *vplusIcon; // 加v的时候使用的图片

@property (weak, nonatomic) IBOutlet UIButton *p_FriendStateChangeBtn; // 用于修改好友状态的按钮，默认是选中 - 添加好友 ； 未选中 - 删除好友状态

@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像

@property (weak, nonatomic) IBOutlet UIImageView *userSexIcon; // 用户性别标签

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel; // 用户名标签

@property (weak, nonatomic) IBOutlet UILabel *userCompanyLabel; // 用户公司标签

@property (weak, nonatomic) IBOutlet UILabel *userPosicationLabel; // 用户行业标签
@property (weak, nonatomic) IBOutlet UILabel *friendLatitudeLabel; // 好友纬度标签
@property (weak, nonatomic) IBOutlet UILabel *friendsTotalLabel; // 好友总数显示标签





@end

@implementation ICN_CommonUserHeaderView

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = NO; // 默认隐藏图标
        [self addSubview:_vplusIcon];
    }
    return _vplusIcon;
}

- (void)setModel:(ICN_DynUserBaseMSGModel *)model{
    _model = model;
    
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        _vplusIcon.image = [UIImage imageNamed:@"手指橙色"];;
    }else{
        _vplusIcon.image = [UIImage imageNamed:@"手指灰色"];;
    }
    
    // 企业用户 - 隐藏好友纬度和好友数量标签
    if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
        // 是企业用户
        self.friendsTotalLabel.hidden = YES;
        self.friendLatitudeLabel.hidden = YES;
        self.p_FriendStateChangeBtn.hidden = YES;
    }else{
        // 不是企业用户
        
        // 设置好友纬度
        self.friendLatitudeLabel.text = SF(@"%@度",_model.level);
        // 若好友数量为0 隐藏好友总数标签
        if ([_model.friendNum integerValue] == 0) {
            self.friendLatitudeLabel.hidden = NO;
            self.friendsTotalLabel.text = SF(@"ta有0位好友");
        }else{
            self.friendLatitudeLabel.hidden = NO;
            self.friendsTotalLabel.text = SF(@"ta有%@位好友",_model.friendNum);
        }
        // 若好友状态为是自己 不显示好友纬度也不显示好友状态变更按钮
        if (self.model.isMe.integerValue == 1) {
            // 是自己
            self.friendLatitudeLabel.hidden = YES;
            self.p_FriendStateChangeBtn.hidden = YES;
        }
        
        // 根据好友状态判断添加好友按钮的选项
        if (_model.staus == nil || _model.staus == NULL || _model.staus.integerValue == 0) {
            // 不是好友
            self.p_FriendStateChangeBtn.selected = NO;
            self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
        }else{
            // 根据状态判断 (1:未审核[申请者]  2:已审核   3:被拒绝  4:已删除  5:待审核[被申请者] 6:已拒绝 )[没关系则没有此数据]
            switch (_model.staus.integerValue) {
                case 1:{
                    self.p_FriendStateChangeBtn.selected = NO;
                    self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                    [self.p_FriendStateChangeBtn setTitle:@"待审核" forState:UIControlStateNormal];
                    break;
                }
                case 2:{
                    self.p_FriendStateChangeBtn.selected = YES;
                    [self.p_FriendStateChangeBtn setTitle:@"删除好友" forState:UIControlStateSelected];
                    self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                    break;
                }
                case 3:{
                    self.p_FriendStateChangeBtn.selected = NO;
                    [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
                    self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                    break;
                }
                case 4:{
                    self.p_FriendStateChangeBtn.selected = NO;
                    [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
                    self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                    if (self.model.delStatus) {
                        // 存在删除好友状态
                        if (self.model.delStatus.integerValue == 2) {
                            // 双删 可以加好友了
                            self.p_FriendStateChangeBtn.selected = NO;
                            self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                            [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
                        }
                    }
                    break;
                }
                case 5:{
                    self.p_FriendStateChangeBtn.selected = NO;
                    [self.p_FriendStateChangeBtn setTitle:@"待审核" forState:UIControlStateNormal];
                    self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                    break;
                }
                case 6:{
                    self.p_FriendStateChangeBtn.selected = NO;
                    [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
                    self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                    break;
                }
                default:
                    break;
            }
        }
    }

    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"头像"]];
    self.userNameLabel.text = _model.memberNick;
    if (_model.memberGender.integerValue == 1) {
        // 男  没有对应图片
        _userSexIcon.image = [UIImage imageNamed:@"性别男"];
    }else{
        // 女
        _userSexIcon.image = [UIImage imageNamed:@"性别女"];
    }
    if (_model.companyName != nil && ![_model.companyName isEqualToString:@""]) {
        // 公司存在不添加学校
        self.userCompanyLabel.text = _model.companyName;
        self.userPosicationLabel.text = _model.memberProfession;
    }else{
        // 没有公司就添加学校的信息
        self.userCompanyLabel.text = _model.memberSchool;
        self.userPosicationLabel.text = _model.memberMajor;
    }
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    // 设置行业标签圆角
    self.userPosicationLabel.layer.masksToBounds = YES;
    self.userPosicationLabel.layer.cornerRadius = 2.0;
    // 设置用户头像圆角
    self.userLogoIcon.layer.cornerRadius = self.userLogoIcon.width / 2.0;
    self.userLogoIcon.layer.masksToBounds = YES;
    self.userLogoIcon.layer.borderWidth = 1.0;
    self.userLogoIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    // 为好友纬度label添加曲率
    self.friendLatitudeLabel.layer.masksToBounds = YES;
    self.friendLatitudeLabel.layer.cornerRadius = 2.0;
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.userLogoIcon];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.userLogoIcon);
            make.bottom.equalTo(self.userLogoIcon);
        }];
    }
}

#pragma mark - ---------- 共有方法 ----------

- (void)changeFriendStateSuccessWithAddfirend:(BOOL)isAddfriend{
    if (isAddfriend) {
        // 执行添加好友成功后的操作
        self.p_FriendStateChangeBtn.selected = YES;
        [self.p_FriendStateChangeBtn setTitle:@"删除好友" forState:UIControlStateNormal];
    }else{
        self.p_FriendStateChangeBtn.selected = NO;
         [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
    }
}


#pragma mark - ---------- IBAction ----------

- (IBAction)changeFriendStateBtnAction:(UIButton *)sender {
    
    
    if (self.delegate) {
        if (!sender.isSelected) {
            if ([self.delegate respondsToSelector:@selector(responseWithAddCurrentFriendAction)]) {
                [self.delegate responseWithAddCurrentFriendAction];

            }
        }else{
            if ([self.delegate respondsToSelector:@selector(responseWithDeleteCurrentFriendAction)]) {
                [self.delegate responseWithDeleteCurrentFriendAction];
            }
        }
    }
    
}

@end
