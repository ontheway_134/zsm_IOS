//
//  ICN_CommonUserHeaderView.h
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynUserBaseMSGModel;

@protocol CommonUserHeaderViewDelegate <NSObject>

- (void)responseWithDeleteCurrentFriendAction; // 在删除当前好友的时候回调的方法

- (void)responseWithAddCurrentFriendAction; // 在添加当前好友的时候回调的方法

@end

@interface ICN_CommonUserHeaderView : UIView

@property (nonatomic , weak)id<CommonUserHeaderViewDelegate> delegate;
@property (nonatomic , strong)ICN_DynUserBaseMSGModel *model;


- (void)changeFriendStateSuccessWithAddfirend:(BOOL)isAddfriend;

@end
