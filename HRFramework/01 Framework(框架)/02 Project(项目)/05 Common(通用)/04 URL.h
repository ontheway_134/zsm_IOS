
/*~!
 | @FUNC  项目网络请求url
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRURLConstant_h
#define HRURLConstant_h

#import <Foundation/Foundation.h>

#pragma mark - ---------- 协议(protocol) ----------
//协议（http/https）（含“//”后缀，不能为空）
#if DEBUG
static NSString *const URL_PROTOCOL = @"http://";
#else
static NSString *const URL_PROTOCOL = @"http://";
#endif


#pragma mark - ---------- 地址(host) ----------
//地址(host) （不能为空）
#if DEBUG
static NSString *const URL_HOST = @"http://1ican.com/";
//@"192.168.1.36/zsm/"
//1ican.com/
#else
static NSString *const URL_HOST = @"http://1ican.com/";
#endif
//http://1ican.com/index.php/
#pragma mark - ---------- 端口(port) ----------
//端口（port），（含“:”前缀，如果 URL_PORT 为空，则不含）
#if DEBUG
static NSString *const URL_PORT = @"";
#else
static NSString *const URL_PORT = @"";
#endif

#pragma mark - ---------- 路径(path) ----------
//路径通用前缀，（含后缀“/” ，如果 URL_PREFIX 为空， 则不含）
static NSString *const URL_PATH_PREFIX = @"index.php/";


//XXXX
static NSString *const PATH_XXXX = @"PATH_XXXX"; // ⚠️：变量名称全部大写，用下划线分割

#pragma mark - ---------- 支付 ----------

// 微信支付获取订单接口 Home/WeixinPay/getInfo
static NSString *const PATH_WeiChatPAYOrder = @"Home/WeixinPay/appPayIos";
// 微信支付成功后的后台回调
static NSString *const PATH_WeiChatOrderRecall = @"Home/WeixinPay/changeActivityOrder";
// 支付宝获取订单接口
static NSString *const PATH_AliPayOrder = @"Home/AliPay/doalipayAPP";

#pragma mark - ---------- 用户登录注册部分 ----------

// 验证是否需要屏蔽三方登录按钮
static NSString *const PATH_HiddenThird = @"Login/Login/isHidden";

//验证邮箱是否存在

static NSString *const PATH_EMIALEXIT = @"Login/Register/isEmailExists";

//验证手机号是否存在
static NSString *const PATH_PHONEISTEIT = @"/Login/Register/isExists";

//邮箱验证
static NSString *const PATH_EMIAL = @"/Member/Member/sendEmail";
//找回密码邮箱的验证
static NSString *const PATH_CHECKEMAIL = @"/Login/Profile/doVerifyEmail";


//获取验证码注册时
static NSString *const PATH_PhoneCode = @"/Login/Profile/doVerifyMobile";
//注册
static NSString *const PATH_USERREGISTER = @"/Login/Register/doReg";
//登录
static NSString *const PATH_USERLOGIN = @"/Login/Login/doLogin";
//退出登录
static NSString *const PATH_USERLOGINOUT = @"/Login/Login/doLogout";
//找回密码
static NSString *const PATH_USERFINDPASSWOED = @"/Login/Profile/doRetrievePassword";
//修改密码
static NSString *const PATH_USERCHANGE = @"/Login/Profile/doProfile";
//验证码
static NSString *const PATH_CODE = @"/Member/Member/sendSms";

#pragma mark - ---------- 人脉部分接口 ----------

// 获取小组中全部成员的id和信息的接口
static NSString * const PATH_HXGroupIdList = @"MyFriend/HxTalk/chatgroupsMumbers";
// 获取好友的环信ID
static NSString *const PATH_HXFriendID = @"MyFriend/MemberInfo/get_huanxinId";

//可能感兴趣的人 6
static NSString *const PATH_INTERPEOPLE = @"MyFriend/Index/fmaybeInterested";
//创建小组接口  9
static NSString *const PATH_CREATEGROUP=@"MyFriend/Group/gcreate";
//好友/新的好友列表 1
static NSString *const PATH_NEWFRIEND = @"MyFriend/Index/findex";
//删除好友接口 5
static NSString *const PATH_DELETEFRIDENT = @"MyFriend/Index/fdelFriend";
//添加好友申请 3
static NSString *const PATH_ADDFRIDENT = @"MyFriend/Index/faddFriend";
//审核好友列表   4
static NSString *const PATH_CHECKFRIDENT =@"MyFriend/Index/freview";
//我的小组   10
static NSString *const PATH_MYGROUP=@"MyFriend/Group/gmy_group";
//发现更多小组  11
static NSString *const PATH_MYMOREGROUP=@"MyFriend/Group/gmore_group";
//小组详情  16
static NSString *const PATH_MYGROUPDETIAL=@"MyFriend/Group/ggroup_info_pc";
//申请加入小组   12
static NSString *const PATH_APPLYGROUP=@"MyFriend/Group/gapply_group";
//编辑小组   16
static NSString *const PATH_EDITGROUP = @"MyFriend/Group/geditor_group";
//搜索好友 2
static NSString *const PATH_SEEKFRIDENT = @"MyFriend/Index/fsearch_friend";
//退出小组 15
static NSString *const PATH_WUITGTROUP = @"MyFriend/Group/gquit_group";
//系统推荐用户接口  7
static NSString *const PATH_SYSTEM = @"MyFriend/Index/frecommend";   //未完成
//验证小组是否存在接口   8
static NSString *const PATH_VERIFY = @"MyFriend/Group/gcheck_name";   //未完成
//审核用户加入小组申请接口:(同意+拒绝)  13    88888888888888888888888888888888888888888888888888
static NSString *const PATH_CHECKGROUP = @"MyFriend/Group/greview_group";   //未完成
//踢出组员接口   14
static NSString *const PATH_KICTGROUP = @"MyFriend/Group/gkickout_group";    //踢出组员
//判断新最大组员数是否小于当前组员数接口  15
static NSString *const PATH_JUDEGGROPU = @"MyFriend/Group/gis_small";
//获取融云用户token:
static NSString *const PATH_TOKRN = @"MyFriend/MemberInfo/get_rongtoken";
//申请加入小组列表接口
static NSString *const PATH_APPLYGROUPSS = @"MyFriend/Group/gapplylist";
//三方登录的接口
static NSString *const PATH_THIRDSREGIST = @"Login/Login/doThirdLogin";
//
static NSString *const PATH_XXCCCCCC =@"MyFriend/Group/gmyapplylist";

static NSString *const PATH_GROUPCHAT = @"MyFriend/Group/gcheck_group";

#pragma mark ----------------职场成长---------------------
static NSString *const PATH_postionclassSect = @"MyPosition/PositionClass/postionclassSect";

#pragma mark - ---------- 企业中心部分接口 ----------

// 获取企业中心接口
static NSString *const PATH_GETENTERPRISEMESSAGE = @"Enterprise/Enterprise/getEnterpriseInfo";
// 更新企业基本信息接口
static NSString *const PATH_UPDATEENTERPRISEMESSAGE = @"Enterprise/Enterprise/setEnterpriseInfo";
// 企业中心行业接口
static NSString *const PATH_ERPISEINFO = @"Enterprise/EnterprisePosition/doPositionLinkage";
// 更新企业中心简历筛选的接口
static NSString *const PATH_RESUMESIZER = @"Enterprise/EnterprisePosition/doPositionLinkage";
// 简历搜索接口
static NSString *const PATH_RESUMESEARCH = @"Enterprise/EnterpriseResume/doSearchMemberResume";

// 收到的简历接口
static NSString *const PATH_RECEIVEDRESUME = @"Enterprise/EnterpriseResume/resumeReceiveList";
// 收藏的简历接口
static NSString *const PATH_COLLECTEDRESUME = @"Enterprise/EnterpriseResume/collectResumeList";
// 合适的简历接口
static NSString *const PATH_SUITABLERESUME = @"Enterprise/EnterpriseResume/resumeFittedList";
// 删除收藏的简历接口
static NSString *const PATH_DELETECOLLECTIONRESUME = @"Enterprise/EnterpriseResume/doModifyCollection";

//已发布职位接口
static NSString *const PATH_POSITIONLIST = @"Enterprise/EnterprisePosition/positionList";

//收藏简历的接口
static NSString *const PATH_DOADDCOLLECTION = @"Enterprise/EnterpriseResume/doAddCollection";
//取消收藏简历的接口
static NSString *const PATH_DODELETECOLLECTION = @"Enterprise/EnterpriseResume/doDeleteCollection";
//搜索简历的接口
static NSString *const PATH_DOSEARCHMEMBERRESUME = @"Enterprise/EnterpriseResume/doSearchMemberResume";

//企业查看教育经历
static NSString *const PATH_OtherEducationExperienceList = @"Member/EducationExperience/otherEducationExperienceList";
//企业查看培训经历
static NSString *const PATH_OtherTrainExperienceList = @"Member/TrainExperience/otherTrainExperienceList";
//企业查看工作经历
static NSString *const PATH_OtherWorkExperienceList = @"Member/WorkExperience/otherWorkExperienceList";
//企业查看用户技能标签
static NSString *const PATH_OtherMemberSkillList = @"Member/MemberSkills/otherMemberSkillList";
//企业查看用户基本信息
static NSString *const PATH_OtherMemberInfo = @"Member/Member/otherMemberInfo";
//企业是否收藏了
static NSString *const PATH_IsCollectResume = @"Enterprise/EnterpriseResume/isCollectResume";
//企业标记简历合适或不合适
static NSString *const PATH_DoFittedResume = @"Enterprise/EnterpriseResume/doFittedResume";
//企业资料图片获取
static NSString *const PATH_GetApprove = @"Api/Company/getApprove";

//企业是否通过审核
static NSString *const PATH_isAudit = @"Enterprise/Enterprise/isAudit";

//企业被查看的简历
static NSString *const PATH_doBrowseResume = @"Enterprise/EnterpriseResume/doBrowseResume";

#pragma mark - ---------- 首页 - 动态 ----------

// 分享接口
static NSString *const PATH_DynamicSHARE = @"Api/Dynamic/shareDynamic";
// 首页动态接口
static NSString *const PATH_DynamicStateFP = @"Api/home/homeDynamic";
// 首页广告接口
static NSString *const PATH_DynamicAD = @"Api/home/advertisement";
// 获取动态评论
static NSString *const PATH_DynamicComment = @"Api/home/comments";
// 发布动态评论
static NSString *const PATH_DynStateCommentPublication = @"Api/home/doComments";
// 智讯评论
static NSString *const PATH_WisdomCommentPublication = @"Api/Topic/doTalk";
// 点赞/取消点赞
static NSString *const PATH_DynamicStateLikeUp = @"Api/dynamic/likeUp";
// 转发到动态
static NSString *const PATH_ReplyDynamicState = @"Api/dynamic/forward";
// 动态举报
static NSString *const PATH_DynamicStateReport = @"Api/dynamic/reportDynamic";
// 搜索热词
static NSString *const PATH_HotWorlds = @"Api/home/search";
// 搜索动态列表
static NSString *const PATH_SearchDynList = @"Api/home/dynamicSearch";
// 小组搜索列表
static NSString *const PATH_SearchGROUPList = @"MyFriend/Group/ggroup_search";
// 地理信息接口
static NSString *const PATH_Location = @"Member/Common/doCityLinkage";
// 动态详情接口
static NSString *const PATH_DynamicDetial = @"Api/home/dynamicDetails";
#pragma mark --- 首页智讯 ---
// 我的智讯
static NSString *const PATH_MineTopic = @"Api/Topic/myTopic";
// 首页智讯接口
static NSString *const PATH_WisdomMessage = @"Api/Topic/homeTopic";

#pragma mark --- 发布 ---
// 多图上传 - 先于智讯发布
static NSString *const PATH_MultipleImagesUpload = @"Api/Upload/formUploads";
// 发布智讯
static NSString *const PATH_WisdomMessagePublication = @"Api/Topic/addTopic";
// 发布动态
static NSString *const PATH_DynamicPublication = @"Api/home/addDynamic";
// 发布动态 - 谁不可看(好友列表)
static NSString *const PATH_DynFriendsList = @"MyFriend/Index/findex";


#pragma mark --- 公司详情 ---
static NSString *const PATH_DynDetialCompany = @"Enterprise/Enterprise/otherEnterpriseInfo";
static NSString *const PATH_DynAddCompanyAttention = @"Member/MemberConcern/doAddConcern";
static NSString *const PATH_DynDeleteCompanyAttention = @"Member/MemberConcern/doDeleteConcern";
static NSString *const PATH_DynCompanyAttentionStatus = @"Member/MemberConcern/isConcerned"; // 企业关注状态

#pragma mark --- 首页 - 用户主页 ---
// 用户主要信息
static NSString *const PATH_UserMainMsg = @"MyFriend/MemberInfo/gmember_info";
// 用户共同好友
static NSString *const PATH_UserComFriends = @"MyFriend/MemberInfo/gsame_friend";
// 用户技能标签
static NSString *const PATH_UserSkillSign = @"Member/MemberSkills/otherMemberSkillList";
// 用户主页动态
static NSString *const PATH_UserDynamicState = @"Api/dynamic/hisHomeDynamic";
// 用户参加的活动
static NSString *const PATH_UserActivities = @"MyPosition/Activity/ActivityListmember";
// 用户参加的小组
static NSString *const PATH_UserGroups = @"MyFriend/Group/gmy_group";
// 用户的工作经历
static NSString *const PATH_UserWorkExperience = @"Member/WorkExperience/otherWorkExperienceList";
// 用户的教育经历
static NSString *const PATH_UserEduExperience = @"Member/EducationExperience/otherEducationExperienceList";
// 用户举报
static NSString *const PATH_UserReport = @"MyFriend/MemberInfo/gmember_report";
// 添加好友接口
static NSString *const PATH_UserAddFriend = @"MyFriend/Index/faddFriend";
// 删除好友接口
static NSString *const PATH_UserDeleteFriend = @"MyFriend/Index/fdelFriend";
// 我的动态
static NSString *const PATH_MainDynamic = @"Api/dynamic/myDynamic";

#pragma mark - ---------- 个人中心 ----------
//设置用户信息接口
static NSString *const PATH_MySaveTheMessage = @"Member/Member/setMemberInfo";
//活动列表接口
static NSString *const PATH_ActivityListmember = @"MyPosition/Activity/ActivityListmember";
//删除活动列表接口
static NSString *const PATH_Activitydel = @"MyPosition/ActivityPay/Activitydel";
//所属行业设置接口
static NSString *const PATH_setIndustry = @"Api/CompanytIndustry/setIndustry";
//所属行业获取接口
static NSString *const PATH_getIndustry = @"Api/CompanytIndustry/getIndustry";
//删除动态
static NSString *const PATH_delDynamic = @"Api/Dynamic/delDynamic";
//删除智讯
static NSString *const PATH_delTopic = @"Api/Topic/delTopic";

/*单张图片上传*/
static NSString *const PATH_FormUpload = @"Api/Upload/formUpload";

#pragma mark - ---------- 我的简历 ----------

//发布添加工作经历
//Member/WorkExperience/doAddWorkExperience
static NSString *const PATH_MyWorkingExperience = @"Member/WorkExperience/doAddWorkExperience";

//删除工作经历
//http://zsm.4pole.cn/index.php/Member/WorkExperience/doDeleteWorkExperience
static NSString *const PATH_MyWorkingDeleteExperience = @"Member/WorkExperience/doDeleteWorkExperience";

//修改工作经历
//http://zsm.4pole.cn/index.php/Member/WorkExperience/doDeleteWorkExperience
static NSString *const PATH_MyWorkingEditExperience = @"Member/WorkExperience/doModifyWorkExperience";
static NSString *const PATH_AddApprove = @"Api/Company/addApprove";

//发布添加教育经历
static NSString *const PATH_MyEducationExperience = @"Member/EducationExperience/doAddEducationExperience";

//删除教育经历
static NSString *const PATH_MyEducationDeleteExperience = @"Member/EducationExperience/doDeleteEducationExperience";

//修改教育经历
static NSString *const PATH_MyEducationEditExperience = @"Member/EducationExperience/doModifyEducationExperience";

// 根据学历级别获取专业Id
//index.php/Member/Common/getQualificationList
static NSString *const PATH_GETDeucationLevel = @"index.php/Member/Common/getQualificationList";

//发布添加培训经历
static NSString *const PATH_MyTrainExperience = @"Member/TrainExperience/doAddTrainExperience";

//删除培训经历
static NSString *const PATH_DeleteTrainExperience = @"Member/TrainExperience/doDeleteTrainExperience";

//修改培训经历
static NSString *const PATH_MyWorkingEditTrainEx = @"Member/TrainExperience/doModifyTrainExperience";
//添加技能标签
static NSString *const PATH_MySkillEditTrainEx = @"Member/MemberSkills/doAddMemberSkill";
//删除技能标签
static NSString *const PATH_MySkillDeleteTrainEx = @"Member/MemberSkills/doDeleteMemberSkill";

static NSString *const PATH_MemberSkillList = @"Member/MemberSkills/memberSkillList";

//删除用户收藏职位
static NSString *const PATH_doDeletePositionCollection = @"Member/MemberCollection/doDeletePositionCollection";
//删除用户收藏咨询
static NSString *const PATH_mypositionNewsCollectDel = @"MyPosition/PositionNews/mypositionNewsCollectDel";
//取消关注
static NSString *const PATH_doDeleteConcern = @"Member/MemberConcern/doDeleteConcern";
//投递反馈删除
static NSString *const PATH_doDeleteResume = @"Member/MemberResume/doDeleteResume";
//设置简历是否公开
static NSString *const PATH_isResumeOpened = @"Member/MemberResume/isResumeOpened";

#pragma mark - ---------- 收藏 ----------
//删除收藏
static NSString *const PATH_MyDeletePositionCollection = @"Member/MemberCollection/doDeletePositionCollection";

#pragma mark - ---------- 消息 ----------
//用户删除消息
static NSString *const PATH_MyDeletedoDeleteMessage = @"Member/MemberMessage/doDeleteMessage";
#pragma mark - ---------- 设置 ----------
//意见反馈
//index.php/Setup/MemberNote/doAddNote
static NSString *const PATH_MyFeedBack = @"Setup/MemberNote/doAddNote";
//用户手机绑定
static NSString *const PATH_MyPhoneBinding = @"Setup/MemberAuth/doBindToMobile";
//修改密码
static NSString *const PATH_MyChangePassword = @"Login/Profile/doProfile";
//是否绑定手机号
static NSString *const PATH_memberMobileInfo = @"Setup/MemberAuth/memberMobileInfo";


#pragma mark - ---------- 其他（others） ----------
//职位搜索
static NSString *const PATH_SupplementTermlist = @"MyPosition/Supplement/SupplementTermlist";
#pragma mark 图片路径通用前缀
//包括协议、地址、端口号...。含“/”，如果 URL_IMG_PREFIX 为空，则不含。
static NSString *const URL_IMG_PREFIX = @"<#xxx/#>";
static NSInteger const CODE_S = 59;

#endif /* HRURLConstant_h */
