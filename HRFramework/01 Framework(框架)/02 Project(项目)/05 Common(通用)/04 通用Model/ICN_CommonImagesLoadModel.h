//
//  ICN_CommonImagesLoadModel.h
//  ICan
//
//  Created by albert on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_CommonImagesLoadModel : BaseOptionalModel

@property (nonatomic , retain)NSDictionary *result;


@end
