//
//  ICN_FirendListCell.h
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_FirendListCell : UITableViewCell

@property (nonatomic , assign , getter=isfriendAlready)BOOL alreadyFriend; // 判断当前是否是好友的属性


@end
