//
//  ICN_FirendListCell.m
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_FirendListCell.h"

@interface ICN_FirendListCell ()


@property (weak, nonatomic) IBOutlet UIButton *p_AddFriendBtn; // 用于添加好友的btn


@end

@implementation ICN_FirendListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - ---------- set方法 ----------

- (void)setAlreadyFriend:(BOOL)alreadyFriend{
    _alreadyFriend = alreadyFriend;
    // 已经是好友的时候隐藏添加好友按钮，不是好友的时候显示
    if (_alreadyFriend) {
        self.p_AddFriendBtn.hidden = YES;
    }else
        self.p_AddFriendBtn.hidden = NO;
}


@end
