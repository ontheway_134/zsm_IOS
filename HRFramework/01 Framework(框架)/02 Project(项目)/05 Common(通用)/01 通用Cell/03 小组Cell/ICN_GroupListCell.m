//
//  ICN_GroupListCell.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_GroupListCell.h"
#import "ICN_DynGroupModel.h"

@interface ICN_GroupListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *groupIcon;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *peopleCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;


@end

@implementation ICN_GroupListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setModel:(ICN_DynGroupModel *)model{
    _model = model;
    [self.groupIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.groupLogo)] placeholderImage:[UIImage imageNamed:@"人物头像"]];
    self.groupNameLabel.text = _model.groupName;
    self.peopleCountLabel.text = _model.number;
    self.signLabel.text = _model.summary;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.groupIcon.layer.cornerRadius = self.groupIcon.width / 2.0;
    self.groupIcon.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
