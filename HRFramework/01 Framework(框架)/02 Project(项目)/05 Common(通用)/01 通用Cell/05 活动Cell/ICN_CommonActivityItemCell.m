//
//  ICN_CommonActivityItemCell.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonActivityItemCell.h"
#import "ICN_DynActivityModel.h"

@interface ICN_CommonActivityItemCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titmeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *contentIcon;

@end

@implementation ICN_CommonActivityItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setModel:(ICN_DynActivityModel *)model{
    _model = model;
    self.titleLabel.text = _model.title;
    self.contentLabel.text = _model.include;
    if (_model.price != nil && ![_model.price isEqualToString:@""] && [_model.price integerValue] == 0) {
        self.priceLabel.text = @"免费";
    }else{
        self.priceLabel.text = SF(@"￥ %@",_model.price);
    }
    self.titmeLabel.text = _model.beginDate;
    [self.contentIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.pic)] placeholderImage:[UIImage imageNamed:@"活动图片"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
