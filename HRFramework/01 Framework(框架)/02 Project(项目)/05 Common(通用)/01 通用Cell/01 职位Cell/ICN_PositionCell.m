//
//  ICN_PositionCell.m
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionCell.h"

@implementation ICN_PositionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
