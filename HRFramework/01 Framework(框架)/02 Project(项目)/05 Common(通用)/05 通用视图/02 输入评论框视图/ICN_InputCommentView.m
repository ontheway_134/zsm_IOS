//
//  ICN_InputCommentView.m
//  ICan
//
//  Created by albert on 2017/3/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_InputCommentView.h"

@interface ICN_InputCommentView ()<UITextFieldDelegate>


@property (nonatomic , copy) NSString *hisComment; // 上一次提交的评论信息


@end

@implementation ICN_InputCommentView

+ (instancetype)loadCommentView{
    // 创建初始化控件
    ICN_InputCommentView *view = XIB(ICN_InputCommentView);
    // 设置文本框的代理
    view.commentTextField.delegate = self;
    return view;
}

- (void)reSetHisCommentText{
    if (self.hisComment) {
        self.commentTextField.text = self.hisComment;
    }
}


- (IBAction)clickOnSendCommentBtn:(UIButton *)sender {
    if (self.commentTextField.text) {
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(responseWithCommitComment:)]) {
                [self.delegate responseWithCommitComment:self.commentTextField.text];
                // 在提交完评论内容后清空
                self.hisComment = self.commentTextField.text;
                self.commentTextField.text = nil;
            }
        }
    }
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextFieldDelegate ---



@end
