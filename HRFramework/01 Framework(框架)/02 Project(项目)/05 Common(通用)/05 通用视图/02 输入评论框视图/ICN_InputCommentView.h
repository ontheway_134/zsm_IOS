//
//  ICN_InputCommentView.h
//  ICan
//
//  Created by albert on 2017/3/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommentTextDelegate <NSObject>

// 用于提交评论的方法
- (void)responseWithCommitComment:(NSString *)comment;

@end

@interface ICN_InputCommentView : UIView

@property (weak, nonatomic) IBOutlet UITextField *commentTextField; // 评论的文本框

@property (nonatomic , weak)id<CommentTextDelegate> delegate; // 设置当前页面的代理属性

// 加载页面的方法
+ (instancetype)loadCommentView;

// 用于在评论内容没有正确提交的时候重设评论历史内容的方法
- (void)reSetHisCommentText;


@end
