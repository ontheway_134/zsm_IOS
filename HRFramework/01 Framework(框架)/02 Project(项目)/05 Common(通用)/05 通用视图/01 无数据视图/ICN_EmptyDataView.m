//
//  ICN_EmptyDataView.m
//  ICan
//
//  Created by albert on 2017/2/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_EmptyDataView.h"

@interface ICN_EmptyDataView ()

@property (weak, nonatomic) IBOutlet UIImageView *showImage;
@property (weak, nonatomic) IBOutlet UILabel *warnLabel;


@end

@implementation ICN_EmptyDataView

- (instancetype)loadXibWithMessage:(NSString *)message DefaultHidden:(BOOL)hidden{
    ICN_EmptyDataView *view = XIB(ICN_EmptyDataView);
    if (view) {
        if (message) {
            view.warnLabel.text = message;
        }
        if (hidden) {
            view.hidden = YES;
            self.viewHidden = hidden;
        }
    }
    return view;
}


- (void)setViewHidden:(BOOL)viewHidden{
    _viewHidden = viewHidden;
    if (viewHidden) {
        self.hidden = YES;
    }
}

@end
