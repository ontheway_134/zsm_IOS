//
//  ICN_CommonSingleTitleHeader.h
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ComSingleTitleSectionView : UITableViewHeaderFooterView

@property (nonatomic , copy)NSString *headerTitle ; //标题内容

@end
