
/*~!
 | @FUNC  项目常量
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef Constant_h
#define Constant_h

/*
    //举例
    //是否是第一次进入程序
    static NSString *const USERDEFAULT_FIRST_ENTER = @"USERDEFAULT_FIRST_ENTER"; // ⚠️变量名称全部大写，多个单词用下划线分隔
 */

#pragma mark - ---------- 偏好设置（Userdefault）----------


#pragma mark - ---------- 解归档（Archive）----------


#pragma mark - ---------- 通知（Notification）----------


#pragma mark - ---------- 颜色 (Color) ----------


#pragma mark - ---------- 字体(Font) ----------


#pragma mark - ---------- 其他（Others） ----------

static NSString *const NET_NOT_WORK = @"网络断开连接，请检查网络";

static NSString *const ERROR_MESSAGE = @"网络请求失败";

#pragma mark - ---------- 三方注册常量 ----------

static NSString * const ICN_BaiduLBSUserID = @"baidu_mapsdk_radarid"; // 百度周边雷达使用的userId

#pragma mark - ---------- 登录注册提示信息 ----------

static NSString *const PHONENUMBER = @"请输入手机号";

static NSString *const PHONECODE = @"请输入验证码";

static NSString *const PASSWORD = @"请输入密码";

static NSString *const REPARTPASSWORD = @"请输入重复密码";

static NSString *const ISQUSENO = @"两次密码不一致";


static NSString *const TUREPHONE = @"请输入正确的手机号";

#pragma mark - ---------- 用户数据 ----------

// 用户昵称存储字段
static NSString * const ICN_UserNick = @"userNick";

// 用户头像网址存储字段
static NSString * const ICN_UserIconUrl = @"userLogoUrl";

// 用户的环信ID
static NSString * const ICN_UserHXId = @"huanxinId";


#endif /* ProjectCommon_h */
