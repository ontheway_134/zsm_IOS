
/*~!
 | @FUNC  View设置圆角
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <UIKit/UIKit.h>

@interface UIView (Rounded)

//1.0 设置圆角
- (void)roundedCorners:(CGFloat)radius;

@end
