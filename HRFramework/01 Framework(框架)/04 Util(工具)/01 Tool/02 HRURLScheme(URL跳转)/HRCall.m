
#import "HRCall.h"
#import <UIKit/UIKit.h>

@implementation HRCall

+ (void)callPhoneNumber:(NSString *)phoneNumber alert:(BOOL)alert {
    NSString *url = @"";
    if (alert) {
        url = [NSString stringWithFormat:@"telprompt://%@", phoneNumber];
    } else {
        url = [NSString stringWithFormat:@"tel:%@", phoneNumber];
    }
    OPEN_URL(url);
}


@end
