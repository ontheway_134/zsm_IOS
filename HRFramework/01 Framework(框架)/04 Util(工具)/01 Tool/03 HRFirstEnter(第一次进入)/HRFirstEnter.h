
/*~!
 | @FUNC  第一次进入程序
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import <Foundation/Foundation.h>

@interface HRFirstEnter : NSObject

//1.0 第一次进入程序block回调
+ (void)isFirst:(void(^)())handle;

@end
