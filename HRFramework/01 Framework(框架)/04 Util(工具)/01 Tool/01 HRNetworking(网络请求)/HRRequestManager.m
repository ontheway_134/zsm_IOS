

#import "HRRequestManager.h"
#import "BaseOptionalModel.h"
#import <CloudPushSDK/CloudPushSDK.h>

@implementation HRRequestManager

#pragma mark - ---------- Lifecycle ----------

#pragma mark - ---------- Public Methods ----------
#pragma mark GET请求
- (void)GET_URL:(NSString *)url params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodGet url:url params:params progress:nil success:success failure:failure];
}

#pragma mark POST请求
- (void)POST_URL:(NSString *)url params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodPost url:url params:params progress:nil success:success failure:failure];
}

#pragma mark 表单请求
- (void)FORM_URL:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    [self requestWay:HRHttpRequestMethodForm url:url params:params progress:progress success:success failure:failure];
}

#pragma mark - ---------- Private Methods ----------
//处理请求
- (void)requestWay:(HRHttpRequestMethod)method url:(NSString *)url params:(NSDictionary *)params progress:(void(^)(NSProgress *progress))progress success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    self.requestUrl = url;
    self.requestParams = params;
    
    // 判断当前为无网络状态则直接返回网络请求失败
    if ([[USERDEFAULT valueForKey:HR_ICNNETSTATUSKEY] integerValue] == 0) {
        if (failure) {
            failure(nil);
        }
        return ;
    }
    
    [self requestProgress:^(NSProgress *p) {
        //进度回调
        if (progress) {
            progress(p);
        }
    } success:^(id responseObject) {
        if (success) {
            // 添加对于获取数据的code的判断处理
            BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:responseObject error:nil];
            if (model.code == 2) {
                // 网络请求到登出数据，，需要登出 并且删除全部的别名
                [CloudPushSDK removeAlias:nil withCallback:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:HR_USERLOGINOUT_NOTIFICATION object:self];
            }
            success(responseObject);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error.userInfo);
        }
    }];
}

@end
