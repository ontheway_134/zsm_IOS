

#import "HRNetworkingManager.h"
#import "AFNetworking.h"
#import "BaseOptionalModel.h"
#import "ICN_SelsectVIew.h"
#import "ICN_StartViewController.h"
#import "ICN_SignViewController.h"
@implementation HRNetworkingManager

#pragma mark - ---------- Public Methods ----------

#pragma mark - ---------- 图片上传方法 ----------

- (void)UPLOAD_IMAGES:(NSArray<UIImage *> *)imageArr
                 Path:(NSString *)path
             progress:(ProgressBlock)progress
              success:(SuccessBlock)success
              failure:(ErrorBlock)failure{

    NSString *PATH= composeUrl(path);
    NSDictionary *parma = @{
                            @"sessionid" : @"ssssss",
                            @"imagetype" : @"image"
                            };
    //处理显示请求信息
    [self handleShowRequestMessageUrlWithPath:path params:nil];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/plain",
                                                         @"text/javascript",
                                                         @"text/json",
                                                         @"text/html",
                                                         @"image/jpeg", nil];
    [manager POST:PATH parameters:parma constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(imageArr){
            __block NSInteger i = 0;
            
            [imageArr enumerateObjectsUsingBlock:^(UIImage * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                //压缩图片
                i++;
                NSData *imageData = UIImageJPEGRepresentation(obj, 0.3);
                
                //压缩图片
                
                //拼接图片名称+时间确保不重复命名
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                formatter.dateFormat = @"yyyyMMddHHmmss";
                NSString *imageFileName = [NSString stringWithFormat:@"%@%ld.jpg", [formatter stringFromDate:[NSDate date]],(long)i];
                // 测试输出文件名
                HRLog(@"图片文件名 ---%@",imageFileName);
                //上传图片，以文件流的格式
                [formData appendPartWithFileData:imageData name: [NSString stringWithFormat:@"uploadFile%ld",(long)i] fileName:imageFileName mimeType:@"image/png"];
                
//                [formData appendPartWithFileData:imageData name:@"file" fileName:imageFileName mimeType:@"image/JPG"];
                
            }];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        HRLog(@"请求结果=%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"\n[Request Failure]:\n[Failure Code]:%ld\n[Failure Message]:%@", (long)error.code, error.userInfo);
        failure(error.userInfo);
    }];
}
#pragma mark 单张图片上传
-(void)UPLOAD_IMAGE:(UIImage *)image imagetype:(NSString *)path progress:(ProgressBlock)progress success:(SuccessBlock)success failure:(ErrorBlock)failure{

    NSString *PATH= composeUrl(path);
    NSDictionary *parma = @{
                            @"sessionid" : @"ssssss",
                            @"imagetype" : @"image"
                            };
    //处理显示请求信息
    [self handleShowRequestMessageUrlWithPath:path params:nil];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",
                                                         @"text/plain",
                                                         @"text/javascript",
                                                         @"text/json",
                                                         @"text/html",
                                                         @"image/jpeg", nil];
    [manager POST:PATH parameters:parma constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSLog(@"%@",image);
        if(image){

                //压缩图片
                NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
                
                //压缩图片
                
                //拼接图片名称+时间确保不重复命名
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                formatter.dateFormat = @"yyyyMMddHHmmss";
                NSString *imageFileName = [NSString stringWithFormat:@"%@.jpg", [formatter stringFromDate:[NSDate date]]];
                // 测试输出文件名
                HRLog(@"图片文件名 ---%@",imageFileName);
                //上传图片，以文件流的格式
                [formData appendPartWithFileData:imageData name: [NSString stringWithFormat:@"uploadFile"] fileName:imageFileName mimeType:@"image/png"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (progress) {
            progress(uploadProgress.completedUnitCount, uploadProgress.totalUnitCount);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        HRLog(@"请求结果=%@",responseObject);
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"\n[Request Failure]:\n[Failure Code]:%ld\n[Failure Message]:%@", (long)error.code, error.userInfo);
        failure(error.userInfo);
    }];

}
#pragma mark GET请求
- (void)GET_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    //处理显示请求信息
    [self handleShowRequestMessageUrlWithPath:path params:params];
    
    [self GET_URL:composeUrl(path) params:params success:^(id result) {
        //处理显示请求成功信息
        [self handleResultMessage:result];
        //成功回调
        if (success) {
            success(result);
        }
    } failure:^(NSDictionary *errorInfo) {
        //处理显示请求失败信息
        [self handleResultMessage:errorInfo];
        //失败回调
        if (failure) {
            failure(errorInfo);
        }
    }];
}

#pragma mark POST请求
- (void)POST_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure {
    //处理显示请求信息
    [self handleShowRequestMessageUrlWithPath:path params:params];
    
    
    
    //请求
    [self POST_URL:composeUrl(path) params:params success:^(NSDictionary *result) {
        //处理显示请求成功信息
        [self handleResultMessage:result];
        //成功回调
        if (success) {
            success(result);
            
        }
    } failure:^(NSDictionary *errorInfo) {
        //处理显示请求失败信息
        [self handleResultMessage:errorInfo];
        //失败回调
        if (failure) {
            failure(errorInfo);
        }
    }];
}

#pragma mark - ---------- Private Methods ----------
//拼接URL
static inline NSString * composeUrl(NSString *path) {
    return SF(@"%@%@%@%@%@", URL_PROTOCOL, URL_HOST, URL_PORT, URL_PATH_PREFIX, path);
}
//处理显示请求信息
- (void)handleShowRequestMessageUrlWithPath:(NSString *)path params:(NSDictionary *)params {
    if (self.showLog) {
        HRLog(@"URL:%@", composeUrl(path));
        HRLog(@"Params:%@", params);
    }
}
//处理显示请求成功信息
- (void)handleResultMessage:(NSDictionary *)result {
    if (self.showLog) {
        //打印返回结果
        HRLog(@"Result:%@", result);
    }
}


@end
