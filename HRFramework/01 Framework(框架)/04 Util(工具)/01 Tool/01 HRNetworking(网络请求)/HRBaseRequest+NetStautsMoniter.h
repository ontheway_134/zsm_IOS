
/*~!
 | @FUNC  监听网络状态
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseRequest.h"

@interface HRBaseRequest (NetStautsMoniter)

//1.0 监测网络状态
- (void)moniterNetStatus:(void(^)(HRNetStatus netStatus))netStautsBlock;

@end
