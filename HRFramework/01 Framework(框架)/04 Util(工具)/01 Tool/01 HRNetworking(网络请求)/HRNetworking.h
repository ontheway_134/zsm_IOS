
/*~!
 | @FUNC  网络请求框架头文件
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRNetworking_h
#define HRNetworking_h

//网络请求框架
#import "HRNetworkingManager.h"
//网络监测框架
#import "HRBaseRequest+NetStautsMoniter.h"


#endif /* HRNetworking_h */
