
/*~!
 | @FUNC  网络请求（业务层）
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRRequestManager.h"


@interface HRNetworkingManager : HRRequestManager

typedef void(^ErrorBlock)(NSDictionary *errorInfo); // 失败数据block
typedef void (^SuccessBlock) (id responseObject); // 成功数据的block
typedef void (^FailureBlock) (NSError *error);
typedef void (^ProgressBlock) (int64_t bytesProgress, int64_t totalBytesProgress); // 上传图片进度的block


#pragma mark GET请求
- (void)GET_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure;

#pragma mark POST请求
- (void)POST_PATH:(NSString *)path params:(NSDictionary *)params success:(void(^)(id result))success failure:(void(^)(NSDictionary *errorInfo))failure;

#pragma mark 图片上传请求
- (void)UPLOAD_IMAGES:(NSArray<UIImage *> *)imageArr
                 Path:(NSString *)path
             progress:(ProgressBlock)progress
              success:(SuccessBlock)success
              failure:(ErrorBlock)failure;

#pragma mark 单张图片上传
- (void)UPLOAD_IMAGE:(UIImage *)image imagetype:(NSString *)path
            progress:(ProgressBlock)progress
             success:(SuccessBlock)success
             failure:(ErrorBlock)failure;

@end
