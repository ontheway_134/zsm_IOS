//
//  ICN_GroupListCell.h
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynGroupModel;

@interface ICN_GroupListCell : UITableViewCell

@property (nonatomic , retain)ICN_DynGroupModel *model;


@end
