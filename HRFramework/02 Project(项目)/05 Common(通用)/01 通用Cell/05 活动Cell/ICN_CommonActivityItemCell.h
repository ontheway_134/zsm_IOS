//
//  ICN_CommonActivityItemCell.h
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynActivityModel;

@interface ICN_CommonActivityItemCell : UITableViewCell

@property (nonatomic , retain)ICN_DynActivityModel *model;


@end
