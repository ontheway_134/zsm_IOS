//
//  ICN_CommonPsersonDynamicCell.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonPsersonDynamicCell.h"
#import "ICN_DynStateContentModel.h"

@interface ICN_CommonPsersonDynamicCell ()

#pragma mark - ---------- IBProperty ----------
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel; // 用户名
@property (weak, nonatomic) IBOutlet UILabel *userPosicationLabel; // 用户职位 / 用户专业
@property (weak, nonatomic) IBOutlet UILabel *userCompanyLabel; // 用户公司 / 用户学校
@property (weak, nonatomic) IBOutlet UIImageView *userPosicatioIcon; // 用户职位/专业 图标
@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UIImageView *userCompanyIcon; // 用户公司/学校 图标
@property (weak, nonatomic) IBOutlet UILabel *userWorkStatusLabel; // 用户状态
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 动态内容
@property (weak, nonatomic) IBOutlet UIImageView *pullIcon; // 下拉图标
@property (weak, nonatomic) IBOutlet UIButton *pullBtn; // 下拉按钮
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel; // 评论人数
@property (weak, nonatomic) IBOutlet UILabel *praiseCountLabel; // 点赞人数
@property (weak, nonatomic) IBOutlet UIImageView *praiseIcon; // 点赞图标

@property (weak, nonatomic) IBOutlet UIView *imageListBackGround; // 图片列表背景
@property (nonatomic , strong)ICN_ReplyingDynStateSummaryView *replayView; // 转发的view




#pragma mark - ---------- 其他属性 ----------

@property (nonatomic , strong)UIButton *typeButton;// 公司类型特有的btn
@property (nonatomic , strong)UIView *p_HiddenDivideView; // 默认隐藏的分隔栏
@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewArr;
@property (nonatomic , strong)UIImageView *vplusIcon; // 加v照片

@end

@implementation ICN_CommonPsersonDynamicCell

- (NSMutableArray *)imageViewArr{
    if (_imageViewArr == nil) {
        _imageViewArr = [NSMutableArray array];
    }
    return _imageViewArr;
}

- (UIImageView *)vplusIcon{
    if (_vplusIcon == nil) {
        _vplusIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
        _vplusIcon.image = [UIImage imageNamed:@"认证"];
        _vplusIcon.userInteractionEnabled = YES;
        _vplusIcon.hidden = YES; // 默认隐藏图标
        [self addSubview:_vplusIcon];
    }
    return _vplusIcon;
}


- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    [self configDynStateModelDefaultConfiguration:model];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    
    // 设置Cell头
    self.timeLabel.text = _model.createDate;
    // 关于是否加v的处理
    if (_model.plusv.integerValue == 1) {
        self.vplusIcon.hidden = NO;
    }else{
        self.vplusIcon.hidden = YES;
    }
    if (_model.roleId) {
        if (_model.roleId.integerValue == 1) {
            // 设置必要标签的显示隐藏
            self.userCompanyLabel.hidden = NO;
            self.userCompanyIcon.hidden = NO;
            self.userPosicationLabel.hidden = NO;
            self.userPosicatioIcon.hidden = NO;
            self.typeButton.hidden = YES;
            // 角色为普通
            self.userNameLabel.text = _model.memberNick;
            self.userWorkStatusLabel.text = _model.memberProfession;
            if (model.workStatus.integerValue == 0) {
                // 未就业
                self.userPosicationLabel.text = _model.memberMajor;
                self.userCompanyIcon.image = [UIImage imageNamed:@"学历"];
                self.userCompanyLabel.text = _model.memberSchool;
            }else if (model.workStatus.integerValue == 1){
                // 已就业
                self.userCompanyLabel.text = _model.companyName; // 对应公司按钮
                self.userCompanyIcon.image = [UIImage imageNamed:@"公司"];
                self.userPosicationLabel.text = _model.memberPosition;
                self.userPosicatioIcon.image = [UIImage imageNamed:@"职位拷贝"];
            }else{
                HRLog(@"用户首页动态数据异常 --- 缺少用户就业状态");
            }
        }else if (_model.roleId.integerValue == 2){
            self.userCompanyLabel.hidden = YES;
            self.userCompanyIcon.hidden = YES;
            self.userPosicationLabel.hidden = YES;
            self.userPosicatioIcon.hidden = YES;
            self.typeButton.hidden = NO;
            
            // 角色为公司
            self.userNameLabel.text = _model.companyName;
            self.userWorkStatusLabel.text = _model.memberProfession;
            
            //设置类型标签的文字以及尺寸
            [self.typeButton setTitle:_model.memberPosition forState:UIControlStateNormal];
            [self.typeButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.userPosicationLabel);
                make.top.equalTo(self.userPosicationLabel.mas_bottom).offset(10);
            }];
        }
    }else{
        HRLog(@"用户首页动态数据异常 --- 缺少用户角色类型");
    }
    // 设置正文及其他
    if (self.model.contentSpreadHeight < 60.0) {
        self.pullBtn.hidden = YES;
        self.pullIcon.hidden = YES;
    }else{
        self.pullBtn.hidden = NO;
        self.pullIcon.hidden = NO;
    }
    if (_model.isSpread) {
        self.contentLabel.numberOfLines = 20;
    }else
        self.contentLabel.numberOfLines = 4;
    // 执行数据添加的工作 如果是智讯需要添加部分内容
    if (_model.isWidsom) {
        // 显示的内容是智讯
        NSString *nickContentStr = SF(@"#%@#%@",_model.title,_model.content);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:nickContentStr];
        [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                                 value:RGB0X(0x009dff)
                                 range:[nickContentStr rangeOfString:SF(@"#%@#",_model.title)]];
        self.contentLabel.attributedText = attributedString;
    }else{
        self.contentLabel.text = SF(@"%@",_model.content);
    }
    
    
    // 设置下半部分点赞等操作
    // 执行数据添加的工作
    self.commentCountLabel.text = _model.commentCount;
    self.praiseCountLabel.text = _model.praiseCount;
    if (self.model.isLikeUp) {
        self.praiseIcon.image = [UIImage imageNamed:@"点赞"];
    }else{
        self.praiseIcon.image = [UIImage imageNamed:@"点赞未选"];
    }
    
    // 设置是否是转发
    if (_model.isTransmit.integerValue == 1) {
        // 是转发
        for (UIView *view in self.imageListBackGround.subviews) {
            if ([view isKindOfClass:[UIImageView class]]) {
                [view removeFromSuperview];
            }
        }
        if (self.replayView == nil) {
            self.replayView = XIB(ICN_ReplyingDynStateSummaryView);
            CGRect frame = self.imageListBackGround.bounds;
            self.replayView.frame = frame;
        }else{
            self.replayView.hidden = NO;
        }
        [self.imageListBackGround addSubview:self.replayView];
        self.contentLabel.text = _model.summary;
        self.replayView.p_DynStateContentLabel.text = _model.content;
        // 回复内容不确定添加转发说明
        NSArray *picArr = [_model.pic componentsSeparatedByString:@","];
        NSString *picPath ;
        if ([picArr.firstObject containsString:@"."]) {
            if ([picArr.firstObject hasPrefix:@"/"]) {
                picPath = ICN_IMG([picArr.firstObject substringFromIndex:1]);
            }else{
                picPath = ICN_IMG(picArr.firstObject);
            }
            [self.replayView.p_DynStateIcon sd_setImageWithURL:[NSURL URLWithString:picPath] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }
        self.replayView.p_DynStateIcon.clipsToBounds = YES;
        if (model.amemberNick) {
            self.replayView.p_UserMarkedLabel.text = SF(@"@%@",_model.amemberNick);
        }else{
            self.replayView.p_UserMarkedLabel.text = @"";
        }
        if (_model.aworkStatus != nil && _model.acompanyName != nil) {
            self.replayView.p_PromulgatorLabel.text = SF(@"%@|%@",_model.acompanyName,_model.amemberPosition);
        }else{
            self.replayView.p_PromulgatorLabel.text = @"";
        }
    }else{
        if (self.replayView) {
            self.replayView.hidden = YES;
        }
        // 设置图片
        if (_imageViewArr) {
            for (UIImageView *imageView in _imageViewArr) {
                if (imageView.superview != nil) {
                    [imageView removeFromSuperview];
                }
            }
        }
        [_imageViewArr removeAllObjects];
        if (_model.pic != nil && [_model.pic containsString:@"."]) {
            NSArray *picArr = [_model.pic componentsSeparatedByString:@","];
            for (NSInteger i = 0; i < picArr.count; i++) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
                imageView.backgroundColor = [UIColor yellowColor];
                [imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(picArr[i])] placeholderImage:[UIImage imageNamed:@"占位图"]];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.userInteractionEnabled = YES;
                [self.imageViewArr addObject:imageView];
                [self.imageListBackGround addSubview:imageView];
            }
        }

    }
    
    [self setNeedsLayout];
    
}

- (UIView *)p_HiddenDivideView{
    if (_p_HiddenDivideView == nil) {
        _p_HiddenDivideView = [[UIView alloc] initWithFrame:CGRectZero];
        _p_HiddenDivideView.backgroundColor = RGB0X(0xf0f0f0);
        [self addSubview:_p_HiddenDivideView];
        [_p_HiddenDivideView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(5.0);
        }];
    }
    return _p_HiddenDivideView;
}

- (void)setListStyle:(BOOL)listStyle{
    _listStyle = listStyle;
    if (self.isListCell) {
        self.p_HiddenDivideView.hidden = NO;
    }else
    {
        if (_p_HiddenDivideView) {
            self.p_HiddenDivideView.hidden = YES;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.userWorkStatusLabel.layer.masksToBounds = YES;
    self.userWorkStatusLabel.layer.cornerRadius = 2.0;
    // 处理加v图标的位置
    if (self.vplusIcon.hidden == NO) {
        [self insertSubview:self.vplusIcon aboveSubview:self.userIcon];
        [self.vplusIcon mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.userIcon);
            make.bottom.equalTo(self.userIcon);
        }];
    }
    self.userIcon.layer.cornerRadius = self.userIcon.width / 2.0;
    self.userIcon.layer.masksToBounds = YES;
    // 处理行业标签的位置
    if (_model) {
        self.userWorkStatusLabel.layer.cornerRadius = 2.0;
        if (_model.roleId.integerValue == 1) {
            // 普通用户
            [self.userWorkStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.userNameLabel.mas_right).offset(5);
                make.right.lessThanOrEqualTo(self.timeLabel.mas_left).offset(-5);
                make.centerY.equalTo(self.userNameLabel);
            }];
        }else{
            // 企业用户
            [self.userWorkStatusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.userNameLabel);
                make.bottom.equalTo(self.userIcon);
            }];
        }
    }
    if (_imageViewArr && self.model.isTransmit.integerValue == 0) {
        if (_imageViewArr.count <= 3) {
            for (NSInteger i = 0; i < self.imageViewArr.count; i++) {
                if (i == 0) {
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.imageListBackGround);
                        make.top.equalTo(self.imageListBackGround);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                    
                }else{
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.imageViewArr[i - 1].mas_right).offset(ICN_KScreen(5.0));
                        make.top.equalTo(self.imageListBackGround);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                }
            }
        }else{
            
            for (NSInteger i = 0; i < self.imageViewArr.count; i++) {
                if (i == 0 || i % 3 == 0) {
                    if (i == 0) {
                        [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.imageListBackGround);
                            make.top.equalTo(self.imageListBackGround);
                            make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                        }];
                    }else{
                        [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                            make.left.equalTo(self.imageListBackGround);
                            make.top.equalTo(self.imageViewArr[i / 3 - 1].mas_bottom).offset(ICN_KScreen(5.0));
                            make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                        }];
                    }
                }else{
                    [self.imageViewArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.imageViewArr[i - 1].mas_right).offset(ICN_KScreen(5.0));
                        make.top.equalTo(self.imageViewArr[i - 1]);
                        make.size.mas_offset(CGSizeMake(ICN_KScreen(97.0), ICN_KScreen(97.0)));
                    }];
                }
            }
            
            
        }
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

#pragma mark - ---------- 私有方法 ----------

- (void)callWhileCellBtnClick:(CellBlock)block{
    self.block = block;
}

- (void)configDynStateModelDefaultConfiguration:(ICN_DynStateContentModel *)model{
    // 针对数据不全的现象添加的默认数据处理
    if (model.isTransmit == nil) {
        model.isTransmit = @"0";
    }else{
        if (model.isTransmit.integerValue == 1) {
            // 是转发Model
            if (model.amemberNick == nil) {
                model.amemberNick = @"原作者昵称";
            }
            if (model.amemberProfession == nil) {
                model.amemberProfession = @"职业标签";
            }
            if (model.aworkStatus) {
                // 是否就业登记过 登记过则根据等级的字段判断赋值内容是否存在
                switch (model.aworkStatus.integerValue) {
                    case 0:{
                        // 未就业
                        if (model.amemberSchool == nil || [model.amemberSchool isEqualToString:@""]) {
                            model.amemberSchool = @"无学校";
                        }
                        if (model.amemberMajor == nil || [model.amemberMajor isEqualToString:@""]) {
                            model.amemberMajor = @"无专业";
                        }
                        break;
                    }
                    case 1:{
                        // 已工作
                        if (model.acompanyName == nil || [model.acompanyName isEqualToString:@""]) {
                            model.acompanyName = @"无公司";
                        }
                        if (model.amemberPosition == nil || [model.amemberPosition isEqualToString:@""]) {
                            model.amemberPosition = @"无职位";
                        }
                        break;
                        
                    }
                    default:
                        break;
                }
            }else{
                model.aworkStatus = @"0"; // 否则默认设置为未登记
                [self configDynStateModelDefaultConfiguration:model];
            }
        }
    }
    if (model.roleId) {
        if (model.memberNick == nil || [model.memberNick isEqualToString:@""]) {
            model.memberNick = @"用户";
        }
        switch (model.roleId.integerValue) {
            case 1:
            case 2:{
                if (model.workStatus) {
                    // 是否就业登记过 登记过则根据等级的字段判断赋值内容是否存在
                    if (model.memberProfession == nil) {
                        model.memberProfession = @"职业标签";
                    }
                    switch (model.workStatus.integerValue) {
                        case 0:{
                            // 未就业
                            if (model.memberSchool == nil || [model.memberSchool isEqualToString:@""]) {
                                model.memberSchool = @"无学校";
                            }
                            if (model.memberMajor == nil || [model.memberMajor isEqualToString:@""]) {
                                model.memberMajor = @"无专业";
                            }
                            break;
                        }
                        case 1:{
                            // 已工作
                            if (model.companyName == nil || [model.companyName isEqualToString:@""]) {
                                model.companyName = @"无公司";
                            }
                            if (model.memberPosition == nil || [model.memberPosition isEqualToString:@""]) {
                                model.memberPosition = @"无职位";
                            }
                            break;
                            
                        }
                        default:
                            break;
                    }
                }else{
                    model.workStatus = @"0"; // 否则默认设置为未登记
                    [self configDynStateModelDefaultConfiguration:model];
                }
                break;
            }
            default:
                break;
        }
    }else{
        model.roleId = @"1";
        // 设置默认值后递归调用
        [self configDynStateModelDefaultConfiguration:model];
    }
    
}


#pragma mark - ---------- IBAction ----------

// 点击Cell按钮执行的方法
- (IBAction)clickOnCellButtonAction:(UIButton *)sender {
    
    if (self.block) {
        self.block(sender.tag , self.model);
    }
    
}

- (IBAction)clickUpInsideBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.pullBtn]) {
        //执行下拉按钮的对应操作
        if (self.block) {
            if (self.model) {
                self.model.contentSpread = !self.model.contentSpread;
                if (self.model.contentSpread) {
                    self.contentLabel.numberOfLines = 20;
                }else
                    self.contentLabel.numberOfLines = 4;
                self.block(sender.tag , self.model);
            }
        }
    }
    
    
}






@end
