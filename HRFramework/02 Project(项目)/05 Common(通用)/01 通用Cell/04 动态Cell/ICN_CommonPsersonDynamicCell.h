//
//  ICN_CommonPsersonDynamicCell.h
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_ReplyingDynStateSummaryView.h"

@class ICN_DynStateContentModel;
typedef void(^CellBlock)(NSInteger SenderTag , ICN_DynStateContentModel *model); // Cell用block

@interface ICN_CommonPsersonDynamicCell : UITableViewCell

@property (nonatomic , assign , getter=isListCell)BOOL listStyle; // 判断是否是列表样式的字段
@property (nonatomic , copy)CellBlock block;
@property (nonatomic , strong)ICN_DynStateContentModel *model;


- (void)callWhileCellBtnClick:(CellBlock)block;

@end
