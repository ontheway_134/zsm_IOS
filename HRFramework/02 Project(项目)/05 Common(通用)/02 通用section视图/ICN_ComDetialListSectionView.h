//
//  ICN_ComDetialListSectionView.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_AuthorHomePagerHeader.h" // 头文件

@class ICN_CommonFriendsModel;

typedef void(^DetialBlock)(); // 点击进入详情的block
typedef void(^ComFriendsBlock)(NSArray <ICN_CommonFriendsModel *>* array);
typedef void(^ExperienceBlock)(NSString *experienceKey); // 经验列表block
typedef void(^CommonKeyBlock)(NSString *blockKey);

@interface ICN_ComDetialListSectionView : UITableViewHeaderFooterView

@property (nonatomic , copy)NSString *titleKey;
@property (nonatomic , assign)NSInteger itemCount; // 数量
#pragma mark - ---------- Model属性 ----------
@property (nonatomic , strong)NSArray <ICN_CommonFriendsModel *>* comfriendsArr; // 共同好友属性列表


@property (nonatomic , copy)DetialBlock block; // 进动态详情的block -- 活动的block
@property (nonatomic , copy)ExperienceBlock expBlock; // 工作列表block

@property (nonatomic , copy)ComFriendsBlock comFBlock;
@property (nonatomic , copy)CommonKeyBlock comBlock;


- (void)callBackWithDetialBlock:(DetialBlock)block;
- (void)callBackWithCommonFriendsBlock:(ComFriendsBlock)block;
- (void)callBackWithExperienceBlock:(ExperienceBlock)block;
- (void)callBackWithCommonKeyBlock:(CommonKeyBlock)block;

@end
