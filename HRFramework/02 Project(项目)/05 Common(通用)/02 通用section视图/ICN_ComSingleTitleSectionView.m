//
//  ICN_CommonSingleTitleHeader.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ComSingleTitleSectionView.h"

@interface ICN_ComSingleTitleSectionView ()

@property (weak, nonatomic) IBOutlet UILabel *p_TitleLabel;



@end

@implementation ICN_ComSingleTitleSectionView

- (void)setHeaderTitle:(NSString *)headerTitle{
    if (headerTitle) {
        _headerTitle = headerTitle;
        self.p_TitleLabel.text = headerTitle;
    }
}


@end
