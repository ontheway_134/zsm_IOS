//
//  ICN_ComDetialListSectionView.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ComDetialListSectionView.h"
#import "ICN_CommonFriendsModel.h"

@interface ICN_ComDetialListSectionView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemCountLabel;

@property (nonatomic , strong)NSMutableArray <UIImageView *>*friendIconsArr; // 装载相同好友的头像的imageview的数组


@end

@implementation ICN_ComDetialListSectionView

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)friendIconsArr{
    if (_friendIconsArr == nil) {
        _friendIconsArr = [NSMutableArray array];
    }
    return _friendIconsArr;
}


#pragma mark - ---------- 属性构造器 ----------

- (void)setItemCount:(NSInteger)itemCount{
    _itemCount = itemCount;
    self.itemCountLabel.text = SF(@"%ld",self.itemCount);
}

- (void)setTitleKey:(NSString *)titleKey{
    _titleKey = titleKey;
    // key = 动态
    if ([_titleKey isEqualToString:KEY_DynamicStates]) {
        self.titleLabel.text = @"动态";
    }
    // key = 共同好友
    if ([_titleKey isEqualToString:KEY_SameFriends]) {
        self.titleLabel.text = @"共同好友";
    }else{
        // 在其他headerview的时候清除之前的共同好友列表
        if (_friendIconsArr) {
            for (UIImageView *temp in _friendIconsArr) {
                [temp removeFromSuperview];
            }
            [_friendIconsArr removeAllObjects];
        }
    }
    // key = 教育经历
    if ([titleKey isEqualToString:KEY_EduExperience]) {
        self.titleLabel.text = @"教育经历";
        self.itemCountLabel.text = SF(@"%ld",self.itemCount);
    }
    // key = 工作经历
    if ([titleKey isEqualToString:KEY_WorkExperience]) {
        self.titleLabel.text = @"工作经历";
        self.itemCountLabel.text = SF(@"%ld",self.itemCount);
    }
    // key = 参加的活动
    if ([titleKey isEqualToString:KEY_ApplyActivity]) {
        self.titleLabel.text = @"参加的活动";
        self.itemCountLabel.text = SF(@"%ld",self.itemCount);
    }
    // key = 参加的小组
    if ([titleKey isEqualToString:KEY_ApplyGroups]) {
        self.titleLabel.text = @"参加的小组";
        self.itemCountLabel.text = SF(@"%ld",self.itemCount);
    }
}

- (void)setComfriendsArr:(NSArray<ICN_CommonFriendsModel *> *)comfriendsArr{
    _comfriendsArr = comfriendsArr;
    // 当共同好友列表有历史数据的时候刷新
    if (_friendIconsArr) {
        for (UIImageView *temp in _friendIconsArr) {
            [temp removeFromSuperview];
        }
        [_friendIconsArr removeAllObjects];
    }
    // 否则开始新建共同好友头像列表 -- 默认显示4个，超过不显示，在最后显示数量
    if (_comfriendsArr) {
        NSInteger tureCount = _comfriendsArr.count > 4 ? 4 : _comfriendsArr.count;
        for (NSInteger i = 0; i < tureCount; i++) {
            NSString *path = _comfriendsArr[i].memberLogo;
            UIImageView *icon = [self configCommonFriendIconWithPath:path];
            [self.friendIconsArr addObject:icon];
        }
        self.itemCountLabel.text = SF(@"%ld",_comfriendsArr.count);
    }
    // 刷新布局
    [self setNeedsLayout];
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (self.friendIconsArr.count > 0) {
        // 当共同好友列表存在数据的时候刷新布局
        for (NSInteger i = 0; i < self.friendIconsArr.count; i++) {
            [self.friendIconsArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.centerY.equalTo(self.contentView);
                make.size.mas_equalTo(CGSizeMake(35, 35));
            }];
            if (i == 0) {
                [self.friendIconsArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.titleLabel.mas_right).offset(10.0);
                }];
            }else{
                [self.friendIconsArr mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.friendIconsArr[i - 1].mas_right).offset(10.0);
                }];
            }
        }
    }
}

#pragma mark - ---------- 私有方法 ----------

- (UIImageView *)configCommonFriendIconWithPath:(NSString *)path{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 35.0, 35.0)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(path)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    imageView.layer.cornerRadius = imageView.width / 2.0;
    imageView.layer.masksToBounds = YES;
    [self.contentView addSubview:imageView];
    return imageView;
}

#pragma mark - ---------- 回调方法 ----------

- (void)callBackWithDetialBlock:(DetialBlock)block{
    self.block = block;
}

- (void)callBackWithCommonFriendsBlock:(ComFriendsBlock)block{
    self.comFBlock = block;
}

- (void)callBackWithExperienceBlock:(ExperienceBlock)block{
    self.expBlock = block;
}

- (void)callBackWithCommonKeyBlock:(CommonKeyBlock)block{
    self.comBlock = block;
}

- (IBAction)clickInToDetialAction:(UIButton *)sender {
    
    if ([self.titleKey isEqualToString:KEY_DynamicStates]) {
        // 点击动态列表时候的key
        if (self.block) {
            self.block();
        }
    }else if ([self.titleKey isEqualToString:KEY_SameFriends]){
        // 点击共同好友列表的时候
        if (self.comFBlock) {
            self.comFBlock(self.comfriendsArr);
        }
    }else if ([self.titleKey isEqualToString:KEY_ApplyActivity]){
        // 点击动态列表的时候
        if (self.block) {
            self.block();
        }
    }else if ([self.titleKey isEqualToString:KEY_WorkExperience] || [self.titleKey isEqualToString:KEY_EduExperience]){
        if (self.expBlock) {
            self.expBlock(self.titleKey);
        }
    }else{
        // 参加的小组的block回调
        if (self.comBlock) {
            self.comBlock(self.titleKey);
        }
    }
    
    
    
}



@end
