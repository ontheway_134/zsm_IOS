//
//  05 Enum.h
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#ifndef _5_Enum_h
#define _5_Enum_h

#pragma mark - ---------- 网路相关 - 枚举 ----------

/** 判断当前网络状态的枚举 */
typedef enum : NSUInteger {
    /** 当前处于WIFI网络状态 */
    HR_AlbertWIFI_NetStatus = 10012368,
    /** 当前处于手机网络状态 */
    HR_AlbertMOBILE_NetStatus,
    /** 当前出去无网络连接状态 */
    HR_AlbertNONE_NetStatus,
    /** 当前处于未知网络状态 */
    HR_AlbertUNKNOW_NetStatus,
    /** 用户被登出状态 */
    HR_UserLoginOut,
} HR_AlbertNetStatus;


/** 获取网络请求标识（get/post） */
typedef enum : NSUInteger {
    HR_AlbertURLRequrest_GET = 10022368,
    HR_AlbertURLRequrest_POST,
} HR_AlbertURLRequrestMODE;

/** 网络请求状态类型及失败原因枚举 */
typedef enum : NSUInteger {
    ICN_NetSuccess = 10032200, // 网络请求成功
    ICN_FileLoadSuccess = 10032201, // 数据上传成功
    ICN_PostFailureWithNetState = 10032368, // 网络请求失败 - post 网络不良失败
    ICN_PostFailureWithMessageError = 10032369,// 网络请求失败 - post 上传信息异常
    ICN_LoadFaileFailureWithNetState = 10032370,// 网络请求失败 - load 网络不良失败
    ICN_LoadFaileFailureWithMessageError = 10032371,// 网络请求失败 - load 上传信息异常
} ICN_NetPostStateType; 


#pragma mark - ---------- 数据库相关 - 枚举 ----------

/** 是否缓存历史数据到数据库的标识 */
typedef enum : NSUInteger {
    HR_Albert_SAVE_NetHistoryData = 10032368,
    HR_Albert_DROP_NetHistoryData,
} HR_AlbertNetHistoryDealMODE;

#pragma mark - ---------- 用户登录 - 枚举 ----------

/** 当前登录状态 -- 游客、个人用户、企业用户 */
typedef enum : NSInteger {
    HR_ICNVisitor = 10042368, // 游客登录
    HR_ICNUser, // 用户 10042369
    HR_ICNEnterprise, // 企业用户 10042370
} HR_ICNUserType;


#endif /* _5_Enum_h */
