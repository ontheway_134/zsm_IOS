//
//  ICN_EmptyDataView.h
//  ICan
//
//  Created by albert on 2017/2/15.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_EmptyDataView : UIView

@property (nonatomic , assign , getter=isHidden)BOOL viewHidden;


- (instancetype)loadXibWithMessage:(NSString *)message DefaultHidden:(BOOL)hidden;

@end
