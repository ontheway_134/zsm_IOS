
/*~!
 | @FUNC  项目网络请求url
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef HRURLConstant_h
#define HRURLConstant_h

#import <Foundation/Foundation.h>

#pragma mark - ---------- 协议(protocol) ----------
//协议（http/https）（含“//”后缀，不能为空）
#if DEBUG
static NSString *const URL_PROTOCOL = @"http://";
#else
static NSString *const URL_PROTOCOL = @"http://";
#endif


#pragma mark - ---------- 地址(host) ----------
//地址(host) （不能为空）
#if DEBUG
//static NSString *const URL_HOST = @"zsm1.4pole.cn/";
static NSString *const URL_HOST = @"1ican.com/";
#else
static NSString *const URL_HOST = @"1ican.com/";
#endif
//http://1ican.com/index.php/
#pragma mark - ---------- 端口(port) ----------
//端口（port），（含“:”前缀，如果 URL_PORT 为空，则不含）
#if DEBUG
static NSString *const URL_PORT = @"";
#else
static NSString *const URL_PORT = @"";
#endif

#pragma mark - ---------- 路径(path) ----------
//路径通用前缀，（含后缀“/” ，如果 URL_PREFIX 为空， 则不含）
static NSString *const URL_PATH_PREFIX = @"index.php/";


//XXXX
static NSString *const PATH_XXXX = @"PATH_XXXX"; // ⚠️：变量名称全部大写，用下划线分割

#pragma mark - ---------- 广告路径 ----------
// 在刚刚进入页面的时候加载广告的路径
static NSString *const PATH_AD = @"Home/StartPageAd/startPageAdList";
// 引导页网址 index.php/Home/StartPageAd/loginPageAdList
static NSString *const PATH_GUIDPICS = @"Home/StartPageAd/loginPageAdList";

#pragma mark - ---------- 支付 ----------

// 微信支付获取订单接口 Home/WeixinPay/getInfo
static NSString *const PATH_WeiChatPAYOrder = @"Home/WeixinPay/appPayIos";
// 微信支付成功后的后台回调
static NSString *const PATH_WeiChatOrderRecall = @"Home/WeixinPay/changeActivityOrder";
// 支付宝获取订单接口
static NSString *const PATH_AliPayOrder = @"Home/AliPay/doalipayAPP";

#pragma mark - ---------- 用户登录注册部分 ----------

// 验证是否需要屏蔽三方登录按钮
static NSString *const PATH_HiddenThird = @"Login/Login/isHidden";

//验证邮箱是否存在

static NSString *const PATH_EMIALEXIT = @"Login/Register/isEmailExists";

//验证手机号是否存在
static NSString *const PATH_PHONEISTEIT = @"/Login/Register/isExists";

//邮箱验证
static NSString *const PATH_EMIAL = @"/Member/Member/sendEmail";
//找回密码邮箱的验证
static NSString *const PATH_CHECKEMAIL = @"/Login/Profile/doVerifyEmail";


//获取验证码注册时
static NSString *const PATH_PhoneCode = @"/Login/Profile/doVerifyMobile";
//注册
static NSString *const PATH_USERREGISTER = @"/Login/Register/doReg";
//登录
static NSString *const PATH_USERLOGIN = @"/Login/Login/doLogin";
//退出登录
static NSString *const PATH_USERLOGINOUT = @"/Login/Login/doLogout";
//找回密码
static NSString *const PATH_USERFINDPASSWOED = @"/Login/Profile/doRetrievePassword";
//修改密码
static NSString *const PATH_USERCHANGE = @"/Login/Profile/doProfile";
//验证码
static NSString *const PATH_CODE = @"/Member/Member/sendSms";

#pragma mark - ---------- 人脉部分接口 ----------

// 邀请QQ微信微博好友接口（返回url）
static NSString *const PATH_UrlInviteShare = @"MyFriend/AliPush/getShareUrl";

//邀请好友短信接口
static NSString *const PATH_NOTE = @"MyFriend/AliPush/send";
//清除坐标
static NSString *const PATH_CLEARBEARBY = @"MyFriend/LngLat/delDistance";

//附近的人
static NSString *const PATH_BEARBYPENPRER = @"MyFriend/LngLat/nearbyPeople";
//联系人的
static NSString *const PATH_CONTACTPERSON = @"MyFriend/Index/AddressBookFriend";
// 获取小组中全部成员的id和信息的接口
static NSString * const PATH_HXGroupIdList = @"MyFriend/HxTalk/chatgroupsMumbers";
// 获取好友的环信ID
static NSString *const PATH_HXFriendID = @"MyFriend/MemberInfo/get_huanxinId";

//可能感兴趣的人 6
static NSString *const PATH_INTERPEOPLE = @"MyFriend/Index/fmaybeInterested";
//创建小组接口  9
static NSString *const PATH_CREATEGROUP=@"MyFriend/Group/gcreate";
//好友/新的好友列表 1
static NSString *const PATH_NEWFRIEND = @"MyFriend/Index/findex";
//删除好友接口 5
static NSString *const PATH_DELETEFRIDENT = @"MyFriend/Index/fdelFriend";
//添加好友申请 3
static NSString *const PATH_ADDFRIDENT = @"MyFriend/Index/faddFriend";
//审核好友列表   4
static NSString *const PATH_CHECKFRIDENT =@"MyFriend/Index/freview";
//我的小组   10
static NSString *const PATH_MYGROUP=@"MyFriend/Group/gmy_group";
//发现更多小组  11
static NSString *const PATH_MYMOREGROUP=@"MyFriend/Group/gmore_group";
//小组详情  16
static NSString *const PATH_MYGROUPDETIAL=@"MyFriend/Group/ggroup_info_pc";
//申请加入小组   12
static NSString *const PATH_APPLYGROUP=@"MyFriend/Group/gapply_group";
//编辑小组   16
static NSString *const PATH_EDITGROUP = @"MyFriend/Group/geditor_group";
//搜索好友 2
static NSString *const PATH_SEEKFRIDENT = @"MyFriend/Index/fsearch_friend";
//退出小组 15
static NSString *const PATH_WUITGTROUP = @"MyFriend/Group/gquit_group";
//系统推荐用户接口  7
static NSString *const PATH_SYSTEM = @"MyFriend/Index/frecommend";   //未完成
//验证小组是否存在接口   8
static NSString *const PATH_VERIFY = @"MyFriend/Group/gcheck_name";
//审核用户加入小组申请接口:(同意+拒绝)  13
static NSString *const PATH_CHECKGROUP = @"MyFriend/Group/greview_group";
//踢出组员接口   14
static NSString *const PATH_KICTGROUP = @"MyFriend/Group/gkickout_group";
//判断新最大组员数是否小于当前组员数接口  15
static NSString *const PATH_JUDEGGROPU = @"MyFriend/Group/gis_small";
//获取融云用户token:
static NSString *const PATH_TOKRN = @"MyFriend/MemberInfo/get_rongtoken";
//申请加入小组列表接口
static NSString *const PATH_APPLYGROUPSS = @"MyFriend/Group/gapplylist";
//三方登录的接口
static NSString *const PATH_THIRDSREGIST = @"Login/Login/doThirdLogin";
//
static NSString *const PATH_XXCCCCCC =@"MyFriend/Group/gmyapplylist";

static NSString *const PATH_GROUPCHAT = @"MyFriend/Group/gcheck_group";

//工作经验
static NSString *const PATH_EXPERIENCE = @"Member/Common/getWorkExperienceList";
//学历要求
static NSString *const PATH_EDUCATION = @"Member/Common/getQualificationList";



#pragma mark - ---------- 企业中心部分接口 ----------

#pragma mark --- 职位相关 ---
//已发布职位接口
static NSString *const PATH_POSITIONLIST = @"Enterprise/EnterprisePosition/positionList";
// 职位详情接口
static NSString *const PATH_PosicationDetial = @"Enterprise/EnterprisePosition/getPosition";

// 获取企业中心接口
static NSString *const PATH_GETENTERPRISEMESSAGE = @"Enterprise/Enterprise/getEnterpriseInfo";
// 更新企业基本信息接口
static NSString *const PATH_UPDATEENTERPRISEMESSAGE = @"Enterprise/Enterprise/setEnterpriseInfo";
// 企业中心行业接口
static NSString *const PATH_ERPISEINFO = @"Enterprise/EnterprisePosition/doPositionLinkage";
// 更新企业中心简历筛选的接口
static NSString *const PATH_RESUMESIZER = @"Enterprise/EnterprisePosition/doPositionLinkage";
// 简历搜索接口
static NSString *const PATH_RESUMESEARCH = @"Enterprise/EnterpriseResume/doSearchMemberResume";


// 收到的简历接口
static NSString *const PATH_RECEIVEDRESUME = @"Enterprise/EnterpriseResume/resumeReceiveList";
// 收藏的简历接口
static NSString *const PATH_COLLECTEDRESUME = @"Enterprise/EnterpriseResume/collectResumeList";
// 合适的简历接口
static NSString *const PATH_SUITABLERESUME = @"Enterprise/EnterpriseResume/resumeFittedList";
// 删除收藏的简历接口
static NSString *const PATH_DELETECOLLECTIONRESUME = @"Enterprise/EnterpriseResume/doModifyCollection";
//收藏简历的接口
static NSString *const PATH_DOADDCOLLECTION = @"Enterprise/EnterpriseResume/doAddCollection";
//取消收藏简历的接口
static NSString *const PATH_DODELETECOLLECTION = @"Enterprise/EnterpriseResume/doDeleteCollection";
//搜索简历的接口
static NSString *const PATH_DOSEARCHMEMBERRESUME = @"Enterprise/EnterpriseResume/doSearchMemberResume";

//企业查看教育经历
static NSString *const PATH_OtherEducationExperienceList = @"Member/EducationExperience/otherEducationExperienceList";
//企业查看培训经历
static NSString *const PATH_OtherTrainExperienceList = @"Member/TrainExperience/otherTrainExperienceList";
//企业查看工作经历
static NSString *const PATH_OtherWorkExperienceList = @"Member/WorkExperience/otherWorkExperienceList";
//企业查看用户技能标签
static NSString *const PATH_OtherMemberSkillList = @"Member/MemberSkills/otherMemberSkillList";
//企业查看用户基本信息
static NSString *const PATH_OtherMemberInfo = @"Member/Member/otherMemberInfo";
//企业是否收藏了
static NSString *const PATH_IsCollectResume = @"Enterprise/EnterpriseResume/isCollectResume";
//企业标记简历合适或不合适
static NSString *const PATH_DoFittedResume = @"Enterprise/EnterpriseResume/doFittedResume";
//企业资料图片获取
static NSString *const PATH_GetApprove = @"Api/Company/getApprove";

//企业是否通过审核
static NSString *const PATH_isAudit = @"Enterprise/Enterprise/isAudit";

//企业被查看的简历
static NSString *const PATH_doBrowseResume = @"Enterprise/EnterpriseResume/doBrowseResume";

#pragma mark - ---------- 首页 - 动态 ----------

#pragma mark --- 转发相关 ---

// 动态/智讯 转发到动态
static NSString *const PATH_ReplyDynamicState = @"Api/dynamic/forward";
// 转发吐槽到动态
static NSString *const PATH_ReplyComplainToDYN = @"MyFriend/OtherShare/complaints";
// 转发提问到动态接口
static NSString *const PATH_ReplyQuestionToDYN = @"MyFriend/OtherShare/askQuestion";
// 转发到活动 / 直播 到动态
static NSString *const PATH_ReplyLiveORActToDYN = @"MyFriend/OtherShare/activity";
// 转发职位 到动态
static NSString *const PATH_ReplyPositionToDYN = @"MyFriend/OtherShare/release";
// 转发行业资讯 到动态
static NSString *const PATH_ReplyIndustryInfoToDYN = @"MyFriend/OtherShare/information";


#pragma mark --- 用户设置权限 ---

// 单个好友的拉黑功能
static NSString *const PATH_SingleDeFriend = @"MyFriend/Secret/inBlack";
// 单个好友 取消拉黑功能
static NSString *const PATH_CancerSingleDeFriend = @"MyFriend/Secret/delBlackId_one";
// 单个用户的 不看ta 的动态
static NSString *const PATH_DontSeeUserDyn = @"MyFriend/Secret/inPassId";
// 取消 单个用户的 不看ta 的动态
static NSString *const PATH_CancerDontSeeUserDyn = @"MyFriend/Secret/delPassId_one";
// 不允许ta看 我的动态
static NSString *const PATH_ForbidUserDynRight = @"MyFriend/Secret/inUnlook";
// 取消 不允许ta看 我的动态
static NSString *const PATH_CancerForbidUserDynRight = @"MyFriend/Secret/delUnlookId_one";

#pragma mark --- 提问 ---
// 获取我的积分接口
static NSString *const PATH_MyRewardPoints = @"MyAsk/AskQuestion/myIntegral";
// 发布提问接口
static NSString *const PATH_PublishQuestion = @"MyAsk/AskQuestion/addQuestion";
// 获取首页提问列表数据
static NSString *const PATH_FirstQuestionList = @"MyAsk/AskQuestion/questionList";
// 获取我的主页提问列表数据
static NSString *const PATH_MyQuestionList = @"MyAsk/AskQuestion/MyquestionList";
// 获取用户id主页提问列表数据
static NSString *const PATH_UserQuestionList = @"MyAsk/AskQuestion/MemberquestionList";
// 获取搜索页面提问列表数据
static NSString *const PATH_SearchQuestionList = @"MyAsk/AskQuestion/screachQuestionList";
// 提问列表的收藏功能
static NSString *const PATH_QuestionListLikeUp = @"MyAsk/AskQuestion/collectQuestion";
// 提问列表的取消收藏功能
static NSString *const PATH_QuestionListDisLikeUp = @"MyAsk/AskQuestion/delcollecQuestion";
// 回答提问内容的接口
static NSString *const PATH_AnswerQuestion = @"MyAsk/AskQuestion/answerQuestion";
// 获取提问详情接口
static NSString *const PATH_QuestionDetial = @"MyAsk/AskQuestion/questionDetail";
// 提问详情 回答的点赞功能
static NSString *const PATH_AnswerLikeUp = @"MyAsk/AskQuestion/ThumbsupAnswerQuestion";
// 提问详情 回答的取消点赞功能
static NSString *const PATH_AnswerDisLikeUp = @"MyAsk/AskQuestion/DelThumbsupAnswerQuestion";
// 提问详情 关注提问人
static NSString *const PATH_FocusQuestionPerson = @"MyFriend/MemberInfo/memberFollow";
// 提问详情 关注取消提问的人
static NSString *const PATH_DisFocusQuestionPerson = @"MyFriend/MemberInfo/memberFollowDel";
// 提问详情 打赏回答
static NSString *const PATH_PayForAnswer = @"MyAsk/AskQuestion/adoptQuestion";




#pragma mark --- 吐槽 ---
// 发布吐槽接口
static NSString *const PATH_PublishComplainState = @"MyAsk/MakeComplaints/AddMakeComplaints";
// 吐槽点赞接口
static NSString *const PATH_ComplainLikeUp = @"MyAsk/MakeComplaints/MakeComplaintsThumbsup";
// 吐槽取消点赞接口
static NSString *const PATH_ComplainDisLikeUp = @"MyAsk/MakeComplaints/DelMakeComplaintsThumbsup";
// 获取自己的吐槽列表
static NSString *const PATH_ComplainMineList = @"MyAsk/MakeComplaints/MyMakeComplaintsList";
// 获取别人的吐槽列表
static NSString *const PATH_ComplainOthersList = @"MyAsk/MakeComplaints/MakeComplaintsList";
// 根据id获取对应的吐槽列表
static NSString *const PATH_ComplainUserIDList = @"MyAsk/MakeComplaints/otherMakeComplaintsList";
// 根据模糊搜索获取吐槽列表
static NSString *const PATH_ComplainSearchList = @"MyAsk/MakeComplaints/selMakeComplaints";
// 发布吐槽评论接口
static NSString *const PATH_PublishComplainComment = @"MyAsk/MakeComplaints/CommentMakeComplaints";
// 获取吐槽详情接口（带评论列表）
static NSString *const PATH_ComplainDetial = @"MyAsk/MakeComplaints/MakeComplaintsDetails";
// 吐槽评论点赞接口
static NSString *const PATH_ComplainCommentLikeUp = @"MyAsk/MakeComplaints/CommentMakeComplaintsThumbsup";
// 吐槽评论取消点赞接口
static NSString *const PATH_ComplainCommentDisLikeUp = @"MyAsk/MakeComplaints/DelCommentMakeComplaintsThumbsup";
// 删除吐槽接口
static NSString *const PATH_ComplainDelete = @"MyAsk/MakeComplaints/delMakeComplaints";


// 分享接口 -- 分享到h5（默认接口）
static NSString *const PATH_DynamicSHARE = @"Api/Dynamic/shareDynamic";
// 分享提问接口 -- 分享到h5（默认接口）
static NSString *const PATH_QuestionSHARE = @"MyAsk/ShareLfx/MyAskQuestion";
// 分享吐槽接口 -- 分享到h5（默认接口）
static NSString *const PATH_ComplainSHARE = @"//MyAsk/ShareLfx/MakeComplaints";

// 首页动态接口
static NSString *const PATH_DynamicStateFP = @"Api/home/homeDynamic";
// 首页广告接口
static NSString *const PATH_DynamicAD = @"Api/home/advertisement";
// 获取动态评论
static NSString *const PATH_DynamicComment = @"Api/home/comments";
// 发布动态评论
static NSString *const PATH_DynStateCommentPublication = @"Api/home/doComments";
// 智讯评论
static NSString *const PATH_WisdomCommentPublication = @"Api/Topic/doTalk";
// 点赞/取消点赞
static NSString *const PATH_DynamicStateLikeUp = @"Api/dynamic/likeUp";
// 动态举报
static NSString *const PATH_DynamicStateReport = @"Api/dynamic/reportDynamic";
// 搜索热词
static NSString *const PATH_HotWorlds = @"Api/home/search";
// 搜索动态列表
static NSString *const PATH_SearchDynList = @"Api/home/dynamicSearch";
// 小组搜索列表
static NSString *const PATH_SearchGROUPList = @"MyFriend/Group/ggroup_search";
// 地理信息接口
static NSString *const PATH_Location = @"Member/Common/doCityLinkage";
// 动态详情接口
static NSString *const PATH_DynamicDetial = @"Api/home/dynamicDetails";
#pragma mark --- 首页智讯 ---
// 我的智讯 - （也可以根据用户id获取智讯）
static NSString *const PATH_MineTopic = @"Api/Topic/myTopic";
// 首页智讯接口
static NSString *const PATH_WisdomMessage = @"Api/Topic/homeTopic";
// 智讯搜索
static NSString *const PATH_WisdomSearch = @"Api/Topic/topicSearch";

#pragma mark --- 发布 ---
// 多图上传 - 先于智讯发布
static NSString *const PATH_MultipleImagesUpload = @"Api/Upload/formUploads";
// 发布智讯
static NSString *const PATH_WisdomMessagePublication = @"Api/Topic/addTopic";
// 发布动态
static NSString *const PATH_DynamicPublication = @"Api/home/addDynamic";
// 发布动态 - 谁不可看(好友列表)
static NSString *const PATH_DynFriendsList = @"MyFriend/Index/findex";


#pragma mark --- 公司详情 ---
static NSString *const PATH_DynDetialCompany = @"Enterprise/Enterprise/otherEnterpriseInfo";
static NSString *const PATH_DynAddCompanyAttention = @"Member/MemberConcern/doAddConcern";
static NSString *const PATH_DynDeleteCompanyAttention = @"Member/MemberConcern/doDeleteConcern";
static NSString *const PATH_DynCompanyAttentionStatus = @"Member/MemberConcern/isConcerned"; // 企业关注状态

#pragma mark --- 首页 - 用户主页 ---
// 用户主要信息
static NSString *const PATH_UserMainMsg = @"MyFriend/MemberInfo/gmember_info";
// 用户共同好友
static NSString *const PATH_UserComFriends = @"MyFriend/MemberInfo/gsame_friend";
// 用户技能标签
static NSString *const PATH_UserSkillSign = @"Member/MemberSkills/otherMemberSkillList";
// 用户主页动态
static NSString *const PATH_UserDynamicState = @"Api/dynamic/hisHomeDynamic";
// 用户参加的活动
static NSString *const PATH_UserActivities = @"MyPosition/Activities/activityOrderList";
// 用户参加的小组
static NSString *const PATH_UserGroups = @"MyFriend/Group/gmy_group";
// 用户的工作经历
static NSString *const PATH_UserWorkExperience = @"Member/WorkExperience/otherWorkExperienceList";
// 用户的教育经历
static NSString *const PATH_UserEduExperience = @"Member/EducationExperience/otherEducationExperienceList";
// 用户举报
static NSString *const PATH_UserReport = @"MyFriend/MemberInfo/gmember_report";
// 添加好友接口
static NSString *const PATH_UserAddFriend = @"MyFriend/Index/faddFriend";
// 删除好友接口
static NSString *const PATH_UserDeleteFriend = @"MyFriend/Index/fdelFriend";
// 我的动态
static NSString *const PATH_MainDynamic = @"Api/dynamic/myDynamic";

#pragma mark - ---------- 个人中心 ----------

// 获取用户信息接口（包含好友字段）
static NSString *const PATH_GetMineUserMSG = @"Member/Member/getMemberInfo";
//设置用户信息接口
static NSString *const PATH_MySaveTheMessage = @"Member/Member/setMemberInfo";
//保存用户行业数据接口
static NSString *const PATH_MySaveIndustry = @"Api/Company/setIndustry";
//活动列表接口
static NSString *const PATH_ActivityListmember = @"MyPosition/Activity/ActivityListmember";
//删除活动列表接口
static NSString *const PATH_Activitydel = @"MyPosition/ActivityPay/Activitydel";
//所属行业设置接口
static NSString *const PATH_setIndustry = @"Api/CompanytIndustry/setIndustry";
//所属行业获取接口
static NSString *const PATH_getIndustry = @"Api/CompanytIndustry/getIndustry";
//删除动态
static NSString *const PATH_delDynamic = @"Api/Dynamic/delDynamic";
//删除智讯
static NSString *const PATH_delTopic = @"Api/Topic/delTopic";





/*辛 专属*/
/*我的关注*/
static NSString *const PATH_MemberConcern = @"Member/MemberConcern/getConcernList";
/*我的直播*/
static NSString *const PATH_MyLiveList = @"MyPosition/Activities/memberActivityList";
/*发布直播*/
static NSString *const PATH_ReleaseLive = @"MyPosition/Activities/addActivity";
/*删除直播*/
static NSString *const PATH_DeleLive = @"MyPosition/Activities/deleteActivity";
/*我发布的 动态接口*/
static NSString *const PATH_MyReleaseDynamic = @"Api/dynamic/hisDynamic";
/*我发布的删除动态*/
static NSString *const PATH_MyReleasedelDynamic = @"Api/Dynamic/delDynamic";
/*我发布的智讯接口*/
static NSString *const PATH_MyReleasedelMyTopic = @"Api/Topic/myTopic";
/*我发布的删除智讯*/
static NSString *const PATH_MyReleasedelTopic = @"Api/Topic/delTopic";
/*我发布的我的吐槽列表*/
static NSString *const PATH_MyReleasedelComplaints = @"MyAsk/MakeComplaints/MyMakeComplaintsList";
/*我发布的我的提问列表*/
static NSString *const PATH_MyquestionList = @"MyAsk/AskQuestion/MyquestionList";


/*我发布的我的提问删除*/
static NSString *const PATH_MyReleasedelQuestion = @"MyAsk/AskQuestion/delQuestion";

/*我发布的我的吐槽删除*/
static NSString *const PATH_MyReleasedelComplaint = @"MyAsk/MakeComplaints/delMakeComplaints";
/*隐私设置首页*/
static NSString *const PATH_MyPrivacySettings = @"MyFriend/Secret/secret";
/*关注的用户列表*/
static NSString *const PATH_UserFollowList = @"MyFriend/MemberInfo/memberFollowList";
/*仅自己可见*/
static NSString *const PATH_UserIsopen = @"MyFriend/Secret/isopen";
/*仅好友可见*/
static NSString *const PATH_UserjustFriend = @"MyFriend/Secret/justFriend";
/*仅看好友动态*/
static NSString *const PATH_UserjustLookFriend = @"MyFriend/Secret/justLookFriend";
/*谁不可看列表*/
static NSString *const PATH_UserGetUnlook = @"MyFriend/Secret/getUnlook";
/*谁不可以看批量操作*/
static NSString *const PATH_UserUnlook = @"MyFriend/Secret/unlook";
/*动态谁不可看 取消操作*/
static NSString *const PATH_delMatterNotLook = @"Api/Home/delMatterNotLook";
/*黑名单列表*/
static NSString *const PATH_UserGetBlack = @"MyFriend/Secret/getBlack";
/*批量操作黑名单列表*/
static NSString *const PATH_UserCanalBlackList = @"MyFriend/Secret/blackList";
/*获取环信信息*/
static NSString *const PATH_hxMemberInfo = @"MyFriend/Secret/hxMemberInfo";

//http://1ican.com/index.php/MyFriend/Secret/hxMemberInfo
/*谁不可以看／黑名单 搜索地址*/
static NSString *const PATH_UserSearch = @"MyFriend/Secret/search";


/*我的收藏 全部*/
static NSString *const PATH_AllCollection = @"Member/MemberCollections/allCollection";
/*我的收藏 职场*/
static NSString *const PATH_CollectionList = @"Member/MemberCollection/positionCollectionList";

/*我的收藏 资讯 */
static NSString *const PATH_PostionNewslist = @"MyPosition/PositionNews/mypostionNewslist";

/*我的收藏 活动 */
static NSString *const PATH_ActivityList = @"Member/MemberActivity/memberActivityList";
/*我的收藏 提问*/
static NSString *const PATH_Questionlist = @"MyAsk/AskQuestion/myCollectQuestionlist";
/*积分明细*/
static NSString *const PATH_ScoreRecordList = @"Member/Member/memberScoreRecord";
/*我的收藏职位删除*/
static NSString *const PATH_CancelPosition = @"Member/MemberCollection/doCancelPositionCollection";
/*我的收藏资讯删除*/
static NSString *const PATH_positionNewsCollectDel = @"MyPosition/PositionNews/positionNewsCollectDel";
/*我的收藏活动删除*/
static NSString *const PATH_DeleteActivity = @"Member/MemberActivity/doDeleteActivityCollection";
/*我的收藏提问删除*/
static NSString *const PATH_DelcollecQuestion = @"MyAsk/AskQuestion/delcollecQuestion";

/*我的订单列表接口*/
static NSString *const PATH_ActivityOrderList = @"MyPosition/Activities/activityOrderList";

/*别人订单列表的接口*/
static NSString *const PATH_otherActivityOrderList = @"MyPosition/Activities/otherActivityOrderList";
/*我发布的职位删除*/
static NSString *const PATH_doDeletePosition = @"Enterprise/EnterprisePosition/doDeletePosition";

/*我的标签*/
static NSString *const PATH_MyactivityNav = @"MyPosition/Activities/activityNav";
/*动态-不可看*/
static NSString *const PATH_getMatterBanList = @"MyFriend/MemberInfo/getMatterBanList";
/*我的消息列表*/
static NSString *const PATH_memberMessageList = @"Member/MemberMessage/memberMessageList";
/*投递简历申请记录*/
static NSString *const PATH_resumeFittedList = @"Member/MemberResume/resumeFittedList";
/*投递申请记录*/
static NSString *const PATH_resumeBrowsedList = @"Member/MemberResume/resumeBrowsedList";
/*投递申请记录*/
static NSString *const PATH_resumeDeliveryList = @"Member/MemberResume/resumeDeliveryList";

/*个人中心主页*/
static NSString *const PATH_getIndexInfo = @"Member/Member/getIndexInfo";
/*我的简历*/
static NSString *const PATH_trainExperienceList = @"Member/TrainExperience/trainExperienceList";

/*单张图片上传*/
static NSString *const PATH_formUpload = @"Api/Upload/formUpload";
/*我的发布-》动态-》谁不可以看*/
static NSString *const PATH_ReleaseGetMatterBanList = @"MyFriend/MemberInfo/getMatterBanList";

#pragma mark - ---------- 我的简历 ----------

//发布添加工作经历
//Member/WorkExperience/doAddWorkExperience
static NSString *const PATH_MyWorkingExperience = @"Member/WorkExperience/doAddWorkExperience";

//删除工作经历
//http://zsm.4pole.cn/index.php/Member/WorkExperience/doDeleteWorkExperience
static NSString *const PATH_MyWorkingDeleteExperience = @"Member/WorkExperience/doDeleteWorkExperience";

//修改工作经历
//http://zsm.4pole.cn/index.php/Member/WorkExperience/doDeleteWorkExperience
static NSString *const PATH_MyWorkingEditExperience = @"Member/WorkExperience/doModifyWorkExperience";
static NSString *const PATH_AddApprove = @"Api/Company/addApprove";

//发布添加教育经历
static NSString *const PATH_MyEducationExperience = @"Member/EducationExperience/doAddEducationExperience";

//删除教育经历
static NSString *const PATH_MyEducationDeleteExperience = @"Member/EducationExperience/doDeleteEducationExperience";

//修改教育经历
static NSString *const PATH_MyEducationEditExperience = @"Member/EducationExperience/doModifyEducationExperience";

// 根据学历级别获取专业Id
//index.php/Member/Common/getQualificationList
static NSString *const PATH_GETDeucationLevel = @"Member/Common/getQualificationList";

//发布添加培训经历
static NSString *const PATH_MyTrainExperience = @"Member/TrainExperience/doAddTrainExperience";

//删除培训经历
static NSString *const PATH_DeleteTrainExperience = @"Member/TrainExperience/doDeleteTrainExperience";

//修改培训经历
static NSString *const PATH_MyWorkingEditTrainEx = @"Member/TrainExperience/doModifyTrainExperience";
//添加技能标签
static NSString *const PATH_MySkillEditTrainEx = @"Member/MemberSkills/doAddMemberSkill";
//删除技能标签
static NSString *const PATH_MySkillDeleteTrainEx = @"Member/MemberSkills/doDeleteMemberSkill";

static NSString *const PATH_MemberSkillList = @"Member/MemberSkills/memberSkillList";

//删除用户收藏职位
static NSString *const PATH_doDeletePositionCollection = @"Member/MemberCollection/doDeletePositionCollection";
//删除用户收藏咨询
static NSString *const PATH_mypositionNewsCollectDel = @"MyPosition/PositionNews/mypositionNewsCollectDel";
//取消关注 企业
static NSString *const PATH_doDeleteConcern = @"Member/MemberConcern/doDeleteConcern";
//取消关注 用户
static NSString *const PATH_doFollowDel = @"MyFriend/MemberInfo/memberFollowDel";

static NSString *const PATH_postionDetail = @"MyPosition/Position/postionDetail";
//投递反馈删除
static NSString *const PATH_doDeleteResume = @"Member/MemberResume/doDeleteResume";
//设置简历是否公开
static NSString *const PATH_isResumeOpened = @"Member/MemberResume/isResumeOpened";

//发布职位的接口
static NSString *const PATH_ISSUEDUTY = @"http://59.110.46.196/index.php/Enterprise/EnterprisePosition/doAddPosition";
//职位详情的接口
static NSString *const PATH_OPTIONDETIAL = @"Enterprise/EnterprisePosition/doPositionLinkage";

//发送短信
static NSString *const PATH_resume_send = @"MyFriend/AliPush/resume_send";
#pragma mark - ---------- 收藏 ----------
//删除收藏
static NSString *const PATH_MyDeletePositionCollection = @"Member/MemberCollection/doDeletePositionCollection";

#pragma mark - ---------- 消息 ----------
//用户删除消息
static NSString *const PATH_MyDeletedoDeleteMessage = @"Member/MemberMessage/doDeleteMessage";
#pragma mark - ---------- 设置 ----------
//意见反馈
//index.php/Setup/MemberNote/doAddNote
static NSString *const PATH_MyFeedBack = @"Setup/MemberNote/doAddNote";
//用户手机绑定
static NSString *const PATH_MyPhoneBinding = @"Setup/MemberAuth/doBindToMobile";
//修改密码
static NSString *const PATH_MyChangePassword = @"Login/Profile/doProfile";
//是否绑定手机号
static NSString *const PATH_memberMobileInfo = @"Setup/MemberAuth/memberMobileInfo";
#pragma mark ----------------职场---------------------
static NSString *const PATH_positionNewsTypeList = @"MyPosition/PositionNewses/positionNewsTypeList";
#pragma mark ----------------活动---------------------
//搜索活动的
static NSString *const PATH_SECRCHERACTIVITY = @"MyPosition/Activities/activityPackageList";
//搜索职业动态和行业资讯
static NSString *const PATH_SEARCHERINDUSTRY = @"MyPosition/PositionNews/postionSelectNewslist";

//活动模块导航条接口
static NSString *const PATH_activityNav = @"MyPosition/Activities/activityNav";
//活动包列表接口
static NSString *const PATH_activityPackageList = @"MyPosition/Activities/activityPackageList";

//直播详情接口
static NSString *const PATH_activityDetail = @"MyPosition/Activities/activityDetail";
//活动包详情接口
static NSString *const PATH_activityPackageDetail = @"MyPosition/Activities/activityPackageDetail";

//直播提问列表接口
static NSString *const PATH_activityQuizList = @"MyPosition/Activities/activityQuizList";

//直播提问详情接口
static NSString *const PATH_activityQuizDetail = @"MyPosition/Activities/activityQuizDetail";
//发布提问及评论接口
static NSString *const PATH_addQuiz = @"MyPosition/Activities/addQuiz";
//提问点赞接口
static NSString *const PATH_doPraise = @"MyPosition/Activities/doPraise";
//活动列表页轮播图接口
static NSString *const PATH_carouselPicList123 = @"MyPosition/Activities/activityBannerList";

//环信获取聊天室id接口:
static NSString *const PATH_getRoomId = @"MyFriend/HxTalk/getRoomId";
//获取环信用户uuid:
static NSString *const PATH_get_huanxinId = @"MyFriend/MemberInfo/get_huanxinId";
///购买活动/参与活动接口
static NSString *const PATH_ActivityPay = @"MyPosition/ActivityPay/ActivityPay";

///活动收藏
static NSString *const PATH_doAddActivityCollection = @"Member/MemberActivity/doAddActivityCollection";
///APP获取微信支付参数接口
static NSString *const PATH_appPay = @"Home/WeixinPay/appPay";

//APP获取支付宝支付参数接口
static NSString *const PATH_doalipayAPP = @"Home/AliPay/doalipayAPP";
//APP生成活动预订单接口:(即活动报名接口)
static NSString *const PATH_createOrderAPP = @"Home/WeixinPay/createOrderAPP";
//活动分享
static NSString *const PATH_shareActivityPackage = @"MyPosition/Activities/shareActivityPackage";

//直播分享
static NSString *const PATH_shareActivity = @"MyPosition/Activities/shareActivity";

//职位分享接口分享
static NSString *const PATH_ShareLfx_position = @"MyPosition/ShareLfx/position";
static NSString *const PATH_doDeliveryResume = @"Member/MemberResume/doDeliveryResume";
static NSString *const PATH_doAddPositionCollection = @"Member/MemberCollection/doAddPositionCollection";
static NSString *const PATH_doCancelPositionCollection = @"Member/MemberCollection/doCancelPositionCollection";
static NSString *const PATH_doAddConcern = @"Member/MemberConcern/doAddConcern";
//static NSString *const PATH_doDeleteConcern = @"Member/MemberConcern/doDeleteConcern";
//职位资讯分享接口
static NSString *const PATH_ShareLfx_news = @"MyPosition/ShareLfx/news";
//职场模糊搜索
static NSString *const PATH_postionSecVague = @"MyPosition/PositionSelect/postionSecVague";
//根据行业查询职位列表接口
static NSString *const PATH_supplementIndustrylist = @"MyPosition/Supplement/SupplementIndustrylist";
//根据行业查询职位列表接口
static NSString *const PATH_postionSelectNewslist = @"MyPosition/PositionNews/postionSelectNewslist";
//根据行业查询职位列表接口
//static NSString *const PATH_postionSelectNewslist = @"MyPosition/PositionNews/postionSelectNewslist";

#pragma mark - ---------- 其他（others） ----------

#pragma mark 图片路径通用前缀
//包括协议、地址、端口号...。含“/”，如果 URL_IMG_PREFIX 为空，则不含。
static NSString *const URL_IMG_PREFIX = @"<#xxx/#>";
static NSInteger const CODE_S = 59;

#endif /* HRURLConstant_h */
