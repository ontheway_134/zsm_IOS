
/*~!
 | @FUNC  项目宏
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#ifndef Macro_h
#define Macro_h

#pragma mark - ---------- 网络相关 - 宏 ----------
/*******************************判断手机机型大小**********************************/
#define kIs_phone4     [UIScreen mainScreen].bounds.size.height == 480
#define kIs_phone5     [UIScreen mainScreen].bounds.size.height == 568
#define kIs_phone6     [UIScreen mainScreen].bounds.size.height == 667
#define kIs_phone6plus [UIScreen mainScreen].bounds.size.height == 736

#define ICN_IMG(path) SF(@"%@%@%@", URL_PROTOCOL, URL_HOST, path) // 拼接img路径方法
#define ICN_HX(userId) SF(@"ixing%@",userId) // 根据用户ID获取环信ID的方法

#define HR_NETWORK_STATUS @"NetWorkStatus" // 存储在userDefault中的网络状态字段 - key
#define HR_ComContentPageSize  9 // 设置默认通用页面正文内容数量
#define HR_ComADPageSize  3 // 设置默认通用每页插入广告数量

#pragma mark - ---------- 用户类型 - 宏 ----------

#define HR_UserTypeKEY @"userTypeKey" // 用户类型key
#define HR_BusinessTypeKEY @"businessTypeKey"  // 企业类型的key;
#define HR_BusinessAuditKEY @"businessAudit"  // 企业审核的key 0 = 未通过 1 = 通过 nil 不存在该类型（不做对应处理）;
#define HR_CurrentUserToken @"AlbertToken" // 当前用户的token的key;
#define HR_LoginTypeKEY @"currentUserLoginTypeKey" // 当前用户的登录状态名 0 三方登录 1 普通用户登录
#define HR_BIdentificationStatusKEY @"businessIdentificationStatus" // 企业用户认证状态key 0 未认证 1 已认证


#pragma mark - ---------- 屏幕适配宏 ----------

#define ICN_KScreen(content) [UIScreen mainScreen].bounds.size.width /320.0 * content

#pragma mark - ---------- 网络状态宏 ----------

#define HR_ICNNETSTATUSKEY @"ICN_NetStatus"

#pragma mark - ---------- 通知 - 宏 ----------

#define HR_NONENETWORK_NOTIFICATION @"noneNetWorkNotification" // APPDelegate中发送网络相关状态的通知的 - name
#define HR_USERLOGINOUT_NOTIFICATION @"userHasbeenLoginOut" // 用户被登出状态
#define HR_REMOTESTART_NOTICATION @"remoteAppStartAction" // 使用远端推送的方式启动APP的通知


#endif /* Macro_h */
