//
//  ICN_CommonUserHeaderView.m
//  ICan
//
//  Created by albert on 2016/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonUserHeaderView.h"
#import "ICN_DynUserBaseMSGModel.h"

@interface ICN_CommonUserHeaderView ()

@property (weak, nonatomic) IBOutlet UIButton *p_FriendStateChangeBtn; // 用于修改好友状态的按钮，默认是选中 - 添加好友 ； 未选中 - 删除好友状态

@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像

@property (weak, nonatomic) IBOutlet UIImageView *userSexIcon; // 用户性别标签

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel; // 用户名标签

@property (weak, nonatomic) IBOutlet UILabel *userCompanyLabel; // 用户公司标签

@property (weak, nonatomic) IBOutlet UILabel *userPosicationLabel; // 用户行业标签


@end

@implementation ICN_CommonUserHeaderView

- (void)setModel:(ICN_DynUserBaseMSGModel *)model{
    _model = model;
    
    if (self.model.isMe.integerValue == 1) {
        // 是自己
        self.p_FriendStateChangeBtn.hidden = YES;
    }
    
    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"头像"]];
    self.userNameLabel.text = _model.memberNick;
    if (_model.memberGender.integerValue == 1) {
        // 男  没有对应图片
        _userSexIcon.image = [UIImage imageNamed:@"性别男"];
    }else{
        // 女
        _userSexIcon.image = [UIImage imageNamed:@"性别女"];
    }
    if (_model.companyName != nil && ![_model.companyName isEqualToString:@""]) {
        // 公司存在不添加学校
        self.userCompanyLabel.text = _model.companyName;
        self.userPosicationLabel.text = _model.memberProfession;
    }else{
        // 没有公司就添加学校的信息
        self.userCompanyLabel.text = _model.memberSchool;
        self.userPosicationLabel.text = _model.memberMajor;
    }
    
    // 根据好友状态判断添加好友按钮的选项
    if (_model.staus == nil || _model.staus == NULL) {
        // 不是好友
        self.p_FriendStateChangeBtn.selected = NO;
        self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
    }else{
        // 根据状态判断 (1:未审核[申请者]  2:已审核   3:被拒绝  4:已删除  5:待审核[被申请者] 6:已拒绝 )[没关系则没有此数据]
        switch (_model.staus.integerValue) {
            case 1:{
                self.p_FriendStateChangeBtn.selected = NO;
                self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                [self.p_FriendStateChangeBtn setTitle:@"待审核" forState:UIControlStateNormal];
                break;
            }
            case 2:{
                self.p_FriendStateChangeBtn.selected = YES;
                [self.p_FriendStateChangeBtn setTitle:@"删除好友" forState:UIControlStateSelected];
                self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                break;
            }
            case 3:{
                self.p_FriendStateChangeBtn.selected = NO;
                [self.p_FriendStateChangeBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
                self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                break;
            }
            case 4:{
                self.p_FriendStateChangeBtn.selected = NO;
                [self.p_FriendStateChangeBtn setTitle:@"已删除" forState:UIControlStateNormal];
                self.p_FriendStateChangeBtn.userInteractionEnabled = YES;
                break;
            }
            case 5:{
                self.p_FriendStateChangeBtn.selected = NO;
                [self.p_FriendStateChangeBtn setTitle:@"待审核" forState:UIControlStateNormal];
                self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                break;
            }
            case 6:{
                self.p_FriendStateChangeBtn.selected = NO;
                [self.p_FriendStateChangeBtn setTitle:@"被拒绝" forState:UIControlStateNormal];
                self.p_FriendStateChangeBtn.userInteractionEnabled = NO;
                break;
            }
            default:
                break;
        }
    }
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.userLogoIcon.layer.cornerRadius = self.userLogoIcon.width / 2.0;
    self.userLogoIcon.layer.masksToBounds = YES;
    self.userLogoIcon.layer.borderWidth = 1.0;
    self.userLogoIcon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userPosicationLabel.layer.cornerRadius = 2.0;
    
    self.userPosicationLabel.layer.cornerRadius = 2.0;
}

#pragma mark - ---------- 共有方法 ----------

- (void)changeFriendStateSuccessWithAddfirend:(BOOL)isAddfriend{
    if (isAddfriend) {
        // 执行添加好友成功后的操作
        self.p_FriendStateChangeBtn.selected = YES;
        [self.p_FriendStateChangeBtn setTitle:@"删除好友" forState:UIControlStateNormal];
    }else{
        self.p_FriendStateChangeBtn.selected = NO;
         [self.p_FriendStateChangeBtn setTitle:@"加好友" forState:UIControlStateNormal];
    }
}


#pragma mark - ---------- IBAction ----------

- (IBAction)changeFriendStateBtnAction:(UIButton *)sender {
    
    
    if (self.delegate) {
        if (!sender.isSelected) {
            if ([self.delegate respondsToSelector:@selector(responseWithAddCurrentFriendAction)]) {
                [self.delegate responseWithAddCurrentFriendAction];

            }
        }else{
            if ([self.delegate respondsToSelector:@selector(responseWithDeleteCurrentFriendAction)]) {
                [self.delegate responseWithDeleteCurrentFriendAction];
            }
        }
    }
    
}

@end
