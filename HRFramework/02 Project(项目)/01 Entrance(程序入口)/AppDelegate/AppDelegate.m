
#import "AppDelegate.h"
#import "HRAppdelegateProtocol.h"
#import "AppDelegate+Switch.h"
#import "AppDelegate+AliPusher.h"
#import "PPGetAddressBook.h"
#import <UMSocialCore/UMSocialCore.h>
#import "AppDelegate+OtherParty.h"
#import <CloudPushSDK/CloudPushSDK.h>
#import "AppDelegate+Hyphenate.h"
#import "AppDelegate+BMKManager.h"


@interface AppDelegate ()<HRAppdelegateProtocol>

@end

@implementation AppDelegate

#pragma mark - ---------- Lifecycle ----------
//程序开始
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = RGB0X(0x009dff);
    [self createReachAblilityNetWorkManager];
    //获取联系人的通讯录的授权
    [PPGetAddressBook requestAddressBookAuthorization];
    [self OtherParty]; // 启动三方平台
    [self initCloudPush]; // 启动阿里云推送
    [self initHyphenate]; // 初始化环信
    [self createBaiduMapManager]; // 初始化百度地图管理器
    [self registerAPNS:[UIApplication sharedApplication]]; //注册苹果推送，获取deviceToken用于推送
    [CloudPushSDK sendNotificationAck:launchOptions]; // 点击通知将App从关闭状态启动时，将通知打开回执上报
    [self SwitchRootViewController];
    return YES;
}


#pragma mark - ---------- 程序默认回调 ----------

//程序将要进入后台
- (void)applicationWillResignActive:(UIApplication *)application {
}
//程序已经进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
}
//程序将要进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
}
//程序已经进入前台
- (void)applicationDidBecomeActive:(UIApplication *)application {
}
//程序关闭
- (void)applicationWillTerminate:(UIApplication *)application {
}

#pragma mark - ---------- Private Methods ----------
//配置根视图
- (void)configRootViewController {
    
}
//配置网络监测环境
- (void)configNetStatusMoniterEnvironment {
    
    [[HRRequest manager] moniterNetStatus:^(HRNetStatus netStatus) {
        if (netStatus == HRNetStatusUnknown || netStatus == HRNetStatusNotReachable) {
            NSLog(@"%@", NET_NOT_WORK);
        }
    }];
}

#pragma mark - ---------- OpenUrl相关回调 ----------

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
        }];
    }
    return YES;
}


@end
