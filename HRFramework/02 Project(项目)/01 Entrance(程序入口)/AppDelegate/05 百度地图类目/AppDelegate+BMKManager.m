//
//  AppDelegate+BMKManager.m
//  ICan
//
//  Created by albert on 2017/2/24.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate+BMKManager.h"

static NSString * const BaiduMapManagerKey = @"Ab0PhQocF1GoEMj9OZUy2M358suaujij";

@implementation AppDelegate (BMKManager)

BMKMapManager *mapManager ; // 百度地图管理器实例

- (BOOL)createBaiduMapManager{
    // 要使用百度地图，请先启动BaiduMapManager
    mapManager = [[BMKMapManager alloc]init];
    // 如果要关注网络及授权验证事件，请设定     generalDelegate参数
    BOOL ret = [mapManager start:BaiduMapManagerKey  generalDelegate:nil];
    if (!ret) {
        NSLog(@"开启管理器失败");
    }
    return YES;
}

@end
