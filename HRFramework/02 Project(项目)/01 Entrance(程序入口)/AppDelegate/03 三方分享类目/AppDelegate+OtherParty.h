//
//  AppDelegate+OtherParty.h
//  ICan
//
//  Created by shilei on 17/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate.h"
#import <WXApi.h>

@interface AppDelegate (OtherParty)<WXApiDelegate>

-(void)OtherParty;

@end
