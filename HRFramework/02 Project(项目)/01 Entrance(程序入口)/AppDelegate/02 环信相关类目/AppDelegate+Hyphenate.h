//
//  AppDelegate+Hyphenate.h
//  ICan
//
//  Created by zjk on 2017/2/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Hyphenate)

- (void)initHyphenate;

@end
