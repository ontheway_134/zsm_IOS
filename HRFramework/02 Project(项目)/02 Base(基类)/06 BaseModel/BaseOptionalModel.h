//
//  BaseOptionalModel.h
//  YKJ.iOS
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 高阳. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface BaseOptionalModel : JSONModel

@property (nonatomic , copy)NSString *info; // 请求失败时提示的信息
@property (nonatomic , assign)NSInteger code; // 请求是否成功(0成功1失败2用户未登录)
@property (nonatomic , assign)NSInteger count; // 数值总量
@property(nonatomic, strong)NSString *myGroupRole;   //是否是组长的判断



@end
