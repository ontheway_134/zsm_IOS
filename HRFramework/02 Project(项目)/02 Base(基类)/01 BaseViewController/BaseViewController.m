

#import "BaseViewController.h"
#import "ICN_SignViewController.h" // 登录页面
#import "ICN_StartViewController.h" // 登录注册页面
#import "BaseTabBarController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

#pragma mark - ---------- Lazy Loading（懒加载） ----------

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.view.backgroundColor == nil) {
        self.view.backgroundColor = RGB0X(0xf2f2f2);
    }
    [self.navigationController.navigationBar setBarTintColor:RGB0X(0x009cff)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (self.navigationController) {
        self.navigationController.navigationBar.translucent = NO;
    }
    
    //监听通知中心
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responseALLNoticifation:) name:nil object:nil];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self.navigationController.viewControllers.firstObject isEqual:self]) {
        self.tabBarController.tabBar.hidden = NO;
    }else
        self.tabBarController.tabBar.hidden = YES;
    
    if (self.navigationController != nil && self.hiddenDefaultNavBar == YES ) {
        [self.navigationController setNavigationBarHidden:YES animated:animated];
    }
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.navigationController != nil) {
        [self.navigationController setNavigationBarHidden:NO animated:animated];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---------- 重写属性构造器 ----------

- (void)setHiddenDefaultNavBar:(BOOL)hiddenDefaultNavBar{
    _hiddenDefaultNavBar = hiddenDefaultNavBar;
    if (_hiddenDefaultNavBar) {
        if (self.navigationController != nil) {
            if ([self isViewLoaded]) {
                [self.navigationController setNavigationBarHidden:YES animated:YES];
            }
        }
    }
}


#pragma mark - ---------- 私有方法 ----------

// 对于用户的登出警告通知
- (void)warnUserWithLoginOut{
    UIAlertController *netWarn = [UIAlertController alertControllerWithTitle:@"异地登录" message:@"您的账号在另一台设备上登录。如非本人操作,则密码可能被泄露,建议修改密码。" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *quitAction = [UIAlertAction actionWithTitle:@"退出" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        BaseTabBarController *tabbarVC = [[BaseTabBarController alloc] init];
        // 清空用户token
        [USERDEFAULT setValue:SF(@"%ld",HR_ICNVisitor) forKey:HR_UserTypeKEY];
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        
        [UIApplication sharedApplication].keyWindow.rootViewController = tabbarVC;
    }];
    UIAlertAction *reLoginAction = [UIAlertAction actionWithTitle:@"重新登录" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 是否需要根据用户的状态跳转到不同的登录页面
        [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
        if ([USERDEFAULT valueForKey:HR_LoginTypeKEY]) {
            if ([[USERDEFAULT valueForKey:HR_LoginTypeKEY] integerValue] == 0) {
                // 三方登录
                ICN_StartViewController *pager = [[ICN_StartViewController alloc] init];
                [self presentViewController:pager animated:YES completion:nil];
            }else{
                // 非三方登录
                
                ICN_SignViewController *pager = [[ICN_SignViewController alloc] init];
                pager.relogin = YES;
                // 设置跟vc是欢迎页面
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:pager];
                [self presentViewController:nav animated:YES completion:nil];
                
            }
        }else{
            // 默认跳转非三方登录
            ICN_SignViewController *pager = [[ICN_SignViewController alloc] init];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:pager];
            [self presentViewController:nav animated:YES completion:nil];

        }
    }];
    [netWarn addAction:quitAction];
    [netWarn addAction:reLoginAction];
    [self presentViewController:netWarn animated:YES completion:^{
        
    }];
}

- (void)presentNoneNetWorkWarnAlert{
    UIAlertController *netWarn = [UIAlertController alertControllerWithTitle:@"当前网络状况不佳" message:@"为了您的用户体验，当前网络状况下您可能只能浏览缓存内容" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"我知道了" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [netWarn addAction:action];
    [self presentViewController:netWarn animated:YES completion:^{
        
    }];
}

#pragma mark - ---------- 公有方法 -- 供子类集成 ----------

/** 用于实现rootVC页面跳转的通用方法 */
- (void)currentPagerJumpToPager:(UIViewController *)pager{
    
    if (pager) {
        if (self.navigationController) {
            [self.navigationController pushViewController:pager animated:YES];
        }else
            [self presentViewController:pager animated:YES completion:nil];
    }
    
}

/** 设置 -- 添加用户状态设置为企业用户未登录状态 */
- (void)setCurrentUserBusinessUnIdentificatedStatus{
    [USERDEFAULT setValue:SF(@"%ld",HR_ICNEnterprise) forKey:HR_UserTypeKEY];
    [USERDEFAULT setValue:@"0" forKey:HR_BIdentificationStatusKEY];
}

/** 判断 -- 当前如果是否是未认证的企业用户状态 */
- (BOOL)isCurrentBusinessUnIdentificatedStatus{
    
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNEnterprise) {
            // 第一步判断当前是企业用户
            if ([USERDEFAULT valueForKey:HR_BIdentificationStatusKEY]) {
                // 判断用户状态标识存在 -- 不存在默认为已认证
                if ([[USERDEFAULT valueForKey:HR_BIdentificationStatusKEY] integerValue] == 0) {
                    // 当前企业用户状态为未认证
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

/** 判断 -- 当前是否是用户登录并在游客登录的时候删除掉本地的token */
- (BOOL)getCurrentUserLoginStatus{
    // 先判断用户的类型key的值存不存在
    NSInteger userLoginType = -100;
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        userLoginType = [[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue];
    }
    // 根据userlogintype进行实际的枚举判断
    switch (userLoginType) {
        case HR_ICNUser:
            // 用户登录 如果存在token 确定是用户登录 ，若不存在则设置类型为游客登录
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return YES;
            }else{
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
                return NO;
            }
            break;
        case HR_ICNVisitor:{
            // 当游客登录存在token的时候，清除token
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            }
            return NO;
            break;
        }
        case HR_ICNEnterprise:{
            // 企业登录 如果存在token 确定是企业登录 ，若不存在则设置类型为游客登录
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                return YES;
            }else{
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
                return NO;
            }
            break;
        }
            
        default:
            // 默认是不存在用户登录类型的情况，此时需要设置用户登录类型为游客登录
            [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNVisitor] forKey:HR_UserTypeKEY];
            [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            break;
    }
    return NO;
}


#pragma mark --- 响应通知方法 ---

- (void)responseALLNoticifation:(NSNotification *)notification{
    if ([notification.name isEqualToString:HR_NONENETWORK_NOTIFICATION]) {
        [self presentNoneNetWorkWarnAlert];
    }
    // 判断当用户被登出的通知发出的时候进行相应操作
    if ([notification.name isEqualToString:HR_USERLOGINOUT_NOTIFICATION]) {
        [self warnUserWithLoginOut];
    }
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------


@end

