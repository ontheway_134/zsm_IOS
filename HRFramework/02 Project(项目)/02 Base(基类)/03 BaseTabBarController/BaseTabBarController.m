

#import "BaseTabBarController.h"

#pragma mark - ---------- 各级模块首页 ----------
#import "ICN_DynamicStateVC.h" // 导入动态首页
#import "ICN_PersonHubChatVCs.h"// 导入人脉首页
#import "ICN_CareerLifeVC.h" // 导入职场成长首页
#import "ICN_ActivityVC.h" // 导入活动首页
#import "ICN_PersonalCenterVC.h" // 导入个人中心首页
#import "ICN_BusinessCenter.h" // 导入企业中心首页
#import "ICN_ThemeVC.h" // 导入话题首页


@interface BaseTabBarController ()
@end

@implementation BaseTabBarController

#pragma mark - ---------- Lazy Loading（懒加载） ----------


#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self configDefaultTabBarUILayoutWithUserType:self.loginUserType];

}

#pragma mark - ---------- Private Methods（私有方法） ----------
- (void) setTabBarItemWithIndex:(NSInteger)index selectedImageName:(NSString*)selectedImageName{
    [self setTabBarItemWithIndex:index nomalImageName:nil selectedImageName:selectedImageName];
}

- (void) setTabBarItemWithIndex:(NSInteger)index nomalImageName:(NSString*)nomalImageName selectedImageName:(NSString*)selectedImageName{
    
    UITabBarItem *item = self.tabBar.items[index];
    
    if (nomalImageName) {
        UIImage *imageNomal = [[UIImage imageNamed:nomalImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.image = imageNomal;
    }
    
    UIImage *imageSelected = [[UIImage imageNamed:selectedImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item.selectedImage = imageSelected;
}


#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）


/** 根据用户状态的不同切换不同的tabbar表现形式 */
- (void)configDefaultTabBarUILayoutWithUserType:(NSInteger)userType{
    
    userType = [[[NSUserDefaults standardUserDefaults] valueForKey:HR_UserTypeKEY] integerValue];
    
    switch (userType) {
        case HR_ICNUser:
            // 个人用户登录
            [self loginOutOfBusinessUserWithLogined:YES];
            break;
        case HR_ICNVisitor:
            // 游客登录
            [self loginOutOfBusinessUserWithLogined:NO];
            break;
        case HR_ICNEnterprise:
            // 企业用户登录
            [self businessUserLoginConfig];
            break;
        default:
            NSLog(@"登录异常错误 - 不存在该类型用户");
            break;
    }
}

// 非企业用户登录的配置
- (void)loginOutOfBusinessUserWithLogined:(BOOL)isLogined{
    
    if (isLogined) {
        // 执行登录相关的操作
    }else{
        // 执行未登录的相关操作
    }
    
    ICN_DynamicStateVC *dynamicPager = [[ICN_DynamicStateVC alloc] init];
    dynamicPager.title = @"动态";
    BaseNatigationViewController *dynamicNav = [[BaseNatigationViewController alloc] initWithRootViewController:dynamicPager];
    
   // ICN_PersonHubChatVC *personChatPager = [[ICN_PersonHubChatVC alloc] init];
     ICN_PersonHubChatVCs *personChatPager = [[ICN_PersonHubChatVCs alloc] init];
    personChatPager.title = @"人脉";
    BaseNatigationViewController *personChatNav = [[BaseNatigationViewController alloc] initWithRootViewController:personChatPager];
    
    ICN_CareerLifeVC *careerLifePager = [[ICN_CareerLifeVC alloc] init];
    careerLifePager.title = @"职场";
    BaseNatigationViewController *careerLifeNav = [[BaseNatigationViewController alloc] initWithRootViewController:careerLifePager];
    
    ICN_PersonalCenterVC *personalCenterPager = [[ICN_PersonalCenterVC alloc] init];
    personalCenterPager.title = @"个人中心";
    BaseNatigationViewController *personalCenterNav = [[BaseNatigationViewController alloc] initWithRootViewController:personalCenterPager];
    
    ICN_ThemeVC *themePager = [[ICN_ThemeVC alloc] init];
    themePager.title = @"活动";
    BaseNatigationViewController *themeNav = [[BaseNatigationViewController alloc] initWithRootViewController:themePager];
    
    // 将对应的子nav放置在tabbar中
    self.viewControllers = @[dynamicNav , personChatNav , careerLifeNav , themeNav , personalCenterNav];
    
    // 添加对应vc的图片
    [self setTabBarItemWithIndex:0 nomalImageName:@"首页黑" selectedImageName:@"首页蓝"];
    [self setTabBarItemWithIndex:1 nomalImageName:@"人脉黑" selectedImageName:@"人脉蓝"];
    [self setTabBarItemWithIndex:2 nomalImageName:@"职场黑" selectedImageName:@"职场蓝"];
    [self setTabBarItemWithIndex:3 nomalImageName:@"活动黑" selectedImageName:@"活动蓝"];
    [self setTabBarItemWithIndex:4 nomalImageName:@"个人中心黑" selectedImageName:@"个人中心蓝"];
    
    //添加对应vc的tabbar的标题配置
    [self.tabBar setBarTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :RGB0X(0x333333)} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :RGB0X(0x36a0e7)} forState:UIControlStateSelected];
    
}

// 企业用户登录的配置
- (void)businessUserLoginConfig{
    
    ICN_DynamicStateVC *dynamicPager = [[ICN_DynamicStateVC alloc] init];
    dynamicPager.title = @"动态";
    BaseNatigationViewController *dynamicNav = [[BaseNatigationViewController alloc] initWithRootViewController:dynamicPager];
    
    ICN_CareerLifeVC *careerLifePager = [[ICN_CareerLifeVC alloc] init];
    careerLifePager.title = @"职场";
    BaseNatigationViewController *careerLifeNav = [[BaseNatigationViewController alloc] initWithRootViewController:careerLifePager];
    
    ICN_BusinessCenter *businessCenterPager = [[ICN_BusinessCenter alloc] init];
    businessCenterPager.title = @"企业中心";
    BaseNatigationViewController *businessCenterNav = [[BaseNatigationViewController alloc] initWithRootViewController:businessCenterPager];
    
    // 将对应的子nav放置在tabbar中
    self.viewControllers = @[dynamicNav , careerLifeNav , businessCenterNav];
    
    // 添加对应vc的图片
    [self setTabBarItemWithIndex:0 nomalImageName:@"首页黑" selectedImageName:@"首页蓝"];
    [self setTabBarItemWithIndex:1 nomalImageName:@"人脉黑" selectedImageName:@"人脉蓝"];
    [self setTabBarItemWithIndex:2 nomalImageName:@"职场黑" selectedImageName:@"职场蓝"];
    
    
    //添加对应vc的tabbar的标题配置
    [self.tabBar setBarTintColor:[UIColor whiteColor]];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :RGB0X(0x333333)} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :RGB0X(0x36a0e7)} forState:UIControlStateSelected];
}



#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

#pragma mark IBActions （点击事件xib）

#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

@end

