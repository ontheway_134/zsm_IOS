
/*~!
 | @FUNC  项目基类标签控制器
 | @AUTH  Nobility
 | @DATE  2016-10-17
 | @BRIF  <#brif#>
 */

#import "HRBaseTabBarController.h"

@interface BaseTabBarController : HRBaseTabBarController

@property (nonatomic , assign)NSInteger loginUserType; // 登录用户的类型属性


/**
 *  设置TabBarItem 选择状态时的图片
 *
 *  @param index  在TabBar上第几个
 *  @param selectedImageName 图片名，NSString类型
 */
- (void) setTabBarItemWithIndex:(NSInteger)index selectedImageName:(NSString*)selectedImageName;
/**
 *  设置TabBarItem nomal和选择状态时的图片
 *
 *  @param index             在TabBar上第几个
 *  @param nomalImageName    图片名，NSString类型
 *  @param selectedImageName 图片名，NSString类型
 */
- (void) setTabBarItemWithIndex:(NSInteger)index nomalImageName:(NSString*)nomalImageName selectedImageName:(NSString*)selectedImageName;

@end
