//
//  ICN_SignViewController.m
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SignViewController.h"
#import "ICN_FindPassWordViewController.h"
#import "ICN_SignModel.h"
#import "BaseOptionalModel.h"
#import "ChatDemoHelper.h"
#import "ICN_StartViewController.h"

//个人版判断
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
//企业版判断
#import "ICN_GetEnterPriseModel.h"
#import "ICN_BusinessMassageViewController.h"
#import "ICN_huanxinModel.h"

@interface ICN_SignViewController ()
@property (weak, nonatomic) IBOutlet UITextField *registerText;
@property (weak, nonatomic) IBOutlet UITextField *repeaPasswordText;
@property (weak, nonatomic) IBOutlet UIButton *signBtn;


@end

@implementation ICN_SignViewController

#pragma mark - --- 网络请求 ---
-(void)HttpLogins{
    NSDictionary *dic=@{@"username":self.registerText.text,@"password":self.repeaPasswordText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERLOGIN params:dic success:^(id result) {
        
        ICN_SignModel *sign = [[ICN_SignModel alloc] initWithDictionary:result error:nil];
        
        if (sign.code == 0) {
            
            if ([sign.result.roleId isEqualToString:@"1"]) {
                
            
                NSDictionary *dictoken=@{@"token":sign.result.token};
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXFriendID params:dictoken success:^(id result) {
                    
                    ICN_huanxinModel *basemodel=[[ICN_huanxinModel alloc]initWithDictionary:result error:nil];
                    
                    if (basemodel.code == 0) {
                        //如果获取到接口，进行环信的登录
                        
                        
                        [[ChatDemoHelper shareHelper] loginHyphenateWithUserName:basemodel.result.username password:basemodel.result.password success:^(id data) {
                            
                            //环信登录成功，把环信的值存起来
                            [[NSUserDefaults standardUserDefaults] setValue:@"huanxin" forKey:@"phonehuanxin"];
                            
                            //环信登录成功   
                            //当用户登录成功后，进行环信的登录，
                                
                                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                                
                                [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
                                    
                                    NSDictionary *dic1 =result[@"result"];
                                    
                                    ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                                    NSLog(@"%@",model);
                                    if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                                        || model.hopePositionName == nil|| model.workStatus == nil) {
                                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                                        ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                                        vc.isFirst = YES;
                                        [self.navigationController pushViewController:vc animated:YES];
                                        
                                    } else {
                                        //设置信息后进入到主页进行环信的登录，登录成功才进入到主页，只有用户账号才需要登录到环信的账号
                                        
                                        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                        [self presentViewController:barController animated:YES completion:nil];
                                        
                                    }
                                    
                                } failure:^(NSDictionary *errorInfo) {
                                    
                                }];

                        } failure:^(id data) {
                            NSLog(@"环信登录失败");
                        }];
                        
                        
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    NSLog(@"请求环信的用户名秘密失败");
                }];

                
                
                
                
         


            }else if([sign.result.roleId isEqualToString:@"2"]){
            
                 [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
    
                 [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNEnterprise] forKey:HR_UserTypeKEY];
                
                [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                    ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
                    if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
                        model.result.provinceName == nil || model.result.companyDetail == nil ) {
                         [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                        ICN_BusinessMassageViewController *vc = [[ICN_BusinessMassageViewController alloc] init];
                        vc.model = model;
                        vc.notSetUserInfo = YES;
                        [self.navigationController pushViewController:vc animated:YES];
                    } else {
                    
                         [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                         BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                         [self presentViewController:barController animated:YES completion:nil];
                  
                    }
                
                } failure:^(NSDictionary *errorInfo) {
                    NSLog(@"请求数据失败");
                }];
                
                
                
                
                
            }else{
                
            }
        }else{
           [MBProgressHUD ShowProgressWithBaseView:self.view Message:sign.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

#pragma mark - --- 生命周期 ---

- (instancetype)init{
    self = [super init];
    if (self) {
        self.relogin = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.signBtn.layer.cornerRadius = 5.0;
    self.signBtn.layer.masksToBounds = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.relogin) {
        self.navigationItem.leftBarButtonItem = nil;
        //        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"返回"] style:UIBarButtonItemStylePlain target:self action:@selector(popToVCc)];
    }
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)popToVCc{
    ICN_StartViewController *pager = [[ICN_StartViewController alloc] init];
    [self presentViewController:pager animated:YES completion:nil];
}

//-(void)viewDidDisappear:(BOOL)animated{
//    [super viewDidDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO];
//
//}

#pragma mark - --- IBActions ---

- (IBAction)ClickBtnActions:(id)sender {
    [self.view endEditing:YES];
    UIButton *button = (UIButton *)sender;
    switch (button.tag) {
        case 0:{
             [self dismissViewControllerAnimated:YES completion:nil];
        }
            break;
            
        case 1:{
            // 点击获取用户密码需要想访问接口确认用户是否存在

            ICN_FindPassWordViewController *find=[[ICN_FindPassWordViewController alloc]init];
            [self.navigationController pushViewController:find animated:YES];
        }
            
            break;
        case 2:{
            
            [self HttpLogins];
        }
            
            break;
    }
}

#pragma mark - --- Private Method ---


@end
