//
//  ICN_UserRegisterViewController.m
//  ICan
//
//  Created by shilei on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserRegisterViewController.h"
#import "ICN_BusinessRegisterViewController.h"
#import "YHCountDownButton.h"
#import "ICN_Regular.h"
#import "HRNetworkingManager.h"
#import "BaseOptionalModel.h"
#import "ICN_SignViewController.h"
#import "ICN_PritocolViewController.h"
#import "ICN_AboutUsPersonProtoclViewController.h"
#import "phoneIstexit.h"

#import "ICN_SignModel.h"
#import "BaseOptionalModel.h"

//个人版判断
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
//企业版判断
#import "ICN_GetEnterPriseModel.h"
#import "ICN_BusinessMassageViewController.h"


@interface ICN_UserRegisterViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mobelPhoneText;
@property (weak, nonatomic) IBOutlet UITextField *codeText;
@property (weak, nonatomic) IBOutlet UITextField *passWordText;
@property (weak, nonatomic) IBOutlet UITextField *repeatPassworeText;
@property (weak, nonatomic) IBOutlet UIButton *UserBtnActions;

@end

@implementation ICN_UserRegisterViewController

#pragma mark - --- 网络请求 ---


- (void)setUserInfo {
    NSDictionary *dic=@{@"username":self.mobelPhoneText.text,@"password":self.passWordText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERLOGIN params:dic success:^(id result) {
        
        ICN_SignModel *sign = [[ICN_SignModel alloc] initWithDictionary:result error:nil];
        
        if (sign.code == 0) {
            
            if ([sign.result.roleId isEqualToString:@"1"]) {
                
                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                
                [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
                    
                    NSDictionary *dic1 =result[@"result"];
                    
                    ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                    NSLog(@"%@",model);
                    if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                        || model.hopePositionName == nil|| model.workStatus == nil) {
                        
                        ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                        vc.isFirst = YES;
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                        [self.navigationController pushViewController:vc animated:YES];
                        
                    } else {
                        
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                    }
                    
                    
                } failure:^(NSDictionary *errorInfo) {
                    
                }];
                
                
            }else if([sign.result.roleId isEqualToString:@"2"]){
                
                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNEnterprise] forKey:HR_UserTypeKEY];
                
                [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                    ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
                    if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
                        model.result.provinceName == nil || model.result.companyDetail == nil ) {
                        ICN_BusinessMassageViewController *vc = [[ICN_BusinessMassageViewController alloc] init];
                        vc.model = model;
                        vc.notSetUserInfo = YES;
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                        [self.navigationController pushViewController:vc animated:YES];
                    } else {
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                        
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    NSLog(@"请求数据失败");
                }];
                
            }else{
                
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}


-(void)HttpRegister{
    NSDictionary *dic=@{@"roleId":@1,@"mobile":self.mobelPhoneText.text,@"password":self.passWordText.text,@"rePassword":self.repeatPassworeText.text,@"verify":self.codeText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERREGISTER params:dic success:^(id result) {
        BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (codemodel.code == 0) {
            [self setUserInfo];
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"注册成功请登录"];
//            ICN_SignViewController *sign=[[ICN_SignViewController alloc]init];
//            sign.hidesBottomBarWhenPushed=YES;
//            [self.navigationController pushViewController:sign animated:YES];
        
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.UserBtnActions.layer.cornerRadius = 5.0;
    self.UserBtnActions.layer.masksToBounds = YES;
    self.mobelPhoneText.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)businessRegisterBenAction:(id)sender {
    ICN_BusinessRegisterViewController *business=[[ICN_BusinessRegisterViewController alloc]init];
    [self.navigationController pushViewController:business animated:YES];
    
}

//获取验证码
- (IBAction)CodeBtnActions:(YHCountDownButton *)sender {
    [self.view endEditing:YES];
    YHCountDownButton *sender_btn = sender;
    
    
    if (![ICN_Regular checkTelephoneNumber:self.mobelPhoneText.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的手机号"];
        return;
    }
    
    if ([self.mobelPhoneText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
        return;
    }
    
    
    NSDictionary *dic=@{@"mobile":self.mobelPhoneText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_PHONEISTEIT params:dic success:^(id result) {
        
        phoneIstexit *model=[[phoneIstexit alloc]initWithDictionary:result error:nil];
        if ([model.result.isExists isEqualToString:@"1"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该手机号已注册"];

        }else{
        
            [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CODE params:@{@"mobile":self.mobelPhoneText.text,@"type":@1} success:^(id result) {
                BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (codemodel.code == 0) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"发送成功请注意查收"];
                    sender_btn.enabled = NO;
                    [sender_btn startWithSecond:CODE_S];
                    [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                         NSString *title = [NSString stringWithFormat:@"%.2d秒",second];                        return title;
                    }];
                    [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                        countDownButton.enabled = YES;
                        return @"重新获取验";
            
                    }];
                }
                else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
                }
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];

        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
    
}

- (IBAction)dealBtnAction:(id)sender {
    [self.view endEditing:YES];
    ICN_AboutUsPersonProtoclViewController *protocol=[[ICN_AboutUsPersonProtoclViewController alloc]init];
    [self.navigationController pushViewController:protocol animated:YES];
}

- (IBAction)RegisterBtnAction:(id)sender {
    
    [self.view endEditing:YES];
    if (![ICN_Regular checkTelephoneNumber:self.mobelPhoneText.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的手机号"];
        return;
    }
    if ([self.mobelPhoneText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
        return;
    }
    if ([self.codeText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONECODE];
        return;
    }
    if ([self.passWordText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:PASSWORD];
        return;
    }
    if ([self.repeatPassworeText.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:REPARTPASSWORD];
        return;
    }
    if (![self.passWordText.text isEqualToString:self.repeatPassworeText.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:ISQUSENO];
        return;
    }
    if (![ICN_Regular checkPassword:self.passWordText.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入6-18位字母和数字混合的密码"];
        return;
    }
   
    [self HttpRegister];
}

@end
