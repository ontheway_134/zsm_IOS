//
//  ICN_BusinessRegisterViewController.m
//  ICan
//
//  Created by shilei on 16/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BusinessRegisterViewController.h"
#import "ICN_RegisterTableViewCell.h"
#import "ICN_RegisterFootView.h"
#import "ICN_CodeTableViewCell.h"
#import "ICN_RegisterHeadView.h"
#import "ICN_SignViewController.h"
#import "BaseOptionalModel.h"
#import "YHCountDownButton.h"
#import "ICN_Regular.h"
#import "ICN_AboutUsEnterpriseProcolViewController.h"
#import "phoneIstexit.h"
#import "ICN_Regular.h"

#import "ICN_SignModel.h"
#import "BaseOptionalModel.h"
#import "ICN_Regular.h"

//个人版判断
#import "ICN_SetUserInfoModel.h"
#import "ICN_SetUserInfoViewController.h"
//企业版判断
#import "ICN_GetEnterPriseModel.h"
#import "ICN_BusinessMassageViewController.h"


@interface ICN_BusinessRegisterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *BusinessRegisterTabelView;
@property(nonatomic,strong)ICN_RegisterHeadView *headView;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property(nonatomic,strong)ICN_RegisterTableViewCell *onecell;
@property(nonatomic,strong)ICN_CodeTableViewCell *cell;
@property(nonatomic,strong)ICN_RegisterTableViewCell *twocell;
@property(nonatomic,strong)ICN_RegisterTableViewCell *threecell;
@property (weak, nonatomic) IBOutlet UIView *allView;

@end

@implementation ICN_BusinessRegisterViewController

#pragma mark - --- 网络请求 ---

- (void)setUserInfo {
    NSDictionary *dic=@{@"username":self.onecell.registTextFiled.text,@"password":self.twocell.registTextFiled.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERLOGIN params:dic success:^(id result) {
        
        ICN_SignModel *sign = [[ICN_SignModel alloc] initWithDictionary:result error:nil];
        
        if (sign.code == 0) {
            
            if ([sign.result.roleId isEqualToString:@"1"]) {
                
                
                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                
                [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
                    
                    NSDictionary *dic1 =result[@"result"];
                    
                    ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:dic1 error:nil];
                    NSLog(@"%@",model);
                    if (model.memberNick == nil || model.memberGender == nil|| model.memberLogo == nil
                        || model.hopePositionName == nil|| model.workStatus == nil) {
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                        ICN_SetUserInfoViewController *vc = [[ICN_SetUserInfoViewController alloc] init];
                        vc.isFirst = YES;
                        [self.navigationController pushViewController:vc animated:YES];
                        
                    } else {
                        
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                    }
                    
                    
                } failure:^(NSDictionary *errorInfo) {
                    
                }];
                
                
            }else if([sign.result.roleId isEqualToString:@"2"]){
                
                [[NSUserDefaults standardUserDefaults] setValue:sign.result.token forKey:HR_CurrentUserToken];
                
                [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNEnterprise] forKey:HR_UserTypeKEY];
                
                [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                    ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
                    if (model.result.companyLogo == nil || model.result.companyName == nil || model.result.cityName == nil ||
                        model.result.provinceName == nil || model.result.companyDetail == nil ) {
                        ICN_BusinessMassageViewController *vc = [[ICN_BusinessMassageViewController alloc] init];
                        vc.model = model;
                        vc.notSetUserInfo = YES;
                        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"loginStatus"];
                        [self.navigationController pushViewController:vc animated:YES];
                    } else {
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                        
                    }
                    
                } failure:^(NSDictionary *errorInfo) {
                    NSLog(@"请求数据失败");
                }];
            
            }
        }
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

-(void)HttpRegister{
    NSDictionary *dic=@{@"roleId":@1,@"mobile":self.onecell.registTextFiled.text,@"password":self.twocell.registTextFiled.text,@"rePassword":self.threecell.registTextFiled.text,@"verify":self.cell.codeText.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERREGISTER params:dic success:^(id result) {
        
       BaseOptionalModel  *codemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (codemodel.code == 0) {
            [self setUserInfo];
            
        }else{
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
    
    }];
}

-(void)HttpEmailRegister{
    NSDictionary *dic=@{@"roleId":@2,@"verify":self.cell.codeText.text,@"password":self.twocell.registTextFiled.text,@"rePassword":self.threecell.registTextFiled.text,@"email":self.onecell.registTextFiled.text};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERREGISTER params:dic success:^(id result) {
        
        BaseOptionalModel  *codemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (codemodel.code == 0) {
            [self setUserInfo];
            
        }else{
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.BusinessRegisterTabelView registerNib:[UINib nibWithNibName:@"ICN_RegisterTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_RegisterTableViewCell"];
    self.BusinessRegisterTabelView.delegate=self;
    self.BusinessRegisterTabelView.dataSource=self;
    self.BusinessRegisterTabelView.separatorStyle=NO;
    [self.BusinessRegisterTabelView setScrollEnabled:NO];
    self.leftBtn.selected=YES;
    self.allView.frame=CGRectMake(SCREEN_WIDTH*45/320, 104, SCREEN_WIDTH*70/320, 1);
    self.onecell.registTextFiled.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- IBActions ---

- (IBAction)registerClick:(UIButton *)sender {
        switch (sender.tag) {
        case 100:{
            self.leftBtn.selected=YES;
            self.rightBtn.selected=NO;
            self.allView.frame=CGRectMake(SCREEN_WIDTH*45/320, 104, SCREEN_WIDTH*70/320, 1);
            [self.BusinessRegisterTabelView reloadData];
        }
            break;
                
        case 101:{
            self.rightBtn.selected=YES;
            self.leftBtn.selected=NO;
            self.allView.frame=CGRectMake(SCREEN_WIDTH*210/320, 104, SCREEN_WIDTH*70/320, 1);
            [self.BusinessRegisterTabelView reloadData];
        }
            break;
    }
}

-(void)protocolBtns{
    [self.view endEditing:YES];
    ICN_AboutUsEnterpriseProcolViewController *about=[[ICN_AboutUsEnterpriseProcolViewController alloc]init];
    [self.navigationController pushViewController:about animated:YES];
}

- (IBAction)BackBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)ClickBtnAction:(UIButton *)sender{
    
    [self.view endEditing:YES];
    

    if ([self.onecell.registTextFiled.text length]==0) {
        
        if (self.leftBtn.selected) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入手机号"];
            return;
        }
        
        if (self.rightBtn.selected) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入邮箱"];
            return;
        }
    }
    
    if (self.leftBtn.selected) {
        if (![ICN_Regular checkTelephoneNumber:self.onecell.registTextFiled.text]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的手机号"];
            return;
        }
    }
    if (self.rightBtn.selected) {
        if (![ICN_Regular checkEmail:self.onecell.registTextFiled.text]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的邮箱"];
            return;
        }
    }
    

    
    
    if ([self.cell.codeText.text length]==0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入验证码"];
        return;
    }
    if ([self.cell.codeText.text length]==0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入验证码"];
        return;
    }
    if ([self.twocell.registTextFiled.text length]==0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入密码"];
        return;
    }
    if ([self.threecell.registTextFiled.text length]==0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入重复密码"];
        return;
    }
    if (![self.twocell.registTextFiled.text isEqualToString:self.threecell.registTextFiled.text ]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"两次密码不一致"];
        return;
    }
    if (![ICN_Regular checkPassword:self.twocell.registTextFiled.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入6-18位字母和数字混合的密码"];
        return;
    }
    
    if (self.leftBtn.selected) {
        self.leftBtn.selected = YES;
        [self HttpRegister];
        
    }else{
        self.leftBtn.selected = NO;
        [self HttpEmailRegister];
    }
}

-(void)ClickBtn:(YHCountDownButton *)sender{
    
    
    if (self.leftBtn.selected) {
        
        [self.view endEditing:YES];
        YHCountDownButton *sender_btn = sender;
        if (![ICN_Regular checkTelephoneNumber:self.onecell.registTextFiled.text]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的手机号"];
            return;
        }
        if ([self.onecell.registTextFiled.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:PHONENUMBER];
            return;
        }
       
        else{
            
        
            NSDictionary *dic=@{@"mobile":self.onecell.registTextFiled.text};
            [[[HRNetworkingManager alloc]init] POST_PATH:PATH_PHONEISTEIT params:dic success:^(id result) {
        
                phoneIstexit *model=[[phoneIstexit alloc]initWithDictionary:result error:nil];
                if ([model.result.isExists isEqualToString:@"1"]) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"已经注册过,请登录"];
                }else{
                
                    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_CODE params:@{@"mobile":self.onecell.registTextFiled.text,@"type":@1} success:^(id result) {
                            BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                            if (codemodel.code == 0) {
                                sender_btn.enabled = NO;
                                [sender_btn startWithSecond:CODE_S];
                                [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                                     NSString *title = [NSString stringWithFormat:@"%.2d秒",second];
                                    return title;
                                }];
                                [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                                    countDownButton.enabled = YES;
                                    return @"重新获取";
                                }];
                            }
                            else{
                                [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
                            }
                            
                        } failure:^(NSDictionary *errorInfo) {
                            
                        }];
                }
        
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"请求数据失败");
            }];

        }

    }
    else{
     
        if (self.onecell.registTextFiled.text == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"邮箱不能为空"];
            return;
        }
        if (![ICN_Regular checkEmail:self.onecell.registTextFiled.text]) {
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的邮箱"];
            [self.onecell.registTextFiled.text isEqualToString:@""];
             return;
        }
        
        
            YHCountDownButton *sender_btn = sender;
            NSDictionary *dic=@{@"email":self.onecell.registTextFiled.text};
            [[[HRNetworkingManager alloc]init] POST_PATH:PATH_EMIALEXIT params:dic success:^(id result) {
        
                phoneIstexit *model=[[phoneIstexit alloc]initWithDictionary:result error:nil];
                if ([model.result.isExists isEqualToString:@"1"]) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"已经注册过,请登录"];
                }else{
                    NSDictionary *dic = @{@"email":self.onecell.registTextFiled.text};
                    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_EMIAL params:dic success:^(id result) {
                        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
                        if (basemodel.code == 0) {
                            
                            
                            sender_btn.enabled = NO;
                            [sender_btn startWithSecond:CODE_S];
                            [sender_btn didChange:^NSString *(YHCountDownButton *countDownButton,int second) {
                                NSString *title = [NSString stringWithFormat:@"%.2d秒",second];
                                return title;
                            }];
                            [sender_btn didFinished:^NSString *(YHCountDownButton *countDownButton, int second) {
                                countDownButton.enabled = YES;
                                return @"重新获取";
                            }];

                            
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请到邮箱中验证"];
                        }
                        
                    } failure:^(NSDictionary *errorInfo) {
                        NSLog(@"请求失败");
                    }];

                }
        
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"请求数据失败");
            }];
    }
 }

#pragma mark - --- Protrol ---

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:{
            self.onecell=[tableView dequeueReusableCellWithIdentifier:@"ICN_RegisterTableViewCell"];
            if (self.onecell==nil) {
                self.onecell=[[NSBundle mainBundle] loadNibNamed:@"ICN_RegisterTableViewCell" owner:self options:nil].lastObject;
            }
            if (self.leftBtn.selected) {
                 self.onecell.falseimage.image=[UIImage imageNamed:@"手机号"];
                 self.onecell.registTextFiled.placeholder=@"请输入手机号";
            }else{
                 self.onecell.falseimage.image=[UIImage imageNamed:@"邮箱"];
                 self.onecell.registTextFiled.placeholder=@"请输入邮箱";
            }

            return self.onecell;
        }
            break;
            
        case 1:{
            self.cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_CodeTableViewCell"];
            if (self.cell==nil) {
                self.cell=[[NSBundle mainBundle] loadNibNamed:@"ICN_CodeTableViewCell" owner:self options:nil].lastObject;
            }
            if (self.leftBtn.selected) {
               
            }else{
                [self.cell.codeText resignFirstResponder];
                self.cell.codeText.placeholder=@"请到邮箱验证";
            }
            [self.cell.codeBtn addTarget:self action:@selector(ClickBtn:) forControlEvents:UIControlEventTouchUpInside];
            return self.cell;
        }
            break;
            
        case 2:{
            self.twocell=[tableView dequeueReusableCellWithIdentifier:@"ICN_RegisterTableViewCell"];
            if (self.twocell==nil) {
                self.twocell=[[NSBundle mainBundle] loadNibNamed:@"ICN_RegisterTableViewCell" owner:self options:nil].lastObject;
            }
            self.twocell.falseimage.image=[UIImage imageNamed:@"密码"];
            self.twocell.registTextFiled.placeholder=@"请输入密码";
            self.twocell.registTextFiled.secureTextEntry = YES;
            return self.twocell;
        }
            break;
            
        case 3:{
            self.threecell=[tableView dequeueReusableCellWithIdentifier:@"ICN_RegisterTableViewCell"];
            if (self.threecell==nil) {
                self.threecell=[[NSBundle mainBundle] loadNibNamed:@"ICN_RegisterTableViewCell" owner:self options:nil].lastObject;
            }
            self.threecell.falseimage.image=[UIImage imageNamed:@"密码"];
            self.threecell.registTextFiled.placeholder=@"请再输入一遍密码";
            self.threecell.registTextFiled.secureTextEntry = YES;
            return self.threecell;
        }
            break;
            
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 46;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    ICN_RegisterFootView *footView=[[ICN_RegisterFootView alloc]init];
    footView=[[NSBundle mainBundle]loadNibNamed:@"ICN_RegisterFootView" owner:self options:nil].lastObject;
    [footView.RegisterBtn addTarget:self action:@selector(ClickBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [footView.protocolBtn addTarget:self action:@selector(protocolBtns) forControlEvents:UIControlEventTouchUpInside];
    return footView;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 100;
}

@end
