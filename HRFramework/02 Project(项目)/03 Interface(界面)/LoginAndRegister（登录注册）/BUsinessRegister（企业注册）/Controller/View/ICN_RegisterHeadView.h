//
//  ICN_RegisterHeadView.h
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef void (^PhoneRegister)();
//typedef void (^mailRegister)();

@class ICN_RegisterHeadView;

@protocol HeadBtn <NSObject>

-(void)ICN_HeadBtn:(ICN_RegisterHeadView *)view;

@end

@interface ICN_RegisterHeadView : UIView


@property (weak, nonatomic) IBOutlet UIView *moveView;
@property (weak, nonatomic) IBOutlet UIView *rightView;

@property(nonatomic,weak)id<HeadBtn>delegate;

@property (weak, nonatomic) IBOutlet UIButton *phoneRegisterBtn;
@property (weak, nonatomic) IBOutlet UIButton *mailRegisterBtn;

//@property(nonatomic,strong)PhoneRegister phoneRegister;
//@property(nonatomic,strong)mailRegister mailRegister;
//
//-(void)phonrRegister:(PhoneRegister)block;
//-(void)mailRegister:(mailRegister)block;

@end
