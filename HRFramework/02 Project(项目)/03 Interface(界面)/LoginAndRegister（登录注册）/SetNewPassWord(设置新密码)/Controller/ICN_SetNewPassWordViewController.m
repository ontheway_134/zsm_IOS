//
//  ICN_SetNewPassWordViewController.m
//  ICan
//
//  Created by shilei on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SetNewPassWordViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_SignViewController.h"
#import "ICN_Regular.h"

@interface ICN_SetNewPassWordViewController()
@property (weak, nonatomic) IBOutlet UIButton *tureBtn;


@property (weak, nonatomic) IBOutlet UITextField *repeatpasswordTextfiled;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextfiled;


@end


@implementation ICN_SetNewPassWordViewController

#pragma mark - --- 网络请求 ---

-(void)httpForGet{
    NSDictionary *dic=@{@"id":self.uesrID,@"password":self.passwordTextfiled.text,@"rePassword":self.repeatpasswordTextfiled.text};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_USERFINDPASSWOED params:dic success:^(id result) {
        BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (codemodel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"修改成功请登录"];
            ICN_SignViewController *sign=[[ICN_SignViewController alloc]init];
            [self.navigationController pushViewController:sign animated:YES];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tureBtn.layer.cornerRadius = 5.0;
    self.tureBtn.layer.masksToBounds = YES;
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)commitActions:(id)sender {
    
    if (![self.passwordTextfiled.text isEqualToString:self.repeatpasswordTextfiled.text] ) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"两次密码不一致"];
        return;
    }
    if (![ICN_Regular checkPassword:self.passwordTextfiled.text]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入6-18位字母和数字混合的密码"];
        return;
    }
    [self httpForGet];
}


@end
