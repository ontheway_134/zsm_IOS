//
//  ICN_EditGroupMessageView.h
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_EditGroupMessageView;

@protocol ICN_BtnCommitDelegate <NSObject>

-(void)ICN_BtnSCommitDelegate:(UIButton *)btn;

@end

@interface ICN_EditGroupMessageView : UIView

@property(nonatomic,weak)id<ICN_BtnCommitDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;  //头像

@property (weak, nonatomic) IBOutlet UITextField *groupTextFiled;  //小组名称

@property (weak, nonatomic) IBOutlet UITextField *groupCount;   //小组人数
@property (weak, nonatomic) IBOutlet UITextView *GroupIntroView;   //小组简介

@property(nonatomic,assign)NSInteger openNumber;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancerBtn;

@end
