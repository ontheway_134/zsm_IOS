//
//  ICN_GroupDetialViewController.m
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_GroupDetialViewController.h"
#import "ICN_GroupDetialHeaderView.h"
#import "ICN_InterestTableViewCell.h"
#import "ICN_EditGroupMessageView.h"
#import "ICN_GroupDetialModel.h"
#import "BaseOptionalModel.h"
#import "ICN_ApplyCommitRequestViewController.h"
#import "ICN_GroupDetialModel.h"
#import "ICN_UserHomePagerVC.h"
#import "ICN_VerityViewController.h"
#import "ICN_CameraBallView.h"
#import "ICN_SelsectVIew.h"
#import "ICN_vetityModelss.h"
#import "ICN_loadHeadImageModel.h"
#import "ChatViewController.h"
#import "ICN_buyaogao.h"
#import "ICN_MyPersonalHeaderModel.h"
#import "BaseOptionalModel.h"

@interface ICN_GroupDetialViewController ()<UITableViewDelegate,UITableViewDataSource,ICN_BtnSelectedDelegate,ICN_BtnCommitDelegate,ICN_BtnSelectedAddDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *GroupDetialTableView;
@property(nonatomic,strong)ICN_EditGroupMessageView *edit;
@property(nonatomic,strong)NSMutableArray *groupData;
@property(nonatomic,strong)NSMutableArray *memberData;
@property(nonatomic,strong)ICN_GroupDetialHeaderView *header;
@property(nonatomic,strong)ICN_InterersPeople *headermodel;
@property(nonatomic,strong)ICN_InterersPeople *membermodel;

@property(nonatomic,strong)NSString *idididid;
@property(nonatomic,strong)NSString *selectmemberid;
@property(nonatomic,strong)NSMutableArray *ImageData;
@property (nonatomic , strong)NSMutableArray *NewFridentDataArrss;
@property(nonatomic,strong)BaseOptionalModel *basemodel;
@property (weak, nonatomic) IBOutlet UIButton *enternBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottom;//表格的高度

@end

@implementation ICN_GroupDetialViewController

#pragma mark - --- 生命周期 ---

//申请新添加的好友的列表
-(void)httpverity{
    
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupId":self.groupId};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_APPLYGROUPSS params:dic success:^(id result) {
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.NewFridentDataArrss removeAllObjects];
        if (base.code == 0) {
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_vetityModelss *model = [[ICN_vetityModelss alloc] initWithDictionary:dic error:nil];
                [self.NewFridentDataArrss addObject:model];
            }
            
        }
        [self.GroupDetialTableView reloadData];
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"失败");
    }];
    
}

//上图图片的网络请求
-(void)loadHeadImageView{
    
    if (self.ImageData.count == 0) {
        [self.ImageData addObject:self.edit.headImageView.image];
    }
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@""];
    [[[HRNetworkingManager alloc]init] UPLOAD_IMAGES:self.ImageData Path:PATH_MultipleImagesUpload progress:^(int64_t bytesProgress, int64_t totalBytesProgress) {
        NSLog(@"请求成功");
    } success:^(id responseObject) {
        ICN_loadHeadImageModel *model = [[ICN_loadHeadImageModel alloc] initWithDictionary:responseObject error:nil];
        if (model.code == 0) {
            [self HttpeditGroup:model.result.fileName];
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
        }else{
            [self HttpeditGroup:ICN_IMG(self.headermodel.groupLogo)];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求 失败");
    }];
    
}

//编辑小组信息
-(void)HttpeditGroup:(NSString *)file{
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupId":self.groupId,@"groupLogo":file,@"groupName":self.edit.groupTextFiled.text,@"summary":self.edit.GroupIntroView.text,@"number":@1,@"open":[NSString stringWithFormat:@"%ld",(long)self.edit.openNumber]};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_EDITGROUP params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [MBProgressHUD  ShowProgressWithBaseView:self.view Message:@"修改成功"];
            
        }else{
            [MBProgressHUD  ShowProgressWithBaseView:self.view Message:basemodel.info];
        }
        [self.edit removeFromSuperview];
        [self httpNetWoring];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"编辑小组信息是失败");
    }];
}

-(void)httpNetWoring{
    
    //此接口数据格式反的应该不对，小组详情应该是｛1234［｛｝｛｝］｝格式，已赋值完成
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MYGROUPDETIAL params:@{@"groupId":self.groupId , @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        self.basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        
        if (self.basemodel.code == 0) {
            
            [self.groupData removeAllObjects];
            [self.memberData removeAllObjects];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:dic error:nil];
                
                NSInteger memberId = [[dic valueForKey:@"memberId"] integerValue];
                model.memberId = SF(@"%ld",memberId);
                [self.groupData addObject:model];
                
            }
            self.headermodel= self.groupData[0];
            
            for (ICN_InterersPeople *tempModel in self.groupData) {
                if (tempModel.groupRole.integerValue == 1) {
                    self.headermodel = tempModel;
                }
                
                if (tempModel.groupRole.integerValue == 3) {
                    [self.memberData addObject:tempModel];
                }
                
            }
            
        }else{
            [MBProgressHUD  ShowProgressWithBaseView:self.view Message:self.basemodel.info];
        }
        [self.GroupDetialTableView reloadData];
        [self.GroupDetialTableView.mj_header endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
        [self.GroupDetialTableView.mj_header endRefreshing];
        
    }];
    
}

//踢出好友
-(void)httpKivk:(NSString *)memberid{
    
    NSDictionary *dic=@{@"memberId":memberid,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupId":self.groupId};
    
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_KICTGROUP params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"踢出成功"];
            [self.GroupDetialTableView reloadData];
            [self refresh];
            
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求失败");
    }];
    
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.GroupDetialTableView registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.GroupDetialTableView.delegate=self;
    self.GroupDetialTableView.dataSource=self;
    self.GroupDetialTableView.separatorStyle = NO;
    [self refresh];
    
    //存值给view使用
    NSLog(@"%@",self.groupId);
    [[NSUserDefaults standardUserDefaults] setObject:self.groupId forKey:@"groupID"];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self refresh];
    [self.navigationController setNavigationBarHidden:YES];
    self.tabBarController.hidesBottomBarWhenPushed = YES;
    
}

#pragma mark - --- Protocol ---

#pragma mark - --- UIImagePickerControllerDelegate ---

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    [self.view addSubview:self.edit];
    [self.edit mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    
    UIImage *newPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    //把newPhono设置成头像
    self.edit.headImageView.image = newPhoto;
    [self.ImageData addObject:self.edit.headImageView.image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ICN_BtnSelectedDelegate


-(void)ICN_BtnSelectedDelegate:(UIButton *)btn and:(ICN_GroupDetialHeaderView *)detialHeader{
    
    switch ([btn tag]) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        case 1:{
            
            self.edit=[[[NSBundle mainBundle] loadNibNamed:@"ICN_EditGroupMessageView" owner:self options:nil] lastObject];
            self.edit.delegate =self;
            [self.view addSubview:self.edit];
            [self.edit mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(0);
                make.bottom.mas_equalTo(0);
                make.left.mas_equalTo(0);
                make.right.mas_equalTo(0);
            }];
            self.edit.headImageView.userInteractionEnabled=YES;
            
            [self.edit.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(self.headermodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
            self.edit.groupTextFiled.text=self.headermodel.groupName;
            self.edit.groupCount.text=[NSString stringWithFormat:@"%@",self.headermodel.number];
            self.edit.GroupIntroView.text=self.headermodel.summary;
            //判断是否公开
            if ([self.headermodel.isopen isEqualToString:@"0"]) {
                self.edit.confirmBtn.selected = YES;
            }else{
                self.edit.cancerBtn.selected = YES;
            }
            
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sssssssss)];
            [self.edit.headImageView addGestureRecognizer:tap];
        }
            
            break;
        case 2:{
            ICN_ApplyCommitRequestViewController *apply=[[ICN_ApplyCommitRequestViewController alloc]init];
            apply.groupId =self.groupId;
            apply.gtoupname=self.headermodel.groupName;
            [self.navigationController pushViewController:apply animated:YES];
            
        }
            break;
        case 3:{
            
            ICN_VerityViewController *newFrident=[[ICN_VerityViewController alloc]init];
            newFrident.gropuid=self.groupId;
            [self.navigationController pushViewController:newFrident animated:YES];
        }
            break;
    }
    
}


#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    NSDictionary *dic;
    if (cell.model) {
        dic=@{@"fromId":SF(@"%@",[USERDEFAULT valueForKey:HR_CurrentUserToken]),@"toId":cell.model.memberId};
    }else{
        ICN_InterersPeople *groupModel = cell.groupDdetialmodel;
        dic=@{@"fromId":SF(@"%@",[USERDEFAULT valueForKey:HR_CurrentUserToken]),@"toId":groupModel.memberId};
    }
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
        
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [btn setTitle:@"等待验证" forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}



#pragma mark ICN_BtnCommitDelegate

//编辑小组信息的上传图片的点击按钮，这里面不需要添加小组名称，小组人数的判断，为空也可以上传图片，判断在走完图片的接口在判断
-(void)ICN_BtnSCommitDelegate:(UIButton *)btn{
    
    if ([self.edit.groupCount.text integerValue] <=0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入正确的人数"];
        
        return;
    }
    if ([self.edit.groupCount.text integerValue] >50) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"最大人数为50"];
        return;
    }
    
    
    [self loadHeadImageView];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 1;
            break;
            
        case 1:
            return self.memberData.count;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    //=== 获取到的是组长的数据
    switch (indexPath.section) {
        case 0:{
            cell.addFridentBtn.hidden = YES;
            cell.model = self.headermodel;
            
        }
            break;
            
        case 1:{
            //=== 获取到的是组员的数据
            //   cell.addFridentBtn.hidden = YES;
            cell.delegate =self;
            cell.addFridentBtn.hidden = NO;
            cell.groupDdetialmodel=self.memberData[indexPath.row];
            
        }
            break;
    }
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 57;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        self.header=[[NSBundle mainBundle]loadNibNamed:@"ICN_GroupDetialHeaderView" owner:self options:nil].lastObject;
        self.header.delegate=self;
        [self.header.groupImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(self.headermodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        self.header.groupNick.text=self.headermodel.groupName;
        self.header.groupPeopleCount.text=[NSString stringWithFormat:@"人数:%@/%@",self.headermodel.memberCount,self.headermodel.number];
        self.header.groupIntroduce.text=self.headermodel.summary;
        
        if (self.isShow) {
//            self.tableViewBottom.constant = -40;
            // 在这里还需要添加判断是不是组长的信息 3为普通成员
            if ([self.basemodel.myGroupRole isEqualToString:@"3"]) {
                self.header.editBtn.hidden = YES;
                self.header.addGroupBtn.hidden = YES;
                self.header.GroupDetialView.hidden = YES;
                self.header.groupAuto.constant = -50;
            }else{
                self.header.addGroupBtn.hidden = YES;
            }
            
        }else{
            self.header.editBtn.hidden = YES;
            self.header.GroupDetialView.hidden = YES;
            self.header.groupAuto.constant = -50;
            self.enternBtn.hidden = YES;
            
        }
        
        if (self.NewFridentDataArrss.count > 0) {
            if (self.NewFridentDataArrss.count == 1) {
                ICN_vetityModelss *model01 = self.NewFridentDataArrss[0];
                [self.header.imageview01 sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model01.memberLogo)]];
            }
            if (self.NewFridentDataArrss.count >= 2) {
                ICN_vetityModelss *model02 = self.NewFridentDataArrss[1];
                [self.header.imageview02 sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model02.memberLogo)]];
            }
            if (self.NewFridentDataArrss.count >= 3) {
                ICN_vetityModelss *model03 = self.NewFridentDataArrss[2];
                [self.header.imageview03 sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model03.memberLogo)]];
            }
        }
        
        return self.header;
        
    }
    return nil;
}

- (NSString*) tableView:(UITableView *) tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return @"组员";
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            
            if ([self.basemodel.myGroupRole isEqualToString:@"3"]){
                return 320;
                
            }
            
            if ([self.basemodel.myGroupRole isEqualToString:@"1"]){
                return 370;
            }
            
            
            if (self.isShow) {
                
                return 370;
            }else{
                return 320;
            }
        }
            
            break;
            
        case 1:
            return 36;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            
            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
            ICN_InterestTableViewCell *cell= (ICN_InterestTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            userhome.memberId=cell.model.memberId;
            if(userhome.memberId == nil){
                userhome.memberId = @"3";
            }
            [self.navigationController pushViewController:userhome animated:YES];
        }
            
            break;
            
        default:{
            
            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
//            ICN_InterestTableViewCell *cell= (ICN_InterestTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            ICN_InterersPeople *model =self.memberData[indexPath.row];
            userhome.memberId=model.memberId;
            if(userhome.memberId == nil){
                userhome.memberId = @"3";
            }
            [self.navigationController pushViewController:userhome animated:YES];
        }
            break;
    }
    
}

#pragma mark - --- IBActins ---

- (IBAction)enterActions:(UIButton *)sender {
    // [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能尚未开通"];
    
    NSDictionary *dic=@{@"token":[[NSUserDefaults standardUserDefaults] valueForKey:HR_CurrentUserToken],@"groupId":self.groupId};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_GROUPCHAT params:dic success:^(id result) {
        
        ICN_buyaogao *basemodels=[[ICN_buyaogao alloc]initWithDictionary:result error:nil];
        //如果code等于0，则code等于1
        if (basemodels.code == 0) {
            
            //  NSDictionary *dic =@{}
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:token forKey:@"token"];
            [[[HRRequestManager alloc] init] POST_URL:@"http://1ican.com/index.php/Member/Member/getIndexInfo" params:dic success:^(id result) {
                NSDictionary *dict = result[@"result"]
                ;
                ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                
                if (model.memberLogo) {
                    [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
                }
                if (model.memberNick) {
                    [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
                }
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            NSString *mylogo=[USERDEFAULT valueForKey:ICN_UserIconUrl];
            NSString *mynick=[USERDEFAULT valueForKey:ICN_UserNick];
            
    
//            ChatViewController *groupchat=[[ChatViewController alloc]initWithConversationChatter:basemodels.result conversationType:EMConversationTypeGroupChat ext:@{@"groupname":self.headermodel.groupName,@"groupheadUrl":self.headermodel.groupLogo,@"username":mynick,@"headUrl":mylogo}];
//            
//           [self.navigationController pushViewController:groupchat animated:YES];
            //warn=== 小组进入群聊的位置
            NSDictionary *params;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                params = @{@"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken] , @"groupId" : self.groupId};
            }
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_HXGroupIdList params:params success:^(id result) {
                NSMutableArray *groupInfoList = [NSMutableArray array];
                BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (baseModel.code == 0) {
                    // 获取数据成功
                    NSArray *array = [result valueForKey:@"result"];
                    if (array) {
                        for (NSDictionary *info in array) {
                            NSDictionary *item = @{
                                                   ICN_UserHXId:[info valueForKey:@"hxusername"],
                                                   ICN_UserNick:[info valueForKey:@"memberNick"],
                                                   ICN_UserIconUrl:ICN_IMG([info valueForKey:@"memberLogo"])
                                                   };
                            [groupInfoList addObject:item];
                        }
                        ChatViewController *groupchat=[[ChatViewController alloc]initWithConversationChatter:basemodels.result conversationType:EMConversationTypeGroupChat ext:@{@"username":@"",@"headUrl":@""}];
                        groupchat.groupChatInfo = groupInfoList;
                        [groupchat setHidesBottomBarWhenPushed:YES];
                        [self.navigationController pushViewController:groupchat animated:YES];
                        
                    }
                }
                
            } failure:^(NSDictionary *errorInfo) {
                HRLog(@"%@",errorInfo);
            }];
        }
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"数据获取失败");
    }];
    
    
    
}

//点击头像弹出相机和相册的蒙版
-(void)sssssssss{
    ICN_CameraBallView *camera=[[ICN_CameraBallView alloc]init];
    camera=[[[NSBundle mainBundle]loadNibNamed:@"ICN_CameraBallView" owner:nil options:nil] lastObject];
    [[UIApplication sharedApplication].keyWindow addSubview:camera];
    [camera mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
    }];
    [camera returnLocationPhoto:^{
        [self.edit removeFromSuperview];
        [camera removeFromSuperview];
        UIImagePickerController *controller=[[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = YES;
        [controller setDelegate:self];
        [self presentViewController:controller animated:YES completion:nil];
        
    }];
    [camera returnTakingPhoto:^{
        [self.edit removeFromSuperview];
        UIImagePickerController *controller = [[UIImagePickerController alloc]init];
        controller.sourceType=UIImagePickerControllerSourceTypeCamera;
        [controller setDelegate:self];
        [self presentViewController:controller animated:YES completion:nil];
        [camera removeFromSuperview];
    }];
    
}





- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return NO;
            break;
            
        default:{
            //isShow为1时是我的小组
            if (self.isShow) {
                
                // 在这里还需要添加判断是不是组长的信息 3为普通成员，普通成员没有权限踢出成员
                if ([self.basemodel.myGroupRole isEqualToString:@"3"]) {
                    return NO;
                }else{
                    return YES;
                }
                
            }else{
                return NO;
            }
            
        }
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        ICN_SelsectVIew *selsect=[[[NSBundle mainBundle]loadNibNamed:@"ICN_SelsectVIew" owner:self options:nil]firstObject];
        selsect.friendId = [self.memberData[indexPath.row] memberId];
        [[UIApplication sharedApplication].keyWindow addSubview:selsect];
        [selsect mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
        }];
        
        [selsect confirmClickBlock:^(NSString *member) {
            
            [self httpKivk:[self.memberData[indexPath.row] memberId]];
            [selsect removeFromSuperview];
        }];
        
        [selsect cancelClickBlock:^{
            [selsect removeFromSuperview];
        }];
        
        
        
    }
    
}


-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"踢出";
}


#pragma mark - --- 自定义方法 ---

-(void)refresh{
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self httpNetWoring];
        [self httpverity];
        
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.GroupDetialTableView.mj_header = header;
    
    [self.GroupDetialTableView.mj_header beginRefreshing];
    
}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)groupData{
    if (!_groupData) {
        _groupData=[NSMutableArray array];
    }
    return _groupData;
}

-(NSMutableArray *)memberData{
    if (!_memberData) {
        _memberData=[NSMutableArray array];
    }
    return _memberData;
}

-(NSMutableArray *)ImageData{
    if (!_ImageData) {
        _ImageData=[NSMutableArray array];
    }
    return _ImageData;
}

- (NSMutableArray *)NewFridentDataArrss{
    if (_NewFridentDataArrss == nil) {
        _NewFridentDataArrss = [NSMutableArray array];
    }
    return _NewFridentDataArrss;
}


@end
