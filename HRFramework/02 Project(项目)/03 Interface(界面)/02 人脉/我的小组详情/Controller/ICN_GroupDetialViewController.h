//
//  ICN_GroupDetialViewController.h
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_GroupDetialViewController : BaseViewController

@property(nonatomic,strong)NSString *groupId;   //小组的id

@property(nonatomic,assign)BOOL isShow;

@end
