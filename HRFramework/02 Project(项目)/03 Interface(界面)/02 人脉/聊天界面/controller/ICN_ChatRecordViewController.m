//
//  ICN_ChatRecordViewController.m
//  ICan
//
//  Created by zjk on 2017/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ChatRecordViewController.h"
#import "ICN_ChatRecordTableViewCell.h"
#import "ToolAboutTime.h"

@interface ICN_ChatRecordViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *mainTableview;

@property (strong, nonatomic) NSArray *conversations;

@end

@implementation ICN_ChatRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.mainTableview registerNib:[UINib nibWithNibName:@"ICN_ChatRecordTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ChatRecordTableViewCell"];
    
    self.naviTitle = @"聊天记录";
    
    [self configDataSoure];
}

- (void)configDataSoure {
    self.conversations = [[EMClient sharedClient].chatManager getAllConversations];
    [self.mainTableview reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.conversations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ICN_ChatRecordTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatRecordTableViewCell" forIndexPath:indexPath];
    EMConversation *conversation = self.conversations[indexPath.row];
    NSLog(@"%@",ICN_IMG(conversation.ext[@"headUrl"]));
    if (conversation.type == 0) {
        if (conversation.ext) {
            [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:conversation.ext[@"headUrl"]?conversation.ext[@"headUrl"]:@""] placeholderImage:[UIImage imageNamed:@"修改头像"]];
            
            cell.nickLab.text = conversation.ext[@"username"]?conversation.ext[@"username"]:@"";
        } else {
            [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:conversation.latestMessage.ext[@"headUrl"]?conversation.latestMessage.ext[@"headUrl"]:@""] placeholderImage:[UIImage imageNamed:@"修改头像"]];
            
            cell.nickLab.text = conversation.latestMessage.ext[@"username"]?conversation.latestMessage.ext[@"username"]:@"";
        }
        
    } else {
        [cell.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(conversation.latestMessage.ext[@"groupheadUrl"]?conversation.latestMessage.ext[@"groupheadUrl"]:@"")] placeholderImage:[UIImage imageNamed:@"修改头像"]];
        
        cell.nickLab.text = conversation.latestMessage.ext[@"groupname"]?conversation.latestMessage.ext[@"groupname"]:@"";
    }
    
    EMMessageBody *msgBody = conversation.latestMessage.body;
    switch (msgBody.type) {
        case EMMessageBodyTypeText:
        {
            // 收到的文字消息
            EMTextMessageBody *textBody = (EMTextMessageBody *)msgBody;
            NSString *txt = textBody.text;
            cell.newsLab.text = txt;
        }
            break;
        case EMMessageBodyTypeImage:
        {
            cell.newsLab.text = @"图片";
        }
            break;
        case EMMessageBodyTypeVoice:
        {
            cell.newsLab.text = @"语音";
        }
            break;
        
            
        default:
            break;
    }
    cell.dateLab.text = [ToolAboutTime getTimeStrByTimeSp:[NSString stringWithFormat:@"%lld",conversation.latestMessage.timestamp]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    EMConversation *conversation = self.conversations[indexPath.row];
    ChatViewController *chatView;
    if (conversation.type == 0) {
        if (conversation.ext) {
            chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"username":conversation.ext[@"username"]?conversation.ext[@"username"]:@"",@"headUrl":conversation.ext[@"headUrl"]?conversation.ext[@"headUrl"]:@""}];
            chatView.title = conversation.ext[@"username"];
        } else {
            chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"username":conversation.latestMessage.ext[@"username"]?conversation.latestMessage.ext[@"username"]:@"",@"headUrl":conversation.latestMessage.ext[@"headUrl"]?conversation.latestMessage.ext[@"headUrl"]:@""}];
            chatView.title = conversation.latestMessage.ext[@"username"];
        }
        
    } else {
        NSLog(@"%@",conversation.ext[@"groupheadUrl"]);
        chatView  = [[ChatViewController alloc] initWithConversationChatter:conversation.conversationId conversationType:conversation.type ext:@{@"groupname":conversation.latestMessage.ext[@"groupname"]?conversation.latestMessage.ext[@"groupname"]:@"",@"groupheadUrl":conversation.latestMessage.ext[@"groupheadUrl"]?conversation.latestMessage.ext[@"groupheadUrl"]:@""}];
        chatView.title = conversation.ext[@"groupname"];
    }
    
    
    [chatView setHidesBottomBarWhenPushed:YES];
    [self.navigationController pushViewController:chatView animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}


@end
