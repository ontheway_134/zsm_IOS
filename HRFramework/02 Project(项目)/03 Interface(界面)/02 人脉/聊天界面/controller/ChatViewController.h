/**
 @method
 @brief 初始化聊天页面
 @discussion
 @param conversationChatter 会话对方的用户名. 如果是群聊, 则是群组的id
 @param conversationType 会话类型
 @param ext 扩展消息（传入用户名:username 头像：headUrl）
 @result
 
- (instancetype)initWithConversationChatter:(NSString *)conversationChatter
conversationType:(EMConversationType)conversationType
ext:(NSDictionary *)ext;

 */


#import "EaseMessageViewController.h"

#define KNOTIFICATIONNAME_DELETEALLMESSAGE @"RemoveAllMessages"


@interface ChatViewController : EaseMessageViewController <EaseMessageViewControllerDelegate, EaseMessageViewControllerDataSource>

@property (nonatomic , retain)NSMutableDictionary *singleChatInfo;

@property (nonatomic , strong)NSMutableArray *groupChatInfo;



- (void)showMenuViewController:(UIView *)showInView
                  andIndexPath:(NSIndexPath *)indexPath
                   messageType:(EMMessageBodyType)messageType;

@end
