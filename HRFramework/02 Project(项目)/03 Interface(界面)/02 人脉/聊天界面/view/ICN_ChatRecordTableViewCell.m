//
//  ICN_ChatRecordTableViewCell.m
//  ICan
//
//  Created by zjk on 2017/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ChatRecordTableViewCell.h"

@implementation ICN_ChatRecordTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.headImageBtn.imageView.contentMode = UIViewContentModeScaleAspectFill;
}


@end
