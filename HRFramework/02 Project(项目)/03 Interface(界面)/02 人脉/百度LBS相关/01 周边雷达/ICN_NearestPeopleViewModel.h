//
//  ICN_NearestPeopleViewModel.h
//  ICan
//
//  Created by albert on 2017/2/22.
//  Copyright © 2017年 albert. All rights reserved.
//


/** 使用周边雷达的处理类
 1. 暂时将该类设置为单例类型以确保在应用的试用期间会持续调用周边雷达的类来获取实时的周边数据
 2. 因为该单例存在代理，可能会出现因为多个位置调用该单例产生的代理异常的问题
 
 */

#import <Foundation/Foundation.h>

//@class <#Model类名#>;

@protocol NearestPeopleDelegate <NSObject>

- (void)responseNearestPeopleSearchWithSuccess:(BOOL)success Error:(NSString *)error;

@end

@interface ICN_NearestPeopleViewModel : NSObject

@property (nonatomic , weak)id<NearestPeopleDelegate> delegate;
//@property (nonatomic , strong)<#Model类名#> *model;

/** 用于获取周边雷达管理器的方法 */
+ (instancetype)shareNearestPeopleManager;

/** 开始周边雷达检索 */
- (void)beginNearestPersonalSearch;

/** 结束周边雷达的检索并清空我的位置 */
- (void)clearUserNearsetLocationData;



@end
