//
//  ICN_NearestPeopleViewModel.m
//  ICan
//
//  Created by albert on 2017/2/22.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_NearestPeopleViewModel.h"

static ICN_NearestPeopleViewModel *manager;
static NSInteger const locationSearchDurationTime = 5; // 默认用户上传位置间隔时间5秒

@interface ICN_NearestPeopleViewModel ()<BMKRadarManagerDelegate>

@property (nonatomic , assign , getter=isLocationClear)BOOL clearLocation; // 用于判断位置是否被清空的方法

@property (nonatomic , strong)BMKRadarManager *radarManager; // 周边雷达管理器属性

@end

@implementation ICN_NearestPeopleViewModel

#pragma mark - ---------- 懒加载 ----------

- (BMKRadarManager *)radarManager{
    //    周边雷达管理类使用了单例模式，并且通过引用计数的方式管理这个实例。可以使用使用下边的方法获取实例（引用计数加1）
    if (_radarManager == nil) {
        _radarManager = [BMKRadarManager getRadarManagerInstance];
    }
    return _radarManager;
}


+ (instancetype)shareNearestPeopleManager{
    if (manager == nil) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            manager = [[ICN_NearestPeopleViewModel alloc] init];
            // 在这里添加属性的初始化
            manager.clearLocation = NO; // 默认设置初始化的时候位置没有被清空
        });
    }
    return manager;
}

- (void)beginNearestPersonalSearch{
    // 获取周边雷达管理器属性 并设置周边雷达管理器的userId
    self.radarManager.userId = ICN_BaiduLBSUserID;
    // 通过添加radar delegate获取自动上传时的位置信息，以及获得雷达操作结果
    [self.radarManager addRadarManagerDelegate:self];
    // 在开始的时候持续调用百度的周边雷达SDK 上传位置
    [self.radarManager startAutoUpload:locationSearchDurationTime];
    // 开始雷达搜索功能
    [self beginNearestPersonalSearch];
}

// 使用百度地图搜索付附近的人的方法
- (void)beginBaiduLBSSearch{
    // 初始化百度检索的类
    BMKRadarNearbySearchOption *option = [[BMKRadarNearbySearchOption alloc] init];
    // 设置检索半径
    option.radius = 8000;
    // 设置默认的排序方式是由近及远
    option.sortType = BMK_RADAR_SORT_TYPE_DISTANCE_FROM_NEAR_TO_FAR;
    // 设置搜索的中心点
    option.centerPt = CLLocationCoordinate2DMake(39.916, 116.404);//检索中心点
    // 发起搜索
    BOOL res = [self.radarManager getRadarNearbySearchRequest:option];
    if (res) {
        // 搜索成功
    }else{
        // 搜索失败
    }
}

- (void)clearUserNearsetLocationData{
    // 在移除雷达的时候的相关操作
    // 1. 清空雷达数据用用户的位置
    [self.radarManager stopAutoUpload];
    [self.radarManager clearMyInfoRequest];
    // 2. 使雷达管理器的引用计数减一
    [BMKRadarManager releaseRadarManagerInstance];
    // 3. 在不需要时，需要移除radar delegate，否则会影响内存的释放
    [self.radarManager removeRadarManagerDelegate:self];
    // 4. 设置清空属性为yes
    self.clearLocation = YES;
}

// 第二步，实现BMKRadarManagerDelegate回调方法获取结果，核心代码如下：
- (void)onGetRadarNearbySearchResult:(BMKRadarNearbyResult *)result error:(BMKRadarErrorCode)error {
    NSLog(@"onGetRadarNearbySearchResult  %d", error);
    if (error == BMK_RADAR_NO_ERROR) {
        HRLog(@"%@",result);
        // 将获取的数据转化成合理的形式并输出
    }
}

- (void)dealloc{
    // 判断雷达在销毁的时候是否清空·没有清空则默认清空
    if (self.isLocationClear == NO) {
        [self clearUserNearsetLocationData];
    }
    // 清空代理
    self.delegate = nil;
    // 设置雷达属性为nil
    self.radarManager = nil;
}


@end
