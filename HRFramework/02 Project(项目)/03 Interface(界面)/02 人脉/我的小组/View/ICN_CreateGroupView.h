//
//  ICN_CreateGroupView.h
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_CreateGroupView : UIView
@property (weak, nonatomic) IBOutlet UIButton *CommitBtnAction;
@property (weak, nonatomic) IBOutlet UITextField *GroupNameTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *GroupCountTextfiled;
@property (weak, nonatomic) IBOutlet UITextView *GroupIntroTextView;

@property (weak, nonatomic) IBOutlet UIImageView *headImageViews;   //这个试图需要添加手势，添加点击事件

@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property(nonatomic,assign)NSInteger openNumber;



@end
