//
//  ICN_JoinGroupViewController.m
//  ICan
//
//  Created by shilei on 17/2/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_JoinGroupViewController.h"
#import "ICN_NewFridentTableViewCell.h"
#import "HRNetworkingManager.h"
#import "ICN_JoinGroupModel.h"
#import "ICN_GroupDetialViewController.h"

@interface ICN_JoinGroupViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *JoinGroupTableView;
@property(nonatomic,strong)NSMutableArray *joinGroupArr;
@property(nonatomic,assign)NSInteger page;


@end

@implementation ICN_JoinGroupViewController

#pragma mark - --- 网络请求 ---

-(void)httpNetworingpage:(NSInteger)page{
    
    
    NSDictionary *dic=@{@"friendId":self.GroupID,@"page":[NSString stringWithFormat:@"%ld",(long)page],@"pagenum":@10};
    
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_UserGroups params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            if (self.page == 1) {
                [self.joinGroupArr removeAllObjects];
            }
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_JoinGroupModel *model=[[ICN_JoinGroupModel alloc]initWithDictionary:dic error:nil];
                [self.joinGroupArr addObject:model];
            }
           
        }
        [self.JoinGroupTableView.mj_header endRefreshing];
        [self.JoinGroupTableView.mj_footer endRefreshing];
        [self.JoinGroupTableView reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
        [self.JoinGroupTableView.mj_footer endRefreshing];
        [self.JoinGroupTableView.mj_header endRefreshing];
    }];

}


#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.JoinGroupTableView registerNib:[UINib nibWithNibName:@"ICN_NewFridentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NewFridentTableViewCell"];
    self.JoinGroupTableView.delegate =self;
    self.JoinGroupTableView.dataSource = self;
    self.JoinGroupTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self frefsh];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.joinGroupArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_NewFridentTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_NewFridentTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_NewFridentTableViewCell" owner:self options:nil] lastObject];
        
    }
    cell.TureBtn.hidden= YES;
    cell.FalseBtn.hidden = YES;
    cell.joinmodel = self.joinGroupArr[indexPath.row];
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_GroupDetialViewController *detial=[[ICN_GroupDetialViewController alloc]init];
    detial.groupId = [self.joinGroupArr[indexPath.row] groupId];
    
    detial.isShow=YES;
    [self.navigationController pushViewController:detial animated:YES];

}

#pragma mark - --- IBActions ---

- (IBAction)backActions:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - --- 自定义方法 ---

//刷新
-(void)frefsh{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{

        self.page = 1;
        [self httpNetworingpage:self.page];
   
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    
    
    self.JoinGroupTableView.mj_header = header;
    self.JoinGroupTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{

        self.page+=1;
        [self httpNetworingpage:self.page];

    }];
    [self.JoinGroupTableView.mj_header beginRefreshing];
    
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)joinGroupArr{
    if (!_joinGroupArr) {
        _joinGroupArr=[NSMutableArray array];
    }
    return _joinGroupArr;

}


@end
