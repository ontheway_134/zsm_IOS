//
//  ICN_MyGroupModel.h
//  ICan
//
//  Created by shilei on 16/12/30.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyGroupModel : BaseOptionalModel

@property(nonatomic,strong)NSString *groupLogo;  //组logo
@property(nonatomic,strong)NSString *groupName;   //组名字
@property(nonatomic,strong)NSString *summary;    //组的描述

@property(nonatomic,strong)NSString *groupId;   //组id;
@property(nonatomic,strong)NSString *groupAssessment;   //

@property(nonatomic,strong)NSString *isopen;   //是否公开

@property(nonatomic,strong)NSString *memberCount;   //会员的数量

@property(nonatomic,strong)NSString *memberId;    //会员的id

@property(nonatomic,strong)NSString *number;   //创建的小组的人数

@property(nonatomic,strong)NSString *status;    //状态

@end
