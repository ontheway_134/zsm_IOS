//
//  ICN_ApplyRecodeModels.h
//  ICan
//
//  Created by shilei on 17/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_ApplyRecodeModels : BaseOptionalModel

@property(nonatomic,strong)NSString *content;   //申请理由, 无则为null

@property(nonatomic,strong)NSString *memberId;   //用户id

@property(nonatomic,strong)NSString *status;      //状态：0正常，1离开  2已申请

@property(nonatomic,strong)NSString *memberLogo;  //用户logo,无则为null

@property(nonatomic,strong)NSString *memberNick;   //用户昵称

@property(nonatomic,strong)NSString *companyName; //用户公司名称,无则为null

@property(nonatomic,strong)NSString *memberPosition;   //会员的职务

@property(nonatomic,strong)NSString *memberSchool;     //用户大学",无则为null

@property(nonatomic,strong)NSString *memberMajor;    //用户专业,无则为null

@property(nonatomic,strong)NSString *plusv;     //是否加V用户,加V验证0未验证1已验证

@property(nonatomic,strong)NSString *groupName;   //小组的名称

@property(nonatomic,strong)NSString *summary;    //小组的描述

@property(nonatomic,strong)NSString *memberCount;   //组内的人数

@property(nonatomic,strong)NSString *groupLogo;     //小组的头像


@end
