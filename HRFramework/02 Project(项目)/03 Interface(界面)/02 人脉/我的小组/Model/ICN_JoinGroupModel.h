//
//  ICN_JoinGroupModel.h
//  ICan
//
//  Created by shilei on 17/2/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_JoinGroupModel : BaseOptionalModel

@property(nonatomic,strong)NSString *groupAssessment;
@property(nonatomic,strong)NSString *groupId;
@property(nonatomic,strong)NSString *groupLogo;
@property(nonatomic,strong)NSString *groupName;
@property(nonatomic,strong)NSString *isopen;
@property(nonatomic,strong)NSString *memberCount;
@property(nonatomic,strong)NSString *memberId;
@property(nonatomic,strong)NSString *number;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *summary;

@end
