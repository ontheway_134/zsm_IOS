//
//  ICN_loadHeadImageModel.h
//  ICan
//
//  Created by shilei on 17/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface loadmodel : JSONModel

@property(nonatomic,strong)NSString *fileName;

@end


@interface ICN_loadHeadImageModel : JSONModel

@property(nonatomic,assign)NSInteger code;
@property(nonatomic,strong)NSString *info;
@property(nonatomic,strong)loadmodel *result;

@end
