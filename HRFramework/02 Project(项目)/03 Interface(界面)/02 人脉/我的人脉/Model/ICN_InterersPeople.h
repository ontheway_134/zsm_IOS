//
//  ICN_InterersPeople.h
//  ICan
//
//  Created by shilei on 17/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_InterersPeople : BaseOptionalModel

//authStatus = 0;
//companyName = hongruisibo;
//friendId = 268;
//memberId = 252;
//memberLogo = "/Public/Uploads/201701181234021713696.png";
//memberMajor = "<null>";
//memberMobile = 18002465327;
//memberNick = 6ge;
//memberPosition = android;
//memberSchool = "<null>";
//plusv = 0;
//status = 2;

@property(nonatomic,copy)NSString *memberLogo;   //头像
@property(nonatomic,copy)NSString *memberNick;  //用户昵称
@property(nonatomic,copy)NSString *companyName;  //公司名
@property(nonatomic,copy)NSString *memberPosition; // 专业
@property(nonatomic,copy)NSString *memberId; // 成员ID
@property(nonatomic,copy)NSString *content;

@property(nonatomic,copy)NSString *g_status;
@property(nonatomic,copy)NSString *groupAssessment;
@property(nonatomic,copy)NSString *groupId;
@property(nonatomic,copy)NSString *groupLogo;
@property(nonatomic,copy)NSString *groupName;
@property(nonatomic,copy)NSString *groupRole;
@property(nonatomic,copy)NSString *isopen;
@property(nonatomic,copy)NSString *joinDate;
@property(nonatomic,copy)NSString *joinType;
@property(nonatomic,copy)NSString *leaveDate;
@property(nonatomic,copy)NSString *memberCount;

@property(nonatomic,copy)NSString *memberMajor;
@property(nonatomic,copy)NSString *memberSchool;
@property(nonatomic,copy)NSString *number;
@property(nonatomic,copy)NSString *summary;



@property(nonatomic,copy)NSString *status;    //登录的状态的判断

@property(nonatomic,copy)NSString *plusv;    //判断是不是会员 1为会员 0不是

@property(nonatomic,copy)NSString *friendDelStatus;

//=== 解决bug字段
@property(nonatomic , copy)NSString *friendStatus; // 用户出现异常数据的时候判断用户好友状态




@end
