//
//  ICN_InterersPeople.m
//  ICan
//
//  Created by shilei on 17/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_InterersPeople.h"

@implementation ICN_InterersPeople

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"memberId"} ];
}

@end
