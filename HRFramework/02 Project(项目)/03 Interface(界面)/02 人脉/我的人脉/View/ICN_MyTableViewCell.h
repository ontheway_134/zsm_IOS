//
//  ICN_MyTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_MyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *MyImageView;
@property (weak, nonatomic) IBOutlet UILabel *MyLabel;

@end
