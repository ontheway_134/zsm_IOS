//
//  ICN_NewFridendsTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_NewFridendsTableViewCell.h"

@implementation ICN_NewFridendsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.oneHeadInamgView.layer.cornerRadius = 17.5;
    self.oneHeadInamgView.layer.masksToBounds = YES;
    self.twoImageView.layer.cornerRadius = 17.5;
    self.twoImageView.layer.masksToBounds = YES;
    self.threeImageview.layer.cornerRadius = 17.5;
    self.threeImageview.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
