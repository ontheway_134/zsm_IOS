//
//  ICN_InterestTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_InterestTableViewCell.h"

@implementation ICN_InterestTableViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headImageview.layer.cornerRadius = 17.5;
    self.headImageview.layer.masksToBounds = YES;
}


- (IBAction)AddClickBtn:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(ICN_BtnSelectedAddDelegate:and:)]) {
        [self.delegate ICN_BtnSelectedAddDelegate:sender and:self];
    }
}


//我的好友
-(void)setMymodel:(ICN_MyyFridentModel *)mymodel{
    _mymodel = mymodel;
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    //self.nickNameLabel.text=_model.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.companyName,_model.memberPosition];
    self.companyLabel.text=str;
}

//可能感兴趣的人
-(void)setModel:(ICN_InterersPeople *)model{
    _model=model;
    if ([_model.plusv isEqualToString:@"0"]) {
        self.memberImageView.hidden = YES;
    
    }else{
        self.memberImageView.hidden = NO;
    }
    
    if ([_model.status isEqualToString:@"1"]) {
        [self.addFridentBtn setTitle:@"等待验证" forState:UIControlStateNormal];
    }
    
    if ([_model.status isEqualToString:@"2"]) {
        [self.addFridentBtn setTitle:@"已添加" forState:UIControlStateNormal];
    }
    
    if ([_model.status isEqualToString:@"5"]) {
        self.addFridentBtn.hidden=YES;
    }
    
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_model.memberNick;
    if (_model.companyName == nil) {

         NSString *str=[NSString stringWithFormat:@"| %@",_model.memberPosition];
         self.companyLabel.text=str;
        
    }
    
    
#warning 这里根据是否已经工作是根据公司的状态去判断的，正常应该是根据某个字段判断具体显示哪个，这块还需要做修改
    if ([_model.companyName isEqualToString:@""] && _model.memberSchool !=nil) {
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.memberSchool,_model.memberMajor];
        self.companyLabel.text=str;
        return;
    }
    
    
    
    if (_model.memberPosition == nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | ",_model.companyName];
        self.companyLabel.text=str;
    }
    if (_model.memberPosition ==nil && _model.companyName ==nil){
        
        self.companyLabel.text=@"";
        
    }
    if (_model.memberPosition !=nil && _model.companyName !=nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.companyName,_model.memberPosition];
        self.companyLabel.text=str;
    }
  
    
}

//我的小组详情页面我的组员model
-(void)setGroupDdetialmodel:(ICN_GroupDetialModel *)groupDdetialmodel{
    _groupDdetialmodel=groupDdetialmodel;
    
    //判断是否是会员
    if ([_groupDdetialmodel.plusv isEqualToString:@"0"]) {
        self.memberImageView.hidden = YES;
        
    }else{
        self.memberImageView.hidden = NO;
    }
    //判断是否是好友 关系(1:未审核[申请者]  2:已审核   3:被拒绝  4:已删除  5:待审核[被申请者] 6:已拒绝 )[没关系则没有此数据]
#warning 这里的数据为了和前面的数据同意 只确定1的状态为等待验证
    self.addFridentBtn.hidden = NO;
    if ([groupDdetialmodel.friendStatus isEqualToString:@"2"]) {
        [self.addFridentBtn setTitle:@"已是好友关系" forState:UIControlStateNormal];
        
        self.addFridentBtn.userInteractionEnabled = NO;
    }
    if ([_groupDdetialmodel.friendStatus isEqualToString:@"3"]) {
        [self.addFridentBtn setTitle:@"被拒绝" forState:UIControlStateNormal];
        self.addFridentBtn.userInteractionEnabled = NO;
    }
    if ([_groupDdetialmodel.friendStatus isEqualToString:@"4"]) {
        [self.addFridentBtn setTitle:@"已删除" forState:UIControlStateNormal];
        self.addFridentBtn.userInteractionEnabled = NO;
    }
    if ([_groupDdetialmodel.friendStatus isEqualToString:@"1"]) {
        [self.addFridentBtn setTitle:@"等待审核" forState:UIControlStateNormal];
        self.addFridentBtn.userInteractionEnabled = NO;
    }
    if ([_groupDdetialmodel.friendStatus isEqualToString:@"6"]) {
        [self.addFridentBtn setTitle:@"已拒绝" forState:UIControlStateNormal];
        self.addFridentBtn.userInteractionEnabled = NO;
    }

    

    
    
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_groupDdetialmodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_groupDdetialmodel.memberNick;
    
    
    if (_groupDdetialmodel.companyName == nil) {
        
        NSString *str=[NSString stringWithFormat:@"| %@",_groupDdetialmodel.memberPosition];
        self.companyLabel.text=str;
        NSLog(@"大家啊");
    }
    
    
#warning 这里根据是否已经工作是根据公司的状态去判断的，正常应该是根据某个字段判断具体显示哪个，这块还需要做修改
    if ([_groupDdetialmodel.companyName isEqualToString:@""] && _groupDdetialmodel.memberSchool !=nil) {
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_groupDdetialmodel.memberSchool,_groupDdetialmodel.memberMajor];
        self.companyLabel.text=str;
        return;
    }
    
    
    
    if (_groupDdetialmodel.memberPosition == nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | ",_groupDdetialmodel.companyName];
        self.companyLabel.text=str;
    }
    if (_groupDdetialmodel.memberPosition ==nil && _groupDdetialmodel.companyName ==nil){
        
        self.companyLabel.text=@"";
        
    }
    if (_groupDdetialmodel.memberPosition !=nil && _groupDdetialmodel.companyName !=nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_groupDdetialmodel.companyName,_groupDdetialmodel.memberPosition];
        self.companyLabel.text=str;
    }
    
    

}

-(void)setCommonFridentModel:(ICN_CommonFriendsModel *)commonFridentModel{
    _commonFridentModel = commonFridentModel;
    [self.headImageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_commonFridentModel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = _commonFridentModel.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",_commonFridentModel.companyName,_commonFridentModel.memberPosition];
    self.companyLabel.text=str;

}

@end
