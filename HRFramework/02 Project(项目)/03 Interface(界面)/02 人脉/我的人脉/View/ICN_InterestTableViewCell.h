//
//  ICN_InterestTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_InterersPeople.h"
#import "ICN_MyyFridentModel.h"
#import "ICN_GroupDetialModel.h"
#import "ICN_CommonFriendsModel.h"

@class ICN_InterestTableViewCell;

@protocol ICN_BtnSelectedAddDelegate <NSObject>

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell;

@end

@interface ICN_InterestTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageview;   //头像
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;       //昵称
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;        //公司名
@property (weak, nonatomic) IBOutlet UIImageView *memberImageView;    //是否事会员

@property (weak, nonatomic) IBOutlet UIButton *addFridentBtn;      //添加按钮

@property(nonatomic,weak)id<ICN_BtnSelectedAddDelegate>delegate;

@property(nonatomic,strong)ICN_MyyFridentModel *mymodel;

@property(nonatomic,strong)ICN_InterersPeople *model;  //可能感兴趣的人的model



@property(nonatomic,strong)ICN_GroupDetialModel *groupDdetialmodel;

@property(nonatomic,strong)ICN_CommonFriendsModel *commonFridentModel;   //共同好友的model

@end
