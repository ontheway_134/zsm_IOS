//
//  ICN_CommonFridentViewController.m
//  ICan
//
//  Created by shilei on 17/2/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CommonFridentViewController.h"
#import "ICN_InterestTableViewCell.h"
#import "HRNetworkingManager.h"
#import "BaseOptionalModel.h"
#import "ICN_CommonFriendsModel.h"

@interface ICN_CommonFridentViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *commonFridentTableViews;
@property(nonatomic,strong)NSMutableArray *commonFridentArr;


@property(nonatomic,assign)NSInteger page;

@end

@implementation ICN_CommonFridentViewController

#pragma mark - --- 网络请求 ---

#warning 共同好友没有添加分页，所以也不需要增加刷新，如果后期需要添加，只需要放开注释的部分

-(void)httpNetWoringpage:(NSInteger)page{
    
    NSDictionary *dic=@{@"sideId":self.commonFridentId,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"page":[NSString stringWithFormat:@"%ld",(long)page],@"pagenum":@10};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserComFriends params:dic success:^(id result) {
        
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
//            if (self.page == 1) {
//                [self.commonFridentArr removeAllObjects];
//            }
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_CommonFriendsModel *model=[[ICN_CommonFriendsModel alloc]initWithDictionary:dic error:nil];
                [self.commonFridentArr addObject:model];
            }
        }
//        [self.commonFridentTableViews.mj_footer endRefreshing];
//        [self.commonFridentTableViews.mj_header endRefreshing];
        [self.commonFridentTableViews reloadData];
        
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
//        [self.commonFridentTableViews.mj_footer endRefreshing];
//        [self.commonFridentTableViews.mj_header endRefreshing];
    }];

}


#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.commonFridentTableViews registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.commonFridentTableViews.delegate =self;
    self.commonFridentTableViews.dataSource = self;
    self.commonFridentTableViews.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self httpNetWoringpage:nil];
//    [self frefsh];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.commonFridentArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    cell.addFridentBtn.hidden = YES;
    cell.commonFridentModel = self.commonFridentArr[indexPath.row];
    
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53;
}

#pragma mark - --- IBActions ---


- (IBAction)backActions:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - --- 自定义的方法 ---

//刷新
-(void)frefsh{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        self.page = 1;
        [self httpNetWoringpage:self.page];
        
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    
    
    self.commonFridentTableViews.mj_header = header;
    self.commonFridentTableViews.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        
        self.page+=1;
        [self httpNetWoringpage:self.page];
        
    }];
    [self.commonFridentTableViews.mj_header beginRefreshing];
    
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)commonFridentArr{
    if (!_commonFridentArr) {
        _commonFridentArr=[NSMutableArray array];
    }
    return _commonFridentArr;

}

@end
