//
//  ICN_ApplyCommitRequestViewController.h
//  ICan
//
//  Created by shilei on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_ApplyCommitRequestViewController : BaseViewController

@property(nonatomic,strong)NSString *groupId;

@property(nonatomic,strong)NSString *gtoupname;


@property (weak, nonatomic) IBOutlet UILabel *applyLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end
