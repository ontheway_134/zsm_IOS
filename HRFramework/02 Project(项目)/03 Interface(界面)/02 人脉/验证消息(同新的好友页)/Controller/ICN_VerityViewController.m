//
//  ICN_VerityViewController.m
//  ICan
//
//  Created by shilei on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_VerityViewController.h"
#import "ICN_NewFridentTableViewCell.h"
#import "BaseOptionalModel.h"
#import "ICN_vetityModelss.h"

@interface ICN_VerityViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *verifyTableView;

@property(nonatomic,strong)NSMutableArray *NewFridentDataArrss;


@property(nonatomic,strong)ICN_NewFridentTableViewCell *cell;

@property(nonatomic,strong)UIImageView *ima;
@property(nonatomic,strong)UILabel *la;

@end

@implementation ICN_VerityViewController

#pragma mark - --- 网络请求 ---

-(void)httpverity{

    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"groupId":self.gropuid};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_APPLYGROUPSS params:dic success:^(id result) {
        BaseOptionalModel *base=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.NewFridentDataArrss removeAllObjects];
        if (base.code == 0) {
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_vetityModelss *model = [[ICN_vetityModelss alloc] initWithDictionary:dic error:nil];
                [self.NewFridentDataArrss addObject:model];
               
            }
            
        }else{
           [self customMethod];
        }
         [self.verifyTableView reloadData];
        
        [self.verifyTableView.mj_header endRefreshing];
   
    } failure:^(NSDictionary *errorInfo) {
         [self.verifyTableView.mj_header endRefreshing];
    }];

}


//好友审核 1通过 2拒绝   memberid（申请者id）
-(void)httpAgreeandRefuseAction:(NSString *)action andFridendid:(NSString *)fridentid{
    
    NSDictionary *dic=@{@"groupId":self.gropuid,@"status":action,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"memberId":fridentid};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_CHECKGROUP params:dic success:^(id result) {
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"发送成功"];
            [self refresh];
        }
                
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}


#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.verifyTableView registerNib:[UINib nibWithNibName:@"ICN_NewFridentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NewFridentTableViewCell"];
    self.verifyTableView.delegate=self;
    self.verifyTableView.dataSource=self;
    self.verifyTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    [self refresh];
    

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.NewFridentDataArrss.count;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_NewFridentTableViewCell"];
    if (self.cell==nil) {
        self.cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_NewFridentTableViewCell" owner:self options:nil] lastObject];
    }
    self.cell.vetitymodel=self.NewFridentDataArrss[indexPath.row];
    
    [self.cell returnLocationPhoto:^(ICN_NewFridentTableViewCell *cell) {
        [self httpAgreeandRefuseAction:@"1" andFridendid:cell.vetitymodel.memberId];
    }];
    
   [self.cell returnTakingPhoto:^(ICN_NewFridentTableViewCell *cell) {
       [self httpAgreeandRefuseAction:@"2" andFridendid:cell.vetitymodel.memberId];
   }];
    
    return self.cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 174;
}


#pragma mark - --- 自定义方法 ---

-(void)customMethod{
    
    if (self.NewFridentDataArrss.count == 0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无好友申请信息";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
}

-(void)refresh{
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self httpverity];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.verifyTableView.mj_header = header;
    [self.verifyTableView.mj_header beginRefreshing];
    
}


#pragma mark - --- 懒加载 ---

-(NSMutableArray *)NewFridentDataArrss{
    if (!_NewFridentDataArrss) {
        _NewFridentDataArrss=[NSMutableArray array];
    }
    return _NewFridentDataArrss;
}

@end
