//
//  ICN_SeekPeopleViewController.m
//  ICan
//
//  Created by shilei on 17/2/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SeekPeopleViewController.h"
#import "ICN_InterestTableViewCell.h"
#import "ICN_UserHomePagerVC.h"

@interface ICN_SeekPeopleViewController ()<UITableViewDelegate,UITableViewDataSource,ICN_BtnSelectedAddDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchTextFiled;
@property (weak, nonatomic) IBOutlet UITableView *SeekSearchTableView;
@property(nonatomic,strong)NSMutableArray *searcherArr;

@property(nonatomic,strong)UIImageView *ima;
@property(nonatomic,strong)UILabel *la;

@end

@implementation ICN_SeekPeopleViewController

//搜索的网络请求
-(void)httpseekFrident{
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"cont":self.searchTextFiled.text,@"all":@1,@"page":@1,@"pagenum":@10};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_SEEKFRIDENT params:dic success:^(id result) {
        [self endRefreshWithTableView:self.SeekSearchTableView];
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.searcherArr removeAllObjects];
        if (basemodel.code == 0) {
            [self.ima removeFromSuperview];
            [self.la removeFromSuperview];
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:dic error:nil];
#warning 具体能够搜索出来哪些好友显示在这上面，根据status判断
                //                if (model.status.integerValue == 1 || model.status.integerValue ==4) {
                [self.searcherArr addObject:model];
                //                }
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            if ([basemodel.info isEqualToString:@"数据为空"]) {
                [self customMethod];
            }
        }
        [self.SeekSearchTableView reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"搜索失败");
        [self endRefreshWithTableView:self.SeekSearchTableView];
    }];
    
}




#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.SeekSearchTableView registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.SeekSearchTableView.delegate = self;
    self.SeekSearchTableView.dataSource = self;
    self.SeekSearchTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self httpseekFrident];
    [self.SeekSearchTableView.mj_header beginRefreshing];
    [self configTableViewRefreshHeaderFooterView];

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if (self.searchContent) {
        self.searchTextFiled.text = self.searchContent;
    }
    [self httpseekFrident];
//    [self.SeekSearchTableView.mj_header beginRefreshing];
//    [self configTableViewRefreshHeaderFooterView];


}

#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    if ([cell.model.status isEqualToString:@"1"]) {
        return;
    }
    
    if ([cell.model.status isEqualToString:@"3"]) {
        return;
    }
    if ([cell.model.status isEqualToString:@"5"]) {
        return;
    }
    
    NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.model.memberId};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
        
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.searcherArr removeAllObjects];
        if (baseModel.code == 0) {
            
            [btn setTitle:@"等待验证" forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
            [self httpseekFrident];
            
        }else{
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}


#pragma mark - --- UITableViewDataSource ---

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.searcherArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    cell.delegate =self;
    cell.model = self.searcherArr[indexPath.row];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
    ICN_InterestTableViewCell *cell= (ICN_InterestTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    userhome.memberId=cell.model.memberId;
    [self.navigationController pushViewController:userhome animated:YES];
}


#pragma mark - --- 自定义方法 ---

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self httpseekFrident];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.SeekSearchTableView.mj_header = header;
    [self.SeekSearchTableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


//添加好友无结果时背景显示无结果的背景图片
-(void)customMethod{
    
    if (self.searcherArr.count == 0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无搜索结果";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
    
}



#pragma mark - --- IBActions ---


- (IBAction)searchBtn:(UIButton *)sender {
    //搜索的点击事件
    [self.view endEditing:YES];
    [self httpseekFrident];
    [self.SeekSearchTableView.mj_header beginRefreshing];
    [self configTableViewRefreshHeaderFooterView];
    
}

- (IBAction)backBtnActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSMutableArray *)searcherArr{
    if (!_searcherArr) {
        _searcherArr=[NSMutableArray array];
    }
    return _searcherArr;
}

@end
