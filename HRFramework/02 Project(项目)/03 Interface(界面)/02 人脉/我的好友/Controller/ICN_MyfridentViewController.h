//
//  ICN_MyfridentViewController.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_MyfridentViewController : BaseViewController

@property(nonatomic,strong)NSString *commonFridentId;     //共同好友的id
@property (nonatomic , copy)NSString *searchContent; // 搜索内容


@end
