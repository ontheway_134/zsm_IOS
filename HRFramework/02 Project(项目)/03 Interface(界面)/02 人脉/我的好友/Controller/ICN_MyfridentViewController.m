//
//  ICN_MyfridentViewController.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyfridentViewController.h"
#import "ICN_AddFridentViewController.h"
#import "ICN_PhoneFridenTableViewCell.h"
#import "ICN_InterestTableViewCell.h"
#import "ICN_SelsectVIew.h"
#import "ICN_NewContactViewController.h"
#import "ICN_NewFrident.h"
#import "SortAlphabetically.h"
#import "ICN_UserHomePagerVC.h"
#import "ICN_ShareManager.h"

@interface ICN_MyfridentViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myFridentTableVIew;

@property(nonatomic,strong)NSMutableArray *FridentDataArr;
@property (nonatomic, strong) NSMutableArray *dataSource; /**<最初的数据*/
@property (nonatomic, strong) NSMutableArray *indexArray; /**< 索引数据源*/
@property (nonatomic, strong) NSMutableDictionary *sortDict; /**< 排序后的数据源*/

@property(nonatomic,strong)NSMutableArray *searchData;    //搜索后的数组；
@property (nonatomic , copy)NSString *selectedFriendId;


@property (weak, nonatomic) IBOutlet UITextField *SeekTextfiled;
@property(nonatomic,assign)BOOL isSeek;   //判断是否是搜索的

@property(nonatomic,strong)UIImageView *ima;
@property(nonatomic,strong)UILabel *la;
@property (nonatomic , assign)NSInteger deleteIndexSection;
@property(nonatomic,strong) ICN_PhoneFridenTableViewCell *cell;

@end

@implementation ICN_MyfridentViewController

#pragma mark - --- 网络请求 ---

//搜索的网络请求
-(void)httpseekFrident{
    NSDictionary *dic=@{@"TOKEN":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"cont":self.SeekTextfiled.text,@"all":@0,@"page":@1,@"pagenum":@"10"};
    // 在获取到搜索数据成功并有值得时候将搜索的标识isSeek设置为YES
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_SEEKFRIDENT params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            // 在有搜索内容的时候刷新数据显示的是搜索数组的内容 否则显示的是好友数组的内容 并根据搜索数组的内容重新建立索引
            [self.searchData removeAllObjects];
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:dic error:nil];
                [self.searchData addObject:model];
            }
            
            // 获取到搜索内容数组后根据是否有搜索内容决定下一部内容
            if (self.searchData.count > 0) {
                [self.dataSource removeAllObjects];
                for (ICN_InterersPeople *peopleModel in self.searchData) {
                    [self.dataSource addObject:peopleModel.memberNick];
                }
                [self configIndexArrListAfterDataSourceUpdated];
                // 在获取到新的索引之后根据索引刷新数据
                self.isSeek = YES;
                [self.myFridentTableVIew reloadData];
            }else{
                // 搜索返回中没有值得时候不刷新数据并弹出HUD
                self.isSeek = NO;
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未检索到好友"];
            }
            
            
        }else{
            self.isSeek = NO;
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        self.isSeek = NO;
        NSLog(@"搜索失败");
    }];
    
}

//删除好友
-(void)httpDelegateFridentWithMemberId:(NSString *)memberId{
    
    NSDictionary *dic=@{@"token":[[NSUserDefaults standardUserDefaults] valueForKey:HR_CurrentUserToken],@"friendId":memberId};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_DELETEFRIDENT params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
            if (self.selectedFriendId) {
                ICN_MyyFridentModel *deleteModel;
                for (ICN_MyyFridentModel *model in self.FridentDataArr) {
                    if ([model.friendId isEqualToString:self.selectedFriendId]) {
                        deleteModel = model;
                    }
                }
                if (deleteModel) {
                    // 如果存在删除Model的话 将删除Model的nick在datasouce中移除并重置索引
                    [self.dataSource removeObject:deleteModel.memberNick];
                    [self.FridentDataArr removeObject:deleteModel];
                    self.sortDict = [NSMutableDictionary dictionary];
                    self.indexArray = [NSMutableArray arrayWithObjects:@" ", nil];
                    [self configIndexArrListAfterDataSourceUpdated];
                    [self.myFridentTableVIew reloadData];
                }
            }
            
            [self httpNetWoringPage];

        }
        [self.myFridentTableVIew reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"删除好友失败");
    }];
    
}

//列表信息,这个列表信息不应该存在分页，和微信相同，
-(void)httpNetWoringPage {
    
    NSDictionary *dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"status":@2};
    
    [[[HRNetworkingManager alloc] init ] POST_PATH:PATH_NEWFRIEND params:dic success:^(id result) {
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [self.FridentDataArr removeAllObjects];
            [self.dataSource removeAllObjects];
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_MyyFridentModel *model = [[ICN_MyyFridentModel alloc] initWithDictionary:dic error:nil];
                
                if (model.status.integerValue == 2) {
                    [self.FridentDataArr addObject:model];
                }
                BOOL ableToAdd = YES;
                if (_dataSource) {
                    for (NSString *nickName in self.dataSource) {
                        if ([nickName isEqualToString:model.memberNick]) {
                            ableToAdd = NO;
                        }
                    }
                    if (ableToAdd) {
                        [self.dataSource addObject:model.memberNick];
                    }
                }else{
                    [self.dataSource addObject:model.memberNick];
                }
                
                self.indexArray = [[SortAlphabetically shareSortAlphabetically] fetchFirstLetterFromArray:self.dataSource];
                
                [self.indexArray insertObject:@" " atIndex:0];
                self.sortDict = [[SortAlphabetically shareSortAlphabetically] sortAlphabeticallyWithDataArray:self.dataSource propertyName:nil];
                
                
            }
            if (self.FridentDataArr.count == 0) {
                // 当列表不存在数据的时候添加显示推荐给，，，好友的索引
                self.indexArray = [NSMutableArray arrayWithObjects:@" ", nil];
            }
        }
        
        else{
            if (self.FridentDataArr) {
                [self.FridentDataArr removeAllObjects];
            }else{
                self.FridentDataArr = [NSMutableArray array];
            }
            if (self.FridentDataArr.count == 0) {
                // 当列表不存在数据的时候添加显示推荐给，，，好友的索引
                self.indexArray = [NSMutableArray arrayWithObjects:@" ", nil];
            }
            self.cell.hidden = NO;
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"暂无好友"];

        }
        [self.myFridentTableVIew reloadData];
        [self.myFridentTableVIew.mj_header endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {
           [self.myFridentTableVIew.mj_header endRefreshing];
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.deleteIndexSection = -1;
    [self setHiddenDefaultNavBar:YES];
    [self.myFridentTableVIew registerNib:[UINib nibWithNibName:@"ICN_PhoneFridenTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_PhoneFridenTableViewCell"];
    self.myFridentTableVIew.delegate=self;
    self.myFridentTableVIew.dataSource=self;
    self.myFridentTableVIew.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myFridentTableVIew.sectionIndexBackgroundColor = [UIColor clearColor];
    [self refresh];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [self refresh];
}

#pragma mark - --- 私有方法 ---

// 在昵称数据源更新后更新索引
- (void)configIndexArrListAfterDataSourceUpdated{
    if (self.dataSource.count == 0) {
        return ;
    }
    self.indexArray = [[SortAlphabetically shareSortAlphabetically] fetchFirstLetterFromArray:self.dataSource];
    [self.indexArray insertObject:@" " atIndex:0];
    self.sortDict = [[SortAlphabetically shareSortAlphabetically] sortAlphabeticallyWithDataArray:self.dataSource propertyName:nil];

}


#pragma mark - --- 自定义方法 ---

-(void)refresh{
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self httpNetWoringPage];

    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.myFridentTableVIew.mj_header = header;

    [self.myFridentTableVIew.mj_header beginRefreshing];
    
}



#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            self.cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_PhoneFridenTableViewCell"];
            if (self.cell==nil) {
                self.cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_PhoneFridenTableViewCell" owner:self options:nil] lastObject];
            }
            
            switch (indexPath.row) {
                case 0:
                    self.cell.phoneIMageVIew.image=[UIImage imageNamed:@"手机通讯录好友"];
                    self.cell.FridendLabel.text=@"手机通讯录好友";
                    break;
                case 1:
                    self.cell.phoneIMageVIew.image=[UIImage imageNamed:@"QQ好友"];
                    self.cell.FridendLabel.text=@"QQ好友";
                    break;
                case 2:
                    self.cell.phoneIMageVIew.image=[UIImage imageNamed:@"微信好友"];
                    self.cell.FridendLabel.text=@"微信好友";
                    break;
                case 3:
                    self.cell.phoneIMageVIew.image=[UIImage imageNamed:@"微博"];
                    self.cell.FridendLabel.text=@"新浪微博好友";
                    break;
            }
            if (self.isSeek) {
                self.cell.hidden = YES;
            }
            
            return self.cell;
        }
            
            break;
            
        default:{
            ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
            if (cell==nil) {
                cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
            }
            cell.addFridentBtn.hidden = YES;
            if (self.isSeek) {
                // 在检索到是搜索的数据的时候根据索引字段获取到指定的Model
                NSString *nickName = [self.sortDict valueForKey:self.indexArray[indexPath.section]][indexPath.row];
                for (ICN_InterersPeople *model in self.searchData) {
                    if ([model.memberNick isEqualToString:nickName]) {
                        cell.model = model;
                    }
                }
                
            }else{
                
                ICN_MyyFridentModel *friendModel;
                NSString *nickName = [self.sortDict valueForKey:self.indexArray[indexPath.section]][indexPath.row];
                for (ICN_MyyFridentModel *model in self.FridentDataArr) {
                    if ([model.memberNick isEqualToString:nickName]) {
                        friendModel = model;
                    }
                }
                
                cell.model = friendModel;
                cell.nickNameLabel.text = self.sortDict[self.indexArray[indexPath.section]][indexPath.row];
               
            }
            
            return cell;
            
        }
            break;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 0.01f;
            break;
            
        default:
            return 17;
            break;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:{
            
            switch (indexPath.row) {
                case 0:{
                    ICN_NewContactViewController *contact=[[ICN_NewContactViewController alloc]init];
                    [self.navigationController pushViewController:contact animated:YES];
                    
                }
                    
                    break;
                case 1:{
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:@"http://1ican.com/" andTitle:@"欢迎使用i行" andImage:nil Detial:@"欢迎使用i行"];
                }
                    break;
                case 2:{
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:@"http://1ican.com/" andTitle:@"欢迎使用i行" andImage:nil Detial:@"欢迎使用i行"];
                }
                    break;
                case 3:{
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:@"http://1ican.com/" andTitle:@"欢迎使用i行" andImage:nil Detial:@"欢迎使用i行"];
                }
                    break;
            }
            
            
        }
            break;
            
        default:{
            
            ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
            NSString *nickName = [self.sortDict valueForKey:self.indexArray[indexPath.section]][indexPath.row];
            for (ICN_MyyFridentModel *model in self.FridentDataArr) {
                if ([model.memberNick isEqualToString:nickName]) {
                    userhome.memberId = model.friendId;
                }
            }
            if(userhome.memberId == nil){
                userhome.memberId = @"3";
            }
            [self.navigationController pushViewController:userhome animated:YES];
            
        }
            break;
    }
    
}

//索引和头标题
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return self.indexArray[section];
}

//右侧索引
- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.indexArray;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            if (self.isSeek) {
                return 0;
            }else{
                return 50;
            }
        }
            
            break;
            
        default:
            return 53;
            break;
    }
}
//返回的组数为索引加1此次没有+是因为上面插入了一个空的索引
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.indexArray.count;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 4;
            break;
            
        default:{
            
            if ([self.sortDict[self.indexArray[section]] count]==0) {
                return 0;
            }else{
                return [self.sortDict[self.indexArray[section]] count];
            }
            
        }
            
            break;
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchData != nil && self.searchData.count > 0) {
        return NO;
    }
    switch (indexPath.section) {
        case 0:
            return NO;
            break;
            
        default:
            return YES;
            break;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //蒙板的弹出
        ICN_SelsectVIew *selsect=[[[NSBundle mainBundle]loadNibNamed:@"ICN_SelsectVIew" owner:self options:nil]firstObject];
        
        ICN_MyyFridentModel *friendModel;
        NSString *nickName = [self.sortDict valueForKey:self.indexArray[indexPath.section]][indexPath.row];
        //取出两层的数据
        for (ICN_MyyFridentModel *model in self.FridentDataArr) {
            if ([model.memberNick isEqualToString:nickName]) {
                friendModel = model;
            }
        }
        selsect.friendId = friendModel.friendId;
        [[UIApplication sharedApplication].keyWindow addSubview:selsect];
        [selsect mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
        }];
        
        [selsect confirmClickBlock:^(NSString *frinedId) {
            self.selectedFriendId = frinedId;
            self.deleteIndexSection = indexPath.section;
            [self httpDelegateFridentWithMemberId:frinedId];
            [selsect removeFromSuperview];
        }];
        
        [selsect cancelClickBlock:^{
            [selsect removeFromSuperview];
        }];
    }
    
}

-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}


#pragma mark - --- IBActions ---

- (IBAction)ClickBtnActions:(UIButton *)sender {
    switch ([sender tag]) {
        case 0:{
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
            
        case 1:{
            
            ICN_AddFridentViewController *add=[[ICN_AddFridentViewController alloc]init];
            [self.navigationController pushViewController:add animated:YES];
        }
            break;
    }
}

//搜索的点击事件
- (IBAction)searchBtnActions:(id)sender {
    [self.view endEditing:YES];
    [self httpseekFrident];
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)FridentDataArr{
    if (!_FridentDataArr) {
        _FridentDataArr=[NSMutableArray array];
    }
    return _FridentDataArr;
    
}

- (NSMutableArray *)dataSource
{
    if(!_dataSource){
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (NSMutableArray *)indexArray
{
    if(!_indexArray){
        _indexArray = [[NSMutableArray alloc] init];
    }
    return _indexArray;
}

- (NSMutableDictionary *)sortDict
{
    if(!_sortDict){
        _sortDict = [[NSMutableDictionary alloc] init];
    }
    return _sortDict;
}

-(NSMutableArray *)searchData{
    if (!_searchData) {
        _searchData=[NSMutableArray array];
    }
    return _searchData;
}

@end
