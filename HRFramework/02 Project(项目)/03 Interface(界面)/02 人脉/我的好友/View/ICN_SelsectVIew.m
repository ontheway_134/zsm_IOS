//
//  ICN_SelsectVIew.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SelsectVIew.h"


@interface ICN_SelsectVIew()
@property (weak, nonatomic) IBOutlet UIView *minView;

@end


@implementation ICN_SelsectVIew

-(void)awakeFromNib{
    [super awakeFromNib];
    self.minView.layer.cornerRadius = 5.0;
    self.minView.layer.masksToBounds = YES;
}
- (void)confirmClickBlock:(ConfirmBlock)block{
    self.confirmBlock = block;
}
- (void)cancelClickBlock:(CancelBlock)block{
    self.cancelBlock = block;
}

#pragma mark - --- IBActions ---

- (IBAction)removeView:(UITapGestureRecognizer *)sender {
    [self removeFromSuperview];
}
- (IBAction)confireAction:(id)sender {
    if (self.confirmBlock) {
        self.confirmBlock(self.friendId);
    }
  
}

- (IBAction)cancelAction:(id)sender {
    if (self.cancelBlock) {
        self.cancelBlock(sender);
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
