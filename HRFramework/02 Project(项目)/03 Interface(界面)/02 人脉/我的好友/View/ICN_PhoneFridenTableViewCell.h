//
//  ICN_PhoneFridenTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_PhoneFridenTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *phoneIMageVIew;
@property (weak, nonatomic) IBOutlet UILabel *FridendLabel;

@end
