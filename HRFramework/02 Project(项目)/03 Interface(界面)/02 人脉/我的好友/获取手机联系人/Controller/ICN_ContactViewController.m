//
//  ICN_ContactViewController.m
//  ICan
//
//  Created by shilei on 17/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ContactViewController.h"
#import "ICN_ConcatTableViewCell.h"
#import "PPPersonModel.h"
#import "PPGetAddressBook.h"


@interface ICN_ContactViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *ContactTableView;
@property (nonatomic, copy) NSDictionary *contactPeopleDict;
@property (nonatomic, copy) NSArray *keys;

@property (weak, nonatomic) IBOutlet UITextField *searcherTextfiled;

@end

@implementation ICN_ContactViewController

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES];
    [self.ContactTableView registerNib:[UINib nibWithNibName:@"ICN_ConcatTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ConcatTableViewCell"];
    self.ContactTableView.delegate= self;
    self.ContactTableView.dataSource = self;
    [PPGetAddressBook getOrderAddressBook:^(NSDictionary<NSString *,NSArray *> *addressBookDict, NSArray *nameKeys) {
        
        
        //装着所有联系人的字典
        self.contactPeopleDict = addressBookDict;
        //联系人分组按拼音分组的Key值
        self.keys = nameKeys;
        
        [self.ContactTableView reloadData];
    } authorizationFailure:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"请在iPhone的“设置-隐私-通讯录”选项中，允许PPAddressBook访问您的通讯录"
                                                       delegate:nil
                                              cancelButtonTitle:@"知道了"
                                              otherButtonTitles:nil];
        [alert show];
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *key = _keys[section];
    return [_contactPeopleDict[key] count];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return _keys[section];
}

//右侧的索引
- (NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return _keys;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_ConcatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_ConcatTableViewCell"];
    if (cell==nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ICN_ConcatTableViewCell" owner:self options:nil] lastObject];
    }
    [cell.ContactBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    NSString *key = _keys[indexPath.section];
    PPPersonModel *people = [_contactPeopleDict[key] objectAtIndex:indexPath.row];
    cell.cantactLabel.text=people.name;
    
    return cell;
}


#pragma mark - --- IBActions ---

- (IBAction)BackBtn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)click{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能还未开通"];
}

- (IBAction)searchBtn:(id)sender {
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"此功能尚未开通"];
    [self.view endEditing:YES];
}





@end
