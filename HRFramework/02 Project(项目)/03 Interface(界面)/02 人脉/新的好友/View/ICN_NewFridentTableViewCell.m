//
//  ICN_NewFridentTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_NewFridentTableViewCell.h"

#define AgreeBGColor RGB0X(0x009dff)  //等待验证也为这个颜色
#define DisAgreeBGColor RGB0X(0x7e94a2)

@interface ICN_NewFridentTableViewCell ()

@property (nonatomic , strong)UILabel *friendStatusLabel; // 好友状态按钮
@property (nonatomic , copy)NSString *currentStatus; // 当前Model当前Cell的status


@end

@implementation ICN_NewFridentTableViewCell

- (UILabel *)friendStatusLabel{
    if (_friendStatusLabel == nil) {
        _friendStatusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [self.contentView addSubview:_friendStatusLabel];
        _friendStatusLabel.textAlignment = NSTextAlignmentCenter;
        _friendStatusLabel.hidden = YES;
        _friendStatusLabel.font = [UIFont systemFontOfSize:11.0];
        _friendStatusLabel.userInteractionEnabled = YES;
    }
    return _friendStatusLabel;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    self.headImageView.layer.cornerRadius=17.5;
    self.headImageView.layer.masksToBounds = YES;
}


-(void)returnLocationPhoto:(ReturnLocationPhoto)block{
    self.locationPhoto=block;
}

-(void)returnTakingPhoto:(ReturnTakingPhoto)block{
    self.returnTakingPhoto= block;
}
- (IBAction)locationBtnAction:(UIButton *)sender {
    if (self.locationPhoto) {
        self.locationPhoto(self);
    }
    
}
- (IBAction)takingBtnAction:(UIButton *)sender {
    if (self.returnTakingPhoto) {
        self.returnTakingPhoto(self);
    }
}


//新的好友的model，避免共用model
-(void)setModel:(ICN_NewFrident *)model{

    _model=model;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = _model.memberNick;
    NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.companyName,_model.memberPosition];
    self.comPanyLabel.text=str;
    self.currentStatus = model.status;
    [self setNeedsLayout];
    if (_model.companyName == nil) {
        
        NSString *str=[NSString stringWithFormat:@"| %@",_model.memberPosition];
        self.comPanyLabel.text=str;
        
    }
    
    if (_model.memberPosition == nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | ",_model.companyName];
        self.comPanyLabel.text=str;
    }
    if (_model.memberPosition ==nil && _model.companyName ==nil){
        
        self.comPanyLabel.text=@"";
        
    }
    if (_model.memberPosition !=nil && _model.companyName !=nil) {
        
        NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.companyName,_model.memberPosition];
        self.comPanyLabel.text=str;
    }


}



-(void)setApplymodel:(ICN_ApplyRecodeModels *)applymodel{
    _applymodel = applymodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_applymodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_applymodel.groupName;
    self.introFridentLabel.text=_applymodel.summary;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_applymodel.memberCount];
    //self.currentStatus = _applymodel.status;
    

}

-(void)setMygroupmodel:(ICN_MyGroupModel *)mygroupmodel{
    _mygroupmodel = mygroupmodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_mygroupmodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_mygroupmodel.groupName;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_mygroupmodel.number];
    self.introFridentLabel.text =_mygroupmodel.summary;
    self.currentStatus = _mygroupmodel.status;
    
}

-(void)setInteresModel:(ICN_InteresMyGroupModel *)InteresModel{
    _InteresModel = InteresModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_InteresModel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_InteresModel.groupName;
    //NSString *str=[NSString stringWithFormat:@"%@ | %@",_InteresModel.number,_InteresModel.summary];
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_InteresModel.number];
    self.introFridentLabel.text=_InteresModel.summary;
   // self.currentStatus = _InteresModel.status;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    if (self.currentStatus) {
        [self changeFriendStatusLabelWithStatus:self.currentStatus];
    }
}

#pragma mark --- 私有方法 ---

- (void)changeFriendStatusLabelWithStatus:(NSString *)status{
//    1:未审核(memberId为申请者)  2:已审核   3:被拒绝  4:已删除  5:待审核(memberId为被申请者) 6:已拒绝
    self.friendStatusLabel.hidden = NO;
    [self.friendStatusLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.size.mas_equalTo(CGSizeMake(57, 20));
        make.centerY.equalTo(_comPanyLabel);
    }];
    switch (status.integerValue) {
        case 2:{
            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"已同意"];
            break;
        }
        case 6:{
            [self changeFriendsStatusLabelWithColor:DisAgreeBGColor Text:@"已拒绝"];
            break;
        }
//        case 5:{
//            [self changeFriendsStatusLabelWithColor:AgreeBGColor Text:@"等待验证"];
//            break;
//        }
        case 0:{
            self.TureBtn.hidden = YES;
            self.FalseBtn.hidden = YES;
            break;
        }
        default:{
            // 默认处理
            self.TureBtn.hidden = NO;
            self.FalseBtn.hidden = NO;
            self.friendStatusLabel.hidden = YES;
            break;
        }
    }
    self.currentStatus = nil;
}

- (void)setSearchModel:(ICN_DynGrooupListModel *)searchModel{
    _searchModel = searchModel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_searchModel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text = _searchModel.groupName;
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_searchModel.number];
    
    self.introFridentLabel.text = _searchModel.summary;

    [self setNeedsLayout];
    
}


- (void)changeFriendsStatusLabelWithColor:(UIColor *)color Text:(NSString *)text{
    // 清空之前的选项按钮
    self.TureBtn.hidden = YES;
    self.FalseBtn.hidden = YES;
    
    self.friendStatusLabel.text = text;
    self.friendStatusLabel.textColor = color;
    self.friendStatusLabel.layer.borderColor = color.CGColor;
    self.friendStatusLabel.layer.borderWidth = 1.0;
    self.friendStatusLabel.layer.cornerRadius = 2.0;
}

//我的小组加入新加入的组员的验证消息
-(void)setVetitymodel:(ICN_vetityModelss *)vetitymodel{
    _vetitymodel= vetitymodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_vetitymodel.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_vetitymodel.memberNick;
    NSString * headerData = _vetitymodel.content;
    headerData = [headerData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; //去除掉首尾的空白字符和换行字符
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\r" withString:@" "];
    headerData = [headerData stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
   
    self.comPanyLabel.text=headerData;
    self.currentStatus = _searchModel.status;

    self.introFridentLabel.text = @"申请加入你的小组";
    
    
//    if (_model.companyName == nil) {
//        
//        NSString *str=[NSString stringWithFormat:@"| %@",_model.memberPosition];
//        self.comPanyLabel.text=str;
//        
//    }
//    
//    if (_model.memberPosition == nil) {
//        
//        NSString *str=[NSString stringWithFormat:@"%@ | ",_model.companyName];
//        self.comPanyLabel.text=str;
//    }
//    if (_model.memberPosition ==nil && _model.companyName ==nil){
//        
//        self.comPanyLabel.text=@"";
//        
//    }
//    if (_model.memberPosition !=nil && _model.companyName !=nil) {
//        
//        NSString *str=[NSString stringWithFormat:@"%@ | %@",_model.companyName,_model.memberPosition];
//        self.comPanyLabel.text=str;
//    }

}

//参加的小组的小组的model

-(void)setJoinmodel:(ICN_JoinGroupModel *)joinmodel{
    _joinmodel = joinmodel;
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_joinmodel.groupLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.nickNameLabel.text=_joinmodel.groupName;
    
    
    self.comPanyLabel.text=[NSString stringWithFormat:@"%@人",_joinmodel.number];
  
    
    self.introFridentLabel.text =_joinmodel.summary;

}


@end
