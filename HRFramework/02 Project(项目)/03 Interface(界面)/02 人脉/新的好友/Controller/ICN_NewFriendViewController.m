//
//  ICN_NewFriendViewController.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_NewFriendViewController.h"
#import "ICN_NewFridentTableViewCell.h"

#import "ICN_NewFrident.h"  //这是个model取名字溜号了
#import "BaseOptionalModel.h"

@interface ICN_NewFriendViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *newfriendsTableView;
@property(nonatomic,strong)ICN_NewFridentTableViewCell *cell;
@property(nonatomic,strong)NSMutableArray *NewFridentDataArr;
@property(nonatomic,assign)NSInteger page;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property(nonatomic,strong)UIImageView *ima;
@property(nonatomic,strong)UILabel *la;

@end

@implementation ICN_NewFriendViewController

#pragma mark - --- 网络请求 ---
//列表信息
-(void)httpNetWoringPage:(NSInteger)page{
    
    NSDictionary *dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"status":@5,@"page" :[NSString stringWithFormat:@"%ld",(long)page], @"pagenum" : @10};
    
    [[[HRNetworkingManager alloc] init ] POST_PATH:PATH_NEWFRIEND params:dic success:^(id result) {
        
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            [self.NewFridentDataArr removeAllObjects];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_NewFrident *model = [[ICN_NewFrident alloc] initWithDictionary:dic error:nil];
                [self.NewFridentDataArr addObject:model];
            }
        }
        else{
            [self customMethod];
        }
        [self.newfriendsTableView reloadData];
        [self.newfriendsTableView.mj_header endRefreshing];
        [self.newfriendsTableView.mj_footer endRefreshing];
        
    } failure:^(NSDictionary *errorInfo) {

    }];

}
//好友审核 1通过 2拒绝
-(void)httpAgreeandRefuseAction:(NSString *)action andFridendid:(NSString *)fridentid{
    
    NSDictionary *dic=@{@"applicationId":fridentid,@"action":action,@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_CHECKFRIDENT params:dic success:^(id result) {
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        if (baseModel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"发送成功"];
            [self refresh];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.newfriendsTableView registerNib:[UINib nibWithNibName:@"ICN_NewFridentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NewFridentTableViewCell"];
    self.newfriendsTableView.delegate=self;
    self.newfriendsTableView.dataSource=self;
    self.newfriendsTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    [self refresh];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.NewFridentDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    self.cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_NewFridentTableViewCell"];
    if (self.cell==nil) {
        self.cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_NewFridentTableViewCell" owner:self options:nil] lastObject];
    }
    self.cell.model = self.NewFridentDataArr[indexPath.row];
    
    //同意和拒绝的响应事件
    [self.cell returnLocationPhoto:^(ICN_NewFridentTableViewCell *cell) {
        NSLog(@"点击了确定按钮");
        [self httpAgreeandRefuseAction:@"1" andFridendid:cell.model.friendId];
    }];
    [self.cell returnTakingPhoto:^(ICN_NewFridentTableViewCell *cell) {
        [self httpAgreeandRefuseAction:@"2" andFridendid:cell.model.friendId];
    }];

    return self.cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}


#pragma mark - --- 自定义方法 ---

-(void)customMethod{
    
    if (self.NewFridentDataArr.count == 0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无好友信息";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
    
}

-(void)refresh{
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self.NewFridentDataArr removeAllObjects];
        [self httpNetWoringPage:self.page];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.newfriendsTableView.mj_header = header;
    
    self.newfriendsTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page+=1;
        [self httpNetWoringPage:self.page];
    }];
    [self.newfriendsTableView.mj_header beginRefreshing];


}

#pragma mark - --- IBActions ---

- (IBAction)backActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)NewFridentDataArr{
    if (!_NewFridentDataArr) {
        _NewFridentDataArr=[[NSMutableArray alloc]init];
    }
    return _NewFridentDataArr;
}


@end
