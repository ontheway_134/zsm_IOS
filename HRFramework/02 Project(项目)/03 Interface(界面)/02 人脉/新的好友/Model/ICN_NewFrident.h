//
//  ICN_NewFrident.h
//  ICan
//
//  Created by shilei on 16/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface ICN_NewFrident : BaseOptionalModel

#pragma mark --- 计算属性 ---
@property (nonatomic , assign , getter=isSelected)BOOL selected; // 是否被选中


@property(nonatomic,strong)NSString *companyName;  //公司名字      公司
@property(nonatomic,strong)NSString *friendId;   //好友member_id
@property(nonatomic,strong)NSString *memberId;   //我的member_id
@property(nonatomic,strong)NSString *memberLogo;   //头像地址;     头像
@property(nonatomic,strong)NSString *memberMajor;   //专业
@property(nonatomic,strong)NSString *memberPosition;  //职位         职务
@property(nonatomic,strong)NSString *memberSchool;   //大学
@property(nonatomic,strong)NSString *memberNick;   //好友名字    昵称
@property(nonatomic,strong)NSString *status;  //关系



@end
