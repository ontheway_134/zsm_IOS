//
//  ICN_AddFridentViewController.m
//  ICan
//
//  Created by shilei on 16/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_AddFridentViewController.h"
#import "ICN_InterestTableViewCell.h"
#import "BaseOptionalModel.h"
#import "BaseOptionalModel.h"
#import "ICN_InterersPeople.h"
#import "ICN_UserHomePagerVC.h"

@interface ICN_AddFridentViewController ()<UITableViewDelegate,UITableViewDataSource,ICN_BtnSelectedAddDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *addFridentTableView;
@property(nonatomic,strong)NSMutableArray *NewFridentDataArr;
@property (weak, nonatomic) IBOutlet UITextField *searchTextFiled;

@property(nonatomic,strong)UIImageView *ima;
@property(nonatomic,strong)UILabel *la;

@end

@implementation ICN_AddFridentViewController

#pragma mark - --- 网络请求 ---

//搜索的网络请求
-(void)httpseekFrident{
    NSDictionary *dic=@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"cont":self.searchTextFiled.text,@"all":@1,@"page":@1,@"pagenum":@10};
    [[[HRNetworkingManager alloc]init] POST_PATH:PATH_SEEKFRIDENT params:dic success:^(id result) {
        [self endRefreshWithTableView:self.addFridentTableView];
        BaseOptionalModel *basemodel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.NewFridentDataArr removeAllObjects];
        if (basemodel.code == 0) {
            [self.ima removeFromSuperview];
            [self.la removeFromSuperview];
         
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_InterersPeople *model = [[ICN_InterersPeople alloc] initWithDictionary:dic error:nil];
#warning 具体能够搜索出来哪些好友显示在这上面，根据status判断
//                if (model.status.integerValue == 1 || model.status.integerValue ==4) {
                    [self.NewFridentDataArr addObject:model];
//                }
            }
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            if ([basemodel.info isEqualToString:@"数据为空"]) {
                [self customMethod];
            }
        }
         [self.addFridentTableView reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"搜索失败");
        [self endRefreshWithTableView:self.addFridentTableView];
    }];
    
}


#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.addFridentTableView registerNib:[UINib nibWithNibName:@"ICN_InterestTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_InterestTableViewCell"];
    self.addFridentTableView.delegate=self;
    self.addFridentTableView.dataSource=self;
    self.addFridentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
#warning 这里不要给我动，如果放开会出现搜索进入默认就有数据，如果别的地方有需要，需要添加bool值进行判断
   // [self configTableViewRefreshHeaderFooterView];
    self.searchTextFiled.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    if (self.searchContent) {
//        self.searchTextFiled.text = self.searchContent;
//    }
    [self.addFridentTableView.mj_header beginRefreshing];
    [self.navigationController setNavigationBarHidden:YES];

}

#pragma mark - --- Protocol ---

#pragma mark ICN_BtnSelectedAddDelegate

-(void)ICN_BtnSelectedAddDelegate:(UIButton *)btn and:(ICN_InterestTableViewCell *)cell{
    
    if ([cell.model.status isEqualToString:@"1"]) {
        return;
    }
   
    if ([cell.model.status isEqualToString:@"3"]) {
        return;
    }
    if ([cell.model.status isEqualToString:@"5"]) {
        return;
    }
    
    NSDictionary *dic=@{@"fromId":[USERDEFAULT valueForKey:HR_CurrentUserToken],@"toId":cell.model.memberId};
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ADDFRIDENT params:dic success:^(id result) {
        
        BaseOptionalModel *baseModel=[[BaseOptionalModel alloc]initWithDictionary:result error:nil];
        [self.NewFridentDataArr removeAllObjects];
        if (baseModel.code == 0) {
           
            [btn setTitle:@"等待验证" forState:UIControlStateNormal];
            [btn setBackgroundImage:[UIImage imageNamed:@"同意按钮"] forState:UIControlStateNormal];
            [self httpseekFrident];
            
        }else{
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
        }
       
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.NewFridentDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_InterestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_InterestTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle]loadNibNamed:@"ICN_InterestTableViewCell" owner:self options:nil] lastObject];
    }
    cell.delegate = self;
    cell.model=self.NewFridentDataArr[indexPath.row];
    return cell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_UserHomePagerVC *userhome=[[ICN_UserHomePagerVC alloc]init];
    ICN_InterestTableViewCell *cell= (ICN_InterestTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    userhome.memberId=cell.model.memberId;
    [self.navigationController pushViewController:userhome animated:YES];
}

#pragma mark - --- 自定义方法 ---

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self httpseekFrident];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.addFridentTableView.mj_header = header;
    [self.addFridentTableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


//添加好友无结果时背景显示无结果的背景图片
-(void)customMethod{
    
    if (self.NewFridentDataArr.count == 0) {
        self.ima=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"无结果"]];
        [self.view addSubview:self.ima];
        
        [self.ima mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.mas_equalTo(self.view);
            make.centerY.mas_equalTo(self.view);
        }];
        
        self.la=[[UILabel alloc]init];
        self.la.text=@"无搜索结果";
        self.la.font=[UIFont systemFontOfSize:14];
        [self.view addSubview:self.la];
        
        [self.la mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.ima.mas_bottom).offset(20);
            make.centerX.mas_equalTo(self.view);
        }];
    }
    
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
//搜索的点击事件
- (IBAction)searchActions:(id)sender {
    [self.view endEditing:YES];
    [self.addFridentTableView.mj_header beginRefreshing];
    [self configTableViewRefreshHeaderFooterView];
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)NewFridentDataArr{
    if (!_NewFridentDataArr) {
        _NewFridentDataArr=[NSMutableArray array];
    }
    return _NewFridentDataArr;
}


@end
