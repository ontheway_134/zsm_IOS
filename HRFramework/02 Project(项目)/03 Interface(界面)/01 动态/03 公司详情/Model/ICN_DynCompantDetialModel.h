//
//  ICN_DynCompantDetialModel.h
//  ICan
//
//  Created by albert on 2017/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynCompantDetialModel : BaseOptionalModel


@property (nonatomic , copy)NSString *memberId; // 公司ID
@property (nonatomic , copy)NSString *companyLogo; // 公司头像
@property (nonatomic , copy)NSString *companyName; // 公司名称
@property (nonatomic , copy)NSString *companyNature; // 企业性质
@property (nonatomic , copy)NSString *companyScale; // 企业规模,
@property (nonatomic , copy)NSString *companyIndustry; // 企业所属行业,
@property (nonatomic , copy)NSString *companyDetail; // 公司详情
@property (nonatomic , copy)NSString *province; // 所在省份,
@property (nonatomic , copy)NSString *city; // 公司所在城市
@property (nonatomic , copy)NSString *companyIndustryName; // 企业性质字段
@property (nonatomic , copy)NSString *isConcern; // 是否关注


@end
