//
//  ICN_DynCompanyViewModel.h
//  ICan
//
//  Created by albert on 2017/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ICN_DynCompantDetialModel;

@protocol DynCompanyViewModelDelegate <NSObject>

/** 响应网络请求成功的回调 */
- (void)responseNetRequestWithSuccess:(BOOL)success Error:(NSString *)error;

/** 响应添加取消关注的回调 */
- (void)responseCompanyAttention:(BOOL)isAttention Success:(BOOL)success Error:(NSString *)error;

@end

@interface ICN_DynCompanyViewModel : NSObject

@property (nonatomic , weak)id<DynCompanyViewModelDelegate> delegate;
@property (nonatomic , strong)ICN_DynCompantDetialModel *model;



- (void)requestForCompanyDetialModelWithCompanyId:(NSString *)companyId;

- (void)requestForCompanyAttention:(BOOL)isAttention CompanyId:(NSString *)companyId;

@end
