//
//  ICN_CompanyDetialVC.m
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CompanyDetialVC.h"
#import "ICN_DynCompanyViewModel.h"
#import "ICN_DynCompantDetialModel.h"

@interface ICN_CompanyDetialVC ()<DynCompanyViewModelDelegate>

@property (weak, nonatomic) IBOutlet UILabel *p_CompanyContentLabel; // 正文内容

@property (weak, nonatomic) IBOutlet UIScrollView *p_ContentScroller;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p_ContentViewConstraint;

@property (weak, nonatomic) IBOutlet UIImageView *companyIcon;
@property (weak, nonatomic) IBOutlet UILabel *companyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *compantSignLabel;
@property (weak, nonatomic) IBOutlet UILabel *compantPropertyLabel; // 公司属性|公司人数
@property (weak, nonatomic) IBOutlet UIImageView *LookUpStatusImage; // 添加/取消关注状态图片
@property (nonatomic , strong)UITapGestureRecognizer *attentionTap;
@property (nonatomic , strong)ICN_DynCompanyViewModel *viewModel;



@end

@implementation ICN_CompanyDetialVC

#pragma mark - ---------- 懒加载 ----------
- (UITapGestureRecognizer *)attentionTap{
    if (_attentionTap == nil) {
        _attentionTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnChangeCompanyAttentionStatus)];
        [self.LookUpStatusImage addGestureRecognizer:_attentionTap];
    }
    return _attentionTap;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 配置ViewModel
    self.viewModel = [[ICN_DynCompanyViewModel alloc] init];
    self.viewModel.delegate = self;
    [self setHiddenDefaultNavBar:YES];
    if (self.companyId) {
        [self.viewModel requestForCompanyDetialModelWithCompanyId:self.companyId];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self attentionTap];
    self.companyIcon.layer.masksToBounds = YES;
    self.companyIcon.layer.cornerRadius = 25.0;
    CGFloat height = self.p_CompanyContentLabel.mj_y + self.p_CompanyContentLabel.height + 85.0;
    
    self.p_ContentViewConstraint.constant = height;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------
// 点击用于取消关注的方法
- (void)tapOnChangeCompanyAttentionStatus{
    if (self.viewModel.model.isConcern.integerValue == 0) {
        // 用于添加关注
        [self.viewModel requestForCompanyAttention:YES CompanyId:self.companyId];
    }else{
        // 用于取消关注
        [self.viewModel requestForCompanyAttention:NO CompanyId:self.companyId];
    }
}

// 在获取到数据之后刷新页面的方法
- (void)configPageDetialPropertyWithModel:(ICN_DynCompantDetialModel *)model{
    
    // 根据公司详情Model处理数据(保证去掉NULL)
    if (model.companyName == nil || [model.companyName isEqualToString:@""]) {
        model.companyName = @"公司名称";
    }
    if (model.companyNature == nil || [model.companyNature isEqualToString:@""]) {
        model.companyNature = @"公司状态";
    }
    if (model.companyScale == nil || [model.companyScale isEqualToString:@""]) {
        model.companyScale = @"100T";
    }
    
    if (model) {
        // Model存在
        [self.companyIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)] placeholderImage:[UIImage imageNamed:@"公司logo"]];
        self.companyTitleLabel.text = model.companyName;
        self.compantSignLabel.text = model.companyIndustryName;
        self.compantPropertyLabel.text = SF(@"%@|%@人",model.companyNature,model.companyScale);
        self.p_CompanyContentLabel.text = model.companyDetail;
        if (model.isConcern.integerValue == 0) {
            // 未关注
            self.LookUpStatusImage.image = [UIImage imageNamed:@"加关注"];
        }else{
            // 关注
            self.LookUpStatusImage.image = [UIImage imageNamed:@"取消关注"];
        }
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"页面显示错误"];
    }
}


#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnNavgationBtnAction:(UIButton *)sender {
    
    switch (sender.tag) {
        case ICN_NavgationLeftBtnType:
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case ICN_NavgationShareBtnType:
            HRLog(@"点击了分享按钮");
            break;
            
        default:
            break;
    }
    
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- DynCompanyViewModelDelegate ---

- (void)responseCompanyAttention:(BOOL)isAttention Success:(BOOL)success Error:(NSString *)error{
    if (success) {
        if (isAttention) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"添加关注成功"];
            self.LookUpStatusImage.image = [UIImage imageNamed:@"取消关注"];
            self.viewModel.model.isConcern = @"1";
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消关注成功"];
             self.LookUpStatusImage.image = [UIImage imageNamed:@"加关注"];
            self.viewModel.model.isConcern = @"0";
        }
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseNetRequestWithSuccess:(BOOL)success Error:(NSString *)error{
    if (success) {
        // 回调网络获取成功
        [self configPageDetialPropertyWithModel:self.viewModel.model];
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"获取公司详情成功"];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:error];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
