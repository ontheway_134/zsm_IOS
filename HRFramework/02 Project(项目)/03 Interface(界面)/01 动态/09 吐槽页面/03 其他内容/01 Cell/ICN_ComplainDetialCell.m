//
//  ICN_ComplainDetialCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainDetialCell.h"

@interface ICN_ComplainDetialCell ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间label
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 正文内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评价按钮



@end

@implementation ICN_ComplainDetialCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOnComplainButtonAction:(UIButton *)sender {
    
}

@end
