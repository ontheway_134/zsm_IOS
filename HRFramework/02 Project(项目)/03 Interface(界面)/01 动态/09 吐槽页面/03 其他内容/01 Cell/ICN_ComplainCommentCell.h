//
//  ICN_ComplainCommentCell.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ComplainCommentCell : UITableViewCell

@property (nonatomic , assign)BOOL hiddenSegmentView; // 是否隐藏分隔栏


@end
