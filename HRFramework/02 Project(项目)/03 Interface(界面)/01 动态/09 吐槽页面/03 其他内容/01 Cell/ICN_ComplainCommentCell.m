//
//  ICN_ComplainCommentCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainCommentCell.h"

@interface ICN_ComplainCommentCell ()


@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 评论内容label
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn; // 收藏按钮
@property (weak, nonatomic) IBOutlet UIView *segmentGrayView; // 分割Cell用灰条 -- 只有最后一个不显示


@end

@implementation ICN_ComplainCommentCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setHiddenSegmentView:(BOOL)hiddenSegmentView{
    _hiddenSegmentView = hiddenSegmentView;
    _segmentGrayView.hidden = _hiddenSegmentView;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOnComplainCommentAction:(UIButton *)sender {
    
}


@end
