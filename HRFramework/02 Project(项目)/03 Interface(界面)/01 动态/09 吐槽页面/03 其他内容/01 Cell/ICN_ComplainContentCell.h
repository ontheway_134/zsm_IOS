//
//  ICN_ComplainContentCell.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_ComplainListModel;

@interface ICN_ComplainContentCell : UITableViewCell

@property (nonatomic , strong) ICN_ComplainListModel *model;

@end
