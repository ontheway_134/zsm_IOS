//
//  ICN_ComplainContentCell.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainContentCell.h"
#import "ICN_ComplainListModel.h"

@interface ICN_ComplainContentCell ()

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 正文内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *commentBtn; // 评价按钮



@end


@implementation ICN_ComplainContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_ComplainListModel *)model{
    _model = model;
    self.nickNameLabel.text = SF(@"%@",_model.RandomName);
    self.timeLabel.text = SF(@"%@",_model.createTime);
    [self.userLogoIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.RandomPicture)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
    self.contentLabel.text = SF(@"%@",_model.contant);
    if (_model.ismakeComplaints != nil) {
        if ([_model.ismakeComplaints integerValue] == 0) {
            // 已点赞
            self.likeUpBtn.selected = YES;
        }else{
            self.likeUpBtn.selected = NO;
        }
        
    }
    [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateNormal];
    [self.likeUpBtn setTitle:SF(@"%@",_model.ThumbsupNum) forState:UIControlStateSelected];
    [self.commentBtn setTitle:SF(@"%@",_model.commentNum) forState:UIControlStateNormal];
    [self.commentBtn setTitle:SF(@"%@",_model.commentNum) forState:UIControlStateSelected];
}

- (IBAction)clickOnComplainButtonAction:(UIButton *)sender {
    
}



@end
