//
//  ICN_ComplainDetialPager.m
//  ICan
//
//  Created by albert on 2017/3/1.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainDetialPager.h"
#import "ICN_ComplainCommentCell.h" // 吐槽中评论你的Cell
#import "ICN_ComplainDetialCell.h" // 吐槽中的详情Cell
#import "ICN_ComSingleTitleSectionView.h" // 吐槽section 的头视图
#import "ICN_InputCommentView.h" // 通用的评论输出框（含代理）

@interface ICN_ComplainDetialPager ()<UITableViewDelegate , UITableViewDataSource , CommentTextDelegate>

@property (strong , nonatomic)UITableView *tableView;
@property (nonatomic , strong)ICN_InputCommentView *commentTextView; // 提交评论的输出框

@end

@implementation ICN_ComplainDetialPager

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainCommentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainCommentCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainDetialCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainDetialCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 0.0;
        _tableView.sectionHeaderHeight = 0.0;
        _tableView.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        
        [self.view addSubview:_tableView];
        // 使用masnory设置tableview的尺寸
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    return _tableView;
}

- (ICN_InputCommentView *)commentTextView{
    if (_commentTextView == nil) {
        _commentTextView = [ICN_InputCommentView loadCommentView];
        _commentTextView.delegate = self;
        // 设置约束
        [self.view addSubview:_commentTextView];
        [_commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(49.0);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.bottom.equalTo(self.view);
        }];
    }
    return _commentTextView;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置显示默认的导航栏
    [self setHiddenDefaultNavBar:NO];
    // 初始化tableview
    [self tableView];
    // 加载评论框页面
    [self commentTextView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 按钮响应事件 ----------

/** 点击发送评论消息的按钮 */
- (void)clickOnCommentSenderBtnAction:(UIButton *)sender{
    
}

#pragma mark - ---------- 协议 ----------

/** 评论输出框的相关代理 */
#pragma mark --- CommentTextDelegate ---

// 获取到评论信息的回调
- (void)responseWithCommitComment:(NSString *)comment{
    
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 设置一共有12个假的评论数据
    switch (section) {
        case 0:{
            return 1;
            break;
        }
        // 返回评论的数量
        default:
            return 12;
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    switch (indexPath.section) {
        case 0:{
            // 返回正文的Cell
            ICN_ComplainDetialCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainDetialCell class])];
            return cell;
            break;
        }
        default:{
            // 返回评论内容Cell
            ICN_ComplainCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainCommentCell class])];
            return cell;
            break;
        }
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 1) {
        ICN_ComSingleTitleSectionView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
//        header.backgroundView.backgroundColor = RGB0X(0xf0f0f0);
        header.contentView.backgroundColor = RGB0X(0xf0f0f0);
        header.headerTitle = @"评论";
        return header;
    }
    
    return nil;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据section来 设置评论的默认高度
    if (indexPath.section == 1) {
        return 111;
    }
    
    return 137;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    // 默认的 section 0 没有头视图 section 1 有
    if (section == 1) {
        return 40.0;
    }
    if (section == 0) {
        return 0.0;
    }
    return 0.0;
    
}



@end
