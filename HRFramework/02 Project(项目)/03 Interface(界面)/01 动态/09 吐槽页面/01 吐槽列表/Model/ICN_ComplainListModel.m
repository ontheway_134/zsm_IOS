//
//  ICN_ComplainListModel.m
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainListModel.h"

@implementation ICN_ComplainListModel


// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"complainId" : @"id"}];
}



@end
