//
//  ICN_ComplainListViewModel.m
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ComplainListViewModel.h"
#import "ICN_ComplainListModel.h" // 评论列表Model
#import "BaseOptionalModel.h"

@implementation ICN_ComplainListViewModel

- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
    }
    return self;
}

- (NSMutableArray *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (void)requestLikeUp:(BOOL)isLikeUp ComplainId:(NSString *)complainId{
    // 第一步 获取到用户token ; 操作需要的吐槽id 否则返回 -- 此处不做对于游客登录的对应处理
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil || complainId == nil) {
        return ;
    }
    
    // 声明参数字典
    NSDictionary *params = @{
                             @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken],
                             @"id" : complainId
                             };
    NSString * netPath ;
    
    if (isLikeUp) {
        // 进行点赞的网络请求 == 赋值路径
        netPath = PATH_ComplainLikeUp;
    }else{
        netPath = PATH_ComplainDisLikeUp;
    }
    
    // 调用接口
    if (netPath) {
        [[HRNetworkingManager alloc] POST_PATH:netPath params:params success:^(id result) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
                // 正确操作
            }else{
                HRLog(@"warn====没有处理吐槽列表ViewModel的代理");
                return ;
            }
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (isLikeUp) {
                // 点赞逻辑
                if (baseModel.code == 0) {
                    [self.delegate responseWithEnumType:Complain_ResponseLikeUp Success:YES Info:@"点赞成功"];
                }else{
                    [self.delegate responseWithEnumType:Complain_ResponseLikeUp Success:NO Info:baseModel.info];
                }
            }else{
                // 取消点赞逻辑
                if (baseModel.code == 0) {
                    [self.delegate responseWithEnumType:Complain_ResponseDisLikeUp Success:YES Info:@"取消点赞成功"];
                }else{
                    [self.delegate responseWithEnumType:Complain_ResponseDisLikeUp Success:NO Info:baseModel.info];
                }

            }
        } failure:^(NSDictionary *errorInfo) {
            if (isLikeUp) {
                [self.delegate responseWithEnumType:Complain_ResponseLikeUp Success:NO Info:@"网络请求失败"];
            }else{
                [self.delegate responseWithEnumType:Complain_ResponseDisLikeUp Success:NO Info:@"网络请求失败"];
            }
        }];
    }
    
}


- (void)requestForComplainedListMineList:(BOOL)isMine load:(BOOL)isLoad{
    
    // 第一步 获取到用户token 访问自己的吐槽列表时候需要
    if (isMine && [USERDEFAULT valueForKey:HR_CurrentUserToken] == nil) {
        return ;
    }
    
    // 第二步 判断是刷新还是加载
    if (isLoad) {
        _currentPage ++;
    }else{
        _currentPage = 1;
    }
    
    // 第三步 生成参数字典
    NSMutableDictionary *params = [@{
                                    @"page" : SF(@"%ld",_currentPage)
                             } mutableCopy];
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        [params setValue:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    }
    
    // 第四步 判断使用的中间路径
    NSString *netPath;
    if (isMine) {
        // 获取自己的吐槽列表
        netPath = PATH_ComplainMineList;
    }else{
        netPath = PATH_ComplainOthersList;
    }
    
    // 根据bool调用对应的回调类型
    NSInteger type = 0;
    if (isMine) {
        type = Complain_ResponseMineList;
    }else{
        type = Complain_ResponseOthersList;
    }
    
    // 第五步 开始网络请求
    if (netPath) {
        [[HRNetworkingManager alloc] POST_PATH:netPath params:params success:^(id result) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(responseWithEnumType:Success:Info:)]) {
                // 正确操作
            }else{
                HRLog(@"warn====没有处理吐槽列表ViewModel的代理");
                return ;
            }
            BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (baseModel.code == 0) {
                // 获取到有效数据的时候
                NSArray *resultArr = [result valueForKey:@"result"];
                if (!isLoad) {
                    // 刷新操作
                    [self.modelsArr removeAllObjects];
                }
                for (NSDictionary *paramsDic in resultArr) {
                    ICN_ComplainListModel *model = [[ICN_ComplainListModel alloc] initWithDictionary:paramsDic error:nil];
                    [self.modelsArr addObject:model];
                }
                
                [self.delegate responseWithEnumType:type Success:YES Info:baseModel.info];
            }else{
                [self.delegate responseWithEnumType:type Success:NO Info:baseModel.info];
            }
        } failure:^(NSDictionary *errorInfo) {
            [self.delegate responseWithEnumType:type Success:NO Info:@"网络请求失败"];
        }];
    }


}







@end




