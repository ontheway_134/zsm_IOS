//
//  ICN_ComplainListViewModel.h
//  ICan
//
//  Created by albert on 2017/3/8.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_ComplainListModel;

typedef enum : NSUInteger {
    Complain_ResponseMineList = 133300011, // 类型是获取到用户自己吐槽列表
    Complain_ResponseOthersList, // 类型是获取到吐槽列表
    Complain_ResponseLikeUp, // 类型是获取用户点赞的回调
    Complain_ResponseDisLikeUp, // 类型是获取到用户取消点赞的回调
} ComplainResponseType;

@protocol ComplainListDelegate <NSObject>

// 获取到数据处理之后的回调
- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info;

@end

@interface ICN_ComplainListViewModel : NSObject

@property (nonatomic , weak)id<ComplainListDelegate> delegate;
@property (nonatomic , strong)NSMutableArray *modelsArr; // 数据列表数组
@property (nonatomic , assign)NSInteger currentPage; // 当前加载的分页数 默认是1


/** 获取吐槽列表的内容 */
- (void)requestForComplainedListMineList:(BOOL)isMine load:(BOOL)isLoad;

/** 请求列表数据（刷新or加载） */
- (void)requestLikeUp:(BOOL)isLikeUp ComplainId:(NSString *)complainId;


@end
