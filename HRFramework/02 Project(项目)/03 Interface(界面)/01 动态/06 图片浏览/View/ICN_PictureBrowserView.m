//
//  ICN_PictureBrowserView.m
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PictureBrowserView.h"

@interface ICN_PictureBrowserView ()<UIScrollViewDelegate>

@property (nonatomic , strong)UIScrollView *scrollerView; // 内置的轮播
@property (nonatomic , strong)UIPageControl *pageControl; // 内置的显示第几张图片的点标记
@property (nonatomic , strong)NSArray <UIImageView *>* imageViewListArr; // imageView列表
@property (nonatomic , strong)NSMutableArray <UITapGestureRecognizer *>*tapGestureListArr;
@property (nonatomic , strong)NSMutableArray <UIPinchGestureRecognizer *>*scaleGestureListArr;


@end

@implementation ICN_PictureBrowserView

#pragma mark - ---------- 属性构造器 ----------

- (void)setUrlListArr:(NSArray<NSString *> *)urlListArr{

    if (urlListArr) {
        _urlListArr = urlListArr;
        // 如果再赋值的时候有历史数据以及相关内容则清除
        if (self.imageViewListArr != nil) {
            for (NSInteger i = 0; i < self.imageViewListArr.count; i++) {
                UIImageView *imageView = [self.imageViewListArr objectAtIndex:i];
                [imageView removeGestureRecognizer:self.scaleGestureListArr[i]];
                [imageView removeFromSuperview];
            }
            self.imageViewListArr = nil;
            [self.scaleGestureListArr removeAllObjects];
        }
        NSMutableArray <UIImageView *>*timageList = [NSMutableArray array];
        for (NSString *path in _urlListArr) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageView.userInteractionEnabled = YES;
            [imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(path)] placeholderImage:[UIImage imageNamed:@"占位图"]];
            // 添加捏合手势
//            imageView.multipleTouchEnabled = YES;
//            UIPinchGestureRecognizer* gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleImage:)];
//            // 为imageView添加手势处理器
//            [imageView addGestureRecognizer:gesture];
//            [self.scaleGestureListArr addObject:gesture];
            
            [self addSubview:imageView];
            [timageList addObject:imageView];
        }
        self.imageViewListArr = [NSArray arrayWithArray:timageList];
        
        [self setNeedsDisplay];
    }
    
}

- (void)setImageListArr:(NSArray<UIImage *> *)imageListArr{
    if (imageListArr) {
        // 如果再赋值的时候有历史数据以及相关内容则清除
        if (self.imageViewListArr != nil) {
            for (NSInteger i = 0; i < self.imageViewListArr.count; i++) {
                UIImageView *imageView = [self.imageViewListArr objectAtIndex:i];
                [imageView removeGestureRecognizer:self.scaleGestureListArr[i]];
                [imageView removeFromSuperview];
            }
            self.imageViewListArr = nil;
            [self.scaleGestureListArr removeAllObjects];
        }
        NSMutableArray <UIImageView *>*timageList = [NSMutableArray array];
        for (UIImage *image in _imageListArr) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageView.userInteractionEnabled = YES;
            imageView.image = image;
//            // 添加捏合手势
//            imageView.multipleTouchEnabled = YES;
//            UIPinchGestureRecognizer* gesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scaleImage:)];
//            // 为imageView添加手势处理器
//            [imageView addGestureRecognizer:gesture];
//            [self.scaleGestureListArr addObject:gesture];
            
            [self addSubview:imageView];
            [timageList addObject:imageView];
        }
        self.imageViewListArr = [NSArray arrayWithArray:timageList];
        
        [self setNeedsDisplay];
    }
    
}

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<UITapGestureRecognizer *> *)tapGestureListArr{
    if (_tapGestureListArr == nil) {
        _tapGestureListArr = [NSMutableArray array];
    }
    return _tapGestureListArr;
}

- (NSMutableArray<UIPinchGestureRecognizer *> *)scaleGestureListArr{
    if (_scaleGestureListArr == nil) {
        _scaleGestureListArr = [NSMutableArray array];
    }
    return _scaleGestureListArr;
}

#pragma mark - ---------- 初始化方法 ----------

- (instancetype)initWithFrame:(CGRect)frame ImagesList:(NSArray<UIImage *> *)imagesArr{
    self = [super initWithFrame:frame];
    if (self) {
        _scrollerView = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _scrollerView.backgroundColor = [UIColor blackColor];
        [self addSubview:_scrollerView];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
        _pageControl.hidesForSinglePage = YES;
        
        if (imagesArr) {
            self.imageListArr = imagesArr;
        }
        _scrollerView.delegate = self;
        _scrollerView.userInteractionEnabled = YES;
        _scrollerView.scrollEnabled = YES;
        _scrollerView.pagingEnabled = YES;
        [self addSubview:_scrollerView];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame ImageUrlList:(NSArray<NSString *> *)strUrlArr{
    self = [super initWithFrame:frame];
    if (self) {
        _scrollerView = [[UIScrollView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _scrollerView.backgroundColor = [UIColor blackColor];
        [self addSubview:_scrollerView];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectZero];
        _pageControl.hidesForSinglePage = YES;
        
        if (strUrlArr) {
            self.urlListArr = strUrlArr;
        }
        _scrollerView.delegate = self;
        [self addSubview:_scrollerView];
    }
    return self;
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    for (NSInteger i = 0; i < self.imageViewListArr.count; i++) {
        
        if (i == 0) {
            [self.imageViewListArr[i] mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.mas_left);
                make.right.equalTo(self.mas_right);
                make.center.equalTo(self);
            }];
        }else{
            [self.imageViewListArr[i] mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.imageViewListArr[i - 1].mas_right);
                make.width.mas_equalTo(self.imageViewListArr[i - 1].mas_width);
                make.centerY.equalTo(self.mas_centerY);
            }];
        }
    }
    
    self.scrollerView.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * self.imageViewListArr.count, self.scrollerView.frame.size.height);
    self.pageControl.numberOfPages = self.imageViewListArr.count;
    self.pageControl.size = CGSizeMake(self.imageViewListArr.count * 26.5, 20);
    [self addSubview:self.pageControl];
    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.bottom).offset(-42.0);
        make.centerX.equalTo(self.mas_centerX);
    }];
    
}

#pragma mark - ---------- 私有方法 ----------

//- (void) scaleImage:(UIPinchGestureRecognizer*)gesture
//
//{
//    
//    CGFloat scale = gesture.scale;
//    CGFloat currentScale = 1.0;
//    NSInteger indexTag = [self.scaleGestureListArr indexOfObject:gesture];
//    // 取到当前的iamgeview
//    UIImageView *imageView = [self.imageViewListArr objectAtIndex:indexTag];
//    
//    // 如果捏合手势刚刚开始
//    
//    if (gesture.state == UIGestureRecognizerStateBegan)
//        
//    {
//        
//        // 计算当前缩放比 -- 可能不准确
//        
//        currentScale = imageView.width / imageView.image.size.width;
//    
//    }
//    
//    // 根据手势处理器的缩放比例计算图片缩放后的目标大小
//    
//    CGSize targetSize = CGSizeMake(imageView.image.size.width * scale * currentScale,
//                                   
//                                   imageView.image.size.height * scale * currentScale);
//    
//    // 对图片进行缩放
//    
//    imageView.image = [imageView.image imageByScalingToSize:targetSize];
//    
//}




#pragma mark - ---------- 代理 ----------

#pragma mark --- UIScrollViewDelegate ---

/** 停止减速的时候 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    self.pageControl.currentPage = index;
}





@end
