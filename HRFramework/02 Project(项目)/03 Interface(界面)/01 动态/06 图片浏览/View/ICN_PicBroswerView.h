//
//  ICN_PicBroswerView.h
//  ICan
//
//  Created by albert on 2017/1/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_PicBroswerView : UIView

@property (nonatomic , strong)NSArray *urlPathArr; // 网址路径列表

@property (nonatomic , assign)NSInteger currentIndex; // 点击后选中的默认的imageview


@end
