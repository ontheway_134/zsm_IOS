//
//  ICN_SingleShowImageScroll.m
//  ICan
//
//  Created by albert on 2017/1/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SingleShowImageScroll.h"

#define PMaxFrame  CGRectMake(0, 0, SCREEN_WIDTH * 3.0, SCREEN_HEIGHT * 3.0)
#define PMinFrame  CGRectMake(0, 0, SCREEN_WIDTH / 2.0, SCREEN_HEIGHT / 2.0)
#define PHolderImage [UIImage imageNamed:@"图层1"]

@interface ICN_SingleShowImageScroll ()<UIScrollViewDelegate>

@property (nonatomic , strong)UIImageView *imageView;
@property (nonatomic , assign)CGRect currentFrame;

// 判断用Boolean属性
@property (nonatomic , assign , getter=isScrollLeftSide)BOOL leftSide; // 是否滑动到左边界
@property (nonatomic , assign , getter=isScrollRightSide)BOOL rightSide; // 是否滑动到右边界
@property (nonatomic , assign , getter=isSideScroll)BOOL sideScroll; // 是否从边界开始滑动的

@end

@implementation ICN_SingleShowImageScroll

- (instancetype)initWithFrame:(CGRect)frame ImagePath:(NSString *)path{
    self = [[ICN_SingleShowImageScroll alloc] initWithFrame:frame];
    if (self) {
        self.scrollEnabled = YES;
        self.showsVerticalScrollIndicator = NO;
        self.showsHorizontalScrollIndicator = NO;
        // 在 UIView 中有一个autoresizingMask的属性，它对应的是一个枚举的值（如下），属性的意思就是自动调整子控件与父控件中间的位置，宽高。
        self.autoresizingMask =UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight;
        self.userInteractionEnabled = YES;
        self.contentSize = self.frame.size;
        [self clearNecessaryBoolProperty];
        [_imageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(path)] placeholderImage:PHolderImage];
        self.delegate = self;
    }
    return self;
}

- (void)clearNecessaryBoolProperty{
    self.sideScroll = NO;
    self.rightSide = NO;
    self.leftSide = NO;
}



#pragma mark --- UIScrollViewDelegate ---

// 当scroll已经开始滑动的时候
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    /** 判断是否翻页的方法
        1. 当前页面的缩放比是 <= 1的时候滑动就是翻页
        2. 当前页面的缩放比 > 1的时候根据偏移量判断是否翻页
           首先判断是否滑到边界 并记录
           其次判断是否从边界开始滑动
           最后判断边界值与边界的对应关系来判断是否翻页
     */
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    
}

@end
