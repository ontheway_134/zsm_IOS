//
//  ICN_DynamicStateHeader.h
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#ifndef ICN_DynamicStateHeader_h
#define ICN_DynamicStateHeader_h

#pragma mark - ---------- Cell常量名 ----------
static NSString const * ICN_SpreadTextCellConstStr = @"SpreadTextCellConstStr"; // 可拓展文本长度的Cell的标识
static NSString const * ICN_ReplyingDynStateCellConstStr = @"ReplyingDynStateCellConstStr"; // 转发内容Cell的标识
static NSString const * ICN_HotWorldCellStr = @"HotWorldCellStr"; // 热词cell标识
static NSString const * ICN_HisCellStr = @"HisCellStr"; // 历史记录cell标识
static NSString const * ICN_DefaultSectionHeaderConstStr = @"DefaultSectionHeaderConstStr"; // 默认section的头视图的标识
static NSString const * ICN_CompanySectionHeaderConstStr = @"CompanySectionHeaderConstStr"; // 公司section的头视图的标识
static NSString const * ICN_DefaultSectionFooterConstStr = @"DefaultSectionFooterConstStr"; // 默认section的尾视图的标识
static NSString const * ICN_ImagesListSectionFooterConstStr = @"ImagesListSectionFooterConstStr"; // 带有图片列表的section的尾视图的标识
static NSString const * ICN_SingleADTextHeaderConstStr = @"ICN_ DynStateSingleAdTextHeader"; // 单行广告标识

#pragma mark - ---------- 动态枚举 ----------

/** 选中按钮类型枚举 */
typedef enum : NSUInteger {
    PRIVATE_DynamicBtn = 1002331, // 动态类型
    PRIVATE_WisdomBtn, // 智讯类型 1002332
    PRIVATE_TopicBtn, // 话题类型 1002333
    PRIVATE_AskActionBtn // 提问类型 1002334
} FirstPager_BtnSelectedTYPE;

/** 动态样式枚举 */
typedef enum : NSUInteger {
    ICN_NormalSpreadTextType = 230101001, // 普通展开文本不带image的样式
    ICN_NormalImagesListType, // 只有图片的样式
    ICN_NormalSpreadTWithImgListType, // 图文都有的样式
    ICN_NormalNullContentType, // 图文都没有只有头尾视图
    ICN_CompanySpreadTextType, // 公司推广有展开文字样式
    ICN_CompanyImagesListType, // 公司推广只有图片样式
    ICN_CompanySpreadTWithImgListType, // 公司推广图文都有样式
    ICN_CompanyNullContentType, // 公司推广只有头尾视图
    ICN_SingleTextADType, // 单行文本广告样式
    ICN_NormalADType, // 普通广告样式
    ICN_ReplyingDynStateType // 好友转发样式
} ICN_DynStateCellType;

/** 提示窗按钮样式枚举 */
typedef enum : NSUInteger {
    Publich_DynamicState = 1002331, // 发布到动态 1002331
    Publich_WisdomState, // 发布到智讯 1002332
    Publich_ComplainState, // 发布到吐槽 1002333
    Publich_AskQuestion, // 发布到提问 1002334
    ICN_WarnBottomBtnType, // 底部按钮样式 230102003
    ICN_ShareToDynamicStateBtnType = 230102004, // 分享到动态 230102004
    ICN_ShareToICNFriendBtnType, // 分享到好友 230102005
    ICN_ShareToWeiChatBtnType, // 分享到微信 230102006
    ICN_ShareToWeiBOBtnType, // 分享到微博 230102007
    ICN_ShareToQQBtnType, // 分享到QQ 230102008
    ICN_ShareToWeiFridBtnType, // 分享到微信朋友劝 230102008
    ICN_ShareToQQFridBtnType, // 分享到QQ空间 230102008
} ICN_WarnViewBtnType;

/** 通用导航栏按钮tag类型 */
typedef enum : NSUInteger {
    ICN_NavgationLeftBtnType = 230103001, // 通用导航栏左按钮tag
    ICN_NavgationRightBtnType, // 通用导航栏右按钮tag 230103002
    ICN_NavgationShareBtnType, // 导航栏分享按钮tag 230103003
    ICN_NavSearchBtnType, // 导航栏搜索按钮tag 230103004
    ICN_NavPublishBtnType, // 导航栏发布按钮tag 230103005
} ICN_CommonNavButtonTagType;

/** 通用Cell按钮的tag */
typedef enum : NSUInteger {
    ICN_CellLikeActionBtnType = 230104001, // Cell通用点赞按钮 230104001
    ICN_CellCommentBtnType, // Cell通用评论按钮 230104002
    ICN_CellReviewBtnType, // Cell通用回复按钮 230104003
    ICN_CellReportBtnType, // Cell通用举报按钮 230104004
    ICN_CellPullBtnType, // Cell通用下拉按钮 230104005
} ICN_CommonCellBtnType;

typedef enum : NSUInteger {
    ICN_PickerViewMakeSureBtn = 230105001, // PickerView通用确认按钮 230105001
    ICN_PickerViewCancerBtn,// PickerView通用取消按钮 230105002
} ICN_DynPickerViewBtnType;

#pragma mark - ---------- 动态Cell高度宏 ----------

#define ICN_DynStateTextSpreadCellHeight 115.5 // 可拓展文本Cell高度

#define ICN_ReplyingDynStateCellHeight 201.0 // 转发动态Cell高度

#define ICN_DynStateSectionHeaderViewHeight 71.0 // 普通头视图高度

#define ICN_DynStateCompanySectionHeaderHeight 125.0 // 公司推广头视图高度

#define ICN_DynStateSectionNormalFooterHeight 45.5 // 普通尾视图高度

#define ICN_DynStateSectionImagesFooterHeight 158.0 // 有图片列表的尾视图高度

#define ICN_DynStateSingleAdTextViewHeight 39.0 // 单行广告头视图高度


#endif /* ICN_DynamicStateHeader_h */
