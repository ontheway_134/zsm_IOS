//
//  ICN_DynamicStateFirstPagerModel.h
//  ICan
//
//  Created by albert on 2016/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_DynStateContentModel; // 动态详情Model
@class ICN_DynStateADModel; // 动态广告Model

@protocol DynamicStateFirstPagerViewModelDelegate <NSObject>

/** 以每三条正文添加一条广告的方式返回结果数组 */
- (void)responseWithModelsArr:(NSMutableArray *)modesArr;

- (void)responseWithNetRequestError:(NSString *)error;

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority;

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success;

/** 点过赞回调 */
- (void)responseWithAlreadyLikeUp;

@end

@interface ICN_DynamicStateFirstPagerViewModel : NSObject

@property (nonatomic , weak)id<DynamicStateFirstPagerViewModelDelegate> delegate;

#pragma mark - ---------- 关于页面相关的属性 ----------
@property (nonatomic , assign)NSInteger currentPage; // 当前页数

@property (nonatomic , assign)NSInteger currentSelectedType; // 当前选中的类型 - 默认是动态类型


// 刷新当前页面的网络请求
- (void)refreshCurrentPageContentCells;

// 加载下一个页面的网络请求
- (void)loadNextPageContentCells;


// 点赞功能
- (void)likeUpWithType:(NSInteger)type
                 Model:(ICN_DynStateContentModel *)model;

@end
