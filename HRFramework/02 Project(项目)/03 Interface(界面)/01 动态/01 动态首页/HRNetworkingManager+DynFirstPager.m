//
//  HRNetworkingManager+DynFirstPager.m
//  ICan
//
//  Created by albert on 2016/12/17.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager+DynFirstPager.h"
#import "BaseOptionalModel.h"
#import "ToolAboutTime.h"
#import "UILabel+ALB_SizeFit.h" // 根据计算label高度的方法

@implementation HRNetworkingManager (DynFirstPager)

+ (void)requestDynADContentWithPage:(NSInteger)page PageSize:(NSInteger)size Success:(ADContentArrBlock)success Failure:(ErrorBlock)failure{
    
    NSDictionary *params = @{
                             @"page" : SF(@"%ld",page),
                             @"size" : SF(@"%ld",size),
                             };
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicAD params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_DynStateADModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_DynStateADModel *model = [[ICN_DynStateADModel alloc] initWithDictionary:dic error:nil];
                [resultArr addObject:model];
            }
            success(resultArr);
            HRLog(@"首页广告 - 数据获取成功");
        }else{
            HRLog(@"首页广告 - 网络请求成功 - 数据获取失败");
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
        HRLog(@"首页广告 - 网络请求失败");
    }];
    
}

+ (void)requestDynamicStateCommentsWithPage:(NSInteger)page PageSize:(NSInteger)size MatterId:(NSString *)matterId Success:(CommentBlock)success Failure:(ErrorBlock)failure{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"page" : SF(@"%ld",page),
                                     @"size" : SF(@"%ld",size),
                                     @"matterId" : matterId ,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicComment params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            HRLog(@"动态评论 - 数据获取成功");
        }else{
            HRLog(@"动态评论 - 网络请求成功 - 数据获取失败");
        }
        NSMutableArray<ICN_DynamicStateCommentModel *> *resultArr = [NSMutableArray array];
        for (NSDictionary *dic in [result valueForKey:@"result"]) {
            ICN_DynamicStateCommentModel *model = [[ICN_DynamicStateCommentModel alloc] initWithDictionary:dic error:nil];
            model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
            model.code = firstModel.code;
            // 设置cell中评论内容的默认高度
            model.commentLabelHeight = 0.0;
            // 根据Model的评论内容的默认情况对于评论内容label的高度进行机选
            if (model.content) {
                model.commentLabelHeight = [UILabel getTextHeight:model.content width:(SCREEN_WIDTH - 20.0) fontSize:13.0];
            }
            
            [resultArr addObject:model];
        }
        success(resultArr);
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"动态评论 - 数据获取失败");
        failure(errorInfo);
    }];
    
}



+ (void)requestDynamicStateWithPage:(NSInteger)page PageSize:(NSInteger)size Success:(StateArrBlock)success Failure:(ErrorBlock)failure{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"page" : SF(@"%ld",page),
                                     @"size" : SF(@"%ld",size),
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicStateFP params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                model.contentSpread = NO;
                model.widsom = NO;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [resultArr addObject:model];
            }
            success(resultArr);
            HRLog(@"首页动态 - 数据获取成功");
        }else{
            HRLog(@"首页动态 - 网络请求成功 - 数据获取失败");
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
        HRLog(@"首页动态 - 网络请求失败");
    }];
}

+ (void)requestWidsomMessageWithPage:(NSInteger)page PageSize:(NSInteger)size Success:(StateArrBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"page" : SF(@"%ld",page),
                                     @"size" : SF(@"%ld",size),
                                     } mutableCopy];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_WisdomMessage params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                model.matterId = model.DynID;
                model.contentSpread = NO;
                model.widsom = YES;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [resultArr addObject:model];
            }
            success(resultArr);
            HRLog(@"首页智讯 - 数据获取成功");
        }else{
            HRLog(@"首页智讯 - 网络请求成功 - 数据获取失败");
        }

        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
        HRLog(@"首页智讯 - 网络请求失败");
    }];
}


#pragma mark - ---------- 动态功能接口 ----------

+ (void)updateDynamicLikeUpStateWithMatterId:(NSString *)matterId Type:(NSString *)type Success:(BaseModelBlock)success Failure:(ErrorBlock)failure{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"matterId" : matterId ,
                                     @"type" : type,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicStateLikeUp params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (model.code == 1) {
            HRLog(@"动态点赞 - 数据获取成功 - 点赞失败");
        }else{
            HRLog(@"动态点赞 - 点赞成功");
            success(model);
        }
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"动态点赞 - 数据获取失败");
        failure(errorInfo);
    }];

}

+ (void)publishDynamicStateCommentWithMatterId:(NSString *)matterId Replyer:(NSString *)replyer Content:(NSString *)content Success:(SuccessBlock)success Failure:(ErrorBlock)failure Wisdom:(BOOL)isWisdom{
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"matterId" : matterId ,
                                     @"content" : content,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    if (replyer) {
        [params setValue:replyer forKey:@"replyer"];
    }
    if (content) {
        [params setValue:content forKey:@"content"];
    }
    
    NSString *path = PATH_DynStateCommentPublication;
    if (isWisdom) {
        path = PATH_WisdomCommentPublication;
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:path params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (model.code == 0) {
            HRLog(@"发布评论 - 数据发送成功");
        }else{
            HRLog(@"发布评论 - 网络请求成功 - 数据发送失败");
        }
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"动态评论 - 数据获取失败");
        failure(errorInfo);
    }];
    
}

+ (void)publichReplayDyanmicStateWithMatterId:(NSString *)matterId Summary:(NSString *)summary Success:(BaseModelBlock)success Failure:(ErrorBlock)failure{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"matterId" : matterId ,
                                     @"summary" : summary,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ReplyDynamicState params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (model.code == 1) {
            HRLog(@"转发动态失败 - 数据获取失败");
        }else{
            HRLog(@"转发动态成功");
        }
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"转发动态失败 - 网络请求失败");
    }];
    
}




+ (void)requestDynCompanyDetialContentWithCompanyId:(NSString *)companyId Success:(CompantDetialModelBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{@"id" : companyId} mutableCopy];
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynDetialCompany params:params success:^(id result) {
        ICN_DynCompantDetialModel *model = [[ICN_DynCompantDetialModel alloc] initWithDictionary:result error:nil];
        if (model.code == 0) {
            // 获取数据正常
            ICN_DynCompantDetialModel *detialModel = [[ICN_DynCompantDetialModel alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
            detialModel.code = model.code;
            success(detialModel);
        }else{
            success(model);
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestAddCompanyAttentionWithCompanyId:(NSString *)companyId Success:(BaseModelBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{@"enterpriseId" : companyId} mutableCopy];
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynAddCompanyAttention params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestDeleteCompanyAttentionWithCompanyId:(NSString *)companyId Success:(BaseModelBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{@"enterpriseId" : companyId} mutableCopy];
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynDeleteCompanyAttention params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestDynamicLocationSuccess:(LocationsBlock)success Failure:(ErrorBlock)failure{
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_Location params:nil success:^(id result) {
        NSMutableArray<ICN_LocationModel *> *resultArr = [NSMutableArray array];
        ICN_LocationModel * firstModel = [[ICN_LocationModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *temp in tempArr) {
                ICN_LocationModel *model = [[ICN_LocationModel alloc] initWithDictionary:temp error:nil];
                model.code = firstModel.code;
                [resultArr addObject:model];
            }
        }else{
            [resultArr addObject:firstModel];
        }
        success([NSArray arrayWithArray:resultArr]);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

@end
