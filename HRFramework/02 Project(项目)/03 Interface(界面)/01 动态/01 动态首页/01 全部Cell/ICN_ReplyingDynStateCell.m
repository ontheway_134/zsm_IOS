//
//  ICN_ReplyingDynStateCell.m
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ReplyingDynStateCell.h"
#import "ICN_ReplyingDynStateSummaryView.h"
#import "ICN_DynStateContentModel.h"

@interface ICN_ReplyingDynStateCell ()

@property (weak, nonatomic) IBOutlet UILabel *p_ReplayingSubmitLabel;

@property (weak, nonatomic) IBOutlet UIView *p_ReplayContentBackGround;

@property (nonatomic , strong)ICN_ReplyingDynStateSummaryView *replyView; // 回复页面

@end


@implementation ICN_ReplyingDynStateCell

- (ICN_ReplyingDynStateSummaryView *)replyView{
    if (_replyView == nil) {
        _replyView = XIB(ICN_ReplyingDynStateSummaryView);
        CGRect frame = self.p_ReplayContentBackGround.bounds;
        _replyView.frame = frame;
        [self.p_ReplayContentBackGround addSubview:_replyView];
    }
    return _replyView;
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    self.replyView.p_DynStateContentLabel.text = _model.content;
    // 回复内容不确定添加转发说明
    NSArray *picArr = [_model.pic componentsSeparatedByString:@","];
    NSString *picPath ;
    if ([picArr.firstObject containsString:@"."]) {
        picPath = ICN_IMG(picArr.firstObject);
        [self.replyView.p_DynStateIcon sd_setImageWithURL:[NSURL URLWithString:picPath] placeholderImage:[UIImage imageNamed:@"占位图"]];
    }
    self.replyView.p_DynStateIcon.clipsToBounds = YES;
    if (model.amemberNick) {
        self.replyView.p_UserMarkedLabel.text = SF(@"@%@",_model.amemberNick);
    }else{
        self.replyView.p_UserMarkedLabel.text = @"";
    }
    if (_model.aworkStatus != nil && _model.acompanyName != nil) {
        self.replyView.p_PromulgatorLabel.text = SF(@"%@|%@",_model.acompanyName,_model.amemberPosition);
    }else{
        self.replyView.p_PromulgatorLabel.text = @"";
    }
    self.p_ReplayingSubmitLabel.text = _model.summary;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // 添加回复控件
    CGRect frame = self.p_ReplayContentBackGround.bounds;
    _replyView.frame = frame;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
