//
//  ICN_DynStateADModel.h
//  ICan
//
//  Created by albert on 2016/12/17.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynStateADModel : BaseOptionalModel

@property (nonatomic , copy)NSString *adID; // 广告ID
@property (nonatomic , copy)NSString *genre; // 广告类型0文字链接1图文模式
@property (nonatomic , copy)NSString *content; // 内容
@property (nonatomic , copy)NSString *pic; // 图片路径
@property (nonatomic , copy)NSString *url; // 链接地址
@property (nonatomic , copy)NSString *memberId; // 企业用户ID
@property (nonatomic , copy)NSString *memberNick; // 企业名称
@property (nonatomic , copy)NSString *memberLogo; // 企业头像









@end
