//
//  ICN_DynStateADModel.m
//  ICan
//
//  Created by albert on 2016/12/17.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateADModel.h"

@implementation ICN_DynStateADModel

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"adID": @"id"} ];
}


@end
