//
//  ICN_DynStateContentModel.h
//  ICan
//
//  Created by albert on 2016/12/17.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynStateContentModel : BaseOptionalModel

#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)CGFloat imageFooterHeight; // 带图片的尾视图高度
@property (nonatomic , assign , getter=isSpread)BOOL contentSpread; // 文本是否延展
@property (nonatomic , assign)CGFloat contentSpreadHeight; // 文本展开的高度
@property (nonatomic , assign , getter=isLikeUp)BOOL likeUp; // 是否被点赞属性
@property (nonatomic , assign , getter=isWidsom)BOOL widsom; // 是否是智讯



@property (nonatomic , copy)NSString *amemberNick; // 动态作者昵称
@property (nonatomic , copy)NSString *summary; // 转发说明
@property (nonatomic , copy)NSString *aworkStatus; // 作者用户工作状态
@property (nonatomic , copy)NSString *acompanyName; // 作者单位名称/企业名称
@property (nonatomic , copy)NSString *amemberPosition; // 作者职位
@property (nonatomic , copy)NSString *amemberSchool; // 作者所在学校
@property (nonatomic , copy)NSString *amemberMajor; // 作者所学专业
@property (nonatomic , copy)NSString *amemberProfession; // 转发者行业


@property (nonatomic , copy)NSString *type; // 类型 0：动态，1：智讯
@property (nonatomic , copy)NSString *createDate; // 动态发布时间(时间戳)
@property (nonatomic , copy)NSString *memberId; // 用户ID
@property (nonatomic , copy)NSString *matterId; // 动态编号
@property (nonatomic , copy)NSString *authorId; // 动态作者ID
@property (nonatomic , copy)NSString *isTransmit; // 是否为转发：0否，1是
@property (nonatomic , copy)NSString *DynID; // 动态ID(获取数据中的id)
@property (nonatomic , copy)NSString *content; // 动态内容
@property (nonatomic , copy)NSString *pic; // 动态图片用逗号分隔
@property (nonatomic , copy)NSString *praiseCount; // 点赞数
@property (nonatomic , copy)NSString *transmitCount; // 转发数
@property (nonatomic , copy)NSString *commentCount; // 评论数
@property (nonatomic , copy)NSString *roleId; // 用户角色：1普通，2企业
@property (nonatomic , copy)NSString *memberNick; // 用户昵称
@property (nonatomic , copy)NSString *memberLogo; // 用户头像/企业LOGO
@property (nonatomic , copy)NSString *workStatus; // 用户就业情况：0未就业，1已就业
@property (nonatomic , copy)NSString *companyName; // 用户单位名称/企业名称
@property (nonatomic , copy)NSString *memberPosition; // 用户职位
@property (nonatomic , copy)NSString *memberProfession; // 用户行业
@property (nonatomic , copy)NSString *memberSchool; // 用户所在学校
@property (nonatomic , copy)NSString *memberMajor; // 用户所学专业
@property (nonatomic , copy)NSString *plusv; // 加V认证0否；1是
@property (nonatomic , copy)NSString *title; // 智讯标题


@end
