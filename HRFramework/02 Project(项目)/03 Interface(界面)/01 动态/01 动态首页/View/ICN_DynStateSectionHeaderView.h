//
//  ICN_DynStateSectionHeaderView.h
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_DynDetialModel.h"
@class ICN_DynStateContentModel; // 动态详情Model


typedef void(^TapBlock)(ICN_DynStateContentModel *model , BOOL isHeaderIcon);

@interface ICN_DynStateSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic , strong)ICN_DynStateContentModel *model;
@property (nonatomic , strong)ICN_DynDetialModel *detialModel;
@property (nonatomic , copy)TapBlock block;
/** 是否是转发属性 */
@property (nonatomic , assign , getter=isTransmit)BOOL transmit;

@property (nonatomic , assign , getter=isFirstSection)BOOL firstSection; // 是不是第一个分栏

- (void)callBackWithTapBlock:(TapBlock)block;

@end
