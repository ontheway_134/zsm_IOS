//
//  ICN_DynStateCompanySectionHeader.h
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateADModel;
typedef void(^HeaderTapBlock)(ICN_DynStateADModel *model);
@interface ICN_DynStateCompanySectionHeader : UITableViewHeaderFooterView

@property (nonatomic , strong)ICN_DynStateADModel *model;

@property (nonatomic , copy)HeaderTapBlock block;

- (void)callBackWithHeaderTapBlock:(HeaderTapBlock)block;


@end
