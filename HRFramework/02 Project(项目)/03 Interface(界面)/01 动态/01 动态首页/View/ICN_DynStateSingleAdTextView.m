//
//  ICN_ DynStateSingleAdTextView.m
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateSingleAdTextView.h"
#import "ICN_DynStateADModel.h"

@interface ICN_DynStateSingleAdTextView ()

@property (weak, nonatomic) IBOutlet UILabel *p_AdContentLabel;
@property (nonatomic , strong)UITapGestureRecognizer *contentViewTap;


@end

@implementation ICN_DynStateSingleAdTextView

- (UITapGestureRecognizer *)contentViewTap{
    if (_contentViewTap == nil) {
        _contentViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnContentView:)];
        [self addGestureRecognizer:_contentViewTap];
    }
    return _contentViewTap;
}

- (void)callBackWithTextTapBlock:(TextTapBlock)block{
    self.adTextBlock = block;
}

- (void)tapOnContentView:(UITapGestureRecognizer *)tap{
    if (self.adTextBlock) {
        self.adTextBlock(_model);
    }
}

- (void)setModel:(ICN_DynStateADModel *)model{
    _model = model;
    
    // 执行数据添加的工作
    self.p_AdContentLabel.text = _model.content;
    [self contentViewTap];
}

- (void)dealloc{
    if (_contentViewTap) {
        [self.contentView removeGestureRecognizer:_contentViewTap];
        _contentViewTap = nil;
    }
}

@end
