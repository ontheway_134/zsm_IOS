//
//  ICN_DynStateSectionImagesFooter.m
//  ICan
//
//  Created by albert on 2016/12/1.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynStateSectionNormalFooter.h"
#import "ICN_DynStateContentModel.h"

/** 标准高度45.5 */

@interface ICN_DynStateSectionNormalFooter ()

@property (weak, nonatomic) IBOutlet UILabel *p_CommentNumberLabel; // 评论数量标签

@property (weak, nonatomic) IBOutlet UILabel *p_LikeCountLabel; // 点赞数量标签
@property (weak, nonatomic) IBOutlet UIImageView *p_LikeIcon;




@end

@implementation ICN_DynStateSectionNormalFooter

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    
    // 执行数据添加的工作
    self.p_CommentNumberLabel.text = _model.commentCount;
    self.p_LikeCountLabel.text = _model.praiseCount;
    if (self.model.isLikeUp) {
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞"];
    }else{
        self.p_LikeIcon.image = [UIImage imageNamed:@"点赞未选"];
    }

}

- (void)callBackWithBtnBlock:(CellBtnBlock)block{
    self.block = block;
}

- (IBAction)cellBtnClickAction:(UIButton *)sender {
    if (self.block) {
        self.block(self.model , sender.tag);
    }
}


@end
