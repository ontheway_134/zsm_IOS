//
//  HRNetworkingManager+SearchFirstPager.h
//  ICan
//
//  Created by albert on 2016/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager.h"
#import "ICN_HotDetialModel.h"

@class ICN_HotWorldsModel;
@class ICN_DynStateContentModel;

typedef void(^hotWordsRequest)(ICN_HotWorldsModel *model);
typedef void(^dynamicStateRequest)(NSMutableArray <ICN_DynStateContentModel *> *array);

@interface HRNetworkingManager (SearchFirstPager)


/** 获取到搜索热词的网络接口 */
+ (void)requestHotWorldsWithHotType:(NSInteger)hotType
                            Success:(hotWordsRequest)success
                            Failure:(ErrorBlock)failure;

/** 根据搜索内容获取到动态列表的网络接口 */
+ (void)requestDynamicStateListWithSearchContent:(NSString *)search
                                            Page:(NSInteger)page
                                            Size:(NSInteger)size
                                         Success:(dynamicStateRequest)success
                                         Failure:(ErrorBlock)failure;

@end
