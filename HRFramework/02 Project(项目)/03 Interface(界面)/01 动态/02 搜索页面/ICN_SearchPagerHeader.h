//
//  ICN_SearchPagerHeader.h
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#ifndef ICN_SearchPagerHeader_h
#define ICN_SearchPagerHeader_h

#pragma mark - ---------- 热词相关 ----------

static const NSInteger ICN_HotWorldBaseTag = 10000; // 热词按钮初始tag

static const CGFloat ICN_ItemHorzantalSpace = 16.0; // 控件间横向间距
static const CGFloat ICN_ItemVerticalSpace = 10.0; // 控件间纵向间距
static const CGFloat ICN_ItemLeftInset = 10.0; // 控件左边距
static const CGFloat ICN_ItemTopInset = 16.0; // 控件顶边距
static const CGFloat ICN_ItemBottomInset = 16.0; // 控件底边距
#define ICN_SearchPagerHotWorldItemSize CGSizeMake(60.0, 26.0) // 搜索页面热词控件的尺寸


#pragma mark - ---------- 搜索跳转宏 ----------

typedef enum : NSUInteger {
    Search_People = 102003001, // 找人 102003001
    Search_WorkJob, // 找工作 102003002
    Search_DynamicState, // 找动态 102003003
    Search_Group, // 找小组 102003004
    Search_Activity, // 找活动 102003005
    Search_WisdomState, // 找智讯 102003006
    Search_ComplainState, // 找吐槽 102003007
    Search_QuestionState, // 找提问 102003008
    Search_CareerPlan, // 找职场规划 102003009
    Search_InsdurtyInformation, // 找行业资讯 102003010
} Search_ButtonType;




#endif /* ICN_SearchPagerHeader_h */
