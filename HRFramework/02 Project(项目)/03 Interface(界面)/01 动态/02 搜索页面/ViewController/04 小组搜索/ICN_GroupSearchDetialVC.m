//
//  ICN_GroupSearchDetialVC.m
//  ICan
//
//  Created by albert on 2017/1/20.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_GroupSearchDetialVC.h"
#import "ICN_NewFridentTableViewCell.h" // 小组列表Cell
#import "ICN_GroupDetialViewController.h" // 小组详情页面
#import "ICN_DynGrooupListModel.h" // 小组列表Model


@interface ICN_GroupSearchDetialVC ()<UITableViewDelegate , UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic , strong)NSMutableArray *modelsArr;
@property (nonatomic , assign)NSInteger currentPage;
@property (weak, nonatomic) IBOutlet UITextField *searchTextF;

@end

@implementation ICN_GroupSearchDetialVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenDefaultNavBar = YES;
    self.currentPage = 1;
    [self configTableViewWhileViewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- configUI ----------

- (void)configTableViewWhileViewDidLoad{
    self.tableView.rowHeight = 74;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_NewFridentTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_NewFridentTableViewCell class])];
    [self configTableViewRefreshHeaderFooterView];
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            self.currentPage = 1;
            [self requestSearchWithContent:self.content Page:self.currentPage];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.currentPage ++;
        [self requestSearchWithContent:self.content Page:self.currentPage];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}

#pragma mark - ---------- IBAction ----------

- (IBAction)searchContentBtnAction:(UIButton *)sender {
    
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""]) {
        // 搜索有效内容
        self.content = self.searchTextF.text;
        if (self.tableView.hidden == YES) {
            self.tableView.hidden = NO;
        }
        [self.tableView.mj_header beginRefreshing];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请搜索有效内容"];
    }
    
}

- (IBAction)clickOnBackAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - ---------- 代理 ----------


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    NSString *groupId = [self.modelsArr[indexPath.row] groupId];
    ICN_GroupDetialViewController *pager = [[ICN_GroupDetialViewController alloc] init];
    pager.groupId = groupId;
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_NewFridentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_NewFridentTableViewCell class])];
    cell.searchModel = self.modelsArr[indexPath.row];
    cell.TureBtn.hidden = YES;
    cell.FalseBtn.hidden = YES;
    
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

#pragma mark - ---------- 网络请求 ----------

- (void)requestSearchWithContent:(NSString *)content Page:(NSInteger)page{
    if (content) {
        self.currentPage ++;
    }
    NSString *token ;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        // 用户存在
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }else{
        // 用户不存在跳转到引导页面
    }
    if (self.content == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加有效搜索内容"];
    }
    NSDictionary *param = @{
                            @"token":token,
                            @"cont":self.content,
                            @"all":@"1",
                            @"page":SF(@"%ld",(long)page)
                            };
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_SearchGROUPList params:param success:^(id result) {
        [self endRefreshWithTableView:self.tableView];
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 获取数据成功
            if (self.tableView.hidden == YES) {
                self.tableView.hidden = NO;
            }
            if (self.modelsArr == nil || page == 1) {
                self.modelsArr = [NSMutableArray array];
            }
            for (NSDictionary *tempDic in [result valueForKey:@"result"]) {
                ICN_DynGrooupListModel *model = [[ICN_DynGrooupListModel alloc] initWithDictionary:tempDic error:nil];
                [self.modelsArr addObject:model];
            }
            [self.tableView reloadData];
            if (self.modelsArr.count == 0) {
                self.tableView.hidden = YES;
            }
            
        }else{
            self.tableView.hidden = YES;
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:firstModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络连接失败"];
        self.tableView.hidden = YES;
        [self endRefreshWithTableView:self.tableView];
    }];
}

@end
