//
//  ICN_DynSearchDetialVC.m
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynSearchDetialVC.h"
#import "ICN_SearchDynamicListViewModel.h"
#import "ICN_UserDynamicStateDetialVC.h" // 用户动态详情Cell
#import "ICN_CommonPsersonDynamicCell.h" // 动态Cell
#import "ICN_DynStateContentModel.h" // 动态Model
#import "ICN_DynWarnView.h" // 通用提示窗口
#import "ICN_ReviewPublication.h" // 转发页面
#import "ICN_ApplyModel.h"
#import "ICN_ShareManager.h"
#import "ICN_UserReportDetialVC.h" // 举报页面

@interface ICN_DynSearchDetialVC ()<UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate , SearchDynamicStatesListDelegate , ICN_DynWarnViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextF;

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (nonatomic , strong)ICN_SearchDynamicListViewModel *viewModel;

@property (nonatomic , strong)ICN_DynStateContentModel *replyModel; // 用于记录转发的Model

@property (nonatomic , strong)ICN_DynWarnView *warnView; // 通知窗口 - 用于转发的操作


@end

@implementation ICN_DynSearchDetialVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = [UIScreen mainScreen].bounds;
        _warnView.frame = frame;
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}


- (ICN_SearchDynamicListViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_SearchDynamicListViewModel alloc] init];
        _viewModel.delegate = self;
    }
    return _viewModel;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setHiddenDefaultNavBar:YES];
    [self viewModel];
    [self warnView];
    [self configSearchBarWhileViewWillAppear];
    [self configTableViewWhileViewWillAppear];
    [self configTableViewRefreshHeaderFooterView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

- (void)configTableViewWhileViewWillAppear{
    self.tableview.delegate = self;
    self.tableview.dataSource = self;
    [self.tableview registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonPsersonDynamicCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
}

- (void)configSearchBarWhileViewWillAppear{
    self.searchTextF.delegate = self;
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self.viewModel requestWithSearchContent:self.searchContent Load:NO Refresh:YES];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableview.mj_header = header;
    
    self.tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.viewModel requestWithSearchContent:self.searchContent Load:YES Refresh:NO];
    }];
    if (self.searchContent) {
        [self.tableview.mj_header beginRefreshing];
    }
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}



#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnBackAction:(UIButton *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clockOnSearchBtnAction:(UIButton *)sender {
    // 调用显示加的搜索内容数据
    [self.view endEditing:YES];
    [self.searchTextF resignFirstResponder];
    if (self.searchTextF.text != nil && ![self.searchTextF.text isEqualToString:@""] && ![self.searchTextF.text isEqualToString:@"搜索关键字"]) {
        [self.tableview.mj_header beginRefreshing];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入搜索内容"];
    }

}


#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text != nil && ![textField.text isEqualToString:@""] && ![textField.text isEqualToString:@"搜索关键字"]) {
        self.tableview.hidden = NO;
        self.searchContent = textField.text;
        [textField resignFirstResponder];
        [self.tableview.mj_header beginRefreshing];
    }
    return YES;
}

#pragma mark --- ICN_DynWarnViewDelegate ---

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
            pager.model = self.replyModel;
            self.replyModel = nil;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            NSDictionary *params = @{@"matterId":self.replyModel.DynID};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
            
        case ICN_ShareToWeiFridBtnType:{
            
            NSLog(@"分享到微信朋友圈");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            
            NSLog(@"分享到QQ空间");
            NSDictionary *params = @{@"matterId":self.replyModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
   
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        default:
            [self.warnView removeFromSuperview];
            break;
    }
}

- (void)cofigShareContent{
    if (self.replyModel.title == nil || [self.replyModel.title isEqualToString:@""]) {
        title = self.replyModel.memberNick;
    }else{
        title = self.replyModel.memberNick;
    }
    if (self.replyModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replyModel.memberLogo);
    }
    if (self.replyModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replyModel.summary == nil || [self.replyModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replyModel.summary;
        }
    }else{
        if (self.replyModel.content == nil || [self.replyModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replyModel.content;
        }
    }
    
}

#pragma mark --- SearchDynamicStatesListDelegate ---

- (void)responseWithLikeUpRequestSuccess:(BOOL)success Error:(NSString *)error{
    if (success) {
        [self.tableview reloadData];
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseSearchListRequestWithSuccess:(BOOL)success Error:(NSString *)error{
    
    [self endRefreshWithTableView:self.tableview];
    if (success) {
        if (self.viewModel.modelsArr.count > 0) {
            self.tableview.hidden = NO;
            [self.tableview reloadData];
        }else{
            self.tableview.hidden = YES;
        }
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
    
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 - 跳转到动态详情页面
    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
    pager.model = self.viewModel.modelsArr[indexPath.row];
    pager.model.matterId = pager.model.DynID;
    [self currentPagerJumpToPager:pager];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.modelsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_CommonPsersonDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonPsersonDynamicCell class])];
    cell.model = self.viewModel.modelsArr[indexPath.row];
    // 点击Cell的下拉按钮
    // 添加对于Cell上点击事件的处理
    [cell callWhileCellBtnClick:^(NSInteger SenderTag , ICN_DynStateContentModel *model) {
        switch (SenderTag) {
            case ICN_CellLikeActionBtnType:{
                model.likeUp = !model.likeUp;
                NSInteger likeType = 0;
                if (model.isLikeUp) {
                    likeType = 1;
                }else{
                    likeType = 2;
                }
                [self.viewModel likeUpWithType:likeType Model:model];
                break;
            }
            case ICN_CellCommentBtnType:{
                //相关逻辑 - 跳转到动态详情页面
                ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                pager.model = self.viewModel.modelsArr[indexPath.row];
                pager.model.matterId = pager.model.DynID;
                [self currentPagerJumpToPager:pager];
                break;
            }
            case ICN_CellReviewBtnType:{
                // 转发
                self.replyModel = model;
                [self.view addSubview:self.warnView];
                break;
            }
            case ICN_CellPullBtnType:{
                [self.tableview reloadData];
                break;
            }
            case ICN_CellReportBtnType:{
                // 执行举报按钮相关操作
                ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                pager.matterId = model.matterId;
                [self currentPagerJumpToPager:pager];
                break;
            }
            default:
                break;
        }
    }];
    [cell setListStyle:YES];
    return cell;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DynStateContentModel *model = self.viewModel.modelsArr[indexPath.row];
    
    if (model.contentSpread == NO) {
        if (model.imageFooterHeight > 0) {
            return 230 + model.imageFooterHeight;
        }
        if (model.contentSpreadHeight > 57.5) {
            return 230;
        }
    }
    
    if (model.imageFooterHeight > 0) {
        return 230 + model.imageFooterHeight + model.contentSpreadHeight - 57.5;
    }else{
        return 230 + model.contentSpreadHeight - 57.5;
    }
    
    return 230.0;
}


@end
