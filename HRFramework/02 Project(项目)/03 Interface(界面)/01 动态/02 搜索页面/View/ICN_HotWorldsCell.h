//
//  ICN_HotWorldsCell.h
//  ICan
//
//  Created by albert on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_HotWorldsModel;
typedef void(^CellResponseBlock)(ICN_HotWorldsModel *model); // 选中的Cell以及对应的Model
@interface ICN_HotWorldsCell : UITableViewCell

@property (nonatomic , retain)ICN_HotWorldsModel *model; // Model
@property (nonatomic , copy)CellResponseBlock block; // 点击热词响应的block



- (void)callBackWithBtnClickAction:(CellResponseBlock)block;

@end
