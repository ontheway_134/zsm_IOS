//
//  ICN_LocationModel.h
//  ICan
//
//  Created by albert on 2017/1/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@protocol ICN_CityDetialModel <NSObject>
@end

@interface ICN_LocationModel : BaseOptionalModel

@property (nonatomic , copy)NSString *provinceId;
@property (nonatomic , copy)NSString *className;
@property (nonatomic , strong)NSArray<ICN_CityDetialModel> *list;

@end


@interface ICN_CityDetialModel : BaseOptionalModel

@property (nonatomic , copy)NSString *cityId;
@property (nonatomic , copy)NSString *className;

@end
