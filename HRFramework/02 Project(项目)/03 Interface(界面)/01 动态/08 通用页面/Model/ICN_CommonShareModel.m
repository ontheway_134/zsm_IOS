//
//  ICN_CommonShareModel.m
//  ICan
//
//  Created by albert on 2017/2/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_CommonShareModel.h"

@implementation ICN_CommonShareModel

- (instancetype)initWithTitle:(NSString *)title Detial:(NSString *)detial ImageUrl:(NSString *)imageUrl{
    self = [super init];
    if (self) {
        if (title != nil && ![title isEqualToString:@""]) {
            _title = title;
        }else{
            _title = @"i行资讯";
        }
        if (detial != nil && ![detial isEqualToString:@""]) {
            _detial = detial;
        }else{
            _detial = @"i行资讯详情";
        }
        if (imageUrl != nil && ![imageUrl isEqualToString:@""]) {
            _imageUrl = imageUrl;
        }
    }
    return self;
    
}

@end
