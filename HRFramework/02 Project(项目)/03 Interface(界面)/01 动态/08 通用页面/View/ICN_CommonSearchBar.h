//
//  ICN_CommonSearchBar.h
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CommonSearchBarDelegate <NSObject>

@optional
- (void)searchBarDidBeginEditingWithText:(NSString *)content; // 搜索栏里面的当前内容

@optional
- (void)searchBarDidBeginReturnWithText:(NSString *)content; // 在搜索栏点击回车后回调显示的文本

@end

@interface ICN_CommonSearchBar : UIView

@property (nonatomic , weak)id<CommonSearchBarDelegate> delegate;


@end
