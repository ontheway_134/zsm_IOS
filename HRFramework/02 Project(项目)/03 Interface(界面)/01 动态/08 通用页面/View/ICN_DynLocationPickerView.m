//
//  ICN_DynLocationPickerView.m
//  ICan
//
//  Created by albert on 2017/1/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DynLocationPickerView.h"
#import "ICN_LocationModel.h"

@interface ICN_DynLocationPickerView ()<UIPickerViewDelegate , UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *locationPickerView; // 地理选择器视图
@property (nonatomic , strong) ICN_LocationModel *selectedModel;

@property (nonatomic , assign)NSInteger selectCityCode; // 选中城市的代码

@end

@implementation ICN_DynLocationPickerView


#pragma mark - ---------- 初始化方法 ----------

+ (instancetype)loadXibWithCurrentBound:(CGRect)bound{
    ICN_DynLocationPickerView *view = XIB(ICN_DynLocationPickerView);
    view.locationPickerView.delegate = view;
    view.locationPickerView.dataSource = view;
    view.selectCityCode = -1;
    view.bounds = bound;
    return view;
}

- (void)setLocationsArr:(NSArray *)locationsArr{
    _locationsArr = locationsArr;
    self.selectedModel = self.locationsArr.firstObject;
}


#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnLocationFouncBtnAction:(UIButton *)sender {
    
    if (sender.tag == ICN_PickerViewCancerBtn) {
        self.hidden = YES;
        [self.delegate responseCityLocationSelectedWithSelecCityCode:-2 Success:NO];
    }else{
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(responseCityLocationSelectedWithSelecCityCode:Success:)]) {
                if (self.selectCityCode != -1) {
                    [self.delegate responseCityLocationSelectedWithSelecCityCode:self.selectCityCode Success:YES];
                }else{
                    [self.delegate responseCityLocationSelectedWithSelecCityCode:-1 Success:NO];
                }
            }
        }
    }
    self.selectCityCode = -1;
}

#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0) {
        return self.locationsArr.count;
    }else{
        return self.selectedModel.list.count;
    }
    return 0;
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3.0;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0) {
        ICN_LocationModel *model = self.locationsArr[row];
        return model.className;
    }else{
        return [self.selectedModel.list[row] className];
    }
    
    return nil;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // 添加选中行的字段内容
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0) {
        // 点击选中省一栏的内容
        self.selectedModel = self.locationsArr[row];
        [self.locationPickerView reloadComponent:1];
        [self.locationPickerView selectRow:0 inComponent:1 animated:YES];
        self.selectCityCode = [[self.selectedModel.list.firstObject cityId] integerValue];
    }
    else {
        // 点击选中市一栏的内容
        ICN_CityDetialModel *model = self.selectedModel.list[row];
        self.selectCityCode = model.cityId.integerValue;
    }
    
}




@end
