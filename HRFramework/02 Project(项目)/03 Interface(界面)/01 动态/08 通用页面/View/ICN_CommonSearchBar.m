//
//  ICN_CommonSearchBar.m
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CommonSearchBar.h"

@interface ICN_CommonSearchBar ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *p_ContentSearchTextF; // 搜索用文本框


@end

@implementation ICN_CommonSearchBar


- (void)awakeFromNib{
    [super awakeFromNib];
    self.p_ContentSearchTextF.delegate = self;
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextFieldDelegate ---

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(searchBarDidBeginEditingWithText:)]) {
            [self.delegate searchBarDidBeginEditingWithText:textField.text];
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text != nil) {
        // 确定有搜索内容
        if (self.delegate) {
            if ([self.delegate respondsToSelector:@selector(searchBarDidBeginReturnWithText:)]) {
                [self.delegate searchBarDidBeginReturnWithText:textField.text];
            }
        }
    }
    [textField resignFirstResponder];
    return YES;
}


@end
