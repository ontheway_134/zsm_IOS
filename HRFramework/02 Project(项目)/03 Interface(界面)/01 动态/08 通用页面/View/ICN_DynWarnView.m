//
//  ICN_DynWaarnView.m
//  ICan
//
//  Created by albert on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynWarnView.h"

#define ICN_BtnListTitleArr @[@"i行动态", @"i行好友", @"微信", @"微博" , @"QQ" ,@"朋友圈" ,@"QQ空间"]

@interface ICN_DynWarnView ()

#pragma mark --- IBProperty ---

@property (weak, nonatomic) IBOutlet UIButton *dynamicStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *wisdomBtn;
@property (weak, nonatomic) IBOutlet UIButton *complainBtn;
@property (weak, nonatomic) IBOutlet UIButton *askQuestionBtn;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;



@property (nonatomic , strong)UIScrollView *btnListScroll; // 当按钮数量过多时候的按钮scroll
@property (nonatomic , strong)NSMutableArray<UIButton *> *btnListArr; // 按钮列表数组
@property (nonatomic , strong)NSMutableArray<UILabel *> *btnLabelListArr; // 按钮label列表数组

@property (nonatomic , strong)UIView *backView; // 背景view

#pragma mark - ---------- 约束 ----------

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *p_ItemBackGroundBottomInsetConstraint; // 控件背景板距离底部约束



@end

@implementation ICN_DynWarnView

#pragma mark - ---------- 懒加载 ----------

- (UIScrollView *)btnListScroll{
    if (_btnListScroll == nil) {
        _btnListScroll = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _btnListScroll.userInteractionEnabled = YES;
        _btnListScroll.backgroundColor = [UIColor whiteColor];
        [self addSubview:_btnListScroll];
    }
    return _btnListScroll;
}

- (NSMutableArray *)btnLabelListArr{
    if (_btnLabelListArr == nil) {
        _btnLabelListArr = [NSMutableArray array];
    }
    return _btnLabelListArr;
}

- (NSMutableArray *)btnListArr{
    if (_btnListArr == nil) {
        _btnListArr = [NSMutableArray array];
    }
    return _btnListArr;
}

#pragma mark - ---------- 初始化方法 ----------

- (instancetype)loadXibWarnViewsWithIcons:(NSArray<UIImage *> *)iconsArr TitleLabels:(NSArray<NSString *> *)titleLabelArrs TabbarHidden:(BOOL)hidden{
    ICN_DynWarnView *view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([ICN_DynWarnView class]) owner:nil options:nil] firstObject];
    
    if (!hidden) {
        view.p_ItemBackGroundBottomInsetConstraint.constant = 44.0;
    }else{
        view.p_ItemBackGroundBottomInsetConstraint.constant = 0.0;
    }
    
    if (iconsArr.count > 2) {
        
        [view btnListScroll];
        UIView *backGroundView = [[UIView alloc] init];
        backGroundView.backgroundColor = [UIColor whiteColor];
        view.backView = backGroundView;
        [view.btnListScroll addSubview:backGroundView];
        NSArray *titleArr = ICN_BtnListTitleArr;
        for (NSInteger i = 0; i < iconsArr.count; i++) {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectZero];
            [button setBackgroundImage:[iconsArr[i] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
            button.tag = ICN_ShareToDynamicStateBtnType + i; // 根据tag获取按钮的含义
            [backGroundView addSubview:button];
            [view.btnListArr addObject:button];
            [button addTarget:view action:@selector(clickOnButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.text = titleArr[i];
            label.userInteractionEnabled = YES;
            label.font = [UIFont systemFontOfSize:12];
            label.textColor = RGB0X(0x333333);
            [backGroundView addSubview:label];
            [view.btnLabelListArr addObject:label];
        }
        
        [view setNeedsLayout];
    }
    return view;
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnButtonAction:(UIButton *)sender {
    
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responsedButtonClickWithBtnType:)]) {
            [self.delegate responsedButtonClickWithBtnType:sender.tag];
        }
    }
    
}

- (IBAction)clickOnHiddenBtnAction:(UIButton *)sender {
    
    self.hidden = YES;
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    if (_btnListArr) {
        
        [self.btnListScroll mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.dynamicStateBtn.mas_top);
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.bottom.equalTo(self.bottomBtn.mas_top);
        }];
        
        [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(self.btnListScroll);
            make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        
        for (NSInteger i = 0; i < self.btnListArr.count; i++) {
            if (i == 0) {
                [self.btnListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.backView.mas_left).offset(10);
                    make.top.equalTo(self.backView.mas_top).offset(25.0);
                    make.size.mas_equalTo(CGSizeMake(40.0, 40.0));
                }];
            }else if (i == self.btnListArr.count - 1){
                [self.btnListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.btnListArr[i - 1].mas_right).offset(35.0);
                    make.top.equalTo(self.btnListArr[i - 1].mas_top);
                    make.size.equalTo(self.btnListArr[i - 1]);
                    make.right.equalTo(self.backView.mas_right).offset(-10);
                }];
            }else{
                [self.btnListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.btnListArr[i - 1].mas_right).offset(35.0);
                    make.top.equalTo(self.btnListArr[i - 1].mas_top);
                    make.size.equalTo(self.btnListArr[i - 1]);
                }];
            }

            [self.btnLabelListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.btnListArr[i].mas_bottom).offset(10.0);
                make.centerX.equalTo(self.btnListArr[i].mas_centerX);
            }];
        }
    }
    
}




@end
