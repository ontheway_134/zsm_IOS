//
//  ICN_UserReportDetialVC.h
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_UserReportDetialVC : BaseViewController

@property (nonatomic , copy)NSString *matterId; // 所举报的动态ID
@property (nonatomic , copy)NSString *memberId; // 所举报的用户的id


@end
