//
//  ICN_UserDynamicStateDetialVC.m
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserDynamicStateDetialVC.h"
#import "ICN_DynStateContentModel.h"
#import "ICN_DynamicStateCommentViewModel.h" // ViewModel头文件
#import "ICN_UserHomePagerVC.h" // 用户主页
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_PicBroswerView.h" // 图片浏览头文件
#import "ICN_ReviewPublication.h" //转发详情页面

#import "ICN_ShareManager.h"
#import "ICN_ApplyModel.h"

#pragma mark - ---------- Cell相关头文件 ----------

#import "ICN_DynStateTextSpreadCell.h" // 动态正文Cell头文件
#import "ICN_DynStateSectionHeaderView.h" // 动态正文sectionheader
#import "ICN_DynStateSectionImagesFooter.h" // 带有图片的动态正文section footer
#import "ICN_DynStateSectionNormalFooter.h" // 不带图片的动态正文section footer
#import "ICN_ComSingleTitleSectionView.h" // 评论内容的section header
#import "ICN_UserReviewCell.h" // 评论Cell
#import "ICN_DynWarnView.h" // 评论弹出视图

static CGFloat const ImageFooterHeight = 60.0; // 当前带有图片的尾视图的基准高度(去掉图片背景图高度后的)
static CGFloat const SpreadContentCellHeight = 40.0; // 当前可展开Cell去掉可展开文本的高度
static CGFloat const SpreadLabelDefaultHeight = 62.5; // 可展开文本Cell的默认高度

@interface ICN_UserDynamicStateDetialVC ()<UITableViewDelegate , UITableViewDataSource , DynamicStateCommentViewModelDelegate , UITextFieldDelegate , ICN_DynWarnViewDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 评论详情的tableView

@property (weak, nonatomic) IBOutlet UITextField *p_ReviewContentTextF; // 评论内容的文本框


#pragma mark - ---------- 内容属性 ----------

@property (nonatomic , retain)NSArray *p_TestModelsArr; // 测试用模型数组
@property (nonatomic , strong)ICN_DynamicStateCommentViewModel *viewModel;
@property (nonatomic , copy)NSString *replyerId; // 回复@的用户ID
@property (nonatomic , strong)ICN_PicBroswerView *pictureView; // 图片浏览器
@property (nonatomic , strong)ICN_DynWarnView *replayView; // 评论弹出视图
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 回复Model

@end

@implementation ICN_UserDynamicStateDetialVC

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = self.view.bounds;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}


#pragma mark - ---------- 懒加载 ----------

- (ICN_PicBroswerView *)pictureView{
    if (_pictureView == nil) {
        _pictureView = [[ICN_PicBroswerView alloc] initWithFrame:SCREEN_BOUNDS];
        _pictureView.hidden = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_pictureView];
    }
    return _pictureView;
}


- (NSArray *)p_TestModelsArr{
    if (_p_TestModelsArr == nil) {
        _p_TestModelsArr = @[
                             @[@1,@"ICN_DynStateSectionHeaderView" ,
                               @"ICN_DynStateTextSpreadCell" ,
                               @"ICN_DynStateSectionImagesFooter" ],
                             @[@3,@"ICN_ComSingleTitleSectionView" ,@"ICN_UserReviewCell"]
                             ];
    }
    return _p_TestModelsArr;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"动态详情";
    self.viewModel = [[ICN_DynamicStateCommentViewModel alloc] init];
    self.viewModel.delegate = self;
    self.viewModel.Wisdom = self.model.isWidsom;
    if (self.viewModel.Wisdom) {
        self.naviTitle = @"智讯详情";
    }
    self.p_ReviewContentTextF.delegate = self;
    // 配置当前页面的tableView
    [self configCurrentPageContentTableView];
    [self configTableViewRefreshHeaderFooterView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            if (self.viewModel.detialModel == nil) {
                [self.viewModel loadDynamicStateDetialWithMatterId:self.model.matterId];
            }
            [self.viewModel refreshCurrentPageContentCellsWithMatterId:self.model.matterId];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.viewModel loadNextPageContentCellsWithMatterId:self.model.matterId];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}


- (void)configCurrentPageContentTableView{
    
    // 签订协议
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    // 注册Cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateTextSpreadCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_DynStateTextSpreadCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserReviewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserReviewCell class])];
    // 注册section
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionHeaderView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionHeaderView class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComSingleTitleSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionImagesFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionImagesFooter class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionNormalFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_DynStateSectionNormalFooter class])];
    // 设置group类型的相关属性
    self.tableView.backgroundView = nil;
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - ---------- IBAction ----------

// 添加评论内容的action
- (IBAction)sendReviewContentAction:(UIButton *)sender {
    if (self.p_ReviewContentTextF.text != nil && ![self.p_ReviewContentTextF.text isEqualToString:@""]) {
        [self.viewModel postDynamicStateCommentContentWithMatterId:self.model.matterId Replyer:self.replyerId Content:self.p_ReviewContentTextF.text];
    }
    self.replyerId = nil;
    self.p_ReviewContentTextF.text = @"";
    [self.viewModel refreshCurrentPageContentCellsWithMatterId:self.model.matterId];
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_DynWarnViewDelegate ---

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

- (void)cofigShareContent{
    if (self.replayModel.title == nil || [self.replayModel.title isEqualToString:@""]) {
        title = self.replayModel.memberNick;
    }else{
        title = self.replayModel.memberNick;
    }
    if (self.replayModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replayModel.memberLogo);
    }
    if (self.replayModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replayModel.summary == nil || [self.replayModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.summary;
        }
    }else{
        if (self.replayModel.content == nil || [self.replayModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.content;
        }
    }
    
}



- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.replayView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
            pager.model = self.replayModel;
            self.replayModel = nil;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
            
            NSLog(@"分享到微信朋友圈");
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            
            break;
        }
        case ICN_ShareToQQFridBtnType:{
            
            NSLog(@"分享到QQ空间");
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
            
        default:
            [self.replayView removeFromSuperview];
            break;
    }
}

- (void)cofigShareContentWithTitle:(NSString *)title IconUrl:(NSString *)iconUrl Content:(NSString *)content{
    if (self.replayModel.title == nil || [self.replayModel.title isEqualToString:@""]) {
        title = self.replayModel.memberNick;
    }else{
        title = self.replayModel.memberNick;
    }
    if (self.replayModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replayModel.memberLogo);
        //                            iconUrl = @"https://mobile.umeng.com/images/pic/home/social/img-1.png";
    }
    if (self.replayModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replayModel.summary == nil || [self.replayModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.summary;
        }
    }else{
        if (self.replayModel.content == nil || [self.replayModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.content;
        }
    }
    
}



#pragma mark --- DynamicStateCommentViewModelDelegate ---

// 获取到用户动态详情时的回调
- (void)responseWithUserDynamicDetial{
    
    [self.tableView reloadData];
}

- (void)responseWhileCommentCommitSuccess:(BOOL)success error:(NSString *)error{
    if (success) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"评论提交成功"];
        if ([self.model.commentCount integerValue]) {
            self.model.commentCount = SF(@"%ld",self.model.commentCount.integerValue + 1);
        }
        [self.p_ReviewContentTextF resignFirstResponder];
        [self.tableView.mj_header beginRefreshing];
    }else{
        // 在发送失败的时候将一切内容置空
        [self.p_ReviewContentTextF resignFirstResponder];
        self.p_ReviewContentTextF.text = @"";
        self.replyerId = nil;
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseWithStateCommentModelsArrSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
        [self endRefreshWithTableView:self.tableView];
    }else{
        [self endRefreshWithTableView:self.tableView];
    }
}

- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
    }
}

- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经点过赞了"];
    [self.tableView reloadData];
}

- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您没有权限进行此操作"];
}


#pragma mark --- UITextFieldDelegate ---

- (void)textFieldDidEndEditing:(UITextField *)textField{
    // 在结束编辑的时候将一切评论相关内容置空
    textField.text = @"";
    self.replyerId = nil;
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        // 返回Cell的数量
        return 1;
    }else{
        // 返回评论Cell的数量
        return self.viewModel.modelsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        // 根据Cell的内容确定使用的Cell - 正文或者是转发
        ICN_DynStateTextSpreadCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynStateTextSpreadCell class])];
        cell.model = self.model;
        cell.contentDetial = YES;
        return cell;
    }else{
        // 评论的Cell
        ICN_UserReviewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserReviewCell class])];
        if (indexPath.row == 0) {
            cell.firstCell = YES;
        }else{
            cell.firstCell = NO;
        }
        cell.model = self.viewModel.modelsArr[indexPath.row];
        [cell callBackWithBtnBlock:^(ICN_DynamicStateCommentModel *model, BOOL tap) {
            if (tap) {
                // 点击用户头像的操作
                ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
                pager.memberId = self.model.memberId;
                [self currentPagerJumpToPager:pager];
            }else{
                // 根据Model的数据去评论
                self.replyerId = model.memberId;
                // 使键盘弹出
                [self.p_ReviewContentTextF becomeFirstResponder];
            }
        }];
        return cell;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            // 正文内容
            ICN_DynStateSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionHeaderView class])];
            if (section == 0) {
                view.firstSection = YES;
            }
            view.model = self.model;
            if (self.model.isTransmit.integerValue == 0) {
                view.transmit = NO;
            }else{
                view.transmit = YES;
                if (self.viewModel.detialModel) {
                    view.transmit = NO;
                    self.viewModel.detialModel.imageFooterHeight = self.model.imageFooterHeight;
                    self.viewModel.detialModel.contentSpread = self.model.contentSpread;
                    self.viewModel.detialModel.contentSpreadHeight = self.model.contentSpreadHeight;
                    self.viewModel.detialModel.widsom = self.model.widsom;
                    self.viewModel.detialModel.likeUp = self.model.likeUp;
                    view.detialModel = self.viewModel.detialModel;
                }
            }
            return view;
        }
    }else{
        ICN_ComSingleTitleSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComSingleTitleSectionView class])];
        view.contentView.backgroundColor = RGB0X(0xf0f0f0);
        view.headerTitle = @"评论";
        return view;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            // 当内容为动态的时候有尾视图
            ICN_DynStateContentModel *model = self.model;
            if ([model.pic containsString:@"."]) {
                // 有图片的时候调用有图片的pic;
                ICN_DynStateSectionImagesFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionImagesFooter class])];
                footer.model = model;
                [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            // 根据tag判断点击的按钮
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isLikeUp) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 执行转发相关操作
                            [self.view addSubview:self.replayView];
                            self.replayModel = model;
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            self.replyerId = nil;
                            [self.p_ReviewContentTextF becomeFirstResponder];
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                            pager.matterId = model.matterId;
                            [self currentPagerJumpToPager:pager];
                            break;
                        }
                        default:
                            // 返回的是image的索引值
                            if (self.pictureView.hidden == YES) {
                                NSArray *pathArr = [model.pic componentsSeparatedByString:@","];
                                self.pictureView.urlPathArr = pathArr;
                                self.pictureView.currentIndex = btnTag;
                                self.pictureView.hidden = NO;
                            }
                            break;
                    }
                }];
                return footer;
            }else{
                ICN_DynStateSectionNormalFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_DynStateSectionNormalFooter class])];
                footer.model = model;
                [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            // 根据tag判断点击的按钮
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isLikeUp) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 执行转发相关操作
                            [self.view addSubview:self.replayView];
                            self.replayModel = model;
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            self.replyerId = nil;
                            [self.p_ReviewContentTextF becomeFirstResponder];
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                            pager.matterId = model.matterId;
                            [self currentPagerJumpToPager:pager];
                            break;
                        }
                        default:
                            
                            break;
                    }
                }];

                return footer;
            }
        }
    }
    
    return nil;

}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            ICN_DynStateContentModel *model = self.model;

                if (model.contentSpreadHeight < SpreadLabelDefaultHeight) {
                    return 20.0 + model.contentSpreadHeight;
                }
                return SpreadContentCellHeight + model.contentSpreadHeight;
        }
    }else{
        // 返回评论列表的高度
        return 100.5 + [self.viewModel.modelsArr[indexPath.row] commentLabelHeight];
    }
    return 132.0;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return ICN_DynStateSectionHeaderViewHeight;
    }
    return 35.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        if ([self.model isKindOfClass:[ICN_DynStateContentModel class]]) {
            ICN_DynStateContentModel *model = self.model;
            if (model.imageFooterHeight != 0.0) {
                return ImageFooterHeight + model.imageFooterHeight;
            }else
                return ICN_DynStateSectionNormalFooterHeight;
        }

    }
    return 0.0;
}





@end
