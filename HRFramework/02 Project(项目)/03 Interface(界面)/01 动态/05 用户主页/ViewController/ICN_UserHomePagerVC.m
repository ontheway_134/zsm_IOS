//
//  ICN_UserHomePagerVCViewController.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserHomePagerVC.h"
#import "ICN_UserMoreFounctionListView.h" // 用户的更多功能视图
#import "ICN_AuthorHomePagerViewModel.h"
#import "ICN_AuthorHomePagerHeader.h"
#import "ICN_DynStateContentModel.h"
#import "ICN_DynEducationModel.h"
#import "ICN_DynWorkExperienceModel.h"
#import "ICN_DynGroupModel.h"
#import "ICN_DynActivityModel.h"
#import "ChatViewController.h"
#import "ICN_MyPersonalHeaderModel.h"
#import "ICN_JoinGroupViewController.h"

#pragma mark - ---------- Cell 与 section ----------
//header AND section header
#import "ICN_CommonUserHeaderView.h" // 通用用户头视图
#import "ICN_PsersonalSignatureSectionView.h" // 个性签名 - section头视图
#import "ICN_ComDetialListSectionView.h" // 通用点击进入详情的头视图
#import "ICN_SkillListSectionView.h" // 技能标签section头视图

// cell
#import "ICN_UserCommonCell.h" // 用户界面显示 动态和话题 的Cell
#import "ICN_DynExperienceCell.h" // 用于展示 教育经历和工作经历 的Cell
#import "ICN_CommonActivityItemCell.h" // 参加的活动 的Cell
#import "ICN_GroupListCell.h" // 参加的小组 的Cell

#pragma mark - ---------- 跳转页面头文件 ----------

#import "ICN_UserDynamicStateListVC.h" // 用户动态与话题的列表页面
#import "ICN_UserExperienceListVC.h" // 工作与教育经历列表界面
#import "ICN_UserReportDetialVC.h" // 用户举报详情页面
#import "ICN_CommonFriendsVC.h" // 共同好友列表页面
#import "ICN_ShareHistoryFriendsListVC.h" // 点击推荐给好友跳转的联系人历史信息页面
#import "ICN_ExperiencessListVC.h" // 经验列表的通用页面
#import "ICN_CommonFridentViewController.h" // 共同好友列表页面
#import "ICN_MyGroupViewController.h" // 参加的小组的列表页面
#import "ICN_MyOrderingViewController.h" // 用户的两种订单的pager
#import "ICN_UserMoreActionPager.h" // 点击更多跳转到的页面
#import "ICN_UserPostMsgListPagerViewController.h" // 点击动态；智讯；吐槽；发布跳转的用户发布列表页

@interface ICN_UserHomePagerVC ()<UITableViewDelegate , UITableViewDataSource , CommonUserHeaderViewDelegate , ICN_AuthorHomePagerDelegate>

@property (nonatomic , retain)UITableView *tableView; // 当前页面的tableView
@property (nonatomic , retain)UIButton *sendMessageBtn; // 当前页面的发送消息按钮

@property (nonatomic , retain)UIView * founctionView; // 当前页面的导航栏
@property (nonatomic , retain)ICN_UserMoreFounctionListView *moreFunctionView; // 点击更多按钮的弹窗页面
@property (nonatomic , retain)UILabel *userLabel; // 当前用户名标签

@property (nonatomic , retain)NSDictionary * testDefaultIdenStrDic; // 设置存储默认标识的字典

@property (nonatomic , retain)NSArray * testCellCountArr; // 默认设置的假数据的每个section的Cell对应的section标题和数量

@property (nonatomic , strong)ICN_AuthorHomePagerViewModel *viewModel;
@property (nonatomic , strong)ICN_CommonUserHeaderView *headerView; // 头视图属性
@property (nonatomic , strong)NSMutableArray *defaultSectionKeyArr; // 默认的scection对应的位置和key的可变数组
@property (nonatomic , strong)NSMutableDictionary *defaultSectionKeyContentDic; // 默认的根据key的Cell配置字典
@property (nonatomic , strong)UIButton *moreButton;

@end

@implementation ICN_UserHomePagerVC

#pragma mark - ---------- 懒加载 ----------


- (NSMutableDictionary *)defaultSectionKeyContentDic{
    if (_defaultSectionKeyContentDic == nil) {
        _defaultSectionKeyContentDic = [@{
                                         KEY_SignTitle:@[@0 , @"45.0"],
                                         KEY_SameFriends:@[@0, @"60.0"],
                                         KEY_DynamicStates:@[@2, @"45.0", @"92.5"],
                                         KEY_SkillSigns:@[@0, @"86.0"],
                                         KEY_EduExperience:@[@1, @"48.0", @"102.0"],
                                         KEY_WorkExperience:@[@1, @"48.0", @"102.0"],
                                         KEY_ApplyActivity:@[@2, @"45.0", @"100.0"],
                                         KEY_ApplyGroups:@[@3, @"45.0", @"83.0"],
                                         } mutableCopy];
    }
    return _defaultSectionKeyContentDic;
}

- (NSMutableArray *)defaultSectionKeyArr{
    if (_defaultSectionKeyArr == nil) {
        _defaultSectionKeyArr = [@[KEY_SignTitle , KEY_SameFriends , KEY_DynamicStates , KEY_SkillSigns , KEY_EduExperience , KEY_WorkExperience , KEY_ApplyActivity , KEY_ApplyGroups] mutableCopy];
    }
    return _defaultSectionKeyArr;
}


// 导航栏
- (UIView *)founctionView{
    if (_founctionView == nil) {
        _founctionView = [[UIView alloc] initWithFrame:CGRectZero];
        UIColor *color = RGB0X(0x009cff);
        _founctionView.backgroundColor = [color colorWithAlphaComponent:0.0];
        [self.view addSubview:_founctionView];
        [_founctionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.top.equalTo(self.view);
            make.height.mas_equalTo(64.0);
        }];
        
        UIButton *back = [[UIButton alloc] initWithFrame:CGRectZero];
        [_founctionView addSubview:back];
        [back setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
        [back mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_founctionView);
            make.top.equalTo(_founctionView).offset(22.5);
            make.size.mas_equalTo(CGSizeMake(40, 30));
        }];
        
        UIButton *more = [[UIButton alloc] initWithFrame:CGRectZero];
        [_founctionView addSubview:more];
        [more setImage:[UIImage imageNamed:@"更多-点"] forState:UIControlStateNormal];
        self.moreButton = more;
        [more mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_founctionView);
            make.top.equalTo(_founctionView).offset(22.5);
            make.size.mas_equalTo(CGSizeMake(40, 30));
        }];
        
        _userLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _userLabel.text = @"";
        _userLabel.textColor = [UIColor whiteColor];
        _userLabel.font = [UIFont systemFontOfSize:14.0];
        [_founctionView addSubview:_userLabel];
        [_userLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_founctionView.mas_centerY).offset(5);
            make.centerX.equalTo(_founctionView);
        }];
        
        // 添加方法
        [back addTarget:self action:@selector(clickBackBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [more addTarget:self action:@selector(clickMoreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        more.selected = YES;
    }
    return _founctionView;
}

// 更多按钮的弹窗页面
- (ICN_UserMoreFounctionListView *)moreFunctionView{
    if (_moreFunctionView == nil) {
        _moreFunctionView = XIB(ICN_UserMoreFounctionListView);
        [_moreFunctionView callWithClickStateBlock:^(BOOL isShare, BOOL isReport) {
            // 跳转到举报页面
            if (isReport) {
                ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                pager.memberId = self.memberId;
                [self currentPagerJumpToPager:pager];
            }
            // 跳转到分享页面
            if (isShare) {
//                ICN_ShareHistoryFriendsListVC *pager = [[ICN_ShareHistoryFriendsListVC alloc] init];
//                [self currentPagerJumpToPager:pager];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            }
        }];
        _moreFunctionView.frame = [UIScreen mainScreen].bounds;
    }
    return _moreFunctionView;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds style:UITableViewStyleGrouped];
        
        ICN_CommonUserHeaderView *header = XIB(ICN_CommonUserHeaderView);
        self.headerView = header;
        header.delegate = self;
        header.frame = CGRectMake(0, 0, SCREEN_WIDTH, 200);
        _tableView.tableHeaderView = header;
        _tableView.separatorStyle = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 配置网络请求的刷新方法
        [self configTableViewRefreshHeaderFooterView];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_PsersonalSignatureSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_PsersonalSignatureSectionView class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComDetialListSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        // 添加技能标签的
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_SkillListSectionView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_SkillListSectionView class])];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserCommonCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserCommonCell class])];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_CommonActivityItemCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_CommonActivityItemCell class])];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynExperienceCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
        
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_GroupListCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_GroupListCell class])];
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.sectionFooterHeight = 1.0;
        _tableView.sectionHeaderHeight = 1.0;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            [self.viewModel refreshCurrentPageContentCellsWithMemberId:self.memberId];
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
}


- (UIButton *)sendMessageBtn{
    WEAK(weakSelf)
    if (_sendMessageBtn == nil) {
        _sendMessageBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        _sendMessageBtn.backgroundColor = RGB0X(0x009cff);
        [_sendMessageBtn addAction:^(NSInteger tag) {
            
            
            // 在当前用户token存在的时候将当前用户的token写入
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:token forKey:@"token"];
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getIndexInfo" params:dic success:^(id result) {
                NSDictionary *dict = result[@"result"]
                ;
                ICN_MyPersonalHeaderModel * model = [[ICN_MyPersonalHeaderModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                
                if (model.memberLogo) {
                    [USERDEFAULT setValue:ICN_IMG(model.memberLogo) forKey:ICN_UserIconUrl];
                }
                if (model.memberNick) {
                    [USERDEFAULT setValue:model.memberNick forKey:ICN_UserNick];
                }
    
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            // 根据好友ID获取好友的环信ID从而进入聊天页面
            // 根据获取到的环信Id进行相应的操作
            ChatViewController *chatView = [[ChatViewController alloc] initWithConversationChatter:ICN_HX(weakSelf.memberId) conversationType:EMConversationTypeChat ext:@{@"username":weakSelf.viewModel.userBaseModel.memberNick,@"headUrl":ICN_IMG(weakSelf.viewModel.userBaseModel.memberLogo)}];
            chatView.singleChatInfo = [@{ICN_UserNick:weakSelf.viewModel.userBaseModel.memberNick,ICN_UserIconUrl:weakSelf.viewModel.userBaseModel.memberLogo} mutableCopy];
            chatView.title = weakSelf.viewModel.userBaseModel.memberNick;
            [chatView setHidesBottomBarWhenPushed:YES];
            [weakSelf.navigationController pushViewController:chatView animated:YES];
            
        }];
        [self.view addSubview:_sendMessageBtn];
        [_sendMessageBtn setTitle:@"发消息" forState:UIControlStateNormal];
        _sendMessageBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [_sendMessageBtn setImage:[UIImage imageNamed:@"发消息"] forState:UIControlStateNormal];
//        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//        imageView.image = [UIImage imageNamed:@"发消息"];
//        imageView.userInteractionEnabled = YES;
//        [_sendMessageBtn addSubview:imageView];
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//        label.textColor = [UIColor whiteColor];
//        label.userInteractionEnabled = YES;
//        label.font = [UIFont systemFontOfSize:14.0];
//        label.text = @"发消息";
//        [_sendMessageBtn addSubview:label];
        [_sendMessageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.mas_left);
            make.right.equalTo(self.view.mas_right);
            make.bottom.equalTo(self.view.mas_bottom);
            make.height.mas_equalTo(36.0);
        }];
//        
//        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(_sendMessageBtn.mas_left).offset(ICN_KScreen(128.0));
//            make.centerY.equalTo(_sendMessageBtn);
//        }];
//        
//        [label mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.left.equalTo(imageView.mas_right).offset(5.0);
//            make.centerY.equalTo(imageView);
//        }];
//        
    }
    return _sendMessageBtn;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [[ICN_AuthorHomePagerViewModel alloc] init];
    self.viewModel.delegate = self;
    // 加载用户基本数据的网络请求
    [self.viewModel loadUserBaseMessageWithUserId:self.memberId];
    [self setHiddenDefaultNavBar:YES];
    [self testDefaultIdenStrDic];
    [self testCellCountArr];
    [self tableView];
    [self sendMessageBtn];
    [self founctionView];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

#pragma mark --- ButtonAction ---

- (void)clickBackBtnAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)clickMoreBtnAction:(UIButton *)sender{
    
//    // 弹出弹窗
//    [self.view addSubview:self.moreFunctionView];
    // 现在点击更多按钮的操作变成跳转页面
    ICN_UserMoreActionPager *pager = [[ICN_UserMoreActionPager alloc] init];
    pager.userId = self.memberId;
    [self currentPagerJumpToPager:pager];
   
}


#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_AuthorHomePagerDelegate ---

// 获取到新数据之后的回调
- (void)responseWithModelsDic:(NSMutableDictionary *)modesDic{
    // 根据有几个真实数据去决定各个内容的布局
    _defaultSectionKeyArr = nil;
    NSMutableArray *deleteArr = [NSMutableArray array];
    for (NSString *key in self.defaultSectionKeyArr) {
        if ([self.viewModel.modelsDic valueForKey:key] == nil) {
            // 不存在这种类型的数据则在默认key数组中删除这个key
            [deleteArr addObject:key];
        }
    }
    [self.defaultSectionKeyArr removeObjectsInArray:deleteArr];
    if ([self.defaultSectionKeyArr containsObject:KEY_SkillSigns]) {
        NSMutableArray *array = [NSMutableArray arrayWithArray:[self.defaultSectionKeyContentDic valueForKey:KEY_SkillSigns]];
        [array removeLastObject];
        [array addObject:[NSNumber numberWithFloat:[[[self.viewModel.modelsDic valueForKey:KEY_SkillSigns] firstObject] CellHeight]]];
        [self.defaultSectionKeyContentDic setValue:[NSArray arrayWithArray:array] forKey:KEY_SkillSigns];
    }

    [self.tableView reloadData];
    [self endRefreshWithTableView:self.tableView];
}

// 获取对于好友操作的网络请求之后的回调
- (void)responseWithAddFriend:(BOOL)isAdd Success:(BOOL)success Error:(NSString *)error{
    if (success) {
        // 刷新页面的用户主页数据
        [self.viewModel loadUserBaseMessageWithUserId:self.memberId];
        if (isAdd) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"添加好友成功"];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除好友成功"];
        }
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

// 获取新数据失败之后的返回
- (void)responseNetRequestFailedWithKey:(NSString *)key{
    [self endRefreshWithTableView:self.tableView];
}

// 获取到用户基本信息之后的回调
- (void)responseWithUserBaseMassageRequest:(BOOL)success Error:(NSString *)error{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (success) {
        // 刷新数据操作
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据更新成功"];
        if (self.viewModel.isMe) {
            if (self.moreButton) {
                self.moreButton.hidden = YES;
            }
        }
        //            status=>关系(1:未审核[申请者]  2:已审核   3:被拒绝  4:已删除  5:待审核[被申请者] 6:已拒绝 )[没关系则没有此数据]
        // 根据好友关系判断该页面是否能否显示发消息的按钮以及之后的相关功能
        self.sendMessageBtn.hidden = YES;
        if (self.viewModel.userBaseModel) {
            if (self.viewModel.userBaseModel.staus) {
                if ([self.viewModel.userBaseModel.staus integerValue] == 2) {
                    // 则现在两者为好友关系
                    self.sendMessageBtn.hidden = NO;
                }
            }
        }

        self.headerView.model = self.viewModel.userBaseModel;
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}


#pragma mark --- CommonUserHeaderViewDelegate ---

- (void)responseWithDeleteCurrentFriendAction{
    
    UIAlertController *pager = [UIAlertController alertControllerWithTitle:@"确认删除好友" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmBtn = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 实行删除好友的相关操作
        [self.viewModel requestFriendAdd:NO WithMemberId:self.memberId];
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"添加好友中"];
//        // 成功后的操作
//        ICN_CommonUserHeaderView *view = (ICN_CommonUserHeaderView *)self.tableView.tableHeaderView;
//        [view changeFriendStateSuccessWithAddfirend:NO];
    }];
    UIAlertAction *cancerBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [pager addAction:confirmBtn];
    [pager addAction:cancerBtn];
    [self presentViewController:pager animated:YES completion:nil];
}

- (void)responseWithAddCurrentFriendAction{
    UIAlertController *pager = [UIAlertController alertControllerWithTitle:@"确认添加好友" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirmBtn = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        // 实行添加好友的相关操作
        [self.viewModel requestFriendAdd:YES WithMemberId:self.memberId];
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除好友中"];
//        // 成功后的操作
//        ICN_CommonUserHeaderView *view = (ICN_CommonUserHeaderView *)self.tableView.tableHeaderView;
//        [view changeFriendStateSuccessWithAddfirend:YES];
    }];
    UIAlertAction *cancerBtn = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [pager addAction:confirmBtn];
    [pager addAction:cancerBtn];
    [self presentViewController:pager animated:YES completion:nil];
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.y > 0) {
        CGFloat alphaValue = scrollView.contentOffset.y / 200.0;
        UIColor *color = RGB0X(0x009cff);
        self.founctionView.backgroundColor = [color colorWithAlphaComponent:alphaValue];
        if (alphaValue > 0.7) {
            _userLabel.text = self.viewModel.userBaseModel.memberNick;
        }else{
            _userLabel.text = @"";
        }
    }else{
        self.founctionView.backgroundColor = [UIColor clearColor];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //相关逻辑 -- 添加点击动态的两种逻辑
    NSString *key = self.defaultSectionKeyArr[indexPath.section];
    
    // 测试 跳转到用户动态列表
    if ([key isEqualToString:KEY_DynamicStates]) {
        ICN_UserDynamicStateListVC *pager = [[ICN_UserDynamicStateListVC alloc] init];
        pager.memberId = self.memberId;
        [self currentPagerJumpToPager:pager];
    }
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSString *key = self.defaultSectionKeyArr[section];
    if ([[self.defaultSectionKeyContentDic valueForKey:key] count] > 2 && [[self.viewModel.modelsDic valueForKey:key]count] <= 2) {
        return [[self.viewModel.modelsDic valueForKey:key] count];
    }
    return [[[self.defaultSectionKeyContentDic valueForKey:key] firstObject] floatValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *key = self.defaultSectionKeyArr[indexPath.section];
    NSInteger row = indexPath.row;
    if ([key isEqualToString:KEY_DynamicStates]) {
        // 返回动态的Cell
        ICN_UserCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserCommonCell class])];
        if ([self.viewModel.modelsDic valueForKey:KEY_DynamicStates]) {
            cell.model = [self.viewModel.modelsDic valueForKey:KEY_DynamicStates][row];
        }
        
        return cell;
    }
    if ([key isEqualToString:KEY_EduExperience]) {
        // 返回教育经历的Cell
        ICN_DynExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
        cell.eduModel = [[self.viewModel.modelsDic valueForKey:KEY_EduExperience] objectAtIndex:row];
        return cell;
    }
    if ([key isEqualToString:KEY_WorkExperience]) {
        // 返回工作经历的Cell
        ICN_DynExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_DynExperienceCell class])];
        cell.workModel = [[self.viewModel.modelsDic valueForKey:KEY_WorkExperience] objectAtIndex:row];
        return cell;
    }
    if ([key isEqualToString:KEY_ApplyActivity]) {
        // 返回参加的活动的Cell
        ICN_CommonActivityItemCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_CommonActivityItemCell class])];
        if ([[self.viewModel.modelsDic valueForKey:KEY_ApplyActivity] count] > 0) {
            cell.model = [[self.viewModel.modelsDic valueForKey:KEY_ApplyActivity] objectAtIndex:row];
        }
        return cell;
    }
    if ([key isEqualToString:KEY_ApplyGroups]) {
        // 返回参加的小组的Cell
        ICN_GroupListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_GroupListCell class])];
        if ([[self.viewModel.modelsDic valueForKey:KEY_ApplyGroups] count] > 0) {
            cell.model = [[self.viewModel.modelsDic valueForKey:KEY_ApplyGroups] objectAtIndex:row];
        }

        return cell;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.defaultSectionKeyArr.count;
}

/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    NSString *key = self.defaultSectionKeyArr[section];
    if (self.viewModel.modelsDic.count == 0) {
        return nil;
    }
    if ([key isEqualToString:KEY_SignTitle]) {
        // 添加个性签名视图
        ICN_PsersonalSignatureSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_PsersonalSignatureSectionView class])];
        if (self.viewModel.userBaseModel) {
            view.psersonalSigurature = self.viewModel.userBaseModel.personalizeSignature;
        }
        return view;
    }
    if ([key isEqualToString:KEY_SameFriends]) {
        // 共同好友视图
        ICN_ComDetialListSectionView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        headerView.titleKey = KEY_SameFriends;
        headerView.comfriendsArr = [self.viewModel.modelsDic valueForKey:KEY_SameFriends];
        [headerView callBackWithCommonFriendsBlock:^(NSArray<ICN_CommonFriendsModel *> *array) {
            // 执行点击共同好友列表之后的操作 - 跳转到共同好友列表
            ICN_CommonFridentViewController *pager = [[ICN_CommonFridentViewController alloc] init];
            pager.commonFridentId = self.memberId;
            [self currentPagerJumpToPager:pager];
        }];
        return headerView;
    }
    if ([key isEqualToString:KEY_DynamicStates]) {
        // 动态列表
        ICN_ComDetialListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        view.titleKey = KEY_DynamicStates;
        if ([self.viewModel.modelsDic valueForKey:KEY_DynamicStates]) {
            NSArray <ICN_DynStateContentModel *>*contentArr = [self.viewModel.modelsDic valueForKey:KEY_DynamicStates];
            if (contentArr.firstObject.count >= 0) {
                view.itemCount = contentArr.firstObject.count;
            }
        }
        [view callBackWithDetialBlock:^{
            //test=== 测试用在动态section跳转到用户发布列表页
            ICN_UserPostMsgListPagerViewController *pager = [[ICN_UserPostMsgListPagerViewController alloc] init];
            pager.currentSelectedTag = PRIVATE_TopicBtn;
            [self currentPagerJumpToPager:pager];
//            // 根据当前页面的Model跳转到用户的详情列表
//            ICN_UserDynamicStateListVC *pager = [[ICN_UserDynamicStateListVC alloc] init];
//            pager.memberId = self.memberId;
//            pager.mineDynamic = NO;
//            [self currentPagerJumpToPager:pager];
        }];
        return view;

    }
    if ([key isEqualToString:KEY_SkillSigns]) {
        // 技能标签
        ICN_SkillListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_SkillListSectionView class])];
        if ([self.viewModel.modelsDic valueForKey:KEY_SkillSigns]) {
            view.modelsArr = [self.viewModel.modelsDic valueForKey:KEY_SkillSigns];
        }
        return view;
    }
    if ([key isEqualToString:KEY_EduExperience]) {
        // 教育经历头视图
        ICN_ComDetialListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        view.titleKey = KEY_EduExperience;
        view.itemCount = [[self.viewModel.modelsDic valueForKey:KEY_EduExperience] count];
        [view callBackWithExperienceBlock:^(NSString *experienceKey) {
            ICN_ExperiencessListVC *pager = [[ICN_ExperiencessListVC alloc] init];
            pager.modelsArr = [self.viewModel.modelsDic valueForKey:KEY_EduExperience];
            [self currentPagerJumpToPager:pager];
        }];
        return view;
    }
    if ([key isEqualToString:KEY_WorkExperience]) {
        // 工作经历头视图
        ICN_ComDetialListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        view.titleKey = KEY_WorkExperience;
        view.itemCount = [[self.viewModel.modelsDic valueForKey:KEY_WorkExperience] count];
        [view callBackWithExperienceBlock:^(NSString *experienceKey) {
            ICN_ExperiencessListVC *pager = [[ICN_ExperiencessListVC alloc] init];
            pager.modelsArr = [self.viewModel.modelsDic valueForKey:KEY_WorkExperience];
            [self currentPagerJumpToPager:pager];
        }];

        return view;
    }
    if ([key isEqualToString:KEY_ApplyActivity]) {
        // 参加的活动头视图 -- 点击跳转到用户的订单页面
        
        ICN_ComDetialListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        view.titleKey = KEY_ApplyActivity;
        ICN_DynActivityModel *model = [[self.viewModel.modelsDic valueForKey:KEY_ApplyActivity] firstObject];
        view.itemCount = model.count;
        [view callBackWithDetialBlock:^{
            ICN_MyOrderingViewController *pager = [[ICN_MyOrderingViewController alloc] init];
            pager.memId = self.memberId; // 将该用户的Id传给pager
            pager.userMySelf = NO; // 设置显示的不是用户自己的订单
            pager.userName = [self.viewModel.userBaseModel memberNick];
            [self currentPagerJumpToPager:pager];
            
        }];
        return view;
    }
    if ([key isEqualToString:KEY_ApplyGroups]) {
        // 参加的小组头视图
        ICN_ComDetialListSectionView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ComDetialListSectionView class])];
        view.titleKey = KEY_ApplyGroups;
        ICN_DynGroupModel *model = [[self.viewModel.modelsDic valueForKey:KEY_ApplyGroups] firstObject];
        view.itemCount = model.count;
        [view callBackWithCommonKeyBlock:^(NSString *blockKey) {
            if ([blockKey isEqualToString:KEY_ApplyGroups]) {
                ICN_JoinGroupViewController *pager = [[ICN_JoinGroupViewController alloc] init];
                pager.GroupID = self.memberId;
                [self currentPagerJumpToPager:pager];
            }
        }];
        return view;
    }

    return nil;
}



/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // 如果工作经历有Cell的高度则使用
    NSString *key = self.defaultSectionKeyArr[indexPath.section];
    if ([key isEqualToString:KEY_WorkExperience]) {
        ICN_DynWorkExperienceModel *model = [[self.viewModel.modelsDic valueForKey:KEY_WorkExperience] objectAtIndex:indexPath.row];
        if (model.CellHeight > 0) {
            return model.CellHeight;
        }
    }else if ([key isEqualToString:KEY_EduExperience]) {
        return 120;
    }else{
        NSArray *contentArr = [self.defaultSectionKeyContentDic valueForKey:key];
        if (contentArr.count == 3) {
            return [contentArr.lastObject floatValue];
        }
    }
    return 0.0;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSString *key = self.defaultSectionKeyArr[section];
    return [[[self.defaultSectionKeyContentDic valueForKey:key] objectAtIndex:1] floatValue];
}



@end
