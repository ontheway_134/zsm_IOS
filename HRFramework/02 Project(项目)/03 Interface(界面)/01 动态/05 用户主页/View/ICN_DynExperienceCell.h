//
//  ICN_DynExperienceCell.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynEducationModel;
@class ICN_DynWorkExperienceModel;

@interface ICN_DynExperienceCell : UITableViewCell

@property (nonatomic , assign , getter=isListCell)BOOL listStyle;

@property (nonatomic , strong)ICN_DynEducationModel *eduModel;
@property (nonatomic , strong)ICN_DynWorkExperienceModel *workModel;


@end
