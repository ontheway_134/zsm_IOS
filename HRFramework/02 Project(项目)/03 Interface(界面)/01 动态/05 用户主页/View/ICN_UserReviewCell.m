//
//  ICN_UserReviewCell.m
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserReviewCell.h"
#import "ICN_DynamicStateCommentModel.h"

@interface ICN_UserReviewCell ()

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel; // 用户名

@property (weak, nonatomic) IBOutlet UILabel *commentTimeLabel; // 评论时间

@property (weak, nonatomic) IBOutlet UILabel *companyStatusLabel; // 公司于职位状态

@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像

@property (weak, nonatomic) IBOutlet UILabel *commentContentLabel;

@property (weak, nonatomic) IBOutlet UIView *creakLineBackGround;



@property (nonatomic , strong)UITapGestureRecognizer *userIconTap;



@end

@implementation ICN_UserReviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    CGFloat width = self.userIcon.width / 2.0;
    self.userIcon.layer.cornerRadius = width;
    self.userIcon.layer.masksToBounds = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // Initialization code
}

- (void)callBackWithBtnBlock:(ModelBlock)block{
    self.block = block;
}

- (void)setFirstCell:(BOOL)firstCell{
    _firstCell = firstCell;
    if (firstCell) {
        self.creakLineBackGround.hidden = YES;
    }else{
        self.creakLineBackGround.hidden = NO;
    }
}

- (UITapGestureRecognizer *)userIconTap{
    if (_userIconTap == nil) {
        _userIconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnUserIcon)];
        [self.userIcon addGestureRecognizer:_userIconTap];
    }
    return _userIconTap;
}

- (void)tapOnUserIcon{
    if (self.block) {
        self.block(self.model , YES);
    }
}

- (void)setModel:(ICN_DynamicStateCommentModel *)model{
    _model = model;
    [self userIconTap];
    [self.userIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"修改头像"]];

    self.userNameLabel.text = _model.memberNick;
    self.commentTimeLabel.text = model.createDate;
    // 判断label的高度是否超出默认高度，若超出则加行数
    if (model.commentLabelHeight > 31.5) {
        self.commentContentLabel.numberOfLines = 0;
    }
    // 判断当回复的内容有@的对象的时候添加对应的内容
    if (model.replyerNick) {
        // 添加用户昵称的相关显示
        NSString *nickContentStr = SF(@"回复%@:%@",_model.replyerNick,_model.content);
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:nickContentStr];
        [attributedString addAttribute:NSForegroundColorAttributeName// 字体颜色
                                 value:RGB0X(0x009dff)
                                 range:[nickContentStr rangeOfString:_model.replyerNick]];
        self.commentContentLabel.attributedText = attributedString;
    }else{
        self.commentContentLabel.text = _model.content;
    }
    if (_model.roleId.integerValue == 1) {
        self.companyStatusLabel.hidden = NO;
        // 普通用户
        NSString *signStr;
        if (_model.workStatus.integerValue == 0) {
            // 未工作
            if (_model.memberSchool == nil || [_model.memberSchool isEqualToString:@""]) {
                _model.memberSchool = @"无学校";
            }
            if (_model.memberMajor == nil || [_model.memberMajor isEqualToString:@""]) {
                _model.memberMajor = @"无专业";
            }
            signStr = SF(@"%@|%@",_model.memberSchool , _model.memberMajor);
        }else{
            if (_model.companyName == nil || [_model.companyName isEqualToString:@""]) {
                _model.companyName = @"无公司";
            }
            if (_model.memberPosition == nil || [_model.memberPosition isEqualToString:@""]) {
                _model.memberPosition = @"无职位";
            }
            signStr = SF(@"%@|%@",_model.companyName , _model.memberPosition);
        }
        self.companyStatusLabel.text = signStr;
    }else{
        self.companyStatusLabel.hidden = YES;
        
    }
    
}

- (IBAction)clickCommentAction:(UIButton *)sender {

    if (self.block) {
        self.block(self.model , NO);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc{
    if (_userIconTap) {
        [self.userIcon removeGestureRecognizer:_userIconTap];
        _userIconTap = nil;
    }
}

@end
