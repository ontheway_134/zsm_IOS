//
//  ICN_UserMoreFounctionListView.m
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserMoreFounctionListView.h"

@interface ICN_UserMoreFounctionListView ()

@property (weak, nonatomic) IBOutlet UIButton *p_ShareToFriendBtn; // 推荐给好友按钮

@property (weak, nonatomic) IBOutlet UIButton *p_ReportBtn; // 举报按钮


@end

@implementation ICN_UserMoreFounctionListView

- (void)callWithClickStateBlock:(ClickStateBlock)block{
    self.block = block;
}

- (IBAction)clickOnBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.p_ReportBtn]) {
        // 点击举报按钮的操作
        
        if (self.block) {
            self.block(NO , YES);
        }
        
        
    }else{
        // 点击推荐给好友的操作
        if (self.block) {
            self.block(YES , NO);
        }
    }
    
    // 无论进行那种操作后都将该页面从父视图中移除
    [self removeFromSuperview];
    
}


#pragma mark - ---------- 私有方法 ----------

- (BOOL)judgePoint:(CGPoint)point isContainedWithView:(UIView *)view{
    
    CGFloat convertMaxOrix = view.width + view.mj_x;
    CGFloat convertMinOrix = view.mj_x;
    CGFloat convertMaxOriy = view.height + view.mj_y;
    CGFloat convertMinOriy = view.mj_y;
    
    if (point.x < convertMaxOrix && point.x > convertMinOrix && point.y < convertMaxOriy && point.y > convertMinOriy) {
        return YES;
    }
    return NO;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    CGPoint point = [[touches anyObject] locationInView:self];
    if ([self judgePoint:point isContainedWithView:self.p_ReportBtn] || [self judgePoint:point isContainedWithView:self.p_ShareToFriendBtn]) {
        return ;
    }
    
    // 执行将当前视图从父视图移除的操作
    HRLog(@"-------ICN_UserMoreFounctionListView背景移除父视图操作");
    [self removeFromSuperview];
}



@end
