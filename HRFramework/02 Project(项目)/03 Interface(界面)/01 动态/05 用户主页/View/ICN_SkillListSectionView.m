//
//  ICN_SkillListSectionView.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SkillListSectionView.h"
#import "ICN_SkillTagModel.h"

@interface ICN_SkillListSectionView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; // 技能标签标题
@property (nonatomic , strong)NSMutableArray <UILabel *>*skillLabelsArr; // 技能标签列表


@end

@implementation ICN_SkillListSectionView

- (NSMutableArray<UILabel *> *)skillLabelsArr{
    if (_skillLabelsArr == nil) {
        _skillLabelsArr = [NSMutableArray array];
    }
    return _skillLabelsArr;
}

- (void)setModelsArr:(NSMutableArray<ICN_SkillTagModel *> *)modelsArr{
    _modelsArr = modelsArr;
    
    if (_modelsArr.firstObject.code == 0) {
        // 获取数据成功
        // 如果有之前的标签需要先清除
        if (self.skillLabelsArr.count > 0) {
            for (UILabel *label in self.skillLabelsArr) {
                [label removeFromSuperview];
            }
            [self.skillLabelsArr removeAllObjects];
        }
        for (NSInteger i = 0 ; i < _modelsArr.count ; i++) {
            UILabel *label = [self configSkillSignLabelWithContent:_modelsArr[i].skillLabel];
            [self.skillLabelsArr addObject:label];
        }
    }
    
    [self setNeedsLayout];
    
}

- (UILabel *)configSkillSignLabelWithContent:(NSString *)content{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = content;
    label.textColor = RGB0X(0x7e94a2);
    label.minimumScaleFactor = 0.5;
    label.adjustsFontSizeToFitWidth = YES;
    label.font = [UIFont systemFontOfSize:12.0];
    label.userInteractionEnabled = YES;
    label.layer.borderColor = RGB0X(0x7e94a2).CGColor;
    label.layer.borderWidth = 1.0;
    label.textAlignment = NSTextAlignmentCenter;
    label.layer.cornerRadius = 2.0;
    label.layer.masksToBounds = YES;
    [self.contentView addSubview:label];
    return label;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGSize itemSize = CGSizeMake(60, 25);
    for (NSInteger i = 0; i < self.modelsArr.count; i = i + [self.modelsArr.firstObject rowCount]) {
        NSInteger Ycount = self.modelsArr.firstObject.rowCount + i < self.modelsArr.count ? self.modelsArr.firstObject.rowCount + i : self.modelsArr.count;
        for (NSInteger j = i; j < Ycount; j++) {
            [self.skillLabelsArr[j] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(itemSize);
            }];
            if (j == i) {
                if (i == 0) {
                    [self.skillLabelsArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.contentView).offset(10);
                        make.top.equalTo(self.titleLabel.mas_bottom).offset(15.0);
                        make.size.mas_equalTo(itemSize);
                    }];
                }else{
                    [self.skillLabelsArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                        make.left.equalTo(self.contentView).offset(10.0);
                        make.top.equalTo(self.skillLabelsArr[j - 1].mas_bottom).offset(10.0);
                        make.size.mas_equalTo(itemSize);
                    }];
                }
            }else{
                [self.skillLabelsArr[j] mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.left.equalTo(self.skillLabelsArr[j - 1].mas_right).offset(15.0);
                    make.top.equalTo(self.skillLabelsArr[j - 1]);
                    make.size.mas_equalTo(itemSize);
                }];
            }
        }
    }

}

@end
