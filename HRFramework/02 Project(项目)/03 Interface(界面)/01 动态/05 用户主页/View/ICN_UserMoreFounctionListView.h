//
//  ICN_UserMoreFounctionListView.h
//  ICan
//
//  Created by albert on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickStateBlock)(BOOL isShare , BOOL isReport);

@interface ICN_UserMoreFounctionListView : UIView

@property (nonatomic , copy)ClickStateBlock block;


- (void)callWithClickStateBlock:(ClickStateBlock)block;


@end
