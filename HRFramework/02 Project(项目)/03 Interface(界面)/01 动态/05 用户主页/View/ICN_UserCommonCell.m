//
//  ICN_UserCommonCell.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserCommonCell.h"
#import "ICN_DynStateContentModel.h"

@interface ICN_UserCommonCell ()

@property (weak, nonatomic) IBOutlet UIImageView *contentIcon; // 动态背景图

@property (weak, nonatomic) IBOutlet UILabel *contentLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;


@end

@implementation ICN_UserCommonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *array = [_model.pic componentsSeparatedByString:@","];
    [self.contentIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(array.firstObject)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.contentLabel.text = _model.content;
    self.timeLabel.text = _model.createDate;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
