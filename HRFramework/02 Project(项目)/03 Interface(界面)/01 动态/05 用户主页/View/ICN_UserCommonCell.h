//
//  ICN_UserCommonCell.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_DynStateContentModel;
@interface ICN_UserCommonCell : UITableViewCell

@property (nonatomic , strong)ICN_DynStateContentModel *model;

@end
