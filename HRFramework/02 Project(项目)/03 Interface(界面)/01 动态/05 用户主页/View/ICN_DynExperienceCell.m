//
//  ICN_DynExperienceCell.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynExperienceCell.h"
#import "ICN_DynWorkExperienceModel.h"
#import "ICN_DynEducationModel.h"

@interface ICN_DynExperienceCell ()

@property (nonatomic , strong)UIView * p_HiddenDivideView;

@property (weak, nonatomic) IBOutlet UILabel *title01;
@property (weak, nonatomic) IBOutlet UILabel *title02;
@property (weak, nonatomic) IBOutlet UILabel *title03;
@property (weak, nonatomic) IBOutlet UILabel *title04;

@property (weak, nonatomic) IBOutlet UILabel *content01;
@property (weak, nonatomic) IBOutlet UILabel *content02;
@property (weak, nonatomic) IBOutlet UILabel *content03;
@property (weak, nonatomic) IBOutlet UILabel *content04;
@property (weak, nonatomic) IBOutlet UIView *undenrLineView;

@end

@implementation ICN_DynExperienceCell

#pragma mark - ---------- 懒加载 ----------

- (UIView *)p_HiddenDivideView{
    if (_p_HiddenDivideView == nil) {
        _p_HiddenDivideView = [[UIView alloc] initWithFrame:CGRectZero];
        _p_HiddenDivideView.backgroundColor = RGB0X(0xf0f0f0);
        [self addSubview:_p_HiddenDivideView];
        [_p_HiddenDivideView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.bottom.equalTo(self);
            make.height.mas_equalTo(5.0);
        }];
    }
    return _p_HiddenDivideView;
}

#pragma mark - ---------- 属性构造器 ----------

- (void)setListStyle:(BOOL)listStyle{
    _listStyle = listStyle;
    if (self.isListCell) {
        self.p_HiddenDivideView.hidden = NO;
    }else
    {
        if (_p_HiddenDivideView) {
            self.p_HiddenDivideView.hidden = YES;
        }
    }
}

- (void)setEduModel:(ICN_DynEducationModel *)eduModel{
    _eduModel = eduModel;
    self.title01.text = @"学校名称：";
    self.title01.textAlignment = NSTextAlignmentRight;
    self.title02.text = @"专业：";
    self.title02.textAlignment = NSTextAlignmentRight;
    self.title03.text = @"毕业年份：";
    self.title03.textAlignment = NSTextAlignmentRight;
    self.title04.text = @"学历：";
    self.title04.textAlignment = NSTextAlignmentRight;
    
    self.content01.text = _eduModel.school;
    self.content02.text = _eduModel.major;
    self.content03.text = _eduModel.graduationDate;
    self.content04.text = _eduModel.qualification;
    
}

- (void)setWorkModel:(ICN_DynWorkExperienceModel *)workModel{
    _workModel = workModel;
    self.title01.text = @"在职时间：";
    self.title01.textAlignment = NSTextAlignmentRight;
    self.title02.text = @"公司名称：";
    self.title02.textAlignment = NSTextAlignmentRight;
    self.title03.text = @"工作类型：";
    self.title03.textAlignment = NSTextAlignmentRight;
    self.title04.text = @"工作内容：";
    self.title04.textAlignment = NSTextAlignmentRight;
    
    self.content01.text = SF(@"%@至%@",_workModel.entryDate , _workModel.outDate);
    self.content02.text = _workModel.companyName;
    self.content03.text = _workModel.position;
    self.content04.attributedText = [[NSAttributedString alloc] initWithString:_workModel.summary];
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
