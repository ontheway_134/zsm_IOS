//
//  ICN_PsersonalSignatureSectionView.h
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_PsersonalSignatureSectionView : UITableViewHeaderFooterView

@property (nonatomic , copy)NSString *psersonalSigurature; // 个性签名字段


@end
