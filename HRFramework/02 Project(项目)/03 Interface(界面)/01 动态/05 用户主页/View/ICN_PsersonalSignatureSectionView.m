//
//  ICN_PsersonalSignatureSectionView.m
//  ICan
//
//  Created by albert on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PsersonalSignatureSectionView.h"

@interface ICN_PsersonalSignatureSectionView ()

@property (weak, nonatomic) IBOutlet UILabel *siguratureContentLabel;


@end

@implementation ICN_PsersonalSignatureSectionView

- (void)setPsersonalSigurature:(NSString *)psersonalSigurature{
    _psersonalSigurature = psersonalSigurature;
    self.siguratureContentLabel.text = _psersonalSigurature;
}


@end
