//
//  ICN_DynamicStateCommentViewModel.h
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HRNetworkingManager+DynFirstPager.h"
#import "ICN_DynDetialModel.h"

@class ICN_DynamicStateCommentModel;

@protocol DynamicStateCommentViewModelDelegate <NSObject>

/** 在获取到动态详情数据时候的回调 */
- (void)responseWithUserDynamicDetial;

/** 用户无权限的回调 */
- (void)responseWhileUserHasNoAuthority;

/** 点赞后响应的回调 */
- (void)responseWithLikeUpRequestSuccess:(BOOL)success;

/** 点过赞回调 */
- (void)responseWithAlreadyLikeUp;


/** 获取到评论数据后返回 */
- (void)responseWithStateCommentModelsArrSuccess:(BOOL)success;

/** 评论成功后回调 */
- (void)responseWhileCommentCommitSuccess:(BOOL)success error:(NSString *)error;

@end

@interface ICN_DynamicStateCommentViewModel : NSObject

@property (nonatomic , weak)id<DynamicStateCommentViewModelDelegate> delegate;

@property (nonatomic , strong)NSMutableArray <ICN_DynamicStateCommentModel *> *modelsArr; // Model数组
@property (nonatomic , strong)ICN_DynDetialModel *detialModel;

@property (nonatomic , assign , getter=isWisdom)BOOL Wisdom; // 是否属于智讯

#pragma mark - ---------- 关于页面相关的属性 ----------
@property (nonatomic , assign)NSInteger currentPage; // 当前页数

// 在刚进入页面的时候获取动态详情的方法
- (void)loadDynamicStateDetialWithMatterId:(NSString *)matterId;

// 刷新当前页面的网络请求
- (void)refreshCurrentPageContentCellsWithMatterId:(NSString *)matterId;

// 加载下一个页面的网络请求
- (void)loadNextPageContentCellsWithMatterId:(NSString *)matterId;

// 发布评论窗口
- (void)postDynamicStateCommentContentWithMatterId:(NSString *)matterId
                                           Replyer:(NSString *)replyer
                                           Content:(NSString *)content;
// 点赞接口
- (void)likeUpWithType:(NSInteger)type Model:(ICN_DynStateContentModel *)model;

@end
