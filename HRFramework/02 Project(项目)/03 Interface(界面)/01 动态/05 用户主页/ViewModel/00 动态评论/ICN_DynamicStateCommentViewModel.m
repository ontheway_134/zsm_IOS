//
//  ICN_DynamicStateCommentViewModel.m
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStateCommentViewModel.h"
#import "ToolAboutTime.h"


@interface ICN_DynamicStateCommentViewModel ()

@property (nonatomic , copy)NSString *errorMessage; // 错误信息


@property (nonatomic , strong)ICN_DynStateContentModel *likeUpModel; // 点赞Model

@end

@implementation ICN_DynamicStateCommentViewModel

/** 初始化方法 */
- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
    }
    return self;
}

- (NSMutableArray<ICN_DynamicStateCommentModel *> *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

#pragma mark - ---------- 网络请求 ----------

- (void)loadDynamicStateDetialWithMatterId:(NSString *)matterId{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token != nil && matterId != nil) {
        // 获取网络请求
        NSDictionary *params = @{@"token":token,
                                 @"matterId":matterId};
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicDetial params:params success:^(id result) {
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (firstModel.code == 0) {
                // 获取成功
                if (self.delegate) {
                    self.detialModel = [[ICN_DynDetialModel alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
                    self.detialModel.createDate = [ToolAboutTime getTimeStrByTimeSp:self.detialModel.createDate];
                    if ([self.delegate respondsToSelector:@selector(responseWithUserDynamicDetial)]) {
                        [self.delegate responseWithUserDynamicDetial];
                    }
                }
            }
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }
}

- (void)likeUpWithType:(NSInteger)type Model:(ICN_DynStateContentModel *)model{
    NSString *typeStr = SF(@"%ld",type);
    if (model) {
        self.likeUpModel = model;
    }
    [HRNetworkingManager updateDynamicLikeUpStateWithMatterId:model.matterId Type:typeStr Success:^(BaseOptionalModel *model) {
        if (model.code == 0) {
            if (self.likeUpModel.likeUp) {
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue + 1);
            }else{
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue - 1);
            }
            [self callBackWithLikeUpRequestSuccess:YES];
        }
        if (model.code == 2) {
            // 用户无权限点赞
            [self callBackWhileAuthorHasNoAuthority];
        }
        if (model.code == 3) {
            // 用户已经点赞现在无法点赞只能取消
            self.likeUpModel.likeUp = YES;
            [self callBackWithAuthorAlreadyLikeUp];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithLikeUpRequestSuccess:NO];
    }];
    
}

- (void)callBackWithAuthorAlreadyLikeUp{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithAlreadyLikeUp)]) {
            [self.delegate responseWithAlreadyLikeUp];
        }
    }
}


- (void)callBackWhileAuthorHasNoAuthority{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWhileUserHasNoAuthority)]) {
            [self.delegate responseWhileUserHasNoAuthority];
        }
    }
}


- (void)callBackWithLikeUpRequestSuccess:(BOOL)success{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithLikeUpRequestSuccess:)]) {
            [self.delegate responseWithLikeUpRequestSuccess:success];
        }
    }
}

- (void)postDynamicStateCommentContentWithMatterId:(NSString *)matterId Replyer:(NSString *)replyer Content:(NSString *)content{
    
    [HRNetworkingManager publishDynamicStateCommentWithMatterId:matterId Replyer:replyer Content:content Success:^(id responseObject) {
        BaseOptionalModel *model = responseObject;
        if (model.code == 0) {
            [self callBackWithNetPublish:YES];
        }else if (model.code == 1){
            self.errorMessage = model.info;
            [self callBackWithNetPublish:NO];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithNetPublish:NO];
        self.errorMessage = @"网络请求异常";
    } Wisdom:self.isWisdom];
}

- (void)refreshCurrentPageContentCellsWithMatterId:(NSString *)matterId{
    _currentPage = 1;
    [HRNetworkingManager requestDynamicStateCommentsWithPage:_currentPage PageSize:9 MatterId:matterId Success:^(NSArray<ICN_DynamicStateCommentModel *> *array) {
        if (_modelsArr) {
            [_modelsArr removeAllObjects];
        }

        [self.modelsArr addObjectsFromArray:array];
        [self callBackWithNetRequest:YES];
    } Failure:^(NSDictionary *errorInfo) {
        // 执行数据获取失败之后的操作
        [self callBackWithNetRequest:NO];
    }];
}


- (void)loadNextPageContentCellsWithMatterId:(NSString *)matterId{
    _currentPage ++;
    [HRNetworkingManager requestDynamicStateCommentsWithPage:_currentPage PageSize:9 MatterId:matterId Success:^(NSArray<ICN_DynamicStateCommentModel *> *array) {
        [self.modelsArr addObjectsFromArray:array];
        [self callBackWithNetRequest:YES];
    } Failure:^(NSDictionary *errorInfo) {
        // 执行数据获取失败之后的操作
        [self callBackWithNetRequest:NO];
    }];

}


#pragma mark - ---------- 私有方法 ----------

- (void)callBackWithNetPublish:(BOOL)success{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWhileCommentCommitSuccess:error:)]) {
            [self.delegate responseWhileCommentCommitSuccess:success error:self.errorMessage];
            self.errorMessage = nil;
        }
    }
}

- (void)callBackWithNetRequest:(BOOL)success{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithStateCommentModelsArrSuccess:)]) {
            [self.delegate responseWithStateCommentModelsArrSuccess:success];
        }
    }
}



@end
