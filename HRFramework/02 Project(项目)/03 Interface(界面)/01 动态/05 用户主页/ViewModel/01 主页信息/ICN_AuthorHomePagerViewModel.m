//
//  ICN_AuthorHomePagerViewModel.m
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_AuthorHomePagerViewModel.h"
#import "HRNetworkingManager+AuthorHomePager.h"
#import "UILabel+ALB_SizeFit.h"
#import "ICN_DynGroupModel.h"

@interface ICN_AuthorHomePagerViewModel ()

@property (nonatomic , assign)NSInteger currentPage;

@end

@implementation ICN_AuthorHomePagerViewModel

#pragma mark - ---------- 公开方法 ----------

/** 初始化方法 */
- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
        _mine = NO;
    }
    return self;
}

#pragma mark - ---------- 私有方法 ----------
// 根据Model和技能标签的总数
- (void)configSkillTagCellWithTagModel:(ICN_SkillTagModel *)model Total:(NSInteger)total{
    CGSize size = CGSizeMake(60, 25);
    NSInteger longth = ((NSInteger)SCREEN_WIDTH - 10) % ((NSInteger)size.width + 15);
    model.rowCount = (SCREEN_WIDTH - 15) / (size.width + 15.0);
    if (longth > (NSInteger)size.width) {
        model.rowCount ++;
    }
    NSInteger count = total / model.rowCount;
    if (total % model.rowCount != 0) {
        count ++;
    }
    model.CellHeight = 64 + (size.height + 10.0) * count - 10.0;
}

- (void)configListCellHeighWithModel:(id)model
{
    if ([model isKindOfClass:[ICN_DynWorkExperienceModel class]]) {
        // 当列表是工作经验列表
        ICN_DynWorkExperienceModel *tempModel = model;
        CGFloat ContentHeightheight = [UILabel getTextHeight:tempModel.summary width:SCREEN_WIDTH - 83 fontSize:14];
        tempModel.CellHeight = 90.0 + ContentHeightheight;
    }
}

#pragma mark - ---------- 懒加载 ----------

- (NSMutableDictionary *)modelsDic{
    if (_modelsDic == nil) {
        _modelsDic = [NSMutableDictionary dictionary];
    }
    return _modelsDic;
}

- (void)refreshCurrentPageContentCellsWithMemberId:(NSString *)memberId{
    
    // 获取这个用户的动态
    [HRNetworkingManager requestDynamicStateWithMemberId:memberId Page:1 PageSize:2 Success:^(NSArray<ICN_DynStateContentModel *> *array) {
        if (array != nil && array.count > 0) {
            [self.modelsDic setValue:array forKey:KEY_DynamicStates];
            [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_DynamicStates];
        }
    } Failure:^(NSDictionary *errorInfo) {
        // 返回获取动态失败需要的相应操作
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_DynamicStates];
    }];
    
    if (!self.isMe) {
        // 获取这个用户的共同好友
        [HRNetworkingManager requestUserCommonFriendsWithUserId:memberId Success:^(NSArray<ICN_CommonFriendsModel *> *array) {
            if (array.firstObject.code == 0) {
                [self.modelsDic setValue:array forKey:KEY_SameFriends];
                [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
            }else{
                [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_SameFriends];
            }
        } Failure:^(NSDictionary *errorInfo) {
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_SameFriends];
        }];
    }else{
        if ([self.modelsDic valueForKey:KEY_SameFriends]) {
            [self.modelsDic removeObjectForKey:KEY_SameFriends];
        }
    }
    
    // 获取这个用户技能标签的数据
    [HRNetworkingManager requestUserSkillSignWithMemberId:memberId Success:^(NSArray<ICN_SkillTagModel *> *array) {
        if (array.count > 0 && array.firstObject.code == 0) {
            [self.modelsDic setValue:array forKey:KEY_SkillSigns];
            // 为技能标签中的第一个Model 添加Cell的计算属性
            [self configSkillTagCellWithTagModel:array.firstObject Total:array.count];
            [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_SkillSigns];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_SkillSigns];
    }];
    
    // 获取教育经历
    [HRNetworkingManager requestUserEducationExperienceWithMemberId:memberId Success:^(NSArray<ICN_DynEducationModel *> *array) {
        if (array.count > 0 && array.firstObject.code == 0) {
            
            [self.modelsDic setValue:array forKey:KEY_EduExperience];
            [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_EduExperience];
        }

    } Failure:^(NSDictionary *errorInfo) {
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_EduExperience];
    }];
    // 获取工作经历
    [HRNetworkingManager requestUserWorkExperienceWithMemberId:memberId Success:^(NSArray<ICN_DynWorkExperienceModel *> *array) {
        if (array.count > 0 && array.firstObject.code == 0) {
            // 需要计算列表高度
            for (ICN_DynWorkExperienceModel *model in array) {
                [self configListCellHeighWithModel:model];
            }
            [self.modelsDic setValue:array forKey:KEY_WorkExperience];
            [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_WorkExperience];
        }

    } Failure:^(NSDictionary *errorInfo) {
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_WorkExperience];
    }];
    // 获取参加活动
    [HRNetworkingManager requestUserAcicvitiesListWithMemberId:memberId Page:1 Success:^(NSArray<ICN_DynActivityModel *> *array) {
        if (array.count > 0 && array.firstObject.code == 0) {
            [self.modelsDic setValue:array forKey:KEY_ApplyActivity];
            [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_ApplyActivity];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_ApplyActivity];
    }];
    
    // 获取参加小组
    [HRNetworkingManager requestUserGruopsListWithMemberId:memberId Success:^(NSArray<ICN_DynGroupModel *> *array) {
        if (array.count > 0 && array.firstObject.code == 0) {
            NSMutableArray *resultArr = [NSMutableArray arrayWithArray:array];
            NSMutableArray *deleteArr = [NSMutableArray array];
            for (ICN_DynGroupModel *model in array) {
                if (model.isopen.integerValue != 0) {
                    // 暂时设置0的时候代表公开
                    [deleteArr addObject:model];
                }
            }
            [resultArr removeObjectsInArray:deleteArr];
            if (resultArr != nil && resultArr.count > 0) {
                [self.modelsDic setValue:[NSArray arrayWithArray:resultArr] forKey:KEY_ApplyGroups];
                [self reloadDataWhileNetRequestSuccess:YES FaileKey:nil];
            }else{
                [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_ApplyGroups];
            }
        }else{
            [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_ApplyGroups];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self reloadDataWhileNetRequestSuccess:NO FaileKey:KEY_ApplyGroups];
    }];
}

// 获取到数据后根据是否成功以及失败的网络请求的key做对应操作
- (void)reloadDataWhileNetRequestSuccess:(BOOL)success FaileKey:(NSString *)key{
    // 在代理存在的情况下根据回调刷新视图
    if (self.delegate) {
        if (success) {
            if ([self.delegate respondsToSelector:@selector(responseWithModelsDic:)]) {
                [self.delegate responseWithModelsDic:_modelsDic];
            }
        }else{
            // 执行获取数据失败之后的对应操作
            if ([self.delegate respondsToSelector:@selector(responseNetRequestFailedWithKey:)]) {
                [self.delegate responseNetRequestFailedWithKey:key];
            }
        }
    }
}

- (void)loadUserBaseMessageWithUserId:(NSString *)sideId{
    if (sideId) {
        [HRNetworkingManager requestUserBaseMessageWithUserId:sideId Success:^(ICN_DynUserBaseMSGModel *model) {
            if (model.code == 0) {
                self.userBaseModel = model;
                if (self.userBaseModel.isMe.integerValue == 1) {
                    self.mine = YES;
                }
                // 添加用户的个性签名内容
                [self.modelsDic setValue:@"sign" forKey:KEY_SignTitle];
            }
            [self callBackWhileLoadUserBaseMessageWithCode:model.code Error:model.info Success:YES];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWhileLoadUserBaseMessageWithCode:0 Error:@"网络请求失败" Success:NO];
        }];
    }
}

- (void)requestFriendAdd:(BOOL)isAdd WithMemberId:(NSString *)memberId{
    if (isAdd) {
        [HRNetworkingManager requestAddFriendWithToId:memberId Success:^(id responseObject) {
            BaseOptionalModel *model = responseObject;
            [self callBackWhileConfigFriendAlreadyWithCode:model.code Error:model.info Add:isAdd];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWhileConfigFriendAlreadyWithCode:-1 Error:@"网络请求失败" Add:isAdd];
        }];
    }else{
        [HRNetworkingManager requestDeleteFriendWithFriendId:memberId Success:^(id responseObject) {
            BaseOptionalModel *model = responseObject;
            [self callBackWhileConfigFriendAlreadyWithCode:model.code Error:model.info Add:isAdd];
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWhileConfigFriendAlreadyWithCode:-1 Error:@"网络请求失败" Add:isAdd];
        }];
    }
}

- (void)callBackWhileConfigFriendAlreadyWithCode:(NSInteger)code Error:(NSString *)error Add:(BOOL)isAdd{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithAddFriend:Success:Error:)]) {
            
            if (code == 0) {
                [self.delegate responseWithAddFriend:isAdd Success:YES Error:nil];
            }else{
                [self.delegate responseWithAddFriend:isAdd Success:NO Error:error];
            }
            
        }
    }
}

- (void)callBackWhileLoadUserBaseMessageWithCode:(NSInteger)code Error:(NSString *)error Success:(BOOL)success{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithUserBaseMassageRequest:Error:)]) {
            if (success) {
                if (code == 0) {
                    [self.delegate responseWithUserBaseMassageRequest:YES Error:nil];
                }else{
                    [self.delegate responseWithUserBaseMassageRequest:NO Error:error];
                }
            }else{
                [self.delegate responseWithUserBaseMassageRequest:NO Error:error];
            }

        }
    }
    
}


@end
