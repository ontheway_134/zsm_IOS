//
//  ICN_DynamicStateListViewModel.m
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStateListViewModel.h"
#import "ICN_DynStateContentModel.h"
#import "HRNetworkingManager+DynFirstPager.h"
#import "HRNetworkingManager+AuthorHomePager.h"
#import "UILabel+ALB_SizeFit.h"
#import "ToolAboutTime.h"

@interface ICN_DynamicStateListViewModel ()

@property (nonatomic , assign)NSInteger currentPage;
@property (nonatomic , strong)ICN_DynStateContentModel *likeUpModel;

@end

@implementation ICN_DynamicStateListViewModel

/** 初始化方法 */
- (instancetype)init{
    self = [super init];
    if (self) {
        _currentPage = 1;
    }
    return self;
}

- (NSMutableArray *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (void)refreshCurrentPageContentCells{
    self.currentPage = 1;
    if (self.isMyWisdom == YES) {
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        NSMutableDictionary *params = [@{
                                         @"page" : SF(@"%ld",(long)self.currentPage),
                                         @"size" : SF(@"%d",9),
                                         } mutableCopy];
        if (token) {
            [params setValue:token forKey:@"token"];
        }
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MineTopic params:params success:^(id result) {
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (firstModel.code == 0) {
                NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
                for (NSDictionary *dic in [result valueForKey:@"result"]) {
                    if (![dic isKindOfClass:[NSDictionary class]]) {
                        continue ;
                    }
                    ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                    model.count = firstModel.count;
                    model.code = firstModel.code;
                    model.info = firstModel.info;
                    model.widsom = YES;
                    model.matterId = model.DynID;
                    model.contentSpread = NO;
                    model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                    [resultArr addObject:model];
                }
                
                
                // 数据获取成功，将数据数组刷新并调用回调方法
                if (_modelsArr) {
                    [_modelsArr removeAllObjects];
                }
                for (ICN_DynStateContentModel *model in resultArr) {
                    [self configStateModel:model];
                }
                [self.modelsArr addObjectsFromArray:resultArr];
                [self callBackWithDynamicStateListCode:0 Request:YES];
            }else{
                [self callBackWithDynamicStateListCode:firstModel.code Request:NO];
            }
        } failure:^(NSDictionary *errorInfo) {
            [self callBackWithDynamicStateListCode:0 Request:YES];
        }];
    }else{
        if (self.isMyDynamic == NO) {
            [HRNetworkingManager requestDynamicStateWithMemberId:self.memberId Page:self.currentPage PageSize:9 Success:^(NSArray<ICN_DynStateContentModel *> *array) {
                if (array.firstObject.code != 0) {
                    // 数据获取失败，将code编码返回
                    [self callBackWithDynamicStateListCode:array.firstObject.code Request:YES];
                }else{
                    // 数据获取成功，将数据数组刷新并调用回调方法
                    if (_modelsArr) {
                        [_modelsArr removeAllObjects];
                    }
                    for (ICN_DynStateContentModel *model in array) {
                        [self configStateModel:model];
                    }
                    [self.modelsArr addObjectsFromArray:array];
                    [self callBackWithDynamicStateListCode:0 Request:YES];
                }
                
            } Failure:^(NSDictionary *errorInfo) {
                // 回调数据获取失败
                [self callBackWithDynamicStateListCode:1 Request:NO];
            }];
        }else{
            [HRNetworkingManager requestMineDynamicStateWithPage:self.currentPage PageSize:HR_ComContentPageSize Success:^(NSArray<ICN_DynStateContentModel *> *array) {
                if (array.firstObject.code != 0) {
                    // 数据获取失败，将code编码返回
                    [self callBackWithDynamicStateListCode:array.firstObject.code Request:YES];
                }else{
                    // 数据获取成功，将数据数组刷新并调用回调方法
                    if (_modelsArr) {
                        [_modelsArr removeAllObjects];
                    }
                    for (ICN_DynStateContentModel *model in array) {
                        [self configStateModel:model];
                    }
                    [self.modelsArr addObjectsFromArray:array];
                    [self callBackWithDynamicStateListCode:0 Request:YES];
                }
            } Failure:^(NSDictionary *errorInfo) {
                // 回调数据获取失败
                [self callBackWithDynamicStateListCode:1 Request:NO];
            }];
        }
    }
}


- (void)loadNextPageContentCells{
    self.currentPage ++;
    if (self.isMyWisdom == YES) {
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        NSMutableDictionary *params = [@{
                                         @"page" : SF(@"%ld",(long)self.currentPage),
                                         @"size" : SF(@"%d",9),
                                         } mutableCopy];
        if (token) {
            [params setValue:token forKey:@"token"];
        }
        [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MineTopic params:params success:^(id result) {
            BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
            if (firstModel.code == 0) {
                NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
                for (NSDictionary *dic in [result valueForKey:@"result"]) {
                    if (![dic isKindOfClass:[NSDictionary class]]) {
                        continue ;
                    }
                    ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                    model.count = firstModel.count;
                    model.code = firstModel.code;
                    model.info = firstModel.info;
                    model.contentSpread = NO;
                    model.matterId = model.DynID;
                    model.widsom = YES;
                    model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                    [resultArr addObject:model];
                }
                
                
                
                for (ICN_DynStateContentModel *model in resultArr) {
                    [self configStateModel:model];
                }
                [self.modelsArr addObjectsFromArray:resultArr];
                [self callBackWithDynamicStateListCode:0 Request:YES];
            }else{
                [self callBackWithDynamicStateListCode:firstModel.code Request:NO];
            }
        } failure:^(NSDictionary *errorInfo) {
            [self callBackWithDynamicStateListCode:0 Request:YES];
        }];
    }else{
    if (self.isMyDynamic == NO) {
        [HRNetworkingManager requestDynamicStateWithMemberId:self.memberId Page:self.currentPage PageSize:HR_ComContentPageSize Success:^(NSArray<ICN_DynStateContentModel *> *array) {
            if (array.firstObject.code != 0) {
                [self callBackWithDynamicStateListCode:array.firstObject.code Request:YES];
            }else{
                for (ICN_DynStateContentModel *model in array) {
                    [self configStateModel:model];
                }
                [self.modelsArr addObjectsFromArray:array];
                [self callBackWithDynamicStateListCode:0 Request:YES];
            }
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithDynamicStateListCode:1 Request:NO];
        }];
    }else{
        [HRNetworkingManager requestMineDynamicStateWithPage:self.currentPage PageSize:HR_ComContentPageSize Success:^(NSArray<ICN_DynStateContentModel *> *array) {
            if (array.firstObject.code != 0) {
                [self callBackWithDynamicStateListCode:array.firstObject.code Request:YES];
            }else{
                for (ICN_DynStateContentModel *model in array) {
                    [self configStateModel:model];
                }
                [self.modelsArr addObjectsFromArray:array];
                [self callBackWithDynamicStateListCode:0 Request:YES];
            }
        } Failure:^(NSDictionary *errorInfo) {
            [self callBackWithDynamicStateListCode:1 Request:NO];
        }];
    }
    }
}

- (void)likeUpWithType:(NSInteger)type Model:(ICN_DynStateContentModel *)model{
    NSString *typeStr = SF(@"%ld",(long)type);
    if (model) {
        self.likeUpModel = model;
    }
    [HRNetworkingManager updateDynamicLikeUpStateWithMatterId:model.matterId Type:typeStr Success:^(BaseOptionalModel *model) {
        if (model.code == 0) {
            if (self.likeUpModel.likeUp) {
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue + 1);
            }else{
                self.likeUpModel.praiseCount = SF(@"%ld",self.likeUpModel.praiseCount.integerValue - 1);
            }
            [self callBackWithLikeUpRequestSuccess:YES];
        }
        if (model.code == 2) {
            // 用户无权限点赞
            [self callBackWhileAuthorHasNoAuthority];
        }
        if (model.code == 3) {
            // 用户已经点赞现在无法点赞只能取消
            self.likeUpModel.likeUp = YES;
            [self callBackWithAuthorAlreadyLikeUp];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithLikeUpRequestSuccess:NO];
    }];
    
}

#pragma mark - ---------- 私有方法 ----------

- (void)configStateModel:(ICN_DynStateContentModel *)model{
    
    NSArray *array = [model.pic componentsSeparatedByString:@","];
    if (array && array.count > 0 && [array.firstObject containsString:@"."]) {
        model.imageFooterHeight = [self countImageFooterHeightWithPicCount:array.count];
    }else
        model.imageFooterHeight = 0.0;
    
    
    model.contentSpreadHeight = [UILabel getTextHeight:model.content width:[UIScreen mainScreen].bounds.size.width - 20.0 fontSize:13.0];
}

- (CGFloat)countImageFooterHeightWithPicCount:(NSInteger)count{
    NSInteger groups = count / 3;
    if (count < 3) {
        groups = 0;
    }
    if (count % 3 > 0) {
        groups ++;
    }
    // 获取5s尺寸下的imageview占用高度
    CGFloat countHeight = groups * (97.0 + 5.0) - 5.0;
    return ICN_KScreen(countHeight);
}

- (void)callBackWithDynamicStateListCode:(NSInteger)code Request:(BOOL)success{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithDynamicStateListRequestSuccess:Code:)]) {
            // 在获取到动态列表的数据请求之后将请求结果返回给vc
            [self.delegate responseWithDynamicStateListRequestSuccess:success Code:code];
        }
    }
}

- (void)callBackWithAuthorAlreadyLikeUp{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithAlreadyLikeUp)]) {
            [self.delegate responseWithAlreadyLikeUp];
        }
    }
}

- (void)callBackWhileAuthorHasNoAuthority{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWhileUserHasNoAuthority)]) {
            [self.delegate responseWhileUserHasNoAuthority];
        }
    }
}

- (void)callBackWithLikeUpRequestSuccess:(BOOL)success{
    if (self.likeUpModel) {
        self.likeUpModel = nil;
    }
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseWithLikeUpRequestSuccess:)]) {
            [self.delegate responseWithLikeUpRequestSuccess:success];
        }
    }
}


@end
