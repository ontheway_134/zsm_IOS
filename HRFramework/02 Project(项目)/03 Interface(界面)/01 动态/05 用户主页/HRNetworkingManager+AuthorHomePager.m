//
//  HRNetworkingManager+AuthorHomePager.m
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager+AuthorHomePager.h"
#import "ToolAboutTime.h"

@implementation HRNetworkingManager (AuthorHomePager)

+ (void)requestDynamicStateWithMemberId:(NSString *)memberId Page:(NSInteger)page PageSize:(NSInteger)size Success:(StateArrBlock)success Failure:(ErrorBlock)failure{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"page" : SF(@"%ld",(long)page),
                                     @"size" : SF(@"%ld",(long)size),
                                     @"memberId" : memberId,
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserDynamicState params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                if (![dic isKindOfClass:[NSDictionary class]]) {
                    continue ;
                }
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                model.count = firstModel.count;
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.contentSpread = NO;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [resultArr addObject:model];
            }
            success(resultArr);
            HRLog(@"用户主页动态 - 数据获取成功");
        }else{
            HRLog(@"用户主页动态 - 网络请求成功 - 数据获取失败");
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
        HRLog(@"用户主页动态 - 网络请求失败");
    }];
}

+ (void)requestMineDynamicStateWithPage:(NSInteger)page PageSize:(NSInteger)size Success:(StateArrBlock)success Failure:(ErrorBlock)failure{
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *params = [@{
                                     @"page" : SF(@"%ld",(long)page),
                                     @"size" : SF(@"%ld",(long)size),
                                     } mutableCopy];
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MainDynamic params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            NSMutableArray<ICN_DynStateContentModel *> *resultArr = [NSMutableArray array];
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                if (![dic isKindOfClass:[NSDictionary class]]) {
                    continue ;
                }
                ICN_DynStateContentModel *model = [[ICN_DynStateContentModel alloc] initWithDictionary:dic error:nil];
                model.count = firstModel.count;
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.contentSpread = NO;
                model.createDate = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                [resultArr addObject:model];
            }
            success(resultArr);
            HRLog(@"用户主页动态 - 数据获取成功");
        }else{
            HRLog(@"用户主页动态 - 网络请求成功 - 数据获取失败");
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
        HRLog(@"用户主页动态 - 网络请求失败");
    }];

}

+ (void)requestReportWithMatterId:(NSString *)matterId Type:(NSString *)type Summary:(NSString *)summary Success:(SuccessBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"matterId" : matterId,
                                     @"reason" : type
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    if (summary) {
        [params setValue:summary forKey:@"summary"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicStateReport params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestUserBaseMessageWithUserId:(NSString *)sideId Success:(UserBaseMSGBlock)success Failure:(ErrorBlock)failure{
    
    NSMutableDictionary *params = [@{
                                     @"sideId" : sideId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserMainMsg params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 获取Model成功
            ICN_DynUserBaseMSGModel *model = [[ICN_DynUserBaseMSGModel alloc] initWithDictionary:[result valueForKey:@"result"] error:nil];
            model.code = firstModel.code;
            model.info = firstModel.info;
            success(model);
        }else{
            // 非网络原因获取数据失败
            ICN_DynUserBaseMSGModel *model = [[ICN_DynUserBaseMSGModel alloc] initWithDictionary:result error:nil];
            success(model);
        }
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestUserCommonFriendsWithUserId:(NSString *)sideId Success:(CommonFriendsArrBlock)success Failure:(ErrorBlock)failure{
    
    NSMutableDictionary *params = [@{
                                     @"sideId" : sideId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserComFriends params:params success:^(id result) {
        NSMutableArray<ICN_CommonFriendsModel *> *array = [NSMutableArray array];
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 数据获取成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_CommonFriendsModel *model = [[ICN_CommonFriendsModel alloc] initWithDictionary:item error:nil];
                [array addObject:model];
                model.code = firstModel.code;
                model.info = firstModel.info;
            }
        }else{
            ICN_CommonFriendsModel *model = [[ICN_CommonFriendsModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestUserSkillSignWithMemberId:(NSString *)memberId Success:(SkillTagsBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"memberId" : memberId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserSkillSign params:params success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray<ICN_SkillTagModel *> *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_SkillTagModel *model = [[ICN_SkillTagModel alloc] initWithDictionary:item error:nil];
                model.code = firstModel.code;
                model.info = firstModel.info;
                [array addObject:model];
            }
        }else{
            ICN_SkillTagModel *model = [[ICN_SkillTagModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestUserAcicvitiesListWithMemberId:(NSString *)memberId Page:(NSInteger)page Success:(ActivitiesListBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"memberId" : memberId,
                                     @"page" : SF(@"%ld",(long)page),
                                     } mutableCopy];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserActivities params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray<ICN_DynActivityModel *> *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynActivityModel *model = [[ICN_DynActivityModel alloc] initWithDictionary:item error:nil];
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.count = firstModel.count;
                [array addObject:model];
            }
        }else{
            ICN_DynActivityModel *model = [[ICN_DynActivityModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);

    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestUserGruopsListWithMemberId:(NSString *)memberId Success:(GroupListBlock)success Failure:(ErrorBlock)failure{
    
    NSMutableDictionary *params = [@{
                                     @"friendId" : memberId,
                                     } mutableCopy];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserGroups params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray<ICN_DynGroupModel *> *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynGroupModel *model = [[ICN_DynGroupModel alloc] initWithDictionary:item error:nil];
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.count = firstModel.count;
                [array addObject:model];
            }
        }else{
            ICN_DynGroupModel *model = [[ICN_DynGroupModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestUserWorkExperienceWithMemberId:(NSString *)memberId Success:(WorkExpListBlock)success Failure:(ErrorBlock)failure{
    
    NSMutableDictionary *params = [@{
                                     @"memberId" : memberId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }

    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserWorkExperience params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray<ICN_DynWorkExperienceModel *> *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynWorkExperienceModel *model = [[ICN_DynWorkExperienceModel alloc] initWithDictionary:item error:nil];
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.count = firstModel.count;
                [array addObject:model];
            }
        }else{
            ICN_DynWorkExperienceModel *model = [[ICN_DynWorkExperienceModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestUserEducationExperienceWithMemberId:(NSString *)memberId Success:(EducationListClock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"memberId" : memberId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserEduExperience params:params success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        NSMutableArray<ICN_DynEducationModel *> *array = [NSMutableArray array];
        if (firstModel.code == 0) {
            // 获取数据成功
            NSArray *tempArr = [result valueForKey:@"result"];
            for (NSDictionary *item in tempArr) {
                ICN_DynEducationModel *model = [[ICN_DynEducationModel alloc] initWithDictionary:item error:nil];
                model.code = firstModel.code;
                model.info = firstModel.info;
                model.count = firstModel.count;
                [array addObject:model];
            }
        }else{
            ICN_DynEducationModel *model = [[ICN_DynEducationModel alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
        
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestUserReportWithReportMemberId:(NSString *)reportMemberId Reason:(NSString *)reason Summary:(NSString *)summary Success:(SuccessBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"reporttoken" : reportMemberId,
                                     @"reason":reason,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"token"];
    }
    if (summary) {
        [params setValue:summary forKey:@"summary"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserReport params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestAddFriendWithToId:(NSString *)toId Success:(SuccessBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"toId" : toId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"fromId"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserAddFriend params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

+ (void)requestDeleteFriendWithFriendId:(NSString *)friendId Success:(SuccessBlock)success Failure:(ErrorBlock)failure{
    NSMutableDictionary *params = [@{
                                     @"friendId" : friendId,
                                     } mutableCopy];
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    if (token) {
        [params setValue:token forKey:@"memberId"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UserDeleteFriend params:params success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];

}

@end
