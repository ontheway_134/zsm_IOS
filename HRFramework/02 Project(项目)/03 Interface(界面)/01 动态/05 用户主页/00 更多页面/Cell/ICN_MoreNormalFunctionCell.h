//
//  ICN_MoreNormalFunctionCell.h
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_MoreNormalFunctionCell : UITableViewCell

@property (nonatomic , assign , getter=isButtonHidden)BOOL hiddenBtn; // 是否隐藏按钮的属性
@property (nonatomic , copy)NSString *title; // 标签名


@end
