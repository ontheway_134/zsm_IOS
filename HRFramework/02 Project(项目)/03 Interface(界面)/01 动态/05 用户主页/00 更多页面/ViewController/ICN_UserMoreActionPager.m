//
//  ICN_UserMoreActionPager.m
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserMoreActionPager.h"

#pragma mark --- 私有宏 ---
#define PRIVATE_TitleContentArr @[@"推荐给好友" , @"不让Ta看我的动态" , @"不看Ta的动态" , @"删除好友" , @"加入黑名单" , @"举报"]

#pragma mark --- 私有枚举 ---




// 导入Cell相关头文件 --
#import "ICN_MoreJumpFounctionCell.h" // 跳转到详情页面按钮的Cell
#import "ICN_MoreNormalFunctionCell.h" // 其他Cell

@interface ICN_UserMoreActionPager ()<UITableViewDelegate , UITableViewDataSource>

@property (nonatomic , strong)UITableView *tableView;
@property (nonatomic , strong)NSArray <NSString *>* contentTltleArr; // 用户存储标签名称的数组


@end

@implementation ICN_UserMoreActionPager

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        CGRect frame = [UIScreen mainScreen].bounds;
        
        _tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_MoreJumpFounctionCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_MoreJumpFounctionCell class])];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_MoreNormalFunctionCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_MoreNormalFunctionCell class])];
        
        // 用于去掉当tableview的Cell不能不满屏的时候显示的空白分割Cell
        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        self.tableView.backgroundColor = RGB(235, 236, 237);
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

#pragma mark - ---------- 声明周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"更多";
    self.hiddenDefaultNavBar = NO;
    [self tableView]; // 配置tableView
    // 初始化标签名称数组
    self.contentTltleArr = PRIVATE_TitleContentArr;
    
    // Do any additional setup after loading the view from its nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 代理 ----------

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 -- 根据选中的Cell添加对应的功能
    switch (indexPath.row) {
        case 0:{
            // 推荐给好友
            break;
        }
        case 3:{
            // 删除好友
            break;
        }
        case 4:{
            // 加入黑名单
            break;
        }
        case 5:{
            // 举报好友
            break;
        }
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据索引值判断需要返回的Cell
    NSInteger tag = indexPath.row;
    switch (tag) {
        // 显示能跳转到详情页面的Cell
        case 0:
        case 5:{
            ICN_MoreJumpFounctionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_MoreJumpFounctionCell class])];
            // 推荐给好友隐藏顶部分隔栏
            if (tag == 0) {
                cell.topHidden = YES;
                cell.bottomHidden = NO;
            }
            // 举报隐藏底部分隔栏
            if (tag == 5) {
                cell.topHidden = NO;
                cell.bottomHidden = YES;
            }
            // 赋值字段名
            cell.title = self.contentTltleArr[tag];
            return cell;
            break;
        }
        case 1:
        case 2:
        case 3:
        case 4:{
            ICN_MoreNormalFunctionCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_MoreNormalFunctionCell class])];
            // 设置Cell的标题字段
            cell.title = self.contentTltleArr[tag];
            // 让不让用户看动态的两种状态
            if (tag == 1 || tag == 2) {
                cell.hiddenBtn = NO;
            }
            // 删除好友还是加入黑名单的状态
            if (tag == 3 || tag == 4) {
                cell.hiddenBtn = YES;
            }
            return cell;
            
            break;
        }
        default: break;
    }
    return nil;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 上下是45.0 区域都是40
    if (indexPath.row == 0 || indexPath.row == 5) {
        return 45.0;
    }
    return 40.0;
}



@end
