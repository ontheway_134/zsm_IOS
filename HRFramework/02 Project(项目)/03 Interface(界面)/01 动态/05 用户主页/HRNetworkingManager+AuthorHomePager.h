//
//  HRNetworkingManager+AuthorHomePager.h
//  ICan
//
//  Created by albert on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager.h"
#import "ICN_DynStateContentModel.h"
#import "ICN_DynUserBaseMSGModel.h"
#import "ICN_CommonFriendsModel.h"
#import "ICN_SkillTagModel.h"
#import "ICN_DynActivityModel.h"
#import "ICN_DynGroupModel.h"
#import "ICN_DynWorkExperienceModel.h"
#import "ICN_DynEducationModel.h"


typedef void(^UserBaseMSGBlock)(ICN_DynUserBaseMSGModel *model);
typedef void(^StateArrBlock)(NSArray <ICN_DynStateContentModel *> *array);
typedef void(^CommonFriendsArrBlock)(NSArray<ICN_CommonFriendsModel *> *array);
typedef void(^SkillTagsBlock)(NSArray<ICN_SkillTagModel *> *array);
typedef void(^ActivitiesListBlock)(NSArray<ICN_DynActivityModel *> *array);
typedef void(^GroupListBlock)(NSArray<ICN_DynGroupModel *> *array);
typedef void(^WorkExpListBlock)(NSArray<ICN_DynWorkExperienceModel *> *array);
typedef void(^EducationListClock)(NSArray<ICN_DynEducationModel *> *array);

@interface HRNetworkingManager (AuthorHomePager)


/** 获取该用户的动态
 根据 token 自己的token
 memberId 该用户的用户ID
 需要配置page size
 */
+ (void)requestDynamicStateWithMemberId:(NSString *)memberId
                                   Page:(NSInteger)page
                               PageSize:(NSInteger)size
                                Success:(StateArrBlock)success
                                Failure:(ErrorBlock)failure;

/** 获取自己的动态
 根据 token 自己的token
 memberId 该用户的用户ID
 需要配置page size
 */
+ (void)requestMineDynamicStateWithPage:(NSInteger)page
                               PageSize:(NSInteger)size
                                Success:(StateArrBlock)success
                                Failure:(ErrorBlock)failure;

/** 添加举报接口
 1. memberId ： 举报的动态ID
 2. token ： 用户标识
 3. type ： 预报类型
 4. summary ： 举报内容
 */
+ (void)requestReportWithMatterId:(NSString *)matterId
                             Type:(NSString *)type
                          Summary:(NSString *)summary
                          Success:(SuccessBlock)success
                          Failure:(ErrorBlock)failure;

/** 获取根据用户id获取该用户的基本信息 */
+ (void)requestUserBaseMessageWithUserId:(NSString *)sideId
                                 Success:(UserBaseMSGBlock)success
                                 Failure:(ErrorBlock)failure;

/** 获取用户共同好友列表 */
+ (void)requestUserCommonFriendsWithUserId:(NSString *)sideId
                                   Success:(CommonFriendsArrBlock)success
                                   Failure:(ErrorBlock)failure;

/** 获取用户技能标签列表 通过 token ： 用户标识 ； memberId ： 用户id */
+ (void)requestUserSkillSignWithMemberId:(NSString *)memberId
                                 Success:(SkillTagsBlock)success
                                 Failure:(ErrorBlock)failure;

/** 获取用户活动列表 通过 memberid page*/
+ (void)requestUserAcicvitiesListWithMemberId:(NSString *)memberId
                                         Page:(NSInteger)page
                                      Success:(ActivitiesListBlock)success
                                      Failure:(ErrorBlock)failure;

/** 获取用户小组列表 通过memberid */
+ (void)requestUserGruopsListWithMemberId:(NSString *)memberId
                                  Success:(GroupListBlock)success
                                  Failure:(ErrorBlock)failure;

/** 获取工作经历数据 通过memberid */
+ (void)requestUserWorkExperienceWithMemberId:(NSString *)memberId
                                      Success:(WorkExpListBlock)success
                                      Failure:(ErrorBlock)failure;

/** 获取教育经历数据 通过memberid */
+ (void)requestUserEducationExperienceWithMemberId:(NSString *)memberId
                                           Success:(EducationListClock)success
                                           Failure:(ErrorBlock)failure;

/** 根据用户Id举报用户接口
 参数 memberId (token) 举报人的Id
 reportMemberId 被举报人的Id
 reason 举报原因(type-str) 必选
 summary 举报说明(str)可选
 */
+ (void)requestUserReportWithReportMemberId:(NSString *)reportMemberId
                                     Reason:(NSString *)reason
                                    Summary:(NSString *)summary
                                    Success:(SuccessBlock)success
                                    Failure:(ErrorBlock)failure;

/** 根据用户Id添加好友
 参数 toId 想要添加的用户的id
     fromId 当前用户token
 */
+ (void)requestAddFriendWithToId:(NSString *)toId
                         Success:(SuccessBlock)success
                         Failure:(ErrorBlock)failure;

/** 根据用户Id删除好友
 参数 friendId  想要删除的用户的id
     memberId 当前用户token
 */
+ (void)requestDeleteFriendWithFriendId:(NSString *)friendId
                             Success:(SuccessBlock)success
                             Failure:(ErrorBlock)failure;
@end
