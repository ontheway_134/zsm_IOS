//
//  ICN_DynDetialModel.m
//  ICan
//
//  Created by albert on 2017/1/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DynDetialModel.h"

@implementation ICN_DynDetialModel

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"DYNId" : @"id"}];
}

@end
