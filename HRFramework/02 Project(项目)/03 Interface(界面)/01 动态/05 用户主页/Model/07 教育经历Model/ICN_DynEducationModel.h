//
//  ICN_DynEducationModel.h
//  ICan
//
//  Created by albert on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynEducationModel : BaseOptionalModel

#pragma mark - ---------- 计算属性 ----------
@property (nonatomic , assign)CGFloat CellHeight;


@property (nonatomic , copy)NSString *educationExpId; // 用户教育经历记录ID,
@property (nonatomic , copy)NSString *enrolDate; // 入学时间
@property (nonatomic , copy)NSString *graduationDate; //毕业时间
@property (nonatomic , copy)NSString *school; // 学校
@property (nonatomic , copy)NSString *qualification; // 学历
@property (nonatomic , copy)NSString *major; // 专业

@end
