//
//  ICN_SkillTagModel.m
//  ICan
//
//  Created by albert on 2016/12/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SkillTagModel.h"

@implementation ICN_SkillTagModel


+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"skillSignId"} ];
}

@end
