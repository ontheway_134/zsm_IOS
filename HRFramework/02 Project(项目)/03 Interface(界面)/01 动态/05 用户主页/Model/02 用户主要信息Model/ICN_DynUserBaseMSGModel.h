//
//  ICN_DynUserBaseMSGModel.h
//  ICan
//
//  Created by albert on 2016/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynUserBaseMSGModel : BaseOptionalModel


@property (nonatomic , copy)NSString *isMe; //是不是自己的账号

@property (nonatomic , copy)NSString *memberLogo; // 用户logo
@property (nonatomic , copy)NSString *memberNick; // 用户昵称
@property (nonatomic , copy)NSString *memberGender; // 性别(1男，2女)
@property (nonatomic , copy)NSString *companyName; // 公司名称
@property (nonatomic , copy)NSString *memberPosition; // 职位
@property (nonatomic , copy)NSString *memberProfession; // 行业
@property (nonatomic , copy)NSString *memberSchool; // 学校
@property (nonatomic , copy)NSString *memberMajor; // 专业
@property (nonatomic , copy)NSString *personalizeSignature; // 个性签名
@property (nonatomic , copy)NSString *plusv; // 是否认证用户(加V验证0未验证1已验证)
@property (nonatomic , copy)NSString *staus; // 好友关系类型[没关系则为null]



@end
