//
//  ICN_DynActivityModel.h
//  ICan
//
//  Created by albert on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynActivityModel : BaseOptionalModel

@property (nonatomic , copy)NSString *activityId; // 包含活动ID
@property (nonatomic , copy)NSString *title; // 活动id
@property (nonatomic , copy)NSString *pic; // 图片 -- 全路径
@property (nonatomic , copy)NSString *subnum; // 已报名人数
@property (nonatomic , copy)NSString *cannum; // 可报名人数
@property (nonatomic , copy)NSString *beginDate; // 时间
@property (nonatomic , copy)NSString *price; // 活动价格
@property (nonatomic , copy)NSString *include; // 包含内容



@end
