//
//  ICN_DynActivityModel.m
//  ICan
//
//  Created by albert on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynActivityModel.h"

@implementation ICN_DynActivityModel

// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"activityId"} ];
}


@end
