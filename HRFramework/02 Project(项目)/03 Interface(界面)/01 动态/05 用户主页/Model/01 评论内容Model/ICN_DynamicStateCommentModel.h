//
//  ICN_DynamicStateCommentModel.h
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DynamicStateCommentModel : BaseOptionalModel

@property (nonatomic , assign)CGFloat commentLabelHeight; // 评论label的高度：有默认高度为31.5；


@property (nonatomic , copy)NSString *memberId; // 发布评论的用户ID
@property (nonatomic , copy)NSString *replyer; // 回复的用户ID（0为空
@property (nonatomic , copy)NSString *replyerNick; // @回复用户昵称
@property (nonatomic , copy)NSString *content; // 回复内容
@property (nonatomic , copy)NSString *createDate; // 评论时间（时间戳
@property (nonatomic , copy)NSString *roleId; // 用户角色：1普通，2企业
@property (nonatomic , copy)NSString *memberNick; // 评论用户昵称,
@property (nonatomic , copy)NSString *memberLogo; // 评论用户头像
@property (nonatomic , copy)NSString *workStatus; // 工作标识0否1是
@property (nonatomic , copy)NSString *companyName; // 用户单位名称/企业名称
@property (nonatomic , copy)NSString *memberPosition; // 用户职位
@property (nonatomic , copy)NSString *memberProfession; // 用户行业/企业行业
@property (nonatomic , copy)NSString *memberSchool; // 用户所在学校
@property (nonatomic , copy)NSString *memberMajor; // 用户所学专业
@property (nonatomic , copy)NSString *plusv; // 加V认证0否



@end
