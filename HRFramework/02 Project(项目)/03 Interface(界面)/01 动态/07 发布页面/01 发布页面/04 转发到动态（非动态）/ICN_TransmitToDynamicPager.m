//
//  ICN_TransmitToDynamicPager.m
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_TransmitToDynamicPager.h"

static NSString * const DefaultContent = @"这一刻的想法..."; // 文本框的默认内容

@interface ICN_TransmitToDynamicPager ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *navBackButton; // 自定义 - 导航栏返回按钮
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel; // 自定义导航栏标签
@property (weak, nonatomic) IBOutlet UITextView *contentTextView; // 内容 - 文本域输入框
@property (weak, nonatomic) IBOutlet UIButton *publishButton; // 用于发布的按钮

// 转发内容
@property (weak, nonatomic) IBOutlet UIImageView *transmitContentIcon; // 转发内容的图片
@property (weak, nonatomic) IBOutlet UILabel *transmitContentLabel; // 转发内容正文




@end

@implementation ICN_TransmitToDynamicPager

- (void)viewDidLoad {
    [super viewDidLoad];
    // 隐藏默认导航栏
    [self setHiddenDefaultNavBar:YES];
    // 设置textview代理
    self.contentTextView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------

// 点击了导航栏上的功能键按钮
- (IBAction)clickOnNavButtonAction:(UIButton *)sender {
    if ([sender isEqual:self.navBackButton]) {
        // 点击的是返回按钮
        [self.navigationController popViewControllerAnimated:YES];
    }
}

// 点击了发布按钮
- (IBAction)clickOnPublishBtnAction:(UIButton *)sender {
   // 进行发布逻辑的对应操作
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}





@end
