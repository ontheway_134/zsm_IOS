//
//  ICN_PublishComplainPager.m
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PublishComplainPager.h"
#import "BaseOptionalModel.h"

static NSString * const DefaultContent = @"来匿名吐槽，爆个料..."; // 文本框的默认内容

@interface ICN_PublishComplainPager ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *navBackButton; // 自定义 - 导航栏返回按钮
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel; // 自定义导航栏标签按钮
@property (weak, nonatomic) IBOutlet UITextView *contentTextView; // 内容 - 文本域输入框
@property (weak, nonatomic) IBOutlet UILabel *textCountLabel; // 记录字数标签
@property (weak, nonatomic) IBOutlet UIButton *publishButton; // 用于发布的按钮


@end

@implementation ICN_PublishComplainPager

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置不显示系统导航栏
    [self setHiddenDefaultNavBar:YES];
    // 设置文本框代理
    self.contentTextView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置文本框的圆角
    self.contentTextView.layer.borderColor = [RGB0X(0xe5e5e5) CGColor];
    self.contentTextView.layer.borderWidth = 1.0;
    self.contentTextView.layer.cornerRadius = 5.0;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------


// 点击导航栏 - 自定义按钮的响应方法
- (IBAction)clickOnNavBarButtonAction:(UIButton *)sender {
    if ([sender isEqual:self.navBackButton]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

// 点击了发布按钮
- (IBAction)clickOnPublishButton:(UIButton *)sender {
    
    if ([sender isEqual:self.publishButton]) {
        // 判断是否是有效内容
        if (self.contentTextView.text == nil || [self.contentTextView.text isEqualToString:DefaultContent]) {
            // 输入内容无效
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入有效内容"];
        }else{
            // 进行提交相关的业务逻辑
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                NSDictionary *params = @{
                                         @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken],
                                         @"contant" : self.contentTextView.text
                                         };
                [[HRNetworkingManager alloc] POST_PATH:PATH_PublishComplainState params:params success:^(id result) {
                    BaseOptionalModel *baseModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                    if (baseModel.code == 0) {
                        // 发布成功
                        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"发布成功"];
                        [self.navigationController popViewControllerAnimated:YES];
                    }else{
                        // 发布失败
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:baseModel.info];
                    }
                } failure:^(NSDictionary *errorInfo) {
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络请求失败"];
                }];
                
            }else{
                // 在获取不到token的情况下不操作该方法
                return ;
            }
        }
    }
}



#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (textView.text.length + text.length - range.length < 150) {
        self.textCountLabel.text = SF(@"%lu/150",textView.text.length + text.length - range.length);
        self.contentTextView.textColor = RGB0X(0xb6b6b6);
        return YES;
    }else{
        self.textCountLabel.text = @"输出文字超限";
        self.contentTextView.textColor = [UIColor redColor];
        if ([text isEqualToString:@""]) {
            return YES;
        }else
            return NO;
    }
    return YES;
}

@end
