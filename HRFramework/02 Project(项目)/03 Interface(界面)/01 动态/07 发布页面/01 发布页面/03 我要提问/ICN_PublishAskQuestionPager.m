//
//  ICN_PublishAskQuestionPager.m
//  ICan
//
//  Created by albert on 2017/3/5.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PublishAskQuestionPager.h"

static NSString * const DefaultContent = @"请写下你的问题，并用问号结尾"; // 文本框的默认内容

@interface ICN_PublishAskQuestionPager ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *navBackButton; // 自定义 - 导航栏返回按钮
@property (weak, nonatomic) IBOutlet UILabel *navTitleLabel; // 自定义导航栏标签
@property (weak, nonatomic) IBOutlet UITextField *titleTextField; // 输入标题文本框
@property (weak, nonatomic) IBOutlet UITextView *contentTextView; // 内容 - 文本域输入框
@property (weak, nonatomic) IBOutlet UIButton *payButton; // 打赏按钮
@property (weak, nonatomic) IBOutlet UIButton *publishButton; // 用于发布的按钮

@end

@implementation ICN_PublishAskQuestionPager

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setHiddenDefaultNavBar:YES];
    self.contentTextView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------
// 点击当前页面按钮的交互
- (IBAction)clickOnIButtonAction:(UIButton *)sender {
    if ([sender isEqual:self.navBackButton]) {
        // 点击的是返回按钮
        [self.navigationController popViewControllerAnimated:YES];
    }
    if ([sender isEqual:self.payButton]) {
        // 点击的是打赏按钮
    }
    if ([sender isEqual:self.publishButton]) {
        // 点击的是发布按钮
        if (self.titleTextField.text == nil || [self.titleTextField.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入标题"];
            return ;
        }
        if ([self.contentTextView.text isEqualToString:DefaultContent] || [self.contentTextView.text isEqualToString:@""]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入有效内容"];
            return ;
        }
        // 进行提交操作
    }
    
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}




@end
