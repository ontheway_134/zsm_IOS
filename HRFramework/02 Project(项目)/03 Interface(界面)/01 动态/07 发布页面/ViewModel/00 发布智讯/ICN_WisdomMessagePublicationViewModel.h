//
//  ICN_WisdomMessagePublicationViewModel.h
//  ICan
//
//  Created by albert on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ICN_WisdomMessagePublicationDelegate <NSObject>

- (void)responseWithMessageUploadSuccess:(BOOL)isSuccess
                               ErrorCode:(NSInteger)errorCode
                                    Info:(NSDictionary *)info;

@end

@interface ICN_WisdomMessagePublicationViewModel : NSObject

@property (nonatomic , weak)id<ICN_WisdomMessagePublicationDelegate> delegate;



- (void)postWisdomMessageWithToken:(NSString *)token Title:(NSString *)title
                           Content:(NSString *)content Pic:(NSString *)pic;

/** 上传图片数组的方法 */
- (void)upLoadImages:(NSArray <UIImage *>*)images;

@end
