//
//  ICN_UserPostMsgListPagerViewController.m
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserPostMsgListPagerViewController.h"
#import "ICN_ComplainContentCell.h" // 吐槽列表页Cell 180（默认高度）
#import "ICN_UserListQuestionContentCell.h" // 提问列表页Cell 210（默认高度）


@interface ICN_UserPostMsgListPagerViewController ()<UITableViewDelegate , UITableViewDataSource>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UILabel *titleLabel; // 标题
@property (weak, nonatomic) IBOutlet UILabel *noContentMsgLabel; // 没有内容信息的提示标签
@property (weak, nonatomic) IBOutlet UIButton *dynamicStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *wisdonStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *complainStateBtn;
@property (weak, nonatomic) IBOutlet UIButton *askActionStateBtn;


// 用户计算位置的背景
@property (weak, nonatomic) IBOutlet UIView *delertSegmentView; // 菜单分隔栏 -- 下面的部分就是tableView
@property (weak, nonatomic) IBOutlet UIView *selectedDelertBackGround; // 选项卡背景板


#pragma mark - ---------- 其他属性 ----------

@property (nonatomic , strong)UIButton *currentSelectedBtn; // 当前选中的按钮
@property (nonatomic , strong)UITableView *tableView; // 当前页面的tableView
@property (nonatomic , strong)UIView *buttonSignView; // 设置按钮下方的切换栏


@end

@implementation ICN_UserPostMsgListPagerViewController

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])]; // 注册吐槽Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_UserListQuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])]; // 注册提问Cell
        
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        // 在使用group的时候设置的确保section间隔取消的方法
        _tableView.sectionFooterHeight = 1;
        [self.view addSubview:_tableView];
        // 设置在tableview的数据显示不全的时候不显示多余的行分割的方法
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        // 使用约束设置tableView
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.delertSegmentView.mas_bottom);
            make.bottom.equalTo(self.view);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
        }];
    }
    return _tableView;
}

- (UIView *)buttonSignView{
    if (_buttonSignView == nil) {
        _buttonSignView = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonSignView.backgroundColor = RGB0X(0x009dff);
        [self.selectedDelertBackGround addSubview:_buttonSignView];
    }
    return _buttonSignView;
}



#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置隐藏默认的导航栏
    [self setHiddenDefaultNavBar:YES];
    // 配置tableView
    [self tableView];
    // 配置默认的选中按钮
    UIButton *button = [self.selectedDelertBackGround viewWithTag:self.currentSelectedTag];
    // 获取到正确的button的时候将其设置为选中状态
    if (button) {
        button.selected = YES;
        self.currentSelectedBtn = button;
    }else{
        HRLog(@"error===未获取到正确的切换按钮");
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 设置默认的选中的按钮下面的切换选项卡
    [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
        make.bottom.equalTo(self.selectedDelertBackGround);
        make.centerX.equalTo(self.currentSelectedBtn);
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 根据选中的按钮的tag来切换选项卡的方法 -- 忽略页面刚载入时的处理
- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    // 根据tag计算选中的是哪个按钮 - 并将其他按钮的选中状态改变
    if (tag != self.currentSelectedBtn.tag) {
        // 两次选中的按钮的tag不一样
        
        // 1. 设置之前选中的按钮取消选择
        self.currentSelectedBtn.selected = NO;
        // 2. 判断选中的按钮并设置currentSelectedBtn 为选中的按钮
        // 3. 设置选中的按钮的状态为yes
        //test=== 暂时设置动态和智讯的时候tableView隐藏;现阶段刷新按钮根据tag值来
        switch (tag) {
            case PRIVATE_DynamicBtn:{
                self.dynamicStateBtn.selected = YES;
                self.currentSelectedBtn = self.dynamicStateBtn;
                self.tableView.hidden = YES;
                break;
            }
            case PRIVATE_WisdomBtn:{
                self.wisdonStateBtn.selected = YES;
                self.currentSelectedBtn = self.wisdonStateBtn;
                self.tableView.hidden = YES;
                break;
            }
            case PRIVATE_TopicBtn:{
                self.complainStateBtn.selected = YES;
                self.currentSelectedBtn = self.complainStateBtn;
                [self.tableView reloadData];
                break;
            }
            case PRIVATE_AskActionBtn:{
                self.askActionStateBtn.selected = YES;
                self.currentSelectedBtn = self.askActionStateBtn;
                [self.tableView reloadData];
                break;
            }
            default:
                break;
        }
        
        // 4. 设置切换选中状态的标签的跟随移动
        [UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.buttonSignView.center;
            center.x = self.currentSelectedBtn.centerX;
            self.buttonSignView.center = center;
        } completion:^(BOOL finished) {
            [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.selectedDelertBackGround);
                make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
                make.centerX.equalTo(self.currentSelectedBtn);
            }];
        }];
    }
    // 5. 设置ViewModel中选中的刷新类型
//    self.viewModel.currentSelectedType = tag;
    
    // 6. 根据选中的内容刷新页面
//    [self.tableView.mj_header beginRefreshing];
    
}



#pragma mark - ---------- IBAction ----------

// 点击返回按钮
- (IBAction)clickOnBackAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// 选中切换状态按钮的方法
- (IBAction)clickOnStateSelectedBtnAction:(UIButton *)sender {
    // 根据按钮的tag进行对应的切换操作
    [self changeSelectedContentLabelStatusWithSenderTage:sender.tag];
}

#pragma mark - ---------- 代理 ----------

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // 默认返回十条用于测试假数据
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 根据选中按钮的tag来判断返回的是什么内容
    switch (self.currentSelectedBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 吐槽
            ICN_ComplainContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
            return cell;
        }
        case PRIVATE_AskActionBtn:{
            // 提问
            ICN_UserListQuestionContentCell * cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_UserListQuestionContentCell class])];
            return cell;
        }
            
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (self.currentSelectedBtn.tag) {
        case PRIVATE_DynamicBtn:{
            return 120;
            break;
        }
        case PRIVATE_WisdomBtn:{
            return 120;
            break;
        }
        case PRIVATE_TopicBtn:{
            // 返回吐槽的高度
            return 180;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 返回提问的高度
            return 210;
            break;
        }
        default:
            return 120;
            break;
    }
    
    return 120;
}





@end
