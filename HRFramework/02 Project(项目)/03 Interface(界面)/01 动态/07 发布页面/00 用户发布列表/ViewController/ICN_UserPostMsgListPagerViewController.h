//
//  ICN_UserPostMsgListPagerViewController.h
//  ICan
//
//  Created by albert on 2017/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//  用于显示用户发布页面的我的发布列表页内容

#import "BaseViewController.h"

@interface ICN_UserPostMsgListPagerViewController : BaseViewController

@property (nonatomic , assign)NSInteger currentSelectedTag; // 当前选中的tag值 - 从用户列表页面进入时候给的初始值


@end
