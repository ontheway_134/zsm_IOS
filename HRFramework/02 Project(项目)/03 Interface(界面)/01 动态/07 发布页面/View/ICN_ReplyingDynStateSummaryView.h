//
//  ICN_ReplyingDynStateSummaryView.h
//  ICan
//
//  Created by albert on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ReplyingDynStateSummaryView : UIView

@property (weak, nonatomic) IBOutlet UILabel *p_UserMarkedLabel; // 标记用户观看按钮 - @“用户名”

@property (weak, nonatomic) IBOutlet UILabel *p_PromulgatorLabel; // 发布者标签

@property (weak, nonatomic) IBOutlet UIImageView *p_DynStateIcon; // 动态贴图

@property (weak, nonatomic) IBOutlet UILabel *p_DynStateContentLabel; // 动态内容

@end
