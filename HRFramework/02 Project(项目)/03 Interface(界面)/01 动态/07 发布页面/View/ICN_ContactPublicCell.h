//
//  ICN_ContactPublicCell.h
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BtnSelectedBlock)(BOOL selected);

@interface ICN_ContactPublicCell : UITableViewCell

@property (nonatomic , copy)BtnSelectedBlock block;
@property (nonatomic , assign)BOOL selectedAll; // 是否全选

- (void)callWhileBtnSelectedAll:(BtnSelectedBlock)block;


@end
