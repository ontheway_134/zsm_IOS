//
//  ICN_ContactPublicCell.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ContactPublicCell.h"

@interface ICN_ContactPublicCell ()



@property (nonatomic , strong)UILabel *p_TitleLabel; // 当前页面标签
@property (nonatomic , strong)UIButton *p_SelectBtn; // 选中按钮
@property (nonatomic , strong)UIView *backView; // 分隔栏

@end

@implementation ICN_ContactPublicCell

- (UILabel *)p_TitleLabel{
    if (_p_TitleLabel == nil) {
        _p_TitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _p_TitleLabel.text = @"选择";
        _p_TitleLabel.userInteractionEnabled = YES;
        _p_TitleLabel.textColor = [UIColor blackColor];
        _p_TitleLabel.font = [UIFont systemFontOfSize:13.0];
        
        // 添加到视图
        [self.contentView addSubview:_p_TitleLabel];
    }
    return _p_TitleLabel;
}

- (UIButton *)p_SelectBtn{
    if (_p_SelectBtn == nil) {
        _p_SelectBtn = [[UIButton alloc] initWithFrame:CGRectZero];
        [_p_SelectBtn setImage:[[UIImage imageNamed:@"未选择"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_p_SelectBtn setImage:[UIImage imageNamed:@"选择"] forState:UIControlStateSelected];
        [_p_SelectBtn addTarget:self action:@selector(clickOnSelectedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        _p_SelectBtn.selected = NO;
        
        // 添加到视图
        [self.contentView addSubview:_p_SelectBtn];
    }
    return _p_SelectBtn;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self p_SelectBtn];
        [self p_TitleLabel];
        // 默认设置全不选
        self.selectedAll = NO;
        _backView = [[UIView alloc] initWithFrame:CGRectZero];
        _backView.backgroundColor = RGB0X(0xf0f0f0);
        [self.contentView addSubview:_backView];
    }
    return self;
}

- (void)setSelectedAll:(BOOL)selectedAll{
    _selectedAll = selectedAll;
    if (self.p_SelectBtn) {
        self.p_SelectBtn.selected = _selectedAll;
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    [self.p_SelectBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10.0);
        make.centerY.equalTo(self.contentView);
    }];
    [self.p_TitleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10.0);
        make.centerY.equalTo(self.contentView);
    }];
    
    [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView);
        make.width.mas_equalTo(self.contentView.width);
        make.height.mas_equalTo(5.0);
    }];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)callWhileBtnSelectedAll:(BtnSelectedBlock)block{
    self.block = block;
}

- (void)clickOnSelectedBtnAction:(UIButton *)sender{
    if (self.block) {
        self.selectedAll = !self.selectedAll;
        sender.selected = self.selectedAll;
        self.block(self.selectedAll);
    }
}

@end
