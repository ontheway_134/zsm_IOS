//
//  HRNetworkingManager+ICN_Publication.m
//  ICan
//
//  Created by albert on 2016/12/16.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "HRNetworkingManager+ICN_Publication.h"
#import "BaseOptionalModel.h"



@implementation HRNetworkingManager (ICN_Publication)

/** 用于发布多张图片 / 单张图片的网络请求 */
+ (void)uploadImagesWithImageArr:(NSArray<UIImage *> *)imageArr Success:(UpLoadImageBlock)success Failure:(ErrorBlock)failure{
    
    [[[HRNetworkingManager alloc] init] UPLOAD_IMAGES:imageArr Path:PATH_MultipleImagesUpload progress:^(int64_t bytesProgress, int64_t totalBytesProgress) {
        
    } success:^(id responseObject) {
        ICN_CommonImagesLoadModel *model = [[ICN_CommonImagesLoadModel alloc] initWithDictionary:responseObject error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}


/**
 1. token  用户密令		是
 2. content 动态内容		是
 3. pic    图片路径		否
 4. notIds  禁止查看用户ID（用逗号分隔）  否
 */
+ (void)postDynamicStatePublicationWithContent:(NSString *)content Pic:(NSString *)pics NotIds:(NSString *)notIds Success:(WisdomUpLoadBlock)success Failure:(ErrorBlock)failure{
    
    NSString *token = [USERDEFAULT valueForKeyPath:HR_CurrentUserToken];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (pics) {
        [params setValue:pics forKey:@"pic"];
    }
    if (content == nil) {
        return ;
    }else{
        [params setValue:content forKey:@"content"];
    }
    if (notIds) {
        [params setObject:notIds forKey:@"notIds"];
    }
    [params setValue:token forKey:@"token"];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicPublication params:params success:^(id result) {
        HRLog(@"发布动态 - 网络请求成功");
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (model.code == 0) {
            HRLog(@"发布动态 - 动态发布成功");
        }else if (model.code == 1){
            HRLog(@"发布动态 - 动态发布失败");
        }else{
            HRLog(@"发布动态 - 用户未登陆");
        }
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        HRLog(@"发布动态 - 网络获取失败");
        failure(errorInfo);
    }];
    
}

/** 在图片上传完成后 -- 上传智讯内容的网络请求 */
+ (void)postWisdomMessagePublicationTextContentWithToken:(NSString *)token Title:(NSString *)title Content:(NSString *)content Pic:(NSString *)pic Success:(WisdomUpLoadBlock)success Failure:(ErrorBlock)failure{
    token = [USERDEFAULT valueForKeyPath:HR_CurrentUserToken];
    NSMutableDictionary *params = [@{
                                    @"token" : token,
                                    @"title" : title,
                                    @"content" : content,
                                    } mutableCopy];
    if (pic) {
        [params setObject:pic forKey:@"pic"];
    }
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_WisdomMessagePublication params:[NSDictionary dictionaryWithDictionary:params] success:^(id result) {
        BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        success(model);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
}

+ (void)requestCurrentUserFriendsWithPage:(NSInteger)page Success:(ComFriendsBlcok)success Failure:(ErrorBlock)failure{
    
    NSString * token = [USERDEFAULT valueForKeyPath:HR_CurrentUserToken];
    if (token == nil) {
        token = @"";
    }
    NSMutableDictionary *params = [@{
                                     @"token" : token,
                                     @"page" : SF(@"%ld",(long)page),
                                     @"status" : @"2",
                                     } mutableCopy];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynFriendsList params:params success:^(id result) {
        NSMutableArray <ICN_NewFrident *>*array = [NSMutableArray array];
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 数据获取成功
            for (NSDictionary *item in [result valueForKey:@"result"]) {
                ICN_NewFrident *model = [[ICN_NewFrident alloc] initWithDictionary:item error:nil];
                model.selected = NO;
                model.code = firstModel.code;
                [array addObject:model];
            }
        }else{
            ICN_NewFrident *model = [[ICN_NewFrident alloc] init];
            model.code = firstModel.code;
            model.info = firstModel.info;
            [array addObject:model];
        }
        success(array);
    } failure:^(NSDictionary *errorInfo) {
        failure(errorInfo);
    }];
    
}



@end
