//
//  ICN_ReviewPublication.h
//  ICan
//
//  Created by albert on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_DynStateContentModel;
@interface ICN_ReviewPublication : BaseViewController

@property (nonatomic , strong)ICN_DynStateContentModel *model;

@end
