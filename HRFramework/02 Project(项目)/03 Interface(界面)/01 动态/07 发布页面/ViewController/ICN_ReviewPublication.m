//
//  ICN_ReviewPublication.m
//  ICan
//
//  Created by albert on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ReviewPublication.h"
#import "ICN_DynStateContentModel.h"
#import "HRNetworkingManager+DynFirstPager.h"

@interface ICN_ReviewPublication ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *reviewContentTextView; // 转发评论内容

@property (weak, nonatomic) IBOutlet UILabel *remindToLookLabel; // @谁 label

@property (weak, nonatomic) IBOutlet UILabel *authorStatusLabel; // 动态所有者的公司与职位

@property (weak, nonatomic) IBOutlet UIImageView *dynStateIcon; // 动态贴图

@property (weak, nonatomic) IBOutlet UILabel *dynStateContentLabel; // 动态内容


@end

@implementation ICN_ReviewPublication

- (void)setModel:(ICN_DynStateContentModel *)model{
    _model = model;
    self.remindToLookLabel.text = SF(@"@%@",_model.memberNick);
    if (_model.workStatus == 0) {
        // 未就业
        self.authorStatusLabel.text = SF(@"%@|%@",_model.memberSchool , _model.memberMajor);
    }else{
        self.authorStatusLabel.text = SF(@"%@|%@",_model.companyName , _model.memberPosition);
    }
    self.dynStateContentLabel.text = _model.content;
    NSArray *array = [self.model.pic componentsSeparatedByString:@","];
    if (array) {
        [self.dynStateIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(array.firstObject)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self setModel:self.model];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.reviewContentTextView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}


- (IBAction)clickOnReplyDynamicStateAction:(UIButton *)sender {
    
    if (self.reviewContentTextView.text == nil || [self.reviewContentTextView.text isEqualToString:@""] || [self.reviewContentTextView.text isEqualToString:@"这一刻的想法..."]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加您的想法"];
    }else{
        [HRNetworkingManager publichReplayDyanmicStateWithMatterId:self.model.matterId Summary:self.reviewContentTextView.text Success:^(BaseOptionalModel *model) {
            // code 0 : 成功 2： 用户未登录 1 : 因为某些原因转发动态失败
            if (model.code == 0) {
                [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"动态转发成功"];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:model.info];
            }
        } Failure:^(NSDictionary *errorInfo) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络不良请稍后发送"];
        }];
    }
    
}

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"这一刻的想法..."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text ==nil || [textView.text isEqualToString:@""]) {
        textView.text = @"这一刻的想法...";
        textView.textColor = RGB0X(0xB6B6B6);
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
