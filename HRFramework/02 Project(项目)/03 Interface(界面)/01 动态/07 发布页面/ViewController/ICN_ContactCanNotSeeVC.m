//
//  ICN_ContactCanNotSeeVC.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ContactCanNotSeeVC.h"
#import "ICN_CommonSearchBar.h" // 搜索栏控件
#import "ICN_ContactPublicCell.h" // 对联系人公开选项Cell
#import "ICN_ContactSelectedCell.h" // 联系人选择Cell
#import "ICN_DynamicStatePublicationViewModel.h" // vc的ViewModel

@interface ICN_ContactCanNotSeeVC ()<UITableViewDelegate , UITableViewDataSource , CommonSearchBarDelegate , ICN_DynamicStatePublicationDelegate>

@property (nonatomic , strong)UITableView *tableView; // 选择联系人列表
@property (nonatomic , strong)ICN_CommonSearchBar *searchBar; // 搜索栏

@end

@implementation ICN_ContactCanNotSeeVC

#pragma mark - ---------- 懒加载 ----------

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundView = nil;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[ICN_ContactSelectedCell class] forCellReuseIdentifier:NSStringFromClass([ICN_ContactSelectedCell class])];
        [_tableView registerClass:[ICN_ContactPublicCell class] forCellReuseIdentifier:NSStringFromClass([ICN_ContactPublicCell class])];
        
        [self.view addSubview:_tableView];
        [self configTableViewRefreshHeaderFooterView];
    }
    return _tableView;
}

- (ICN_CommonSearchBar *)searchBar{
    if (_searchBar == nil) {
        _searchBar = XIB(ICN_CommonSearchBar);
        _searchBar.delegate = self;
        [self.view addSubview:_searchBar];
    }
    return _searchBar;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 添加控件
    [self tableView];
    [self searchBar];
    [self viewModel];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.viewModel.delegate = self;
    // 布局
    [self configUIWhileViewWillAppear];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.viewModel.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 在视图将要出现的时候布局UI
- (void)configUIWhileViewWillAppear{
    [self.searchBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(self.view.width, 50.0));
        make.left.equalTo(self.view);
    }];
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBar.mas_bottom);
        make.width.equalTo(self.view.mas_width);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
}

- (void)configTableViewRefreshHeaderFooterView{
 
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self.viewModel refreshCurrentUserFriendsList];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        [self.viewModel loadCurrentUserFriendsList];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}




#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_DynamicStatePublicationDelegate ---

- (void)responseWithUserFriendsListSuccess:(BOOL)isSuccess Error:(NSString *)error{
    if (isSuccess) {
        [self.tableView reloadData];
        [self endRefreshWithTableView:self.tableView];
    }else{
        [self endRefreshWithTableView:self.tableView];
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

#pragma mark --- CommonSearchBarDelegate ---

- (void)searchBarDidBeginEditingWithText:(NSString *)content{
    
}


/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑 设置点击的列表为选中状态，然后刷新数据
    if (indexPath.row == 0) {
        // 公开按钮
        BOOL selected = ![(ICN_ContactPublicCell *)[tableView cellForRowAtIndexPath:indexPath] selectedAll];
        for (ICN_NewFrident *model in self.viewModel.friendModelsArr) {
            model.selected = selected;
            [(ICN_ContactPublicCell *)[tableView cellForRowAtIndexPath:indexPath] setSelectedAll:selected];
        }
        [self.viewModel updateSelectedFriendModelsList];
        [self.tableView reloadData];
    }else{
        ICN_NewFrident *model = self.viewModel.friendModelsArr[indexPath.row - 1];
        model.selected = !model.isSelected;
        [self.viewModel updateSelectedFriendModelsList];
        [self.tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.viewModel.friendModelsArr.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        ICN_ContactPublicCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ContactPublicCell class])];
        [cell callWhileBtnSelectedAll:^(BOOL selected) {
            for (ICN_NewFrident *model in self.viewModel.friendModelsArr) {
                model.selected = selected;
            }
            [self.viewModel updateSelectedFriendModelsList];
            [self.tableView reloadData];
        }];
        return cell;
    }else{
        ICN_ContactSelectedCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ContactSelectedCell class])];
        cell.model = self.viewModel.friendModelsArr[indexPath.row - 1];
        return cell;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return 45.0;
    }else{
        return 55.0;
    }
    
    return 0.0;
}



@end
