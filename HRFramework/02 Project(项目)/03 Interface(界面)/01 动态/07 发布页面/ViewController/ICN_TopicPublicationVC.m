//
//  ICN_TopicPublicationVC.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_TopicPublicationVC.h"
#import "TZImagePickerController.h"
#import "TZImageManager.h"
#import "ICN_WisdomMessagePublicationViewModel.h"

static const NSInteger ICN_TopicImageAddMaxCount = 6; // 默认添加图片的最大数量

@interface ICN_TopicPublicationVC ()<TZImagePickerControllerDelegate , UITextFieldDelegate , UITextViewDelegate , ICN_WisdomMessagePublicationDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITextField *p_TitleTextF; // 标题文本框

@property (weak, nonatomic) IBOutlet UITextView *p_ContentTextView; // 正文文本域

@property (weak, nonatomic) IBOutlet UIScrollView *p_ImageScroll; // 图片选择器背景

@property (weak, nonatomic) IBOutlet UILabel *p_ImageCountLabel; // 限制选择图片数量label

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;

#pragma mark - ---------- 私有属性 ----------

@property (nonatomic , strong)TZImagePickerController *p_ImagePickerVC; // 跳转的图片选择器页面vc
@property (nonatomic , strong)NSMutableArray * imageListArr; // 当前图片列表数组
@property (nonatomic , strong)UIButton *addImageBtn; // 添加图片用btn
@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewListArr; // 当前相框列表

@property (nonatomic , strong)ICN_WisdomMessagePublicationViewModel *viewmodel;


@end

@implementation ICN_TopicPublicationVC

#pragma mark - ---------- 懒加载 ----------

- (ICN_WisdomMessagePublicationViewModel *)viewmodel{
    if (_viewmodel == nil) {
        _viewmodel = [[ICN_WisdomMessagePublicationViewModel alloc] init];
        _viewmodel.delegate = self;
    }
    return _viewmodel;
}

- (UIButton *)addImageBtn{
    if (_addImageBtn == nil) {
        _addImageBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
        [_addImageBtn setBackgroundImage:[[UIImage imageNamed:@"加"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_p_ImageScroll addSubview:_addImageBtn];
        [_addImageBtn addTarget:self action:@selector(clickImagePickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageBtn;
}

- (NSMutableArray *)imageListArr{
    if (_imageListArr == nil) {
        _imageListArr = [NSMutableArray array];
    }
    return _imageListArr;
}

- (NSMutableArray<UIImageView *> *)imageViewListArr{
    if (_imageViewListArr == nil) {
        _imageViewListArr = [NSMutableArray array];
    }
    return _imageViewListArr;
}

- (TZImagePickerController *)p_ImagePickerVC{
    if (_p_ImagePickerVC == nil) {
        
        NSInteger maxCountInRow = [UIScreen mainScreen].bounds.size.width / 100.0;
        _p_ImagePickerVC = [[TZImagePickerController alloc] initWithMaxImagesCount:ICN_TopicImageAddMaxCount columnNumber:maxCountInRow delegate:self pushPhotoPickerVc:NO];
        _p_ImagePickerVC.alwaysEnableDoneBtn = NO;
        _p_ImagePickerVC.sortAscendingByModificationDate = NO;
        _p_ImagePickerVC.allowPickingVideo = NO;
        _p_ImagePickerVC.hideWhenCanNotSelect = YES;
    }
    return _p_ImagePickerVC;
}


#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self viewmodel];
    self.naviTitle = @"发布";
    self.p_TitleTextF.delegate = self;
    self.p_ContentTextView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self addImageBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnPublishAction:(UIButton *)sender {
    
    // 判断有没有标题，没有标题不允许上传
    if (self.p_TitleTextF.text == nil || [self.p_TitleTextF.text isEqualToString:@""]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加标题后再发布智讯"];
    }else{
        if (self.p_ContentTextView.text == nil || [self.p_ContentTextView.text isEqualToString:@"正文内容"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加正文后再发布智讯"];
        }else{
            // 设置在该次请求有结果之前button不能点击
            sender.userInteractionEnabled = NO;
            if (self.imageListArr.count > 0) {
                [self.viewmodel upLoadImages:self.imageListArr];
                [MBProgressHUD ShowProgressToSuperView:self.view Message:@"智讯上传中"];
            }else{
                // 纯文本发布 - 测试
                [self postSingleTextWisdomMessageWithInfo:nil];
                [MBProgressHUD ShowProgressToSuperView:self.view Message:@"正在发布"];
            }
        }
    }
}

#pragma mark - ---------- 私有方法 ----------

- (void)postSingleTextWisdomMessageWithInfo:(NSDictionary *)info{
    NSString *token = @"ssss";
    NSString *title = self.p_TitleTextF.text;
    NSString *content = self.p_ContentTextView.text;
    NSString *pic = [info valueForKey:@"fileName"];
    [self.viewmodel postWisdomMessageWithToken:token Title:title Content:content Pic:pic];
}

- (void)clickImagePickAction:(UIButton *)sender{
    [self presentViewController:self.p_ImagePickerVC animated:YES completion:nil];
}

// 对于图片列表的重新布局 - 在图片数组更新之后
- (void)configImageViewlistAfterImageListUpdate{
    
    for (NSInteger i = 0; i < self.imageListArr.count; i++) {
        if (i < self.imageViewListArr.count) {
            self.imageViewListArr[i].image = self.imageListArr[i];
        }else{
            UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageview.image = self.imageListArr[i];
            imageview.userInteractionEnabled = YES;
            [self.p_ImageScroll addSubview:imageview];
            [self.imageViewListArr addObject:imageview];
        }
        
    }
    
    [self updateImageViewFromIndex:0 EndIndex:self.imageListArr.count - 1];
    if (self.imageViewListArr.count > self.imageListArr.count) {
        NSMutableArray *removeItemsArr = [NSMutableArray array];
        for (NSInteger i = self.imageListArr.count; i < self.imageViewListArr.count; i++) {
            [self.imageViewListArr[i] removeFromSuperview];
            [removeItemsArr addObject:self.imageViewListArr[i]];
        }
        [self.imageViewListArr removeObjectsInArray:removeItemsArr];
    }
    
    BOOL isallowContentSize = self.imageViewListArr.count * 70.0 > self.p_ImageScroll.size.width - 80.0;
    if (isallowContentSize) {
        CGFloat width =  self.imageViewListArr.count * 70.0 + 80.0;
        CGFloat height = self.p_ImageScroll.height;
        self.p_ImageScroll.contentSize = CGSizeMake(width, height);
    }else{
        self.p_ImageScroll.contentSize = CGSizeZero;
    }
    
}


- (void)updateImageViewFromIndex:(NSInteger)startIndex
                        EndIndex:(NSInteger)endIndex{
    for (NSInteger i = startIndex; i <= endIndex; i++) {
        if (i == 0) {
            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.p_ImageScroll).offset(10.0);
                make.top.equalTo(self.p_ImageScroll).offset(10.0);
                make.size.mas_equalTo(CGSizeMake(60, 60));
            }];
        }else{
            UIImageView *lastItem = self.imageViewListArr[i - 1];
            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastItem.mas_right).offset(10.0);
                make.top.equalTo(lastItem);
                make.size.equalTo(lastItem);
            }];
        }
    }
    CGRect frame = CGRectMake(self.imageListArr.count * 70.0 + 10.0, 10.0, self.addImageBtn.width, self.addImageBtn.height);
    self.addImageBtn.frame = frame;
}


#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_WisdomMessagePublicationDelegate ---

- (void)responseWithMessageUploadSuccess:(BOOL)isSuccess ErrorCode:(NSInteger)errorCode Info:(NSDictionary *)info{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.publishBtn.userInteractionEnabled = YES;
    if (errorCode == ICN_FileLoadSuccess) {
        // 获取图片上传成功
        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"内容上传中"];
        // 开始post智讯内容
        [self postSingleTextWisdomMessageWithInfo:info];
        
    }
    
    switch (errorCode) {
        case ICN_NetSuccess:
        case ICN_FileLoadSuccess:
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"上传成功"];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case ICN_PostFailureWithNetState:
        case ICN_LoadFaileFailureWithNetState:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
            break;
        }
        case ICN_PostFailureWithMessageError:
        case ICN_LoadFaileFailureWithMessageError:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"服务器异常"];
            break;
        }
        default:
            break;
    }
    
    
}


#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (textField.text == nil || [textField.text isEqualToString:@""]) {
        textField.text = @"";
        textField.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.text == nil || [textField.text isEqualToString:@""]) {
        textField.text = nil;
        textField.textColor = RGB0X(0xb6b6b6);
    }
}




#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView isFirstResponder]) {
        if ([textView.text isEqualToString:@"正文内容"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"正文内容";
        textView.textColor = RGB0X(0xb6b6b6);
    }
}

#pragma mark --- TZImagePickerControllerDelegate ---
// The picker should dismiss itself; when it dismissed these handle will be called.
// You can also set autoDismiss to NO, then the picker don't dismiss itself.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的handle
// 你也可以设置autoDismiss属性为NO，选择器就不会自己dismis了
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    if (isSelectOriginalPhoto) {
        // 当选中原图的时候获取到原图并添加到图片展示列表中
        [[TZImageManager manager] getOriginalPhotoWithAsset:assets.firstObject completion:^(UIImage *photo, NSDictionary *info) {
            [self.imageListArr addObject:photo];
        }];
        
    }else{
        
        if (_imageListArr) {
            [_imageListArr removeAllObjects];
        }
        [self.imageListArr addObjectsFromArray:photos];
        
        [self configImageViewlistAfterImageListUpdate];
    }
    
}



@end
