//
//  ICN_DynamicStatePublicationVC.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStatePublicationVC.h"
#import "TZImagePickerController.h"
#import "TZImageManager.h"
#import "ICN_DynamicStatePublicationViewModel.h"

#pragma mark - ---------- tableView相关头文件 ----------
#import "ICN_HistoryContactCell.h" // 联系人Cell
#import "ICN_ShareToContentCell.h" // 谁不可看Cell

#pragma mark - ---------- 跳转页面 ----------
#import "ICN_ContactCanNotSeeVC.h" // 谁不可看页面


static const NSInteger ICN_TopicImageAddMaxCount = 6; // 默认添加图片的最大数量
static NSString * const DefaultContent = @"这一刻的想法..."; // 文本框的默认内容

@interface ICN_DynamicStatePublicationVC ()<TZImagePickerControllerDelegate  , UITextViewDelegate , UITableViewDelegate , UITableViewDataSource , ICN_DynamicStatePublicationDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UITextView *p_ContentTextView; // 正文文本域

@property (weak, nonatomic) IBOutlet UIScrollView *p_ImageScroll; // 图片选择器背景

@property (weak, nonatomic) IBOutlet UILabel *p_ImageCountLabel; // 限制选择图片数量label

@property (weak, nonatomic) IBOutlet UILabel *p_ContentTextCountLabel; // 正文字数限制label

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 谁不可看列表

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;



#pragma mark - ---------- 私有属性 ----------

@property (nonatomic , strong)TZImagePickerController *p_ImagePickerVC; // 跳转的图片选择器页面vc
@property (nonatomic , strong)NSMutableArray * imageListArr; // 当前图片列表数组
@property (nonatomic , strong)UIButton *addImageBtn; // 添加图片用btn
@property (nonatomic , strong)NSMutableArray<UIImageView *> *imageViewListArr; // 当前相框列表
@property (nonatomic , strong)ICN_DynamicStatePublicationViewModel *viewModel;


@end

@implementation ICN_DynamicStatePublicationVC

#pragma mark - ---------- 懒加载 ----------

- (UIButton *)addImageBtn{
    if (_addImageBtn == nil) {
        _addImageBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 60, 60)];
        [_addImageBtn setBackgroundImage:[[UIImage imageNamed:@"加"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
        [_p_ImageScroll addSubview:_addImageBtn];
        [_addImageBtn addTarget:self action:@selector(clickImagePickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addImageBtn;
}

- (NSMutableArray *)imageListArr{
    if (_imageListArr == nil) {
        _imageListArr = [NSMutableArray array];
    }
    return _imageListArr;
}

- (NSMutableArray<UIImageView *> *)imageViewListArr{
    if (_imageViewListArr == nil) {
        _imageViewListArr = [NSMutableArray array];
    }
    return _imageViewListArr;
}

- (TZImagePickerController *)p_ImagePickerVC{
    if (_p_ImagePickerVC == nil) {
        
        NSInteger maxCountInRow = [UIScreen mainScreen].bounds.size.width / 100.0;
        _p_ImagePickerVC = [[TZImagePickerController alloc] initWithMaxImagesCount:ICN_TopicImageAddMaxCount columnNumber:maxCountInRow delegate:self pushPhotoPickerVc:NO];
        _p_ImagePickerVC.alwaysEnableDoneBtn = NO;
        _p_ImagePickerVC.sortAscendingByModificationDate = NO;
        _p_ImagePickerVC.allowPickingVideo = NO;
        _p_ImagePickerVC.hideWhenCanNotSelect = YES;
    }
    return _p_ImagePickerVC;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    self.naviTitle = @"发布";
    self.viewModel = [[ICN_DynamicStatePublicationViewModel alloc] init];
    self.viewModel.delegate = self;
    [self.viewModel refreshCurrentUserFriendsList];
    [self configTableViewWhileViewDidLoad];
    self.p_ContentTextView.delegate = self;
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 刷新ViewModel中的选中列表和代理
    self.viewModel.delegate = self;
    [self.viewModel updateSelectedFriendModelsList];
    [self.tableView reloadData];
    [self addImageBtn];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.viewModel.delegate = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickOnPublishAction:(UIButton *)sender {
    
    // 上传文本内容不能为空
    if ([self judgeWithTextViewContent:self.p_ContentTextView]) {
        // 文本框有实际意义的内容
        self.publishBtn.userInteractionEnabled = NO;
        // 执行发布相关的操作
        if (self.imageListArr.count > 0) {
            [self.viewModel upLoadImages:self.imageListArr];
            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"动态上传中"];
        }else{
            // 纯文本发布 - 测试
            [self postSingleTextWisdomMessageWithInfo:nil];
            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"内容上传中"];
        }
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入动态内容"];
    }
}

#pragma mark - ---------- 私有方法 ----------

- (BOOL)judgeWithTextViewContent:(UITextView *)textView{
    if (textView.text != nil && ![textView.text isEqualToString:DefaultContent]) {
        // 文本框的内容真实有效
        return YES;
    }else{
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xB6B6B6);
        return NO;
    }
    return NO;
}

// 还需要添加notId但是现在不知道怎么传递
- (void)postSingleTextWisdomMessageWithInfo:(NSDictionary *)info{
    NSString *content = self.p_ContentTextView.text;
    NSString *pic = [info valueForKey:@"fileName"];
    // 设置不可看人的id
    NSMutableArray *IDarr = [NSMutableArray array];
    for (ICN_NewFrident *model in self.viewModel.selectedFriendModelsArr) {
        [IDarr addObject:model.friendId];
    }
    NSString *notIdStr;
    if (IDarr) {
        notIdStr = [IDarr componentsJoinedByString:@","];
    }
    [self.viewModel postDynamicStateWithContent:content Pic:pic NotIds:notIdStr];
}


- (void)clickImagePickAction:(UIButton *)sender{
    [self presentViewController:self.p_ImagePickerVC animated:YES completion:nil];
}

// 在视图加载后配置tableView
- (void)configTableViewWhileViewDidLoad{
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ICN_HistoryContactCell class] forCellReuseIdentifier:NSStringFromClass([ICN_HistoryContactCell class])];
    [self.tableView registerClass:[ICN_ShareToContentCell class] forCellReuseIdentifier:NSStringFromClass([ICN_ShareToContentCell class])];
}

// 对于图片列表的重新布局 - 在图片数组更新之后
- (void)configImageViewlistAfterImageListUpdate{
    
    for (NSInteger i = 0; i < self.imageListArr.count; i++) {
        if (i < self.imageViewListArr.count) {
            self.imageViewListArr[i].image = self.imageListArr[i];
        }else{
            UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageview.image = self.imageListArr[i];
            imageview.userInteractionEnabled = YES;
            [self.p_ImageScroll addSubview:imageview];
            [self.imageViewListArr addObject:imageview];
        }
        
    }
    
    [self updateImageViewFromIndex:0 EndIndex:self.imageListArr.count - 1];
    if (self.imageViewListArr.count > self.imageListArr.count) {
        NSMutableArray *removeItemsArr = [NSMutableArray array];
        for (NSInteger i = self.imageListArr.count; i < self.imageViewListArr.count; i++) {
            [self.imageViewListArr[i] removeFromSuperview];
            [removeItemsArr addObject:self.imageViewListArr[i]];
        }
        [self.imageViewListArr removeObjectsInArray:removeItemsArr];
    }
    
    BOOL isallowContentSize = self.imageViewListArr.count * 70.0 > self.p_ImageScroll.size.width - 80.0;
    if (isallowContentSize) {
        CGFloat width =  self.imageViewListArr.count * 70.0 + 80.0;
        CGFloat height = self.p_ImageScroll.height;
        self.p_ImageScroll.contentSize = CGSizeMake(width, height);
    }else{
        self.p_ImageScroll.contentSize = CGSizeZero;
    }
    
}


- (void)updateImageViewFromIndex:(NSInteger)startIndex
                        EndIndex:(NSInteger)endIndex{
    for (NSInteger i = startIndex; i <= endIndex; i++) {
        if (i == 0) {
            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.p_ImageScroll).offset(10.0);
                make.top.equalTo(self.p_ImageScroll).offset(10.0);
                make.size.mas_equalTo(CGSizeMake(60, 60));
            }];
        }else{
            UIImageView *lastItem = self.imageViewListArr[i - 1];
            [self.imageViewListArr[i] mas_updateConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(lastItem.mas_right).offset(10.0);
                make.top.equalTo(lastItem);
                make.size.equalTo(lastItem);
            }];
        }
    }
    CGRect frame = CGRectMake(self.imageListArr.count * 70.0 + 10.0, 10.0, self.addImageBtn.width, self.addImageBtn.height);
    self.addImageBtn.frame = frame;
}


#pragma mark - ---------- 代理 ----------

#pragma mark --- ICN_DynamicStatePublicationDelegate ---

- (void)responseWithUserFriendsListSuccess:(BOOL)isSuccess Error:(NSString *)error{
    if (isSuccess) {
        [self.tableView reloadData];
    }else{
//        [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
    }
}

- (void)responseWithMessageUploadSuccess:(BOOL)isSuccess ErrorCode:(NSInteger)errorCode Info:(NSDictionary *)info{
    
    [MBProgressHUD hiddenHUDWithSuperView:self.view];
    self.publishBtn.userInteractionEnabled = YES;
    if (errorCode == ICN_FileLoadSuccess) {
        
        if (info) {
            // 开始post话题内容
            [self postSingleTextWisdomMessageWithInfo:info];
            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"内容上传中"];
        }
    }
    
    switch (errorCode) {
        case ICN_NetSuccess:
        case ICN_FileLoadSuccess:
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"上传成功"];
            // 返回上级界面
            [self.navigationController popViewControllerAnimated:YES];
            break;
        case ICN_PostFailureWithNetState:
        case ICN_LoadFaileFailureWithNetState:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络异常"];
            break;
        }
        case ICN_PostFailureWithMessageError:
        case ICN_LoadFaileFailureWithMessageError:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"服务器异常"];
            break;
        }
        default:
            break;
    }

    
}

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    if ([textView.text isEqualToString:DefaultContent]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        textView.text = DefaultContent;
        textView.textColor = RGB0X(0xb6b6b6);
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (textView.text.length + text.length - range.length < 500) {
        self.p_ContentTextCountLabel.text = SF(@"%lu/500",textView.text.length + text.length - range.length);
        self.p_ContentTextCountLabel.textColor = RGB0X(0xb6b6b6);
        return YES;
    }else{
        self.p_ContentTextCountLabel.text = @"输出文字超限";
        self.p_ContentTextCountLabel.textColor = [UIColor redColor];
        if ([text isEqualToString:@""]) {
            return YES;
        }else
            return NO;
    }
    return YES;
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

//划动cell是否出现del按钮
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ICN_NewFrident *model = self.viewModel.selectedFriendModelsArr[indexPath.row - 1];
        model.selected = NO;
        [self.viewModel.selectedFriendModelsArr removeObject:model];
        [self.tableView reloadData];
    }
}

// 自定义删除按钮字段以及样式的方法
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewRowAction *rowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        ICN_NewFrident *model = self.viewModel.selectedFriendModelsArr[indexPath.row - 1];
        model.selected = NO;
        [self.viewModel.selectedFriendModelsArr removeObject:model];
        [self.tableView reloadData];
    }];
    return @[rowAction];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
    
    if (indexPath.row == 0) {
        ICN_ContactCanNotSeeVC *pager = [[ICN_ContactCanNotSeeVC alloc] init];
        pager.viewModel = self.viewModel;
        [self currentPagerJumpToPager:pager];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.viewModel.selectedFriendModelsArr.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        ICN_ShareToContentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ShareToContentCell class])];
        cell.cellTitle = @"谁不可看";
        return cell;
    }else{
        ICN_HistoryContactCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_HistoryContactCell class])];
        cell.model = self.viewModel.selectedFriendModelsArr[indexPath.row - 1];
        return cell;
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 40.0;
    }else{
        return 56.0;
    }
    return 0.0;
}


#pragma mark --- TZImagePickerControllerDelegate ---

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    if (isSelectOriginalPhoto) {
        // 当选中原图的时候获取到原图并添加到图片展示列表中
        [[TZImageManager manager] getOriginalPhotoWithAsset:assets.firstObject completion:^(UIImage *photo, NSDictionary *info) {
            [self.imageListArr addObject:photo];
        }];
        
    }else{
        
        if (_imageListArr) {
            [_imageListArr removeAllObjects];
        }
        [self.imageListArr addObjectsFromArray:photos];
        
        [self configImageViewlistAfterImageListUpdate];
    }
    
}



@end
