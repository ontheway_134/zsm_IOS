//
//  ICN_HistoryContactCell.m
//  ICan
//
//  Created by albert on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_HistoryContactCell.h"
#import "ICN_NewFrident.h"

@interface ICN_HistoryContactCell ()

@property (nonatomic , strong)UILabel *p_UserNameLabel;
@property (nonatomic , strong)UIImageView *p_Imageview;

@end

@implementation ICN_HistoryContactCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _p_UserNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _p_UserNameLabel.userInteractionEnabled = YES;
        _p_UserNameLabel.text = @"测试 - 夏一灿";
        _p_UserNameLabel.font = [UIFont systemFontOfSize:13.0];
        [self.contentView addSubview:_p_UserNameLabel];
        _p_Imageview = [[UIImageView alloc] initWithFrame:CGRectZero];
        _p_Imageview.image = [UIImage imageNamed:@"修改头像"];
        _p_Imageview.userInteractionEnabled = YES;
        [self.contentView addSubview:_p_Imageview];
    }
    return self;
}

- (void)setModel:(ICN_NewFrident *)model{
    _model = model;
    if (model) {
        self.p_UserNameLabel.text = _model.memberNick;
        [self.p_Imageview sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_model.memberLogo)] placeholderImage:[UIImage imageNamed:@"头像"]];
        [self setNeedsLayout];
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self configCurrentViewUIWhileWillLayout];
    self.p_Imageview.layer.cornerRadius = 35.0 / 2.0;
    self.p_Imageview.layer.masksToBounds = YES;
}

// 在视图将要布局的时候添加约束
- (void)configCurrentViewUIWhileWillLayout{
    
    [self.p_Imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(35, 35));
    }];
    
    [self.p_UserNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.p_Imageview.mas_right).offset(10.0);
        make.centerY.equalTo(self.p_Imageview);
    }];

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
