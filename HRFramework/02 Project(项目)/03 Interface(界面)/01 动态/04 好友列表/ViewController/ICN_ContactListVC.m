//
//  ICN_ContactListVC.m
//  ICan
//
//  Created by albert on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ContactListVC.h"
#import "ICN_ContactIndexSectionHeaderView.h" // 带有字母标识的section Header
#import "ICN_FirendListCell.h" // 联系人Cell
#import "ICN_CommonSearchBar.h"


@interface ICN_ContactListVC ()<UITableViewDelegate , UITableViewDataSource , CommonSearchBarDelegate>

@property (nonatomic , strong)NSArray *contactIndexArr; // 索引列表
@property (nonatomic , strong)UITableView *tableView; // 当前列表控件
@property (nonatomic , strong)ICN_CommonSearchBar *searchbar; // 通用搜索栏控件

@end

@implementation ICN_ContactListVC

#pragma mark - ---------- 懒加载 ----------

- (NSArray *)contactIndexArr{
    if (_contactIndexArr == nil) {
        _contactIndexArr = @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",];
    }
    return _contactIndexArr;
}

- (ICN_CommonSearchBar *)searchbar{
    if (_searchbar == nil) {
        _searchbar = XIB(ICN_CommonSearchBar);
        _searchbar.delegate = self;
        [self.view addSubview:_searchbar];
    }
    return _searchbar;
}

- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        [self.view addSubview:_tableView];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = RGB0X(0xf0f0f0);
        _tableView.sectionHeaderHeight = 0.0;
        _tableView.sectionFooterHeight = 0.0;
        
        // tableView 注册Cell
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_FirendListCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_FirendListCell class])];
        
        // tableView 注册section header
        [_tableView registerClass:[ICN_ContactIndexSectionHeaderView class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([ICN_ContactIndexSectionHeaderView class])];
    }
    return _tableView;
}

#pragma mark - ---------- 生命周期 ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    [self tableView];
    [self searchbar];
    [self contactIndexArr];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configCurrentPagerUIWhileViewWillAppear];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---------- 私有方法 ----------

// 在视图将要出现的时候配置UI
- (void)configCurrentPagerUIWhileViewWillAppear{
    [self.searchbar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.top.equalTo(self.view);
        make.height.mas_equalTo(50.0);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchbar.mas_bottom);
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

#pragma mark - ---------- 代理 ----------

/** searchbar的代理方法 */
#pragma mark --- CommonSearchBarDelegate ---

- (void)searchBarDidBeginEditingWithText:(NSString *)content{
    
}

/** tableView相关代理 */
#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //相关逻辑
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    ICN_ContactIndexSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([ICN_ContactIndexSectionHeaderView class])];
    view.indexTitle = self.contactIndexArr[section];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_FirendListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_FirendListCell class])];
    cell.alreadyFriend = YES;
    
    return cell;
}

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return self.contactIndexArr;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.contactIndexArr.count;
}





/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55.0;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18.5;
}




@end
