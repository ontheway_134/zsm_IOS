//
//  ICN_UserListQuestionContentCell.m
//  ICan
//
//  Created by albert on 2017/3/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_UserListQuestionContentCell.h"

@interface ICN_UserListQuestionContentCell ()

#pragma mark --- 用户部分 ---

@property (weak, nonatomic) IBOutlet UIImageView *userIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *userNickLabel; // 用户昵称
@property (weak, nonatomic) IBOutlet UILabel *professionLabel; // 行业标签
@property (weak, nonatomic) IBOutlet UILabel *MajorOrWorkLabel; // 专业/职位标签
@property (weak, nonatomic) IBOutlet UILabel *schoolOrCompanyLabel; // 学校/公司标签

#pragma mark --- 正文部分 ---

@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel; // 问题标题标签
@property (weak, nonatomic) IBOutlet UILabel *questionContentLabel; // 问题正文标签
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏标签
@property (weak, nonatomic) IBOutlet UIButton *payCountBtn; // 打赏金额按钮（并不添加点击事件）

#pragma mark --- 功能按钮 ---

@property (weak, nonatomic) IBOutlet UIButton *transmitBtn; // 转发问题按钮


@end


@implementation ICN_UserListQuestionContentCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
