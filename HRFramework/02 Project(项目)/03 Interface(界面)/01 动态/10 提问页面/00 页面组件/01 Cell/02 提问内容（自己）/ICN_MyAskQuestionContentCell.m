//
//  ICN_MyAskQuestionContentCell.m
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyAskQuestionContentCell.h"

@interface ICN_MyAskQuestionContentCell ()

@property (weak, nonatomic) IBOutlet UILabel *contentTitleLabel; // 内容标题
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 内容正文
@property (weak, nonatomic) IBOutlet UILabel *timeLabel; // 时间栏
@property (weak, nonatomic) IBOutlet UILabel *freePointsLabel; // 悬赏积分标签
@property (weak, nonatomic) IBOutlet UIButton *collectionBtn; // 收藏按钮
@property (weak, nonatomic) IBOutlet UIButton *reviewBtn; // 转发按钮



@end

@implementation ICN_MyAskQuestionContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.collectionBtn]) {
        // 点击的是收藏按钮
    }
    if ([sender isEqual:self.reviewBtn]) {
        // 点击的是转发按钮
        
    }
    
}


@end
