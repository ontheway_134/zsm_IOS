//
//  ICN_AnswerQuestionCell.m
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_AnswerQuestionCell.h"

@interface ICN_AnswerQuestionCell ()

@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel; // 昵称label
@property (weak, nonatomic) IBOutlet UIImageView *userLogoIcon; // 用户头像
@property (weak, nonatomic) IBOutlet UILabel *contentLabel; // 评论内容label
@property (weak, nonatomic) IBOutlet UIButton *likeUpBtn; // 点赞按钮
@property (weak, nonatomic) IBOutlet UIButton *payForButton; // 打赏按钮


@property (weak, nonatomic) IBOutlet UIView *segmentGrayView; // 分割Cell用灰条 -- 只有最后一个不显示
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;



@end

@implementation ICN_AnswerQuestionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// 点击了Cell上的按钮
- (IBAction)clickOnCellBtnAction:(UIButton *)sender {
    
    if ([sender isEqual:self.likeUpBtn]) {
        // 点击了点赞按钮
    }
    if ([sender isEqual:self.payForButton]) {
        // 点击了打赏按钮
    }
    
}



@end
