//
//  ICN_QuestionContentCell.h
//  ICan
//
//  Created by albert on 2017/3/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_QuestionContentCell : UITableViewCell

@property (nonatomic , assign , getter=isFirstPager) BOOL firstPager; // 判断是否是动态首页的属性（动态首页对于时间栏有新的布局并且隐藏了关注按钮）

@end
