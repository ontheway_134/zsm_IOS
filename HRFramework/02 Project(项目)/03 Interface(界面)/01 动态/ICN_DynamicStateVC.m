//
//  ICN_DynamicStateVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DynamicStateVC.h"
#import "ICN_DynamicStateHeader.h" // 动态的头文件
#import "ICN_DynamicStateVC+HXLogin.h" // 环信登录类目
#import "HRNetworkingManager+DynFirstPager.h" // 网络请求类目
#import "ICN_DynamicStateFirstPagerViewModel.h" // 首页ViewModel
#import "ICN_NearestPeopleViewModel.h" // 周边雷达管理器
#import "ICN_ShareManager.h" // 分享管理器
#import "ICN_ComplainListViewModel.h" // 吐槽的ViewModel


#pragma mark --- 常用宏 ---
static CGFloat const ImageFooterHeight = 60.0; // 当前带有图片的尾视图的基准高度(去掉图片背景图高度后的)
static CGFloat const SpreadContentCellHeight = 40.0; // 当前可展开Cell去掉可展开文本的高度
static CGFloat const SpreadLabelDefaultHeight = 62.5; // 可展开文本Cell的默认高度

#pragma mark --- Model相关头文件 ---
#import "ICN_DynStateADModel.h" // 广告专用Model
#import "ICN_DynStateContentModel.h" // 动态正文Model
#import "ICN_LocationModel.h" // 城市选择列表Model
#import "ICN_ApplyModel.h" // 友盟分享用Model


#pragma mark --- CellOrView相关头文件 ---
#import "ICN_QuestionContentCell.h" // 首页提问Cell列表
#import "ICN_ComplainContentCell.h" // 首页吐槽Cell列表
#import "ICN_DynStateTextSpreadCell.h" // 可拓展文本长度的Cell
#import "ICN_ReplyingDynStateCell.h" // 转发内容Cell
#import "ICN_DynStateSectionHeaderView.h" // 默认section的头视图
#import "ICN_DynStateCompanySectionHeader.h" // 公司推广section的头视图
#import "ICN_DynStateSectionNormalFooter.h" // 默认section的尾视图
#import "ICN_DynStateSectionImagesFooter.h" // 带有图片的section尾视图
#import "ICN_DynStateSingleAdTextView.h" // 单行广告头视图
#import "ICN_DynLocationPickerView.h" // 首页城市选择器


#pragma mark - ---------- 跳转页面/视图 头文件 ----------
#import "ICN_StartViewController.h" // 引导页
#import "ICN_DynWarnView.h" // 信息提示窗界面
#import "ICN_DynSearchPagerVC.h" // 搜索页面
#import "ICN_UserHomePagerVC.h" // 用户主页界面
#import "ICN_TopicPublicationVC.h" // 跳转到发布话题页面
#import "ICN_DynamicStatePublicationVC.h" // 跳转到发布动态页面
#import "ICN_UserDynamicStateDetialVC.h" // 跳转到动态评论页面
#import "ICN_ReviewPublication.h" // 转发到I行动态页面
#import "ICN_UserReportDetialVC.h" // 举报页面
#import "ICN_CompanyDetialVC.h" // 公司详情
#import "ICN_PicBroswerView.h" // 图片浏览器简化版
#import "ICN_PublishComplainPager.h" // 跳转到发布吐槽页面
#import "ICN_ComplainDetialPager.h" // 跳转到吐槽详情页面
#import "ICN_PublishAskQuestionPager.h" // 跳转到发布提问页面
#import "ICN_MyQuestionDetialPager.h" // 跳转到我的提问详情页面
#import "ICN_OthersQuestionDetialPager.h" // 跳转到用户的提问详情页面

@interface ICN_DynamicStateVC ()<UITableViewDelegate , UITableViewDataSource , ICN_DynWarnViewDelegate , DynamicStateFirstPagerViewModelDelegate , DynLocationPickerViewDelegate,ComplainListDelegate>

#pragma mark - ---------- IBProperty ----------

@property (weak, nonatomic) IBOutlet UILabel *p_CurrentCityLabel; // 当前城市标签

@property (weak, nonatomic) IBOutlet UILabel *p_SearchContextTextF; // 搜索文本框

@property (weak, nonatomic) IBOutlet UITableView *tableView; // 详情内容列表

@property (weak, nonatomic) IBOutlet UIView *breakLineView; // 动态/智讯的背景分隔栏

// 选项卡按钮·（动态、智讯、话题、提问）
@property (weak, nonatomic) IBOutlet UIButton *dynamicSelectedBtn; // 选中动态按钮
@property (weak, nonatomic) IBOutlet UIButton *wisdomSelectedBtn; // 选中智讯按钮
@property (weak, nonatomic) IBOutlet UIButton *topicSelectedBtn; // 选中话题按钮
@property (weak, nonatomic) IBOutlet UIButton *askActionSelectedBtn; // 选中提问按钮

@property (nonatomic , strong) UIButton *selectedStateBtn; // 选中的状态按钮 - 默认是动态的

@property (nonatomic , assign , getter=isViewAppearAgain)BOOL viewAppearAgain; // 视图是否是再次出现（是否是只走viewwillappear）



#pragma mark - ---------- 视图/控件属性 ----------

@property (nonatomic , strong)ICN_DynWarnView * warnView; // 提示窗页面
@property (nonatomic , strong)ICN_DynWarnView * replayView; // 转发提示窗
@property (nonatomic , strong)ICN_DynStateContentModel *replayModel; // 想要转发的数据
@property (nonatomic , strong)ICN_DynLocationPickerView *citySelectedView; // 城市选择器
@property (nonatomic , strong)ICN_StartViewController *loginPager; // 登录页面
@property (nonatomic , strong)UIView *buttonSignView; // 用于button状态切换的uiview

#pragma mark - ---------- 其他属性 ----------
@property (nonatomic , strong)ICN_DynamicStateFirstPagerViewModel *viewModel;
@property (nonatomic , strong)NSMutableArray *modelsArr; // Model数组
@property (nonatomic , strong)NSMutableArray <ICN_LocationModel *>* locationModelsArr;

@property (nonatomic , strong)ICN_PicBroswerView *pictureView; // 图片浏览视图
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightConstraint;
@property (nonatomic , assign , getter=isUserLogin)BOOL userLoginIn; // 用户是否登录
@property (nonatomic , strong)ICN_ComplainListViewModel *complainViewModel; // 吐槽相关的ViewModel

@end

@implementation ICN_DynamicStateVC

#pragma mark - ---------- Lazy Loading（懒加载） ----------

- (UIView *)buttonSignView{
    if (_buttonSignView == nil) {
        _buttonSignView = [[UIView alloc] initWithFrame:CGRectZero];
        _buttonSignView.backgroundColor = RGB0X(0x009dff);
        [self.breakLineView addSubview:_buttonSignView];
    }
    return _buttonSignView;
}

- (ICN_StartViewController *)loginPager{
    if (_loginPager == nil) {
        _loginPager = [[ICN_StartViewController alloc] init];
    }
    return _loginPager;
}

- (BOOL)isUserLoginIn{
    _userLoginIn = [self getCurrentUserLoginStatus];
    if (_userLoginIn == NO) {
        // 切换现在游客登录也不会默认跳转到智讯页面
    }
    return _userLoginIn;
}

- (NSMutableArray<ICN_LocationModel *> *)locationModelsArr{
    if (_locationModelsArr == nil) {
        _locationModelsArr = [NSMutableArray array];
    }
    return _locationModelsArr;
}

- (ICN_PicBroswerView *)pictureView{
    if (_pictureView == nil) {
        _pictureView = [[ICN_PicBroswerView alloc] initWithFrame:SCREEN_BOUNDS];
        _pictureView.hidden = YES;
        [[UIApplication sharedApplication].keyWindow addSubview:_pictureView];
    }
    return _pictureView;
}

- (ICN_DynLocationPickerView *)citySelectedView{
    if (_citySelectedView == nil && _locationModelsArr != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _citySelectedView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _citySelectedView.locationsArr = self.locationModelsArr;
        _citySelectedView.frame = frame;
        _citySelectedView.delegate = self;
        [[UIApplication sharedApplication].keyWindow addSubview:_citySelectedView];
    }
    return _citySelectedView;
}

- (NSMutableArray *)modelsArr{
    if (_modelsArr == nil) {
        _modelsArr = [NSMutableArray array];
    }
    return _modelsArr;
}

- (ICN_DynWarnView *)warnView{
    if (_warnView == nil) {
        _warnView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:@[[UIImage imageNamed:@"发布动态"] , [UIImage imageNamed:@"发布智讯"]] TitleLabels:@[@"发布动态" , @"发布智讯"] TabbarHidden:NO];
        _warnView.delegate = self;
    }
    
    if (_warnView.hidden) {
        _warnView.hidden = NO;
    }
    
    return _warnView;
}

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:NO];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}

#pragma mark - ----------   Lifecycle（生命周期） ----------

- (void)viewDidLoad {
    [super viewDidLoad];
    // 设置第一次加载的时候页面将要再次出现属性为no
    self.viewAppearAgain = NO;
    // 动态按钮默认为选中状态
    self.dynamicSelectedBtn.selected = YES;
    // 设置第一次进入页面的时候页面默认显示的按钮是动态按钮
    self.selectedStateBtn = self.dynamicSelectedBtn;
    // 隐藏页面的默认导航栏
    [self setHiddenDefaultNavBar:YES];
    // 初始化ViewModel
    self.viewModel = [[ICN_DynamicStateFirstPagerViewModel alloc] init];
    self.viewModel.delegate = self;
    self.complainViewModel = [[ICN_ComplainListViewModel alloc] init];
    self.complainViewModel.delegate = self;
    // 获取到用户是不是游客登录状态
    [self isUserLoginIn];
    
    // 设置tableView的默认属性
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = 1.0;
    self.tableView.sectionHeaderHeight = 1.0;
    [self configTableViewRegisterNessaryView];
    [self configTableViewRefreshHeaderFooterView];
    
    // 获取地理信息网络数据
    if (_locationModelsArr) {
        [_locationModelsArr removeAllObjects];
    }
    [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            [self.locationModelsArr addObjectsFromArray:modelsArr];
        }
        self.citySelectedView.hidden = YES;
    } Failure:^(NSDictionary *errorInfo) {
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isViewAppearAgain) {
        [self.tableView.mj_header beginRefreshing];
    }
    self.leftConstraint.constant = ICN_KScreen(67.0);
    self.rightConstraint.constant = ICN_KScreen(67.0);
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor whiteColor];  //改自己喜欢的颜色
    
    // 添加现在页面的四个选项卡下面的分隔栏的设置 -- 设置是位于选中按钮的居中
    [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
        make.bottom.equalTo(self.breakLineView);
        make.centerX.equalTo(self.selectedStateBtn);
    }];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.viewAppearAgain = YES;
    if (_warnView) {
        [_warnView removeFromSuperview];
        _warnView = nil;
    }
    if (_replayView) {
        [_replayView removeFromSuperview];
        _replayView = nil;
    }
    if (_citySelectedView) {
        [_citySelectedView removeFromSuperview];
        _citySelectedView = nil;
    }
}

- (void)dealloc{
    if (_warnView) {
        _warnView.delegate = nil;
    }
    if (_replayView) {
        _replayView.delegate = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - ---------- Private Methods（私有方法） ----------

- (void)configTableViewRefreshHeaderFooterView{
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        if (!header.isRefreshing) {
            
            // 判断调用的ViewModel以及对应的逻辑处理
            switch (self.selectedStateBtn.tag) {
                case PRIVATE_DynamicBtn:
                case  PRIVATE_WisdomBtn:{
                    [self.viewModel refreshCurrentPageContentCells];
                    break ;
                }
                case PRIVATE_TopicBtn:{
                    // 刷新吐槽操作
                    [self.complainViewModel requestForComplainedListMineList:NO load:NO];
                }
                case PRIVATE_AskActionBtn:{
                    // 刷新提问列表操作
                }
                default:
                    break;
            }
            
            
        }
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        // 判断调用的ViewModel以及对应的逻辑处理
        switch (self.selectedStateBtn.tag) {
            case PRIVATE_DynamicBtn:
            case  PRIVATE_WisdomBtn:{
                [self.viewModel loadNextPageContentCells];
                break ;
            }
            case PRIVATE_TopicBtn:{
                // 刷新吐槽操作
                [self.complainViewModel requestForComplainedListMineList:NO load:YES];
            }
            case PRIVATE_AskActionBtn:{
                // 刷新提问列表操作
            }
            default:
                break;
        }

    }];
    [self.tableView.mj_header beginRefreshing];
}

- (void)endRefreshWithTableView:(UITableView *)tableView{
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
}

/** 根据Model的详情跳转个人详情
 1. 动态详情页面
 2. 公司详情页面
 */
- (void)jumpToPersonalDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    if (model != nil) {
        if (model.roleId.integerValue == 1) {
            // 个人主页
            ICN_UserHomePagerVC *pager = [[ICN_UserHomePagerVC alloc] init];
            pager.memberId = model.memberId;
            if (self.isUserLogin) {
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
        }else{
            // 企业主页
            if (self.isUserLogin) {
                ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                pager.companyId = model.memberId;
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
        }
    }
}

/** 根据Model跳转到动态详情页面 */
- (void)jumpToStateDetialPagerWithModel:(ICN_DynStateContentModel *)model{
    // 先区分是否是转发
    
    
}

#pragma mark initliaze data(初始化数据)

#pragma mark config control（布局控件）

/** 在tableView中注册必要的Cell */
- (void)configTableViewRegisterNessaryView{
    
    // 注册提问和吐槽Cell
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_QuestionContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_ComplainContentCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
    
    UINib *nibCellOne = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateTextSpreadCell class]) bundle:nil];
    [self.tableView registerNib:nibCellOne forCellReuseIdentifier:SF(@"%@", ICN_SpreadTextCellConstStr)];
    UINib *nibCellTwo = [UINib nibWithNibName:NSStringFromClass([ICN_ReplyingDynStateCell class]) bundle:nil];
    [self.tableView registerNib:nibCellTwo forCellReuseIdentifier:SF(@"%@", ICN_ReplyingDynStateCellConstStr)];
    UINib *headerOne = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionHeaderView class]) bundle:nil];
    [self.tableView registerNib:headerOne forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_DefaultSectionHeaderConstStr)];
    UINib *headerTwo = [UINib nibWithNibName:NSStringFromClass([ICN_DynStateCompanySectionHeader class]) bundle:nil];
    [self.tableView registerNib:headerTwo forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_CompanySectionHeaderConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSingleAdTextView class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_SingleADTextHeaderConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionNormalFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_DefaultSectionFooterConstStr)];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_DynStateSectionImagesFooter class]) bundle:nil] forHeaderFooterViewReuseIdentifier:SF(@"%@",ICN_ImagesListSectionFooterConstStr)];
    
}

#pragma mark networkRequest (网络请求)

#pragma mark actions （点击事件）

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
}

#pragma mark IBActions （点击事件xib）

- (IBAction)clickOnCityChangeBtn:(UIButton *)sender {
    
    if (_locationModelsArr == nil || self.locationModelsArr.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未获取到地理信息列表"];
        [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
            if (modelsArr.firstObject.code == 0) {
                [self.locationModelsArr addObjectsFromArray:modelsArr];
            }
        } Failure:nil];
    }
    // 进行切换城市标签的操作
    if (self.citySelectedView.hidden == YES) {
        self.citySelectedView.hidden = NO;
    }else{
        self.citySelectedView.hidden = YES;
    }
}

- (IBAction)clickOnNavgationBtnAction:(UIButton *)sender {
    
    switch (sender.tag) {
        case ICN_NavPublishBtnType:
        {
            if (self.isUserLogin == NO) {
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }else{
                self.warnView.frame = [UIScreen mainScreen].bounds;
                [self.view addSubview:self.warnView];
            }
            break;
        }
        case ICN_NavSearchBtnType:{
            if (self.isUserLogin) {
                ICN_DynSearchPagerVC *pager = [[ICN_DynSearchPagerVC alloc] init];
                [self currentPagerJumpToPager:pager];
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
            break;
        }
            
        default:
            break;
    }
    
}


- (IBAction)clickOnSelectContentAction:(UIButton *)sender {
    
    [self changeSelectedContentLabelStatusWithSenderTage:sender.tag];
    
}

- (void)changeSelectedContentLabelStatusWithSenderTage:(NSInteger)tag{
    // 根据tag计算选中的是哪个按钮 - 并将其他按钮的选中状态改变
    if (tag != self.selectedStateBtn.tag) {
        // 两次选中的按钮的tag不一样
        
        // 1. 设置之前选中的按钮取消选择
        self.selectedStateBtn.selected = NO;
        // 2. 判断选中的按钮并设置selectedStateBtn 为选中的按钮
        // 3. 设置选中的按钮的状态为yes
        switch (tag) {
            case PRIVATE_DynamicBtn:{
                self.dynamicSelectedBtn.selected = YES;
                self.selectedStateBtn = self.dynamicSelectedBtn;
                break;
            }
            case PRIVATE_WisdomBtn:{
                self.wisdomSelectedBtn.selected = YES;
                self.selectedStateBtn = self.wisdomSelectedBtn;
                break;
            }
            case PRIVATE_TopicBtn:{
                self.topicSelectedBtn.selected = YES;
                self.selectedStateBtn = self.topicSelectedBtn;
                break;
            }
            case PRIVATE_AskActionBtn:{
                self.askActionSelectedBtn.selected = YES;
                self.selectedStateBtn = self.askActionSelectedBtn;
                break;
            }
            default:
                break;
        }
        
        // 4. 设置切换选中状态的标签的跟随移动
        [UIView animateWithDuration:0.3 animations:^{
            CGPoint center = self.buttonSignView.center;
            center.x = self.selectedStateBtn.centerX;
            self.buttonSignView.center = center;
        } completion:^(BOOL finished) {
            [self.buttonSignView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.breakLineView);
                make.size.mas_equalTo(CGSizeMake(29.0, 1.0));
                make.centerX.equalTo(self.selectedStateBtn);
            }];
        }];
    }
    // 5. 设置ViewModel中选中的刷新类型
    // 注现在需要将ViewModel中关于是动态还是智讯的相关内容修改为枚举四种选项均可的
    self.viewModel.currentSelectedType = tag;
    
    // 6. 根据选中的内容刷新页面
    [self.tableView.mj_header beginRefreshing];
    
    //test=== 在吐槽和提问是假数据的时候手动刷新数据
    [self.tableView reloadData];
    
}




#pragma mark - ---------- Public Methods（公有方法） ----------

#pragma mark self declare （本类声明）

#pragma mark override super （重写父类）

#pragma mark setter （重写set方法）

#pragma mark - ---------- Protocol Methods（代理方法） ----------

#pragma mark --- DynLocationPickerViewDelegate ---

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (success) {
        // 将选中的城市提交给服务器
        
        // 修改当前显示的城市
        for (ICN_LocationModel *localModel in self.locationModelsArr) {
            for (ICN_CityDetialModel *detialModel in localModel.list) {
                if (detialModel.cityId.integerValue == code) {
                    self.p_CurrentCityLabel.text = detialModel.className;
                }
            }
        }
        
    }else{
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未选中城市"];
    }
    self.citySelectedView.hidden = YES;
}

#pragma mark --- 吐槽相关代理 ---

- (void)responseWithEnumType:(NSUInteger)enumType Success:(BOOL)success Info:(NSString *)info{
    switch (enumType) {
        case Complain_ResponseLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            break ;
        }
        case Complain_ResponseDisLikeUp:{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            break ;
        }
        case Complain_ResponseOthersList:{
            [self endRefreshWithTableView:self.tableView];
            if (!success) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:info];
            }else{
                [self.tableView reloadData];
            }
            break ;
        }
        default:
            break;
    }
}



#pragma mark --- DynamicStateFirstPagerViewModelDelegate ---

- (void)responseWithNetRequestError:(NSString *)error{
    [self endRefreshWithTableView:self.tableView];
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:error];
}

- (void)responseWithModelsArr:(NSMutableArray *)modesArr{
    [self endRefreshWithTableView:self.tableView];
    if (_modelsArr) {
        [_modelsArr removeAllObjects];
    }
    [self.modelsArr addObjectsFromArray:modesArr];
    [self.tableView reloadData];
}

- (void)responseWithLikeUpRequestSuccess:(BOOL)success{
    if (success) {
        [self.tableView reloadData];
    }
}

- (void)responseWithAlreadyLikeUp{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您已经点过赞了"];
    [self.tableView reloadData];
}

- (void)responseWhileUserHasNoAuthority{
    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"您没有权限进行此操作"];
}

#pragma mark --- ICN_DynWarnViewDelegate ---

// 添加常量
static NSString *iconUrl = @"";
static NSString *title = @"";
static NSString *content = @"";

//#import "ICN_PublishComplainPager.h" // 跳转到发布吐槽页面
//#import "ICN_PublishAskQuestionPager.h" // 跳转到发布提问页面

- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    
//    1002331  动态  1002332 智讯
    
    switch (type) {
        // 点击发布动态
        case Publich_DynamicState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_DynamicBtn];
            ICN_DynamicStatePublicationVC *pager = [[ICN_DynamicStatePublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        // 点击发布智讯
        case Publich_WisdomState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_WisdomBtn];
            ICN_TopicPublicationVC *pager = [[ICN_TopicPublicationVC alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        // 发布吐槽
        case Publich_ComplainState:{
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_TopicBtn];
            ICN_PublishComplainPager *pager = [[ICN_PublishComplainPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        // 发布提问
        case Publich_AskQuestion:{
            HRLog(@"点击的是右边的按钮");
            [self changeSelectedContentLabelStatusWithSenderTage:PRIVATE_AskActionBtn];
            ICN_PublishAskQuestionPager *pager = [[ICN_PublishAskQuestionPager alloc] init];
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.warnView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            ICN_ReviewPublication *pager = [[ICN_ReviewPublication alloc] init];
            pager.model = self.replayModel;
            self.replayModel = nil;
            [self currentPagerJumpToPager:pager];
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"该功能暂未开通"];
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        case ICN_ShareToWeiFridBtnType:{
         
            NSLog(@"分享到微信朋友圈");
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            
            break;
        }
        case ICN_ShareToQQFridBtnType:{
        
            NSLog(@"分享到QQ空间");
            NSDictionary *params = @{@"matterId":self.replayModel.matterId};
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DynamicSHARE params:params success:^(id result) {
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0 && [[result valueForKey:@"result"] valueForKey:@"url"] != nil) {
                    NSString *url ;
                    if ([[result valueForKey:@"result"] valueForKey:@"url"]) {
                        url = [[result valueForKey:@"result"] valueForKey:@"url"];
                        
                        [self cofigShareContent];
                        
                        [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:url andTitle:title andImage:iconUrl Detial:content];
                    }
                    
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
            } failure:nil];
            break;
        }
        default:
            [self.warnView removeFromSuperview];
            [self.replayView removeFromSuperview];
            break;
    }
}

- (void)cofigShareContent{
    if (self.replayModel.title == nil || [self.replayModel.title isEqualToString:@""]) {
        title = self.replayModel.memberNick;
    }else{
        title = self.replayModel.memberNick;
    }
    if (self.replayModel.memberLogo) {
        // 使用友盟提供的链接图片就可以访问，使用自己获取的就不行，未知原因
        iconUrl = ICN_IMG(self.replayModel.memberLogo);
    }
    if (self.replayModel.isTransmit.integerValue == 1) {
        // 是转发
        if (self.replayModel.summary == nil || [self.replayModel.summary isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.summary;
        }
    }else{
        if (self.replayModel.content == nil || [self.replayModel.content isEqualToString:@""]) {
            content = @"转发内容";
        }else{
            content = self.replayModel.content;
        }
    }

}

#pragma mark --- UITableViewDelegate , UITableViewDataSource ---

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            ICN_ComplainDetialPager *pager = [[ICN_ComplainDetialPager alloc] init];
            [self currentPagerJumpToPager:pager];
            return ;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是提问 -- 测试跳转到自己的提问详情
//            ICN_MyQuestionDetialPager *pager = [[ICN_MyQuestionDetialPager alloc] init];
//            [self currentPagerJumpToPager:pager];
//            return ;
            // --测试跳转到别人的提问详情
            ICN_OthersQuestionDetialPager *pager = [[ICN_OthersQuestionDetialPager alloc] init];
            [self currentPagerJumpToPager:pager];
            return ;
            break;
        }
        default:
            break;
    }
    if (self.isUserLogin) {
        //相关逻辑 - 跳转到评论页面
        ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
        pager.model = self.modelsArr[indexPath.section];
        [self currentPagerJumpToPager:pager];
    }else{
        self.hidesBottomBarWhenPushed = YES;
        [self currentPagerJumpToPager:self.loginPager];
    }
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            return self.complainViewModel.modelsArr.count;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是提问
            return 10;
            break;
        }
        default:
            break;
    }
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        return 1;
    }else
        return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 选中的是吐槽
            ICN_ComplainContentCell *testCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_ComplainContentCell class])];
            return testCell;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是提问
            ICN_QuestionContentCell *testCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ICN_QuestionContentCell class])];
            testCell.firstPager = YES;
            return testCell;
            break;
        }
        default:
            break;
    }

    if ([self.modelsArr[indexPath.section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        ICN_DynStateContentModel *model = self.modelsArr[indexPath.section];
        if (model.isTransmit.integerValue == 1) {
            ICN_ReplyingDynStateCell *cell = (ICN_ReplyingDynStateCell *)[tableView dequeueReusableCellWithIdentifier:SF(@"%@" , ICN_ReplyingDynStateCellConstStr)];
            cell.model = model;
            return cell;
        }else{
            ICN_DynStateTextSpreadCell *cell = (ICN_DynStateTextSpreadCell *)[tableView dequeueReusableCellWithIdentifier:SF(@"%@" , ICN_SpreadTextCellConstStr)];
            cell.model = model;
            [cell callBackWithSpreadBlock:^(BOOL spread) {
                [self.tableView reloadData];
            }];
            return cell;
        }
    }
    
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽
            return 1;
            break;
        }
        default:
            break;
    }

    return self.modelsArr.count;
}


/** section 的两部分节视图 */
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return nil;
            break;
        }
        default:
            break;
    }
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateADModel class]]) {
        // 进行对于广告头视图的处理
        ICN_DynStateADModel *adModel = [self.modelsArr objectAtIndex:section];
        if (adModel.genre) {
            if (adModel.genre.integerValue == 0) {
                // 文字连接
                ICN_DynStateSingleAdTextView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_SingleADTextHeaderConstStr)];
                [view callBackWithTextTapBlock:^(ICN_DynStateADModel *model) {
                    if (self.isUserLogin) {
                        //相关逻辑 - 跳转到评论页面
                        ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                        pager.companyId = model.memberId;
                        [self currentPagerJumpToPager:pager];
                    }else{
                        self.hidesBottomBarWhenPushed = YES;
                        [self currentPagerJumpToPager:self.loginPager];
                    }
                }];
                view.model = adModel;
                return view;
            }else if (adModel.genre.integerValue == 1){
                ICN_DynStateCompanySectionHeader *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_CompanySectionHeaderConstStr)];
                [view callBackWithHeaderTapBlock:^(ICN_DynStateADModel *model) {
                    if (self.isUserLogin) {
                        //相关逻辑 - 跳转到评论页面
                        ICN_CompanyDetialVC *pager = [[ICN_CompanyDetialVC alloc] init];
                        pager.companyId = model.memberId;
                        [self currentPagerJumpToPager:pager];
                    }else{
                        self.hidesBottomBarWhenPushed = YES;
                        [self currentPagerJumpToPager:self.loginPager];
                    }
                }];
                view.model = adModel;
                return view;
            }
        }
    }
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        // 正文内容
        ICN_DynStateSectionHeaderView *view = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_DefaultSectionHeaderConstStr)];
        if (section == 0) {
            view.firstSection = YES;
        }else{
            view.firstSection = NO;
        }
        view.model = self.modelsArr[section];
        [view callBackWithTapBlock:^(ICN_DynStateContentModel *model, BOOL isHeaderIcon) {
            if (self.isUserLogin) {
                [self jumpToPersonalDetialPagerWithModel:model];
//                if (isHeaderIcon) {
//                    // 点击用户头像调用主页跳转方法
//                    
//                    
//                }
//                else{
//                    //相关逻辑 - 跳转到评论页面
//                    ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
//                    pager.model = model;
//                    if (model.isTransmit.integerValue == 1) {
//                        // 说明是转发的内容
//                        pager.transmit = YES;
//                    }else{
//                        pager.transmit = NO;
//                    }
//                    [self currentPagerJumpToPager:pager];
//                }
            }else{
                self.hidesBottomBarWhenPushed = YES;
                [self currentPagerJumpToPager:self.loginPager];
            }
            
        }];
        return view;
    }
    
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return nil;
            break;
        }
        default:
            break;
    }

    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        // 当内容为动态的时候有尾视图
        ICN_DynStateContentModel *model = self.modelsArr[section];
        if (model.pic && [model.pic containsString:@"."] && model.isTransmit.integerValue == 0) {
            // 有图片的时候调用有图片的pic;
            ICN_DynStateSectionImagesFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_ImagesListSectionFooterConstStr)];
            footer.model = model;
            [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                switch (btnTag) {
                    case ICN_CellLikeActionBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        model.likeUp = !model.likeUp;
                        NSInteger type;
                        if (model.isLikeUp) {
                            type = 1;
                        }else{
                            type = 2;
                        }
                        [self.viewModel likeUpWithType:type Model:model];
                        break;
                    }
                    case ICN_CellReviewBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        // 执行转发相关操作
                        [self.view addSubview:self.replayView];
                        self.replayModel = model;
                        break;
                    }
                    case ICN_CellCommentBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        // 执行回复评论的相关操作
                        //相关逻辑 - 跳转到评论页面
                        ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                        pager.model = model;
                        [self currentPagerJumpToPager:pager];
                        break;
                    }
                    case ICN_CellReportBtnType:{
                        // 根据tag判断点击的按钮
                        if (self.isUserLogin == NO) {
                            self.hidesBottomBarWhenPushed = YES;
                            [self currentPagerJumpToPager:self.loginPager];
                            break;
                        }
                        // 执行举报按钮相关操作
                        ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                        pager.matterId = model.matterId;
                        [self currentPagerJumpToPager:pager];
                        break;
                    }
                    default:
                        // 返回的是image的索引值
                        if (self.pictureView.hidden == YES) {
                            NSArray *pathArr = [model.pic componentsSeparatedByString:@","];
                            self.pictureView.urlPathArr = pathArr;
                            self.pictureView.currentIndex = btnTag;
                            self.pictureView.hidden = NO;
                        }
                        break;
                }
            }];
            return footer;
        }else{
            ICN_DynStateSectionNormalFooter *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SF(@"%@",ICN_DefaultSectionFooterConstStr)];
            footer.model = model;
            [footer callBackWithBtnBlock:^(ICN_DynStateContentModel *model, NSInteger btnTag) {
                if (self.isUserLogin == NO) {
                    self.hidesBottomBarWhenPushed = YES;
                    [self currentPagerJumpToPager:self.loginPager];
                }else{
                    // 根据tag判断点击的按钮
                    switch (btnTag) {
                        case ICN_CellLikeActionBtnType:{
                            model.likeUp = !model.likeUp;
                            NSInteger type;
                            if (model.isLikeUp) {
                                type = 1;
                            }else{
                                type = 2;
                            }
                            [self.viewModel likeUpWithType:type Model:model];
                            break;
                        }
                        case ICN_CellReviewBtnType:{
                            // 执行转发相关操作
                            [self.view addSubview:self.replayView];
                            self.replayModel = model;
                            break;
                        }
                        case ICN_CellCommentBtnType:{
                            // 执行回复评论的相关操作
                            //相关逻辑 - 跳转到评论页面
                            ICN_UserDynamicStateDetialVC *pager = [[ICN_UserDynamicStateDetialVC alloc] init];
                            pager.model = model;
                            [self currentPagerJumpToPager:pager];
                            break;
                        }
                        case ICN_CellReportBtnType:{
                            // 执行举报按钮相关操作
                            ICN_UserReportDetialVC *pager = [[ICN_UserReportDetialVC alloc] init];
                            pager.matterId = model.matterId;
                            [self currentPagerJumpToPager:pager];
                            break;
                        }
                        default:
                            break;
                    }
                    
                }
            }];
            return footer;
        }
    }
    
    return nil;
}

/** 逐条调节tableView的Cell高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:{
            // 返回提问的高度
            return 180;
            break;
        }
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽
            return 210;
            break;
        }
        default:
            break;
    }

    CGFloat height = 0.0;
    
    if ([self.modelsArr[indexPath.section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        ICN_DynStateContentModel *model = self.modelsArr[indexPath.section];
        if (model.isTransmit.integerValue == 0) {
            if (model.contentSpread) {
                return SpreadContentCellHeight + model.contentSpreadHeight;
            }
            if (model.contentSpreadHeight < SpreadLabelDefaultHeight) {
                return 20.0 + model.contentSpreadHeight;
            }
            return SpreadContentCellHeight + SpreadLabelDefaultHeight;
        }else{
            return ICN_ReplyingDynStateCellHeight;
        }
    }
    
    return height;
}

/** 修改tableView的section的高度 */
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return 0.0;
            break;
        }
        default:
            break;
    }

    CGFloat height = 0.0;
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        if (section == 0) {
            return ICN_DynStateSectionHeaderViewHeight - 5;
        }
        return ICN_DynStateSectionHeaderViewHeight;
    }else{
        ICN_DynStateADModel *model = self.modelsArr[section];
        if (model.genre.integerValue == 0) {
            return ICN_DynStateSingleAdTextViewHeight;
        }else{
            return ICN_DynStateCompanySectionHeaderHeight;
        }
    }
    
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    //test=== 测试关于提问和吐槽的相关内容
    switch (self.selectedStateBtn.tag) {
        case PRIVATE_TopicBtn:
        case PRIVATE_AskActionBtn:{
            // 选中的是吐槽和提问
            return 0.0;
            break;
        }
        default:
            break;
    }

    CGFloat height = 0.0;
    
    if ([self.modelsArr[section] isKindOfClass:[ICN_DynStateContentModel class]]) {
        ICN_DynStateContentModel *model = self.modelsArr[section];
        if (model.imageFooterHeight != 0.0 && model.isTransmit.integerValue == 0) {
            return ImageFooterHeight + model.imageFooterHeight;
        }else
            return ICN_DynStateSectionNormalFooterHeight;
    }
    return height;
}



@end
