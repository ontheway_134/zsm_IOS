//
//  ICN_GetEnterPriseModel.h
//  ICan
//
//  Created by shilei on 17/1/11.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "BaseOptionalModel.h"

@interface EnterPriseModel : JSONModel

@property(nonatomic,copy)NSString *memberId;   //企业的用户表ID,
@property(nonatomic,copy)NSString *companyLogo;  //企业图标;
@property(nonatomic,copy)NSString *companyName;   //企业名称
@property(nonatomic,copy)NSString *companyIndustryName;   //企业所属行业名称
@property(nonatomic,copy)NSString *companyDetail;   //公司的内容
@property (nonatomic , copy)NSString *city; // 所在省份
@property (nonatomic , copy)NSString *cityName;
@property (nonatomic , copy)NSString *authStatus; // 企业状态

@property (nonatomic , copy)NSString *province; // 所在城市
@property (nonatomic , copy)NSString *provinceName;

@property (nonatomic , copy)NSString *companyIndustry; // 企业所属行业
//'companyNature'：企业性质（
//'1'：'外资（欧美）','2'：'外资（非欧美）','3'：'合资','4'：'国企',
//'5'：'民营公司','6'：'上市公司','7'：'创业公司','8'：'外资代表处',
//'9'：'政府机关','10'：'事业单位','11'：'非盈利机构'
@property (nonatomic , copy)NSString *companyNature; // 企业性质
@property (nonatomic , copy)NSString *companyNatureName;

@property (nonatomic , copy)NSString *companyScale; // 企业规模



@end

@interface ICN_GetEnterPriseModel : BaseOptionalModel

@property(nonatomic,strong)EnterPriseModel *result;

@end
