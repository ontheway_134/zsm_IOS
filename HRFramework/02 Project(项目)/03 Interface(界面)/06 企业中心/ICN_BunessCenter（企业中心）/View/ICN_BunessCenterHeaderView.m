//
//  ICN_BunessCenterHeaderView.m
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BunessCenterHeaderView.h"

@implementation ICN_BunessCenterHeaderView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.logoImageView.layer.cornerRadius = 25;
    self.logoImageView.layer.masksToBounds = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
