//
//  ICN_BunessCenterHeaderView.h
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_BunessCenterHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyCount;       
@property (weak, nonatomic) IBOutlet UILabel *companyNature;

@end
