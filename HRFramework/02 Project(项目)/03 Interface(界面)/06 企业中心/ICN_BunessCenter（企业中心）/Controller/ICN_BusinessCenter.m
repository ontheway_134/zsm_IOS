//
//  ICN_BusinessCenter.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BusinessCenter.h"
#import "ICN_BunessCenterHeaderView.h"
#import "ICN_BusinessCenterTableViewCell.h"
#import "ICN_BusinessMassageViewController.h"
#import "ICN_ResumeManageViewController.h"
#import "ICN_IssuePostViewController.h"
#import "ICN_GetEnterPriseModel.h"
#import "ICN_SettingViewController.h"

@interface ICN_BusinessCenter ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *BusinessCenterTabel;
@property(nonatomic,strong)ICN_BunessCenterHeaderView *headerView;
@property (nonatomic , strong)ICN_GetEnterPriseModel *basicMessageModel; // 从网络获取的基本信息的Model

@property (copy, nonatomic) NSString *isAudit;

@end

@implementation ICN_BusinessCenter

#pragma mark - --- 网络请求 ---

-(void)httpEnterPriseMessage{
    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_GETENTERPRISEMESSAGE params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        ICN_GetEnterPriseModel *model=[[ICN_GetEnterPriseModel alloc]initWithDictionary:result error:nil];
        if (model.code == 0) {
            self.basicMessageModel = model;
            [self.headerView.logoImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.result.companyLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
            self.headerView.companyLabel.text = model.result.companyName;
            self.headerView.companyNature.text = model.result.companyIndustryName;
            self.headerView.companyCount.text= SF(@"%@ 丨 %@人", model.result.companyNatureName, model.result.companyScale);
        }
        [self.BusinessCenterTabel.mj_header endRefreshing];
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
    
    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_isAudit params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        NSString *tempStr = SF(@"%@",result[@"code"]);
        if ([tempStr isEqualToString:@"0"]) {
            _isAudit = SF(@"%@",result[@"result"][@"audit"]);
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
    
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.BusinessCenterTabel registerNib:[UINib nibWithNibName:@"ICN_BusinessCenterTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_BusinessCenterTableViewCell"];
    [self.BusinessCenterTabel setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.BusinessCenterTabel.delegate=self;
    self.BusinessCenterTabel.dataSource=self;
    self.BusinessCenterTabel.scrollEnabled=NO;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 每次页面刷新的时候就重新走一遍网络请求
    [self httpEnterPriseMessage];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 1;
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_BusinessCenterTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessCenterTableViewCell"];
    if (cell==nil) {
        cell=[[NSBundle mainBundle]loadNibNamed:@"ICN_BusinessCenterTableViewCell" owner:self options:nil].lastObject;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    switch (indexPath.section) {
            
        case 0:{
            
            switch (indexPath.row) {
                case 0:{
                    cell.BusinessImageView.image=[UIImage imageNamed:@"基本信息"];
                    cell.BusinessLabel.text=@"基本信息";
                }
                    break;
                case 1:{
                    cell.BusinessImageView.image=[UIImage imageNamed:@"简历管理"];
                    cell.BusinessLabel.text=@"简历管理";
                
                }
                    break;
                case 2:{
                    cell.BusinessImageView.image=[UIImage imageNamed:@"已发布职位"];
                    cell.BusinessLabel.text=@"已发布职位";
                    
                }
                    break;
                
            }
            
        }
           
           break;
            
        case 1:{
        
            cell.BusinessImageView.image=[UIImage imageNamed:@"设置"];
            cell.BusinessLabel.text=@"设置";
        }
            
            break;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT * 40/568;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return  84;
            break;
            
        case 1:
            return 4;
            break;
    }
    return 0;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:{
            self.headerView=[[ICN_BunessCenterHeaderView alloc]init];
            self.headerView=[[[NSBundle mainBundle]loadNibNamed:@"ICN_BunessCenterHeaderView" owner:self options:nil]lastObject];
            return self.headerView;
        }
            break;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:{
                    ICN_BusinessMassageViewController *massage=[[ICN_BusinessMassageViewController alloc]init];
                    massage.hidesBottomBarWhenPushed =YES;
                    massage.model = self.basicMessageModel;
                    [self.navigationController pushViewController:massage animated:YES];
                    
                }
                    break;
                
                case 1:{
                    if ([_isAudit isEqualToString:@"1"]) {
                        ICN_ResumeManageViewController *resumeManage=[[ICN_ResumeManageViewController alloc]init];
                        resumeManage.hidesBottomBarWhenPushed=YES;
                        [self.navigationController pushViewController:resumeManage animated:YES];
                    } else {
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未通过审核，功能不可用"];
                    }

                }
                    break;
                
                case 2:{
                    if ([_basicMessageModel.result.authStatus isEqualToString:@"1"]) {
                    ICN_IssuePostViewController *issuePost=[[ICN_IssuePostViewController alloc]init];
                    issuePost.hidesBottomBarWhenPushed=YES;
                    [self.navigationController pushViewController:issuePost animated:YES];
                    } else {
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未通过审核，功能不可用"];
                    }
                }
                    break;
            }
        }
            break;
            
        case 1:{
            ICN_SettingViewController *vc = [[ICN_SettingViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
    }

}

@end
