//
//  ICN_PositionListModel.m
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_PositionListModel.h"

@implementation ICN_PositionListModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"PositionId" : @"id"} ];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
