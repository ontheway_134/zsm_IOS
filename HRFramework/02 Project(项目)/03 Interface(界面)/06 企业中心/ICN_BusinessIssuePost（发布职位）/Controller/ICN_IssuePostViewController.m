//
//  ICN_IssuePostViewController.m
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IssuePostViewController.h"
#import "ICN_IsuePostTableViewCell.h"
#import "ICN_PositionListModel.h"
#import "ICN_PositionNextOneViewController.h"

@interface ICN_IssuePostViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *IssuePostTableView;
@property (nonatomic, copy) ICN_PositionListModel *model;
@property (nonatomic, strong) NSMutableArray *modelArr;

@end

@implementation ICN_IssuePostViewController

-(void)httpEnterPriseMessage{
    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_POSITIONLIST params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        
        NSString *tempStr = SF(@"%@",result[@"code"]);
        if ([tempStr isEqualToString:@"0"]) {
            _modelArr = [NSMutableArray array];
            NSMutableArray *tempArr = [NSMutableArray arrayWithArray:result[@"result"]];
            for (int i=0; i<tempArr.count; i++) {
                _model = [[ICN_PositionListModel alloc]initWithDictionary:result[@"result"][i] error:nil];
                [_modelArr addObject:_model];
            }
            [self.IssuePostTableView reloadData];
        }
        [self.IssuePostTableView.mj_header endRefreshing];
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
        [self.IssuePostTableView.mj_header endRefreshing];
    }];
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.IssuePostTableView registerNib:[UINib nibWithNibName:@"ICN_IsuePostTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_IsuePostTableViewCell"];
    self.IssuePostTableView.delegate=self;
    self.IssuePostTableView.dataSource=self;
    [self.IssuePostTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self httpEnterPriseMessage];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [self setMJRefresh];
}

#pragma mark - --- Ptotocol ---

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (_modelArr == nil) {
//        return 0;
//    }
    return _modelArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_IsuePostTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_IsuePostTableViewCell"];
    if (cell==nil) {
        cell=[[NSBundle mainBundle]loadNibNamed:@"ICN_IsuePostTableViewCell" owner:self options:nil].lastObject;
    }
    [cell setModel:_modelArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

#pragma mark - --- IBActions ---

- (IBAction)backActions:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_PositionNextOneViewController *vc = [[ICN_PositionNextOneViewController alloc] init];
    ICN_PositionListModel *positionModel = [[ICN_PositionListModel alloc]init];
    positionModel = _modelArr[indexPath.row];
    vc.url = positionModel.PositionId;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setMJRefresh {
    //下拉刷新
    self.IssuePostTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(httpEnterPriseMessage)];
    [self.IssuePostTableView.mj_header beginRefreshing];
    //上拉加载更多
    //    self.ResumeManageTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(mjMoreNetRequest)];
}

@end
