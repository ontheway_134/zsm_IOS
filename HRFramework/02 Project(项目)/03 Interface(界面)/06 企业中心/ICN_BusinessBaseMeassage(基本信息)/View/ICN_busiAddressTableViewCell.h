//
//  ICN_busiAddressTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_busiAddressTableViewCell : UITableViewCell

@property (nonatomic , assign)NSInteger cellKey; // Cell所属的key的类型

@property (nonatomic , copy)NSString *content; // 内容


@end
