//
//  ICN_BaseBusinessMessagessTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_BaseBusinessMessagessTableViewCell : UITableViewCell

@property (nonatomic , copy)NSString *iconPath; // 图片的中间路径

@property (nonatomic , strong)UIImage *headerImage;

@property (weak, nonatomic) IBOutlet UIImageView *headerIcon;

@end
