//
//  ICN_BusinessNameTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BusinessNameTableViewCell.h"

static NSString *const WARNMessage = @"请输入有实际意义的内容";
#define NUMBERS @"0123456789\n"
@interface ICN_BusinessNameTableViewCell ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *contentTitleLabel; // 标题字段标签



@end

@implementation ICN_BusinessNameTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configTextFieldDelegate{
    if (self.contentTextField.delegate == nil) {
        self.contentTextField.delegate = self;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - ---------- 属性构造器 ----------

- (void)setTitleStr:(NSString *)TitleStr{
    if (TitleStr) {
        _TitleStr = TitleStr;
        self.contentTitleLabel.text = TitleStr;
        [self configTextFieldDelegate];
    }
}

- (void)setContentStr:(NSString *)contentStr{
    if (contentStr) {
        _contentStr = contentStr;
        NSString *content = [contentStr stringByReplacingOccurrencesOfString:@" " withString:@""];
        if (![content isEqualToString:@""]) {
            // 有实际意义的内容，添加在textF上
            self.contentTextField.text = content;
        }else{
            self.contentTextField.text = nil;
        }
    }
}


#pragma mark - ---------- 私有方法 ----------

- (void)callBackWithTextComplitedBlock:(TextComplitedBlock)block{
    self.block = block;
}

#pragma mark - ---------- 代理 ----------

#pragma mark --- UITextFieldDelegate ---

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    // 在textF将要开始编辑的时候判断文本颜色将警告颜色取消
    if ([textField.textColor isEqual:[UIColor redColor]]) {
        textField.textColor = [UIColor blackColor];
        textField.text = nil;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason{
    // 判断内容是否存储完成若完成则回调到VC做对应存储
    if (textField.text) {
        NSString *contentStr = [textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];;
        if (![contentStr isEqualToString:@""]) {
            // 判断textF有实际内容
            if (self.block) {
                self.block(SF(@"%ld",self.keyType) , contentStr);
            }
        }else{
            textField.text = WARNMessage;
            textField.textColor = [UIColor redColor];
        }
        
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (_highestNumber == 0) {
        _highestNumber = 100;
    }
    
    if (range.location >= _highestNumber)
    {
        return  NO;
    } else {
        if (_isNumber) {
            NSCharacterSet *NumberSet;
            NumberSet = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS] invertedSet];
            NSString *filtered = [[string componentsSeparatedByCharactersInSet:NumberSet] componentsJoinedByString:@""];
            BOOL basicTest = [string isEqualToString:filtered];
            if(!basicTest)
            {
                return NO;
            }
        }
        return YES;
    }
}

@end
