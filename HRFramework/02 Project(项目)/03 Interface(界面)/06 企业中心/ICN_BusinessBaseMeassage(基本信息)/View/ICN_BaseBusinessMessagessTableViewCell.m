//
//  ICN_BaseBusinessMessagessTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BaseBusinessMessagessTableViewCell.h"

@interface ICN_BaseBusinessMessagessTableViewCell ()




@end

@implementation ICN_BaseBusinessMessagessTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setIconPath:(NSString *)iconPath{
    if (iconPath != nil && _headerImage == nil) {
        _iconPath = iconPath;
        [self.headerIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(_iconPath)] placeholderImage:[UIImage imageNamed:@"修改头像"]];
        [self setNeedsLayout];
    }
}

- (void)setHeaderImage:(UIImage *)headerImage{
    if (headerImage) {
        _headerImage = headerImage;
        self.headerIcon.image = _headerImage;
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.headerIcon.layer.cornerRadius = self.headerIcon.width / 2.0;
    self.headerIcon.clipsToBounds = YES;
}

@end
