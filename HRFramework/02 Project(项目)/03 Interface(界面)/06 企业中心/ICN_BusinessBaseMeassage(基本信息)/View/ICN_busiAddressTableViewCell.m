//
//  ICN_busiAddressTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_busiAddressTableViewCell.h"

@interface ICN_busiAddressTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *contentBtn;

@end

@implementation ICN_busiAddressTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setContent:(NSString *)content{
    _content = content;
    if (_content != nil && ![content isEqualToString:@""]) {
        [self.contentBtn setTitle:content forState:UIControlStateNormal];
        [self.contentBtn setTitleColor:RGB0X(0x333333) forState:UIControlStateNormal];
    }else{
        [self.contentBtn setTitle:@"请选择" forState:UIControlStateNormal];
        [self.contentBtn setTitleColor:RGB0X(0xB6B6B6) forState:UIControlStateNormal];
    }
}

- (void)setCellKey:(NSInteger)cellKey{
    _cellKey = cellKey;
    switch (cellKey) {
        case ICN_BusinessCompanyINDUSTRY:{
            self.titleLabel.text = @"所属行业";
            break;
        }
        case ICN_BusinessCompanyAREA:{
            self.titleLabel.text = @"所属地区";
            break;
        }
        case ICN_BusinessCompanyNATURE:{
            self.titleLabel.text = @"企业性质";
            break;
        }
        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
