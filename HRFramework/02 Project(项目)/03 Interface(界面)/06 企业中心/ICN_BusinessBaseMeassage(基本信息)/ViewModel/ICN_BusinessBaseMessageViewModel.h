//
//  ICN_BusinessBaseMessageViewModel.h
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ICN_LocationModel;

@protocol BusinessBaseMessageDelegate <NSObject>

- (void)responseContentPOSTWithSuccess:(BOOL)success Error:(NSString *)error keyType:(NSInteger)keyType;

@end

@interface ICN_BusinessBaseMessageViewModel : NSObject

@property (nonatomic , weak)id<BusinessBaseMessageDelegate> delegate;
@property (nonatomic , strong)NSMutableDictionary *pickerModelsDic; // 根据key值存储各种选择器数据的字典
@property (nonatomic , strong)NSMutableDictionary *updateResultDic; // 跟新结果的字典

/** 获取企业设置了联动的相关接口 */
- (void)requestWithBusinessLinkNetWork;

/** 根据ViewModel中的修改文件数组上传 */
- (void)updateCurrentMessageWithViewModelDic;

- (NSInteger)getNumberIdFormIndusruryWithContent:(NSString *)content;

@end

