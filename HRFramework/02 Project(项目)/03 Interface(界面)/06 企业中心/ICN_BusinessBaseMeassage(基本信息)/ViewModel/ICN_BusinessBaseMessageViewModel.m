//
//  ICN_BusinessBaseMessageViewModel.m
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BusinessBaseMessageViewModel.h"
#import "HRNetworkingManager+ICN_Publication.h"
#import "HRNetworkingManager+DynFirstPager.h"
#import "ICN_BaseClassKindModel.h"

//企业性质（
//'1'：'外资（欧美）','2'：'外资（非欧美）','3'：'合资','4'：'国企',
//'5'：'民营公司','6'：'上市公司','7'：'创业公司','8'：'外资代表处',
//'9'：'政府机关','10'：'事业单位','11'：'非盈利机构'
//）

@interface ICN_BusinessBaseMessageViewModel ()

@property (nonatomic , strong)NSArray * businessNatureArr;

@end

@implementation ICN_BusinessBaseMessageViewModel

#pragma mark - ---------- 懒加载 ----------

- (NSArray *)businessNatureArr{
    if (_businessNatureArr == nil) {
        _businessNatureArr = @[
                               [[ICN_BaseClassKindModel alloc] initWithId:@"1" ClassName:@"外资（欧美）"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"2" ClassName:@"外资（非欧美）"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"3" ClassName:@"合资"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"4" ClassName:@"国企"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"5" ClassName:@"民营公司"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"6" ClassName:@"上市公司"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"7" ClassName:@"创业公司"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"8" ClassName:@"外资代表处"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"9" ClassName:@"政府机关"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"10" ClassName:@"事业单位"],
                               [[ICN_BaseClassKindModel alloc] initWithId:@"11" ClassName:@"非盈利机构"]
                               ];
    }
    return _businessNatureArr;
    
}

- (NSInteger)getNumberIdFormIndusruryWithContent:(NSString *)content{
    
    return [self.businessNatureArr indexOfObject:content] + 1;
}

- (NSMutableDictionary *)pickerModelsDic{
    if (_pickerModelsDic == nil) {
        _pickerModelsDic = [NSMutableDictionary dictionary];
    }
    return _pickerModelsDic;
}

- (NSMutableDictionary *)updateResultDic{
    if (_updateResultDic == nil) {
        _updateResultDic = [NSMutableDictionary dictionary];
    }
    return _updateResultDic;
}

#pragma mark - ---------- 公开方法 ----------

- (void)requestWithBusinessLinkNetWork{
    // 地址
    [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
        if (modelsArr.firstObject.count == 0) {
            // 数据获取成功
            [self.pickerModelsDic setValue:modelsArr forKey:SF(@"%ld",(long)ICN_BusinessCompanyAREA)];
        }
    } Failure:nil];
    // 行业
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ERPISEINFO params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 数据获取成功
            NSMutableArray *array = [NSMutableArray array];
            for (NSDictionary *tempDic in [result valueForKey:@"result"]) {
                ICN_BaseClassKindModel *model = [[ICN_BaseClassKindModel alloc] initWithDictionary:tempDic error:nil];
                [array addObject:model];
            }
            [self.pickerModelsDic setValue:array forKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)];
        }
    } failure:nil];
    // 企业性质
    [self.pickerModelsDic setValue:self.businessNatureArr forKey:SF(@"%ld",ICN_BusinessCompanyNATURE)];
    

}

#pragma mark - ---------- 私有方法 ----------

// 图片上传的方法 -- 带有上传图片对应的key的类型
- (void)loadImageWithImages:(NSArray *)imagesArr KeyType:(NSInteger)keyType{
    [HRNetworkingManager uploadImagesWithImageArr:imagesArr Success:^(ICN_CommonImagesLoadModel *model) {
        //        [info valueForKey:@"fileName"]
        if (model.code == 0) {
            // 图片上传成功
            [self callBackWithUpdateContentSuccess:YES Error:[model.result valueForKey:@"fileName"] keyType:keyType];
        }else{
            [self callBackWithUpdateContentSuccess:NO Error:model.info keyType:keyType];
        }
    } Failure:^(NSDictionary *errorInfo) {
        [self callBackWithUpdateContentSuccess:NO Error:@"数据获取失败" keyType:keyType];
    }];
}

- (void)postContentWithPath:(NSString *)path Content:(NSString *)content KeyType:(NSInteger)keyType{
    
}




- (void)callBackWithUpdateContentSuccess:(BOOL)success Error:(NSString *)error keyType:(NSInteger)keyType{
    switch (keyType) {
        
        case ICN_BusinessCompanyLOGO:{
            
            break;
        }
        case ICN_BusinessCompanyNAME:{
            
            break;
        }
        case ICN_BusinessCompanyINDUSTRY:{
            
            break;
        }
        case ICN_BusinessCompanyAREA:{
            
            break;
        }
        case ICN_BusinessCompanySCALE:{
            
            break;
        }
        case ICN_BusinessCompanyCONTENT:{
            
            break;
        }
        case ICN_BusinessCompanyNATURE:{
            
            break;
        }
            
        default:
            break;
    }
}



@end
