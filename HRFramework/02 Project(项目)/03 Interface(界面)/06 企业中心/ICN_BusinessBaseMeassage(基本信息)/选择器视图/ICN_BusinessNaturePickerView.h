//
//  ICN_BusinessNaturePickerView.h
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_BaseClassKindModel;

@protocol BusinessNaturePickerViewDelegate <NSObject>

- (void)responseWithConfirm:(BOOL)isConfirm
              SelectedModel:(ICN_BaseClassKindModel *)model;

@end

@interface ICN_BusinessNaturePickerView : UIView

@property (nonatomic , strong)NSArray <ICN_BaseClassKindModel *>*dataModelsArr; // 数据源

@property (nonatomic , weak)id<BusinessNaturePickerViewDelegate> delegate;

+ (instancetype)loadXibWithCurrentBound:(CGRect)bound;

@end
