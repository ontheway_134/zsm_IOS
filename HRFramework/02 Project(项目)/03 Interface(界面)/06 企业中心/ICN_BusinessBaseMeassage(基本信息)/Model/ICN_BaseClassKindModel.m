//
//  ICN_BaseClassKindModel.m
//  ICan
//
//  Created by albert on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BaseClassKindModel.h"

@implementation ICN_BaseClassKindModel


// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"baseId" : @"id"}];
}

- (instancetype)initWithId:(NSString *)baseId ClassName:(NSString *)name{
    self = [super init];
    if (self) {
        _baseId = baseId;
        _className = name;
    }
    return self;
}

@end
