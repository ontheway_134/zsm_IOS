//
//  ICN_BusinessBasicMessageHeader.h
//  ICan
//
//  Created by albert on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#ifndef ICN_BusinessBasicMessageHeader_h
#define ICN_BusinessBasicMessageHeader_h

/** 企业基本信息类型key - int类型需要转换成字符串 */
typedef NS_ENUM(NSInteger, ICN_BusinessKeyENUM) {
    ICN_BusinessCompanyLOGO = 60020111, // logo key = 60020111
    ICN_BusinessCompanyNAME, // 公司名称 key = 60020112
    ICN_BusinessCompanyINDUSTRY, // 公司行业 key = 60020113
    ICN_BusinessCompanyAREA, // 公司地址 key = 60020114
    ICN_BusinessCompanySCALE, // 公司规模 key = 60020115
    ICN_BusinessCompanyCONTENT, // 公司内容 key = 60020116
    ICN_BusinessCompanyNATURE, // 公司性质 key = 60020117
    ICN_BusinessCompanyINDUSTRYID, // 公司行业ID key = 60020118
    ICN_BusinessCompanyNATUREID, // 公司性质ID key = 60020119
};



#endif /* ICN_BusinessBasicMessageHeader_h */
