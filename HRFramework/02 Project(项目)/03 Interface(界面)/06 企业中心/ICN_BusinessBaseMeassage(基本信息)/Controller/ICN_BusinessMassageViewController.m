//
//  ICN_BusinessMassageViewController.m
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_BusinessMassageViewController.h"
#import "ICN_BaseBusinessMessagessTableViewCell.h"
#import "ICN_BusinessNameTableViewCell.h"
#import "ICN_busiAddressTableViewCell.h"
#import "ICN_BriefTableViewCell.h"
#import "ICN_PhotoTableViewCell.h"
#import "ICN_GetEnterPriseModel.h" // Model头文件
#import "HRNetworkingManager.h" // 网络请求头文件
#import "ICN_HeadImageSelView.h" // 编辑logo视图头文件
#import "STPhotoKitController.h" // 单张视图编辑所调用三方头文件
#import "UIImagePickerController+ST.h"
#import "HRNetworkingManager+ICN_Publication.h" // 发布页面的网络请求
#import "ICN_BusinessBaseMessageViewModel.h" // ViewModel
#import "ICN_BaseClassKindModel.h"
#import "ICN_LocationModel.h"


#pragma mark - ---------- 视图属性 ----------
#import "ICN_BusinessNaturePickerView.h"
#import "ICN_BusinessIndustryPickerView.h"
#import "ICN_DynLocationPickerView.h"

#import "HRNetworkingManager+ICN_Publication.h"


@interface ICN_BusinessMassageViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,STPhotoKitDelegate,BusinessNaturePickerViewDelegate,BusinessIndustryPickerViewDelegate,DynLocationPickerViewDelegate>{
    UIView *grayView;
     NSArray *imgPath ;
    NSMutableArray *imageArr;
    ICN_BusinessNameTableViewCell *nickcell;
    
}

@property (nonatomic , assign ,getter = isFirstLoad)BOOL firstLoad;


@property (nonatomic , strong)ICN_BusinessNaturePickerView *naturePickerView;
@property (nonatomic , strong)ICN_BusinessIndustryPickerView *industryPickerView;
@property (nonatomic , strong)ICN_DynLocationPickerView *locationPickerView;
@property (nonatomic , strong)ICN_BusinessBaseMessageViewModel *viewModel;

@property (weak, nonatomic) IBOutlet UITableView *baseMessagessTabelView;

@property(nonatomic,strong)ICN_BriefTableViewCell *Briefcell;

@property (nonatomic , strong)ICN_BaseBusinessMessagessTableViewCell *headImageCell; // 注册头像用头视图Cell

@property (nonatomic , strong)NSMutableDictionary *updateContentDic; // 更新修改的内容字典

@property (nonatomic , strong)NSMutableDictionary *firstContentDic; // 更新修改的内容字典

@property (nonatomic , strong)ICN_HeadImageSelView *changeHeadImageView; // 修改企业中心logo用弹出视图
@property (nonatomic , strong)UIView *pickerBacground; // 用于显示pickerView的背景板

@property (strong, nonatomic) ICN_PhotoTableViewCell *photoCell;
@property (nonatomic , strong)NSMutableArray *locationIdArr; // 选中的城市的Id数组（省id，市id）
@property (weak, nonatomic) IBOutlet UIButton *leftBarBtn;


@property (strong, nonatomic) UIImage *upImage; //上传用Image
@end

@implementation ICN_BusinessMassageViewController

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)locationIdArr{
    if (_locationIdArr == nil) {
        _locationIdArr = [NSMutableArray array];
    }
    return _locationIdArr;
}

- (ICN_DynLocationPickerView *)locationPickerView{
    if (_locationPickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _locationPickerView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _locationPickerView.locationsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)];
        _locationPickerView.frame = frame;
        _locationPickerView.delegate = self;
        [self.pickerBacground addSubview:_locationPickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _locationPickerView;

}

- (ICN_BusinessNaturePickerView *)naturePickerView{
    if (_naturePickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyNATURE)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _naturePickerView = [ICN_BusinessNaturePickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _naturePickerView.dataModelsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyNATURE)];
        _naturePickerView.frame = frame;
        _naturePickerView.delegate = self;
        [self.pickerBacground addSubview:_naturePickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyNATURE)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _naturePickerView;
}

- (ICN_BusinessIndustryPickerView *)industryPickerView{
    if (_industryPickerView == nil && [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)] != nil) {
        CGRect frame = SCREEN_BOUNDS;
        frame.size.height = 161.5;
        _industryPickerView = [ICN_BusinessIndustryPickerView loadXibWithCurrentBound:frame];
        frame.origin.y = SCREEN_HEIGHT - frame.size.height;
        _industryPickerView.dataModelsArr = [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)];
        _industryPickerView.frame = frame;
        _industryPickerView.delegate = self;
        [self.pickerBacground addSubview:_industryPickerView];
    }else{
        if ([self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)] == nil) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"正在获取数据"];
        }
    }
    return _industryPickerView;
}

- (UIView *)pickerBacground{
    if (_pickerBacground == nil) {
        _pickerBacground = [[UIView alloc] initWithFrame:SCREEN_BOUNDS];
        _pickerBacground.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.05];
        UITapGestureRecognizer *backGroundHideTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideBackGroundViewAction)];
        [_pickerBacground addGestureRecognizer:backGroundHideTap];
        [[UIApplication sharedApplication].keyWindow addSubview:_pickerBacground];
    }
    return _pickerBacground;
}

- (ICN_BusinessBaseMessageViewModel *)viewModel{
    if (_viewModel == nil) {
        _viewModel = [[ICN_BusinessBaseMessageViewModel alloc] init];
    }
    return _viewModel;
}

- (ICN_HeadImageSelView *)changeHeadImageView {
    if (!_changeHeadImageView)
    {
        _changeHeadImageView = [[NSBundle mainBundle] loadNibNamed:@"ICN_HeadImageSelView" owner:self options:nil].lastObject;
    }
    return _changeHeadImageView;
}

- (NSMutableDictionary *)updateContentDic{
    if (_updateContentDic == nil) {
        _updateContentDic = [NSMutableDictionary dictionary];
    }
    return _updateContentDic;
}

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    self.firstLoad = YES;
    [self.viewModel requestWithBusinessLinkNetWork];
    [self.navigationController setNavigationBarHidden:YES];
    // 注册用户名头像的Cell
    [self.baseMessagessTabelView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_BaseBusinessMessagessTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_BaseBusinessMessagessTableViewCell class])];
    // 注册基本用户信息的Cell
    [self.baseMessagessTabelView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_BusinessNameTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_BusinessNameTableViewCell class])];
    // 注册用户所在地址的Cell
    [self.baseMessagessTabelView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_busiAddressTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_busiAddressTableViewCell class])];
    // 注册公司简介的Cell
    [self.baseMessagessTabelView registerNib:[UINib nibWithNibName:NSStringFromClass([ICN_BriefTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ICN_BriefTableViewCell class])];
    self.baseMessagessTabelView.delegate=self;
    self.baseMessagessTabelView.dataSource=self;
    [self.baseMessagessTabelView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.hiddenDefaultNavBar = YES;
    
    if (_notSetUserInfo) {
        self.leftBarBtn.hidden = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.firstLoad = NO;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_pickerBacground) {
        [_pickerBacground removeFromSuperview];
    }
}

- (void)hideBackGroundViewAction{
    self.locationPickerView.hidden = YES;
    self.naturePickerView.hidden = YES;
    self.industryPickerView.hidden = YES;
    self.pickerBacground.hidden = YES;
}


#pragma mark - ---------- 代理 ----------

#pragma mark - ---------- pickerView代理 ----------

#pragma mark --- BusinessNaturePickerViewDelegate,BusinessIndustryPickerViewDelegate,DynLocationPickerViewDelegate ---

- (void)responseWithConfirm:(BOOL)isConfirm SelectedModel:(ICN_BaseClassKindModel *)model{
    if (isConfirm) {
        [self.updateContentDic setValue:model.className forKey:SF(@"%ld",ICN_BusinessCompanyNATURE)];
        [self.updateContentDic setValue:model.baseId forKey:SF(@"%ld", ICN_BusinessCompanyNATUREID)];
        [self.baseMessagessTabelView reloadData];
    }
    self.pickerBacground.hidden = YES;
}

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (code > 0) {
        // 获取到数据了
        NSMutableArray *array = [NSMutableArray array];
        for (ICN_LocationModel *model in [self.viewModel.pickerModelsDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)]) {
            for (ICN_CityDetialModel *detialModel in model.list) {
                if (detialModel.cityId.integerValue == code) {
                    [array addObject:model.className];
                    [array addObject:detialModel.className];
                    [self.locationIdArr removeAllObjects];
                    [self.locationIdArr addObject:model.provinceId];
                    [self.locationIdArr addObject:detialModel.cityId];
                }
            }
        }
        if (array.count == 2) {
            [self.updateContentDic setObject:array forKey:SF(@"%ld",ICN_BusinessCompanyAREA)];
            [self.baseMessagessTabelView reloadData];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未选中城市"];
        }
    }
    self.pickerBacground.hidden = YES;
}

- (void)responseWithIndustryConfirm:(BOOL)isConfirm SelectedModel:(ICN_BaseClassKindModel *)model{
    if (isConfirm) {
        [self.updateContentDic setValue:model.className forKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)];
        [self.updateContentDic setValue:model.baseId forKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRYID)];
        [self.baseMessagessTabelView reloadData];
    }
    self.pickerBacground.hidden = YES;
}



#pragma mark UITableViewDataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
        {
            // 点击修改logo的Cell - 跳转到图片选择界面
            [self editImageSelected];
            break;
        }
        // 联动相关的Cell
        case 2:{
            // 弹出所属行业
            self.pickerBacground.hidden = NO;
            self.industryPickerView.hidden = NO;
            break;
        }
        case 3:{
            // 弹出所属地区
            self.pickerBacground.hidden = NO;
            self.locationPickerView.hidden = NO;
            break;
        }
        case 4:{
            // 弹出所属性质
            self.pickerBacground.hidden = NO;
            self.naturePickerView.hidden = NO;
            break;
        }
            
        default:
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            // 用户头像
            ICN_BaseBusinessMessagessTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BaseBusinessMessagessTableViewCell"];
            if (_upImage) {
                cell.headerIcon.image = _upImage;
            } else {
            [cell.headerIcon sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(self.model.result.companyLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
            self.headImageCell = cell;
                _upImage = cell.headerIcon.image;
            }
            return cell;
            break;
        }
        case 1:{
            
            nickcell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessNameTableViewCell"];
            nickcell.TitleStr = @"公司名称";
            nickcell.keyType = ICN_BusinessCompanyNAME;
            NSString *contentStr = [self.updateContentDic valueForKey:SF(@"%ld",(long)ICN_BusinessCompanyNAME)];
            if (contentStr == nil) {
                contentStr = self.model.result.companyName;
            }
            nickcell.contentStr = contentStr;
            [nickcell callBackWithTextComplitedBlock:^(NSString *textKey, NSString *contentStr) {
                [self.updateContentDic setValue:contentStr forKey:textKey];
            }];
            return nickcell;
            
        }
            
        case 2:{
            // 所属行业也是联动
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyINDUSTRY;
            if ([self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)]) {
                cell.content = [self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyINDUSTRY)];
            }else{
                cell.content = @"";
            }
            if (cell.content.length == 0) {
                cell.content = self.model.result.companyIndustryName;
            }
            return cell;
            break;
        }

        case 3:{
            // 用户地址
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyAREA;
            if ([self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)]) {
                cell.content = [[self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyAREA)] componentsJoinedByString:@" "];
            } else if (cell.content.length == 0) {
                if (self.model.result.provinceName == nil || self.model.result.cityName == nil) {
                    cell.content = @"";
                } else {
                    cell.content = SF(@"%@ %@",self.model.result.provinceName, self.model.result.cityName);
                }
            }
            return cell;
            break;
        }
            
        case 4:{
            // 企业性质也是联动
            ICN_busiAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_busiAddressTableViewCell"];
            cell.cellKey = ICN_BusinessCompanyNATURE;
            if ([self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyNATURE)]) {
                cell.content = [self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanyNATURE)];
            }else{
                cell.content = @"";
            }
            if (cell.content.length == 0) {
                cell.content = self.model.result.companyNatureName;
            }
            return cell;
            break;
        }
        
        case 5:{
            
            ICN_BusinessNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BusinessNameTableViewCell"];
            cell.TitleStr = @"公司规模";
            cell.isNumber = YES;
            cell.highestNumber = 10;
            cell.keyType = ICN_BusinessCompanySCALE;
            NSString *contentStr = [self.updateContentDic valueForKey:SF(@"%ld",ICN_BusinessCompanySCALE)];
            if (contentStr == nil) {
                contentStr = self.model.result.companyScale;
            }
            cell.contentStr = contentStr;
            [cell callBackWithTextComplitedBlock:^(NSString *textKey, NSString *contentStr) {
                [self.updateContentDic setValue:contentStr forKey:textKey];
            }];
            return cell;
            
        }
            
        case 6:{
            // 公司简介的Cell
            self.Briefcell = [tableView dequeueReusableCellWithIdentifier:@"ICN_BriefTableViewCell"];
            if (self.Briefcell.BriefTextView.text.length == 0) {
                self.Briefcell.BriefTextView.text = self.model.result.companyDetail;
                self.Briefcell.CountLabel.text = SF(@"%ld/500",self.Briefcell.BriefTextView.text.length);
            }
//            NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:self.Briefcell.CountLabel.text];
//            if ([self.Briefcell.BriefTextView.text length] <10 && [self.Briefcell.BriefTextView.text length] > 0) {
//                [str1 addAttribute:NSForegroundColorAttributeName value:RGB0X(0x009dff) range:NSMakeRange(0,1)];
//                
//            }
//            if ([self.Briefcell.BriefTextView.text length] <100 && [self.Briefcell.BriefTextView.text length]>9) {
//                [str1 addAttribute:NSForegroundColorAttributeName value:RGB0X(0x009dff) range:NSMakeRange(0,2)];
//            }
//            if ([self.Briefcell.BriefTextView.text length] <1000 && [self.Briefcell.BriefTextView.text length] >99) {
//                [str1 addAttribute:NSForegroundColorAttributeName value:RGB0X(0x009dff) range:NSMakeRange(0,3)];
//                
//            }
//            self.Briefcell.CountLabel.attributedText = str1;
            self.Briefcell.BriefTextView.delegate=self;

            return self.Briefcell;
        }
            break;
            
            
        case 7:{
            // 添加资料图片的Cell
            _photoCell=[tableView dequeueReusableCellWithIdentifier:@"ICN_PhotoTableViewCell"];
            if (_photoCell==nil) {
                _photoCell=[[NSBundle mainBundle]loadNibNamed:@"ICN_PhotoTableViewCell" owner:self options:nil].lastObject;
                
            }
            if (self.isFirstLoad) {
                [self ynetRequestWithCell:_photoCell];
            }
            
            return _photoCell;
        }
            break;
    }
    
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            return 55;
            break;
            
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
            return 45;
            break;
            
        case 6:
            return 110;
            break;
            
        case 7:
            return 115;
            break;
            
    }
    return 0;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=500)
    {
        return  NO;
    }
    else
    {
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    
    
    //该判断用于联想输入
    if (textView.text.length >= 500)
    {
        textView.text = [textView.text substringToIndex:500];
        
    }
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.Briefcell.CountLabel.text=[NSString stringWithFormat:@"%ld/500",existTextNum];
}


#pragma mark - 1.STPhotoKitDelegate的委托

- (void)photoKitController:(STPhotoKitController *)photoKitController resultImage:(UIImage *)resultImage
{
    _upImage = resultImage;
    [self.headImageCell setHeaderImage:resultImage];
    [self.updateContentDic setValue:resultImage forKey:SF(@"%ld",ICN_BusinessCompanyLOGO)];
    
}

#pragma mark - 2.UIImagePickerController的委托

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *imageOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
        STPhotoKitController *photoVC = [STPhotoKitController new];
        [photoVC setDelegate:self];
        [photoVC setImageOriginal:imageOriginal];
        
        [photoVC setSizeClip:CGSizeMake(70, 70)];
        
        [self presentViewController:photoVC animated:YES completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark - --- event response 事件相应 ---
- (void)editImageSelected
{
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
    
    [grayView addSubview:self.changeHeadImageView];
    [self.changeHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 160));
    }];
    self.changeHeadImageView.layer.cornerRadius = 8;
    [self.changeHeadImageView.takePhotoButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        [grayView removeFromSuperview];
        if ([controller isAvailableCamera] && [controller isSupportTakingPhotos]) {
            [controller setDelegate:self];
            [self presentViewController:controller animated:YES completion:nil];
        }else {
            NSLog(@"%s %@", __FUNCTION__, @"相机权限受限");
        }
    }];
    [self.changeHeadImageView.localAlbumButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [controller setDelegate:self];
        [grayView removeFromSuperview];
        if ([controller isAvailablePhotoLibrary]) {
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];
    [self.changeHeadImageView.closeViewButton addAction:^(NSInteger tag) {
        [self dismissContactView];
    }];
}

- (void)dismissContactView {
    [grayView removeFromSuperview];
}

#pragma mark - --- IBActions ---

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveBtnAction:(id)sender {
    // 调用网络请求在获取到网络请求数据后根据内容判断是否刷新
    [self updateBusinessBasicMessage];
}

#pragma mark - ---------- 网络请求 ----------
//接口地址：index.php/Enterprise/Enterprise/setEnterpriseInfo
//参数传递方式为AJAX.POST
//参数：
//'token'：用户唯一标识,
//'companyLogo'：企业图标,
//'companyName'：企业名称,
//'companyNature'：企业性质（
//'1'：'外资（欧美）','2'：'外资（非欧美）','3'：'合资','4'：'国企',
//'5'：'民营公司','6'：'上市公司','7'：'创业公司','8'：'外资代表处',
//'9'：'政府机关','10'：'事业单位','11'：'非盈利机构'
//）,
//'companyScale'：企业规模,
//'companyIndustry'：企业所属行业,
//'companyDetail'：企业介绍,
//'province'：所在省份,
//'city'：所在城市
// 更新企业基本信息的方法
- (void)updateBusinessBasicMessage{
    
    if (_notSetUserInfo) {
        
        
        NSMutableDictionary *params ;
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        
        if (token) {
            params = [@{@"token" : token} mutableCopy];
        } else {
            return ;
        }
        
        
        //公司简介不重复则可以上传
        if (![self.Briefcell.BriefTextView.text isEqualToString:self.model.result.companyDetail])
        {
            [params setValue:self.Briefcell.BriefTextView.text forKey:@"companyDetail"];
        }
        
        
        // 根据更新字典中的字段名称添加内容
        for (NSString *typeStr in self.updateContentDic) {
            NSInteger keyType = typeStr.integerValue;
            switch (keyType) {
                case ICN_BusinessCompanyLOGO:{
                    // 更新的是企业logo -- 需要先将图片上传后的路径获取在将内容传递
                    
                    break;
                }
                case ICN_BusinessCompanyNAME:{
                    // 更新的是企业名称
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyName"];
                    
                    break;
                }
                case ICN_BusinessCompanyINDUSTRYID:{
                    // 更新的是企业行业
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyIndustry"];
                    
                    break;
                }
                case ICN_BusinessCompanyAREA:{
                    // 更新的是企业地址 -- 需要添加所在省份和所在城市两个内容'province'：所在省份 'city'：所在城市
                    [params setValue:_locationIdArr[0] forKey:@"province"];
                    [params setValue:_locationIdArr[1] forKey:@"city"];
                    break;
                }
                case ICN_BusinessCompanyNATUREID:{
                    //企业性质怎么忘了！
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyNature"];
                    
                    break;
                }
                case ICN_BusinessCompanySCALE:{
                    // 更新的是企业规模
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyScale"];
                    break;
                }
                default:
                    break;
            }
        }
        
        if (nickcell.contentTextField.text.length != 0) {
            [params setValue:nickcell.contentTextField.text forKey:@"companyName"];
        }
        if ([nickcell.contentTextField.text isEqualToString:@"请输入有实际意义的内容"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写公司名称"];
            return;
        }
        NSArray *headArr;
        
        if ([self.updateContentDic valueForKey:@"60020111"]) {
            UIImage *image = [self.updateContentDic valueForKey:@"60020111"];
            headArr = [NSArray arrayWithObject:image];
        } else {
            headArr = [NSArray arrayWithObject:_upImage];
        }
        if (params.count < 8 || headArr.count < 1 || _photoCell.imageListArr.count < 1) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写完整的信息"];
        } else {
            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"头像上传中"];
            [HRNetworkingManager uploadImagesWithImageArr:headArr Success:^(ICN_CommonImagesLoadModel *model) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                
                
                [params setValue:model.result[@"fileName"] forKey:@"companyLogo"];
                
                // 根据参数传递网络请求
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UPDATEENTERPRISEMESSAGE params:params success:^(id result) {
                    BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                    if (model.code == 0) {
                        // 修改成功
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"修改成功"];
                        
                        
                        
                        if (_photoCell.imageListArr.count > 0) {
                            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"图片上传中"];
                            [HRNetworkingManager uploadImagesWithImageArr:_photoCell.imageListArr Success:^(ICN_CommonImagesLoadModel *model) {
                                
                                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                
                                // 成功后将路径信息传递过去
                                NSString *photeStr = model.result[@"fileName"];
                                NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                                dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                                dic[@"pic"] = photeStr;
                                [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_AddApprove params:dic success:^(id result) {
                                    [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                                    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                                    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                        [self presentViewController:barController animated:YES completion:nil];
                                    });
                                } failure:^(NSDictionary *errorInfo) {
                                    [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                                    dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                                    dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                        [self presentViewController:barController animated:YES completion:nil];
                                    });
                                }];
                                
                            } Failure:^(NSDictionary *errorInfo) {
                                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                HRLog(@"图片上传 - 网络请求失败");
                            }];
                        } else {
                                [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"loginStatus"];
                                BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                                [self presentViewController:barController animated:YES completion:nil];
                        }
                        
                        
                        
                        
                        
                        
                    }else{
                        // 修改失败 将内容清空并将更新内容数组清空
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"修改失败"];
                        NSLog(@"%@", result[@"info"]);
                        //            [self.updateContentDic removeAllObjects];
                        //            [self.baseMessagessTabelView reloadData];
                    }
                } failure:^(NSDictionary *errorInfo) {
                    // 网络获取失败
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络错误"];
                }];
                
                
            } Failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                HRLog(@"图片上传 - 网络请求失败");
            }];
        }


    } else {
        NSMutableDictionary *params ;
        NSString *token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        
        if (token) {
            params = [@{@"token" : token} mutableCopy];
        } else {
            return ;
        }
        
        //公司简介不重复则可以上传
        if (![self.Briefcell.BriefTextView.text isEqualToString:self.model.result.companyDetail])
        {
            [params setValue:self.Briefcell.BriefTextView.text forKey:@"companyDetail"];
        }
        
        
        // 根据更新字典中的字段名称添加内容
        for (NSString *typeStr in self.updateContentDic) {
            NSInteger keyType = typeStr.integerValue;
            switch (keyType) {
                case ICN_BusinessCompanyLOGO:{
                    // 更新的是企业logo -- 需要先将图片上传后的路径获取在将内容传递
                    
                    break;
                }
                case ICN_BusinessCompanyNAME:{
                    // 更新的是企业名称
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyName"];
                    
                    break;
                }
                case ICN_BusinessCompanyINDUSTRYID:{
                    // 更新的是企业行业
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyIndustry"];
                    
                    break;
                }
                case ICN_BusinessCompanyAREA:{
                    // 更新的是企业地址 -- 需要添加所在省份和所在城市两个内容'province'：所在省份 'city'：所在城市
                    [params setValue:_locationIdArr[0] forKey:@"province"];
                    [params setValue:_locationIdArr[1] forKey:@"city"];
                    break;
                }
                case ICN_BusinessCompanyNATUREID:{
                    //企业性质怎么忘了！
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyNature"];
                    
                    break;
                }
                case ICN_BusinessCompanySCALE:{
                    // 更新的是企业规模
                    [params setValue:[self.updateContentDic valueForKey:typeStr] forKey:@"companyScale"];
                    break;
                }
                default:
                    break;
            }
        }
        if (nickcell.contentTextField.text.length != 0) {
            [params setValue:nickcell.contentTextField.text forKey:@"companyName"];
        }
        if ([nickcell.contentTextField.text isEqualToString:@"请输入有实际意义的内容"]) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写公司名称"];
            return;
        }
        
        NSArray *headArr;
        
        if ([self.updateContentDic valueForKey:@"60020111"]) {
            UIImage *image = [self.updateContentDic valueForKey:@"60020111"];
            headArr = [NSArray arrayWithObject:image];
        } else {
            headArr = [NSArray arrayWithObject:_upImage];
        }
        
        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"头像上传中"];
        [HRNetworkingManager uploadImagesWithImageArr:headArr Success:^(ICN_CommonImagesLoadModel *model) {
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            
            
            [params setValue:model.result[@"fileName"] forKey:@"companyLogo"];
            
            // 根据参数传递网络请求
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_UPDATEENTERPRISEMESSAGE params:params success:^(id result) {
                BaseOptionalModel *model = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
                if (model.code == 0) {
                    // 修改成功
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"修改成功"];
                    
                    
                    
                    
                    
                    
                    if (_photoCell.imageListArr.count > 0) {
                        [MBProgressHUD ShowProgressToSuperView:self.view Message:@"图片上传中"];
                        [HRNetworkingManager uploadImagesWithImageArr:_photoCell.imageListArr Success:^(ICN_CommonImagesLoadModel *model) {
                            
                            [MBProgressHUD hiddenHUDWithSuperView:self.view];
                            
                            // 成功后将路径信息传递过去
                            NSString *photeStr = model.result[@"fileName"];
                            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
                            dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
                            dic[@"pic"] = photeStr;
                            [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_AddApprove params:dic success:^(id result) {
                                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                    [self.navigationController popViewControllerAnimated:YES];
                                });
                                [self.navigationController popViewControllerAnimated:YES];
                            } failure:^(NSDictionary *errorInfo) {
                                [MBProgressHUD hiddenHUDWithSuperView:self.view];
                                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                                dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                                dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                    [self.navigationController popViewControllerAnimated:YES];
                                });
                            }];
                            
                        } Failure:^(NSDictionary *errorInfo) {
                            [MBProgressHUD hiddenHUDWithSuperView:self.view];
                            HRLog(@"图片上传 - 网络请求失败");
                        }];
                    } else {
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                [self.navigationController popViewControllerAnimated:YES];
                            });
                    }

                    
                }else{
                    // 修改失败 将内容清空并将更新内容数组清空
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"修改失败"];
                    NSLog(@"%@", result[@"info"]);
                    //            [self.updateContentDic removeAllObjects];
                    //            [self.baseMessagessTabelView reloadData];
                }
            } failure:^(NSDictionary *errorInfo) {
                // 网络获取失败
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"网络错误"];
            }];
            
            
        } Failure:^(NSDictionary *errorInfo) {
            [MBProgressHUD hiddenHUDWithSuperView:self.view];
            HRLog(@"图片上传 - 网络请求失败");
        }];

    }

    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)ynetRequestWithCell:(ICN_PhotoTableViewCell *)cell{
    
    imageArr = [NSMutableArray array];
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_GetApprove params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
        
        NSString *tempStr = SF(@"%@", result[@"result"]);
        if ([tempStr isEqualToString:@"<null>"]) {
            
        } else {
        NSString *contentPic = [[result valueForKey:@"result"] valueForKey:@"pic"];
        if (contentPic != nil && [contentPic containsString:@"."]) {
            imgPath = [contentPic componentsSeparatedByString:@","];
            NSLog(@"%@", imgPath);
            
            cell.firstLoad = YES;
            cell.urlArray = [NSMutableArray arrayWithArray:imgPath];
        }
        }
        
    } failure:nil];
}

@end
