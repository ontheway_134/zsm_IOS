//
//  ICN_BusinessMassageViewController.h
//  ICan
//
//  Created by shilei on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@class ICN_GetEnterPriseModel;
@interface ICN_BusinessMassageViewController : BaseViewController

@property (nonatomic , strong)ICN_GetEnterPriseModel *model;

@property (nonatomic , assign) BOOL notSetUserInfo;

@end
