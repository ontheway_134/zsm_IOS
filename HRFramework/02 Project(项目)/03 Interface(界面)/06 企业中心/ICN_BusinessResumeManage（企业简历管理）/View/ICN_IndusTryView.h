//
//  ICN_IndusTryView.h
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CellSelectedBlock)(NSString *selectedContent);
@class ICN_PosicationListModel;

@interface ICN_IndusTryView : UIView
@property (weak, nonatomic) IBOutlet UITableView *indusTryTabelView;

@property (weak, nonatomic) IBOutlet UIView *leftHiddenBackground; // 左侧可用于隐藏的背板


@property (nonatomic , strong)NSMutableArray <ICN_PosicationListModel *>* modelsArr; // Model数组
@property (nonatomic , copy)CellSelectedBlock block;

- (void)configTableViewWihleViewWillLoad; // 在视图加载后用于配置tableView的方法
- (void)callBackWithCellSelectedBlock:(CellSelectedBlock)block;

@end
