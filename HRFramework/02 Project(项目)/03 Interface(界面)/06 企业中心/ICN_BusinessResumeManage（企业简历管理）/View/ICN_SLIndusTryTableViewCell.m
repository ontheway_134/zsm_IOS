//
//  ICN_IndusTryTableViewCell.m
//  ICan
//
//  Created by shilei on 16/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SLIndusTryTableViewCell.h"

@interface ICN_SLIndusTryTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *secondIcon;

@end

@implementation ICN_SLIndusTryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setTitleStr:(NSString *)titleStr{
    _titleStr = titleStr;
    if (_titleStr) {
        // 设置标题内容
        self.titleLabel.text = titleStr;
    }
    if (self.isShowCell) {
        self.secondIcon.hidden = NO;
    }else{
        self.secondIcon.hidden = YES;
    }
    if (self.isSelectedColor) {
        self.titleLabel.textColor = RGB0X(0x009dff);
    }else{
        self.titleLabel.textColor = RGB0X(0x333333);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
