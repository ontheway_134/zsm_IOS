//
//  ICN_SkillTagTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_LSkillTagTableViewCell.h"
#import "ICN_MySkillTagAllModel.h"
#import "ICN_SkillTagCollectionViewCell.h"
#import "ICN_SkillTagAllsModel.h"

@interface ICN_LSkillTagTableViewCell()

@property (nonatomic, strong) UICollectionView *topCollectionView;
@property (nonatomic, strong) NSMutableArray *topdataArr;
@property (nonatomic, strong) NSMutableArray *arrOfSkill;
@property (nonatomic, strong) NSMutableDictionary *dic;

@end


@implementation ICN_LSkillTagTableViewCell
- (NSMutableArray *)topdataArr
{
    if (!_topdataArr) {
        _topdataArr = [NSMutableArray array];
    }
    return _topdataArr;
}
- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    dic[@"memberId"] = self.memberId;
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_OtherMemberSkillList params:dic success:^(id result) {
        NSLog(@"feewfejfopewjeqwopj%@", result);
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                
                [self.topdataArr addObject:model];
            }
            //[self.topCollectionView reloadData];
            
            if (self.topdataArr.count > 4) {
                NSRange arrrra = NSMakeRange(0, 4);
                NSArray *arr = [self.topdataArr subarrayWithRange:arrrra];
                for (int i = 0; i< arr.count ;i++) {
                    ICN_SkillTagAllsModel *model = arr[i];
                    UIButton *buttonOf = [[UIButton alloc] init];
                    buttonOf.frame = CGRectMake(10 + i * (SCREEN_WIDTH / 5+10), 10, SCREEN_WIDTH / 5 , 30) ;
                    buttonOf.backgroundColor = [UIColor whiteColor];
                    [buttonOf setTitleColor:RGB0X(0x7e94a2) forState:UIControlStateNormal];
                    
                    [buttonOf setTitle:model.skillLabel forState:UIControlStateNormal];
                    //warn=== 暂时解决方法 修改填充标签的显示字体
                    buttonOf.titleLabel.adjustsFontSizeToFitWidth = YES;
                    buttonOf.titleLabel.minimumScaleFactor = 0.5;
                    [self addSubview:buttonOf];
                    buttonOf.layer.cornerRadius = 4;
                    buttonOf.layer.borderWidth = 1.0f;
                    buttonOf.layer.masksToBounds = YES;
                    buttonOf.layer.borderColor = RGB0X(0x7e94a2).CGColor;
                    buttonOf.titleLabel.font = [UIFont systemFontOfSize:12];
                    
                    
                    
                }
            }else {
                
                NSRange arrrra = NSMakeRange(0, self.topdataArr.count);
                NSArray *arr = [self.topdataArr subarrayWithRange:arrrra];
                for (int i = 0; i< arr.count ;i++) {
                    ICN_SkillTagAllsModel *model = arr[i];
                    UIButton *buttonOf = [[UIButton alloc] init];
                    buttonOf.frame = CGRectMake(10 + i * (SCREEN_WIDTH / 5+10), 10, SCREEN_WIDTH / 5 , 30) ;
                    buttonOf.backgroundColor = [UIColor whiteColor];
                    [buttonOf setTitleColor:RGB0X(0x7e94a2) forState:UIControlStateNormal];
                    
                    [buttonOf setTitle:model.skillLabel forState:UIControlStateNormal];
                    [self addSubview:buttonOf];
                    buttonOf.layer.cornerRadius = 4;
                    buttonOf.layer.borderWidth = 1.0f;
                    buttonOf.layer.masksToBounds = YES;
                    buttonOf.layer.borderColor = RGB0X(0x7e94a2).CGColor;
                    buttonOf.titleLabel.font = [UIFont systemFontOfSize:12];
                    
                    
                    
                }
                
                
                
            }
            
        }
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];

}
@end
