//
//  ICN_ResumeManageTableViewCell.h
//  ICan
//
//  Created by shilei on 16/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_BiographicalNotesManageModel;

@interface ICN_ResumeManageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameAndPosition;

@property (weak, nonatomic) IBOutlet UILabel *cityAndExperienceAndQualification;

@property (weak, nonatomic) IBOutlet UILabel *mobile;


- (void)setModel:(ICN_BiographicalNotesManageModel *)model;

@end
