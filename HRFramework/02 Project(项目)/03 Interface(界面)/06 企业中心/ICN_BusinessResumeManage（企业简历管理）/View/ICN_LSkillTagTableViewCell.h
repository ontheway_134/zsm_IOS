//
//  ICN_SkillTagTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MySkillTagAllModel;
@interface ICN_LSkillTagTableViewCell : UITableViewCell

@property (nonatomic, strong)ICN_MySkillTagAllModel *model;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (copy, nonatomic) NSString *memberId;

@end
