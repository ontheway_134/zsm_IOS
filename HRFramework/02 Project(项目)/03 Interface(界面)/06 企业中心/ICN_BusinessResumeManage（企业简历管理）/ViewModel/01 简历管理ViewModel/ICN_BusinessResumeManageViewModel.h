//
//  ICN_BusinessResumeManageViewModel.h
//  ICan
//
//  Created by albert on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ICN_PosicationListModel;

@protocol ICN_BusinessResumeManageViewModelDelegate <NSObject>

- (void)responseIndustryModelsRequestWithSuccess:(BOOL)success Error:(NSString *)error;

@end

@interface ICN_BusinessResumeManageViewModel : NSObject

@property (nonatomic , weak)id<ICN_BusinessResumeManageViewModelDelegate> delegate;
@property (nonatomic , strong)NSMutableArray *industryModelsArr;
@property (nonatomic , strong)NSMutableArray *searchModelsArr; // 搜索到的数组内容



- (void)requestForIndustryModelListArrData;

- (void)searchResumeWithContent:(NSString *)content;

@end
