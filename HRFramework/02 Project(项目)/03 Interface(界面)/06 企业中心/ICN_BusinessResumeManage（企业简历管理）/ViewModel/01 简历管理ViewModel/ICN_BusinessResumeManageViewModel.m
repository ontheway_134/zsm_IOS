//
//  ICN_BusinessResumeManageViewModel.m
//  ICan
//
//  Created by albert on 2017/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BusinessResumeManageViewModel.h"
#import "ICN_PosicationListModel.h"

@interface ICN_BusinessResumeManageViewModel ()

@end

@implementation ICN_BusinessResumeManageViewModel

#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray *)industryModelsArr{
    if (_industryModelsArr == nil) {
        _industryModelsArr = [NSMutableArray array];
    }
    return _industryModelsArr;
}

- (NSMutableArray *)searchModelsArr{
    if (_searchModelsArr == nil) {
        _searchModelsArr = [NSMutableArray array];
    }
    return _searchModelsArr;
}


- (void)searchResumeWithContent:(NSString *)content{
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] == nil || content == nil) {
        return ;
    }
    NSDictionary *params = @{
                             @"token" : [USERDEFAULT valueForKey:HR_CurrentUserToken],
                             @"industry" : content
                             };
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_RESUMESEARCH params:params success:^(id result) {
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
}


- (void)requestForIndustryModelListArrData{
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_RESUMESIZER params:nil success:^(id result) {
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            // 数据获取成功
            for (NSDictionary *temp in [result valueForKey:@"result"]) {
                ICN_PosicationListModel *model = [[ICN_PosicationListModel alloc] initWithDictionary:temp error:nil];
                [self.industryModelsArr addObject:model];
            }
            [self callBackWithResumeNetRequestSuccess:YES Info:nil];
        }else{
            // 数据获取失败
            [self callBackWithResumeNetRequestSuccess:NO Info:firstModel.info];
        }
    } failure:^(NSDictionary *errorInfo) {
        // 网络获取失败
        [self callBackWithResumeNetRequestSuccess:NO Info:@"网络连接失败"];
    }];
}

- (void)callBackWithResumeNetRequestSuccess:(BOOL)success Info:(NSString *)info{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(responseIndustryModelsRequestWithSuccess:Error:)]) {
            [self.delegate responseIndustryModelsRequestWithSuccess:success Error:info];
        }
    }
}

@end


