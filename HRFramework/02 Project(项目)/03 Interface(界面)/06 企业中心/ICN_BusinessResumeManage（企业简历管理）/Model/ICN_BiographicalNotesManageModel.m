//
//  ICN_BiographicalNotesManageModel.m
//  ICan
//
//  Created by Lym on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_BiographicalNotesManageModel.h"

@implementation ICN_BiographicalNotesManageModel

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"BiographicalNotesManageId" : @"id"} ];
}

+(BOOL)propertyIsOptional:(NSString*)propertyName
{
    return YES;
}

@end
