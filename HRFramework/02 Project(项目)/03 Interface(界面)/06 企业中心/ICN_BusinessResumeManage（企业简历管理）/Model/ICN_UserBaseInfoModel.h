//
//  ICN_UserBaseInfoModel.h
//  ICan
//
//  Created by Lym on 2017/1/19.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface ICN_UserBaseInfoModel : JSONModel

@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *cityName;

@property (copy, nonatomic) NSString *companyName;
@property (copy, nonatomic) NSString *hopePosition;
@property (copy, nonatomic) NSString *hopePositionName;
@property (copy, nonatomic) NSString *memberBirthday;
@property (copy, nonatomic) NSString *memberEmail;
@property (copy, nonatomic) NSString *memberGender;
@property (copy, nonatomic) NSString *memberId;
@property (copy, nonatomic) NSString *memberLogo;
@property (copy, nonatomic) NSString *memberMajor;
@property (copy, nonatomic) NSString *memberMobile;
@property (copy, nonatomic) NSString *memberNick;
@property (copy, nonatomic) NSString *memberPosition;
@property (copy, nonatomic) NSString *memberQualification;
@property (copy, nonatomic) NSString *memberQualificationName;
@property (copy, nonatomic) NSString *memberSchool;
@property (copy, nonatomic) NSString *personalizeSignature;
@property (copy, nonatomic) NSString *province;
@property (copy, nonatomic) NSString *provinceName;
@property (copy, nonatomic) NSString *workExperience;
@property (copy, nonatomic) NSString *workExperienceName;
@property (copy, nonatomic) NSString *workStatus;

@end
