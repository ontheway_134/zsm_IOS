//
//  ICN_MyOrderingTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrderingTableViewCell.h"
#import "ICN_MyMyOrderingFuFeiModel.h"
@implementation ICN_MyOrderingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_MyMyOrderingFuFeiModel *)model{

    self.title.text = model.title;
    self.beginDate.text = model.beginDate;
    [self.pic sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    self.include.text = model.include;
    if ([model.price isEqualToString:@"0"]) {
        self.price.text = @"免费";
    }else {
    NSString *str = @"¥";
    self.price.text = [str stringByAppendingString:model.price];
    
    }
    
}

@end
