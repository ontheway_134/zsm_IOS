//
//  ICN_MyOrderingDetailsView.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  ICN_MyOrderingDetailsTopModel;
@interface ICN_MyOrderingDetailsView : UIView
@property(nonatomic)UIImageView *picImageView;
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *rmbLabel;
@property(nonatomic)UILabel *numberLabel;
@property(nonatomic)UILabel *createdateLabel;
@property(nonatomic)ICN_MyOrderingDetailsTopModel *model;

@end
