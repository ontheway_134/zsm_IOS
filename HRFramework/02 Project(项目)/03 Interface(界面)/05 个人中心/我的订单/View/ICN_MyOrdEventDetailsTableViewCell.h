//
//  ICN_MyOrdEventDetailsTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyOrderingDownModel;
@interface ICN_MyOrdEventDetailsTableViewCell : UITableViewCell
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UIImageView *memberlogoImage;
@property(nonatomic)UILabel *nameLabel;
@property(nonatomic)UILabel *companynameLabel;
@property(nonatomic)UILabel *addressLabel;
@property(nonatomic)UILabel *timeLabel;
@property(nonatomic)ICN_MyOrderingDownModel *model;

@end
