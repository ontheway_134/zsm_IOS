//
//  ICN_MyOrdEventDetailsTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrdEventDetailsTableViewCell.h"
#import "ICN_MyOrderingDownModel.h"
@implementation ICN_MyOrdEventDetailsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.text = @"asdf";
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(14);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(13);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:14];
        
        UILabel *label = [[UILabel alloc]init];
        [self.contentView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(9);
            make.right.mas_equalTo(-9);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(14);
            make.height.mas_equalTo(1);
        }];
        label.backgroundColor = RGB(236, 236, 236);
        
        _memberlogoImage = [[UIImageView alloc]init];
        [self.contentView addSubview:_memberlogoImage];
        [_memberlogoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.width.mas_equalTo(35);
            make.height.mas_equalTo(35);
            
        }];
        
        _nameLabel = [[UILabel alloc]init];
        _nameLabel.text = @"asdf";
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.height.mas_equalTo(13);
        }];
        _nameLabel.textColor = RGB0X(0X000000);
        _nameLabel.font = [UIFont systemFontOfSize:14];
        
        _companynameLabel = [[UILabel alloc]init];
        _companynameLabel.text = @"fasdfa";
        [self.contentView addSubview:_companynameLabel];
        [_companynameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_nameLabel.mas_right).offset(10);
            make.height.mas_equalTo(11);
        }];
        _companynameLabel.textColor = RGB0X(0X666666);
        _companynameLabel.font = [UIFont systemFontOfSize:11];
        
        UIImageView *image = [[UIImageView alloc]init];
        image.image = [UIImage imageNamed:@"地点.png"];
        [self.contentView addSubview:image];
        [image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            
        }];
        
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.text = @"asdfasdf";
        [self.contentView addSubview:_addressLabel];
        [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(image.mas_right).offset(5);
            make.top.mas_equalTo(_companynameLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(11);
        }];
        _addressLabel.textColor = RGB0X(0X666666);
        _addressLabel.font = [UIFont systemFontOfSize:11];
        
        UIImageView *image1 =[[UIImageView alloc]init];
        image1.image = [UIImage imageNamed:@"时间.png"];
        [self.contentView addSubview:image1];
        [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.top.mas_equalTo(_addressLabel.mas_bottom).offset(10);
            
        }];
        
        _timeLabel = [[UILabel alloc]init];
        _titleLabel.text = @"sdfasdf";
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(image1.mas_right).offset(5);
            make.top.mas_equalTo(_addressLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(11);
        }];
        
        
        _timeLabel.textColor = RGB0X(0X666666);
        _timeLabel.font = [UIFont systemFontOfSize:11];
        
        
        UILabel *label2 = [[UILabel alloc]init];
        [self.contentView addSubview:label2];
        [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_timeLabel.mas_bottom).offset(15);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(-0);
            make.height.mas_equalTo(4);
        }];
        label2.backgroundColor = RGB(236, 236, 236);
    }
    
    return self;
    
    
}
//-(void)setModel:(ICN_MyOrderingDownModel *)model{
//    
//    _titleLabel.text = model.title;
//    [_memberlogoImage sd_setImageWithURL:[NSURL URLWithString:model.memberlogo]];
//    _nameLabel.text = model.membernick;
//    _companynameLabel.text = [NSString stringWithFormat:@"%@ | %@",model.companyname,model.memberposition];
//    _addressLabel.text = model.address;
//    _timeLabel.text = model.time;
//    
//    
//    
//}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
