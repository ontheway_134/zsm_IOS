//
//  ICN_MyOrderingViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyOrderingViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_MyMyOrderingViewController.h"
#import "ICN_MyMyOrderFreeViewController.h"
@interface ICN_MyOrderingViewController ()<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;


@end

@implementation ICN_MyOrderingViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        _userMySelf = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.isMe) {
        self.navigationItem.title = @"我的订单";
    }else{
        self.navigationItem.title = SF(@"%@的订单",self.userName);
    }
    
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    //显示滚动条
    style.showLine = YES;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    style.scrollTitle = YES;
    style.autoAdjustTitlesWidth = YES;
    style.adjustCoverOrLineWidth = YES;
    style.selectedTitleColor = RGB0X(0X009dff);
    style.scrollLineColor = RGB0X(0X009dff);
    style.segmentHeight = 40;
    self.titles = @[@"付费活动",
                    @"免费活动",
                    ];
     self.childVcs = [self setupChildVc];
    // 初始化
    ZJScrollPageView *scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.0) segmentStyle:style titles:self.titles parentViewController:self delegate:self];
    [scrollPageView setSelectedIndex:self.index animated:YES];
    [self.view addSubview:scrollPageView];
    
}




#pragma mark ----------------ZJScrollPageViewDelegate---------------------
- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}
- (NSArray *)setupChildVc {
    
    ICN_MyMyOrderingViewController *vc1 = [ICN_MyMyOrderingViewController new];
    vc1.memId = self.memId;
    if (!self.isMe) {
        vc1.deleteAction = NO;
    }
    ICN_MyMyOrderFreeViewController *vc2 = [ICN_MyMyOrderFreeViewController new];
    if (!self.isMe) {
        vc2.deleteAction = NO;
    }
    vc2.memId = self.memId;
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2,nil];
    return childVcs;
}


- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        childVc = self.childVcs[index];
    }
    
    
    
    
    return childVc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
