//
//  ICN_MyOrderingViewController.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_MyOrderingViewController : BaseViewController
@property (nonatomic,assign) NSInteger  index;

// 用户的Id
@property (nonatomic,copy) NSString *memId;

@property (nonatomic , copy)NSString *userName; // 在用户不是本人的时候显示用户的名字


@property (nonatomic , assign , getter=isMe)BOOL userMySelf; // 是不是用户自己 -- 默认不是


@end
