//
//  ICN_MyMyOrderingViewController.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@interface ICN_MyMyOrderingViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>

@property (nonatomic , assign , getter=isAbleToDelete)BOOL deleteAction; // 能否删除

@property (nonatomic,copy) NSString *memId;
@end
