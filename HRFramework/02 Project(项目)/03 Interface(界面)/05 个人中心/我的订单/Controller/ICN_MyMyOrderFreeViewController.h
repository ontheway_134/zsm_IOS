//
//  ICN_MyMyOrderFreeViewController.h
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@interface ICN_MyMyOrderFreeViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>


@property (nonatomic,copy) NSString *memId;

@property (nonatomic , assign , getter=isAbleToDelete)BOOL deleteAction; // 能否删除


@end
