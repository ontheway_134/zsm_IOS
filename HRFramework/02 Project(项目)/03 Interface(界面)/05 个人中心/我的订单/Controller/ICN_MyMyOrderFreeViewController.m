//
//  ICN_MyMyOrderFreeViewController.m
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_MyMyOrderFreeViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_MyOrderingTableViewCell.h"
#import "ICN_MyOrdEventDetailsViewController.h"
#import "ICN_MyMyOrderingFuFeiModel.h"
#import "ICN_ActivityParticularsViewController.h"
@interface ICN_MyMyOrderFreeViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;



@end

@implementation ICN_MyMyOrderFreeViewController

- (instancetype)init{
    self = [super init];
    if (self) {
        _deleteAction = YES; // 默认可以删除
    }
    return self;
}

- (void)zj_viewWillAppearForIndex:(NSInteger)index {
    if (!self.tableView.mj_header.isRefreshing) {
        [self.tableView.mj_header beginRefreshing];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
    //    [self configData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
    
    // Do any additional setup after loading the view.
}

- (void)configData{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    
    [dic setObject:token forKey:@"token"];
    dic[@"activityclass"] = @"3";
    dic[@"memberId"] = self.memId;
    dic[@"page"] = @"1";
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_ActivityListmember params:dic success:^(id result) {
        
        NSLog(@"QQQQQQQQQQQQQQQQQQQQQQQQQQQ%@", result);
        NSArray *arr1 =result[@"result"];
        NSLog(@"%@",arr1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in arr1 ) {
            ICN_MyMyOrderingFuFeiModel *model = [[ICN_MyMyOrderingFuFeiModel alloc]initWithDictionary:dic error:nil];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];

    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"%@",errorInfo);
        
    }];

    
    
}


- (void)zj_viewDidLoadForIndex:(NSInteger)index {
    //    NSLog(@"%@",self.view);
    //    NSLog(@"%@", self.zj_scrollViewController);
    
    
}



- (void)zj_viewDidAppearForIndex:(NSInteger)index {
    // NSLog(@"viewDidAppear-----");
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 140;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_MyOrderingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_MyOrderingTableViewCell" forIndexPath:indexPath];
    ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    if (self.isAbleToDelete == NO) {
        cell.delButton.hidden = YES;
    }else{
        cell.delButton.hidden = NO;
    }
    [cell.delButton addAction:^(NSInteger tag) {
        
        // 删除按钮需要添加提示窗
        ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
        UIAlertController *deleteWarnView = [UIAlertController alertControllerWithTitle:model.title message:@"是否确认删除订单" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancerAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // 添加删除相关的网络请求
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
            dic[@"activityId"] = model.baseId;
            
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_Activitydel params:dic success:^(id result) {
                [self configData];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"数据请求失败"];
            }];

        }];
        [deleteWarnView addAction:cancerAction];
        [deleteWarnView addAction:confirmAction];
        [self presentViewController:deleteWarnView animated:YES completion:nil];

    }];
    
    return cell;
    
    
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ICN_ActivityParticularsViewController *vc = [[ICN_ActivityParticularsViewController alloc] init];
    ICN_MyMyOrderingFuFeiModel *model = self.dataArr[indexPath.row];
    vc.url = model.baseId;
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)creationView{
    
    self.navigationItem.title = @"我的订单";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 104);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyOrderingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MyOrderingTableViewCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setMJRefresh {
    //下拉刷新
    //    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(configData)];
    //上拉加载更多
    //    self.ResumeManageTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(mjMoreNetRequest)];
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    
    [self.tableView.mj_header beginRefreshing];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
