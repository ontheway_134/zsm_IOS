//
//  ICN_MyOrderingDownModel.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_MyOrderingDownModel : NSObject
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *memberlogo;
//名字
@property(nonatomic)NSString *membernick;
//公司
@property(nonatomic)NSString *companyname;
//职位
@property(nonatomic)NSString *memberposition;
//地址
@property(nonatomic)NSString *address;
//日期
@property(nonatomic)NSString *time;
@end
