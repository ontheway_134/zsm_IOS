//
//  ICN_MyMyOrderingFuFeiModel.h
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyMyOrderingFuFeiModel : BaseOptionalModel
@property (nonatomic, copy) NSString *price;//价钱
@property (nonatomic, copy) NSString *title;//标题
@property (nonatomic, copy) NSString *pic;//图片
@property (nonatomic, copy) NSString *beginDate;//时间
@property (nonatomic, copy) NSString *include;
@property (nonatomic, copy) NSString *baseId;
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *activityclass;//判断


@end
