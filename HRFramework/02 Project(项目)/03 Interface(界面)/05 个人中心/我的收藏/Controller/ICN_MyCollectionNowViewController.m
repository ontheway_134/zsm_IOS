//
//  ICN_MyCollectionNowViewController.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCollectionNowViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_MyCollectingViewController.h"
#import "ICN_MyCollectionInformationViewController.h"
#import "ICN_quizViewController.h"   //提问
#import "ICN_activityViewController.h"   //活动
#import "ICN_informationViewController.h"   //资讯
@interface ICN_MyCollectionNowViewController ()
<ZJScrollPageViewDelegate>
@property(strong, nonatomic)NSArray<NSString *> *titles;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;


@end

@implementation ICN_MyCollectionNowViewController


//－－－－－－－－我的收藏

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的收藏";
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    //显示滚动条
    style.showLine = YES;
    // 颜色渐变
    style.gradualChangeTitleColor = YES;
    style.scrollTitle = YES;
    style.autoAdjustTitlesWidth = YES;
    style.adjustCoverOrLineWidth = YES;
    style.selectedTitleColor = RGB0X(0X009dff);
    style.scrollLineColor = RGB0X(0X009dff);
    style.segmentHeight = 40;
    self.titles = @[@"全部",
                    @"职位",@"资讯",@"活动",@"提问"
                    ];
    self.childVcs = [self setupChildVc];
    
    // 初始化
    ZJScrollPageView *scrollPageView = [[ZJScrollPageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64.0) segmentStyle:style titles:self.titles parentViewController:self delegate:self];
    [scrollPageView setSelectedIndex:self.index animated:YES];
    scrollPageView.contentView.scrollView.scrollEnabled = NO;
    [self.view addSubview:scrollPageView];
    
}




#pragma mark ----------------ZJScrollPageViewDelegate---------------------
- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}

- (NSArray *)setupChildVc {
    
#warning 这里设置子控制器
    
    ICN_MyCollectingViewController *vc1 = [ICN_MyCollectingViewController new];
    
    ICN_MyCollectionInformationViewController *vc2 = [ICN_MyCollectionInformationViewController new];
    
    ICN_informationViewController *information =[[ICN_informationViewController alloc]init];
    
    ICN_activityViewController *activity=[[ICN_activityViewController alloc]init];
    
    ICN_quizViewController *quiz=[[ICN_quizViewController alloc]init];
    
     
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2, information, activity, quiz,nil];
    return childVcs;
}

- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    
    if (!childVc) {
        childVc = self.childVcs[index];
    }
    
    
    
    
    return childVc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
