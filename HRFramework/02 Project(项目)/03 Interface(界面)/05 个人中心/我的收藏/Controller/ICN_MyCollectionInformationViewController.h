//
//  ICN_MyCollectionInformationViewController.h
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@interface ICN_MyCollectionInformationViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>

@end
