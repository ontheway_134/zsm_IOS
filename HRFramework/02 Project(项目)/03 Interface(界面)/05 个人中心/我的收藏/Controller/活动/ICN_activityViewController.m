//
//  ICN_activityViewController.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_activityViewController.h"

@interface ICN_activityViewController ()

@property(nonatomic,strong)UITableView *activityTableView;
@property(nonatomic,strong)NSMutableArray *activityArr;

@end

@implementation ICN_activityViewController

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)activityArr{
    if (!_activityArr) {
        _activityArr=[NSMutableArray array];
    }
    return _activityArr;
}

@end
