//
//  ICN_informationViewController.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_informationViewController.h"
#import "ICN_MyCollectionInformationTableViewCell.h"

@interface ICN_informationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)UITableView *informationTableView;
@property(nonatomic,strong)NSMutableArray *informationArr;

@end

@implementation ICN_informationViewController

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewWillAppear:(BOOL)animated{
   
}

- (void)zj_viewWillAppearForIndex:(NSInteger)index {
   
    if (!self.informationTableView.mj_header.isRefreshing) {
        [self.informationTableView.mj_header beginRefreshing];
    }
    
}

#pragma mark - --- Protocol ---


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.informationArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 103;
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_MyCollectionInformationTableViewCell *cell = [self.informationTableView dequeueReusableCellWithIdentifier:@"ICN_MyCollectionInformationTableViewCell" forIndexPath:indexPath];
    ICN_MyInformationCollModel *model = self.informationArr[indexPath.row];
//    cell.model = model;
    return cell;
    
    
}
#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
//        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
//        ICN_MyInformationCollModel *model = self.dataArr[indexPath.row];
//        dic[@"id"] = model.infoid;
//        
//        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_mypositionNewsCollectDel params:dic success:^(id result) {
//            
//            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
//            
//        } failure:^(NSDictionary *errorInfo) {
//            NSLog(@"请求数据失败");
//        }];
//        
//        
//        
//        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
//        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell
        
    }
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"取消收藏";
}



#pragma mark - --- 懒加载 ---

-(NSMutableArray *)informationArr{
    if (!_informationArr) {
        _informationArr=[NSMutableArray array];
    }
    return _informationArr;
}

@end
