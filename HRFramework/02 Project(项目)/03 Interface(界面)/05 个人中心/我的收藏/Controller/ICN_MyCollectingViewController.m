//
//  ICN_MyCollectingViewController.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCollectingViewController.h"
#import "ICN_MyCollcetingTableViewCell.h"
#import "ICN_MyCollcetPositionModel.h"
#import "ICN_IndustryNextViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_PositionNextOneViewController.h"
#import "ICN_PositionNextModel.h"

@interface ICN_MyCollectingViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

@implementation ICN_MyCollectingViewController


//－－－－－－－－－－全部------------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
}

- (void)zj_viewDidLoadForIndex:(NSInteger)index {
    //    NSLog(@"%@",self.view);
    //    NSLog(@"%@", self.zj_scrollViewController);
}

- (void)zj_viewWillAppearForIndex:(NSInteger)index {
    // NSLog(@"viewWillAppear------");
    if (!self.tableView.mj_header.isRefreshing) {
        [self.tableView.mj_header beginRefreshing];
    }
    
}


- (void)zj_viewDidAppearForIndex:(NSInteger)index {
    // NSLog(@"viewDidAppear-----");
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_MyCollcetingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_MyCollcetingTableViewCell" forIndexPath:indexPath];
    ICN_MyCollcetPositionModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    
    return cell;
    
    
}
#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        ICN_MyCollcetPositionModel *model = self.dataArr[indexPath.row];
        dic[@"id"] = model.positionId;
        
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_doDeletePositionCollection params:dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        
        
        
        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell

    }

    
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"取消收藏";
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
    icn.hidesBottomBarWhenPushed = YES;
    ICN_MyCollcetPositionModel *model = self.dataArr[indexPath.row];
    icn.url = model.positionId;
    NSLog(@"YYYYYYYYYYYYYYYYYYYYYYYYYYYY%@", icn.url);
    [self.navigationController pushViewController:icn animated:YES];
    
    
}

- (void)creationView{
    
    self.navigationItem.title = @"我的收藏";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.tableFooterView = [[UITableView alloc] initWithFrame:CGRectZero];
    
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    
}

- (void)configData{
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberCollection/positionCollectionList" params:nil success:^(id result) {
        
        NSArray *dic1 =result[@"result"];
        NSLog(@"SSSSSSSSSSSSSSSSSSSSSSSSSSSS%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyCollcetPositionModel *model = [[ICN_MyCollcetPositionModel alloc]initWithDictionary:dic error:nil];
            //===待解决问题   不清楚id的属性有什么用
//            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyCollcetingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MyCollcetingTableViewCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMJRefresh {
    //下拉刷新
    //    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(configData)];
    //上拉加载更多
    //    self.ResumeManageTabelView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(mjMoreNetRequest)];
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    
    [self.tableView.mj_header beginRefreshing];
}

@end
