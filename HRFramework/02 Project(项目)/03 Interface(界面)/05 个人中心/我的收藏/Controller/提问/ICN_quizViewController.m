//
//  ICN_quizViewController.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_quizViewController.h"

@interface ICN_quizViewController ()

@property(nonatomic,strong)UITableView *quizTableView;
@property(nonatomic,strong)NSMutableArray *quizArr;

@end

@implementation ICN_quizViewController

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark - --- 懒加载 ---

-(NSMutableArray *)quizArr{
    if (!_quizArr) {
        _quizArr=[NSMutableArray array];
    }
    return _quizArr;
}

@end
