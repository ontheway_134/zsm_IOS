//
//  ICN_MyCollcetingTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyCollcetPositionModel;
@interface ICN_MyCollcetingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *companyLogo;
@property (weak, nonatomic) IBOutlet UILabel *positionTitle;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *money;
@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *workType;
@property (weak, nonatomic) IBOutlet UILabel *IndustryId;
@property (nonatomic, strong) ICN_MyCollcetPositionModel *model;
@end
