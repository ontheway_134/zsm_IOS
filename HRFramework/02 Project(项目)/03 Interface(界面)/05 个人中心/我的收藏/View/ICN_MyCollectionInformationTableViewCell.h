//
//  ICN_MyCollectionInformationTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyInformationCollModel;
@interface ICN_MyCollectionInformationTableViewCell : UITableViewCell

@property(nonatomic, strong)UILabel *titleLabel;
@property(nonatomic, strong)UIImageView *picImage;
@property(nonatomic, strong)UILabel *dataLabel;
@property(nonatomic, strong)UILabel *pageLabel;
@property (nonatomic, strong) ICN_MyInformationCollModel *model;

@end
