//
//  ICN_MyCollectionInformationTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCollectionInformationTableViewCell.h"
#import "ICN_MyInformationCollModel.h"
@implementation ICN_MyCollectionInformationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 98)];
        [self.contentView addSubview:view];
        
        
        _picImage = [[UIImageView alloc]init];
        [view addSubview:_picImage];
        [_picImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.right.mas_equalTo(self.contentView).offset(-10);
            make.size.mas_equalTo(CGSizeMake(70, 70));
            
        }];
        
        
        _titleLabel = [[UILabel alloc]init];
        [view addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(_picImage.mas_left).offset(-15);
            make.height.mas_equalTo(28);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.numberOfLines = 0;
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.image = [UIImage imageNamed:@"时间.png"];
        [view addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            
        }];
        
        _dataLabel = [[UILabel alloc]init];
        [view addSubview:_dataLabel];
        [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            make.left.mas_equalTo(imageV.mas_right).offset(5);
            make.height.mas_equalTo(11);
        }];
        _dataLabel.textColor = RGB0X(0X666666);
        _dataLabel.font = [UIFont systemFontOfSize:11];
        
        UIImageView *imageV2 = [[UIImageView alloc]init];
        imageV2.image = [UIImage imageNamed:@"阅读量.png"];
        [view addSubview:imageV2];
        [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28.8);
            make.left.mas_equalTo(_dataLabel.mas_right).offset(15);
            
        }];
        
        _pageLabel = [[UILabel alloc]init];
        [view addSubview:_pageLabel];
        [_pageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            make.left.mas_equalTo(imageV2.mas_right).offset(5);
            make.height.mas_equalTo(11);
        }];
        _pageLabel.textColor = RGB0X(0X666666);
        _pageLabel.font = [UIFont systemFontOfSize:11];
        
        UIView *view2 = [[UIView alloc]init];
        view2.backgroundColor = RGB(236, 236, 236);
        
        [self.contentView addSubview:view2];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.left.mas_equalTo(self.contentView.mas_left).offset(0);
            make.top.mas_equalTo(view.mas_bottom).offset(0);
            make.height.mas_equalTo(5);
        }];
    }
    
    return self;
    
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self dealDeleteButton];
}
- (void)dealDeleteButton{
    for (UIView *subView in self.subviews) {
        
        if ([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            
            subView.backgroundColor = [UIColor blueColor];
            
            for (UIButton *button in subView.subviews) {
                
                if ([button isKindOfClass:[UIButton class]]) {
                    
                    button.backgroundColor = RGB0X(0x7e94a2);
                    button.titleLabel.font = [UIFont systemFontOfSize:14.0];
                    
                }
            }
        }
    }
}




- (void)setModel:(ICN_MyInformationCollModel *)model{
    [_picImage sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    _titleLabel.text = model.title;
    _dataLabel.text = model.createDate;
    _pageLabel.text = model.pageViews;
    

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
