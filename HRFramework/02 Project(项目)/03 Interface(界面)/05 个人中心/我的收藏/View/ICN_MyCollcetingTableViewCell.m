//
//  ICN_MyCollcetingTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyCollcetingTableViewCell.h"
#import "ICN_MyCollcetPositionModel.h"
@implementation ICN_MyCollcetingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     [self setNeedsLayout];
}
- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self dealDeleteButton];
}
- (void)dealDeleteButton{
    for (UIView *subView in self.subviews) {
        
        if ([subView isKindOfClass:NSClassFromString(@"UITableViewCellDeleteConfirmationView")]) {
            
            subView.backgroundColor = [UIColor blueColor];
            
            for (UIButton *button in subView.subviews) {
                
                if ([button isKindOfClass:[UIButton class]]) {
                    
                    button.backgroundColor = RGB0X(0x7e94a2);
                    button.titleLabel.font = [UIFont systemFontOfSize:14.0];
                    
                }
            }
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ICN_MyCollcetPositionModel *)model{

    self.positionTitle.text = model.positionTitle;

    if ([model.industryName isEqualToString:@""]) {
        self.IndustryId.hidden = YES;
    }else {
    self.IndustryId.text = model.industryName;
    }
    if ([model.workType isEqualToString:@"3"]) {
        
        self.workType.text = @"实习";
    }else if ([model.workType isEqualToString:@"2"]){
    
        self.workType.text = @"兼职";
    } else if ([model.workType isEqualToString:@"1"]) {
    
        self.workType.text = @"全职";
    }

    self.companyName.text = model.companyName;
    [self.companyLogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
    NSString *minSalaryStr =  SF(@"%ld",model.maxSalary.integerValue / 1000);
    NSString *str = [minSalaryStr stringByAppendingString:@"-"];
    NSString *maxSalaryStr = SF(@"%ld", model.minSalary.integerValue / 1000);
    NSString *str2 = [maxSalaryStr stringByAppendingString:@"k"];
    self.money.text = [str stringByAppendingString:str2];

    self.cityName.text = model.cityName;
    


}

@end
