//
//  ICN_MyConcerningTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyConcerningTableViewCell.h"
#import "ICN_MyConcerningModel.h"
@implementation ICN_MyConcerningTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)callBackWithEditBlock:(EditBtnBlock)block{
    self.block = block;
}

- (IBAction)quXiao:(UIButton *)sender {
    if (self.block) {
        self.block(self.model);
    }
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_MyConcerningModel *)model{

    _model = model;
    [self.logoImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
    self.logoImage.layer.cornerRadius = self.logoImage.bounds.size.width*0.5;
    self.logoImage.layer.masksToBounds = YES;
    NSString *str = [model.companyNatureName.description stringByAppendingString:@"|"];
    self.companyNumberLab.text = [str stringByAppendingString:SF(@"%@人",model.companyScale)];
    self.industryName.text = model.IndustryName;
    self.companyContentLab.text = model.companyName;


}
@end
