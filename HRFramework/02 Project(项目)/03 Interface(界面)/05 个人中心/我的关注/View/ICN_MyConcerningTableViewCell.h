//
//  ICN_MyConcerningTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyConcerningModel;

typedef void(^EditBtnBlock)(NSString *modelId);

@interface ICN_MyConcerningTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;
@property (weak, nonatomic) IBOutlet UILabel *companyContentLab;
@property (weak, nonatomic) IBOutlet UILabel *companyNumberLab;
@property (weak, nonatomic) IBOutlet UILabel *industryName;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic , copy)EditBtnBlock block;

@property (nonatomic, strong) ICN_MyConcerningModel *model;

- (void)callBackWithEditBlock:(EditBtnBlock)block;

@end
