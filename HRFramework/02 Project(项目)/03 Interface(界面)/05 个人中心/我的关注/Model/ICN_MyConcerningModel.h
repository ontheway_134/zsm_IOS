//
//  ICN_MyConcerningModel.h
//  ICan
//
//  Created by apple on 2016/12/21.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_MyConcerningModel : NSObject
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *memberId;//
@property (nonatomic, copy) NSString *enterpriseId;//
@property (nonatomic, copy) NSString *companyLogo;//企业图标
@property (nonatomic, copy) NSString *companyName;//企业名称
@property (nonatomic, copy) NSString *companyNature;//企业性质
@property (nonatomic, copy) NSString *companyNatureName;
@property (nonatomic, copy) NSString *companyScale;//企业规模
@property (nonatomic, copy) NSString *IndustryName;//所属行业
@property (nonatomic, copy) NSString *id;


@end
