//
//  ICN_MyConcerningViewController.h
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_MyConcerningModel;
@interface ICN_MyConcerningViewController : BaseViewController
@property (nonatomic, strong) ICN_MyConcerningModel *model;


@end
