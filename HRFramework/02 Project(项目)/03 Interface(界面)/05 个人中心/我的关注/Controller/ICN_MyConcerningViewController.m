//
//  ICN_MyConcerningViewController.m
//  ICan
//
//  Created by apple on 2016/12/15.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyConcerningViewController.h"
#import "ICN_MyConcerningTableViewCell.h"
#import "ICN_MyConcerningModel.h"
#import "ICN_CompanyDetialVC.h"

@interface ICN_MyConcerningViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *dict;
@end

@implementation ICN_MyConcerningViewController

#pragma mark - ---------- 生命周期 ----------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
}
- (void)creationView{
    
    self.navigationItem.title = @"我的关注";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
}

- (void)regeditCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyConcerningTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MyConcerningTableViewCell"];
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- Cell的数量 ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.dataArr.count;
    
}
#pragma mark - ---------- cell的高度 ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
    
}
#pragma mark - ---------- 每个cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_MyConcerningTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_MyConcerningTableViewCell" forIndexPath:indexPath];
    //    [cell setSelectionStyle:UITableViewCellSelectionStyleNone]; //去除点击阴影
//    [cell.cancelButton addTarget:self action:@selector(delegateButton) forControlEvents:UIControlEventTouchUpInside];
    
   [cell callBackWithEditBlock:^(NSString *modelId) {

       NSMutableDictionary *dic = [NSMutableDictionary dictionary];
       dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
       ICN_MyConcerningModel *model = self.dataArr[indexPath.row];
       dic[@"enterpriseId"] = model.enterpriseId;
       
       [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_doDeleteConcern params:dic success:^(id result) {
           
           [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
           [self.tableView.mj_header beginRefreshing];
       } failure:^(NSDictionary *errorInfo) {
           NSLog(@"请求数据失败");
       }];
       
   }];
    
    ICN_MyConcerningModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ICN_CompanyDetialVC *vc = [[ICN_CompanyDetialVC alloc] init];
    ICN_MyConcerningModel *model = self.dataArr[indexPath.row];
    vc.companyId = model.enterpriseId;
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)configData{
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *page = @"1";
    [dic setObject:page forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberConcern/getConcernList" params:dic success:^(id result) {
        
        
        NSLog(@"mmmmmmmmmmmmmmmmmmmmmmmmmmmm%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyConcerningModel *model = [[ICN_MyConcerningModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}
- (void)delegateButton:(NSString *)modelId{

    
//    NSLog(@"DDDDDDDDDDDDDDDDDDDDDDDDDDDDD%@", self.dict);
//    //18740018407
//    NSString *token;
//    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
//        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
//    }
//    self.model.id = modelId;
//    
//    [self.dict setObject:token forKey:@"token"];
//    [self.dict setObject:modelId forKey:@"id"];
//    
//    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"删除中"];
//    
//    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyDeleteConcerning params:self.dict success:^(id result) {
//        
//        if (firstModel.code == 0) {
//            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除成功"];
//
//            // 数据获取完成
//            
//            
//        }else{
//            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            
//        }
//    } failure:^(NSDictionary *errorInfo) {
//        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
//        
//    }];
//
    
    
    
}

- (void)setMJRefresh {
    //下拉刷新
    //下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    [self.tableView.mj_header beginRefreshing];
    
}


@end
