//
//  ICN_MyMessageDetailsModel.h
//  ICan
//
//  Created by apple on 2017/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyMessageDetailsModel : BaseOptionalModel
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *createDate;
@property(nonatomic)NSString *pic;
@property(nonatomic)NSString *content;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *subject;
@property (nonatomic) NSString *img;
//举报
@property(nonatomic)NSString *isreport;
//收藏
@property(nonatomic)NSString *iscollect;
@property (nonatomic, copy) NSString *id;

@end
