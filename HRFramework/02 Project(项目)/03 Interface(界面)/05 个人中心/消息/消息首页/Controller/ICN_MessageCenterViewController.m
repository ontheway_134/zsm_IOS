//
//  ICN_MessageCenterViewController.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MessageCenterViewController.h"
#import "ICN_MessageDetailsViewController.h"
#import "ICN_MessageDetailsMModel.h"
#import "ICN_MessageCenterTableViewCell.h"
#import "ICN_IndustryNextViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_MyMessageDetailsModel.h"
@interface ICN_MessageCenterViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSInteger pageNum;
}
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation ICN_MessageCenterViewController
#pragma mark - ---------- 生命周期 ----------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];

}
- (void)configData{
    
    pageNum = 1;
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberMessage/memberMessageList" params:dic success:^(id result) {
        NSLog(@"sssssssssssssssssssssssssssss%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@" sssssssssssssssssssssssssss%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MessageDetailsMModel *model = [[ICN_MessageDetailsMModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

}

- (void)creationView{

    self.navigationItem.title = @"消息";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    pageNum = 1;

}
- (void)regeditCell{

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MessageCenterTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_MessageCenterTableViewCell"];
    

}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- Cell的数量 ----------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
   return self.dataArr.count;
//    return  3;
    
}

#pragma mark - ---------- 每个cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_MessageCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ICN_MessageCenterTableViewCell" forIndexPath:indexPath];
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone]; //去除点击阴影
    
    ICN_MessageDetailsMModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    

    
    return cell;
    
    
}
#pragma mark - ---------- cell的高度 ----------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
    
    
}

#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        ICN_MessageDetailsMModel *model = self.dataArr[indexPath.row];
        dic[@"id"] = model.id;
        
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_MyDeletedoDeleteMessage params:dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:result[@"info"]];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        
        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell
        
        
    }

    
    
    
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    ICN_MessageDetailsViewController *vc = [[ICN_MessageDetailsViewController alloc] init];
    ICN_MessageDetailsMModel *model = _dataArr[indexPath.row];
   
//
//    ICN_IndustryModel *model = _dataArr[indexPath.row];
    
    vc.url = model.id;
    
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc
                                         animated:YES];
    

    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setMJRefresh {
    //下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    //上拉加载更多
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreNetRequest)];
}

- (void)moreNetRequest {

    pageNum = pageNum + 1;
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    [dic setObject:SF(@"%ld", pageNum) forKey:@"page"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberMessage/memberMessageList" params:dic success:^(id result) {
        NSLog(@"sssssssssssssssssssssssssssss%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@" sssssssssssssssssssssssssss%@",dic1);
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MessageDetailsMModel *model = [[ICN_MessageDetailsMModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
}

@end
