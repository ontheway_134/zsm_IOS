//
//  ICN_MessageCenterTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MessageCenterTableViewCell.h"
#import "ICN_MessageDetailsMModel.h"
#import "ToolAboutTime.h"
@implementation ICN_MessageCenterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_MessageDetailsMModel *)model{

    if ([model.type isEqualToString:@"1"]) {
        _announcementImage.image = [UIImage imageNamed:@"公告"];
    } else {
        _announcementImage.image = [UIImage imageNamed:@"系统通知"];
    }
    self.titleLab.text = model.title;
    self.subjectLab.text = model.subject;
    self.createDateLab.text = [ToolAboutTime getTimeStrByTimeSp:model.createDate];

}
@end
