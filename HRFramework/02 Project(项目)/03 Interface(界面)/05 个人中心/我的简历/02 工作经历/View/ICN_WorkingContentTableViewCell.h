//
//  ICN_WorkingContentTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_EditTrainAndExModel;
@interface ICN_WorkingContentTableViewCell : UITableViewCell<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (weak, nonatomic) IBOutlet UITextView *signTextView;
@property (weak, nonatomic) IBOutlet UILabel *ContentLab;
@property (nonatomic, strong) ICN_EditTrainAndExModel *editTrainModel;

@end
