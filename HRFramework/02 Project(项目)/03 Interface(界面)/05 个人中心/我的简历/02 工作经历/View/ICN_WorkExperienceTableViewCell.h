//
//  ICN_WorkExperienceTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_WorkingExperienceModel;

typedef void(^EditBlock)(ICN_WorkingExperienceModel *model);

@interface ICN_WorkExperienceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;
@property (weak, nonatomic) IBOutlet UILabel *workTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *contentWorkLab;
@property (nonatomic , copy) EditBlock block;
@property (nonatomic, strong) ICN_WorkingExperienceModel *model;

- (void)callBackEditBlock:(EditBlock)block;

@end
