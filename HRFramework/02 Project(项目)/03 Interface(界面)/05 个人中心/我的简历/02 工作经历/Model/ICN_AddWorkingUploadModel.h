//
//  ICN_AddWorkingUploadModel.h
//  ICan
//
//  Created by apple on 2016/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_AddWorkingUploadModel : BaseOptionalModel
@property (nonatomic, copy) NSString *entryDate;
@property (nonatomic, copy) NSString *outData;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *position;
@property (nonatomic, copy) NSString *summary;
@end
