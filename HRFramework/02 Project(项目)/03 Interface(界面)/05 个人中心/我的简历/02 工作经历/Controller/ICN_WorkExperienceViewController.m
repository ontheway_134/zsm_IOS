//
//  ICN_WorkExperienceViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_WorkExperienceViewController.h"
#import "ICN_WorkExperienceTableViewCell.h"
#import "ICN_EditWorkingExperienceViewController.h"
#import "ICN_AddWorkingExperienceViewController.h"
#import "ICN_WorkingExperienceModel.h"
@interface ICN_WorkExperienceViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@end

@implementation ICN_WorkExperienceViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    
    
    // Do any additional setup after loading the view.
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 100;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    ICN_WorkExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_WorkExperienceTableViewCell" forIndexPath:indexPath];
    [cell callBackEditBlock:^(ICN_WorkingExperienceModel *model) {
        [self editWorkExperienceWithModel:model];
    }];

    ICN_WorkingExperienceModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    return cell;


}

- (void)configData{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }

    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/WorkExperience/workExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =[result objectForKey:@"result"];
//index.php/Member/WorkExperience/workExperienceList
        NSLog(@"%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_WorkingExperienceModel *model = [[ICN_WorkingExperienceModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    

    

}

- (void)creationView{

    self.navigationItem.title = @"工作经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    self.tableView.tableFooterView.userInteractionEnabled = YES;
    UIButton *buttonAddWork = [[UIButton alloc] init];
    [buttonAddWork addTarget:self action:@selector(buttonAddWork) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView.tableFooterView addSubview:buttonAddWork];
    buttonAddWork.backgroundColor = RGB(38, 135, 250);
    buttonAddWork.layer.cornerRadius = 5.0f;
    buttonAddWork.layer.masksToBounds = YES;
    buttonAddWork.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonAddWork mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.tableFooterView).with.offset(35);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(40);
        
    }];
    [buttonAddWork setTitle:@"添加工作履历" forState:UIControlStateNormal];

    
    

    
    
    

}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_WorkExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_WorkExperienceTableViewCell"];

    
}
//编辑工作
- (void)editWorkExperienceWithModel:(ICN_WorkingExperienceModel *)model{

    ICN_EditWorkingExperienceViewController *vc = [[ICN_EditWorkingExperienceViewController alloc] init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
    

}
//添加工作履历
- (void)buttonAddWork{
    ICN_AddWorkingExperienceViewController *vc = [[ICN_AddWorkingExperienceViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
