//
//  ICN_EditWorkingExperienceViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EditWorkingExperienceViewController.h"
#import "ICN_NickNameTableViewCell.h"
#import "ICN_WorkingContentTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_EditWorkingExModel.h"
#import "ICN_WorkingExperienceModel.h"

@interface ICN_EditWorkingExperienceViewController ()<UITableViewDelegate, UITableViewDataSource,birthdayDelegate>

{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *educationHighest;
    ICN_WorkingContentTableViewCell *workingContentCell;
    
    
    ICN_NickNameTableViewCell *nickcell;
    ICN_NickNameTableViewCell *positioncell;
    ICN_NoImageTableCell *timecell1;
    ICN_NoImageTableCell *timecell2;
    
}

@property (nonatomic, strong) UITableView *tableView;

@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (nonatomic, strong) NSMutableDictionary *dict;
@end

@implementation ICN_EditWorkingExperienceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    
    self.dict = [[NSMutableDictionary alloc] init];
    // Do any additional setup after loading the view.
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 4;
    }else {
    
        return 1;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 40;
    }else {
    
        return 150;
    
    }
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        
    
    
        if (indexPath.row == 0) {
            nickcell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
            nickcell.leftLabel.text = @"公司名称";
            nickcell.rightTextField.text = self.model.companyName;
            return nickcell;
        } else if (indexPath.row == 1) {
            positioncell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell" forIndexPath:indexPath];
            positioncell.leftLabel.text = @"你的职位";
            positioncell.rightTextField.text = self.model.position;
            return positioncell;
        
        }else if (indexPath.row == 2) {
            timecell1 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
            timecell1.leftLabel.text = @"入职时间";
            timecell1.rightLabel.text = self.model.entryDate;
            timecell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return timecell1;
        } else {
            timecell2 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
            timecell2.leftLabel.text = @"离职时间";
            timecell2.rightLabel.text = self.model.outDate;
            timecell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return timecell2;
        }
    

    
    }else {
        workingContentCell  = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_WorkingContentTableViewCell" forIndexPath:indexPath];
        workingContentCell.ContentLab.text = @"工作经历";
        workingContentCell.signTextView.text = self.model.summary;
       
        return workingContentCell;
    
    }
    
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 2) {
            [self setUserBirthday];
        }else if (indexPath.row == 3) {
            [self setUserquit];
        
            
        
        }
    }
    
}
- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}
- (void)dismissContactView {
    [grayView removeFromSuperview];
}
#pragma mark - ---------- 入职时间 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    self.birthdayView.saveBirthdayButton.tag = 2000;
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)birthdayChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:2 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempBirthdayStr == NULL) {
        tempBirthdayStr = @"1960年1月";
    }
    cell.rightLabel.text = tempBirthdayStr;
    [self.dict setObject:cell.rightLabel.text forKey:@"entryDate"];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1960年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    if (self.birthdayView.saveBirthdayButton.tag == 2000) {
        
        tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
    } else {
        educationHighest = [NSString stringWithFormat:@"%@%@",year,month];

        
    }
}

#pragma mark - ---------- 离职时间 ----------
- (void)setUserquit {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    self.birthdayView.saveBirthdayButton.tag = 3000;
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(quitChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)quitChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:3 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (educationHighest == NULL) {
        educationHighest = @"1990年1月";
    }
    cell.rightLabel.text = educationHighest;
    [self.dict setObject:cell.rightLabel.text forKey:@"outDate"];
    [grayView removeFromSuperview];
}



- (void)creationView{

    self.navigationItem.title = @"编辑工作经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    self.tableView.tableFooterView.userInteractionEnabled = YES;
    UIButton *buttonDeleteWork = [[UIButton alloc] init];
    [self.tableView.tableFooterView addSubview:buttonDeleteWork];
    buttonDeleteWork.backgroundColor =RGB(230, 47, 39);

    buttonDeleteWork.layer.cornerRadius = 5.0f;
    buttonDeleteWork.layer.masksToBounds = YES;
    buttonDeleteWork.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonDeleteWork mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.tableFooterView).with.offset(35);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(40);
        
    }];
    [buttonDeleteWork setTitle:@"删除工作履历" forState:UIControlStateNormal];
    [buttonDeleteWork addTarget:self action:@selector(buttonDeleteWork) forControlEvents:UIControlEventTouchUpInside];
    


}
- (void)regeditCell{

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NickNameTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_NickNameTableViewCell"];

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_WorkingContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_WorkingContentTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NoImageTableCell" bundle:nil] forCellReuseIdentifier:@"ICN_NoImageTableCell"];
    
}
//删除工作履历
- (void)buttonDeleteWork{

    NSLog(@"DDDDDDDDDDDDDDDDDDDDDDDDDDDDD%@", self.dict);
    //18740018407
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    [self.dict setObject:token forKey:@"token"];
    [self.dict setObject:self.model.id forKey:@"expId"];
    
     [MBProgressHUD ShowProgressToSuperView:self.view Message:@"删除中"];
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyWorkingDeleteExperience params:self.dict success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        
        if (firstModel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除成功"];
            // 数据获取完成
            [self.navigationController popViewControllerAnimated:YES];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
        
    }];

}
//保存
- (void)saveAction{
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        
        [self.dict setObject:token forKey:@"token"];
        
        if (nickcell.rightTextField.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写公司名称"];
        } else if (positioncell.rightTextField.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写职位"];
        } else if (timecell1.rightLabel.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写入职时间"];
        } else if (timecell2.rightLabel.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写离职时间"];
        } else if (workingContentCell.signTextView.text.length == 0) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写工作经历"];
        } else {
            NSInteger year1 = [[timecell1.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
            NSInteger month1 = timecell1.rightLabel.text.length == 7 ? [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
            
            NSInteger year2 = [[timecell2.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
            NSInteger month2 = timecell2.rightLabel.text.length == 7 ? [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
            if (year1>year2) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
            } else if (year1 == year2 && month1 > month2) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
            } else {
                [self.dict setObject:self.model.id forKey:@"expId"];
                [self.dict setObject:nickcell.rightTextField.text forKey:@"companyName"];
                [self.dict setObject:positioncell.rightTextField.text forKey:@"position"];
                [self.dict setObject:workingContentCell.signTextView.text forKey:@"summary"];
                [self.dict setObject:timecell1.rightLabel.text forKey:@"entryDate"];
                [self.dict setObject:timecell2.rightLabel.text forKey:@"outDate"];
                
                [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyWorkingEditExperience params:self.dict success:^(id result) {
                    
                    
                    [self.navigationController popViewControllerAnimated:YES];
                } failure:^(NSDictionary *errorInfo) {
                    
                    
                }];
            }
            
        }
        
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
