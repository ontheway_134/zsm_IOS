//
//  ICN_SkillTagAllsModel.h
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_SkillTagAllsModel : BaseOptionalModel
@property (nonatomic, copy) NSString *skillLabel;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *labelId;
@property (strong,nonatomic)NSString * id;

@end
