//
//  ICN_SkillTagDeleteCollectionViewCell.h
//  ICan
//
//  Created by apple on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_SkillTagAllsModel;
@interface ICN_SkillTagDeleteCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *contentLab;
@property (nonatomic, strong) ICN_SkillTagAllsModel *model;
@end
