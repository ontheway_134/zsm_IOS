//
//  ICN_PreviewResumeViewController.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PreviewResumeViewController.h"
#import "ICN_Basic informationTableViewCell.h"
#import "ICN_SkillTagTableViewCell.h"
#import "ICN_ResumeWorkExperienceTableViewCell.h"
#import "ICN_ResumeEducationExperienceTableViewCell.h"
#import "ICN_ResumeTrainTableViewCell.h"
#import "ICN_MyResumeSection.h"


#import "ICN_MyResumeWorkExModel.h"
#import "ICN_MyResumeEducationExperienceModel.h"
#import "ICN_MyResumeTrainExperienceModel.h"
#import "ICN_MyResumeBasicInformModel.h"


#import "ICN_SkillTagAllsModel.h"

@interface ICN_PreviewResumeViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *arrOfWork;
@property (nonatomic, strong) NSMutableArray *arrOfEducation;
@property (nonatomic, strong) NSMutableArray *arrOfTrain;
@property (nonatomic, strong) NSMutableArray *arrOfXiao;


@property (nonatomic,strong)  NSMutableArray *topdataArr;

@end

@implementation ICN_PreviewResumeViewController
- (NSMutableArray *)topdataArr
{
    if (!_topdataArr) {
        _topdataArr = [NSMutableArray array];
    }
    return _topdataArr;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self configDataWork];
    [self configDataEducation];
    [self configDataTrain];
    [self configDataxiaoxi];
    [self getDataSkill];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];

}



#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

#pragma mark - ---------- Section的内容 ----------
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"基本信息";
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
        
    }else if (section == 1) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"技能标签";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
        
    }else if (section == 2) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"工作经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        
        return sectionWork;
    } else if (section == 3) {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.titleLabel.text = @"教育经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return sectionWork;
        
        
    } else {
        
        ICN_MyResumeSection *sectionWork = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"ICN_MyResumeSection"];
        sectionWork.backgroundColor = [UIColor whiteColor];
        
        sectionWork.titleLabel.text = @"培训经历";
        sectionWork.bianjiButton.hidden = YES;
        sectionWork.titleLabel.textColor = RGB0X(0x009dff);
        sectionWork.titleLabel.font = [UIFont systemFontOfSize:13];
        sectionWork.contentView.backgroundColor = [UIColor whiteColor];
        return sectionWork;
        
    }
    
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 45;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (section == 0) {
        return 1;
    } else if (section == 1) {
    
        return 1;
    } else if (section == 2) {
    
        return  self.arrOfWork.count;
    } else if (section == 3) {
    
        return  self.arrOfEducation.count;
        
    }else {
    
        return self.arrOfTrain.count;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 75;
    } else if (indexPath.section == 1){
        return 87;
    } else if (indexPath.section == 2) {
        return 100;
    } else {
    
        return 80;
    }
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    

    if (indexPath.section == 0) {
        
        ICN_Basic_informationTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_Basic informationTableViewCell" forIndexPath:indexPath];

        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeBasicInformModel *model = self.arrOfXiao.firstObject;
        cell.model = model;
        return cell;
        
    }else if (indexPath.section == 1) {
        
        ICN_SkillTagTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_SkillTagTableViewCell" forIndexPath:indexPath];


        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        
        cell.dataSource = self.topdataArr;
        return cell;
        
        
    }else if (indexPath.section == 2) {
        
        ICN_ResumeWorkExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeWorkExperienceTableViewCell" forIndexPath:indexPath];

        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeWorkExModel *model = self.arrOfWork[indexPath.row];
        cell.modelWork = model;
        return cell;
        
    }else if (indexPath.section == 3){
        
        ICN_ResumeEducationExperienceTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeEducationExperienceTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeEducationExperienceModel *model = self.arrOfEducation[indexPath.row];
        cell.modelEducation = model;
        return cell;
        
        
    }else {
        
        
        ICN_ResumeTrainTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_ResumeTrainTableViewCell" forIndexPath:indexPath];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//设置cell点击效果
        ICN_MyResumeTrainExperienceModel *model = self.arrOfTrain[indexPath.row];
        cell.modeloOfTrain = model;
        return cell;
        
    }
    
    
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == self.tableView)
        
    {
        
        CGFloat sectionHeaderHeight = 64; //你的header高度
        
        if (scrollView.contentOffset.y <= sectionHeaderHeight && scrollView.contentOffset.y >= 0) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
            
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
            
        }
        
    }
    
    
    
}


#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)creationView{
    
    self.navigationItem.title = @"我的简历";
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT- 64);
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = RGB(235, 236, 237);
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
    
    
    
}
- (void)regeditCell{
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_PerfectTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_PerfectTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_Basic informationTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_Basic informationTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_SkillTagTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_SkillTagTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeWorkExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeWorkExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeEducationExperienceTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeEducationExperienceTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeTrainTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeTrainTableViewCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_ResumeHideTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_ResumeHideTableViewCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_MyResumeSection" bundle:nil] forHeaderFooterViewReuseIdentifier:@"ICN_MyResumeSection"];
    
}

//处理工作经历接口
- (void)configDataWork{
    
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/WorkExperience/workExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        
        self.arrOfWork = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeWorkExModel *model = [[ICN_MyResumeWorkExModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfWork addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

//处理教育经历接口
- (void)configDataEducation{
    
    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/EducationExperience/educationExperienceList" params:dic success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfEducation = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeEducationExperienceModel *model = [[ICN_MyResumeEducationExperienceModel alloc]init];
            [dic setValue:model.expID forKey:@"id"];
            
            
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfEducation addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}
//培训经历
- (void)configDataTrain{
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/TrainExperience/trainExperienceList" params:nil success:^(id result) {
        NSLog(@"fsdaaaaaaaaaaaaaa%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        self.arrOfTrain = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_MyResumeTrainExperienceModel *model = [[ICN_MyResumeTrainExperienceModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [self.arrOfTrain addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

//消息接口
- (void)configDataxiaoxi{
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
        
        NSLog(@"ededededededededededededededededdddde%@", result);
        
        NSDictionary *dic1 =result[@"result"];
        
        NSLog(@"111111111111111111111111%@",dic1);
        _arrOfXiao = [[NSMutableArray alloc]init];
        
        
        ICN_MyResumeBasicInformModel *model = [[ICN_MyResumeBasicInformModel alloc] initWithDictionary:dic1 error:nil];
        [_arrOfXiao addObject:model];
        
        
        [_tableView reloadData];
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDataSkill{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    NSString *str = @"1";
    [dic setObject:str forKey:@"page"];
    
    
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MemberSkillList params:@{@"token":token} success:^(id result) {
        NSLog(@"feewfejfopewjeqwopj%@", result);
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (firstModel.code == 0) {
            self.topdataArr = nil;
            for (NSDictionary *dic in [result valueForKey:@"result"]) {
                ICN_SkillTagAllsModel *model = [[ICN_SkillTagAllsModel alloc] initWithDictionary:dic error:nil];
                
                [self.topdataArr addObject:model];
            }
            [self.tableView reloadData];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        
        
    }];
    
}

@end
