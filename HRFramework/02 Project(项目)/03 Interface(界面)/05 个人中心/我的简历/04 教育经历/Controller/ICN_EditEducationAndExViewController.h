//
//  ICN_EditEducationAndExViewController.h
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_EducationAndExperienceModel;
@interface ICN_EditEducationAndExViewController : BaseViewController
@property (nonatomic, strong) ICN_EducationAndExperienceModel *modelEd;

@end
