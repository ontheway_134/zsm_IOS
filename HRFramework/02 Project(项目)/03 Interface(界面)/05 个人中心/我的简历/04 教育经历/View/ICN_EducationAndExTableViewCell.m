//
//  ICN_EducationAndExTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EducationAndExTableViewCell.h"
#import "ICN_EducationAndExperienceModel.h"
@implementation ICN_EducationAndExTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)callBackEditBlock:(EditBlock)block{
    self.block = block;
}
- (IBAction)jiaoyuButton:(UIButton *)sender {
    if (self.block) {
        self.block(self.model);
    }
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_EducationAndExperienceModel *)model{
    _model = model;
    NSString *str = [model.enrolDate.description stringByAppendingString:@"-"];
    self.timeLab.text = [str stringByAppendingString:model.graduationDate];
    self.schoolLab.text = model.school;
    NSString *str2 = [model.qualificationName.description stringByAppendingString:@"|"];
    self.qualificationAndMajor.text = [str2 stringByAppendingString:model.major];
    


}
@end
