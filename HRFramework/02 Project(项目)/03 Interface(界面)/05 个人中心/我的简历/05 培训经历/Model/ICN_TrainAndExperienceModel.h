//
//  ICN_TrainAndExperienceModel.h
//  ICan
//
//  Created by apple on 2016/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_TrainAndExperienceModel : BaseOptionalModel

@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *id;

@end
