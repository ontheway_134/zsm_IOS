//
//  ICN_EditTrainAndExViewController.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_EditTrainAndExViewController.h"
#import "ICN_WorkingContentTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_EditTrainAndExModel.h"
#import "ICN_TrainAndExperienceModel.h"


@interface ICN_EditTrainAndExViewController ()<UITableViewDelegate, UITableViewDataSource,birthdayDelegate,UITextViewDelegate>
{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *strasdf;
    
    ICN_NoImageTableCell *timecell1;
    ICN_NoImageTableCell *timecell2;
    
    ICN_WorkingContentTableViewCell *workingContentCell;
    
}

@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) ICN_WorkingContentTableViewCell *cellOfWork;
@end

@implementation ICN_EditTrainAndExViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataArr = [[NSMutableArray alloc] init];
    self.dict = [[NSMutableDictionary alloc] init];
    self.cellOfWork = [[ICN_WorkingContentTableViewCell alloc] init];
    self.cellOfWork.signTextView.delegate = self;
//    [self configData];
    [self creationView];
    [self regeditCell];
    
    // Do any additional setup after loading the view.
}
//textview delegate
#pragma mark ##############方法没走?????????????????????/
- (void)textViewDidChange:(UITextView *)textView
{
    
   
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView{

    [self.dict setObject:textView.text forKey:@"summary"];
    
}
//处理数据
- (void)configData{
    
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://zsm.4pole.cn/index.php/Member/TrainExperience/trainExperienceDetail" params:nil success:^(id result) {
        NSLog(@"ededededededededededededededededdddde%@", result);
        NSArray *dic1 =result[@"result"];
        NSLog(@"%@",dic1);
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_EditTrainAndExModel *model = [[ICN_EditTrainAndExModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }else {
        
        return 1;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 40;
    }else {
        
        return 150;
        
    }
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
        
        timecell1 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
        timecell1.leftLabel.text = @"开始时间";
        timecell1.rightLabel.text = self.modelTrain.startDate;
        
            
            
       
        timecell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return timecell1;
        }else {
            timecell2 = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell" forIndexPath:indexPath];
            timecell2.leftLabel.text = @"结束时间";
            timecell2.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

            timecell2.rightLabel.text = self.modelTrain.endDate;
            
            return timecell2;
            
        
        }

    }else {
        
        workingContentCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_WorkingContentTableViewCell" forIndexPath:indexPath];
        workingContentCell.ContentLab.text = @"培训介绍";
        
        workingContentCell.signTextView.text = self.modelTrain.summary;
        

        return workingContentCell;
        
    }
    
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self setUserBirthday];
        }else if (indexPath.row == 1) {
            [self setUserquit];
            
            
            
        }
    }
    
}
- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}
- (void)dismissContactView {
    [grayView removeFromSuperview];
}
#pragma mark - ---------- 开始时间 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    self.birthdayView.saveBirthdayButton.tag = 2000;
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)birthdayChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:0 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (tempBirthdayStr == NULL) {
        tempBirthdayStr = @"1966年1月";
    }
    cell.rightLabel.text = tempBirthdayStr;
    
    
    [self.dict setObject:cell.rightLabel.text forKey:@"startDate"];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1960年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    if (self.birthdayView.saveBirthdayButton.tag == 2000) {
        
        tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
    }else {
    
        strasdf = [NSString stringWithFormat:@"%@%@",year,month];
    }
}

#pragma mark - ---------- 结束时间 ----------
- (void)setUserquit {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    self.birthdayView.saveBirthdayButton.tag = 1000;
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(quitChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)quitChoose {
    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:1 inSection:0];//获取cell的位置
    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (strasdf == NULL) {
        strasdf = @"1960年1月";
    }
    cell.rightLabel.text = strasdf;

    
    [self.dict setObject:cell.rightLabel.text forKey:@"endDate"];
    [grayView removeFromSuperview];
}




//创建View
- (void)creationView{
    
    self.navigationItem.title = @"编辑培训经历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    [self.view addSubview:self.tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60)];
    self.tableView.tableFooterView.userInteractionEnabled = YES;
    UIButton *buttonDeleteTrain = [[UIButton alloc] init];
    [self.tableView.tableFooterView addSubview:buttonDeleteTrain];
    buttonDeleteTrain.backgroundColor =RGB(230, 47, 39);
    
    buttonDeleteTrain.layer.cornerRadius = 5.0f;
    buttonDeleteTrain.layer.masksToBounds = YES;
    buttonDeleteTrain.titleLabel.font = [UIFont systemFontOfSize:14];
    [buttonDeleteTrain mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.tableFooterView).with.offset(35);
        make.left.equalTo(self.view).with.offset(20);
        make.right.equalTo(self.view).with.offset(-20);
        make.height.offset(40);
        
    }];
    [buttonDeleteTrain setTitle:@"删除培训经历" forState:UIControlStateNormal];
    [buttonDeleteTrain addTarget:self action:@selector(buttonDeleteTrain) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
//注册cell
- (void)regeditCell{

    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_WorkingContentTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_WorkingContentTableViewCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"ICN_NoImageTableCell" bundle:nil] forCellReuseIdentifier:@"ICN_NoImageTableCell"];
}
//删除培训经历
- (void)buttonDeleteTrain
{
    NSLog(@"DDDDDDDDDDDDDDDDDDDDDDDDDDDDD%@", self.dict);
    //18740018407
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    [self.dict setObject:token forKey:@"token"];
    [self.dict setObject:self.modelTrain.id forKey:@"expId"];
    [MBProgressHUD ShowProgressToSuperView:self.view Message:@"删除中"];
    [[[HRNetworkingManager alloc] init] POST_PATH:PATH_DeleteTrainExperience params:self.dict success:^(id result) {
        
        BaseOptionalModel *firstModel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];

        if (firstModel.code == 0) {
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除成功"];
            [self.navigationController popViewControllerAnimated:YES];
            // 数据获取完成
            
            
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
    } failure:^(NSDictionary *errorInfo) {
        [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"删除失败"];
        
        
    }];
    

}
//保存
- (void)saveAction{
    
    
    if ([timecell1.rightLabel.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入入学时间"];
    } else if ([timecell2.rightLabel.text isEqualToString:@"请输入"]) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入毕业时间"];
    } else if (workingContentCell.signTextView.text.length == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请输入内容"];
    } else {
        
        NSInteger year1 = [[timecell1.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
        NSInteger month1 = timecell1.rightLabel.text.length == 7 ? [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell1.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
        
        NSInteger year2 = [[timecell2.rightLabel.text substringWithRange:NSMakeRange(0, 4)] intValue];
        NSInteger month2 = timecell2.rightLabel.text.length == 7 ? [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 1)] intValue] : [[timecell2.rightLabel.text substringWithRange:NSMakeRange(5, 2)] intValue];
        if (year1>year2) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
        } else if (year1 == year2 && month1 > month2) {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的时间"];
        } else {
            
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            [self.dict setObject:token forKey:@"token"];
            [self.dict setObject:self.modelTrain.id forKey:@"expId"];
            [self.dict setObject:workingContentCell.signTextView.text forKey:@"summary"];
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MyWorkingEditTrainEx params:self.dict success:^(id result) {
                
                
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSDictionary *errorInfo) {
                
                
            }];
            
            
        }
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
