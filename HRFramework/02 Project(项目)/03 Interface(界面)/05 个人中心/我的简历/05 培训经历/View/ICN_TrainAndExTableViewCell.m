//
//  ICN_TrainAndExTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_TrainAndExTableViewCell.h"
#import "ICN_TrainAndExperienceModel.h"
#import "ICN_AddTrainAndExModel.h"
#import "ICN_EditTrainAndExModel.h"

@implementation ICN_TrainAndExTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)callBackEditBlock:(EditBlock)block{
    self.block = block;
}
- (IBAction)peixunButton:(UIButton *)sender {
    if (self.block) {
        self.block(self.model);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_TrainAndExperienceModel *)model{
    _model = model;
    NSString *str = [model.startDate.description stringByAppendingString:@"-"];
    self.timeOfTrainLab.text = [str stringByAppendingString:model.endDate];
    self.contentOfTrainLab.text = model.summary;
    
    


}
- (void)setAddModel:(ICN_AddTrainAndExModel *)addModel{

    NSString *str = [addModel.startDate.description stringByAppendingString:@"-"];
    self.timeOfTrainLab.text = [str stringByAppendingString:addModel.endDate];
    self.contentOfTrainLab.text = addModel.summary;
    

    
}


@end
