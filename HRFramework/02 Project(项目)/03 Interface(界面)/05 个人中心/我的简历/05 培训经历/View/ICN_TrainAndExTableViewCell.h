//
//  ICN_TrainAndExTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_AddTrainAndExModel;
@class ICN_TrainAndExperienceModel;
typedef void(^EditBlock)(ICN_TrainAndExperienceModel *model);
@interface ICN_TrainAndExTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *editTrainExBtn;
@property (weak, nonatomic) IBOutlet UILabel *timeOfTrainLab;
@property (weak, nonatomic) IBOutlet UILabel *contentOfTrainLab;
@property (nonatomic, strong) ICN_AddTrainAndExModel *addModel;
@property (nonatomic, strong) ICN_TrainAndExperienceModel *model;


@property (nonatomic , copy) EditBlock block;


- (void)callBackEditBlock:(EditBlock)block;


@end
