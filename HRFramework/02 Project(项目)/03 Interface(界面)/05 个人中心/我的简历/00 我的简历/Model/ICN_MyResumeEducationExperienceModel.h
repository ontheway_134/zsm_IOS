//
//  ICN_MyResumeEducationExperienceModel.h
//  ICan
//
//  Created by apple on 2016/12/29.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyResumeEducationExperienceModel : BaseOptionalModel
@property (nonatomic, copy) NSString *enrolDate;
@property (nonatomic, copy) NSString *graduationDate;
@property (nonatomic, copy) NSString *school;
@property (nonatomic, copy) NSString *qualification;
@property (nonatomic, copy) NSString *major;
@property (nonatomic ,copy) NSString *expID;
@property (nonatomic, copy) NSString *qualificationName;
@property (nonatomic, copy) NSString *summary;

@end
