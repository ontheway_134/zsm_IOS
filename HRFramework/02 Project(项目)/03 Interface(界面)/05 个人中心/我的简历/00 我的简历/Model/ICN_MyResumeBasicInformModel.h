//
//  ICN_MyResumeBasicInformModel.h
//  ICan
//
//  Created by apple on 2017/1/6.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyResumeBasicInformModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberLogo;//用户头像
@property (nonatomic, copy) NSString *memberNick;// 用户名称
@property (nonatomic, copy) NSString *hopePositionName;//用户职位

@property (nonatomic, copy) NSString *cityName;//省份
@property (nonatomic, copy) NSString *memberQualificationName;//最高学历
@property (nonatomic, copy) NSString *workExperienceName;//工作经验
@property (nonatomic, copy) NSString *memberMobile; //手机号

@end
