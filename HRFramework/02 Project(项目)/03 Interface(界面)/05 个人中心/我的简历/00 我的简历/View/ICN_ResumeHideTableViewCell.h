//
//  ICN_ResumeHideTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ResumeHideTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *hiddenButton;

@end
