//
//  ICN_SkillLabelCollectionViewCell.h
//  ICan
//
//  Created by 何壮壮 on 17/2/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_SkillTagAllsModel.h"
@interface ICN_SkillLabelCollectionViewCell : UICollectionViewCell
@property(strong,nonatomic)ICN_SkillTagAllsModel *model ;
@property (nonatomic,strong) UILabel * label;
@end
