//
//  ICN_MyResumeSection.m
//  ICan
//
//  Created by apple on 2016/12/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_MyResumeSection.h"

@implementation ICN_MyResumeSection

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)callBackWithClickBlock:(ClickBlock)block{
    self.block = block;
}

- (IBAction)clickOnEditBtn:(UIButton *)sender {
    if (self.block) {
        self.block(self.titleLabel.text);
    }
    
}


@end
