//
//  ICN_ResumeWorkExperienceTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyResumeWorkExModel;
@interface ICN_ResumeWorkExperienceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@property (weak, nonatomic) IBOutlet UILabel *companyNameAndPosition;
@property (weak, nonatomic) IBOutlet UILabel *summaryWork;

@property (nonatomic, strong) ICN_MyResumeWorkExModel *modelWork;

@end
