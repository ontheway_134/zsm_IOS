//
//  ICN_ResumeTrainTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyResumeTrainExperienceModel;
@interface ICN_ResumeTrainTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeTrainLan;

@property (weak, nonatomic) IBOutlet UILabel *trainSummary;
@property (nonatomic, strong) ICN_MyResumeTrainExperienceModel *modeloOfTrain;
@end
