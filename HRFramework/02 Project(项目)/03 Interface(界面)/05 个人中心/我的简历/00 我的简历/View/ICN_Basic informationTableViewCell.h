//
//  ICN_Basic informationTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyResumeBasicInformModel;
@interface ICN_Basic_informationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *touXiang;
@property (weak, nonatomic) IBOutlet UILabel *mingZhi;
@property (weak, nonatomic) IBOutlet UILabel *zhiWei;
@property (weak, nonatomic) IBOutlet UILabel *xueLIZhuanYe;
@property (weak, nonatomic) IBOutlet UILabel *shouJIhao;

@property (nonatomic, strong)ICN_MyResumeBasicInformModel *model;


@end
