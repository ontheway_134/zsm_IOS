//
//  ICN_SkillTagTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SkillTagTableViewCell.h"
#import "ICN_MySkillTagAllModel.h"
#import "ICN_SkillTagCollectionViewCell.h"
#import "ICN_SkillTagAllsModel.h"
#import "ICN_SkillLabelCollectionViewCell.h"
@interface ICN_SkillTagTableViewCell()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *topdataArr;
@property (nonatomic, strong) NSMutableArray *arrOfSkill;
@property (nonatomic, strong) NSMutableDictionary *dic;

@end


@implementation ICN_SkillTagTableViewCell
- (void)setDataSource:(NSMutableArray *)dataSource
{
    _dataSource = dataSource;
    [self.collectionView reloadData];
}
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 5;
        flowLayout.minimumInteritemSpacing = 5;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH-30, SCREEN_HEIGHT-100) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [self addSubview:_collectionView];
        [_collectionView registerNib:[UINib nibWithNibName:@"ICN_SkillLabelCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ICN_SkillLabelCollectionViewCell"];
        [_collectionView setBackgroundColor:[UIColor clearColor]];
       
        //注册
        //[_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"jin"];
       
    }
     return _collectionView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self addSubview:self.collectionView];

}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ICN_SkillTagAllsModel *model = self.dataSource[indexPath.row];
    CGFloat width = [self widthForLabel:[NSString stringWithFormat:@"%@",model.skillLabel] fontSize:16];
    return CGSizeMake(width+10,22);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ICN_SkillLabelCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ICN_SkillLabelCollectionViewCell" forIndexPath:indexPath];
    ICN_SkillTagAllsModel *model = self.dataSource[indexPath.row];
    cell.model = model;
//    UILabel *label = [[UILabel alloc] init];
//    label.text = [NSString stringWithFormat:@"%@",model.skillLabel];
//    label.frame = CGRectMake(0, 0, ([self widthForLabel:label.text fontSize:16] + 10), 22);
//    label.font = [UIFont systemFontOfSize:16];
//    label.layer.cornerRadius = 2.0;
//    label.layer.masksToBounds = YES;
//    label.layer.borderWidth = 1.0;
//    label.textAlignment = NSTextAlignmentCenter;
//    label.textColor = [UIColor grayColor];
//    label.layer.borderColor = [UIColor blackColor].CGColor;
//    [cell.contentView addSubview:label];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

/**
 *  计算文字长度
 */
- (CGFloat)widthForLabel:(NSString *)text fontSize:(CGFloat)font
{
    CGSize size = [text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:font], NSFontAttributeName, nil]];
    return size.width;
}

@end
