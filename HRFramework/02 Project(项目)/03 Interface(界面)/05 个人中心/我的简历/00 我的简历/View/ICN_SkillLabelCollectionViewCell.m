//
//  ICN_SkillLabelCollectionViewCell.m
//  ICan
//
//  Created by 何壮壮 on 17/2/10.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_SkillLabelCollectionViewCell.h"

@implementation ICN_SkillLabelCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.label = [[UILabel alloc] init];
    [self.contentView addSubview:_label];
}
- (void)setModel:(ICN_SkillTagAllsModel *)model
{
    _model = model;
    _label.text = [NSString stringWithFormat:@"%@",model.skillLabel];
    _label.frame = CGRectMake(0, 0, ([self widthForLabel:_label.text fontSize:16] + 10), 22);
    _label.font = [UIFont systemFontOfSize:16];
    _label.layer.cornerRadius = 2.0;
    _label.layer.masksToBounds = YES;
    _label.layer.borderWidth = 1.0;
    _label.textAlignment = NSTextAlignmentCenter;
    _label.textColor = [UIColor grayColor];
    _label.layer.borderColor = [UIColor blackColor].CGColor;
}
/**
 *  计算文字长度
 */
- (CGFloat)widthForLabel:(NSString *)text fontSize:(CGFloat)font
{
    CGSize size = [text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:font], NSFontAttributeName, nil]];
    return size.width;
}

@end
