//
//  ICN_PerfectTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyPerfectsModel;
@interface ICN_PerfectTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageViewResume;
@property (weak, nonatomic) IBOutlet UIView *rootView;


@property (nonatomic, strong)ICN_MyPerfectsModel *model;
@end
