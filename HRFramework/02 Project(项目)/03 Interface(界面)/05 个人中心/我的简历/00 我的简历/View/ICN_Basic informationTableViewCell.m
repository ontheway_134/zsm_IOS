//
//  ICN_Basic informationTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/12.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_Basic informationTableViewCell.h"
#import "ICN_MyResumeBasicInformModel.h"
@implementation ICN_Basic_informationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.touXiang.layer.cornerRadius = self.touXiang.frame.size.height * 0.5;
    self.touXiang.layer.masksToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_MyResumeBasicInformModel *)model{

    self.mingZhi.text = model.memberNick;
    [self.touXiang sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
    self.xueLIZhuanYe.text = SF(@"%@|%@|%@", model.cityName, model.workExperienceName, model.memberQualificationName);
    NSString *str = @"手机号:";
    self.shouJIhao.text = SF(@"%@ %@", str, model.memberMobile);
    self.zhiWei.text = model.hopePositionName;
    
    
    

}
@end
