//
//  ICN_creditdetialViewController.m
//  ICan
//
//  Created by shilei on 17/2/28.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_creditdetialViewController.h"
#import "ICN_CreditTableViewCell.h"

@interface ICN_creditdetialViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *creditDetialTableView;

@property(nonatomic,strong)NSMutableArray *creditArr;


@end

@implementation ICN_creditdetialViewController

#pragma mark - --- 生命周期 ---

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.creditDetialTableView registerNib:[UINib nibWithNibName:@"ICN_CreditTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_CreditTableViewCell"];
    self.creditDetialTableView.delegate = self;
    self.creditDetialTableView.dataSource = self;
    self.creditDetialTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

}

#pragma mark - --- Protocol ---

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    ICN_CreditTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ICN_CreditTableViewCell"];
    if (cell==nil) {
        cell=[[[NSBundle mainBundle] loadNibNamed:@"ICN_CreditTableViewCell" owner:self options:nil]lastObject];
    }
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 67;
}

#pragma mark - --- IBActions ---

- (IBAction)backBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
