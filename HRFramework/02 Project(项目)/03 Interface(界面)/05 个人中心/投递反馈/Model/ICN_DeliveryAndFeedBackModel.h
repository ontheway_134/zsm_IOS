//
//  ICN_DeliveryAndFeedBackModel.h
//  ICan
//
//  Created by apple on 2016/12/27.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_DeliveryAndFeedBackModel : BaseOptionalModel

@property (nonatomic, copy) NSString *companyLogo;//用户头像
@property (nonatomic, copy) NSString *companyName;//公司名称
@property (nonatomic, copy) NSString *industryName;//公司所在行业
@property (nonatomic, copy) NSString *positionTitle;//招聘职位
@property (nonatomic, copy) NSString *cityName;//所在城市
@property (nonatomic, copy) NSString *maxSalary;//薪资
@property (nonatomic, copy) NSString *minSalary;//薪资
@property (nonatomic, copy) NSString *workTypeName;//工作类型
@property (nonatomic, copy) NSString *isFitted;//是否适合职位
@property (nonatomic, copy) NSString *page;
@property (nonatomic, copy) NSString *positionId;
@property (nonatomic, copy) NSString *workType;
@property (nonatomic, copy) NSString *industryId;
@property (nonatomic, copy) NSString *baseId;


@end
