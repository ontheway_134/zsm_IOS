//
//  ICN_DeliveAndFeedBackReadModel.m
//  ICan
//
//  Created by apple on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DeliveAndFeedBackReadModel.h"

@implementation ICN_DeliveAndFeedBackReadModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"baseId" : @"id"}];
}
@end
