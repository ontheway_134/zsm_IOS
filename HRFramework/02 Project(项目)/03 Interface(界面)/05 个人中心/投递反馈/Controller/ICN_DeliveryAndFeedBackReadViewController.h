//
//  ICN_DeliveryAndFeedBackReadViewController.h
//  ICan
//
//  Created by apple on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"
#import "ZJScrollPageViewDelegate.h"
@interface ICN_DeliveryAndFeedBackReadViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>

@end
