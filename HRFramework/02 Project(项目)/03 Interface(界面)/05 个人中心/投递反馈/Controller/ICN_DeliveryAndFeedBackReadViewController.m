//
//  ICN_DeliveryAndFeedBackReadViewController.m
//  ICan
//
//  Created by apple on 2017/1/12.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_DeliveryAndFeedBackReadViewController.h"
#import "ICN_DeliveFeedbackTableViewCell.h"
#import "ICN_DeliveAndFeedBackReadModel.h"
#import "ICN_PositionNextOneViewController.h"

@interface ICN_DeliveryAndFeedBackReadViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic)UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;


@end

@implementation ICN_DeliveryAndFeedBackReadViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creationView];
    [self regeditCell];
    [self setMJRefresh];
    //被查看简历
    // Do any additional setup after loading the view.
}
- (void)configData{
    
    // 在当前用户token存在的时候将当前用户的token写入
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    [dic setObject:token forKey:@"token"];
    //index.php/Member/MemberResume/resumeBrowsedList
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberResume/resumeBrowsedList" params:dic success:^(id result) {
        NSLog(@"WWWWWWWWWWWWWWWWWWWW%@", result);
        NSArray *dic1 =result[@"result"];
        _dataArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dic1 ) {
            ICN_DeliveAndFeedBackReadModel *model = [[ICN_DeliveAndFeedBackReadModel alloc]initWithDictionary:dic error:nil];
            
            [_dataArr addObject:model];
        }
        
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
}


- (void)zj_viewDidLoadForIndex:(NSInteger)index {
    //    NSLog(@"%@",self.view);
    //    NSLog(@"%@", self.zj_scrollViewController);
    
    
}
- (void)zj_viewWillAppearForIndex:(NSInteger)index {
    // NSLog(@"viewWillAppear------");
    [self.tableView.mj_header beginRefreshing];
    
}


- (void)zj_viewDidAppearForIndex:(NSInteger)index {
    // NSLog(@"viewDidAppear-----");
    
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 80;
    
}
#pragma mark - ---------- 每个Cell的内容 ----------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ICN_DeliveFeedbackTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_DeliveFeedbackTableViewCell" forIndexPath:indexPath];
    ICN_DeliveAndFeedBackReadModel *model = self.dataArr[indexPath.row];
    cell.modelRead = model;
    
    return cell;
    
    
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
    icn.hidesBottomBarWhenPushed = YES;
    ICN_DeliveAndFeedBackReadModel *model = self.dataArr[indexPath.row];
    icn.url = model.positionId;
    NSLog(@"YYYYYYYYYYYYYYYYYYYYYYYYYYYY%@", icn.url);
    [self.navigationController pushViewController:icn animated:YES];
    
    
}

#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        ICN_DeliveAndFeedBackReadModel *model = self.dataArr[indexPath.row];
        dic[@"id"] = model.baseId;
        
        [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_doDeleteResume params:dic success:^(id result) {
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"删除成功"];
            
        } failure:^(NSDictionary *errorInfo) {
            NSLog(@"请求数据失败");
        }];
        
        
        [self.dataArr removeObjectAtIndex:indexPath.row];  //删除数组里的数据
        [self.tableView  deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath]withRowAnimation:UITableViewRowAnimationAutomatic];  //删除对应数据的cell
        
        
    }
    
    
    
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return @"删除";
}




- (void)creationView{
    
    self.navigationItem.title = @"投递简历";
    self.tableView = [[UITableView alloc] init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    self.tableView.backgroundColor = RGB(235, 236, 237);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.tableView];
    
}

- (void)regeditCell{
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_DeliveFeedbackTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_DeliveFeedbackTableViewCell"];
    
    
}

- (void)setMJRefresh {
    //下拉刷新
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        //        [self.viewModel refreshCurrentPageContentCells];
        [self configData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    
    
    [self.tableView.mj_header beginRefreshing];}

@end
