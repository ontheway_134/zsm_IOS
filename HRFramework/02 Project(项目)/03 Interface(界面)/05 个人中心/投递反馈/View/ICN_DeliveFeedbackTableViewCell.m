//
//  ICN_DeliveFeedbackTableViewCell.m
//  ICan
//
//  Created by apple on 2016/12/14.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_DeliveFeedbackTableViewCell.h"
#import "ICN_DeliveryAndFeedBackModel.h"
#import "ICN_DeliveAndFeedBackReadModel.h"
#import "ICN_DeliveAndFeedBackAppropriateModel.h"
@implementation ICN_DeliveFeedbackTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setModel:(ICN_DeliveryAndFeedBackModel *)model{

    self.companyName.text = model.companyName;
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
//    [self.companyLogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
    if ([model.industryName isEqualToString:@""]) {
        self.companyIndustry.hidden = YES;
    }else {
        self.companyIndustry.text = model.industryName;
    }
//    self.companyIndustry.text = model.industryName;
    self.positionLab.text = model.positionTitle;
    self.cityName.text = model.cityName;
//    NSString *minSalaryStr =  SF(@"%ld",model.maxSalary.integerValue / 1000);
//    NSString *str = [minSalaryStr stringByAppendingString:@"-"];
//    NSString *maxSalaryStr = SF(@"%ld", model.minSalary.integerValue / 1000);
//    NSString *str2 = [maxSalaryStr stringByAppendingString:@"k"];
    self.salary.text = SF(@"%.1fK",model.minSalary.floatValue / 1000);
    
    if ([model.workType isEqualToString:@"3"]) {
        
        self.workType.text = @"实习";
    }else if ([model.workType isEqualToString:@"2"]){
        
        self.workType.text = @"兼职";
    } else if ([model.workType isEqualToString:@"1"]) {
        
        self.workType.text = @"全职";
    }
    
//    self.workType.text = model.workTypeName;

    if ([model.isFitted isEqualToString:@"0"]) {
        self.Fitted.text = @"已投递";
        self.Fitted.textColor = RGB(38, 135, 250);
        
    }
    

}
- (void)setModelRead:(ICN_DeliveAndFeedBackReadModel *)modelRead{

    self.companyName.text = modelRead.companyName;
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(modelRead.companyLogo)]];
    //    [self.companyLogo sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.companyLogo)]];
    if ([modelRead.industryName isEqualToString:@""]) {
        self.companyIndustry.hidden = YES;
    }else {
        self.companyIndustry.text = modelRead.industryName;
    }
//    self.companyIndustry.text = modelRead.industryName;
    if (self.companyIndustry.text.length == 0) {
        self.companyIndustry.text = @"未设置";
    }
    self.positionLab.text = modelRead.positionTitle;
    self.cityName.text = modelRead.cityName;
//    NSString *minSalaryStr =  SF(@"%ld",modelRead.minSalary.integerValue / 1000);
//    NSString *str = [minSalaryStr stringByAppendingString:@"-"];
//    NSString *maxSalaryStr = SF(@"%ld", modelRead.maxSalary.integerValue / 1000);
//    NSString *str2 = [maxSalaryStr stringByAppendingString:@"k"];
//    self.salary.text = [str stringByAppendingString:str2];
    self.salary.text = SF(@"%.1fK",modelRead.minSalary.floatValue / 1000);
    
    if ([modelRead.workType isEqualToString:@"3"]) {
        
        self.workType.text = @"实习";
    }else if ([modelRead.workType isEqualToString:@"2"]){
        
        self.workType.text = @"兼职";
    } else if ([modelRead.workType isEqualToString:@"1"]) {
        
        self.workType.text = @"全职";
    }
    
//    self.workType.text = modelRead.workTypeName;
    

    if ([modelRead.isFitted isEqualToString:@"0"]){
    self.Fitted.text = @"HR已查看,请您耐心等待";
    self.Fitted.textColor = RGB(38, 135, 250);

    }

}
- (void)setModelApp:(ICN_DeliveAndFeedBackAppropriateModel *)modelApp{
    self.companyName.text = modelApp.companyName;
    [self.headerImage sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(modelApp.companyLogo)]];

    if ([modelApp.industryName isEqualToString:@""]) {
        self.companyIndustry.hidden = YES;
    }else {
        self.companyIndustry.text = modelApp.industryName;
    }
//    self.companyIndustry.text = modelApp.industryName;
    self.positionLab.text = modelApp.positionTitle;
    self.cityName.text = modelApp.cityName;
//    NSString *minSalaryStr =  SF(@"%ld",modelApp.minSalary.integerValue / 1000);
//    NSString *str = [minSalaryStr stringByAppendingString:@"-"];
//    NSString *maxSalaryStr = SF(@"%ld", modelApp.maxSalary.integerValue / 1000);
//    NSString *str2 = [maxSalaryStr stringByAppendingString:@"k"];
//    self.salary.text = [str stringByAppendingString:str2];
    self.salary.text = SF(@"%.1fK",modelApp.minSalary.floatValue / 1000);
    
    if ([modelApp.workType isEqualToString:@"3"]) {
        
        self.workType.text = @"实习";
    }else if ([modelApp.workType isEqualToString:@"2"]){
        
        self.workType.text = @"兼职";
    } else if ([modelApp.workType isEqualToString:@"1"]) {
        
        self.workType.text = @"全职";
    }
    
    
    
//    self.workType.text = modelApp.workTypeName;
    
    //    if ([modelRead.isFitted isEqualToString:@"0"]) {
    //        self.Fitted.text = @"抱歉,您不符合我们的要求";
    //        self.Fitted.textColor = RGB0X(0xee0133);
    //
    //    }
    if ([modelApp.isFitted isEqualToString:@"0"]){
        self.Fitted.text = @"请保持电话畅通";
        self.Fitted.textColor = RGB(38, 135, 250);
        
    }

    
}

@end
