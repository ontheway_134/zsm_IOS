//
//  ICN_SsettingFeedBackModel.h
//  ICan
//
//  Created by apple on 2017/1/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_SsettingFeedBackModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberName;
@property (nonatomic, copy) NSString *memberMobile;
@property (nonatomic, copy) NSString *memberEmail;
@property (nonatomic, copy) NSString *memberQQ;
@property (nonatomic, copy) NSString *noteContent;
@end
