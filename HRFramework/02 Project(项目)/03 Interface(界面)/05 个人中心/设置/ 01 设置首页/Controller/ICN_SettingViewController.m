//
//  ICN_SettingViewController.m
//  ICan
//
//  Created by apple on 2016/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SettingViewController.h"
#import "ICN_SettingTableViewCell.h"
#import "ICN_MyAccountViewController.h"
#import "ICN_ChangeAccountViewController.h"
#import "ICN_PrivacySetViewController.h"
#import "ICN_changePasswordViewController.h"
#import "ICN_FeedbackViewController.h"
#import "ICN_AboutUsViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_SignViewController.h"
//#import "ICN_Model.h"
#import "ICN_StartViewController.h"
#import "ChatDemoHelper.h"

@interface ICN_SettingViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *cacheLabel;

@property (copy, nonatomic) NSString *authMobile;
@property (copy, nonatomic) NSString *phoneStr;
@end

@implementation ICN_SettingViewController


#pragma mark - --- 网络请求 ---

-(void)httpHttpNetworing{
    
    NSDictionary *dic =@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]};
    
     [[[HRNetworkingManager alloc]init] POST_PATH:PATH_USERLOGINOUT params:dic success:^(id result) {
        
        BaseOptionalModel *codemodel = [[BaseOptionalModel alloc] initWithDictionary:result error:nil];
        if (codemodel.code == 0) {
            [USERDEFAULT setValue:nil forKey:HR_CurrentUserToken];
            ICN_StartViewController *sign=[[ICN_StartViewController alloc]init];
            sign.hidesBottomBarWhenPushed=YES;
            [self.navigationController pushViewController:sign animated:YES];
            
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:codemodel.info];
        }
        
    } failure:^(NSDictionary *errorInfo) {
      
    }];
}


#pragma mark - ---------- 生命周期 ----------


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self ismemberMobileInfo];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"设置";
    self.view.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    
    [self configUI];
    [self regeditCell];
    
    
    
}
#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 3;
    }
    else if (section == 1){
        return 2;
        
    }else {
        return 1;
        
    }
    
}
#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    ICN_SettingTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_SettingTableViewCell"                                                                        forIndexPath:indexPath];
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            cell.leftLabel.text = @"我的账号";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else if (indexPath.row == 1){
            cell.leftLabel.text = @"隐私设置";
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }else {
            cell.leftLabel.text = @"清除缓存";
            
            self.cacheLabel = [[UILabel alloc] init];
            self.cacheLabel.textAlignment = NSTextAlignmentRight;
            self.cacheLabel.font = [UIFont systemFontOfSize:13];
            self.cacheLabel.text = [NSString stringWithFormat : @"%.2fM" , [self filePath]];
            [cell addSubview:self.cacheLabel];
            [self.cacheLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(cell).with.offset(10);
                make.right.equalTo(self.view).with.offset(-10);
                make.bottom.equalTo(cell).with.offset(-10);
                make.width.offset(100);
            }];
        }
        
        return cell;
    }
    else if (indexPath.section == 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        if (indexPath.row == 0) {
            cell.leftLabel.text = @"关于我们";
        }
        else if (indexPath.row == 1){
            cell.leftLabel.text = @"意见反馈";
        }
        return cell;
    }else {
        cell.leftLabel.text = @"密码修改";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {  //我的账号
            if ([_authMobile isEqual:[NSNull null]]||[_authMobile isEqualToString:@""]) {
                ICN_MyAccountViewController *vc = [[ICN_MyAccountViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            } else {
            
                ICN_ChangeAccountViewController *vc = [[ICN_ChangeAccountViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.phoneStr = _authMobile;
                [self.navigationController pushViewController:vc animated:YES];
            }

        }
        else if (indexPath.row == 1) { //隐私设置
        
//            ICN_PrivacySetViewController *vc = [[ICN_PrivacySetViewController alloc] init];
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//            [MBProgressHUD ShowProgressToSuperView:self.view Message:@"功能暂时关闭"];
//           [MBProgressHUD hiddenHUDWithSuperView:self.view];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"功能暂时关闭"];
        
            
            
        }else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"是否清除缓存" preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction *action) {
                 [self clearFile];
                
                }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:(UIAlertActionStyleCancel) handler:nil];
            [alert addAction:action];
            [alert addAction:cancel];
            [self showDetailViewController:alert sender:nil];
            //清除缓存
            
            
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            
            //关于我们
            ICN_AboutUsViewController *vc = [[ICN_AboutUsViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
       
        }else if(indexPath.row == 1) {
            
            ICN_FeedbackViewController *vc = [[ICN_FeedbackViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
            
            //意见反馈
    
            
        }
        
    }
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            //密码修改
            if ([USERDEFAULT valueForKey:@"loginisThirdQQ"]) {
                
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"QQ登录不可修改密码"];
                return;
            }
            if ([USERDEFAULT valueForKey:@"loginisThirdWeixin"]) {
                  [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"微信登录不可修改密码"];
                return;
            }
            if ([USERDEFAULT valueForKey:@"loginisThirdWeiBo"]) {
                  [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"微博登录不可修改密码"];
                return;
            }
            ICN_changePasswordViewController *vc = [[ICN_changePasswordViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc
                                                 animated:YES];
            
        }
    }
}



- (void)configUI {
    self.tableView = [[UITableView alloc]init];
    self.tableView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    self.tableView.tableFooterView.userInteractionEnabled = YES;
    UIButton *exitLoginBtn = [[UIButton alloc] init];
    [self.tableView.tableFooterView addSubview:exitLoginBtn];
    exitLoginBtn.layer.cornerRadius = 5;
    exitLoginBtn.layer.masksToBounds = YES;
    [exitLoginBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    exitLoginBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    exitLoginBtn.backgroundColor = [UIColor colorWithRed:41 / 255.0f green:141 / 255.0f blue:250 / 255.0f alpha:1];
    [exitLoginBtn addTarget:self action:@selector(exitLoginButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [exitLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(12);
        make.right.equalTo(self.view).with.offset(-12);
        make.top.equalTo(self.tableView.tableFooterView).with.offset(35);
        make.height.offset(40);
        
    }];
    
    
    self.tableView.backgroundColor = RGB(235, 236, 237);
}

- (void)regeditCell {
    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_SettingTableViewCell" bundle:nil] forCellReuseIdentifier:@"ICN_SettingTableViewCell"];
}
//退出登录
- (void)exitLoginButtonClick{
    
    [self httpHttpNetworing];
    
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY]) {
        if ([[USERDEFAULT valueForKey:HR_UserTypeKEY] integerValue] == HR_ICNUser) {
            // 在用户是个人用户的情况下退出环信登录
            [[ChatDemoHelper shareHelper] HXLogoutWithDeviceToken:NO];
        }
    }
    
}

//首先计算单个文件的大小

- (long long)fileSizeAtPath:(NSString *)filePath{
    
    NSFileManager * manager = [NSFileManager defaultManager];
    
    if ([manager fileExistsAtPath:filePath]){
        
        return [[manager attributesOfItemAtPath:filePath error : nil] fileSize];
        
    }
    
    return 0 ;
    
}

//遍历文件夹获得文件夹大小，返回多少M

- (float)folderSizeAtPath:(NSString *)folderPath{
    
    NSFileManager * manager = [NSFileManager defaultManager];
    
    if (![manager fileExistsAtPath:folderPath]) return 0 ;
    
    NSEnumerator *childFilesEnumerator = [[manager subpathsAtPath :folderPath] objectEnumerator];
    
    NSString * fileName;
    
    long long folderSize = 0 ;
    
    while ((fileName = [childFilesEnumerator nextObject]) != nil){
        
        NSString * fileAbsolutePath = [folderPath stringByAppendingPathComponent:fileName];
        
        folderSize += [self fileSizeAtPath:fileAbsolutePath];
        
    }
    
    return folderSize/( 1024.0 * 1024.0 );
    
}

// 显示缓存大小

- (float)filePath

{
    
    NSString * cachPath = [NSSearchPathForDirectoriesInDomains( NSCachesDirectory , NSUserDomainMask , YES) firstObject];
    
    return [self folderSizeAtPath:cachPath];
    
}

// 清理缓存

- (void)clearFile

{
    
    NSString * cachPath = [NSSearchPathForDirectoriesInDomains ( NSCachesDirectory , NSUserDomainMask , YES) firstObject];
    
    NSArray * files = [[NSFileManager defaultManager] subpathsAtPath :cachPath];
    
    NSLog (@"cachpath = %@" , cachPath);
    
    for (NSString *p in files) {
        
        NSError *error = nil ;
        
        NSString *path = [cachPath stringByAppendingPathComponent:p];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath :path]){
            
            [[NSFileManager defaultManager] removeItemAtPath :path error :&error];
            
        }
        
    }
    
    [self performSelectorOnMainThread:@selector(clearCachSuccess) withObject:nil waitUntilDone:YES];
    
}

- (void)clearCachSuccess

{
    
    NSLog(@"清理成功");
    
    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message: @"缓存清理完毕" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    
    [alertView show];
    self.cacheLabel.text = nil;
    
    [_tableView reloadData];//清理完之后重新导入数据
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)ismemberMobileInfo {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"token"] = [USERDEFAULT valueForKey:HR_CurrentUserToken];

    [[[HRNetworkingManager alloc]init ] POST_PATH:PATH_memberMobileInfo params:dic success:^(id result) {
        NSString *tempStr = SF(@"%@",result[@"code"]);
        
        if ([tempStr isEqualToString:@"0"]) {
            _authMobile = result[@"result"][@"authMobile"];
            _phoneStr = result[@"result"][@"authStatus"];
        }
        
    } failure:^(NSDictionary *errorInfo) {
        NSLog(@"请求数据失败");
    }];
}

@end
