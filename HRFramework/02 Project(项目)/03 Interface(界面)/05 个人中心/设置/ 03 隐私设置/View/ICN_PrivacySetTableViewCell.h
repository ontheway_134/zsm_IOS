//
//  ICN_PrivacySetTableViewCell.h
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_PrivacySetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headPortraitsImage;
@property (weak, nonatomic) IBOutlet UILabel *labelName;

@end
