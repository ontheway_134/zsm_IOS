//
//  ICN_PrivacySetViewController.m
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PrivacySetViewController.h"
#import "ICN_PrivacySetTableViewCell.h"
#import "ICN_SettingNormalTableViewCell.h"


@interface ICN_PrivacySetViewController ()<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *viewOfHead;
@property (nonatomic, strong) UIButton *buttonOfPublicity;
@end

@implementation ICN_PrivacySetViewController

#pragma mark - ---------- 生命周期 ----------
- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationItem.title = @"隐私设置";
    self.view.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [self creationView];
    [self regeditCell];
    
    
}
#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
    
}
#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (indexPath.row == 0) {
        
        ICN_SettingNormalTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_SettingNormalTableViewCell" forIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
            }
    else {
    
        ICN_PrivacySetTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_PrivacySetTableViewCell" forIndexPath:indexPath];
        
        return cell;

    }

        
        
        
        
}
#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

- (void)creationView{

    self.viewOfHead = [[UIView alloc] init];
    [self.view addSubview:self.viewOfHead];
    self.viewOfHead.backgroundColor = [UIColor whiteColor];
    [self.viewOfHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(0);
        make.left.equalTo(self.view).with.offset(0);
        make.right.equalTo(self.view).with.offset(0);
        make.height.offset(81);
    }];

    UILabel *labelOfPublicity = [[UILabel alloc] init];
    [self.viewOfHead addSubview:labelOfPublicity];
    labelOfPublicity.text = @"公开";
    labelOfPublicity.font = [UIFont systemFontOfSize:13];
    labelOfPublicity.backgroundColor = [UIColor whiteColor];
    [labelOfPublicity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.viewOfHead).with.offset(10);
        make.left.equalTo(self.viewOfHead).with.offset(10);
        make.height.offset(20);
        make.width.offset(120);
    }];
    
    self.buttonOfPublicity = [[UIButton alloc] init];
    [self.viewOfHead addSubview:self.buttonOfPublicity];
//    switchOfPublicity.backgroundColor = [UIColor colorWithRed:41 / 255.0f green:141 / 255.0f blue:250 / 255.0f alpha:1];
    [self.buttonOfPublicity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(labelOfPublicity.mas_centerY);
        make.right.equalTo(self.viewOfHead).with.offset(-10);
    }];
    [self.buttonOfPublicity setImage:[UIImage imageNamed:@"隐藏简历开"] forState:UIControlStateNormal];
    [self.buttonOfPublicity setImage:[UIImage imageNamed:@"隐藏简历关"] forState:UIControlStateSelected];


    [self.buttonOfPublicity addTarget:self action:@selector(buttonPublicityClick:) forControlEvents:UIControlEventValueChanged];
    
    UILabel *lableOfOneH = [[UILabel alloc] init];
    [self.viewOfHead addSubview:lableOfOneH];
    lableOfOneH.backgroundColor = [UIColor colorWithRed:236 / 255.0f green:236 / 255.0f blue:236 / 255.0f alpha:1];
    [lableOfOneH mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelOfPublicity.mas_bottom).with.offset(10);
        make.left.equalTo(self.viewOfHead).with.offset(10);
        make.right.equalTo(self.viewOfHead).with.offset(-10);
        make.height.offset(1);
    }];
    UILabel *labelOfFriend = [[UILabel alloc] init];
    [self.viewOfHead addSubview:labelOfFriend];
    labelOfFriend.text = @"仅好友可见";
    labelOfFriend.font = [UIFont systemFontOfSize:13];
    labelOfFriend.backgroundColor = [UIColor whiteColor];
    [labelOfFriend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lableOfOneH.mas_bottom).with.offset(10);
        make.left.equalTo(self.viewOfHead).with.offset(10);
        make.height.offset(20);
        make.width.offset(150);
    }];
    UIButton *buttonOfFriend = [[UIButton alloc] init];
    [self.viewOfHead addSubview:buttonOfFriend];
    [buttonOfFriend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(labelOfFriend.mas_centerY);
        make.right.equalTo(self.viewOfHead).with.offset(-10);
        
    }];
    [buttonOfFriend setImage:[UIImage imageNamed:@"隐藏简历关"] forState:UIControlStateNormal];
    [buttonOfFriend addTarget:self action:@selector(buttonFriendClick) forControlEvents:UIControlEventValueChanged];
    self.tableView = [[UITableView alloc] init];
    [self.view addSubview:self.tableView];
    self.tableView.frame = CGRectMake(0, 91, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    

    


}
#pragma mark - ---------- cell可以编辑 ----------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - ---------- cell编辑样式 ----------
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - ---------- 进入编辑模式，按下出现的编辑按钮后,进行删除操作 ----------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
}
#pragma mark - ---------- 删除 ----------
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}


- (void)regeditCell{

    [self.tableView registerNib:[UINib nibWithNibName:@"ICN_PrivacySetTableViewCell"bundle:nil] forCellReuseIdentifier:@"ICN_PrivacySetTableViewCell"];
     [self.tableView registerNib:[UINib nibWithNibName:@"ICN_SettingNormalTableViewCell"bundle:nil] forCellReuseIdentifier:@"ICN_SettingNormalTableViewCell"];
    

}
//公开
- (void)buttonPublicityClick:(UIButton *)sender{

    sender.selected = !sender.selected;


}
//好友可见
- (void)buttonFriendClick{


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
