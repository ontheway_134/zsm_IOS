//
//  ICN_AboutUsViewController.m
//  ICan
//
//  Created by apple on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_AboutUsViewController.h"
#import "ICN_FeedbackViewController.h"
#import "ICN_UserProtocolViewController.h"

@interface ICN_AboutUsViewController ()

@end

@implementation ICN_AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"关于i行";
//    [self creationView];
    
    
    
}
- (IBAction)Feedback:(UIButton *)sender {
    
    ICN_FeedbackViewController *vc = [[ICN_FeedbackViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
    
}
- (IBAction)companyWebsite:(UIButton *)sender {
    
}
- (IBAction)userProtocoluser:(UIButton *)sender {
    
    ICN_UserProtocolViewController *vc = [[ICN_UserProtocolViewController alloc] init];
    [self.navigationController pushViewController:vc
                                         animated:YES];
    
    
}








- (void)creationView{

    UILabel *labelOfAboutUs = [[UILabel alloc] init];
    [self.view addSubview:labelOfAboutUs];
    labelOfAboutUs.backgroundColor = [UIColor whiteColor];
//    labelOfAboutUs.frame = CGRectMake(0, 0, SCREEN_WIDTH, 400);
    labelOfAboutUs.font = [UIFont systemFontOfSize:13];
    [labelOfAboutUs setNumberOfLines:0];
    labelOfAboutUs.text = @"首先，我们要明确一点，同步和异步都是在线程中使用的。在iOS开发中，比如网络请求数据时，若使实打实费迪南德爱上的话费卡就收到货啊刷卡机发卡上就和刷卡机沙发看机会客户奥斯卡积分撒娇和看撒谎客户卡视角开始计划开始计划卡上框架好卡好卡加咖啡和卡号卡好卡好卡黑科技好卡建行卡按计划开花结实空间和开发计划的空间化考试计划看撒娇和地方开始交电话费看机会撒地方科技萨科技风沙发的离开家撒垃圾拉屎的离开房间杀戮空间的来刷卡缴费爱上了空间发的拉斯卡积分拉斯减肥爱上了咖啡姐了萨科技风老师的风景垃圾开发了;按实际开发了;是会计法律;咖啡豆了;看进来撒;看风景了;沙发接口拉萨复健科拉萨的接口费老师大家看法拉萨的会计法老师大家看法拉萨的会计法拉萨看风景了;圣诞节开发拉萨科技福是豆腐干豆腐告诉对方公司的风格利送积分用同步请求，则只有请求成功或者请求失败得到响应返回后，才能继续往下走，也就是才能访问其它资源（会阻塞了线程）机会感觉会根据客观环境和及韩国进口韩国进口和国际化机会感觉很高科技和国际化。";
    //初始化段落，设置段落风格
    
    NSMutableParagraphStyle *paragraphstyle=[[NSMutableParagraphStyle alloc]init];
    
    paragraphstyle.lineBreakMode=NSLineBreakByCharWrapping;
    
    
    
    //设置label的字体和段落风格
    
    NSDictionary *dic=@{NSFontAttributeName:labelOfAboutUs.font,NSParagraphStyleAttributeName:paragraphstyle.copy};
    
    //NSDictionary *dic=@{NSFontAttributeName:self.label.font};
    
    
    
    //计算label的真正大小,其中宽度和高度是由段落字数的多少来确定的，返回实际label的大小
    
    CGRect rect=[labelOfAboutUs.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH - 20, SCREEN_HEIGHT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil];
    
    
    
    //设置到屏幕顶部的距离，如果不设置就x,y都为0
    
    labelOfAboutUs.frame=CGRectMake(10, 10, rect.size.width,rect.size.height);
    
    
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
