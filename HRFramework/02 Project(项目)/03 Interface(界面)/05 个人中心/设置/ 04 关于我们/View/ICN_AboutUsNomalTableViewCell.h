//
//  ICN_AboutUsNomalTableViewCell.h
//  ICan
//
//  Created by apple on 2017/1/9.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_AboutUsNomalTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *AboutUsLabel;

@end
