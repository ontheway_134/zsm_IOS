//
//  ICN_ChangeAccountViewController.h
//  ICan
//
//  Created by apple on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseViewController.h"
@class ICN_MySettingMyAccountModel;

@interface ICN_ChangeAccountViewController : BaseViewController
@property (copy ,nonatomic) NSString *phoneStr;

@property (weak, nonatomic) IBOutlet UILabel *phoneNumLab;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumTF;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeTF;
@property (nonatomic, strong) ICN_MySettingMyAccountModel *model;
@end
