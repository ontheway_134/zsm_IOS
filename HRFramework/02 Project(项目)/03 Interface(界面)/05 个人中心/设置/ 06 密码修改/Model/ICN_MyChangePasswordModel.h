//
//  ICN_MyChangePasswordModel.h
//  ICan
//
//  Created by apple on 2016/12/22.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyChangePasswordModel : BaseOptionalModel

@property (nonatomic, copy) NSString *old;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *rePassword;

@end
