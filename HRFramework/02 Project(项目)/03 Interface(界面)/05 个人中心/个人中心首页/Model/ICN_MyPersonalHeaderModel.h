//
//  ICN_MyPersonalHeaderModel.h
//  ICan
//
//  Created by apple on 2017/1/4.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyPersonalHeaderModel : BaseOptionalModel
@property (nonatomic, copy) NSString *memberLogo;
@property (nonatomic, copy) NSString *memberNick;
@property (nonatomic, copy) NSString *memberGender;
@property (nonatomic, copy) NSString *isAuthStatus;
@property (strong,nonatomic)NSString * memberId;


@end
