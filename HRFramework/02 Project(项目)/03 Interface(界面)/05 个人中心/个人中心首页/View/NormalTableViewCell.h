//
//  NormalTableViewCell.h
//  UbiTalk
//
//  Created by Lym on 2016/11/28.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NormalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *leftImage;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelOfRight;

@end
