//
//  PersonHeaderView.h
//  UbiTalk
//
//  Created by Lym on 2016/11/23.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_MyPersonalHeaderModel;
@interface ICN_PersonHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *setUserInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *getUserMessageButton;
@property (weak, nonatomic) IBOutlet UIButton *CertificationButton;//认证
@property (weak, nonatomic) IBOutlet UIButton *edittingBtn;
@property (weak, nonatomic) IBOutlet UIImageView *memberLogo;//头像
@property (weak, nonatomic) IBOutlet UILabel *memberNick;//昵称
@property (weak, nonatomic) IBOutlet UIImageView *memberGender;//性别
@property (nonatomic, strong) ICN_MyPersonalHeaderModel *model;

@property (weak, nonatomic) IBOutlet UIButton *clearButton;


@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
@end
