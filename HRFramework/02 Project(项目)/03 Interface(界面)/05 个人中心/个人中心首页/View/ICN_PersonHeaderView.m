//
//  PersonHeaderView.m
//  UbiTalk
//
//  Created by Lym on 2016/11/23.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import "ICN_PersonHeaderView.h"
#import "ICN_MyPersonalHeaderModel.h"

@implementation ICN_PersonHeaderView
- (void)awakeFromNib {
    [super awakeFromNib];
    self.memberLogo.layer.cornerRadius = self.memberLogo.frame.size.width * 0.5;
    self.memberLogo.layer.masksToBounds = YES;
    
}

- (void)setModel:(ICN_MyPersonalHeaderModel *)model{

    [self.memberLogo sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    self.memberNick.text = model.memberNick;
    
    if ([model.memberGender isEqualToString:@"1"]) {
        self.memberGender.image = [UIImage imageNamed:@"性别男"];
        
    }else if ([model.memberGender isEqualToString:@"2"]) {
    
        self.memberGender.image = [UIImage imageNamed:@"性别女"];
    }
    
    
    if ([model.isAuthStatus isEqualToString:@"0"]) {
        [self.CertificationButton setImage:[UIImage imageNamed:@"未认证"] forState:UIControlStateNormal];
    }else if ([model.isAuthStatus isEqualToString:@"1"]) {
    
        [self.CertificationButton setImage:[UIImage imageNamed:@"已认证"] forState:UIControlStateNormal];
    }
}
@end
