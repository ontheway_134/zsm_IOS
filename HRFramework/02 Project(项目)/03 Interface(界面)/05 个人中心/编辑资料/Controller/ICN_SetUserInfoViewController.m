//
//  ICN_SetUserInfoViewController.m
//  ICan
//
//  Created by Lym on 2016/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_SetUserInfoViewController.h"
#import "ICN_HeadImageTableViewCell.h"
#import "ICN_NickNameTableViewCell.h"
#import "ICN_GanderTableViewCell.h"
#import "ICN_NoImageTableCell.h"
#import "ICN_UserSignTableViewCell.h"
#import "STPhotoKitController.h"
#import "UIImagePickerController+ST.h"
#import "NSObject+Unicode.h"
#import "ICN_HeadImageSelView.h"
#import "ICN_birthdayPickerView.h"
#import "ICN_EducationView.h"
#import "ICN_JobConditionView.h"
#import "ICN_JobExperienceView.h"
#import "ICN_PositionDesiredView.h"
#import "ICN_LocalityView.h"
#import "ICN_DynLocationPickerView.h"
#import "HRNetworkingManager+ICN_Publication.h"
#import "HRNetworkingManager+DynFirstPager.h"
#import "ICN_Regular.h"

#import "ICN_HangYeView.h"

#import "ICN_SetUserInfoModel.h"
#import "ICN_LocationModel.h"
#import "ICN_MyWorkPositionModel.h"


#ifdef DEBUG
#   define WKLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define WKLog(...)
#endif
@interface ICN_SetUserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate, STPhotoKitDelegate,birthdayDelegate,EducationDelegate,JobExperienceDelegate, PositionDesiredDelegate, UITextFieldDelegate,DynLocationPickerViewDelegate,HangYeViewDelegate>
{
    UIView *grayView;
    NSString *tempBirthdayStr;
    NSString *educationHighest;
    
    NSString *hopehangye;
    NSString *hopehangyeID;
    
    NSString *jobCondition;
    NSString *experience;
    NSString *PositionDesired;
    NSString *LocalityStr;
    ICN_NickNameTableViewCell *nameCell;
    
    UIImage *tempImage;
    
    
    ICN_NickNameTableViewCell *emailCell;
    ICN_NickNameTableViewCell *phoneCell;
    ICN_UserSignTableViewCell *personalizeSignatureCell;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) ICN_HeadImageTableViewCell *headImageCell;
@property (strong, nonatomic) ICN_HeadImageSelView *changeHeadImageView;

@property (strong, nonatomic) ICN_birthdayPickerView *birthdayView;
@property (strong, nonatomic) ICN_EducationView *educationView;
@property (strong, nonatomic) ICN_HangYeView *hangyeView;
@property (strong, nonatomic) ICN_JobConditionView *jobView;
@property (strong, nonatomic) ICN_JobExperienceView *experienceView;
@property (strong, nonatomic) ICN_PositionDesiredView *PositionDesiredView;
@property (strong, nonatomic) ICN_LocalityView *LocalityView;

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableDictionary *dict;
@property (nonatomic, strong) NSMutableArray *industryIdArrary;



@property (nonatomic, strong) ICN_DynLocationPickerView *citySelectedView;

@property (nonatomic , strong)NSMutableArray <ICN_LocationModel *>* locationModelsArr;


@end

@implementation ICN_SetUserInfoViewController
#pragma mark - ---------- 懒加载 ----------

- (NSMutableArray<ICN_LocationModel *> *)locationModelsArr{
    if (_locationModelsArr == nil) {
        _locationModelsArr = [NSMutableArray array];
    }
    return _locationModelsArr;
}

- (ICN_HeadImageSelView *)changeHeadImageView {
    if (!_changeHeadImageView)
    {
        _changeHeadImageView = [[NSBundle mainBundle] loadNibNamed:@"ICN_HeadImageSelView" owner:self options:nil].lastObject;
    }
    return _changeHeadImageView;
}

#pragma mark - ---------- 生命周期 ----------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (_isFirst) {
        self.navigationItem.leftBarButtonItem = nil;
        self.navigationItem.hidesBackButton = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configData];
    [self configUI];
    [self regeditCell];
    [self configDates];
    
    NSLog(@"%d",__LINE__);
    
    self.dict = [[NSMutableDictionary alloc] init];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}


- (void) configDates{
    //所在地
    [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
        if (modelsArr.firstObject.code == 0) {
            [self.locationModelsArr addObjectsFromArray:modelsArr];
        }
        
    } Failure:^(NSDictionary *errorInfo) {
    }];
    
    //期望职位
    //最高学历
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - ---------- Section的数量 ----------
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

#pragma mark - ---------- 每个Section的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 5) {
        return 10;
    }
    else {
        return 0;
    }
}

#pragma mark - ---------- Cell的数量 ----------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3) {
        return 7;
    }
    else if (section == 4) {
        return 2;
    }
    else {
        return 1;
    }
}

#pragma mark - ---------- 每个Cell的高度 ----------
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 70;
    }
    else if (indexPath.section == 5) {
        return 110;
    }
    else {
        return 44;
    }
}

#pragma mark - ---------- 每个Cell的内容 ----------
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    NSArray *sectionThreeArr = @[@"出生年月",@"最高学历",@"所属行业",@"工作情况",@"工作经验",@"期望职位",@"所在地"];
    
    
    if (indexPath.section == 0) {
        self.headImageCell = [self.tableView
                              dequeueReusableCellWithIdentifier:@"ICN_HeadImageTableViewCell"
                              forIndexPath:indexPath];
        self.headImageCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        self.headImageCell.model = model;
        
        if (tempImage) {
            self.headImageCell.headImageView.image = tempImage;
        } else {
            [self.headImageCell.headImageView sd_setImageWithURL:[NSURL URLWithString:ICN_IMG(model.memberLogo)] placeholderImage:[UIImage imageNamed:@"占位图"]];
        }
        self.headImageCell.headImageView.layer.masksToBounds = YES;
        self.headImageCell.headImageView.layer.cornerRadius = self.headImageCell.headImageView.bounds.size.width*0.5;
        
        
        return self.headImageCell;
    }
    else if (indexPath.section == 1) {
        nameCell = [self.tableView
                    dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                    forIndexPath:indexPath];
        if (nameCell.rightTextField.text.length == 0) {
            ICN_SetUserInfoModel *model = self.dataArr.firstObject;
            nameCell.rightTextField.text = model.memberNick;
        }
        nameCell.leftLabel.text = @"姓名";
        
        
        return nameCell;
        
    }
    else if (indexPath.section == 2) {
        ICN_GanderTableViewCell *cell = [self.tableView
                                         dequeueReusableCellWithIdentifier:@"ICN_GanderTableViewCell"
                                         forIndexPath:indexPath];
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        
        if ([model.memberGender isEqualToString:@"1"]) {
            cell.manButton.selected = YES;
            cell.womenButton.selected = NO;
        } else {
            cell.womenButton.selected = YES;
            cell.manButton.selected = NO;
        }
        
        
        if ([[USERDEFAULT valueForKey:@"memberGender"] isEqualToString:@"1"]) {
            
            cell.manButton.selected = YES;
            cell.womenButton.selected = NO;
        } else if ([[USERDEFAULT valueForKey:@"memberGender"]  isEqualToString:@"2"]) {
            
            cell.womenButton.selected = YES;
            cell.manButton.selected = NO;
        }
        
        
        return cell;
        
    }
    else  if (indexPath.section == 3) {
        ICN_NoImageTableCell *cell = [self.tableView
                                      dequeueReusableCellWithIdentifier:@"ICN_NoImageTableCell"
                                      forIndexPath:indexPath];
        cell.leftLabel.text = sectionThreeArr[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        cell.model = model;
        if (indexPath.row == 0) {
            
            cell.rightLabel.text = tempBirthdayStr.length == 0 ? model.memberBirthday : tempBirthdayStr;
        }
        if (indexPath.row == 1) {
            cell.rightLabel.text = educationHighest.length == 0 ? model.memberQualificationName : educationHighest;
        }
        if (indexPath.row == 2) {
            cell.rightLabel.text = hopehangye;
        }
        if (indexPath.row == 3) {
            if ([model.workStatus isEqualToString:@"1"]) {
                if (model.companyName == NULL || model.memberPosition == NULL) {
                    cell.rightLabel.text = @"";
                } else {
                    cell.rightLabel.text = jobCondition.length == 0 ? SF(@"%@ %@", model.companyName, model.memberPosition) : jobCondition;
                    
                }
     
            } else if ([model.workStatus isEqualToString:@"0"]) {
                if (model.companyName == NULL || model.memberPosition == NULL) {
                    cell.rightLabel.text = @"";
                } else {
                    cell.rightLabel.text = jobCondition.length == 0 ? SF(@"%@ %@", model.companyName, model.memberPosition) : jobCondition;
                }
            } else {
                cell.rightLabel.text = jobCondition.length == 0 ? @"" : jobCondition;
            }
        }
        if (indexPath.row == 4) {
            cell.rightLabel.text = experience == 0 ? model.workExperienceName : experience;
        }
        if (indexPath.row == 5) {
            cell.rightLabel.text = PositionDesired == 0 ? model.hopePositionName : PositionDesired;
        }
        if (indexPath.row == 6) {
            cell.rightLabel.text = LocalityStr == 0 ? SF(@"%@ %@", model.provinceName, model.cityName) : LocalityStr;
            if (model == nil && LocalityStr.length == 0) {
                cell.rightLabel.text = @"";
            }
        }
        return cell;
    }
    
    else  if (indexPath.section == 4) {

        if (indexPath.row == 0) {
            emailCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                                               forIndexPath:indexPath];
            emailCell.leftLabel.text = @"邮箱";
            if (emailCell.rightTextField.text.length == 0) {
                ICN_SetUserInfoModel *model = self.dataArr.firstObject;
                emailCell.rightTextField.text = model.memberEmail;
            }
            return emailCell;
        }else {
            phoneCell = [self.tableView dequeueReusableCellWithIdentifier:@"ICN_NickNameTableViewCell"
                                               forIndexPath:indexPath];
            phoneCell.leftLabel.text = @"手机";
            if (phoneCell.rightTextField.text.length == 0) {
                ICN_SetUserInfoModel *model = self.dataArr.firstObject;
                phoneCell.rightTextField.text = model.memberMobile;
            }
            
            return phoneCell;
            
        }
        
        
    }
    
    else {
        personalizeSignatureCell = [self.tableView
                                           dequeueReusableCellWithIdentifier:@"ICN_UserSignTableViewCell"
                                           forIndexPath:indexPath];
        ICN_SetUserInfoModel *model = self.dataArr.firstObject;
        personalizeSignatureCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (personalizeSignatureCell.signTextView.text.length == 0 || [personalizeSignatureCell.signTextView.text isEqualToString:@"写个让自己个性的签名吧！"]) {
            personalizeSignatureCell.signTextView.text = model.personalizeSignature;
        }
        personalizeSignatureCell.numberLabel.text = SF(@"%ld/50",personalizeSignatureCell.signTextView.text.length);
        return personalizeSignatureCell;
    }
    
}

#pragma mark - ---------- 每个Cell的点击事件 ----------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        [self editImageSelected];
    }
    else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            
            [self setUserBirthday];
        }else if (indexPath.row == 1) {
            
            [self setUserEducation];
        }else if (indexPath.row == 2){
            
            [self setUserHangYe];
            
        }else if (indexPath.row == 3){
            
            [self setUserJob];
            
        }else if (indexPath.row == 4) {
            
            [self setUserJobExperience];
        }else if (indexPath.row == 5) {
            
            [self setUserPositionDesired];
        }else if (indexPath.row == 6) {
            
            [self setUserLocality];
            
        }
    }
    
    
    
    
}
#pragma mark - --- delegate 视图委托 ---

#pragma mark - 1.STPhotoKitDelegate的委托

- (void)photoKitController:(STPhotoKitController *)photoKitController resultImage:(UIImage *)resultImage
{
    tempImage = resultImage;
    [self.tableView reloadData];
}

#pragma mark - 2.UIImagePickerController的委托

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        UIImage *imageOriginal = [info objectForKey:UIImagePickerControllerOriginalImage];
        STPhotoKitController *photoVC = [STPhotoKitController new];
        [photoVC setDelegate:self];
        [photoVC setImageOriginal:imageOriginal];
        
        [photoVC setSizeClip:CGSizeMake(self.headImageCell.headImageView.frame.size.width*2.5, self.headImageCell.headImageView.frame.size.width*2.5)];
        
        
        [self presentViewController:photoVC animated:YES completion:nil];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark - --- event response 事件相应 ---
- (void)editImageSelected
{
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
    
    [grayView addSubview:self.changeHeadImageView];
    [self.changeHeadImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(SCREEN_WIDTH, 160));
    }];
    self.changeHeadImageView.layer.cornerRadius = 8;
    [self.changeHeadImageView.takePhotoButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypeCamera];
        [grayView removeFromSuperview];
        if ([controller isAvailableCamera] && [controller isSupportTakingPhotos]) {
            [controller setDelegate:self];
            [self presentViewController:controller animated:YES completion:nil];
        }else {
            NSLog(@"%s %@", __FUNCTION__, @"相机权限受限");
        }
    }];
    [self.changeHeadImageView.localAlbumButton addAction:^(NSInteger tag) {
        UIImagePickerController *controller = [UIImagePickerController imagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [controller setDelegate:self];
        [grayView removeFromSuperview];
        if ([controller isAvailablePhotoLibrary]) {
            [self presentViewController:controller animated:YES completion:nil];
        }
    }];
    [self.changeHeadImageView.closeViewButton addAction:^(NSInteger tag) {
        [self dismissContactView];
    }];
}

- (void)dismissContactView {
    [grayView removeFromSuperview];
}

- (void)configUI {
    self.navigationItem.title = @"基本信息";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(saveAction)];
    _tableView=[[UITableView alloc]init];
    _tableView.frame=CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64);
    _tableView.delegate=self;
    _tableView.dataSource=self;
    [self.view addSubview:_tableView];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = RGB(235, 236, 237);
}

- (void)setGrayView {
    grayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    grayView.backgroundColor = RGBColor(0, 0, 0, 0.2);
    [[UIApplication sharedApplication].keyWindow addSubview:grayView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissContactView)];
    [grayView addGestureRecognizer:tapGesture];
}

#pragma mark - ---------- 生日选择 ----------
- (void)setUserBirthday {
    [self setGrayView];
    _birthdayView = XIB(ICN_birthdayPickerView);
    _birthdayView.delegate = self;
    self.birthdayView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.birthdayView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.birthdayView];
    [self.birthdayView.saveBirthdayButton addTarget:self action:@selector(birthdayChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)birthdayChoose {
    //    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:0 inSection:3];//获取cell的位置
    //    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    //    if (tempBirthdayStr == NULL) {
    //        tempBirthdayStr = @"1990年1月";
    //    }
    //    cell.rightLabel.text = tempBirthdayStr;
    if (tempBirthdayStr == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择生日"];
    }else {
        [self.dict setObject:tempBirthdayStr forKey:@"memberBirthday"];
    }
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getBirthdayYear:(NSString *)year andMonth:(NSString *)month {
    if (year == NULL) {
        year = @"1990年";
    }
    if (month == NULL) {
        month = @"1月";
    }
    tempBirthdayStr = [NSString stringWithFormat:@"%@%@",year,month];
}
#pragma mark - ---------- 学历选择 ----------
- (void)setUserEducation{
    
    [self setGrayView];
    self.educationView = XIB(ICN_EducationView);
    self.educationView.delegate = self;
    self.educationView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.educationView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.educationView];
    [self.educationView.confirmButton addTarget:self action:@selector(educationChoose) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
- (void)educationChoose {
    if (educationHighest == NULL) {
        educationHighest = @"小学";
        [self.dict setObject:@"1" forKey:@"memberQualification"];
    }
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getEducation:(NSString *)education andID:(NSString *)ID{
    educationHighest = [NSString stringWithFormat:@"%@",education];
    if (educationHighest == nil) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请选择学历"];
    }else {
        [self.dict setObject:ID forKey:@"memberQualification"];
    }
    
}
#pragma mark - ---------- 所属行业 ----------
- (void)setUserHangYe{
    
    [self setGrayView];
    self.hangyeView = XIB(ICN_HangYeView);
    self.hangyeView.delegate = self;
    self.hangyeView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.hangyeView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.hangyeView];
    [self.hangyeView.confirmButton addTarget:self action:@selector(hangYeChoose) forControlEvents:UIControlEventTouchUpInside];
  
}
- (void)hangYeChoose {
    if (hopehangye == NULL) {
        hopehangye = @"互联网/软件";
    }
    if (hopehangyeID == NULL) {
        hopehangyeID = @"59";
    }
    
    
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}

- (void)getHangYeView:(NSString *)HangYeView andID:(NSString *)ID {
    hopehangye = HangYeView;
    hopehangyeID = ID;
}

#pragma mark - ---------- 工作情况 ----------
- (void)setUserJob{
    
    [self setGrayView];
    self.jobView = XIB(ICN_JobConditionView);
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.jobView.cacelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    

    ICN_SetUserInfoModel *model = self.dataArr.firstObject;
    if (self.jobView.inWorkButton.selected == YES) {
        
        self.jobView.companyTF.text = model.companyName;
        self.jobView.positionTF.text = model.memberPosition;
    }else if (self.jobView.outOfWorkButton.selected == YES) {
    
        self.jobView.companyTF.text = model.memberSchool;
        self.jobView.positionTF.text = model.memberMajor;
        
    }
    
    [grayView addSubview:self.jobView];
    [self.jobView.confirmButton addTarget:self action:@selector(jobChoose) forControlEvents:UIControlEventTouchUpInside];
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


//当键盘出现或改变时调用
- (void)keyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度
    NSDictionary *userInfo = [aNotification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    int height = keyboardRect.size.height;
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163 - height, SCREEN_WIDTH, 163);
}

//当键退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification{
    self.jobView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
}

- (void)jobChoose{
    

    
    if (self.jobView.inWorkButton.selected == YES) {
        
        jobCondition = [NSString stringWithFormat:@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text];
        NSString *str = @"1";
 //       memberSchool ;memberMajor
        [self.dict setObject:str forKey:@"workStatus"];
        [self.dict setObject:self.jobView.companyTF.text forKey:@"companyName"];
        [self.dict setObject:self.jobView.positionTF.text forKey:@"memberPosition"];
        [grayView removeFromSuperview];
    }else if  (self.jobView.outOfWorkButton.selected == YES) {
        
        jobCondition = [NSString stringWithFormat:@"%@%@",self.jobView.companyTF.text,self.jobView.positionTF.text];
        NSString *str = @"0";
        [self.dict setObject:self.jobView.companyTF.text forKey:@"memberSchool"];
        [self.dict setObject:self.jobView.positionTF.text forKey:@"memberMajor"];

        [self.dict setObject:str forKey:@"workStatus"];
        [grayView removeFromSuperview];
    }
    
    [self.tableView reloadData];
    
    
}

#pragma mark - ---------- 工作经验 ----------
- (void)setUserJobExperience{
    [self setGrayView];
    self.experienceView = XIB(ICN_JobExperienceView);
    self.experienceView.delegate = self;
    self.experienceView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.experienceView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.experienceView];
    [self.experienceView.confirmButton addTarget:self action:@selector(JobExperienceChoose) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
- (void)JobExperienceChoose {
    //    NSIndexPath *ganderPath = [NSIndexPath indexPathForRow:3 inSection:3];//获取cell的位置
    //    ICN_NoImageTableCell *cell = [self.tableView cellForRowAtIndexPath:ganderPath];
    if (experience == NULL) {
        experience = @"在读";
    }
    
    //[self.dict setObject:cell.rightLabel.text forKey:@"workExperience"];
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}
- (void)getJobExperiencen:(NSString *)JobExperience andID:(NSString *)ID{
    
    if (experience == NULL) {
        experience = @"在读";
    }
    
    experience = [NSString stringWithFormat:@"%@",JobExperience];
    WKLog(@"%@",ID);
    [self.dict setObject:ID forKey:@"workExperience"];
    
}
#pragma mark - ---------- 期望职位 ----------
- (void)setUserPositionDesired{
    [self setGrayView];
    self.PositionDesiredView = XIB(ICN_PositionDesiredView);
    self.PositionDesiredView.delegate = self;
    self.PositionDesiredView.frame = CGRectMake(0, SCREEN_HEIGHT-163, SCREEN_WIDTH, 163);
    [self.PositionDesiredView.cancelButton addAction:^(NSInteger tag) {
        [grayView removeFromSuperview];
    }];
    [grayView addSubview:self.PositionDesiredView];
    [self.PositionDesiredView.savePositionDesiredButton addTarget:self action:@selector(PositionDesiredChoose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)PositionDesiredChoose {
    [self.tableView reloadData];
    [grayView removeFromSuperview];
}

- (void)getPositionDesired:(NSString *)industryId andpositioId:(NSString *)positioId andworkId:(NSString *)workId andID:(NSString *)ID{
    
    NSLog(@"6666666666666666666666666%@", workId);
    PositionDesired = SF(@"%@", workId);
    [self.dict setObject:ID forKey:@"hopePosition"];
    
}
#pragma mark - ---------- 所在地 ----------
- (void)setUserLocality{
    [self setGrayView];
    CGRect frame = SCREEN_BOUNDS;
    frame.size.height = 161.5;
    _citySelectedView = [ICN_DynLocationPickerView loadXibWithCurrentBound:frame];
    frame.origin.y = SCREEN_HEIGHT - frame.size.height;
    _citySelectedView.locationsArr = self.locationModelsArr;
    _citySelectedView.frame = frame;
    _citySelectedView.delegate = self;
    [grayView addSubview:_citySelectedView];
    
    if (_locationModelsArr == nil || self.locationModelsArr.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"未获取到地理信息列表"];
        [HRNetworkingManager requestDynamicLocationSuccess:^(NSArray<ICN_LocationModel *> *modelsArr) {
            if (modelsArr.firstObject.code == 0) {
                [self.locationModelsArr addObjectsFromArray:modelsArr];
            }
        } Failure:nil];
    }

    
}


//代理方法
#pragma mark --- DynLocationPickerViewDelegate ---

- (void)responseCityLocationSelectedWithSelecCityCode:(NSInteger)code Success:(BOOL)success{
    if (success) {

        // 修改当前显示的城市
        for (ICN_LocationModel *localModel in self.locationModelsArr) {
            for (ICN_CityDetialModel *detialModel in localModel.list) {
                if (detialModel.cityId.integerValue == code) {
                    LocalityStr = SF(@"%@ %@",localModel.className , detialModel.className);
                    
                    [self.dict setObject:localModel.provinceId forKey:@"province"];
                    [self.dict setObject:detialModel.cityId forKey:@"city"];
                    
                    [self.tableView reloadData];
                }
            }
        }
        
    }else{
        LocalityStr = SF(@"%@ %@",@"北京市" , @"北京市");
        
        [self.dict setObject:@"110000" forKey:@"province"];
        [self.dict setObject:@"110100" forKey:@"city"];
    }
    
    [grayView removeFromSuperview];
}


//处理数据
- (void)configData{
    
    if (!_isFirst) {
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/Member/getMemberInfo" params:nil success:^(id result) {
            _dataArr = [[NSMutableArray alloc]init];
            ICN_SetUserInfoModel *model = [[ICN_SetUserInfoModel alloc] initWithDictionary:result[@"result"] error:nil];
            [_dataArr addObject:model];
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/Api/Company/getIndustry" params:@{@"token":[USERDEFAULT valueForKey:HR_CurrentUserToken]} success:^(id result) {
                if (![result[@"result"] isEqual:[NSNull null]]) {
                    hopehangye = SF(@"%@",result[@"result"][@"classname"]);
                }
                [_tableView reloadData];
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
            }];
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }

}





- (void)regeditCell {
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_HeadImageTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_HeadImageTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_NickNameTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_NickNameTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_GanderTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_GanderTableViewCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_NoImageTableCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_NoImageTableCell"];
    
    [self.tableView registerNib:[UINib
                                 nibWithNibName:@"ICN_UserSignTableViewCell"
                                 bundle:nil]
         forCellReuseIdentifier:@"ICN_UserSignTableViewCell"];
}
//保存
- (void)saveAction{
    
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
        [self.dict setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
    } else {
        [self.dict setObject:_token forKey:@"token"];
        [[NSUserDefaults standardUserDefaults] setValue:_token forKey:HR_CurrentUserToken];
    }
    

    if (nameCell.rightTextField.text.length != 0) {
        [self.dict setValue:nameCell.rightTextField.text forKey:@"memberNick"];
    }
    if ([USERDEFAULT objectForKey:@"memberGender"] != nil) {
        [self.dict setObject:[USERDEFAULT objectForKey:@"memberGender"] forKey:@"memberGender"];
    }
    if (personalizeSignatureCell.signTextView.text.length != 0) {
        [self.dict setValue:personalizeSignatureCell.signTextView.text forKey:@"personalizeSignature"];
    }
    //判断邮箱
    if (emailCell.rightTextField.text.length != 0) {
        if ([self isValidateEmail:emailCell.rightTextField.text]) {
             [self.dict setObject:emailCell.rightTextField.text forKey:@"memberEmail"];
        } else {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的邮箱"];
            return;
        }
    }
    if (phoneCell.rightTextField.text.length != 0) {
        if ([self isValidateMobile:phoneCell.rightTextField.text]) {
            [self.dict setObject:phoneCell.rightTextField.text forKey:@"memberMobile"];
        }else {
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写正确的手机号"];
            return;
        }
    }
    
    ICN_SetUserInfoModel *model = self.dataArr.firstObject;
    
    if (_isFirst && nameCell.rightTextField.text.length > 0 && jobCondition.length > 0 && hopehangye.length > 0 && PositionDesired.length > 0 && experience.length > 0 && (jobCondition.length > 0 || model.companyName.length >0)) {
        [HRNetworkingManager uploadImagesWithImageArr:@[self.headImageCell.headImageView.image] Success:^(ICN_CommonImagesLoadModel *model) {
            WKLog(@"%@",model.result);
            
            
            NSString *photeStr = model.result[@"fileName"];
            [self.dict setObject:photeStr forKey:@"memberLogo"];
            
            
            WKLog(@"%@",self.dict);
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySaveTheMessage params:self.dict success:^(id result) {
                
                //如果填写了所属行业，则多保存一个，否则不保存所属行业
                if (hopehangyeID.length != 0) {
                    
                    NSMutableDictionary *hangyeDic = [NSMutableDictionary dictionary];
                    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                        [hangyeDic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
                    } else {
                        [hangyeDic setObject:_token forKey:@"token"];
                    }
                    
                    [hangyeDic setObject:hopehangyeID forKey:@"id"];
                    
                    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/Api/Company/setIndustry" params:hangyeDic success:^(id result) {
                        
                        if (_isFirst) {
#warning 这里一共四个导航栏，具体是哪个跳转还需要在测试，目前是全部跳转过去
                            [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            [self presentViewController:barController animated:YES completion:nil];
                            
                            
                            
                            
                        } else {
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                [self.navigationController popViewControllerAnimated:YES];
                            });
                        }
                        
                        
                    } failure:^(NSDictionary *errorInfo) {
                        NSLog(@"%@",errorInfo);
                    }];
                    
                    
                } else {
                    if (_isFirst) {
                        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                    } else {
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                        dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                        dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }
                }
                
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
                
            }];
            
        } Failure:^(NSDictionary *errorInfo) {
            
        }];
        
    }else if (!_isFirst && (jobCondition.length > 0 || model.companyName.length >0)) {
        [HRNetworkingManager uploadImagesWithImageArr:@[self.headImageCell.headImageView.image] Success:^(ICN_CommonImagesLoadModel *model) {
            WKLog(@"%@",model.result);
            ///
            NSString *photeStr = model.result[@"fileName"];
            [self.dict setObject:photeStr forKey:@"memberLogo"];
            
            
            WKLog(@"%@",self.dict);
            [[[HRNetworkingManager alloc] init] POST_PATH:PATH_MySaveTheMessage params:self.dict success:^(id result) {
                
                //如果填写了所属行业，则多保存一个，否则不保存所属行业
                if (hopehangyeID.length != 0) {
                    
                    NSMutableDictionary *hangyeDic = [NSMutableDictionary dictionary];
                    if ([USERDEFAULT valueForKey:HR_CurrentUserToken]) {
                        [hangyeDic setObject:[USERDEFAULT valueForKey:HR_CurrentUserToken] forKey:@"token"];
                    } else {
                        [hangyeDic setObject:_token forKey:@"token"];
                    }
                    
                    [hangyeDic setObject:hopehangyeID forKey:@"id"];
                    
                    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/Api/Company/setIndustry" params:hangyeDic success:^(id result) {
                        
                        if (_isFirst) {
                            [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                            BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                            [self presentViewController:barController animated:YES completion:nil];
                        } else {
                            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                            dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                            dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                                [self.navigationController popViewControllerAnimated:YES];
                            });
                        }
                        
                        
                    } failure:^(NSDictionary *errorInfo) {
                        NSLog(@"%@",errorInfo);
                    }];
                    
                    
                } else {
                    if (_isFirst) {
                        [USERDEFAULT setValue:[NSNumber numberWithInteger:HR_ICNUser] forKey:HR_UserTypeKEY];
                        BaseTabBarController *barController = [[BaseTabBarController alloc] init];
                        [self presentViewController:barController animated:YES completion:nil];
                    } else {
                        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"保存成功"];
                        dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC));
                        dispatch_after(delayTime, dispatch_get_main_queue(), ^{
                            [self.navigationController popViewControllerAnimated:YES];
                        });
                    }
                }
                
            } failure:^(NSDictionary *errorInfo) {
                NSLog(@"%@",errorInfo);
                
            }];
            
        } Failure:^(NSDictionary *errorInfo) {
            
        }];

    }else {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请填写完整的信息"];
    }
    
    
    
    

    
}

//手机号码的正则表达式
- (BOOL)isValidateMobile:(NSString *)mobile{
    //手机号以13、15、18开头，八个\d数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

//邮箱地址的正则表达式
- (BOOL)isValidateEmail:(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


@end
