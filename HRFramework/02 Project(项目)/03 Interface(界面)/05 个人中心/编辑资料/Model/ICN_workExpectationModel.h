//
//  ICN_workExpectationModel.h
//  ICan
//
//  Created by apple on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"
@protocol expectationModel <NSObject>
@end

@interface expectationModel : BaseOptionalModel
@property (strong,nonatomic)NSString *className;
@property (strong,nonatomic)NSString * ID;

@end
@protocol workExpectationModel <NSObject>
@end

@interface workExpectationModel : BaseOptionalModel
@property (strong,nonatomic)NSString *className;
@property (strong,nonatomic)NSString * ID;
@property (strong,nonatomic)NSArray<expectationModel> *list;
@end
@interface ICN_workExpectationModel : BaseOptionalModel
@property (strong,nonatomic)NSString *className;
@property (strong,nonatomic)NSString * ID;
@property (strong,nonatomic)NSArray<workExpectationModel> * list;
@end
