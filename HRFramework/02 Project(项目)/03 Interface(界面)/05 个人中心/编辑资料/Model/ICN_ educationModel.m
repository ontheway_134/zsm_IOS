//
//  ICN_ educationModel.m
//  ICan
//
//  Created by apple on 2017/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_ educationModel.h"

@implementation ICN__educationModel
// 用于完成属性与获取的key映射的方法
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id" : @"ID"} ];
}

@end
