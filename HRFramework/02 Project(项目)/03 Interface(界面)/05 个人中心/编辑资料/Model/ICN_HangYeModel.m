//
//  ICN_HangYeModel.m
//  ICan
//
//  Created by apple on 2017/2/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_HangYeModel.h"

@implementation ICN_HangYeModel
+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{@"ID" : @"id"} ];
}

@end
