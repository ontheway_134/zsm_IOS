//
//  ICN_MyEducationChoiceModel.h
//  ICan
//
//  Created by apple on 2017/1/17.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseOptionalModel.h"

@interface ICN_MyEducationChoiceModel : BaseOptionalModel
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSArray *modelArr;

@end
