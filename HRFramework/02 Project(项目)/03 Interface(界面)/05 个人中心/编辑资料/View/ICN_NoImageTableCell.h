//
//  UBI_NoImageTableCell.h
//  UbiTalk
//
//  Created by Lym on 2016/11/30.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ICN_SetUserInfoModel;
@class ICN_EditTrainAndExModel;
@class ICN_EducationAndExperienceModel;
@class ICN_TrainAndExperienceModel;
@interface ICN_NoImageTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (nonatomic, strong) ICN_SetUserInfoModel *model;
@property (nonatomic, strong) ICN_EditTrainAndExModel *editTrainModel;
@property (nonatomic, strong) ICN_TrainAndExperienceModel *trainModel;
@property (nonatomic, strong) ICN_EducationAndExperienceModel *modelAndEx;


@end
