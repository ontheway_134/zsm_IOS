//
//  ICN_GanderTableViewCell.m
//  ICan
//
//  Created by Lym on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_GanderTableViewCell.h"
#import "ICN_SetUserInfoModel.h"
@implementation ICN_GanderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (IBAction)userGanderButtonClick:(UIButton *)sender {
    if (sender.tag == 100) {
        self.manButton.selected = NO;
         [USERDEFAULT setObject:@"2" forKey:@"memberGender"];
    }
    else {
        self.womenButton.selected = NO;
         [USERDEFAULT setObject:@"1" forKey:@"memberGender"];
    }
    sender.selected = !sender.selected;
}

- (void)setModelOfGander:(ICN_SetUserInfoModel *)modelOfGander{

    if ([self.manLabel.text isEqualToString:modelOfGander.memberGender]) {
        self.manButton.selected = YES;
        [USERDEFAULT setObject:@"1" forKey:@"memberGender"];
        
    }else if ([self.womenLabel.text isEqualToString:modelOfGander.memberGender]) {
         [USERDEFAULT setObject:@"2" forKey:@"memberGender"];
        self.womenButton.selected = YES;
    }

}

@end
