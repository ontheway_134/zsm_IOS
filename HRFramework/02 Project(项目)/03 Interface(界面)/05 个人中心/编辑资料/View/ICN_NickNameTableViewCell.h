//
//  UBI_NickNameTableViewCell.h
//  UbiTalk
//
//  Created by Lym on 2016/11/30.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ICN_NickNameTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UITextField *rightTextField;

@end
