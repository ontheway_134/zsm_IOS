//
//  ICN_PositionDesiredView.h
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ICN_MyWorkPositionModel;

@protocol PositionDesiredDelegate <NSObject>

- (void)getPositionDesired:(NSString *)industryId andpositioId:(NSString *)positioId andworkId:(NSString *)workId andID:(NSString *)ID;

@end
@interface ICN_PositionDesiredView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>





@property (weak, nonatomic) IBOutlet UIButton *savePositionDesiredButton;
@property (weak, nonatomic) IBOutlet UIPickerView *PositionDesiredPickerView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (strong, nonatomic) NSArray *industryIdArr;
@property (strong, nonatomic) NSArray *positioIdArr;
@property (nonatomic, strong) NSArray *workIdArr;


@property (nonatomic, copy) NSString *industryId;//行业
@property (nonatomic, copy) NSString *positioId;//职位
@property (nonatomic, copy) NSString *workId;
@property (nonatomic, copy) NSString *ymId;
@property (nonatomic, strong) ICN_MyWorkPositionModel*model;

@property (weak, nonatomic) id<PositionDesiredDelegate> delegate;


@end
