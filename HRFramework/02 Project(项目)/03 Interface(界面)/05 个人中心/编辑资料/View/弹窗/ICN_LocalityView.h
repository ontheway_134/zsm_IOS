//
//  ICN_LocalityView.h
//  ICan
//
//  Created by apple on 2016/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol LocalityDelegate <NSObject>

- (void)getLocality:(NSString *)province andcity:(NSString *)city;

@end

@interface ICN_LocalityView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *saveLocalityButton;
@property (weak, nonatomic) IBOutlet UIPickerView *LocalityPickerView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (strong, nonatomic) NSArray *provinceArr;
@property (strong, nonatomic) NSArray *cityArr;

@property (copy, nonatomic) NSString *provinceStr;
@property (copy, nonatomic) NSString *cityStr;

@property (weak, nonatomic) id<LocalityDelegate> delegate;



@end
