//
//  ICN_JobExperienceView.h
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol JobExperienceDelegate <NSObject>

- (void)getJobExperiencen:(NSString *)JobExperience andID:(NSString *)ID;

@end
@interface ICN_JobExperienceView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;

@property (nonatomic, strong) NSMutableArray *arrayOfJobExperience;
@property (copy, nonatomic) NSString *JobExperienceArr;
@property (weak, nonatomic) id<JobExperienceDelegate> delegate;


@end
