//
//  ICN_JobConditionView.m
//  ICan
//
//  Created by apple on 2016/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_JobConditionView.h"

@implementation ICN_JobConditionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

//- (void)getJobCondition:(NSString *)company andposition:(NSString *)position{
//
//    self.companyTF.text = company;
//    self.positionTF.text = position;
//
//}

- (IBAction)userWork:(UIButton *)sender {
    
    if (sender.tag == 1) {
        self.outOfWorkButton.selected = NO;
        self.companyTF.placeholder = @"请输入公司名";
        self.positionTF.placeholder = @"请输入所处职位";
    } else {
        self.inWorkButton.selected = NO;
        self.companyTF.placeholder = @"请输入学校名称";
        self.positionTF.placeholder = @"请输入所学专业";
        
    }

    sender.selected = !sender.selected;

}
@end
