//
//  ICN_HangYeView.h
//  ICan
//
//  Created by apple on 2017/2/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HangYeViewDelegate <NSObject>

- (void)getHangYeView:(NSString *)HangYeView andID:(NSString *)ID;

@end
@interface ICN_HangYeView : UIView<UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickView;


@property (nonatomic, strong) NSMutableArray *arrayOfHangYeArr;
@property (copy, nonatomic) NSString *HangYeArr;
@property (weak, nonatomic) id<HangYeViewDelegate> delegate;



@end
