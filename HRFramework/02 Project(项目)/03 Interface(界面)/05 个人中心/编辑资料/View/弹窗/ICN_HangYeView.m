//
//  ICN_HangYeView.m
//  ICan
//
//  Created by apple on 2017/2/7.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_HangYeView.h"
#import "ICN_HangYeModel.h"

@interface ICN_HangYeView ()
@property (nonatomic,strong)NSMutableArray *arr1;
@end

@implementation ICN_HangYeView

- (void)awakeFromNib{

    [super awakeFromNib];
    self.pickView.delegate = self;
    self.pickView.dataSource = self;
    self.arrayOfHangYeArr = [NSMutableArray array];
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Enterprise/EnterprisePosition/doPositionLinkage" params:@{} success:^(id result) {
        
        NSLog(@"Wwwwwwwwww%@", result);
        self.arr1 = [NSMutableArray array];
        NSArray *arr = result[@"result"];
        [arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            ICN_HangYeModel *model =  [[ICN_HangYeModel alloc]initWithDictionary:obj error:nil];
            [self.arrayOfHangYeArr addObject:model];
            [self.pickView reloadAllComponents];
        }];
        
        
        
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

}
#pragma mark --- 与DataSource有关的代理方法
//返回列数（必须实现）
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

//返回每列里边的行数（必须实现）
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.arrayOfHangYeArr.count;
}

#pragma mark --- 与处理有关的代理方法
//设置组件的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return SCREEN_WIDTH/3;
    
}
//设置组件中每行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
//设置组件中每行的标题row:行
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    ICN_HangYeModel *model = self.arrayOfHangYeArr[row];
    
    return model.className;
    
    
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
        //adjustsFontSizeToFitWidth property to YES
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        pickerLabel.font = [UIFont systemFontOfSize:14];
        
    }
    // Fill the label text here
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}
//选中行的事件处理
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    ICN_HangYeModel *model = self.arrayOfHangYeArr[row];
    self.HangYeArr = model.className;
    
//    [self.delegate getJobExperiencen:self.JobExperienceArr andID:model.ID];
    [self.delegate getHangYeView:self.HangYeArr andID:model.ID];
    
}

@end
