//
//  UBI_HeadImageTableViewCell.h
//  UbiTalk
//
//  Created by Lym on 2016/11/30.
//  Copyright © 2016年 Risenb. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_SetUserInfoModel;
@interface ICN_HeadImageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (nonatomic, strong) ICN_SetUserInfoModel *model;


@end
