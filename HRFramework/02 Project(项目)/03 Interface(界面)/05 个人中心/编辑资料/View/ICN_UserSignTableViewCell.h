//
//  ICN_UserSignTableViewCell.h
//  ICan
//
//  Created by Lym on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_SetUserInfoModel;

@interface ICN_UserSignTableViewCell : UITableViewCell<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@property (weak, nonatomic) IBOutlet UITextView *signTextView;


@property (nonatomic, strong) ICN_SetUserInfoModel *model;

@end
