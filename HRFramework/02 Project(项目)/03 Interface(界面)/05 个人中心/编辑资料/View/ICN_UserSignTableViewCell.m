//
//  ICN_UserSignTableViewCell.m
//  ICan
//
//  Created by Lym on 2016/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_UserSignTableViewCell.h"

#import "ICN_SetUserInfoModel.h"
@implementation ICN_UserSignTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.signTextView.delegate = self;
    self.numberLabel.text = SF(@"%ld/100",self.signTextView.text.length);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.location>=50)
    {
        return  NO;
    }
    else
    {
        return YES;
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    //该判断用于联想输入
    if (textView.text.length >= 50)
    {
        textView.text = [textView.text substringToIndex:50];
        
    }
    NSString  * nsTextContent=textView.text;
    long existTextNum=[nsTextContent length];
    self.numberLabel.text=[NSString stringWithFormat:@"%ld/50",existTextNum];
}

- (void)setModel:(ICN_SetUserInfoModel *)model{

    self.signTextView.text = model.personalizeSignature;

}
@end
