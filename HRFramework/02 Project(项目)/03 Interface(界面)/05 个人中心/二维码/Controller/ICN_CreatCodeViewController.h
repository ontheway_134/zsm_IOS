//
//  ICN_CreatCodeViewController.h
//  ICan
//
//  Created by shilei on 17/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "BaseViewController.h"

@interface ICN_CreatCodeViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *codeImageView;


@end
