//
//  ICN_ActivityMainCell.m
//  ICan
//
//  Created by 那风__ on 16/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ActivityMainCell.h"
#import "ICN_ActivityMainModel.h"

@implementation ICN_ActivityMainCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 200)];
        
        UIView *view=[[UIView alloc]init];
        [self.contentView addSubview:view];

        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self.contentView.top).offset(0);
            make.left.mas_equalTo(self.contentView.left).offset(0);
            make.right.mas_equalTo(self.contentView.right).offset(0);
            make.height.mas_equalTo(100);
        }];
        
        view.backgroundColor = [UIColor whiteColor];
        
        
        
        _photoImage = [[UIImageView alloc]init];
        [view addSubview:_photoImage];
        [_photoImage mas_makeConstraints:^(MASConstraintMaker *make) {
           // make.size.mas_equalTo(CGSizeMake(100, 70));
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(70);
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(10);
            
        }];
        
       

        
        _titleLabel = [[UILabel alloc]init];
        [view addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(150, 14));
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(_photoImage.mas_right).offset(10);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:14];
        
        _rmbLabel = [[UILabel alloc]init];
        [view addSubview:_rmbLabel];

        [_rmbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.right.mas_equalTo(view.mas_right).offset(-10);
            make.height.mas_equalTo(14);
            
        }];
        _rmbLabel.textColor = RGB0X(0Xff561b);
        _rmbLabel.font = [UIFont systemFontOfSize:14];
        
        _contentLabel = [[UILabel alloc]init];
        [view addSubview:_contentLabel];
        [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //make.size.mas_equalTo(CGSizeMake(200, 24));
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
            make.left.mas_equalTo(_photoImage.mas_right).offset(10);
            make.right.mas_equalTo(view.mas_right).offset(-10);
           // make.bottom.mas_equalTo(_timeLabel.mas_top).offset(-10);
            
        }];
        _contentLabel.font = [UIFont systemFontOfSize:12];
        _contentLabel.textColor = RGB0X(0X666666);
        _contentLabel.numberOfLines = 2;
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.image = [UIImage imageNamed:@"报名人数.png"];
        [view addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_photoImage.mas_right).offset(10);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(48.5);
            make.height.mas_equalTo(12);
        }];
        
        _numberLabel = [[UILabel alloc]init];
        [view addSubview:_numberLabel];
        [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageV.mas_right).offset(5);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(48.5);
            make.height.mas_equalTo(12);
        }];
        _numberLabel.font = [UIFont systemFontOfSize:12];
        _numberLabel.textColor = RGB0X(0X666666);
        
        _cannumLabel = [[UILabel alloc]init];
        [view addSubview:_cannumLabel];
        [_cannumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_numberLabel.mas_right).offset(0);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(48.5);
            make.height.mas_equalTo(12);
        }];
        _cannumLabel.font = [UIFont systemFontOfSize:12];
        _cannumLabel.textColor = RGB0X(0X666666);
        
        _timeLabel = [[UILabel alloc]init];
        [view addSubview:_timeLabel];
        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            
            make.right.mas_equalTo(view.mas_right).offset(-10);
              make.top.mas_equalTo(_titleLabel.mas_bottom).offset(50);
            make.height.mas_offset(9);
            
        }];
        _timeLabel.font = [UIFont systemFontOfSize:9];
        _timeLabel.textColor = RGB0X(0Xb6b6b6);
        
        
        
//        UIView *view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 200, self.contentView.frame.size.width, 5)];
//        view1.backgroundColor = RGB(229, 229, 229);
//        [self.contentView addSubview:view1];
        UIView *view1 = [[UIView alloc]init];
        [self.contentView addSubview:view1];
        [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(view.mas_bottom).offset(0);
            make.left.mas_equalTo(self.contentView.left).offset(0);
            make.right.mas_equalTo(self.contentView.right).offset(0);
            make.height.mas_equalTo(5);

        }];
        view1.backgroundColor = RGB(229, 229, 229);
        
    }
    
    return self;
}
-(void)setModel:(ICN_ActivityMainModel *)model{
    
    _titleLabel.text = model.title;
    [_photoImage sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    _rmbLabel.text = [NSString stringWithFormat:@"¥%@",model.price];
    if ([model.price isEqualToString:@"0"]) {
        _rmbLabel.text = [NSString stringWithFormat:@"免费"];
    }
    
    
    
    _contentLabel.text = model.include;
    _numberLabel.text = [NSString stringWithFormat:@"%@/",model.subnum];
    _cannumLabel.text = [NSString stringWithFormat:@"%@人",model.cannum];
    
    _timeLabel.text = model.createdate;
    
    
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
