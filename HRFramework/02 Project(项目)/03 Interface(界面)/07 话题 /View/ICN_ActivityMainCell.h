//
//  ICN_ActivityMainCell.h
//  ICan
//
//  Created by 那风__ on 16/12/5.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_ActivityMainModel;
@interface ICN_ActivityMainCell : UITableViewCell
@property(nonatomic)UIImageView *photoImage;
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *rmbLabel;
@property(nonatomic)UILabel *contentLabel;
@property(nonatomic)UILabel *numberLabel;
@property(nonatomic)UILabel *cannumLabel;
@property(nonatomic)UILabel *timeLabel;

@property(nonatomic)ICN_ActivityMainModel *model;
@end
