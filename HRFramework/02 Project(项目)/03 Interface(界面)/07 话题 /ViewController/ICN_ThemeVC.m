//
//  ICN_ThemeVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ThemeVC.h"
#import "ICN_ActivityMainCell.h"
#import "ICN_ActivityMainModel.h"

#import "ICN_ActivityParticularsViewController.h"
#define Width [UIScreen mainScreen].bounds.size.width
#define Height [UIScreen mainScreen].bounds.size.height

@interface ICN_ThemeVC ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic)NSInteger page;
@end

@implementation ICN_ThemeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createUI];
//    [self datas];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // [self datas];
    [self createUI];
}

-(void)datas{

    NSMutableDictionary *dic = [@{@"page":[NSString stringWithFormat:@"%ld",_page]} mutableCopy];
    if (self.memberId) {
        [dic setValue:self.memberId forKey:@"memberId"];
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php／MyPosition/Activity/ActivityListmember" params:dic success:^(id result) {
            NSLog(@"%@",result);
            _dataArr = [[NSMutableArray alloc]init];
            
            NSArray *result1 = result[@"result"];
            for (NSDictionary *dic in result1) {
                ICN_ActivityMainModel *model = [[ICN_ActivityMainModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [_dataArr addObject:model];
            }
            
            [_tableView reloadData];
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];

    }else{
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Activity/ActivityList" params:dic success:^(id result) {
            NSLog(@"%@",result);
            
            if (_page == 1) {
                [_dataArr removeAllObjects];
            }
            
            NSArray *result1 = result[@"result"];
            for (NSDictionary *dic in result1) {
                ICN_ActivityMainModel *model = [[ICN_ActivityMainModel alloc]init];
                [model setValuesForKeysWithDictionary:dic];
                [_dataArr addObject:model];
            }
            if (_page == 1) {
                [_tableView.mj_header endRefreshing];
            }
            [_tableView.mj_footer endRefreshing];
            [_tableView reloadData];
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];

    }
    
    

}
-(void)createUI{

    _dataArr = [[NSMutableArray alloc]init];
    _page = 1;
    self.title = @"活动";
    self.navigationItem.leftBarButtonItem = nil;
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Width, Height - 64 - 49) style:UITableViewStylePlain];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        _page = 1;
//        [self datas];
//    }];
//    _tableView.mj_header.
//    _tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
//        _page ++;
//        
//        [self datas];
//
//    }];
    
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        _page = 1;
        [self datas];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    _tableView.mj_header = header;
    _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        self.page+=1;
        [self datas];
    }];
    [_tableView.mj_header beginRefreshing];
    
    
    
    [_tableView registerClass:[ICN_ActivityMainCell class] forCellReuseIdentifier:@"cell"];
   

    [self.view addSubview:_tableView];


}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _dataArr.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    ICN_ActivityMainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ICN_ActivityMainModel *model = _dataArr[indexPath.row];
    cell.model = model;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 105;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //加上此句，返回时直接就是非选中状态。
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ICN_ActivityParticularsViewController *par = [[ICN_ActivityParticularsViewController alloc]init];
    //添加上这句话为了隐藏分栏
    par.hidesBottomBarWhenPushed = YES;
    ICN_ActivityMainModel *model = _dataArr[indexPath.row];
    
    par.url = model.aid;
   
    [self currentPagerJumpToPager:par];
//    [self.navigationController pushViewController:par animated:YES];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
