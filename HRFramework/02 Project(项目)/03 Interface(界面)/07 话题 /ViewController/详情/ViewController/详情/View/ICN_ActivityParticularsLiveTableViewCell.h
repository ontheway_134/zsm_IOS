//
//  ICN_ActivityParticularsLiveTableViewCell.h
//  ICan
//
//  Created by 那风__ on 16/12/26.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_ActivityParticularsPlayCellModel;
@interface ICN_ActivityParticularsLiveTableViewCell : UITableViewCell
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UIImageView *memberlogoImage;
@property(nonatomic)UILabel *nameLabel;
@property(nonatomic)UILabel *companynameLabel;
@property(nonatomic)UILabel *timeLabel;
@property(nonatomic)ICN_ActivityParticularsPlayCellModel *model;
@end
