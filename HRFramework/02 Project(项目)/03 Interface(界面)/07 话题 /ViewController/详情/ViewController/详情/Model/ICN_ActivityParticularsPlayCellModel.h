//
//  ICN_ActivityParticularsPlayCellModel.h
//  ICan
//
//  Created by 那风__ on 16/12/23.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ActivityParticularsPlayCellModel : NSObject
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *memberLogo;
@property(nonatomic)NSString *memberNick;
@property(nonatomic)NSString *companyName;
@property(nonatomic)NSString *memberPosition;
@property(nonatomic)NSString *time;
@property(nonatomic)NSString *address;
@end
