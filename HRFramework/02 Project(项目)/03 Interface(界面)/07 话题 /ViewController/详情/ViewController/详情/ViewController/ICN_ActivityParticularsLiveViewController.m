//
//  ICN_ActivityParticularsLiveViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/19.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ActivityParticularsLiveViewController.h"
#import "ICN_ActivityParticularsLiveTableViewCell.h"
#import "ICN_ActivityParticularsPlayCellModel.h"
#import "ICN_ActivityParticularsDownModel.h"
#import "ICN_ICN_ActivityParticularsPlayModel.h"

@interface ICN_ActivityParticularsLiveViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic)UIImageView *banImageView;
@property(nonatomic)UILabel *playLabel;
@property(nonatomic)UIImageView *peopleImage;
@property(nonatomic)UILabel *peopleNameLabel;
@property(nonatomic)UILabel *peopleNameNextLabel;
@property(nonatomic)UILabel *contentLabel;
@property(nonatomic)UILabel *timeLabel;

@property(nonatomic)CGRect labelHeight;
@property(nonatomic)UIView *headerView;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)NSMutableArray *topDataArr;
@end

@implementation ICN_ActivityParticularsLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     [self createNAV];
    //[self createHV];
     [self datas];
}
-(void)datas{
    
    NSDictionary *dic = @{@"includeid":[NSString stringWithFormat:@"%@",_url]};
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Activity/Lecturedetail" params:dic success:^(id result) {
        
        _dataArr = [[NSMutableArray alloc]init];
        NSDictionary *dict =result[@"result"];
        NSArray *about = dict[@"about"];
        for (NSDictionary *dic in about) {
            ICN_ActivityParticularsDownModel *model = [[ICN_ActivityParticularsDownModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        [_tableView.mj_header endRefreshing];
        [_tableView reloadData];
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _dataArr.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_ActivityParticularsLiveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ICN_ICN_ActivityParticularsPlayModel *model = _dataArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = model;
    
    return cell;
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 104;
    
}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)createHV{
    
    _headerView = [[UIView alloc]init];
    //[self.view addSubview:_headerView];
    
    _banImageView = [[UIImageView alloc]init];
    
    [_headerView addSubview:_banImageView];
    [_banImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(130);
    }];
    
    _playLabel = [[UILabel alloc]init];
    _playLabel.font = [UIFont systemFontOfSize:13];
    _playLabel.textColor = RGB0X(0X000000);
    [_headerView addSubview:_playLabel];
    [_playLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(_banImageView.mas_bottom).offset(14);
        make.height.mas_equalTo(13);
        
    }];
    
    UILabel *line1label = [[UILabel alloc]init];
    line1label.backgroundColor = RGB0X(0Xe5e5e5);
    [_headerView addSubview:line1label];
    [line1label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(_playLabel.mas_bottom).offset(14);
        
    }];
    
    _peopleImage = [[UIImageView alloc]init];
    [_headerView addSubview:_peopleImage];
    [_peopleImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(line1label.mas_bottom).offset(15);
        make.width.mas_equalTo(35);
        make.height.mas_equalTo(35);
        
    }];
    
    _peopleNameLabel = [[UILabel alloc]init];
    _peopleNameLabel.font = [UIFont systemFontOfSize:13];
    _peopleNameLabel.textColor = RGB0X(0X000000);
    [_headerView addSubview:_peopleNameLabel];
    [_peopleNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(line1label.mas_bottom).offset(15);
        make.left.mas_equalTo(_peopleImage.mas_right).offset(10);
        make.height.mas_equalTo(13);
    }];
    
    _peopleNameNextLabel = [[UILabel alloc]init];
    _peopleNameNextLabel.font = [UIFont systemFontOfSize:11];
    _peopleNameNextLabel.textColor = RGB0X(0X666666);
    [_headerView addSubview:_peopleNameNextLabel];
    [_peopleNameNextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_peopleImage.mas_right).offset(10);
        make.top.mas_equalTo(_peopleNameLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(11);
    }];
    
    UILabel *line2Label = [[UILabel alloc]init];
    line2Label.backgroundColor = RGB0X(0Xe5e5e5);
    [_headerView addSubview:line2Label];
    [line2Label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(1);
        make.top.mas_equalTo(_peopleNameNextLabel.mas_bottom).offset(17.5);
    }];
    
    UILabel *jzLabel = [[UILabel alloc]init];
    jzLabel.textColor = RGB0X(0X000000);
    jzLabel.font = [UIFont systemFontOfSize:13];
    jzLabel.text = @"直播内容";
    [_headerView addSubview:jzLabel];
    [jzLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(line2Label.mas_bottom).offset(15);
        make.height.mas_equalTo(13);
    }];
    
    _contentLabel = [[UILabel alloc]init];
    _contentLabel.textColor = RGB0X(0X333333);
    _contentLabel.text = @"";
    [_headerView addSubview:_contentLabel];
    
//    _labelHeight = [_contentLabel.text boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 20, 10000) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} context:nil];
    _contentLabel.numberOfLines = 0;
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(jzLabel.mas_bottom).offset(10);
        make.right.mas_equalTo(-10);
        make.left.mas_equalTo(10);
        
    }];
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:_contentLabel.text];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:5.0];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [_contentLabel.text length])];
    [_contentLabel setAttributedText:attributedString1];
    
//    UIImageView *regionImg = [[UIImageView alloc]init];
//    regionImg.image = [UIImage imageNamed:@"地点.png"];
//    [headerView addSubview:regionImg];
//    [regionImg mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(10);
//        make.top.mas_equalTo(_contentLabel.mas_bottom).offset(15);
//    }];
//    
//    _regionLabel = [[UILabel alloc]init];
//    _regionLabel.textColor = RGB0X(0X666666);
//    _regionLabel.font = [UIFont systemFontOfSize:11];
//    [headerView addSubview:_regionLabel];
//    [_regionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(regionImg.mas_right).offset(5);
//        make.top.mas_equalTo(_contentLabel.mas_bottom).offset(15);
//        make.height.mas_equalTo(11);
//    }];
//    
    UIImageView *timeImg = [[UIImageView alloc]init];
    timeImg.image = [UIImage imageNamed:@"时间.png"];
    [_headerView addSubview:timeImg];
    [timeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(_contentLabel.mas_bottom).offset(15);
    }];
    
    _timeLabel = [[UILabel alloc]init];
    _timeLabel.textColor = RGB0X(0X666666);
    _timeLabel.font = [UIFont systemFontOfSize:11];
    [_headerView addSubview:_timeLabel];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(timeImg.mas_right).offset(5);
        make.top.mas_equalTo(_contentLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(11);
    }];
    
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = RGB0X(0Xe5e5e5);
    [_headerView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(_timeLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(36);
        
    }];
    
    UILabel *xgLabel = [[UILabel alloc]init];
    xgLabel.textColor = RGB0X(0X333333);
    xgLabel.font = [UIFont systemFontOfSize:12];
    xgLabel.text = @"相关直播";
    [view addSubview:xgLabel];
    [xgLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10);
        make.top.mas_equalTo(15);
        make.height.mas_equalTo(12);
    }];
    
    [_headerView setFrame:CGRectMake(0, 0, self.view.frame.size.width, 355 + _labelHeight.size.height)];
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    NSLog(@"%f",_labelHeight.size.height);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView setTableHeaderView:_headerView];;
    [_tableView registerClass:[ICN_ActivityParticularsLiveTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self datas];
    }];
    [self.view addSubview:_tableView];
    
}

-(void)createNAV{
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"直播详情";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:16],
       NSForegroundColorAttributeName:RGB0X(0Xffffff)}];
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
    
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = leftItem3;
    
    
    UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(0,0, 15, 15)];
   // [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setBackgroundImage:[[UIImage imageNamed:@"分享.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:btn1];
    self.navigationItem.rightBarButtonItem  = item;
    NSDictionary *dic = @{@"includeid":[NSString stringWithFormat:@"%@",_url]};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Activity/Lecturedetail" params:dic success:^(id result) {
        NSDictionary *dict = result[@"result"];
        NSDictionary *dictTop = dict[@"lecture"];
        
        ICN_ICN_ActivityParticularsPlayModel *model = [[ICN_ICN_ActivityParticularsPlayModel alloc]init];
        [model setValuesForKeysWithDictionary:dictTop];
        //[self createHV];
        
        //        _contentLabel.text = model.content;
        //        _contentLabel.font = [UIFont systemFontOfSize:12.0];
        _labelHeight = [model.content boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 20, 10000) options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12]} context:nil];
        
        
        
        [self createHV];
        [_banImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]]];
        
        _playLabel.text = model.title;
        [_peopleImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.memberLogo]]];
        _peopleNameLabel.text = model.memberNick;
        _peopleNameNextLabel.text = [NSString stringWithFormat:@"%@ | %@",model.companyName,model.memberPosition];
        _contentLabel.text = model.content;
        _contentLabel.font = [UIFont systemFontOfSize:12.0];
        
        
        //_regionLabel.text = model.address;
        _timeLabel.text = model.time;
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
