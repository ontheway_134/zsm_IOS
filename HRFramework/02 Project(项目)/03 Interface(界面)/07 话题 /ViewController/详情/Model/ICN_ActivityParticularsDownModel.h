//
//  ICN_ActivityParticularsDownModel.h
//  ICan
//
//  Created by 那风__ on 16/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ActivityParticularsDownModel : NSObject
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *memberLogo;
//名字
@property(nonatomic)NSString *memberNick;
//公司
@property(nonatomic)NSString *companyName;
//职位
@property(nonatomic)NSString *memberPosition;
//地址
@property(nonatomic)NSString *address;
//日期
@property(nonatomic)NSString *time;

@property(nonatomic)NSString *includeid;
@property(nonatomic)NSString *activityClass;
@end
