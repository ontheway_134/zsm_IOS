//
//  ICN_ActivityParticularsDownTableViewCell.m
//  ICan
//
//  Created by 那风__ on 16/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ActivityParticularsDownTableViewCell.h"
#import "ICN_ActivityParticularsDownModel.h"
@implementation ICN_ActivityParticularsDownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _selyView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        [self.contentView addSubview:_selyView];
        
        _titleLabel = [[UILabel alloc]init];
        [_selyView addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(14);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(13);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:14];
        
        UILabel *label = [[UILabel alloc]init];
        [_selyView addSubview:label];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(9);
            make.right.mas_equalTo(self.contentView).offset(-9);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(14);
            make.height.mas_equalTo(1);
        }];
        label.backgroundColor = RGB(236, 236, 236);
        
        _memberlogoImage = [[UIImageView alloc]init];
      
        [_selyView addSubview:_memberlogoImage];
        [_memberlogoImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.width.mas_equalTo(35);
            make.height.mas_equalTo(35);
            
        }];
        _memberlogoImage.layer.cornerRadius =17.5;
        _memberlogoImage.layer.masksToBounds = YES;
        _nameLabel = [[UILabel alloc]init];
        [_selyView addSubview:_nameLabel];
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.height.mas_equalTo(13);
        }];
        _nameLabel.textColor = RGB0X(0X000000);
        _nameLabel.font = [UIFont systemFontOfSize:14];

        _companynameLabel = [[UILabel alloc]init];
        [_selyView addSubview:_companynameLabel];
        [_companynameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(label.mas_bottom).offset(15);
            make.left.mas_equalTo(_nameLabel.mas_right).offset(10);
            make.height.mas_equalTo(11);
        }];
        _companynameLabel.textColor = RGB0X(0X666666);
        _companynameLabel.font = [UIFont systemFontOfSize:11];

       
            _image2 = [[UIImageView alloc]init];
            _image2.image = [UIImage imageNamed:@"地点.png"];
            [_selyView addSubview:_image2];
            [_image2 mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
                make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            }];
            _addressLabel = [[UILabel alloc]init];
            [_selyView addSubview:_addressLabel];
            [_addressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(_image2.mas_right).offset(5);
                make.top.mas_equalTo(_companynameLabel.mas_bottom).offset(10);
                make.height.mas_equalTo(11);
            }];
                _addressLabel.textColor = RGB0X(0X666666);
                _addressLabel.font = [UIFont systemFontOfSize:11];
                _image1 =[[UIImageView alloc]init];
                _image1.image = [UIImage imageNamed:@"时间.png"];
                [_selyView addSubview:_image1];
                [_image1 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
                    make.top.mas_equalTo(_addressLabel.mas_bottom).offset(10);
                    //make.bottom.mas_equalTo(-15);
                    
                }];
                
                _timeLabel = [[UILabel alloc]init];
                [_selyView addSubview:_timeLabel];
                [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(_image1.mas_right).offset(5);
                    make.top.mas_equalTo(_addressLabel.mas_bottom).offset(10);
                    make.height.mas_equalTo(11);
                    //make.bottom.mas_equalTo(-15);
                }];
                
                
                _timeLabel.textColor = RGB0X(0X666666);
                _timeLabel.font = [UIFont systemFontOfSize:11];
                
                
                _label2 = [[UILabel alloc]init];
                [_selyView addSubview:_label2];
                [_label2 mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.top.mas_equalTo(_timeLabel.mas_bottom).offset(15);
                    make.left.mas_equalTo(0);
                    make.right.mas_equalTo(self.contentView).offset(0);
                    make.height.mas_equalTo(4);
                    //make.bottom.mas_equalTo(0);
                }];
                _label2.backgroundColor = RGB(236, 236, 236);
        
        
        }
    return self;
                 
}
-(void)setModel:(ICN_ActivityParticularsDownModel *)model{

    
    

    _titleLabel.text = model.title;
    [_memberlogoImage sd_setImageWithURL:[NSURL URLWithString:model.memberLogo]];
    _nameLabel.text = model.memberNick;
    _companynameLabel.text = [NSString stringWithFormat:@"%@ | %@",model.companyName,model.memberPosition];
    _addressLabel.text = model.address;
    _timeLabel.text = model.time;
    
    _model1 = model.activityClass;

    if ([_model1 isEqualToString:@"2"]) {
        

        [_image1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_memberlogoImage.mas_right).offset(10);
            make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            //make.bottom.mas_equalTo(-15);
            
        }];
        

        [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(_image1.mas_right).offset(5);
            make.top.mas_equalTo(_nameLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(11);
           
        }];
        
        
        _timeLabel.textColor = RGB0X(0X666666);
        _timeLabel.font = [UIFont systemFontOfSize:11];
        
        

        [_label2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_timeLabel.mas_bottom).offset(15);
            make.left.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.height.mas_equalTo(4);
          
        }];
        _label2.backgroundColor = RGB(236, 236, 236);
        
        [_image2 removeFromSuperview];
        [_addressLabel removeFromSuperview];

    }

    
   

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
