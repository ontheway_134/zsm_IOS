//
//  ICN_ActivityParticularsDownTableViewCell.h
//  ICan
//
//  Created by 那风__ on 16/12/7.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_ActivityParticularsDownModel;

@interface ICN_ActivityParticularsDownTableViewCell : UITableViewCell
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UIImageView *memberlogoImage;
@property(nonatomic)UILabel *nameLabel;
@property(nonatomic)UILabel *companynameLabel;
@property(nonatomic)UILabel *addressLabel;
@property(nonatomic)UILabel *timeLabel;
@property(nonatomic)ICN_ActivityParticularsDownModel *model;
@property(nonatomic)UIImageView *image2;
@property(nonatomic)NSString *model1;
@property(nonatomic)UIView *selyView;
@property(nonatomic)UIImageView *image1;
@property(nonatomic)UILabel *label2;
@end
