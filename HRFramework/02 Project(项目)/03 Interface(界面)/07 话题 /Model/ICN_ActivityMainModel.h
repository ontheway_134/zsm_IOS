//
//  ICN_ActivityMainModel.h
//  ICan
//
//  Created by 那风__ on 16/12/6.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_ActivityMainModel : NSObject
@property(nonatomic)NSString *title;
@property(nonatomic)NSString *pic;
//已报名人数
@property(nonatomic)NSString *subnum;
//可报名人数
@property(nonatomic)NSString *cannum;
//时间
@property(nonatomic)NSString *createdate;
//活动价格
@property(nonatomic)NSString *price;
//包含内容
@property(nonatomic)NSString *include;

@property(nonatomic)NSString *aid;
@end
