//
//  ICN_PositionNextModel.h
//  ICan
//
//  Created by 那风__ on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_PositionNextModel : NSObject
@property(nonatomic)NSString *memberLogo;
@property(nonatomic)NSString *positionTitle;

@property(nonatomic)NSString *salary;
@property(nonatomic)NSString *companyName;
@property(nonatomic)NSString *city;
@property(nonatomic)NSString *qualification;

@property(nonatomic)NSString *citycode;
//是否隐藏
@property(nonatomic)NSString *ishot;
//全部id
@property(nonatomic)NSString *classSmallId;
@property(nonatomic)NSString *positionid;
//搜索内容
@end
