//
//  ICN_ProfessionSearchViewController.h
//  ICan
//
//  Created by 那风__ on 17/1/3.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_ProfessionSearchViewController : BaseViewController
/*判断当前视图*/
@property (strong,nonatomic)NSString * typeStr;
@property (nonatomic , copy)NSString *searchContent;
@end
