//
//  ICN_PositionNextOneViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionNextOneViewController.h"
#import "ICN_PositionNextOneModel.h"
#import "ICN_reportReportViewController.h"
#import "BaseOptionalModel.h"
#import "ICN_MyResumeViewController.h"
#import "ICN_DynWarnView.h"
#import "ICN_ShareManager.h"
#import "ICN_ApplyModel.h"
#import "ICN_StartViewController.h"
#import "ICN_CommonShareModel.h"

@interface ICN_PositionNextOneViewController ()<ICN_DynWarnViewDelegate,UIGestureRecognizerDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIView *contentView;
@property(nonatomic)UIImageView *topImageView;
@property(nonatomic)UILabel *topTitleLabel;
@property(nonatomic)UILabel *topRmbLabel;
@property(nonatomic)UILabel *topTitleNetLabel;
@property(nonatomic)UILabel *label1;
@property(nonatomic)UILabel *label2;
@property(nonatomic)UILabel *label3;
@property(nonatomic)UILabel *label4;
@property(nonatomic)UILabel *label5;
@property(nonatomic)UILabel *topBigLabel;


@property(nonatomic)UIImageView *downImageView;
@property(nonatomic)UIButton *lookBtn;
@property(nonatomic)UILabel *downTitleLabel;
@property(nonatomic)UILabel *downTitleNextLabel;
@property(nonatomic)UILabel *downyidongLabel;
@property(nonatomic)UILabel *downBigLabel;


@property(nonatomic)UIButton *shoucangBtn;
@property(nonatomic)UIButton *jianliBtn;

@property(nonatomic)NSString *iscollect;
@property(nonatomic)NSString *isfollow;

@property(nonatomic)NSMutableArray *dataArr;
@property(nonatomic)NSInteger type;
@property(nonatomic)NSString *memberId;

@property(nonatomic)NSString *issend;   //是否投递  0可投递 1不可投递
@property(nonatomic)NSString *iscompleteness;  // 简历完成度 0大于70%  1没有
//分享
@property(nonatomic)ICN_DynWarnView *replayView;

@property(nonatomic)UIView *winView;
@property(nonatomic)UIView *shareView;
@property (nonatomic , strong)ICN_PositionNextOneModel *shareModel;

@end

@implementation ICN_PositionNextOneViewController
-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    self.hiddenDefaultNavBar = NO;
    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    if (![self getCurrentUserLoginStatus]) {
        ICN_StartViewController *vc = [[ICN_StartViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self datas];
        
        [self createUI];
        [self neirongUI];    }

    
    //NSLog(@"%@",_url);
   
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (_replayView) {
        [_replayView removeFromSuperview];
        _replayView = nil;
    }
}
-(void)onClick:(UIButton *)btn{

    //[[UIApplication sharedApplication].keyWindow addSubview:self.replayView];
    btn.selected = !btn.selected;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    view.tag = 111;
    _winView = view;
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];
    
    UIView *shareView = [[UIView alloc]init];
    shareView.backgroundColor = [UIColor whiteColor];
    _shareView = shareView;
    [currentWindow addSubview:shareView];
    [shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(150);
    }];
    
    UIButton *wxBtn = [[UIButton alloc]init];
    [wxBtn setImage:[UIImage imageNamed:@"微信.png"] forState:UIControlStateNormal];
    
    [shareView addSubview:wxBtn];
    [wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(10);
        
    }];
    
    UILabel *wxLabel = [[UILabel alloc]init];
    wxLabel.text = @"微信";
    wxLabel.textColor = RGB0X(0x333333);
    wxLabel.font = [UIFont systemFontOfSize:12];
    [wxBtn addTarget:self action:@selector(wxBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:wxLabel];
    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wxBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
     
        make.left.mas_equalTo(17);
    }];
    
    UIButton *wbBtn = [[UIButton alloc]init];
    [wbBtn setImage:[UIImage imageNamed:@"微博.png"] forState:UIControlStateNormal];
    [wbBtn addTarget:self action:@selector(wbBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:wbBtn];
    [wbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(wxBtn.mas_right).offset(35);
        
    }];

    UILabel *wbLabel = [[UILabel alloc]init];
    wbLabel.text = @"微博";
    wbLabel.textColor = RGB0X(0x333333);
    wbLabel.font = [UIFont systemFontOfSize:12];
    [shareView addSubview:wbLabel];
    [wbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wbBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
        make.left.mas_equalTo(92);
    }];

    
    UIButton *qqBtn = [[UIButton alloc]init];
    [qqBtn setImage:[UIImage imageNamed:@"QQ.png"] forState:UIControlStateNormal];
    [qqBtn addTarget:self action:@selector(qqBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:qqBtn];
    [qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(wbBtn.mas_right).offset(35);
        
    }];
    
    UILabel *qqLabel = [[UILabel alloc]init];
    qqLabel.text = @"QQ";
    qqLabel.textColor = RGB0X(0x333333);
    qqLabel.font = [UIFont systemFontOfSize:12];
    [shareView addSubview:qqLabel];
    [qqLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(qqBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
        make.left.mas_equalTo(171);
    }];

    UIButton *gbBtn = [[UIButton alloc]init];
    [gbBtn setImage:[UIImage imageNamed:@"弹窗关闭.png"] forState:UIControlStateNormal];
    [gbBtn addTarget:self action:@selector(gbBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:gbBtn];
    
    [gbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(qqLabel.mas_bottom).offset(15);
        make.bottom.mas_equalTo(shareView.mas_bottom).offset(0);
    }];
    
    // 单击的 Recognizer
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap:)];
    //点击的次数
    //    singleRecognizer.numberOfTapsRequired = 1; // 单击
    //_tap = singleRecognizer;
    singleRecognizer.delegate = self;
    //给self.view添加一个手势监测；
    
    [view addGestureRecognizer:singleRecognizer];
    
}

//=== 分享按钮
-(void)wxBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    NSDictionary *dic = @{@"positionid":_url};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
        NSLog(@"%@",result);
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        
        if (basemodel.code == 0) {
            
            ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.positionTitle Detial:SF(@"月薪%@",self.shareModel.salary) ImageUrl:self.shareModel.companyLogo];
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

}
-(void)wbBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    NSDictionary *dic = @{@"positionid":_url};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
        
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.positionTitle Detial:SF(@"月薪%@",self.shareModel.salary) ImageUrl:self.shareModel.companyLogo];
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

}
-(void)qqBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    NSDictionary *dic = @{@"positionid":_url};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
        
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.positionTitle Detial:SF(@"月薪%@",self.shareModel.salary) ImageUrl:self.shareModel.companyLogo];
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];

}
-(void)gbBtnAction:(UIButton *)btn{

    _winView.hidden = YES;
    _shareView.hidden = YES;
}
-(void)SingleTap:(UITapGestureRecognizer*)recognizer  {
    
    
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    
    
}
//- (void)responsedButtonClickWithBtnType:(NSInteger)type{
//    switch (type) {
//        case ICN_WarnBottomBtnType:{
//            HRLog(@"点击的是底部的按钮");
//            [self.replayView removeFromSuperview];
//            break;
////        }
////        case ICN_ShareToDynamicStateBtnType:{
////            // 分享到 动态
////            
////            break;
////        }
////        case ICN_ShareToICNFriendBtnType:{
////            // 分享到 I行好友
////            break;
//        }
//        case ICN_ShareToWeiChatBtnType:{
//            // 分享到 微信
//            
//            NSDictionary *dic = @{@"positionId":_url};
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
//                NSLog(@"%@",result);
//                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//                if (basemodel.code == 0) {
//                    
//                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
//                }else{
//                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//                    
//                }
//                
//                
//            } failure:^(NSDictionary *errorInfo) {
//                
//            }];
//
//
//            break;
//        }
//        case ICN_ShareToWeiBOBtnType:{
//            // 分享到 微博
//            
//            
//            
//            NSDictionary *dic = @{@"positionId":_url};
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
//                
//                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//                if (basemodel.code == 0) {
//                    
//                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:basemodel.result.pic  Detial:@""];
//                }else{
//                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//                    
//                }
//                
//                
//            } failure:^(NSDictionary *errorInfo) {
//                
//            }];
//            
//
//            
//            break;
//        }
//        case ICN_ShareToQQBtnType:{
//            // 分享到 QQ
//            NSDictionary *dic = @{@"positionId":_url};
//            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/position" params:dic success:^(id result) {
//                
//                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
//                if (basemodel.code == 0) {
//                    
//                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
//                }else{
//                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
//                    
//                }
//                
//                
//            } failure:^(NSDictionary *errorInfo) {
//                
//            }];
//            
//            
//            
//            break;
//        }
//        default:
//            //[self.warnView removeFromSuperview];
//            [self.replayView removeFromSuperview];
//            break;
//    }
//}

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}

//投递简历
-(void)jianliAction:(UIButton *)btn{
    
    if ([_iscompleteness isEqualToString:@"0"]) {
        if ([_issend isEqualToString:@"0"]) {
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            NSLog(@"%@",token);
            NSDictionary *dic = @{@"positionId":_url,@"token":token};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberResume/doDeliveryResume" params:dic success:^(id result) {
                [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
                _jianliBtn.backgroundColor = RGB(16, 134, 254);
                [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
                _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"投递简历成功"];
            } failure:^(NSDictionary *errorInfo) {
                
            }];
 
        }else{
            [_jianliBtn setTitle:@"已经投递" forState:UIControlStateNormal];
            _jianliBtn.backgroundColor = RGB(16, 134, 254);
            [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
            _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        
        }
    }else{
    
        ICN_MyResumeViewController *resu = [[ICN_MyResumeViewController alloc]init];
        [self.navigationController pushViewController:resu animated:YES];
    
    }
    
    


}
//举报
-(void)jubaoAction:(UIButton *)btn{


    ICN_reportReportViewController * report = [[ICN_reportReportViewController alloc]init];
    report.positionid = _url;
    [self.navigationController pushViewController:report animated:YES];




}
//收藏
-(void)choucangAction:(UIButton *)btn{

     if ([_iscollect isEqualToString:@"0"]) {
         NSString *token;
         if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
             token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
         }
    NSDictionary *dic = @{@"positionId":_url,@"token":token};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberCollection/doAddPositionCollection" params:dic success:^(id result) {
       
     
            [_shoucangBtn setImage:[UIImage imageNamed:@"取消收藏2"] forState:UIControlStateNormal];
            [_shoucangBtn setTitleColor:RGB0X(0X666666) forState:UIControlStateSelected];
            _shoucangBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
            _shoucangBtn.titleLabel.font = [UIFont systemFontOfSize:12];
            [_shoucangBtn setTitle:@"取消收藏" forState:UIControlStateSelected];
            
            CGRect frame = _shoucangBtn.frame;
            frame.size.width = 68;
            [_shoucangBtn setFrame:frame];
            //NSLog(@"%f",_shoucangBtn.width);
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
          [self datas];
            NSLog(@"%@",_iscollect);
        
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
     }else{
         NSString *token;
         if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
             token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
         }
     NSDictionary *dic = @{@"id":_url,@"token":token};
         [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberCollection/doCancelPositionCollection" params:dic success:^(id result) {
            
             
//             [_shoucangBtn setTitle:@"收藏" forState:UIControlStateNormal];
             
//             [_shoucangBtn setTitleColor:RGB0X(0X666666) forState:UIControlStateNormal];
//             [_shoucangBtn setImage:[UIImage imageNamed:@"收藏2.png"] forState:UIControlStateNormal];
             _shoucangBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
             _shoucangBtn.titleLabel.font = [UIFont systemFontOfSize:12];
             
             [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏"];
             [self datas];
             NSLog(@"%@",_iscollect);
             
         } failure:^(NSDictionary *errorInfo) {
             
         }];
     
        
     
     
     }

    
}
//关注
-(void)lookAction:(UIButton *)btn{
    //btn.selected = !btn.selected;
    if ([_isfollow isEqualToString:@"0"]) {
        NSString *token;
        
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
        NSLog(@"%@",token);
        NSDictionary *dic = @{@"enterpriseId":_memberId,@"token":token};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberConcern/doAddConcern" params:dic success:^(id result) {
            NSLog(@"%@",result);
            [btn setImage:[UIImage imageNamed:@"取消关注.png"] forState:UIControlStateNormal];
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"已关注"];
            
//            _isfollow = @"1";
            [self datas];
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }else{
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }
     NSDictionary *dic = @{@"enterpriseId":_memberId,@"token":token};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberConcern/doDeleteConcern" params:dic success:^(id result) {
            NSLog(@"%@",result);
            [btn setImage:[UIImage imageNamed:@"加关注.png"] forState:UIControlStateNormal];
            
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消关注"];
//           _isfollow = @"0";
            [self datas];
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    
    
    }


}
-(void)datas{
    _dataArr = [[NSMutableArray alloc]init];

    
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    NSLog(@"%@",token);
    
    if (token == nil) {
        return;
    }
    else{
        NSLog(@"%@",_url);
        NSDictionary *dic = @{@"positionid":[NSString stringWithFormat:@"%@",_url],@"token":token};
        NSLog(@"%@",dic);
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Position/postionDetail" params:dic success:^(id result) {
            
            
            
            NSLog(@"%@",result);
            if ([[result valueForKey:@"code"] integerValue] != 0 ) {
                return ;
            }
            else{
                
                NSDictionary *dict = result[@"result"][0];
                ICN_PositionNextOneModel *model =[[ICN_PositionNextOneModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                self.shareModel = model;
                
                //=====
                _iscollect = [NSString stringWithFormat:@"%@",model.iscollect];
                NSLog(@"%@",_iscollect);
                _isfollow = [NSString stringWithFormat:@"%@",model.isfollow];
                _memberId = model.memberId;
                
                if ([_isfollow isEqualToString:@"0"]) {
                    [_lookBtn setImage:[UIImage imageNamed:@"加关注.png"] forState:UIControlStateNormal];
                }else{
                    [_lookBtn setImage:[UIImage imageNamed:@"取消关注.png"] forState:UIControlStateNormal];
                    
                }
                if ([_iscollect isEqualToString:@"0"]) {
                    [_shoucangBtn setTitle:@"收藏" forState:UIControlStateNormal];
                    [_shoucangBtn setImage:[UIImage imageNamed:@"收藏2.png"] forState:UIControlStateNormal];
                }else{
                    [_shoucangBtn setImage:[UIImage imageNamed:@"取消收藏2"] forState:UIControlStateNormal];
                    [_shoucangBtn setTitleColor:RGB0X(0X666666) forState:UIControlStateNormal];
                    _shoucangBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
                    _shoucangBtn.titleLabel.font = [UIFont systemFontOfSize:12];
                    [_shoucangBtn setTitle:@"取消收藏" forState:UIControlStateNormal];
                    
                    CGRect frame = _shoucangBtn.frame;
                    frame.size.width = 68;
                    [_shoucangBtn setFrame:frame];
                
                }
               
                _issend = [NSString stringWithFormat:@"%@",model.issend];
                _iscompleteness = [NSString stringWithFormat:@"%@",model.iscompleteness];
                if ([_iscompleteness isEqualToString:@"0"]) {
                    [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
                    _jianliBtn.backgroundColor = RGB(16, 134, 254);
                    [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
                    _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                    if ([_issend isEqualToString:@"0"]) {
                        [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
                        _jianliBtn.backgroundColor = RGB(16, 134, 254);
                        [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
                        _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                    }else{
                        [_jianliBtn setTitle:@"已经投递" forState:UIControlStateNormal];
                        _jianliBtn.backgroundColor = RGB(16, 134, 254);
                        [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
                        _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                    
                    }
                }else{
                    [_jianliBtn setTitle:@"完善简历" forState:UIControlStateNormal];
                    _jianliBtn.backgroundColor = RGB(16, 134, 254);
                    [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
                    _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
                    if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == [NSNumber numberWithInteger:HR_ICNEnterprise]) {
                        _jianliBtn.hidden = YES;
                    }
                }
                
                [_topImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.companyLogo]]];
                _topTitleLabel.text =  model.positionTitle;
                _topTitleNetLabel.text = model.companyName;
                _topRmbLabel.text = model.salary;
                _label1.text = model.city;
                _label2.text = model.qualification;
                _label3.text = model.workExperience;
                _label4.text = model.workType;
                _label5.text = model.personCount;
                _topBigLabel.text = model.summary;
                self.positionid = model.positionid;
                
                [_downImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.companyLogo]]];
                _downTitleLabel.text = model.companyName;
                _downTitleNextLabel.text = [NSString stringWithFormat:@"%@|%@人",model.companyNature,model.companyScale];
                _downyidongLabel.text = model.companyIndustry;
                _downBigLabel.text = model.companyDetail;
                
                [_scrollView.mj_header endRefreshing];
            }
            
            
            
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
        

    }
    

}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

-(void)createUI{

   
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
    
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = leftItem3;
    self.title = @"职位详情";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSFontAttributeName:[UIFont systemFontOfSize:16],
       NSForegroundColorAttributeName:RGB0X(0Xffffff)}];

    
    UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(0,0, 15, 15)];
    
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setBackgroundImage:[[UIImage imageNamed:@"分享.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:btn1];
    self.navigationItem.rightBarButtonItem  = item;
    
    UIView *view = [[UIView alloc]init];
    view.tag = 11;
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(38);
    }];
    
    
    _jianliBtn = [[UIButton alloc]init];
//    [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
//    _jianliBtn.backgroundColor = RGB(16, 134, 254);
//    [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
//    _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    if ([_iscompleteness isEqualToString:@"0"]) {
        [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
        _jianliBtn.backgroundColor = RGB(16, 134, 254);
        [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
        _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        if ([_issend isEqualToString:@"0"]) {
            [_jianliBtn setTitle:@"投递简历" forState:UIControlStateNormal];
            _jianliBtn.backgroundColor = RGB(16, 134, 254);
            [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
            _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        }else{
            [_jianliBtn setTitle:@"已经投递" forState:UIControlStateNormal];
            _jianliBtn.backgroundColor = RGB(16, 134, 254);
            [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
            _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            
        }
    }else{
        [_jianliBtn setTitle:@"完善简历" forState:UIControlStateNormal];
        _jianliBtn.backgroundColor = RGB(16, 134, 254);
        [_jianliBtn setTitleColor:RGB0X(0Xffffff) forState:UIControlStateNormal];
        _jianliBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        
    }


    [_jianliBtn addTarget:self action:@selector(jianliAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_jianliBtn];
       [_jianliBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(39);
        make.width.mas_equalTo(view.mas_width).multipliedBy(0.5);
    }];
    
    
    UIButton *jubaoBtn = [[UIButton alloc]init];
    [jubaoBtn setTitle:@"举报" forState:UIControlStateNormal];
    [jubaoBtn setTitleColor:RGB0X(0X666666) forState:UIControlStateNormal];
    [jubaoBtn addTarget:self action:@selector(jubaoAction:) forControlEvents:UIControlEventTouchUpInside];
    [jubaoBtn setImage:[UIImage imageNamed:@"举报.png"] forState:UIControlStateNormal];
     jubaoBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
    jubaoBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [view addSubview:jubaoBtn];
    [jubaoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(24);
        make.width.mas_equalTo(52);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(38);
    }];
   
    
    
    _shoucangBtn = [[UIButton alloc]init];
    [_shoucangBtn setTitle:@"收藏" forState:UIControlStateNormal];
    [_shoucangBtn setImage:[UIImage imageNamed:@"收藏2.png"] forState:UIControlStateNormal];
    [_shoucangBtn setImage:[UIImage imageNamed:@"取消收藏2.png"] forState:UIControlStateSelected];
    [_shoucangBtn addTarget:self action:@selector(choucangAction:) forControlEvents:UIControlEventTouchUpInside];
    [_shoucangBtn setTitleColor:RGB0X(0X666666) forState:UIControlStateNormal];
    
    _shoucangBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-5, 0, 0);
    _shoucangBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [view addSubview:_shoucangBtn];
    [_shoucangBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_jianliBtn.mas_left).offset(-24);
        make.width.mas_equalTo(52);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(39);
    }];
    //企业用户隐藏
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == [NSNumber numberWithInteger:HR_ICNEnterprise]) {
        _shoucangBtn.hidden = YES;
    }
    
    
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = RGB(236, 236, 236);
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(_jianliBtn.mas_left).offset(0);
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(0.5);
        
    }];
    
    
    

}
-(void)refreshingData{
    
    _type = 1;
    [self datas];
    
    
}
-(void)neirongUI{
 
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64 - 38)];

    //_scrollView.backgroundColor = [UIColor whiteColor];
   [self.view addSubview:_scrollView];
    
  
    _scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
      
        
        [self datas];
    }];
    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        
        
        //self.tableView.mj_header = header;
        [self refreshingData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];

    _contentView = [[UIView alloc] initWithFrame:CGRectZero];
    [_scrollView addSubview:_contentView];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_scrollView);
        make.bottom.equalTo(_scrollView);
        make.top.equalTo(_scrollView);
        make.right.equalTo(_scrollView);
        make.width.equalTo(self.scrollView.mas_width);
    }];


    _topImageView = [[UIImageView alloc]init];
    [_contentView addSubview:_topImageView];
    [_topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_contentView.mas_left).offset(10);
        make.top.mas_equalTo(_contentView.mas_top).offset(12.5);
        make.width.mas_equalTo(54);
        make.height.mas_equalTo(54);
    }];
    
    _topTitleLabel = [[UILabel alloc]init];
    _topTitleLabel.textColor = RGB0X(0X000000);
    _topTitleLabel.font = [UIFont systemFontOfSize:15];
    [_contentView addSubview:_topTitleLabel];

    _topRmbLabel = [[UILabel alloc]init];
    _topRmbLabel.textColor = RGB0X(0Xff561b);
    _topRmbLabel.font = [UIFont systemFontOfSize:15];
    [_contentView addSubview:_topRmbLabel];
    [_topRmbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(12.5);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(15);
    }];
    
    [_topTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(12.5);
        make.left.mas_equalTo(_topImageView.mas_right).offset(10);
        make.right.mas_lessThanOrEqualTo(self.topRmbLabel.left).offset(-20);
        make.height.mas_equalTo(15);
    }];
    

    _topTitleNetLabel = [[UILabel alloc]init];
    _topTitleNetLabel.textColor = RGB0X(0X333333);
    _topTitleNetLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_topTitleNetLabel];
    [_topTitleNetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_topImageView.mas_right).offset(10);
        make.top.mas_equalTo(_topTitleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];

    

    UIImageView *image1 = [[UIImageView alloc]init];
    image1.image = [UIImage imageNamed:@"地点.png"];
    [_contentView addSubview:image1];
    [image1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_topImageView.mas_right).offset(10);
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
    }];
    
    
    _label1 = [[UILabel alloc]init];
    _label1.textColor = RGB0X(0X666666);
    _label1.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_label1];
    [_label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(image1.mas_right).offset(3);
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];
    
    
    UIImageView *image2 = [[UIImageView alloc]init];
    image2.image = [UIImage imageNamed:@"学历.png"];
    [_contentView addSubview:image2];
    [image2 mas_makeConstraints:^(MASConstraintMaker *make) {
       make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(_label1.mas_right).offset(10);
    }];
    
    
    

    _label2 = [[UILabel alloc]init];
    _label2.textColor = RGB0X(0X666666);
    _label2.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_label2];
    [_label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(image2.mas_right).offset(3);
        make.height.mas_equalTo(12);

    }];
    
    UIImageView *image3 = [[UIImageView alloc]init];
    image3.image = [UIImage imageNamed:@"工作经验.png"];
    [_contentView addSubview:image3];
    [image3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(_label2.mas_right).offset(10);
    }];
    
    _label3 = [[UILabel alloc]init];
    _label3.textColor = RGB0X(0X666666);
    _label3.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_label3];
    [_label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(image3.mas_right).offset(3);
        make.height.mas_equalTo(12);
        
    }];
    
    
    UIImageView *image4 = [[UIImageView alloc]init];
    image4.image = [UIImage imageNamed:@"全职.png"];
    [_contentView addSubview:image4];
    [image4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(11);
        make.left.mas_equalTo(_label3.mas_right).offset(10);
    }];
    
    
    _label4 = [[UILabel alloc]init];
    _label4.textColor = RGB0X(0X666666);
    _label4.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_label4];
    [_label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(image4.mas_right).offset(3);
        make.height.mas_equalTo(12);
        
    }];
    

    UIImageView *image5 = [[UIImageView alloc]init];
    image5.image = [UIImage imageNamed:@"招聘人数.png"];
    [_contentView addSubview:image5];
    [image5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(_label4.mas_right).offset(10);
    }];
    
    _label5 = [[UILabel alloc]init];
    _label5.textColor = RGB0X(0X666666);
    _label5.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_label5];
    [_label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_topTitleNetLabel.mas_bottom).offset(10);
        make.left.mas_equalTo(image5.mas_right).offset(3);
        make.height.mas_equalTo(12);
        
    }];
    
    UILabel *label11 = [[UILabel alloc]init];
    label11.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label11];
    [label11 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.top.mas_equalTo(_label3.mas_bottom).offset(13);
        make.height.mas_equalTo(5);
    }];
    
    UILabel *label12 = [[UILabel alloc]init];
    label12.textColor = RGB0X(0X000000);
    label12.font = [UIFont systemFontOfSize:14];
    [_contentView addSubview:label12];
    [label12 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label11.mas_bottom).offset(13.5);
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.height.mas_equalTo(14);
    }];
    label12.text = @"职位描述";
    
    UILabel *label13 = [[UILabel alloc]init];
    label13.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label13];
    [label13 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(label12.mas_bottom).offset(13.5);
        make.height.mas_equalTo(1);
    }];
    
    UILabel *label14 = [[UILabel alloc]init];
    label14.textColor = RGB0X(0X666666);
    label14.font = [UIFont systemFontOfSize:13];
    [_contentView addSubview:label14];
    [label14 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(label13.mas_bottom).offset(15);
        make.height.mas_equalTo(13);
    }];
    label14.text = @"工作职责:";
    
    
    _topBigLabel = [[UILabel alloc]init];
    _topBigLabel.textColor = RGB0X(0X666666);
    _topBigLabel.font = [UIFont systemFontOfSize:12];

   
    
    
    _topBigLabel.numberOfLines = 0;
    [_contentView addSubview:_topBigLabel];
    [_topBigLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(label14.mas_bottom).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
    }];
    _topBigLabel.text = @"";
    //label行间距
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:_topBigLabel.text];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:5.0];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [_topBigLabel.text length])];
    [_topBigLabel setAttributedText:attributedString1];

    UILabel *label15 = [[UILabel alloc]init];
    label15.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label15];
    [label15 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.top.mas_equalTo(_topBigLabel.mas_bottom).offset(20);
        make.height.mas_equalTo(5);
        //make.bottom.mas_equalTo(0);
    }];

    _downImageView = [[UIImageView alloc]init];
    [_contentView addSubview:_downImageView];
    [_downImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(label15.mas_bottom).offset(12.5);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(50);
    }];
    
    _downImageView.layer.cornerRadius =25;
    _downImageView.layer.masksToBounds = YES;
    

    _downTitleLabel = [[UILabel alloc]init];
    _downTitleLabel.textColor = RGB0X(0X000000);
    _downTitleLabel.font = [UIFont systemFontOfSize:14];
    [_contentView addSubview:_downTitleLabel];
    [_downTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_downImageView.mas_right).offset(10);
        make.top.mas_equalTo(label15.mas_bottom).offset(12.5);
        make.height.mas_equalTo(14);
    }];
  
    
    _downTitleNextLabel = [[UILabel alloc]init];
    _downTitleNextLabel.textColor = RGB0X(0X666666);
    _downTitleNextLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_downTitleNextLabel];
    [_downTitleNextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_downImageView.mas_right).offset(10);
        make.top.mas_equalTo(_downTitleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
    }];
 
    
    
    
    
    _downyidongLabel = [[UILabel alloc]init];
    _downyidongLabel.textColor = RGB0X(0X666666);
    _downyidongLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_downyidongLabel];
    [_downyidongLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_downImageView.mas_right).offset(10);
        make.top.mas_equalTo(_downTitleNextLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];
    

    
    UILabel *label16 = [[UILabel alloc]init];
    label16.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label16];
    [label16 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.top.mas_equalTo(_downyidongLabel.mas_bottom).offset(13);
        make.height.mas_equalTo(5);
        //make.bottom.mas_equalTo(0);
    }];
    
    UILabel *label17 = [[UILabel alloc]init];
    label17.textColor = RGB0X(0X000000);
    label17.font = [UIFont systemFontOfSize:13];
    [_contentView addSubview:label17];
    [label17 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(label16.mas_bottom).offset(13.5);
        make.height.mas_equalTo(13);
    }];
    
    
    self.lookBtn = [[UIButton alloc]init];
    
    
    [_lookBtn addTarget:self action:@selector(lookAction:) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:_lookBtn];
    [_lookBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-15);
        make.top.mas_equalTo(label15.mas_bottom).offset(20);
        make.bottom.mas_equalTo(label17.mas_top).offset(-30);
        make.width.mas_equalTo(30);
    }];
    //企业用户隐藏
    if ([USERDEFAULT valueForKey:HR_UserTypeKEY] == [NSNumber numberWithInteger:HR_ICNEnterprise]) {
        _lookBtn.hidden = YES;
    }
    
    
    label17.text = @"公司介绍";

    UILabel *label18 = [[UILabel alloc]init];
    label18.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label18];
    [label18 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(label17.mas_bottom).offset(13.5);
        make.height.mas_equalTo(1);
        //make.bottom.mas_equalTo(0);
        
    }];

    _downBigLabel = [[UILabel alloc]init];
    _downBigLabel.textColor = RGB0X(0X666666);
    _downBigLabel.font = [UIFont systemFontOfSize:12];
    _downBigLabel.numberOfLines = 0;
    [_contentView addSubview:_downBigLabel];
    
    [_downBigLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(label18.mas_bottom).offset(15);
        
    }];
    
    _downBigLabel.text = @"";
    //label行间距
    NSMutableAttributedString * attributedString2 = [[NSMutableAttributedString alloc] initWithString:_downBigLabel.text];
    NSMutableParagraphStyle * paragraphStyle2 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle2 setLineSpacing:5.0];
    [attributedString2 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [_downBigLabel.text length])];
    [self.downBigLabel setAttributedText:attributedString2];
    
    
    UILabel *label19 = [[UILabel alloc]init];
    label19.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:label19];
    [label19 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.top.mas_equalTo(_downBigLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(15);
        make.bottom.mas_equalTo(0);
    }];
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
