//
//  ICN_reportReportViewController.h
//  ICan
//
//  Created by 那风__ on 17/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICN_reportReportViewController : BaseViewController
@property (nonatomic , copy)NSString *matterId; // 所举报的动态ID
@property (nonatomic , copy)NSString *memberId; // 所举报的用户的id

@property (nonatomic , copy)NSString *positionid;
@property (strong,nonatomic)NSString * tempStr;
@property (strong,nonatomic)NSString *urlStr;
@end
