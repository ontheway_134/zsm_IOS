//
//  ICN_reportReportViewController.m
//  ICan
//
//  Created by 那风__ on 17/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//
#import "ICN_reportReportViewController.h"
#import "HRNetworkingManager+AuthorHomePager.h"

static NSString *const MessageText = @"请输入您的意见或建议(选填)";

static const NSInteger p_ReportSelectBtnBaseTag = 12200100; // 设定默认的第一个侵权选中按钮的tag减去基础tag是1

@interface ICN_reportReportViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *p_TextViewBackGround;

@property (weak, nonatomic) IBOutlet UITextView *p_ContentTextView;

@property (weak, nonatomic) IBOutlet UILabel *p_WordsCountLabel;

@property (weak, nonatomic) IBOutlet UIButton *p_SubmitBtn; // 提交按钮

@property (nonatomic , strong)NSMutableArray *selectTypeArr; // 选中的类型数组

@end

@implementation ICN_reportReportViewController

- (NSMutableArray *)selectTypeArr{
    if (_selectTypeArr == nil) {
        _selectTypeArr = [NSMutableArray array];
    }
    return _selectTypeArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.naviTitle = @"举报";
    self.p_ContentTextView.delegate = self;
    [self configCurrentPagerItems];
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
    
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
    
    self.navigationItem.leftBarButtonItem = leftItem3;
    // Do any additional setup after loading the view from its nib.
}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configCurrentPagerItems{
    // 添加提交按钮的圆角
    self.p_SubmitBtn.layer.cornerRadius = 2.0;
    self.p_SubmitBtn.layer.masksToBounds = YES;
    
    // 添加举报文本背景的边界
    self.p_TextViewBackGround.layer.borderColor = RGB0X(0xe5e5e5).CGColor;
    self.p_TextViewBackGround.layer.borderWidth = 1.0;
    self.p_TextViewBackGround.layer.cornerRadius = 5.0;
}

#pragma mark - ---------- 私有方法 ----------

/** 通过用户Id或者是动态Id进行举报 */
- (void)reportWithMemberId:(NSString *)memberId MatterId:(NSString *)matterId Content:(NSString *)content{
    
    NSString *type = [self.selectTypeArr componentsJoinedByString:@","];
    
    if ([self.tempStr isEqualToString:@"1"]) {
        
        if (self.urlStr != nil) {
            // 举报用户
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            NSDictionary *dic = @{@"informationId":_urlStr,@"token":token,@"reason":type,@"summary":_p_ContentTextView.text};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/Report/PositionNewsReport" params:dic success:^(id result) {
                NSLog(@"%@",result);
                [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"举报成功"];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSDictionary *errorInfo) {
                
            }];

            
        }
        else{
        
            // 举报动态
            [HRNetworkingManager requestReportWithMatterId:self.matterId Type:type Summary:content Success:^(id responseObject) {
                BaseOptionalModel *model = responseObject;
                if (model.code == 0) {
                    // 成功
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"举报成功"];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:model.info];
                }
            } Failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"举报失败 - 网络请求失败"];
            }];
        }
        
        
    }
    else{
        if (self.positionid != nil) {
            // 举报用户
            NSString *token;
            if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
                token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
            }
            NSDictionary *dic = @{@"positionId":_positionid,@"token":token,@"reason":type,@"summary":_p_ContentTextView.text};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/Member/MemberResume/doDeliveryResume" params:dic success:^(id result) {
                
                [MBProgressHUD ShowProgressWithBaseView:[UIApplication sharedApplication].keyWindow Message:@"举报成功"];
                [self.navigationController popViewControllerAnimated:YES];
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            
            
        }else{
            // 举报动态
            [HRNetworkingManager requestReportWithMatterId:self.matterId Type:type Summary:content Success:^(id responseObject) {
                BaseOptionalModel *model = responseObject;
                if (model.code == 0) {
                    // 成功
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"举报成功"];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:model.info];
                }
            } Failure:^(NSDictionary *errorInfo) {
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"举报失败 - 网络请求失败"];
            }];
            
        }
        

    }
    
    
    
}

#pragma mark - ---------- IBAction ----------

- (IBAction)clickReportSelectedAction:(UIButton *)sender {
    // 根据tag判断具体点击那个按钮 并将选中的按钮添加在选中数组
    NSLog(@"选中的按钮的编号是：%ld",sender.tag - p_ReportSelectBtnBaseTag);
    NSInteger selectType = sender.tag - p_ReportSelectBtnBaseTag;
    if (_selectTypeArr != nil && [self.selectTypeArr containsObject:SF(@"%ld",selectType)]) {
        // 如果数组中有证明是取消选中
        [self.selectTypeArr removeObject:SF(@"%ld",selectType)];
        [sender setBackgroundImage:[UIImage imageNamed:@"未选择"] forState:UIControlStateNormal];
    }else{
        [sender setBackgroundImage:[UIImage imageNamed:@"选择"] forState:UIControlStateNormal];
        [self.selectTypeArr addObject:SF(@"%ld",selectType)];
    }
    
}


- (IBAction)clickOnSubmitBtnAction:(UIButton *)sender {
    if (self.selectTypeArr.count == 0) {
        [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"请添加一个举报理由"];
    }else{
        
        if ([self.tempStr isEqualToString:@"1"]) {
            NSString *content;
            if (self.p_ContentTextView.text != nil || ![self.p_ContentTextView.text isEqualToString:@""]) {
                
                self.p_ContentTextView.text = content;
            }
            if (self.urlStr!=nil) {
                // 举报用户用
                [self reportWithMemberId:self.urlStr MatterId:nil Content:content];
            }else{
            
            [self reportWithMemberId:nil MatterId:self.urlStr Content:content];
            }
            
            
            
        }
        else{
            NSString *content;
            if (self.p_ContentTextView.text != nil || ![self.p_ContentTextView.text isEqualToString:@""]) {
                
                self.p_ContentTextView.text = content;
            }
            
            if (self.positionid != nil) {
                // 举报用户用
                [self reportWithMemberId:self.positionid MatterId:nil Content:content];
            }else{
                [self reportWithMemberId:nil MatterId:self.positionid Content:content];
            }

        }
        
        
    }
}

#pragma mark --- UITextViewDelegate ---

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:MessageText]) {
        textView.text = @"";
        textView.textColor = RGB0X(0x333333);
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (textView.text.length + text.length - range.length < 150) {
        self.p_WordsCountLabel.text = SF(@"%lu/150",textView.text.length + text.length - range.length);
        self.p_WordsCountLabel.textColor = RGB0X(0xb6b6b6);
        return YES;
    }else{
        self.p_WordsCountLabel.text = @"输出文字超限";
        self.p_WordsCountLabel.textColor = [UIColor redColor];
        if ([text isEqualToString:@""]) {
            return YES;
        }else
            return NO;
    }
    return YES;
}






@end
