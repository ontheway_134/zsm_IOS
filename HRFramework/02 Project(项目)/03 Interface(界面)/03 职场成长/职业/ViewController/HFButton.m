//
//  HFButton.h
//  ZRHY
//
//  Created by  on 16/7/21.
//  Copyright © 2016年 辛忠志. All rights reserved.
//

#import "HFButton.h"
#import "UIViewExt.h"

@implementation HFButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 修改按钮内部子控件位置
    if (self.titleLabel.x > self.imageView.x) {
        // 设置label
        self.titleLabel.x = self.imageView.x;
        // 设置ImageView
        self.imageView.x = CGRectGetMaxX(self.titleLabel.frame) + 10;
    }
    
    NSLog(@"titleLabel:%f imageView:%f",self.titleLabel.x,self.imageView.x);
}

- (void)setTitle:(nullable NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];

    [self sizeToFit];
}

- (void)setImage:(nullable UIImage *)image forState:(UIControlState)state
{
    [super setImage:image forState:state];
    [self sizeToFit];
    
}


@end
