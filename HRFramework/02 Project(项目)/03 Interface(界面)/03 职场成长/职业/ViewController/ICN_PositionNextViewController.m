//
//  ICN_PositionNextViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_PositionNextViewController.h"
#import "ICN_PositionNextModel.h"
#import "ICN_PositionNextTableViewCell.h"

#import "ICN_PositionNextOneViewController.h"
@interface ICN_PositionNextViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@end

@implementation ICN_PositionNextViewController
- (NSMutableArray *)dataArr
{
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self createUI];
    
   
    
    
    
}
- (void)zj_viewWillAppearForIndex:(NSInteger)index
{
    [self datasWith:index];

    
}
//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [self datasWith:self.index];
//   
//    
//}
-(void)datasWith:(NSInteger)index{

    NSDictionary *dic = @{@"page":@"1",@"bigclassid":[NSString stringWithFormat:@"%ld",(long)index]};
    
    self.dataArr = nil;
[[[HRRequestManager alloc]init]POST_URL:@"http://192.168.1.36/zsm/index.php/MyPosition/PositionClass/postionclassSect" params:dic success:^(id result) {
    NSArray *dic1 =result[@"result"];
    //NSLog(@"%@",dic1);
    
    
    for (NSDictionary *dic in dic1 ) {
        ICN_PositionNextModel *model = [[ICN_PositionNextModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [self.dataArr addObject:model];
    }
    
    [self.tableView reloadData];
    
    
    
    
} failure:^(NSDictionary *errorInfo) {
    
}];



}
- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 49 - 175) style:UITableViewStylePlain];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        [_tableView registerClass:[ICN_PositionNextTableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}
-(void)createUI{
    
    
    [self.view addSubview:self.tableView];

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.dataArr.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    ICN_PositionNextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ICN_PositionNextModel *model = self.dataArr[indexPath.row];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;



}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 86;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    ICN_PositionNextOneViewController *icn = [[ICN_PositionNextOneViewController alloc]init];
    icn.hidesBottomBarWhenPushed = YES;
    ICN_PositionNextModel *model = self.dataArr[indexPath.row];
    icn.url = model.positionid;
    [self.navigationController pushViewController:icn animated:YES];



}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
