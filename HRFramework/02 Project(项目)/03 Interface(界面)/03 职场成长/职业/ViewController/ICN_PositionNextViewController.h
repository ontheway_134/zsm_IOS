//
//  ICN_PositionNextViewController.h
//  ICan
//
//  Created by 那风__ on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZJScrollPageViewDelegate.h"
@interface ICN_PositionNextViewController : BaseViewController<ZJScrollPageViewChildVcDelegate>
@property (assign,nonatomic)NSInteger index;
@end
