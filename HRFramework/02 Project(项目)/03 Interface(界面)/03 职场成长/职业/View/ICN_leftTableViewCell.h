//
//  ICN_leftTableViewCell.h
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_leftModel.h"
@interface ICN_leftTableViewCell : UITableViewCell
@property(nonatomic)UILabel *shengLabel;
@property(nonatomic,strong)ICN_leftModel *model;
@end
