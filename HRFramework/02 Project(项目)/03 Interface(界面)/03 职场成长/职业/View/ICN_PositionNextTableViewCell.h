//
//  ICN_PositionNextTableViewCell.h
//  ICan
//
//  Created by 那风__ on 16/12/8.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ICN_PositionNextModel;
@interface ICN_PositionNextTableViewCell : UITableViewCell
@property(nonatomic)UIImageView *logoimageView;
@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *rmbLabel;
@property(nonatomic)UILabel *compLabel;
@property(nonatomic)UILabel *cityLabel;
@property(nonatomic)UILabel *qualiLabel;
@property(nonatomic)ICN_PositionNextModel *model;
@property(nonatomic)NSString *ishot1;
@property(nonatomic)UIImageView *img;

@end
