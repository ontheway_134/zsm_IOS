//
//  ICN_rightTableViewCell.h
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICN_rightModel.h"
@interface ICN_rightTableViewCell : UITableViewCell
@property(nonatomic)UILabel *shiLabel;
@property(nonatomic,strong)ICN_rightModel *model;
@end
