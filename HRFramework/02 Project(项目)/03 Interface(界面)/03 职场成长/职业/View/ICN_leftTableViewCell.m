//
//  ICN_leftTableViewCell.m
//  ICan
//
//  Created by 那风__ on 17/1/16.
//  Copyright © 2017年 albert. All rights reserved.
//

#import "ICN_leftTableViewCell.h"

@implementation ICN_leftTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _shengLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_shengLabel];
        [_shengLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(14);
            make.center.equalTo(self.contentView);
        }];
        _shengLabel.font = [UIFont systemFontOfSize:14];
        
    }
    return self;

}
- (void)layoutSubviews{

    [super layoutSubviews];
   

}
-(void)setModel:(ICN_leftModel *)model{
    _model = model;
    _shengLabel.text = model.provincename;
//    if ([model.ID isEqualToString:@"1"]) {
//        _shengLabel.textColor = [UIColor blueColor];
//    }
    // 如果Model被选中了则改变其颜色
    if (model.selected != nil && model.selected.integerValue == 1) {
        _shengLabel.textColor = [UIColor blueColor];
    }else{
        _shengLabel.textColor = [UIColor blackColor];
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
