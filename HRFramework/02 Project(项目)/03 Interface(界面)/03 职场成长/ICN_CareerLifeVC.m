//
//  ICN_CareerLifeVC.m
//  ICan
//
//  Created by albert on 2016/11/28.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_CareerLifeVC.h"
#import "ICN_ProjectViewController.h"
#import "ICN_IndustryViewController.h"
#import "ICN_PositionViewController.h"
#import "ZJScrollPageView.h"
#import "ICN_ProfessionSearchViewController.h"
@interface ICN_CareerLifeVC ()<ZJScrollPageViewDelegate>


@property(strong, nonatomic)NSArray<NSString *> *titles;
@property (weak, nonatomic) ZJScrollSegmentView *segmentView;
@property (weak, nonatomic) ZJContentView *contentView;
@property(strong, nonatomic)NSArray<UIViewController<ZJScrollPageViewChildVcDelegate> *> *childVcs;
@end

@implementation ICN_CareerLifeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.leftBarButtonItem = nil;
    UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(0,0, 14, 14)];
    
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setBackgroundImage:[[UIImage imageNamed:@"搜索2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:btn1];
    self.navigationItem.rightBarButtonItem  = item;
    //必要的设置, 如果没有设置可能导致内容显示不正常
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    self.childVcs = [self setupChildVc];
    
    
    ZJSegmentStyle *style = [[ZJSegmentStyle alloc] init];
    style.showCover = YES;
    // 不要滚动标题, 每个标题将平分宽度
    style.scrollTitle = YES;
    // 渐变
    style.gradualChangeTitleColor = YES;
    style.coverCornerRadius = 0;
    // 遮盖背景颜色
    style.coverBackgroundColor = [UIColor clearColor];
    //标题一般状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.normalTitleColor = [UIColor colorWithRed:30/255.0 green:130/255.0 blue:210/255.0 alpha:1.0];
    
    //style.titleMargin = 50;
    
    //标题选中状态颜色 --- 注意一定要使用RGB空间的颜色值
    style.selectedTitleColor = RGB0X(0Xffffff);
    style.scrollLineColor = RGB0X(0Xffffff);
    style.normalTitleColor = RGB0X(0Xc0e7ff);
    style.showCover = YES;
    style.showLine = YES;
    
    self.titles = @[@"招聘", @"行业资讯",@"职场规划"];
    self.navigationController.navigationBar.barTintColor = RGB(16, 134, 255);
    // 注意: 一定要避免循环引用!!
    __weak typeof(self) weakSelf = self;
    ZJScrollSegmentView *segment = [[ZJScrollSegmentView alloc] initWithFrame:CGRectMake(0, 64.0, 250, 28.0) segmentStyle:style delegate:self titles:self.titles titleDidClick:^(ZJTitleView *titleView, NSInteger index) {
        
        [weakSelf.contentView setContentOffSet:CGPointMake(weakSelf.contentView.bounds.size.width * index, 0.0) animated:YES];
        
    }];
    
    self.segmentView = segment;
    self.navigationItem.titleView = self.segmentView;
    
    
    
    
    
    ZJContentView *content = [[ZJContentView alloc] initWithFrame:CGRectMake(0.0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 44.0) segmentView:self.segmentView parentViewController:self delegate:self];
//    NSLog(@"=========%.2f , %.2f" , self.view.bounds.size.width , self.view.bounds.size.height);
    self.contentView = content;
    [self.view addSubview:self.contentView];
    
       
    

}
- (NSArray *)setupChildVc {
    
    ICN_PositionViewController *vc1 = [ICN_PositionViewController new];
    
    ICN_IndustryViewController *vc2 = [ICN_IndustryViewController new];
    
    ICN_ProjectViewController *vc3 = [ICN_ProjectViewController new];
    
    NSArray *childVcs = [NSArray arrayWithObjects:vc1, vc2,vc3,nil];
    return childVcs;
}

- (NSInteger)numberOfChildViewControllers {
    return self.titles.count;
}




- (UIViewController<ZJScrollPageViewChildVcDelegate> *)childViewController:(UIViewController<ZJScrollPageViewChildVcDelegate> *)reuseViewController forIndex:(NSInteger)index {
    UIViewController<ZJScrollPageViewChildVcDelegate> *childVc = reuseViewController;
    NSLog(@"%ld",index);
    self.typeStr = [NSString stringWithFormat:@"%ld",index];
    NSLog(@"%@",self.typeStr);
    if (!childVc) {
        childVc = self.childVcs[index];
        
    }
    return childVc;
}
-(CGRect)frameOfChildControllerForContainer:(UIView *)containerView {
    return  CGRectInset(containerView.bounds, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)onClick:(UIButton *)btn{

    ICN_ProfessionSearchViewController *search = [[ICN_ProfessionSearchViewController alloc]init];
    /*属性传值*/
    search.typeStr =self.typeStr;
    
//    [self presentViewController:search animated:YES completion:nil];
    search.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:search animated:YES];
//    [self presentViewController:search animated:YES completion:nil];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
