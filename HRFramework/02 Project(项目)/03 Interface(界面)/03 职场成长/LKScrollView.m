//
//  LKScrollView.m
//  PoorTravel
//
//  Created by 黄金伟 on 16/7/13.
//  Copyright © 2016年 黄金伟. All rights reserved.
//

#import "LKScrollView.h"

typedef enum {
    FisrtButtonTag = 20
} ButtonTag;

@interface LKScrollView ()
/** 回调block */
@property (nonatomic, copy) callBack callBack;
@end

@implementation LKScrollView
{
    UIScrollView * _scrollView;
    UILabel * _titleLabel;
    
    //图片信息
    NSArray * _dataInfo;
    
    NSTimer * _timer;
    
    id _target;
    SEL _action;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self =  [super initWithFrame:frame]) {
        //创建子视图
        [self buildSubviews];
        //创建timer
        _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerGo:) userInfo:nil repeats:YES];
        [_timer setFireDate:[NSDate distantFuture]];//有可能没添加子视图
    }
    return self;
}

- (void)dealloc {
    if (_timer) {
        [_timer invalidate];
    }
}

- (void)buildSubviews {
    
    //滚动视图
    _scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    _scrollView.pagingEnabled = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;//横向滚动条
    _scrollView.delegate = self;
    
    //设置偏移量
    _scrollView.contentOffset = CGPointMake(self.bounds.size.width, 0);
    
    //标题
    //    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.bounds.size.height - 30, self.bounds.size.width, 30)];
    //    _titleLabel.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.6];
    //    _titleLabel.textColor = [UIColor colorWithWhite:0.8 alpha:0.9];
    _pageControl = [[UIPageControl alloc] init];
//    _pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(self.bounds.size.width - 140, self.bounds.size.height - 30, 140, 30)];
    //选中小圆点的颜色
    //    _pageControl.currentPageIndicatorTintColor = [UIColor colorWithWhite:0.7 alpha:0.9];
    _pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    //未选中的小圆点的颜色
    //    _pageControl.pageIndicatorTintColor = [UIColor colorWithWhite:0.1 alpha:0.8];
    _pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    //绑定方法
    [_pageControl addTarget:self action:@selector(pagging:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self addSubview:_scrollView];
    //    [self addSubview:_titleLabel];
    [self addSubview:_pageControl];
}

//填充子视图
- (void)loadDataWithArray:(NSArray *)dataInfo placeholderImage:(UIImage *)image andPageControlType:(PageControlType)type {
    _dataInfo = dataInfo;
    CGFloat pageControlX = 0;
    switch (type) {
        case PageControlTypeLeft:
            break;
        case PageControlTypeMiddel:
            pageControlX = self.bounds.size.width * 0.5 - 70;
            break;
        case PageControlTypeRight:
            pageControlX = self.bounds.size.width - 140;
            break;
        default:
            break;
    }
    _pageControl.frame = CGRectMake(pageControlX, self.bounds.size.height - 30, 140, 30);
    //初始化标题
    //    _titleLabel.text = [NSString stringWithFormat:@"    %@",dataInfo[0]];
    
    //pageControl
    _pageControl.numberOfPages = dataInfo.count;
    _pageControl.currentPage = 0;
    
    if (dataInfo.count != 0) {
        //设置包含内容大小
        //        _scrollView.contentSize = CGSizeMake((dataInfo.count + 1) * self.bounds.size.width, self.bounds.size.height);
        _scrollView.contentSize = CGSizeMake((dataInfo.count + 2) * self.bounds.size.width, self.bounds.size.height);
        //滚动视图
        //添加子视图
        //        for (int i = 0; i < dataInfo.count + 1; i++) {
        for (int i = 0; i < dataInfo.count + 2; i++) {
#if 1
            
            UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(i * self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height);
            button.tag = FisrtButtonTag + i;
            [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //最后一页判断
            if (i == dataInfo.count + 1) {
                if ([dataInfo[0] hasPrefix:@"http"] || [dataInfo[0] hasPrefix:@"https"]) {
                    [button sd_setBackgroundImageWithURL:[NSURL URLWithString:dataInfo[0]] forState:UIControlStateNormal placeholderImage:image];
                } else {
                    NSString * imagePath = [[NSBundle mainBundle] pathForResource:dataInfo[0] ofType:nil];
                    [button setBackgroundImage:[UIImage imageWithContentsOfFile:imagePath] forState:UIControlStateNormal];
                    button.userInteractionEnabled = NO;
                }
                [_scrollView addSubview:button];
                continue;            }
            //第一页判断
            if (i == 0) {
                if ([dataInfo[dataInfo.count - 1] hasPrefix:@"http"] || [dataInfo[dataInfo.count - 1] hasPrefix:@"https"]) {
                    [button sd_setBackgroundImageWithURL:[NSURL URLWithString:dataInfo[dataInfo.count - 1]] forState:UIControlStateNormal placeholderImage:image];
                } else {
                    NSString * imagePath = [[NSBundle mainBundle] pathForResource:dataInfo[dataInfo.count - 1] ofType:nil];
                    [button setBackgroundImage:[UIImage imageWithContentsOfFile:imagePath] forState:UIControlStateNormal];
                    button.userInteractionEnabled = NO;
                    
                }
                [_scrollView addSubview:button];
                continue;
            }
            
            if ([dataInfo[i - 1] hasPrefix:@"http"] || [dataInfo[i - 1] hasPrefix:@"https"]) {
                [button sd_setBackgroundImageWithURL:[NSURL URLWithString:dataInfo[i - 1]] forState:UIControlStateNormal placeholderImage:image];
            } else {
                NSString * imagePath = [[NSBundle mainBundle] pathForResource:dataInfo[i - 1] ofType:nil];
                [button setBackgroundImage:[UIImage imageWithContentsOfFile:imagePath] forState:UIControlStateNormal];
                button.userInteractionEnabled = NO;
                
            }
            [_scrollView addSubview:button];
#else
            UIImageView * imageView = [[UIImageView alloc]initWithFrame:CGRectMake(i * self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height)];
            if (i == dataInfo.count) {
                //最后一项的特殊处理
                //            NSString * imagePath = [[NSBundle mainBundle]pathForResource:dataInfo[0] ofType:nil];
                [imageView sd_setImageWithURL:[NSURL URLWithString:dataInfo[0]] placeholderImage:[UIImage imageNamed:@"bg_detail_cover_default"]];
                //            imageView.image = [UIImage imageWithContentsOfFile:imagePath];
                [_scrollView addSubview:imageView];
                break;

            }
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:dataInfo[i]] placeholderImage:[UIImage imageNamed:@"bg_detail_cover_default"]];
            //        NSString * imagePath = [[NSBundle mainBundle]pathForResource:dataInfo[i] ofType:nil];
            //        imageView.image = [UIImage imageWithContentsOfFile:imagePath];
            
            [_scrollView addSubview:imageView];
#endif
        }
        
        //多线程,延迟执行某些代码
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            //开启定时器
            [_timer setFireDate:[NSDate distantPast]];
        });
    }
}

#pragma mark - 定时器方法
- (void)timerGo:(NSTimer *)timer {
    NSInteger currentPage = (_scrollView.contentOffset.x) / self.bounds.size.width;
    //修改滚动视图的偏移量
    [_scrollView setContentOffset:CGPointMake((currentPage+1) * self.bounds.size.width, 0) animated:YES];
}

#pragma mark - pageControl方法
- (void)pagging:(UIPageControl *)pgc {
    NSInteger currentPage = pgc.currentPage;
    [_scrollView setContentOffset:CGPointMake((currentPage + 1) * _scrollView.bounds.size.width, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    //停止
    NSInteger currentPage = scrollView.contentOffset.x / self.bounds.size.width;
    NSInteger wholePages = scrollView.contentSize.width / self.bounds.size.width;//总页数
    //最后一页判断
    if (currentPage == wholePages - 1) {
        [scrollView setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
        _pageControl.currentPage = 0;
        
    } else if (currentPage == 0) {
        _pageControl.currentPage = _dataInfo.count - 1;
    }
    else {
        _pageControl.currentPage = currentPage - 1;
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //停止
    NSInteger currentPage = scrollView.contentOffset.x / self.bounds.size.width;
    NSInteger wholePages = scrollView.contentSize.width / self.bounds.size.width;//总页数
    //最后一页判断
    if (currentPage == wholePages - 1) {
        //        [scrollView setContentOffset:CGPointZero animated:NO];
        [scrollView setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
        _pageControl.currentPage = 0;
        
    } else if (currentPage == 0) {
        [scrollView setContentOffset:CGPointMake((wholePages - 2) * self.bounds.size.width, 0) animated:NO];
        _pageControl.currentPage = _dataInfo.count - 1;
        
    } else {
        _pageControl.currentPage = currentPage - 1;
    }
    
    //修改文本内容
    //    _titleLabel.text = [NSString stringWithFormat:@"    %@",_dataInfo[currentPage]];
}

- (void)buttonClicked:(UIButton *)button {
    if (self.callBack) {
        self.callBack(button, FisrtButtonTag + 1);
    }
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [_target performSelector:_action withObject:button];
#pragma clang diagnostic pop
    
}

- (void)addTarget:(id)target withAction:(SEL)action {
    _target = target;
    _action = action;
}

- (void)addActionWithBlock:(callBack)callBack {
    self.callBack = callBack;
}

@end
