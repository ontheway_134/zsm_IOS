//
//  ICN_ProjectTableViewCell.m
//  ICan
//
//  Created by 那风__ on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_ProjectTableViewCell.h"
#import "ICN_ProjectModel.h"
@implementation ICN_ProjectTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        UIView *view1 = [[UIView alloc]init];
        [self.contentView addSubview:view1];
        [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.left.mas_equalTo(self.contentView.mas_left).offset(9);
            make.top.mas_equalTo(self.contentView.mas_top).offset(0);
            make.height.mas_equalTo(198);
        }];
        _picImage = [[UIImageView alloc]init];
        [view1 addSubview:_picImage];
        [_picImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.height.mas_equalTo(140);
            make.top.mas_equalTo(0);
        }];
        
        _titleLabel = [[UILabel alloc]init];
        [view1 addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_picImage.mas_bottom).offset(10);
            make.left.mas_equalTo(10);
            make.height.mas_equalTo(15);
        }];
        
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:15];
        
        UIImageView *imageV1 = [[UIImageView alloc]init];
        imageV1.image = [UIImage imageNamed:@"时间.png"];
        [view1 addSubview:imageV1];
        [imageV1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
            
        }];
        
        _dataLabel = [[UILabel alloc]init];
        [view1 addSubview:_dataLabel];
        [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(imageV1.mas_right).offset(5);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(12);
        }];
        _dataLabel.textColor = RGB0X(0X666666);
        _dataLabel.font = [UIFont systemFontOfSize:12];
        
        _pageLabel = [[UILabel alloc]init];
        [view1 addSubview:_pageLabel];
        [_pageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-9);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
            make.height.mas_equalTo(12);
        }];
        _pageLabel.textColor = RGB0X(0X666666);
        _pageLabel.font = [UIFont systemFontOfSize:12];
        
        UIImageView *imageV2 = [[UIImageView alloc]init];
        imageV2.image = [UIImage imageNamed:@"阅读量.png"];
        [view1 addSubview:imageV2];
        [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(_pageLabel.mas_left).offset(-5);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10.8);
        }];
        
        UIView *view2 = [[UIView alloc]init];
        view2.backgroundColor = RGB(236, 236, 236);
        [self.contentView addSubview:view2];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.left.mas_equalTo(self.contentView.mas_left).offset(0);
            make.top.mas_equalTo(view1.mas_bottom).offset(0);
            make.height.mas_equalTo(5);
        }];
        
    }

    return self;
}
-(void)setModel:(ICN_ProjectModel *)model{

    [_picImage sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    _titleLabel.text = model.title;
    _dataLabel.text = model.createDate;
    _pageLabel.text = model.pageViews;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
