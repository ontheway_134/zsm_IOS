//
//  ICN_IndustryViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/2.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndustryViewController.h"
#import "ICN_IndustryModel.h"
#import "ICN_IndustryTableViewCell.h"
#import "LKScrollView.h"
#import "ICN_IndustryGDModel.h"

#import "ICN_IndustryNextViewController.h"

@interface ICN_IndustryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong)LKScrollView *lksview;
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSMutableArray *dataArr;
@property(nonatomic,strong)NSMutableArray *topGDArr;
@property(nonatomic,assign)NSInteger page;




@end

@implementation ICN_IndustryViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createUI];
    [self datas];
}
-(void)loadMoreData{
    
    _page ++;
    
    [self datas];
    
    
}
-(void)refreshingData{
    
    _page = 1;
    [self datas];
    
    
}
-(void)datas{
    
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewsBanPic1" params:nil success:^(id result) {
        
        NSDictionary *dict =result[@"result"];
        _topGDArr = [[NSMutableArray alloc]init];
        
        for (NSDictionary *dic in dict) {
            ICN_IndustryGDModel *model = [[ICN_IndustryGDModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_topGDArr addObject:model.lunbo_pic];
            
        }
        
        [_lksview loadDataWithArray:_topGDArr placeholderImage:nil andPageControlType:1];
        [_tableView reloadData];
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
    
    
    
    NSDictionary *dic = @{@"page":[NSString stringWithFormat:@"%ld",_page]};
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewslist1" params:dic success:^(id result) {
        
        if (_page == 1) {
            [_dataArr removeAllObjects];
        }
        
        NSArray *arr = result[@"result"];
                for (NSDictionary *dic in arr) {
            ICN_IndustryModel *model = [[ICN_IndustryModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
        }
        
        if (_page == 1) {
            [_tableView.mj_header endRefreshing];
        }
        [_tableView.mj_footer endRefreshing];
        [_tableView reloadData];
    } failure:^(NSDictionary *errorInfo) {
        
    }];


}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    ICN_IndustryNextViewController *icn = [[ICN_IndustryNextViewController alloc]init];
    ICN_IndustryModel *model = _dataArr[indexPath.row];
    
    icn.url = model.infoid;
    
    icn.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:icn animated:YES];
    [self datas];
    

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 103;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArr.count;
    

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ICN_IndustryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    ICN_IndustryModel *model = _dataArr[indexPath.row];
    cell.model = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;



}
-(void)createUI{

    
//    UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 130)];
//    imageV.image = [UIImage imageNamed:@"banner.png"];
    

    _page = 1;
    _dataArr = [[NSMutableArray alloc]init];

    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64 - 49) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    MJRefreshGifHeader *header = [MJRefreshGifHeader headerWithRefreshingBlock:^{
        [self refreshingData];
    }];
    header.stateLabel.hidden = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    
    [header setImages:@[[UIImage imageNamed:@"刷新图"]] forState:MJRefreshStatePulling];
  
    NSMutableArray *imagelist = [NSMutableArray array];
    for (NSInteger i = 1; i <= 35; i++) {
        [imagelist addObject:[UIImage imageNamed:SF(@"加载大象动画%ld",(long)i)]];
    }
    [header setImages:@[imagelist[0]] forState:MJRefreshStateIdle];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStatePulling];
    [header setImages:imagelist duration:1.0 forState:MJRefreshStateRefreshing];
    [header setImages:@[imagelist.lastObject] forState:MJRefreshStateNoMoreData];
    self.tableView.mj_header = header;
    _tableView.mj_footer = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    [_tableView registerClass:[ICN_IndustryTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_tableView];
    
    _lksview = [[LKScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 170)];

    _tableView.tableHeaderView = _lksview;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
