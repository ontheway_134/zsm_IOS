//
//  ICN_IndustryNextViewController.m
//  ICan
//
//  Created by 那风__ on 16/12/13.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndustryNextViewController.h"
#import "ICN_ICN_IndustryNextModel.h"
#import "ICN_reportReportViewController.h"
#import "ICN_DynWarnView.h"
#import "ICN_ShareManager.h"
#import "ICN_ApplyModel.h"
#import "ICN_StartViewController.h"
#import "ToolAboutTime.h"
#import "ICN_CommonShareModel.h"

@interface ICN_IndustryNextViewController ()<UIGestureRecognizerDelegate,ICN_DynWarnViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UILabel *wenziLabel;
@property (nonatomic , strong)UIView *alongView;
@property (nonatomic , strong)UIImageView *downImgView;
@property (nonatomic , strong)UITapGestureRecognizer *tap;

@property(nonatomic)UILabel *titleLabel;
@property(nonatomic)UILabel *dataLabel;
@property(nonatomic)UIImageView *picImageView;
@property(nonatomic)UILabel *summaryLabel;

@property(nonatomic)NSString *collect;

@property(nonatomic)UIView *contentView;
//分享
@property(nonatomic)ICN_DynWarnView *replayView;

@property(nonatomic)UIView *winView;
@property(nonatomic)UIView *shareView;

@property (nonatomic , strong)ICN_ICN_IndustryNextModel *shareModel; // 分享需要的Model
@end

@implementation ICN_IndustryNextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    //self.tap.delegate = self;
    if (![self getCurrentUserLoginStatus]) {
        ICN_StartViewController *vc = [[ICN_StartViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [self createUI];
        [self datas];
    }
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.scrollView.mj_header beginRefreshing];
}

-(void)btnAction:(UIButton *)btn{

    
    ICN_reportReportViewController  * report = [[ICN_reportReportViewController alloc]init];
    _alongView.hidden = YES;
    _downImgView.hidden = YES;
    report.tempStr = @"1";
    report.urlStr = _url;
    [self.navigationController pushViewController:report animated:YES];


}
//收藏
-(void)collectionAction:(UIButton *)btn{
    ///btn.selected = !btn.selected;
    if ([_collect isEqualToString:@"0"]) {
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }

        NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url],@"token":token};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollect" params:dic success:^(id result) {
           
            if ([_collect isEqualToString:@"0"]) {
              [btn setTitle:@"取消收藏" forState:UIControlStateNormal];
                [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"收藏成功"];
            }
           
            [self datas];
            
            
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    }else{
        NSString *token;
        if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
            token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
        }

    NSDictionary *dic = @{@"informationId":[NSString stringWithFormat:@"%@",_url],@"token":token};
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/positionNewsCollectDel" params:dic success:^(id result) {
          
           
            if ([_collect isEqualToString:@"1"]) {
                [btn setTitle:@"收藏" forState:UIControlStateNormal];
                 [MBProgressHUD ShowProgressWithBaseView:self.view Message:@"取消收藏"];
            }
            
            
            
            
              [self datas];
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];
    
    }
   


}

-(void)datas{
    NSString *token;
    if ([USERDEFAULT valueForKey:HR_CurrentUserToken] != nil) {
        token = [USERDEFAULT valueForKey:HR_CurrentUserToken];
    }
    
    if (token == nil) {
        return;
    }else{
        NSDictionary *dic = @{@"infoid":[NSString stringWithFormat:@"%@",_url],@"token":token};
        
        [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/PositionNews/postionNewsdetail" params:dic success:^(id result) {
            
            NSLog(@"%@",result);
            if ([[result valueForKey:@"code"] integerValue] != 0 ) {
                return ;
            }
            else{
            
                NSDictionary *dict = result[@"result"][0];
                //NSDictionary *dict0 = dict[@"0"];
                ICN_ICN_IndustryNextModel *model = [[ICN_ICN_IndustryNextModel alloc]init];
                [model setValuesForKeysWithDictionary:dict];
                self.shareModel = model;
                _titleLabel.text = model.title;
                
                if (model.createDate == nil) {
                    model.createDate = @"";
                }
                _dataLabel.text = [ToolAboutTime getTimeStrByTimeSp:model.createDate];
                
                [_picImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.pic]]];
                _summaryLabel.text = model.content;
                _collect  = [NSString stringWithFormat:@"%@",model.iscollect];
                NSLog(@"%@",model.iscollect);
                
                [_scrollView.mj_header endRefreshing];
            }
            
        } failure:^(NSDictionary *errorInfo) {
            
        }];

    }

    

}


- (void)onClick:(UIButton *)button{
    
//    if (_alongView) {
//        _alongView.hidden = YES;
//        _downImgView.hidden = YES;
//        return ;
//    }
    button.selected = !button.selected;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    view.tag = 111;
    _alongView = view;
    
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];

    UIImageView *imageV = [[UIImageView alloc]init];
    imageV.userInteractionEnabled = YES;
    imageV.image = [UIImage imageNamed:@"下拉.png"];
    imageV.tag = 112;
    [currentWindow addSubview:imageV];
    [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(60);
        make.width.mas_equalTo(75);
        make.height.mas_equalTo(118);
    }];
    UIButton *btn = [[UIButton alloc]init];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitle:@"举报" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [imageV addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(11);
        make.height.mas_equalTo(35);
        
    }];
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *btn2 = [[UIButton alloc]init];
    if ([_collect isEqualToString:@"0"]) {
        [btn2 setTitle:@"收藏" forState:UIControlStateNormal];
    }else{
    
    [btn2 setTitle:@"取消收藏" forState:UIControlStateNormal];
    }
    
    btn2.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn2 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(collectionAction:) forControlEvents:UIControlEventTouchUpInside];
    [imageV addSubview:btn2];
    [btn2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    _downImgView = imageV;
    UILabel *label2 = [[UILabel alloc]init];
    label2.backgroundColor = RGB0X(0Xe5e5e5);
    [imageV addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(btn2.mas_bottom).offset(0);
        make.height.mas_equalTo(1);
    }];
    
    UIButton *btn3 = [[UIButton alloc]init];
    [btn3 setTitle:@"分享" forState:UIControlStateNormal];
    btn3.titleLabel.font = [UIFont systemFontOfSize:11];
    [btn3 setTitleColor:RGB0X(0X333333) forState:UIControlStateNormal];
   [imageV addSubview:btn3];
    [btn3 addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(label2.mas_bottom).offset(0);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];

    // 单击的 Recognizer
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(SingleTap:)];
    //点击的次数
//    singleRecognizer.numberOfTapsRequired = 1; // 单击
    _tap = singleRecognizer;
    singleRecognizer.delegate = self;
    //给self.view添加一个手势监测；
    
    [_alongView addGestureRecognizer:singleRecognizer];
    
    

    
}
-(void)shareBtnAction:(UIButton *)btn{

// [[UIApplication sharedApplication].keyWindow addSubview:self.replayView];
    btn.selected = !btn.selected;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+64)];
    view.backgroundColor = [UIColor blackColor];
    view.alpha = 0.4;
    view.tag = 111;
    _winView = view;
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    
    [currentWindow addSubview:view];
    
    UIView *shareView = [[UIView alloc]init];
    shareView.backgroundColor = [UIColor whiteColor];
    _shareView = shareView;
    [currentWindow addSubview:shareView];
    [shareView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(0);
        make.right.mas_equalTo(self.view.mas_right).offset(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(150);
    }];
    
    UIButton *wxBtn = [[UIButton alloc]init];
    [wxBtn setImage:[UIImage imageNamed:@"微信.png"] forState:UIControlStateNormal];
    
    [shareView addSubview:wxBtn];
    [wxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(10);
        
    }];
    
    UILabel *wxLabel = [[UILabel alloc]init];
    wxLabel.text = @"微信";
    wxLabel.textColor = RGB0X(0x333333);
    wxLabel.font = [UIFont systemFontOfSize:12];
    [wxBtn addTarget:self action:@selector(wxBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:wxLabel];
    [wxLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wxBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
        make.left.mas_equalTo(17);
    }];
    
    UIButton *wbBtn = [[UIButton alloc]init];
    [wbBtn setImage:[UIImage imageNamed:@"微博.png"] forState:UIControlStateNormal];
    [wbBtn addTarget:self action:@selector(wbBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:wbBtn];
    [wbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(wxBtn.mas_right).offset(35);
        
    }];
    
    UILabel *wbLabel = [[UILabel alloc]init];
    wbLabel.text = @"微博";
    wbLabel.textColor = RGB0X(0x333333);
    wbLabel.font = [UIFont systemFontOfSize:12];
    [shareView addSubview:wbLabel];
    [wbLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(wbBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
        make.left.mas_equalTo(92);
    }];
    
    
    UIButton *qqBtn = [[UIButton alloc]init];
    [qqBtn setImage:[UIImage imageNamed:@"QQ.png"] forState:UIControlStateNormal];
    [qqBtn addTarget:self action:@selector(qqBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:qqBtn];
    [qqBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(25);
        make.left.mas_equalTo(wbBtn.mas_right).offset(35);
        
    }];
    
    UILabel *qqLabel = [[UILabel alloc]init];
    qqLabel.text = @"QQ";
    qqLabel.textColor = RGB0X(0x333333);
    qqLabel.font = [UIFont systemFontOfSize:12];
    [shareView addSubview:qqLabel];
    [qqLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(qqBtn.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
        
        make.left.mas_equalTo(171);
    }];
    
    UIButton *gbBtn = [[UIButton alloc]init];
    [gbBtn setImage:[UIImage imageNamed:@"弹窗关闭.png"] forState:UIControlStateNormal];
    [gbBtn addTarget:self action:@selector(gbBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [shareView addSubview:gbBtn];
    
    [gbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(qqLabel.mas_bottom).offset(15);
        make.bottom.mas_equalTo(shareView.mas_bottom).offset(0);
    }];
    
    // 单击的 Recognizer
    UITapGestureRecognizer* singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    //点击的次数
    //    singleRecognizer.numberOfTapsRequired = 1; // 单击
    //_tap = singleRecognizer;
    singleRecognizer.delegate = self;
    //给self.view添加一个手势监测；
    
    [view addGestureRecognizer:singleRecognizer];


}

//=== 分享  分享行业资讯和职场规划：图片、标题（超10字部分“…”）、内容（超20字部分“…”
-(void)wxBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
        NSLog(@"%@",result);
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}

// 职场相关分享 --微博
-(void)wbBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
        
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}


// 职场相关分享 --QQ
-(void)qqBtnAction:(UIButton *)btn{
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    _downImgView.hidden = YES;
    _alongView.hidden = YES;
    NSDictionary *dic = @{@"informationid":_url};
    ICN_CommonShareModel *model = [[ICN_CommonShareModel alloc] initWithTitle:self.shareModel.title Detial:self.shareModel.content ImageUrl:self.shareModel.pic];
    [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
        
        ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
        if (basemodel.code == 0) {
            
            [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:model.title andImage:model.imageUrl Detial:model.detial];
        }else{
            [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
            
        }
        
        
    } failure:^(NSDictionary *errorInfo) {
        
    }];
    
}
-(void)gbBtnAction:(UIButton *)btn{
    
    _winView.hidden = YES;
    _shareView.hidden = YES;
}
-(void)tapAction:(UITapGestureRecognizer*)recognizer  {
    
    
    _winView.hidden = YES;
    _shareView.hidden = YES;
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [APPLICATION.keyWindow removeGestureRecognizer:_tap];
    if (_replayView) {
        [_replayView removeFromSuperview];
        _replayView = nil;
    }
}
- (void)responsedButtonClickWithBtnType:(NSInteger)type{
    switch (type) {
        case ICN_WarnBottomBtnType:{
            HRLog(@"点击的是底部的按钮");
            [self.replayView removeFromSuperview];
            break;
        }
        case ICN_ShareToDynamicStateBtnType:{
            // 分享到 动态
            
            break;
        }
        case ICN_ShareToICNFriendBtnType:{
            // 分享到 I行好友
            break;
        }
        case ICN_ShareToWeiChatBtnType:{
            // 分享到 微信
            
            NSDictionary *dic = @{@"positionId":_url};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
                NSLog(@"%@",result);
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0) {
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatSession andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            break;
        }
        case ICN_ShareToWeiBOBtnType:{
            // 分享到 微博
            
            
            NSDictionary *dic = @{@"positionId":_url};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
                
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0) {
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Sina andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:basemodel.result.pic  Detial:@""];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            
            break;
        }
        case ICN_ShareToQQBtnType:{
            // 分享到 QQ
            NSDictionary *dic = @{@"positionId":_url};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
                
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0) {
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_QQ andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            
            break;
        }
            
        case ICN_ShareToWeiFridBtnType:{
            
            NSDictionary *dic = @{@"positionId":_url};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
                
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0) {
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_WechatTimeLine andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            
            break;

        }
        case ICN_ShareToQQFridBtnType:{
            
            NSLog(@"分享到QQ空间");
            NSDictionary *dic = @{@"positionId":_url};
            [[[HRRequestManager alloc]init]POST_URL:@"http://1ican.com/index.php/MyPosition/ShareLfx/news" params:dic success:^(id result) {
                
                ICN_ApplyModel *basemodel=[[ICN_ApplyModel alloc]initWithDictionary:result error:nil];
                if (basemodel.code == 0) {
                    
                    [[ICN_ShareManager defaultInstance] shareWebPageToPlatformType:UMSocialPlatformType_Qzone andVC:self andUrl:basemodel.result.src andTitle:@"ggg" andImage:nil Detial:@""];
                }else{
                    [MBProgressHUD ShowProgressWithBaseView:self.view Message:basemodel.info];
                    
                }
                
                
            } failure:^(NSDictionary *errorInfo) {
                
            }];
            
            
            break;
        }
   
            
        default:
            //[self.warnView removeFromSuperview];
            [self.replayView removeFromSuperview];
            break;
    }
}

- (ICN_DynWarnView *)replayView{
    
    if (_replayView == nil) {
        NSArray *icons = ICN_BtnListIconsArr;
        _replayView = [[ICN_DynWarnView alloc] loadXibWarnViewsWithIcons:icons TitleLabels:nil TabbarHidden:YES];
        CGRect frame = SCREEN_BOUNDS;
        _replayView.frame = frame;
        _replayView.delegate = self;
    }
    
    if (_replayView.hidden) {
        _replayView.hidden = NO;
    }
    
    return _replayView;
}

-(void)SingleTap:(UITapGestureRecognizer*)recognizer  {

   
        _downImgView.hidden = YES;
        _alongView.hidden = YES;
    _alongView = nil;
 
    

}
-(void)leftItemClicked:(UIBarButtonItem *)btn{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(void)createUI{

    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"资讯详情";
    [self.navigationController.navigationBar setTitleTextAttributes:
  @{NSFontAttributeName:[UIFont systemFontOfSize:16],
    NSForegroundColorAttributeName:RGB0X(0Xffffff)}];
    
    UIButton *btn =[[UIButton alloc]initWithFrame:CGRectMake(10, 3, 10, 16)];
   
    [btn addTarget:self action:@selector(leftItemClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"返回.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftItem3 = [[UIBarButtonItem alloc]initWithCustomView:btn];
   
    self.navigationItem.leftBarButtonItem = leftItem3;
    
    
    
    UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(0,0, 20, 5)];
    
    [btn1 addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setBackgroundImage:[[UIImage imageNamed:@"更多-点.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithCustomView:btn1];
    self.navigationItem.rightBarButtonItem  = item;

   
     
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    
     [self.view addSubview:_scrollView];
    
    _scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self datas];
    }];
    
    _contentView = [[UIView alloc]init];
    
    [_scrollView addSubview:_contentView];
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.scrollView);
        make.width.equalTo(self.scrollView);
    }];

    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont systemFontOfSize:15];
    _titleLabel.text = @"";
    _titleLabel.numberOfLines = 0;
    _titleLabel.textColor = RGB0X(0X000000);
    [_contentView addSubview:_titleLabel];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
       make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        //make.width.mas_equalTo(100);
        
        make.top.mas_equalTo(15);
        
    }];
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [UIImage imageNamed:@"时间.png"];

    [_contentView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        
    }];
    
    _dataLabel =[[UILabel alloc]init];
    _dataLabel.text = @"";
    _dataLabel.textColor = RGB0X(0X666666);
    _dataLabel.font = [UIFont systemFontOfSize:12];
    [_contentView addSubview:_dataLabel];
    [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imageView.mas_right).offset(5);
        make.top.mas_equalTo(_titleLabel.mas_bottom).offset(10);
        make.height.mas_equalTo(12);
    }];
    
    UILabel *xiantiaoLabel = [[UILabel alloc]init];
    xiantiaoLabel.backgroundColor = RGB(236, 236, 236);
    [_contentView addSubview:xiantiaoLabel];
    
    [xiantiaoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_dataLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(1);
        
    }];
    
    _picImageView = [[UIImageView alloc]init];
    _picImageView.image = [UIImage imageNamed:@"图片.png"];
    [_contentView addSubview:_picImageView];
    [_picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(xiantiaoLabel.mas_bottom).offset(15);
        make.height.mas_equalTo(160);
    }];
    
    _summaryLabel = [[UILabel alloc]init];
    _summaryLabel.text = @"";
    _summaryLabel.textColor = RGB0X(0X333333);
    //wenziLabel.font = [UIFont systemFontOfSize:12];
    _summaryLabel.numberOfLines = 0;
    _summaryLabel.font = [UIFont systemFontOfSize:13];
    [_contentView addSubview:_summaryLabel];
    [_summaryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-10);
        make.top.mas_equalTo(_picImageView.mas_bottom).offset(10);
        make.bottom.mas_equalTo(_contentView.mas_bottom).offset(0);

    }];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    
//    if ([touch.view isKindOfClass:[UIButton class]]){
//        
//        return NO;
//        
//    }
//        return YES;
//    
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
