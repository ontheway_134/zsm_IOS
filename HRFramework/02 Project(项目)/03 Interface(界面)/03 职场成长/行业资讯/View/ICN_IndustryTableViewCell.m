//
//  ICN_IndustryTableViewCell.m
//  ICan
//
//  Created by 那风__ on 16/12/9.
//  Copyright © 2016年 albert. All rights reserved.
//

#import "ICN_IndustryTableViewCell.h"
#import "ICN_IndustryModel.h"
@implementation ICN_IndustryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, 98)];
        [self.contentView addSubview:view];
        
        
        _picImage = [[UIImageView alloc]init];
        [view addSubview:_picImage];
        [_picImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.right.mas_equalTo(self.contentView.mas_right).offset(-10);
            make.size.mas_equalTo(CGSizeMake(70, 70));
            
        }];
        
        
        
        _titleLabel = [[UILabel alloc]init];
        [view addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(_picImage.mas_left).offset(-15);
            make.height.mas_equalTo(28);
        }];
        _titleLabel.textColor = RGB0X(0X000000);
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.numberOfLines = 0;
        
        UIImageView *imageV = [[UIImageView alloc]init];
        imageV.image = [UIImage imageNamed:@"时间.png"];
        [view addSubview:imageV];
        [imageV mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(10);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            
        }];
        
        _dataLabel = [[UILabel alloc]init];
        [view addSubview:_dataLabel];
        [_dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            make.left.mas_equalTo(imageV.mas_right).offset(5);
            make.height.mas_equalTo(12);
        }];
        _dataLabel.textColor = RGB0X(0X666666);
        _dataLabel.font = [UIFont systemFontOfSize:12];
        
        UIImageView *imageV2 = [[UIImageView alloc]init];
        imageV2.image = [UIImage imageNamed:@"阅读量.png"];
        [view addSubview:imageV2];
        [imageV2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28.8);
            make.left.mas_equalTo(_dataLabel.mas_right).offset(15);
            
        }];
        
        _pageLabel = [[UILabel alloc]init];
        [view addSubview:_pageLabel];
        [_pageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(28);
            make.left.mas_equalTo(imageV2.mas_right).offset(5);
            make.height.mas_equalTo(12);
        }];
        _pageLabel.textColor = RGB0X(0X666666);
        _pageLabel.font = [UIFont systemFontOfSize:12];
        
        UIView *view2 = [[UIView alloc]init];
        view2.backgroundColor = RGB(236, 236, 236);
        
        [self.contentView addSubview:view2];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(self.contentView.mas_right).offset(0);
            make.left.mas_equalTo(self.contentView.mas_left).offset(0);
            make.top.mas_equalTo(view.mas_bottom).offset(0);
            make.height.mas_equalTo(5);
        }];
    }

    return self;

}
-(void)setModel:(ICN_IndustryModel *)model{

    [_picImage sd_setImageWithURL:[NSURL URLWithString:model.pic]];
    _titleLabel.text = model.title;
    _dataLabel.text = model.createDate;
    _pageLabel.text = model.pageViews;

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
