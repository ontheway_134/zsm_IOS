//
//  AppDelegate+NetStatusObsver.m
//  YKJ.iOS
//
//  Created by 高阳 on 16/10/19.
//  Copyright © 2016年 高阳. All rights reserved.
//

#import "AppDelegate+NetStatusObsver.h"
#import "AFNetworkReachabilityManager.h"

@implementation AppDelegate (NetStatusObsver)

#pragma mark - ---------- 私有方法 ----------

- (void)createReachAblilityNetWorkManager{
    //获取AFN网络状态监控管理器单例
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    //调用管理器
    [manager startMonitoring];
    //网络状态改变时调用的回调block
    //1. 将网络状态枚举以nsnumber的形式存储在userdefault中
    //2. 当没有网络的时候弹出提示窗
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown: {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:HR_AlbertUNKNOW_NetStatus] forKey:HR_NETWORK_STATUS];
                break;
            }
            case AFNetworkReachabilityStatusNotReachable: {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:HR_AlbertNONE_NetStatus] forKey:HR_NETWORK_STATUS];
                [self appdelegatePushNoticationToCurrentViewControllerWithEnum:0];
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWWAN: {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:HR_AlbertMOBILE_NetStatus] forKey:HR_NETWORK_STATUS];
                break;
            }
            case AFNetworkReachabilityStatusReachableViaWiFi: {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:HR_AlbertWIFI_NetStatus] forKey:HR_NETWORK_STATUS];
                break;
            }
        }
    }];
    
}

- (void)appdelegatePushNoticationToCurrentViewControllerWithEnum:(NSInteger)enumIndex{
    switch (enumIndex) {
        case 0:
            //执行弹出网络请求的通知
            
            [[NSNotificationCenter defaultCenter] postNotificationName:HR_NONENETWORK_NOTIFICATION object:self];
            
            break;
            
        default:
            break;
    }
}

@end
