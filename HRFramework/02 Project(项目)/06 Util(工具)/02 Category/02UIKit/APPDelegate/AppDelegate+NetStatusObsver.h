//
//  AppDelegate+NetStatusObsver.h
//  YKJ.iOS
//
//  Created by 高阳 on 16/10/19.
//  Copyright © 2016年 高阳. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (NetStatusObsver)

/** 使用appdelegate对象本身创建AFN的网络状态监控的方法 */
- (void)createReachAblilityNetWorkManager;

//- (void)appdelegatePushNoticationToCurrentViewControllerWithEnum:(NSInteger)enumIndex;


@end
