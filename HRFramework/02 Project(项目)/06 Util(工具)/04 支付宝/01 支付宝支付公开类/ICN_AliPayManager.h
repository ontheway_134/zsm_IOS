//
//  ICN_AliPayManager.h
//  ICan
//
//  Created by albert on 2017/2/27.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICN_AliPayManager : NSObject

/** 根据订单号调用支付宝支付 
   1. 如果不存在token的时候返回no
 */
+ (BOOL)AliPayWithActivityId:(NSString *)activityId;

@end
