//
//  ICN_ShareManager.h
//  ICan
//
//  Created by shilei on 17/1/18.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMSocialCore/UMSocialCore.h>

typedef NS_ENUM(NSInteger, QJSharePlatformType)
{
    
    QJSharePlatformTypeForQQ       = 0, // QQ
    QJSharePlatformTypeForSin      = 1, // sina
    QJSharePlatformTypeForQQZone   = 2, // qzong
    QJSharePlatformTypeForWeiXin   = 3, // weChat
    QJSharePlatformTypeForWeiChat  = 4  // weTimeline
    
};


@interface ICN_ShareManager : NSObject

/**
 *  分享--单利对象
 *
 *  @return self
 */
+ (ICN_ShareManager *)defaultInstance;


/**
 分享

 @param platformType 分享平台
 @param MVC 控制器
 @param url 分享网址
 @param title 分享文字
 @param image 分享图片
 */
- (void)shareWebPageToPlatformType:(UMSocialPlatformType)platformType andVC:(UIViewController *)MVC andUrl:(NSString *)url andTitle:(NSString *)title andImage:(NSString *)image Detial:(NSString *)detial;



@end
