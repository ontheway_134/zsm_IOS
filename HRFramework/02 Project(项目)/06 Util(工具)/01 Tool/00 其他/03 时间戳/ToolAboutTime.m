//
//  ToolAboutTime.m
//  数字尾巴
//
//  Created by dllo on 16/2/25.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "ToolAboutTime.h"

@implementation ToolAboutTime

+ (NSString *)getTimeStrByTimeSp:(NSString *)timeSp{
    //时间戳转时间的方法
    timeSp = [timeSp substringToIndex:10];
    NSTimeInterval time2 =[timeSp doubleValue];
    NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:time2];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    NSString *currentTime = [formatter stringFromDate:date2];
    return currentTime;
}


@end
