//
//  MBProgressHUD+PrograssHUDHelper.h
//  ICan
//
//  Created by albert on 2016/12/20.
//  Copyright © 2016年 albert. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (PrograssHUDHelper)

+ (void)ShowProgressWithBaseView:(UIView *)view Message:(NSString *)message;

/**  不主动调用方法就会一直执行的HUD  **/
+ (void)ShowProgressToSuperView:(UIView *)view Message:(NSString *)message;

/**  主动隐藏HUD的方法  **/
+ (void)hiddenHUDWithSuperView:(UIView *)view;

@end
