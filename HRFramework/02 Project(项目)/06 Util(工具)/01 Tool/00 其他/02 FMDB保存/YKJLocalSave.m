//
//  YKJLocalSave.m
//  YKJ.iOS
//
//  Created by 高阳 on 16/10/19.
//  Copyright © 2016年 高阳. All rights reserved.
//

#import "YKJLocalSave.h"
#define HR_Albert_TableList @"albertTableList"

static const NSString *CurrentDBName = @"defaultDB.db";


@implementation YKJLocalSave

/** YTKKeyValueStore *store = [[YTKKeyValueStore alloc] initDBWithName:@"test.db"];
 NSString *tableName = @"user_table";
 [store createTableWithName:tableName];
 // 保存
 NSString *key = @"1";
 NSDictionary *user = @{@"id": @1, @"name": @"tangqiao", @"age": @30};
 [store putObject:user withId:key intoTable:tableName];
 // 查询
 NSDictionary *queryUser = [store getObjectById:key fromTable:tableName];
 NSLog(@"query data result: %@", queryUser); */

+ (BOOL)saveDataLocalWithModuleName:(NSString *)module pahtName:(NSString *)path ContentData:(id)data{
    if (module == nil) {
        return NO;
    }
    if (path == nil) {
        return NO;
    }
    if (data == nil) {
        return NO;
    }
    YKJKeyValueStore *store = [[YKJKeyValueStore alloc] initDBWithName:SF(@"%@", CurrentDBName)];
    
    [store createTableWithName:SF(@"%@" , module)];
    
    [store putObject:data withId:SF(@"%@" , path) intoTable:SF(@"%@" , module)];
    
    NSString *tableNames = [USERDEFAULT valueForKey:HR_Albert_TableList];
    if (tableNames == nil) {
        tableNames = SF(@"%@" , module);
    }else{
        tableNames = SF(@"%@,%@" , module , tableNames);
    }
    
    [store close];
    
    return YES;
    
}

+ (id)getLocalDataWithModuleName:(NSString *)module pahtName:(NSString *)path{
    if (module == nil) {
        return nil;
    }
    if (path == nil) {
        return nil;
    }
    
    YKJKeyValueStore *store = [[YKJKeyValueStore alloc] initDBWithName:SF(@"%@", CurrentDBName)];
    
    [store createTableWithName:SF(@"%@" , module)];
    
    
    return [store getObjectById:SF(@"%@" , path) fromTable:SF(@"%@" , module)];
}

+ (BOOL)clearLocalDataCache{
    NSString *tableNames = [USERDEFAULT valueForKey:HR_Albert_TableList];
    if (tableNames == nil) {
        return YES;
    }else{
        YKJKeyValueStore *store = [[YKJKeyValueStore alloc] initDBWithName:SF(@"%@", CurrentDBName)];
        NSArray *array = [tableNames componentsSeparatedByString:@","];
        for (NSString *tableName in array) {
            [store clearTable:SF(@"%@",tableName)];
        }
        tableNames = @" ";
        return YES;
    }
}

@end
