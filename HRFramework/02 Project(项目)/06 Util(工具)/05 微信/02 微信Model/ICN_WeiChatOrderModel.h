//
//  ICN_WeiChatOrderModel.h
//  ICan  微信预订单Model
//
//  Created by albert on 2017/2/21.
//  Copyright © 2017年 albert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseOptionalModel.h"

@interface ICN_WeiChatOrderModel : BaseOptionalModel

//appid = wxe0ec26e811385ef5;
//noncestr = PzQhmoWaJrYM01KAgvRZflDtSFR8KFZ7;
//package = "Sign=WXPay";
//partnerid = 1440431502;
//prepayid = wx20170221161340adfcab275d0361156498;
//serverOrder = 599;
//sign = D75FDC165744D4486B69B8C7DB3D6B9C;
//timestamp = 1487664820;


@property (nonatomic , copy)NSString *appid; // 微信公众账号ID 即appid
@property (nonatomic , copy)NSString *package; // 固定签名

@property (nonatomic , copy)NSString *partnerid; // 微信商户号
@property (nonatomic , copy)NSString *noncestr; // 微信返回的随机字符串,
@property (nonatomic , copy)NSString *sign; // 微信返回的签名值,
@property (nonatomic , copy)NSString *prepayid; // 微信生成的预支付回话标识,
@property (nonatomic , copy)NSString *serverOrder; // 系统订单号(需在下一接口中返回给后台)
@property (nonatomic , copy)NSString *timestamp; // 时间戳



@end
